from ariadne.contrib.tracing.opentelemetry import OpenTelemetryExtension
from ariadne_django.views import GraphQLView
from django.conf import settings
from django.contrib.sitemaps.views import sitemap
from django.shortcuts import render
from django.urls import include, path, re_path
from django.views.decorators.cache import cache_page

from core import views as core_views
from core.rss import RssFeed
from core.sitemaps import sitemaps
from entities.event import views as event_views
from entities.file import views as file_views
from tenants import views as tenants_views
from user import views as user_views

from .schema import schema

urlpatterns = [
    path("unsubscribe/<str:token>", core_views.unsubscribe, name="unsubscribe"),
    path("logout", core_views.logout, name="logout"),
    path("action/logout", core_views.logout),
    path("register", core_views.register, name="register"),
    path("login", core_views.login, name="login"),
    path("oidc/failure/", core_views.logout, name="oidc_failure"),
    path("oidc/", include("mozilla_django_oidc.urls")),
    path(
        "graphql",
        GraphQLView.as_view(
            schema=schema,
            introspection=settings.DEBUG,
            extensions=[OpenTelemetryExtension],
        ),
        name="graphql",
    ),
    path("file/download/<uuid:file_id>", file_views.download, name="download"),
    path(
        "file/download/<uuid:file_id>/<str:file_name>",
        file_views.download,
        name="download",
    ),
    path("file/embed/<uuid:file_id>", file_views.embed, name="embed"),
    path("file/embed/<uuid:file_id>/<str:file_name>", file_views.embed, name="embed"),
    path("file/thumbnail/<uuid:file_id>", file_views.thumbnail, name="thumbnail"),
    path("file/featured/<uuid:entity_guid>", file_views.featured, name="featured"),
    path("attachment/<uuid:attachment_id>", core_views.attachment, name="attachment"),
    # old url for backwards compatability
    path(
        "attachment/<str:attachment_type>/<uuid:attachment_id>",
        core_views.attachment,
        name="attachment",
    ),
    path(
        "agreement/<slug:slug>",
        tenants_views.site_agreement_version_document,
        name="agreement",
    ),
    path(
        "custom_agreement/<int:custom_agreement_id>",
        core_views.site_custom_agreement,
        name="custom_agreement",
    ),
    path("bulk_download", file_views.bulk_download, name="bulk_download"),
    path(
        "download_rich_description_as/<uuid:entity_id>/<str:file_type>",
        core_views.download_rich_description_as,
        name="download_rich_description_as",
    ),
    path("events/view/guest-list", event_views.check_in, name="check_in"),
    path(
        "exporting/content/selected",
        core_views.export_selected_content,
        name="selected_content_export",
    ),
    path(
        "exporting/content/<str:content_type>",
        core_views.export_content,
        name="content_export_type",
    ),
    path(
        "exporting/group/<uuid:group_id>",
        core_views.export_group_members,
        name="group_members_export",
    ),
    path("exporting/event/<uuid:event_id>", event_views.export, name="event_export"),
    path(
        "exporting/calendar/", event_views.export_calendar, name="event_calendar_export"
    ),
    path("exporting/users", user_views.export, name="users_export"),
    path(
        "exporting/banned-users",
        user_views.export_banned_users,
        name="users_export_banned",
    ),
    path(
        "exporting/group-owners",
        core_views.export_groupowners,
        name="group_owners_export",
    ),
    path("qr/url/<uuid:entity_id>", core_views.get_url_qr, name="url_qr"),
    path("qr/access/<uuid:entity_id>", event_views.get_access_qr, name="access_qr"),
    path(
        "comment/confirm/<uuid:entity_id>",
        core_views.comment_confirm,
        name="comment_confirm",
    ),
    path("onboarding", core_views.OnboardingView.as_view(), name="onboarding"),
    path(
        "onboarding/requested",
        core_views.OnboardingRequestedView.as_view(),
        name="access_requested",
    ),
    path(
        "unsupported_browser",
        core_views.unsupported_browser,
        name="unsupported_browser",
    ),
    path(
        "edit_email_settings/<str:token>",
        core_views.edit_email_settings,
        name="edit_email_settings",
    ),
    path("custom.css", core_views.custom_css),
    path("favicon-<int:size>.png", core_views.favicon),
    path("robots.txt", core_views.robots_txt),
    path(
        "service-worker.js",
        core_views.ServiceWorkerView.as_view(),
        name="service_worker",
    ),
    path(
        "sitemap.xml", cache_page(3600)(sitemap), {"sitemaps": sitemaps}, name="sitemap"
    ),
    path("flow/", include("flow.urls")),
    path("profile_sync_api/", include("profile_sync.urls")),
    path("api/", include("concierge.urls")),
    path("podcast/", include("entities.podcast.urls")),
    path(".well-known/security.txt", core_views.security_txt_view, name="security_txt"),
    path(
        ".well-known/security_txt_pgp.pub",
        core_views.security_txt_pgp,
        name="security_txt_pgp",
    ),
    path("rss", RssFeed(), name="rss"),
    path("rss/<str:entity_type>", RssFeed(), name="rss_entity_type"),
    path("manifest.json", core_views.manifest, name="manifest"),
    # Include elgg url's for redirects
    path("", include("elgg.urls")),
    # Default catch all URL's
    re_path(
        r"^.*\/view\/(?P<entity_id>[0-9A-Fa-f-]+)\/(?:[^\/.]+)$",
        core_views.entity_view,
        name="entity_view",
    ),
    re_path(r".*", core_views.default, name="default"),
]

handler404 = "core.views.default"


def handler500(request):
    response = render(request, "500.html", {})
    response.status_code = 500
    return response
