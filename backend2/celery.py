from __future__ import absolute_import, unicode_literals

import os

import psutil
from celery import Celery
from celery.signals import worker_process_init
from celery.utils.log import get_task_logger
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.instrumentation.celery import CeleryInstrumentor
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.sampling import TraceIdRatioBased

from .crontab import beat_schedule

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend2.settings")

logger = get_task_logger(__name__)

cpu_count = psutil.cpu_count() or 4

OTEL_EXPORTER_OTLP_ENDPOINT = os.getenv("OTEL_EXPORTER_OTLP_ENDPOINT")


@worker_process_init.connect(weak=False)
def init_celery_tracing(*args, **kwargs):
    if OTEL_EXPORTER_OTLP_ENDPOINT:
        try:
            OTEL_SAMPLER_RATIO = float(os.getenv("OTEL_SAMPLER_RATIO"))
        except Exception:
            OTEL_SAMPLER_RATIO = 0.1

        sampler = TraceIdRatioBased(OTEL_SAMPLER_RATIO)
        resource = Resource.create(
            attributes={"service.name": "background", "service.namespace": "pleio"}
        )

        trace.set_tracer_provider(TracerProvider(resource=resource, sampler=sampler))
        span_processor = BatchSpanProcessor(
            OTLPSpanExporter(endpoint=OTEL_EXPORTER_OTLP_ENDPOINT)
        )
        trace.get_tracer_provider().add_span_processor(span_processor)
    CeleryInstrumentor().instrument()


app = Celery("backend2")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")
app.logger = logger

app.conf.beat_schedule = beat_schedule
# commented out to use startup paramater
# app.conf.worker_concurrency = 8 if cpu_count > 8 else cpu_count
app.conf.task_reject_on_worker_lost = True

if os.getenv("CELERY_CONTROL_QUEUE") == "True":
    app.conf.task_routes = {
        "control.tasks.*": {"queue": "control"},
    }

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
