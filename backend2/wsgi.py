# pragma: no cover
import os

from django.core.wsgi import get_wsgi_application
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.instrumentation.django import DjangoInstrumentor
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.sampling import TraceIdRatioBased
from uwsgidecorators import postfork

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend2.settings")

OTEL_EXPORTER_OTLP_ENDPOINT = os.getenv("OTEL_EXPORTER_OTLP_ENDPOINT")


@postfork
def init_tracing():
    if OTEL_EXPORTER_OTLP_ENDPOINT:
        try:
            OTEL_SAMPLER_RATIO = float(os.getenv("OTEL_SAMPLER_RATIO"))
        except Exception:
            OTEL_SAMPLER_RATIO = 0.1

        sampler = TraceIdRatioBased(OTEL_SAMPLER_RATIO)
        resource = Resource.create(
            attributes={"service.name": "api", "service.namespace": "pleio"}
        )

        trace.set_tracer_provider(TracerProvider(resource=resource, sampler=sampler))
        span_processor = BatchSpanProcessor(
            OTLPSpanExporter(endpoint=OTEL_EXPORTER_OTLP_ENDPOINT)
        )
        trace.get_tracer_provider().add_span_processor(span_processor)


if OTEL_EXPORTER_OTLP_ENDPOINT:
    DjangoInstrumentor().instrument()

application = get_wsgi_application()
