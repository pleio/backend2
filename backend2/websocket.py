import logging
import os

import django
from ariadne.asgi.handlers import GraphQLTransportWSHandler
from asgiref.sync import sync_to_async
from django.conf import settings
from django.utils import timezone
from django_tenants.utils import tenant_context

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "backend2.settings")

django.setup()

# import after django.setup()
from backend2.asgi_middleware import MiddlewareStack  # noqa: E402
from backend2.schema import schema  # noqa: E402

logger = logging.getLogger(__name__)


@sync_to_async
def set_online_status(user, tenant, is_online):
    from django.contrib.auth.models import AnonymousUser

    if isinstance(user, AnonymousUser):
        return

    with tenant_context(tenant):
        user.profile.is_chat_online = is_online
        user.profile.is_chat_online_updated_at = timezone.now()
        user.profile.save(update_fields=["is_chat_online", "is_chat_online_updated_at"])


async def on_connect(websocket, params):
    user = websocket.scope.get("user", None)
    tenant = websocket.scope.get("tenant", None)
    if user and tenant:
        logger.info(
            "New connection: %s/%s ",
            tenant,
            user,
        )
        await set_online_status(user, tenant, True)


async def on_disconnect(websocket):
    user = websocket.scope.get("user", None)
    tenant = websocket.scope.get("tenant", None)
    if user and tenant:
        logger.info(
            "Disconnected: %s/%s ",
            tenant,
            user,
        )
        await set_online_status(user, tenant, False)


ariadne_handler = GraphQLTransportWSHandler(
    on_connect=on_connect, on_disconnect=on_disconnect
)
ariadne_handler.configure(schema=schema, debug=settings.DEBUG)


async def app(scope, receive, send):
    if scope["type"] == "websocket":
        await MiddlewareStack(ariadne_handler.handle)(scope, receive, send)
    elif scope["type"] == "http":
        await send(
            {
                "type": "http.response.start",
                "status": 200,
                "headers": [
                    [b"content-type", b"text/plain"],
                ],
            }
        )
        await send(
            {
                "type": "http.response.body",
                "body": b"OK",
            }
        )
