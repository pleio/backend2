from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import connections


class PrimaryReplicaRouter:
    def __init__(self):
        self.has_replica = bool(settings.DATABASES.get("replica", False))

    def db_for_read(self, model, **hints):
        """
        Directs read operations to a replica if available; otherwise, falls back to the primary database.
        """
        return "replica" if self.has_replica else "default"

    def db_for_write(self, model, **hints):
        """
        Directs all write operations to the primary database.
        """
        return "default"

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed if both objects are
        in the primary/replica pool.
        """
        db_set = {"default", "replica"}
        if obj1._state.db in db_set and obj2._state.db in db_set:  # pragma: no cover
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        All non-auth models end up in this pool.
        """
        return True


def extra_set_tenant_method(wrapper_class, tenant):
    """
    Set tenant on the replica database (if it exists)
    """

    # Not clearing cache can cause issues with ContentType.objects.get_for_model over different tenants
    ContentType.objects.clear_cache()

    if (
        not bool(settings.DATABASES.get("replica", False))
        or wrapper_class.settings_dict["DATABASE"] != "default"
    ):
        return

    try:  # pragma: no cover
        replica_connection = connections["replica"]
    except Exception:
        return

    if replica_connection.schema_name != tenant.schema_name:
        replica_connection.set_schema(tenant.schema_name)
