import socket

from django.http import HttpResponse
from django_tenants.middleware import TenantMainMiddleware


class PleioTenantMiddleware(TenantMainMiddleware):
    def get_tenant(self, domain_model, hostname):
        """Overwrites the default get_tenant to support disable active sites"""
        domain = domain_model.objects.select_related("tenant").get(
            domain=hostname, tenant__is_active=True
        )
        return domain.tenant


class HealthCheckMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path == "/_health":
            return HttpResponse("OK")
        return self.get_response(request)


class PodHeaderMiddleware:
    """
    Adds X-Pod header with the pod's hostname, allowing better troubleshooting
    """

    def __init__(self, get_response):
        self.get_response = get_response
        self.hostname = socket.gethostname()

    def __call__(self, request):
        response = self.get_response(request)
        response["X-Pod"] = self.hostname
        return response
