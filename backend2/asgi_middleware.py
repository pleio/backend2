from typing import Optional, Tuple

from ariadne.asgi import WebSocketConnectionError
from asgiref.sync import sync_to_async
from channels.sessions import CookieMiddleware, SessionMiddleware
from django.conf import settings
from django.contrib.auth import (
    BACKEND_SESSION_KEY,
    HASH_SESSION_KEY,
    SESSION_KEY,
    get_user_model,
    load_backend,
)
from django.utils.crypto import constant_time_compare
from django.utils.functional import LazyObject
from django_tenants.utils import get_tenant_domain_model, remove_www, tenant_context


class TenantMiddleware:
    """
    Base class for implementing ASGI middleware.
    Note that subclasses of this are not self-safe; don't store state on
    the instance, as it serves multiple application instances. Instead, use
    scope.
    """

    def __init__(self, inner):
        """
        Middleware constructor - just takes inner application.
        """
        self.inner = inner

    async def __call__(self, scope, receive, send):
        """
        ASGI application; can insert things into the scope and run asynchronous
        code.
        """
        # Copy scope to stop changes going upstream
        scope = dict(scope)

        hostname = self._get_host(scope["headers"])

        domain_model = get_tenant_domain_model()
        try:
            tenant = await self.get_tenant(domain_model, hostname)
        except domain_model.DoesNotExist:
            raise WebSocketConnectionError(
                {"message": "Tenant not found", "code": "auth"}
            )

        tenant.domain_url = hostname
        scope["tenant"] = tenant

        # Run the inner application along with the scope
        return await self.inner(scope, receive, send)

    @sync_to_async
    def get_tenant(self, domain_model, hostname):
        domain = domain_model.objects.select_related("tenant").get(domain=hostname)
        return domain.tenant

    def _get_host(self, headers: Tuple[bytes, bytes]) -> Optional[str]:
        for name, value in headers:
            if name.lower() == b"host":
                host = value.decode().split(":")[0]
                return remove_www(host)


class UserLazyObject(LazyObject):
    """
    Throw a more useful error message when scope['user'] is accessed before
    it's resolved
    """

    def _setup(self):
        msg = "Accessing scope user before it is ready."
        raise ValueError(msg)


class AuthMiddleware:
    def __init__(self, inner):
        """
        Middleware constructor - just takes inner application.
        """
        self.inner = inner

    def populate_scope(self, scope):
        # Make sure we have a session
        if "tenant" not in scope:
            msg = (
                "AuthMiddleware cannot find tenant in scope. "
                "TenantMiddleware must be above it."
            )
            raise ValueError(msg)

        if "session" not in scope:
            msg = (
                "AuthMiddleware cannot find session in scope. "
                "SessionMiddleware must be above it."
            )
            raise ValueError(msg)
        # Add it to the scope if it's not there already
        if "user" not in scope:
            scope["user"] = UserLazyObject()

    async def resolve_scope(self, scope):
        scope["user"]._wrapped = await self.get_user(scope)

    async def __call__(self, scope, receive, send):
        """
        ASGI application; can insert things into the scope and run asynchronous
        code.
        """
        # Copy scope to stop changes going upstream
        scope = dict(scope)
        # Scope injection/mutation per this middleware's needs.
        self.populate_scope(scope)
        # Grab the finalized/resolved scope
        await self.resolve_scope(scope)

        # Run the inner application along with the scope
        return await self.inner(scope, receive, send)

    @sync_to_async
    def get_user(self, scope):
        """
        Return the user model instance associated with the given scope.
        If no user is retrieved, return an instance of `AnonymousUser`.
        """
        # postpone model import to avoid ImproperlyConfigured error before Django
        # setup is complete.
        from django.contrib.auth.models import AnonymousUser

        if "session" not in scope:
            msg = (
                "Cannot find session in scope. You should wrap your consumer in "
                "SessionMiddleware."
            )
            raise ValueError(msg)
        session = scope["session"]
        user = None
        with tenant_context(scope["tenant"]):
            try:
                user_id = self._get_user_session_key(session)
                backend_path = session[BACKEND_SESSION_KEY]
            except KeyError:
                pass
            else:
                if backend_path in settings.AUTHENTICATION_BACKENDS:
                    backend = load_backend(backend_path)
                    user = backend.get_user(user_id)
                    # Verify the session
                    if hasattr(user, "get_session_auth_hash"):
                        session_hash = session.get(HASH_SESSION_KEY)
                        session_hash_verified = session_hash and constant_time_compare(
                            session_hash, user.get_session_auth_hash()
                        )
                        if not session_hash_verified:
                            session.flush()
                            user = None

        return user or AnonymousUser()

    def _get_user_session_key(self, session):
        # This value in the session is always serialized to a string, so we need
        # to convert it back to Python whenever we access it.
        return get_user_model()._meta.pk.to_python(session[SESSION_KEY])


# Handy shortcut for applying all three layers at once
def MiddlewareStack(inner):
    return TenantMiddleware(
        CookieMiddleware(SessionMiddleware(AuthMiddleware(AuthMiddleware(inner))))
    )
