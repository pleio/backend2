from mixer.backend.django import mixer

from core.tests.helpers import PleioTenantTestCase
from profile_sync.models import Logs
from user.models import User


class LogTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = mixer.blend(User, roles=[], is_delete_requested=False)
        self.admin = mixer.blend(User, roles=["ADMIN"], is_delete_requested=False)

        self.log_1 = mixer.blend(Logs, uuid="uuid_1", content="log_1")
        self.log_2 = mixer.blend(Logs, uuid="uuid_2", content="log_2")
        self.log_3 = mixer.blend(Logs, uuid="uuid_3", content="log_3")

        self.query = """
            query ProfileSyncLogs {
                profileSyncLogs {
                    total
                    edges {
                        uuid
                        content
                        timeCreated
                    }
                }
            }
        """

    def test_by_user(self):
        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.query, {})

    def test_by_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {})

        data = result["data"]
        self.assertEqual(data["profileSyncLogs"]["total"], 3)
        self.assertEqual(data["profileSyncLogs"]["edges"][0]["uuid"], "uuid_3")

    def test_not_logged_in(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query, {})
