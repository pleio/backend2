from django.db import models
from django.utils import timezone


class Logs(models.Model):
    class Meta:
        ordering = ("-created_at",)

    uuid = models.CharField(max_length=36)
    content = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)
