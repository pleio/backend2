### Description

Users van be added, edited, deleted through the profile sync api.

### Authorization

A Token can be configured by setting config.PROFILE_SYNC_TOKEN.

This can be used to do authorization, with a bearer header.

Use header
```
'Authorization': 'Bearer $token'
```

### Endpoints

**Get users**

Request: `GET /profile_sync_api/users`

Response:
```
{
  "users": [
    {
      "guid": "2f911792-0901-41d1-84b2-c6cfea79d191",
      "external_id": null,
      "name": "Test User",
      "email": "test@pleio.nl",
      "is_member": true,
      "is_banned": false,
      "time_created": "2020-07-16T15:13:25+00:00",
      "time_updated": "2023-05-25T12:29:24.421159+00:00",
      "icontime": null,
      "profile": {
        "werkzaambij": "Pleio"
      }
    },
```

**Post user**

- guid, uuid of user (optional)
- external_id, attribute to link local users with users on the subsite (optional)
- name, the full name of the user (required)
- email, the e-mailaddress of the user (required)
- avatar, a relative link to the avatar in jpeg of the user (optional)
- groups, a comma separated field of group guids that adds a user to a group, example field content: b052355d-df67-4e49-8e70-2bf3af3b9d0a,d68ad4c6-0cd4-4873-aef3-9c1e6589fe84 (optional)
- profile.*, a field containing profile information, example field name: profile.occupation (optional)

Request: `POST /profile_sync_api/users`
```
Form-Data (json):
    {
        "guid": "cc29afe2-2fc6-4cba-b340-80dc56ba6482",
        "name": "user name",
        "email": "test@pleio.nl",
        "external_id": "3243453452134234",
        "profile.telephone" "0123456789",
        "groups": "b052355d-df67-4e49-8e70-2bf3af3b9d0a,d68ad4c6-0cd4-4873-aef3-9c1e6589fe84"

    }
```

**Delete user**

Request: `DELETE /profile_sync_api/users/<uuid:user_id>`

Response:
```
{
  "status": 200
}
```


**Ban user**

Request: `POST /profile_sync_api/users/<uuid:user_id>/ban`

Response:
```
{
  "status": 200,
  "user": {
    "guid": "2f911792-0901-41d1-84b2-c6cfea79d191",
    "external_id": null,
    "name": "Test User",
    "email": "test@pleio.nl",
    "is_member": true,
    "is_banned": true,
    "time_created": "2020-07-16T15:13:25+00:00",
    "time_updated": "2023-05-25T12:29:24.421159+00:00",
    "icontime": null,
    "profile": {
      "werkzaambij": "Pleio"
    }
  }
}
```


**Unban user**

Request: `POST /profile_sync_api/users/<uuid:user_id>/unban`

Response:
```
{
  "status": 200,
  "user": {
    "guid": "2f911792-0901-41d1-84b2-c6cfea79d191",
    "external_id": null,
    "name": "Test User",
    "email": "test@pleio.nl",
    "is_member": true,
    "is_banned": false,
    "time_created": "2020-07-16T15:13:25+00:00",
    "time_updated": "2023-05-25T12:29:24.421159+00:00",
    "icontime": null,
    "profile": {
      "werkzaambij": "Pleio"
    }
  }
}
```

**Post user avatar**

Request: `POST /profile_sync_api/users/<uuid:user_id>/avatar`
```
Form-Data: FILES["avatar"]={avatar_file}
```

Response:
```
{
  "status": 200,
  "user": {
    "guid": "2f911792-0901-41d1-84b2-c6cfea79d191",
    "external_id": null,
    "name": "Test User",
    "email": "test@pleio.nl",
    "is_member": true,
    "is_banned": false,
    "time_created": "2020-07-16T15:13:25+00:00",
    "time_updated": "2023-05-25T12:29:24.421159+00:00",
    "icontime": null,
    "profile": {
      "werkzaambij": "Pleio"
    }
  }
}
```


**Add log**

Request: `POST /profile_sync_api/logs`
```
Form-Data (json):
    {
    "uuid": "dbdb97eb-3902-4e8d-b56b-0182718c6aa1",
    "content": "test"
    }
```

Response:
```
{
  "status": 200,
  "log": {
    "uuid": "dbdb97eb-3902-4e8d-b56b-0182718c6aa1"
  }
}
```
