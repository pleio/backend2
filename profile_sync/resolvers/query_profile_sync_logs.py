from ariadne import ObjectType

from core.resolvers import shared
from profile_sync.models import Logs

query = ObjectType("Query")


def resolve_profile_sync_logs(_, info, offset=0, limit=10):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    qs = Logs.objects.all()
    total = qs.count()
    qs = qs[offset : offset + limit]

    logs = []
    for log in qs:
        logs.append(
            {"uuid": log.uuid, "content": log.content, "timeCreated": log.created_at}
        )

    return {"total": total, "edges": logs}


query.set_field("profileSyncLogs", resolve_profile_sync_logs)
