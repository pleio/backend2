# Git hooks

Git hooks are scripts that git executes when specific Git operations take place.
To give an example, when you do a "git push", git calls the "pre-push" hooks
before it does the actual push.

The nice thing is that you, the user of git, can provide these scripts. To give
an example, if you provide a pre-push hook that calls flake8 on the changed
files, git will abort the push when flake8 finds issues. Have you ever waited on
a CI build to complete only to find out it has aborted due to a flake8 warning?
That's a scenario of the past when you have such a pre-push hook in place.

**Note:** flake8 has since been replaced with
[Ruff](https://docs.astral.sh/ruff/) in this project's configuration.

This repo uses Python package [pre-commit](https://pre-commit.com/) to configure
the hooks using the YAML file ``.pre-commit-config.yaml`` in the root of this
repo. pre-commit not only makes it easy to install and deinstall hooks, it also
provides a library of predefined hooks that you can use.

Currently the pre-commit YAML file configures the following hooks:

ruff
: reformat code according to PEP8 rules, and check for code issues

All these hooks are pre-push hooks: they are executed just before the actual
push takes place. If one of the hooks encounters improperly formatted code, the
push will abort.

## (De-)installation of hooks

You have to install the hooks before they become active. To install all pre-push
hooks you execute::

    (venv) $ pre-commit install --hook-type pre-push

To uninstall the pre-push hooks you execute::

    (venv) $ pre-commit uninstall --hook-type pre-push

The ``pre-commit`` command is a Python developer dependency so your
development virtualenv has to be active when you execute it. To execute the git
hooks themselves, the development virtualenv does not have to be active.

## Temporarily disable hooks

There are times you just want to quickly push your changes to remote, for
example to have a backup of your local commits. In those cases you don't want
Git to tell that it won't push your code. To let Git bypass any pre-push hook,
pass option ``--no-verify`` to ``git push``. Pass this option to ``git commit``
to let Git will bypass any commit hook.
