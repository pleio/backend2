# Generated by Django 4.2.10 on 2024-04-09 10:22

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tenants", "0011_client_custom_file_storage"),
    ]

    operations = [
        migrations.AlterField(
            model_name="client",
            name="administrative_details",
            field=models.JSONField(default=dict, editable=False),
        ),
        migrations.AlterField(
            model_name="client",
            name="custom_file_storage",
            field=models.CharField(
                blank=True, editable=False, max_length=100, null=True
            ),
        ),
    ]
