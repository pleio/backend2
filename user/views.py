from django.contrib import auth
from django.http import StreamingHttpResponse

from core.http import BadRequestReact, NotAllowedReact, UnauthorizedReact
from core.views.shared import Echo
from user.exception import ExportError
from user.exporting import (
    BanDateSerializer,
    BanReasonSerializer,
    CreateDateSerializer,
    EmailSerializer,
    ExportUsers,
    GuidSerializer,
    LastOnlineDateSerializer,
    NameSerializer,
)
from user.models import User

from .admin_permissions import HasUserManagementPermission


def export(request):
    # Method not tested...
    user = request.user

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    if not HasUserManagementPermission(user).has_permission():
        msg = "Not admin"
        raise NotAllowedReact(msg)

    user_fields = request.GET.getlist("user_fields[]")
    profile_field_guids = request.GET.getlist("profile_field_guids[]")

    if not user_fields and not profile_field_guids:
        msg = "No fields passed"
        raise BadRequestReact(msg)

    export_users = ExportUsers(
        User.objects.get_filtered_users(include_superadmin=False).order_by(
            "created_at"
        ),
        user_fields=user_fields,
        profile_field_guids=profile_field_guids,
    )

    try:
        response = StreamingHttpResponse(
            streaming_content=export_users.stream(buffer=Echo()),
            content_type="text/csv",
        )
        response["Content-Disposition"] = "attachment;filename=exported_users.csv"
        return response
    except ExportError as e:
        raise BadRequestReact(str(e))


def export_banned_users(request):
    user = auth.get_user(request)

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    if not HasUserManagementPermission(user).has_permission():
        msg = "Not admin"
        raise NotAllowedReact(msg)

    export_users = ExportUsers(
        queryset=User.objects.get_filtered_users(
            include_superadmin=False,
            is_banned=True,
            acting_user=user,
        ).order_by("created_at"),
        user_fields=[
            GuidSerializer.field,
            NameSerializer.field,
            EmailSerializer.field,
            CreateDateSerializer.field,
            LastOnlineDateSerializer.field,
            BanReasonSerializer.field,
            BanDateSerializer.field,
        ],
    )

    try:
        response = StreamingHttpResponse(
            streaming_content=export_users.stream(buffer=Echo()),
            content_type="text/csv",
        )
        response["Content-Disposition"] = "attachment;filename=banned_users.csv"
        return response
    except ExportError as e:
        raise BadRequestReact(str(e))
