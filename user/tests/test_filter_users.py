from django.contrib.auth.models import AnonymousUser

from core.lib import access_id_to_acl
from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    QuestionManagerFactory,
    UserFactory,
    UserManagerFactory,
)
from user.models import User


class TestFilterUsers(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.field = ProfileField.objects.create(
            key="profile_field",
            name="Nickname",
            field_type="text_field",
        )
        self.public_field = ProfileField.objects.create(
            key="public_field",
            name="Always readable",
            field_type="text_field",
        )
        self.override_config(
            PROFILE_SECTIONS=[{"profileFieldGuids": [self.field.guid]}]
        )

        # Phrase to look for, and should always find one at.
        self.QUE = "Nicky"
        self.NO_QUE = "Bobby"

        self.user1 = self.build_user(UserFactory, "User1", 2)
        self.user2 = self.build_user(UserFactory, "User2", 1)
        self.user3 = self.build_user(UserFactory, "User3", 0)
        self.user4 = self.build_user(UserFactory, "User4", 0)

        self.user = UserFactory(name=self.QUE, email="user@example.com")
        self.admin = AdminFactory(name=self.NO_QUE, email="admin@example.com")

    def build_user(self, factory, name, access_id):
        user = factory(name=name, email=f"{name}@example.com")

        user.profile.user_profile_fields.create(
            profile_field=self.field,
            value=self.QUE,
            read_access=access_id_to_acl(user, access_id),
        )
        user.profile.user_profile_fields.create(
            profile_field=self.public_field,
            value=self.NO_QUE,
            read_access=access_id_to_acl(user, 2),
        )
        return user

    def query_users(self, user=None):
        if not user:
            user = AnonymousUser()
        return User.objects.get_filtered_users(q=self.QUE, acting_user=user)

    def test_query_as_anonymous_user(self):
        """
        Test that the query by anonymous users returns records that have public matches
        """
        # Note that it does not mean that anonymous users can actually access
        # this information. It only means that the query works as designed.
        # Proper measures to hide sensitive information are taken in the API.
        users = self.query_users()
        self.assertEqual(len(users), 2)
        self.assertEqual({*users}, {self.user, self.user1})

    def test_query_as_logged_in_user(self):
        """
        Test that the query by logged in users returns records that have logged_in
        acl matches.
        """
        users = self.query_users(self.user)
        self.assertEqual(len(users), 3)
        self.assertEqual({*users}, {self.user, self.user1, self.user2})

    def test_query_as_authorized_user(self):
        """
        Test that owners result include their own protected records.
        """
        users = self.query_users(self.user3)
        self.assertEqual(len(users), 4)
        self.assertEqual({*users}, {self.user, self.user1, self.user2, self.user3})

    def test_query_as_admin_user(self):
        """
        Test admins can see protected records.
        """
        users = self.query_users(self.admin)
        self.assertEqual(len(users), 5)
        self.assertEqual(
            {*users}, {self.user, self.user1, self.user2, self.user3, self.user4}
        )


class TestFilterUsersByEmailAddress(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.QUE = "match@example.com"

        self.mail_as_mail = UserFactory(email=self.QUE, name="Mail as mail")
        self.mail_as_name = UserFactory(name=self.QUE)

    def test_authenticated_users(self):
        for user, description in [
            (self.mail_as_mail, "User with email as email"),
            (self.mail_as_name, "User with email as name"),
            (EditorFactory(), "Editor"),
            (QuestionManagerFactory(), "Question manager"),
            (NewsEditorFactory(), "News editor"),
        ]:
            with self.subTest(description):
                users = User.objects.get_filtered_users(q=self.QUE, acting_user=user)
                self.assertEqual(len(users), 1)
                self.assertEqual([*users], [self.mail_as_name])

    def test_authorized_users(self):
        for user, description in [
            (UserManagerFactory(), "User manager"),
            (AdminFactory(), "Admin"),
        ]:
            with self.subTest(description):
                users = User.objects.get_filtered_users(q=self.QUE, acting_user=user)
                self.assertEqual(len(users), 2)
                self.assertEqual({*users}, {self.mail_as_mail, self.mail_as_name})
