from datetime import datetime
from unittest.mock import MagicMock, patch

from django.contrib.auth.models import AnonymousUser
from mixer.backend.django import mixer

from core.factories import GroupFactory
from core.lib import access_id_to_acl
from core.models import ProfileField, UserProfileField
from core.tests.helpers import ElasticsearchTestCase, PleioTenantTestCase
from user.factories import UserFactory
from user.models import User


class UserTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.birthday_field = ProfileField.objects.create(
            key="birthday", name="birthday", field_type="date_field"
        )
        self.user = AnonymousUser()
        self.user1 = UserFactory(name="Aa")
        self.user2 = UserFactory(name="Bb")

    def create_user_with_bday(self, date):
        user = UserFactory(email=date)

        mixer.blend(
            UserProfileField,
            user_profile=user.profile,
            profile_field=self.birthday_field,
            value=date,
            read_access=access_id_to_acl(user, 2),
        )

        return user

    def test_users_by_birthday_sorted(self):
        first = self.create_user_with_bday("2021-12-02")
        second = self.create_user_with_bday("2021-12-03")
        third = self.create_user_with_bday("2021-12-04")
        start_date = datetime.strptime("2021-12-01", "%Y-%m-%d")
        end_date = datetime.strptime("2021-12-05", "%Y-%m-%d")

        users = User.objects.get_upcoming_birthday_users(
            self.birthday_field.guid, self.user, start_date, end_date
        ).edges

        self.assertListEqual([first, second, third], users)

    def test_users_by_birthday_excluding_later_bdays(self):
        first = self.create_user_with_bday("2021-12-02")
        second = self.create_user_with_bday("2021-12-03")
        too_late = self.create_user_with_bday("2021-12-06")  # noqa: F841
        start_date = datetime.strptime("2021-12-01", "%Y-%m-%d")
        end_date = datetime.strptime("2021-12-05", "%Y-%m-%d")

        users = User.objects.get_upcoming_birthday_users(
            self.birthday_field.guid, self.user, start_date, end_date
        ).edges

        self.assertListEqual([first, second], users)

    def test_users_by_birthday_sorted_across_years(self):
        second = self.create_user_with_bday("1975-01-05")
        first = self.create_user_with_bday("2000-12-31")
        start_date = datetime.strptime("2021-12-01", "%Y-%m-%d")
        end_date = datetime.strptime("2022-01-31", "%Y-%m-%d")

        users = User.objects.get_upcoming_birthday_users(
            self.birthday_field.guid, self.user, start_date, end_date
        ).edges

        self.assertListEqual([first, second], users)

    def test_users_by_birthday_reality(self):
        (self.create_user_with_bday("1980-12-13"),)
        (self.create_user_with_bday("1982-12-24"),)
        (self.create_user_with_bday("1992-12-27"),)
        (self.create_user_with_bday("1971-12-28"),)

        expected_users = [
            self.create_user_with_bday("1980-12-04"),
            self.create_user_with_bday("1974-12-09"),
            self.create_user_with_bday("1900-12-11"),
            self.create_user_with_bday("1990-12-11"),
            self.create_user_with_bday("1991-12-11"),
        ]

        # extra's outside of limit
        (self.create_user_with_bday("1978-12-13"),)
        (self.create_user_with_bday("1993-12-30"),)
        (self.create_user_with_bday("1975-01-01"),)
        (self.create_user_with_bday("1955-01-05"),)

        start_date = datetime.strptime("2021-12-04", "%Y-%m-%d")
        end_date = datetime.strptime("2022-01-06", "%Y-%m-%d")

        users = User.objects.get_upcoming_birthday_users(
            self.birthday_field.guid, self.user, start_date, end_date, 0, 5
        ).edges

        self.assertListEqual(expected_users, users)


class TestUserInGroupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.member = UserFactory()
        self.group.join(self.member)

    def test_find_user_in_group(self):
        """
        Test that a user is found as member of a group.
        """
        users = User.objects.filter_group(self.group.id)
        self.assertIn(self.member, users)

    def test_find_user_halfway_into_membership(self):
        """
        Test that the user is also found when the user is halfway into the membership process at another group.
        """
        group2 = GroupFactory(owner=self.owner)
        group2.join(self.member, "pending")

        users = User.objects.filter_group(self.group.id)
        self.assertIn(self.member, users)


class TestApplyClaimsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()

    @patch("user.models.UserManager.get")
    @patch("user.utils.claims.apply_claims")
    def test_apply_claims(self, apply_claims, mocked_get):
        # Given.
        mocked_get.return_value = self.user
        claims = MagicMock()

        # When.
        User.objects.get_or_create_claims(claims)

        # Then.
        apply_claims.assert_called_once_with(self.user, claims)


class TestGetUsersByIdsInOrderTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = self.build_user()
        self.user2 = self.build_user()
        self.user3 = self.build_user()
        self.user4 = self.build_removed_user()

    @staticmethod
    def build_user(**kwargs):
        user = UserFactory(**kwargs)
        return user.guid

    @staticmethod
    def build_removed_user(**kwargs):
        user = UserFactory(**kwargs)
        try:
            return user.guid
        finally:
            user.delete()

    def test_query_users(self):
        users = User.objects.users_in_order(
            [self.user1, self.user3, self.user2, self.user4]
        )
        self.assertEqual(len(users), 3)
        self.assertEqual(users[0].guid, self.user1)
        self.assertEqual(users[1].guid, self.user3)
        self.assertEqual(users[2].guid, self.user2)

    def test_query_users_reordered(self):
        users = User.objects.users_in_order(
            [self.user2, self.user4, self.user1, self.user3]
        )
        self.assertEqual(len(users), 3)
        self.assertEqual(users[0].guid, self.user2)
        self.assertEqual(users[1].guid, self.user1)
        self.assertEqual(users[2].guid, self.user3)
