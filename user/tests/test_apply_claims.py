from unittest.mock import patch

from django.utils.translation import gettext

from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory
from user.utils.claims import ApplyClaims, apply_claims


class TestApplyClaimsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.claims = {
            "email": "user@example.com",
            "sub": "123",
            "picture": "http://example.com/picture.jpg",
        }
        self.user = UserFactory(
            picture="http://example.com/picture.jpg",
            is_government=False,
            has_2fa_enabled=False,
            is_superadmin=False,
            external_id="123",
            email="user@example.com",
        )
        self.user_data = ApplyClaims(self.user, self.claims)._serialize_user()

    @patch("user.utils.claims.ApplyClaims.claim_username")
    @patch("user.utils.claims.ApplyClaims.claim_ban_user_if_email_is_changed")
    @patch("user.utils.claims.ApplyClaims.claim_email_address")
    @patch("user.utils.claims.ApplyClaims.claim_user_picture")
    @patch("user.utils.claims.ApplyClaims.claim_is_government")
    @patch("user.utils.claims.ApplyClaims.claim_has_2fa_enabled")
    @patch("user.utils.claims.ApplyClaims.claim_external_id")
    @patch("user.utils.claims.ApplyClaims.claim_is_superadmin")
    @patch("user.utils.claims.ApplyClaims.undo_ban_if_superadmin")
    def test_apply_claims(self, *mocks):
        processor = ApplyClaims(self.user, self.claims)

        processor.apply_claims()

        for mock in mocks:
            self.assertEqual(mock.call_count, 1, "Unexpectedly did not call %s" % mock)

    def test_apply_username(self):
        processor = ApplyClaims(self.user, {**self.claims, "name": "Foo"})

        processor.claim_username()

        self.assertEqual(self.user.name, "Foo")
        self.assertNotEqual(processor._serialize_user(), self.user_data)

    def test_apply_username_when_edit_user_name_disabled(self):
        self.override_config(EDIT_USER_NAME_ENABLED=True)
        processor = ApplyClaims(self.user, {**self.claims, "name": "Foo"})

        processor.claim_username()

        self.assertEqual(processor._serialize_user(), self.user_data)

    def test_apply_ban_user_if_email_is_changed(self):
        self.override_config(ALLOW_REGISTRATION=True)
        processor = ApplyClaims(
            self.user, {**self.claims, "email": "user@other-domain.com"}
        )

        processor.claim_ban_user_if_email_is_changed()

        self.assertTrue(self.user.is_active)
        self.assertEqual(self.user.ban_reason, "")

    def test_apply_ban_user_if_email_is_changed_to_allowed_domain(self):
        self.override_config(ALLOW_REGISTRATION=False)
        self.override_config(DIRECT_REGISTRATION_DOMAINS=["example.com"])
        processor = ApplyClaims(self.user, self.claims)

        processor.claim_ban_user_if_email_is_changed()

        self.assertTrue(self.user.is_active)
        self.assertEqual(self.user.ban_reason, "")

    def test_apply_ban_user_if_email_is_changed_to_invalid_domain(self):
        self.override_config(ALLOW_REGISTRATION=False)
        self.override_config(DIRECT_REGISTRATION_DOMAINS=["example.com"])
        processor = ApplyClaims(
            self.user, {**self.claims, "email": "user@other-domain.com"}
        )

        processor.claim_ban_user_if_email_is_changed()

        self.assertFalse(self.user.is_active)
        self.assertEqual(self.user.ban_reason, gettext("Email changed"))

    def test_apply_email_address(self):
        processor = ApplyClaims(
            self.user, {**self.claims, "email": "changed@example.com"}
        )

        processor.claim_email_address()

        self.assertEqual(self.user.email, "changed@example.com")

    def test_apply_email_address_with_whitespace(self):
        processor = ApplyClaims(
            self.user, {**self.claims, "email": "\r\n   changed@example.com \t\n  "}
        )

        processor.claim_email_address()

        self.assertEqual(self.user.email, "changed@example.com")

    @patch("core.models.user.UserProfile.update_avatar")
    def test_apply_user_picture_not_changed(self, mocked_update_avatar):
        processor = ApplyClaims(self.user, {**self.claims})

        processor.claim_user_picture()

        self.assertEqual(self.user.picture, "http://example.com/picture.jpg")
        self.assertFalse(mocked_update_avatar.called)

    @patch("core.models.user.UserProfile.update_avatar")
    def test_apply_user_picture_changed(self, mocked_update_avatar):
        processor = ApplyClaims(
            self.user, {**self.claims, "picture": "http://example.com/changed.jpg"}
        )

        processor.claim_user_picture()

        self.assertEqual(self.user.picture, "http://example.com/changed.jpg")
        mocked_update_avatar.assert_called_once_with("http://example.com/changed.jpg")

    def test_apply_is_government(self):
        processor = ApplyClaims(self.user, {**self.claims, "is_government": True})

        processor.claim_is_government()

        self.assertTrue(self.user.is_government)

    def test_apply_has_2fa_enabled(self):
        processor = ApplyClaims(self.user, {**self.claims, "has_2fa_enabled": True})

        processor.claim_has_2fa_enabled()

        self.assertTrue(self.user.has_2fa_enabled)

    def test_apply_external_id(self):
        processor = ApplyClaims(self.user, {**self.claims, "sub": "456"})

        processor.claim_external_id()

        self.assertEqual(self.user.external_id, "456")

    def test_apply_is_superadmin(self):
        processor = ApplyClaims(self.user, {**self.claims, "is_admin": True})

        processor.claim_is_superadmin()

        self.assertTrue(self.user.is_superadmin)

    def test_undo_ban_if_superadmin(self):
        self.user.is_active = False
        self.user.ban_reason = gettext("Email changed")
        processor = ApplyClaims(self.user, {**self.claims, "is_admin": True})

        processor.claim_is_superadmin()
        processor.undo_ban_if_superadmin()

        self.assertTrue(self.user.is_active)
        self.assertEqual(self.user.ban_reason, "")


class TestApplyClaimsWrapperTestCase(PleioTenantTestCase):
    def test_creating_apply_claims(self):
        user = UserFactory()
        claims = {"email": user.email}

        processor = ApplyClaims(user, claims)

        self.assertEqual(processor.user, user)
        self.assertEqual(processor.claims, claims)

    def test_create_apply_claims_without_email(self):
        try:
            user = UserFactory()
            ApplyClaims(user, {"foo": "bar"})
            self.fail("Unexpectedly did not raise an AssertionError")
        except AssertionError as e:
            self.assertEqual(str(e), "Email not found in claims")

    @patch("user.utils.claims.ApplyClaims.apply_claims")
    @patch("user.utils.claims.ApplyClaims.save")
    def test_apply_claims(self, mocked_save, mocked_apply_claims):
        user = UserFactory()
        claims = {"email": user.email}

        apply_claims(user, claims)

        self.assertEqual(mocked_apply_claims.call_count, 1)
        self.assertEqual(mocked_save.call_count, 1)
