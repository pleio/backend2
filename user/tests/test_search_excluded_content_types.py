from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestSearchWithExcludedContentTypesTestCase(
    Template.TestSearchWithExcludedContentTypesTestCase
):
    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return UserFactory(name=title)
