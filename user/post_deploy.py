from django.utils.translation import activate, gettext
from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public
from user.models import User


@post_deploy_action
def translate_ban_reasons():
    if is_schema_public():
        return

    activate(config.LANGUAGE)

    ban_reasons = {
        "Banned by admin": gettext("Blocked by admin"),
        "bouncing email adres": gettext("Bouncing e-mail address"),
        "user deleted in account": gettext("User deleted externally"),
        "banned": gettext("Removed by Profile-Sync"),
        "Deleted": gettext("Deleted"),
    }

    for user in User.objects.with_deleted().filter(is_active=False):
        user.ban_reason = ban_reasons.get(user.ban_reason) or user.ban_reason
        user.save()


@post_deploy_action
def complete_onboarding_for_existing_users():
    if is_schema_public():
        return

    if config.ONBOARDING_ENABLED:
        User.objects.update(onboarding_complete=True)
