from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy


def get_search_filters():
    yield {
        "key": "user",
        "value": _("User"),
        "plural": pgettext_lazy("plural", "Users"),
    }
