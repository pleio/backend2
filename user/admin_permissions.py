class PermissionCheckBase:
    PERMISSION = None

    def __init__(self, user):
        self.user = user

    def has_permission(self):
        raise NotImplementedError()


class HasAdminAccessPermission(PermissionCheckBase):
    PERMISSION = "accessAdmin"

    def has_permission(self):
        return self.user.is_authenticated and (
            self.user.is_user_admin
            or self.user.is_editor
            or self.user.is_site_admin
            or self.user.is_request_manager
        )


class HasFullControlPermission(PermissionCheckBase):
    PERMISSION = "fullControl"

    def has_permission(self):
        return self.user.is_authenticated and self.user.is_site_admin


class HasUserManagementPermission(PermissionCheckBase):
    PERMISSION = "userManagement"

    def has_permission(self):
        return self.user.is_authenticated and (
            self.user.is_site_admin or self.user.is_user_admin
        )


class HasAccessRequestManagementPermission(PermissionCheckBase):
    PERMISSION = "accessRequestManagement"

    def has_permission(self):
        return self.user.is_authenticated and (
            self.user.is_site_admin
            or self.user.is_user_admin
            or self.user.is_request_manager
        )


class HasPublicationRequestManagementPermission(PermissionCheckBase):
    PERMISSION = "publicationRequestManagement"

    def has_permission(self):
        return self.user.is_authenticated and (
            self.user.is_site_admin or self.user.is_editor
        )


class HasContentManagementPermission(PermissionCheckBase):
    PERMISSION = "contentManagement"

    def has_permission(self):
        return self.user.is_authenticated and (
            self.user.is_site_admin or self.user.is_editor
        )
