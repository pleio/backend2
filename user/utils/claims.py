import logging

from django.utils.translation import gettext

from core import config

logger = logging.getLogger(__name__)


def apply_claims(user, claims):
    processor = ApplyClaims(user, claims)
    processor.apply_claims()
    processor.save()


class ApplyClaims:
    def __init__(self, user, claims):
        assert claims.get("email"), "Email not found in claims"
        self.user = user
        self.profile = user.profile
        self.claims = claims

        self._original_user = self._serialize_user()
        self._original_profile = self._serialize_profile()

    def _serialize_user(self):
        user = self.user
        return {
            "external_id": user.external_id,
            "name": user.name,
            "email": user.email,
            "picture": user.picture,
            "is_government": user.is_government,
            "has_2fa_enabled": user.has_2fa_enabled,
            "is_superadmin": user.is_superadmin,
            "is_active": user.is_active,
            "ban_reason": user.ban_reason,
        }

    def _serialize_profile(self):
        profile = self.user.profile
        return {
            "avatar": profile.avatar_file.guid if profile.avatar_file else None,
        }

    def apply_claims(self):
        self.claim_username()
        self.claim_ban_user_if_email_is_changed()
        self.claim_email_address()
        self.claim_user_picture()
        self.claim_is_government()
        self.claim_has_2fa_enabled()
        self.claim_external_id()
        self.claim_is_superadmin()
        self.undo_ban_if_superadmin()

    def save(self):
        if self._serialize_profile() != self._original_profile:
            self.profile.save()
        if self._serialize_user() != self._original_user:
            self.user.save()

    def claim_username(self):
        if not config.EDIT_USER_NAME_ENABLED:
            self.user.name = self.claims.get("name")

    def claim_ban_user_if_email_is_changed(self):
        if (
            not config.ALLOW_REGISTRATION
            and _email_domain(self.user.email)
            != _email_domain(self.claims.get("email"))
            and _email_domain(self.claims.get("email"))
            not in config.DIRECT_REGISTRATION_DOMAINS
        ):
            self.user.is_active = False
            self.user.ban_reason = gettext("Email changed")

    def claim_email_address(self):
        self.user.email = self.claims.get("email").strip()

    def claim_user_picture(self):
        original_value = self.user.picture
        self.user.picture = self.claims.get("picture")

        if original_value != self.user.picture:
            self.profile.update_avatar(self.user.picture)

    def claim_is_government(self):
        self.user.is_government = bool(self.claims.get("is_government"))

    def claim_has_2fa_enabled(self):
        self.user.has_2fa_enabled = bool(self.claims.get("has_2fa_enabled"))

    def claim_external_id(self):
        self.user.external_id = self.claims.get("sub")

    def claim_is_superadmin(self):
        self.user.is_superadmin = bool(self.claims.get("is_admin"))

    def undo_ban_if_superadmin(self):
        if self.user.is_active:
            return

        if self.user.is_superadmin:
            self.user.is_active = True
            self.user.ban_reason = ""


def _email_domain(email):
    try:
        return email.split("@")[1]
    except IndexError:
        return ""
