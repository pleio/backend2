import functools

from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from core import config
from core.lib import validate_token
from core.models import Comment, Entity
from core.resolvers import shared
from core.utils.convert import html_to_tiptap
from entities.file.models import FileFolder
from user.models import User


def require_flow_token(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        if not config.FLOW_ENABLED or not validate_token(request, config.FLOW_TOKEN):
            return JsonResponse({"error": "Bad request"}, status=400)
        return func(request, *args, **kwargs)

    return wrapper


@csrf_exempt
@require_flow_token
@require_http_methods(["POST"])
def add_comment(request):
    description = request.POST.get("description", "")
    object_id = request.POST.get("container_guid", "")
    files = request.FILES

    if not description or not object_id:
        return JsonResponse({"error": "Bad request"}, status=400)

    try:
        entity = Entity.objects.get_subclass(id=object_id)
    except ObjectDoesNotExist:
        return JsonResponse(
            {"error": "Entity for flow comment does not exist"}, status=401
        )

    try:
        user = User.objects.get(id=config.FLOW_USER_GUID)
    except ObjectDoesNotExist:
        return JsonResponse(
            {"error": "User configured in flow does not exist"}, status=401
        )
    links = []
    comment = Comment.objects.create(
        container=entity, owner=user, rich_description=description
    )

    for file_name, file_obj in files.items():
        attachment = FileFolder.objects.create(upload=file_obj, owner=user)
        shared.scan_file(attachment, delete_if_virus=True)
        link = f"<a data-type='file' data-name='{attachment.slug}' data-size='{attachment.size}' data-mime-type='{attachment.mime_type}' href='{attachment.url}'>{file_name}</a>"
        links.append(link)

    comment.rich_description = html_to_tiptap(comment.rich_description + "".join(links))
    comment.save()

    return JsonResponse(
        {
            "error": None,
            "status": "Comment created from flow",
            "comment_id": str(comment.id),
        }
    )


@csrf_exempt
@require_flow_token
@require_http_methods(["POST"])
def edit_comment(request):
    description = request.POST.get("description", "")
    object_id = request.POST.get("container_guid", "")
    files = request.FILES

    if not description or not object_id:
        return JsonResponse({"error": "Bad request"}, status=400)

    try:
        comment = Comment.objects.get(id=object_id)
    except ObjectDoesNotExist:
        return JsonResponse({"error": "Flow comment does not exist"}, status=401)

    links = []
    for file_name, file_obj in files.items():
        attachment = FileFolder.objects.create(upload=file_obj, owner=comment.owner)
        shared.scan_file(attachment, delete_if_virus=True)
        link = f"<a data-type='file' data-name='{attachment.slug}' data-size='{attachment.size}' data-mime-type='{attachment.mime_type}' href='{attachment.url}'>{file_name}</a>"
        links.append(link)

    comment.rich_description = html_to_tiptap(description + "".join(links))
    comment.save()

    return JsonResponse(
        {"error": None, "status": "Comment edited from flow"}, status=200
    )
