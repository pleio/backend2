import logging
import re

import requests
from django.core.exceptions import ObjectDoesNotExist
from django.db import connection
from django.db.models.signals import post_save

from core import config
from core.models import Comment
from core.utils.convert import is_tiptap, tiptap_to_html
from entities.blog.models import Blog
from entities.discussion.models import Discussion
from entities.news.models import News
from entities.question.models import Question
from flow.models import FlowId
from tenants.models import Client

logger = logging.getLogger(__name__)


# use correct links for attachments
def replace_attachment_base_url(input_html):
    tenant = Client.objects.get(schema_name=connection.schema_name)
    base_url = "https://" + tenant.domains.first().domain
    pattern = re.compile(
        r'<a href="/attachment/entity/([a-z0-9\-]+)">([^<]+)</a>',
        re.IGNORECASE,
    )

    def replacement(match):
        entity_id = match.group(1)
        filename = match.group(2)

        return f"<a href='{base_url}/attachment/entity/{entity_id}'>{filename}</a> "

    result = re.sub(pattern, replacement, input_html)
    return result


def object_handler(sender, instance, created, **kwargs):
    if (
        not config.FLOW_ENABLED
        or not created
        or instance.type_to_string not in config.FLOW_SUBTYPES
    ):
        return

    try:
        headers = {
            "Authorization": "Token " + config.FLOW_TOKEN,
            "Accept": "application/json",
        }
        url = config.FLOW_APP_URL + "api/cases/"

        tenant = Client.objects.get(schema_name=connection.schema_name)
        url_prefix = "https://" + tenant.domains.first().domain

        title = (
            instance.title
            if hasattr(instance, "title") and instance.title
            else "Geen titel gegeven"
        )

        abstract = (
            tiptap_to_html(instance.abstract)
            if hasattr(instance, "abstract") and instance.abstract
            else ""
        )

        content = tiptap_to_html(instance.rich_description)

        description = f"{abstract}{content}<p><a href='{url_prefix}{instance.url}'>{instance.url}</a></p>"

        try:
            description = replace_attachment_base_url(description)
        except Exception as e:
            logger.error(e)

        json = {
            "casetype": str(config.FLOW_CASE_ID),
            "name": title,
            "description": description,
            "external_id": str(instance.id),
            "external_author": str(instance.owner.name),
            "external_email": str(instance.owner.email),
            "tags": [],
        }

        r = requests.post(url, headers=headers, json=json, timeout=30)
        FlowId.objects.create(flow_id=r.json()["id"], object_id=instance.id)

    except Exception as e:
        logger.error("Error saving object on connected flow app: %s", e)


def comment_handler(sender, instance, created, **kwargs):
    if not config.FLOW_ENABLED or not created:
        return

    root_container = instance.get_root_container()
    if root_container.type_to_string not in config.FLOW_SUBTYPES:
        return

    try:
        flow_id = str(FlowId.objects.get(object_id=root_container.id).flow_id)
    except ObjectDoesNotExist:
        return

    try:
        headers = {
            "Authorization": "Token " + config.FLOW_TOKEN,
            "Accept": "application/json",
        }
        url = config.FLOW_APP_URL + "api/externalcomments/"
        description = instance.rich_description

        try:
            description = replace_attachment_base_url(description)
        except Exception as e:
            logger.error(e)

        if is_tiptap(description):
            description = tiptap_to_html(description)

        json = {
            "case": flow_id,
            "author": instance.owner.name,
            "description": description,
        }

        requests.post(url, headers=headers, json=json, timeout=30)
    except Exception as e:
        logger.error("Error saving comment on connected flow app: %s", e)


post_save.connect(object_handler, sender=Blog)
post_save.connect(object_handler, sender=Discussion)
post_save.connect(object_handler, sender=News)
post_save.connect(object_handler, sender=Question)
post_save.connect(comment_handler, sender=Comment)
