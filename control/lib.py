import urllib.parse

from django.conf import settings
from django.shortcuts import reverse as django_reverse
from django_tenants.utils import get_tenant_model


def get_full_url(path):
    if settings.DEBUG:
        return urllib.parse.urljoin("http://localhost:8888", path)
    return urllib.parse.urljoin(f"https://{settings.CONTROL_PRIMARY_DOMAIN}", path)


def reverse(*args, **kwargs):
    return django_reverse(*args, urlconf="control.urls", **kwargs)


def all_site_schemas():
    client_model = get_tenant_model()
    clients = client_model.objects.exclude(schema_name="public")
    return clients.values_list("schema_name", flat=True)
