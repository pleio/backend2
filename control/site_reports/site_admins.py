from control.site_reports.base import RenderToCsv, SiteReportBase
from core.constances import USER_ROLES
from user.models import User


class SiteAdminsReport(RenderToCsv, SiteReportBase):
    def get_columns(self):
        return ["Name", "E-mail"]

    def get_rows(self):
        for user in User.objects.filter(
            is_active=True, is_superadmin=False, roles__contains=[USER_ROLES.ADMIN]
        ):
            yield [user.name, user.email]
