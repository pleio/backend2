import csv
from io import StringIO


class RenderToCsv:
    def render(self):
        buffer = StringIO()
        writer = csv.writer(buffer)
        writer.writerow(self.get_columns())
        for row in self.get_rows():
            writer.writerow(row)
        return buffer.getvalue()


class SiteReportBase:
    def get_columns(self):
        raise NotImplementedError()

    def get_rows(self):
        raise NotImplementedError()

    def render(self):
        raise NotImplementedError()
