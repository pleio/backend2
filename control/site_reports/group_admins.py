from control.site_reports.base import RenderToCsv, SiteReportBase
from core.models import Group, GroupMembership


class GroupAdminsReport(RenderToCsv, SiteReportBase):
    def get_columns(self):
        return ["Name", "E-mail", "Roles", "Groups"]

    def get_rows(self):
        register = AdminRegistration()
        register.fetch()

        for properties in register.user_repository.values():
            yield [
                properties["name"],
                properties["email"],
                ", ".join(properties["roles"]),
                ", ".join(properties["groups"]),
            ]


class AdminRegistration:
    def __init__(self):
        self.user_repository = {}

    def fetch(self):
        self._fetch_admins()
        self._fetch_owners()

    def _fetch_admins(self):
        admin_memberships = (
            GroupMembership.objects.filter(type="member")
            .filter(roles=["GROUP_ADMIN"])
            .order_by("user__email")
        )
        for membership in admin_memberships:
            self._log(membership.user, "admin", membership.group)

    def _fetch_owners(self):
        owned_groups = Group.objects.all()
        for group in owned_groups:
            self._log(group.owner, "owner", group)

    def _log(self, user, type, group):
        if not user.is_active:
            return

        if user.email not in self.user_repository:
            self.user_repository[user.email] = {
                "name": user.name,
                "email": user.email,
                "roles": set(),
                "groups": set(),
            }
        self.user_repository[user.email]["roles"].add(type)
        self.user_repository[user.email]["groups"].add("%s (%s)" % (group.name, type))
