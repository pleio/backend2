from urllib.parse import urlencode

from django import template

from control.lib import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context["request"].GET.dict()
    query.update(kwargs)
    return urlencode(query)


@register.simple_tag(takes_context=True)
def maybe_active_url(context, name, *args):
    expected_path = reverse(name, args=args)
    return "active" if context.request.path == expected_path else ""
