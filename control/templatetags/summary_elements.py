import logging
import os

from auditlog.models import LogEntry
from django import template
from django.db.models import Q
from django_tenants.utils import tenant_context
from post_deploy.models import PostDeployLog

from control.lib import reverse
from control.models import Task
from control.utils.post_deploy import serialize_log_record
from control.views.profile_forms.login_options import SettingsForm as LoginOptionsForm
from control.views.profile_forms.optional_features import (
    SettingsForm as OptionalFeaturesForm,
)
from control.views.profile_forms.rich_text_editor_features import (
    SettingsForm as RichTextEditorFeaturesForm,
)
from core import config
from core.lib import CspHeaderExceptionConfig, get_full_url
from core.models import CustomAgreement, FloodLog, MailLog, ProfileSet
from core.models.search import SearchIndexLog
from core.resolvers.shared import resolve_load_appointment_types
from entities.external_content.api_handlers.rss import get_full_rss_path
from entities.external_content.models import ExternalContentSource
from entities.file.models import FileFolder

logger = logging.getLogger(__name__)

register = template.Library()

ENABLED_STYLE = "bg-success text-light"
DISABLED_STYLE = "bg-light text-dark"


@register.simple_tag(takes_context=True)
def optional_features(context):
    with tenant_context(context["site"]):
        for key, label in OptionalFeaturesForm.OPTIONAL_FEATURES:
            yield bool(getattr(config, key)), label


@register.simple_tag(takes_context=True)
def event_add_email_attendee(context):
    with tenant_context(context["site"]):
        for key, label in OptionalFeaturesForm.EVENT_ADD_EMAIL_ATTENDEE_CHOICES:
            style = (
                ENABLED_STYLE
                if key == config.EVENT_ADD_EMAIL_ATTENDEE
                else DISABLED_STYLE
            )
            yield style, label


@register.simple_tag(name="config", takes_context=True)
def config_tag(context, key):
    with tenant_context(context["site"]):
        return getattr(config, key, []) or None


@register.simple_tag(takes_context=True)
def login_options(context):
    with tenant_context(context["site"]):
        for key, label in LoginOptionsForm.LOGIN_CHOICES:
            yield bool(key in config.OIDC_PROVIDERS), label


@register.simple_tag(takes_context=True)
def rich_text_editor_features(context):
    with tenant_context(context["site"]):
        for key, label in RichTextEditorFeaturesForm.RTE_FEATURE_CHOICES:
            style = (
                ENABLED_STYLE
                if key in config.RICH_TEXT_EDITOR_FEATURES
                else DISABLED_STYLE
            )
            yield style, label


@register.simple_tag(takes_context=True)
def load_appointment_type_config(context):
    with tenant_context(context["site"]):
        try:
            return resolve_load_appointment_types()
        except Exception:
            pass


@register.filter
def checkbox_if_true(value):
    return "✅" if value else "-"


@register.filter
def badge_style(value):
    return ENABLED_STYLE if value else DISABLED_STYLE


@register.simple_tag(takes_context=True)
def load_profile_sets(context):
    with tenant_context(context["site"]):
        return [
            {
                "name": p.name,
                "field": p.field.name,
                "edit_url": reverse(
                    "site_profile_profilefieldset",
                    args=[context["site_id"], "edit:{}".format(p.id)],
                ),
                "delete_url": reverse(
                    "site_profile_profilefieldset",
                    args=[context["site_id"], "delete:{}".format(p.id)],
                ),
            }
            for p in ProfileSet.objects.all()
        ]


@register.simple_tag(takes_context=True)
def load_custom_agreements(context):
    with tenant_context(context["site"]):
        return [
            {
                "name": a.name,
                "filename": os.path.basename(a.document.name),
                "url": get_full_url("/custom_agreement/{}".format(a.id)),
                "delete_url": reverse(
                    "site_profile_delete_custom_agreement",
                    args=[context["site_id"], a.id],
                ),
            }
            for a in CustomAgreement.objects.all()
        ]


@register.simple_tag(takes_context=True)
def load_external_content_sources(context):
    with tenant_context(context["site"]):

        def get_source_url(source):
            if source.handler_id in ["rss", "moodle"]:
                return reverse(
                    "site_profile_external_content_rss_source",
                    args=[context["site_id"], source.id],
                )
            return None

        return [
            {
                "name": source.name,
                "type": source.handler_id,
                "update_url": get_source_url(source),
            }
            for source in ExternalContentSource.objects.all()
        ]


@register.simple_tag(takes_context=True)
def load_external_content_source_rss_filters(context):
    def filter_as_dict(rss_filter):
        return {
            "filter": rss_filter.get("filter") or "",
            "full_path": get_full_rss_path(
                context["source"].settings.get("feed_url"), rss_filter.get("filter")
            ),
            "categories": rss_filter.get("categories") or "",
            "value": rss_filter.get("category_value") or "",
            "edit_url": reverse(
                "site_profile_external_content_rss_filter_edit",
                args=[context["site_id"], context["source"].id, rss_filter["id"]],
            ),
            "delete_url": reverse(
                "site_profile_external_content_rss_filter_delete",
                args=[context["site_id"], context["source"].id, rss_filter["id"]],
            ),
        }

    with tenant_context(context["site"]):
        return [
            filter_as_dict(rss_filter)
            for rss_filter in context["source"].settings.get("feed_filters", [])
        ]


@register.simple_tag(takes_context=True)
def add_external_content_source_rss_filter_url(context):
    return reverse(
        "site_profile_external_content_rss_filter_add",
        args=[context["site_id"], context["source"].id],
    )


@register.simple_tag(takes_context=True)
def load_external_content_source_tag_images(context):
    def load_image(image_guid):
        try:
            assert image_guid, "No image guid provided."
            return get_full_url("/file/thumbnail/%s" % image_guid)
        except Exception as e:
            logger.error(e)
            pass

    def tag_image_as_dict(tag_image):
        return {
            "src": (
                reverse(
                    "embed_media",
                    args=[context["site"].schema_name, tag_image.get("image_guid")],
                )
                if tag_image.get("image_guid")
                else ""
            ),
            "alt": tag_image.get("alt_text") or "",
            "tag": tag_image.get("tag") or "",
            "category": tag_image.get("category") or "",
            "edit_url": reverse(
                "site_profile_external_content_tag_image_edit",
                args=[context["site_id"], context["source"].id, tag_image["id"]],
            ),
            "delete_url": reverse(
                "site_profile_external_content_tag_image_delete",
                args=[context["site_id"], context["source"].id, tag_image["id"]],
            ),
        }

    with tenant_context(context["site"]):
        return [
            tag_image_as_dict(tag_image)
            for tag_image in context["source"].settings.get("tag_images", [])
        ]


@register.simple_tag(takes_context=True)
def add_external_content_source_tag_image_url(context):
    return reverse(
        "site_profile_external_content_tag_image_add",
        args=[context["site_id"], context["source"].id],
    )


@register.simple_tag(takes_context=True)
def external_content_all_content_link(context):
    return reverse(
        "site_profile_external_content_rss_content",
        args=[context["site_id"], context["source"].id],
    )


@register.simple_tag(takes_context=True)
def external_content_settings_link(context):
    return reverse(
        "site_profile_external_content_rss_source_settings",
        args=[context["site_id"], context["source"].id],
    )


@register.simple_tag(takes_context=True)
def load_csp_header_exceptions(context):
    with tenant_context(context["site"]):
        exception_config = CspHeaderExceptionConfig()
        return exception_config.exceptions


@register.simple_tag(takes_context=True)
def load_csp_src_types(context):
    with tenant_context(context["site"]):
        exception_config = CspHeaderExceptionConfig()
        return exception_config.SOURCES


@register.inclusion_tag("elements/summaries/audit_log_summary.html", takes_context=True)
def audit_log_summary(context):
    with tenant_context(context["site"]):
        return {
            "site": context["site"],
            "log_lines": [
                {
                    "when": line.timestamp,
                    "who": line.actor,
                    "changed": line.action,
                    "what": line.content_type,
                }
                for line in LogEntry.objects.filter(actor__isnull=False).order_by(
                    "-timestamp"
                )[:5]
            ],
        }


@register.inclusion_tag("elements/summaries/scan_log_summary.html", takes_context=True)
def scan_log_summary(context, view_mode=None):
    with tenant_context(context["site"]):
        return {
            "first_scanned_file": FileFolder.objects.order_by("last_scan").first(),
            "last_scanned_file": FileFolder.objects.order_by("-last_scan").first(),
            "total_files": FileFolder.objects.count(),
            "site": context["site"],
            "view_mode": view_mode,
        }


@register.inclusion_tag(
    filename="elements/summaries/deploy_task_summary.html",
    takes_context=True,
)
def deploy_task_summary(context):
    with tenant_context(context["site"]):
        qs = PostDeployLog.objects.order_by("-started_at")
        qs = qs.filter(Q(message__isnull=False) | Q(completed_at__isnull=True))
        return {
            "items": [serialize_log_record(record) for record in qs[:5]],
            "site": context["site"],
        }


@register.inclusion_tag(
    filename="elements/summaries/control_task_summary.html",
    takes_context=True,
)
def control_task_summary(context, task=None):
    qs = Task.objects.filter(client__schema_name=context["site"])
    if task:
        qs = qs.filter(name=task)
    qs = qs.order_by("-created_at")

    return {
        "site": context["site"],
        "items": qs[:5],
    }


@register.inclusion_tag(
    filename="elements/summaries/search_index_log_summary.html",
    takes_context=True,
)
def search_index_log_summary(context, view_mode):
    with tenant_context(context["site"]):
        return {
            "site": context["site"],
            "view_mode": view_mode,
            "log_lines": [
                {
                    "when": line.created_at,
                    "message": line.message,
                    "object_type": line.object_type,
                    "object_id": line.object_id,
                }
                for line in SearchIndexLog.objects.order_by("-created_at")[:5]
            ],
        }


@register.inclusion_tag("elements/summaries/mail_log_summary.html", takes_context=True)
def mail_log_summary(context):
    with tenant_context(context["site"]):
        return {
            "site": context["site"],
            "log_lines": [
                {
                    "created_at": line.created_at,
                    "subject": line.subject,
                    "recipient": line.receiver_email,
                }
                for line in MailLog.objects.order_by("-created_at")[:5]
            ],
        }


@register.inclusion_tag("elements/summaries/floodlog_summary.html", takes_context=True)
def floodlog_summary(context):
    with tenant_context(context["site"]):
        return {
            "site": context["site"],
            "log_lines": [
                {
                    "user": line.user_identifier,
                    "ip": line.ip,
                    "expires": line.expires,
                }
                for line in FloodLog.objects.order_by("user_identifier")[:5]
            ],
        }
