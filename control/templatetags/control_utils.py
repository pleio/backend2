import json
import logging
import os
from datetime import timedelta

from django import template
from django.conf import settings
from django.utils.translation import gettext
from django_tenants.utils import tenant_context

from control.lib import reverse
from control.models import Task
from core.lib import get_full_url, safe_file_path
from core.models import Group

register = template.Library()
logger = logging.getLogger(__name__)


@register.simple_tag
def backup_url(task: Task):
    if task.state == "SUCCESS" and task.response:
        backup_file = safe_file_path(settings.BACKUP_PATH, task.response)
        if os.path.exists(backup_file) and os.path.isfile(backup_file):
            return reverse("download_backup", args=(task.id,))


@register.simple_tag
def if_without_files(task: Task, if_true_message, else_message=""):
    if task.arguments[1]:
        return if_true_message
    return else_message


@register.inclusion_tag("elements/site_details_menu.html", takes_context=True)
def site_details_menu(context, site_id):
    context["menu_items"] = [
        (reverse("site_details", args=[site_id]), gettext("General")),
        (reverse("site_update", args=[site_id]), gettext("Update")),
        (reverse("site_profile", args=[site_id]), gettext("Profile")),
        (reverse("site_backup", args=[site_id]), gettext("Backups")),
        (reverse("site_snapshot", args=[site_id]), gettext("Snapshots")),
        (reverse("site_log_dashboard", args=[site_id]), gettext("Logs")),
        (reverse("site_elasticsearch", args=[site_id]), gettext("Elasticsearch")),
        (reverse("site_backgroundtasks", args=[site_id]), gettext("Background tasks")),
        (reverse("site_copy_group", args=[site_id]), gettext("Copy groups")),
        (
            reverse("site_custom_configuration", args=[site_id]),
            gettext("Custom configuration"),
        ),
    ]
    return context


@register.filter(name="bool2text")
def boolean_to_text(maybe_true, table="✅❌"):
    return table[0] if maybe_true else table[1]


@register.simple_tag(takes_context=True)
def group_names_for_copy_json(context):
    with tenant_context(context["site"]):
        return json.dumps(
            {str(group.id): "Copy: " + group.name for group in Group.objects.all()}
        )


@register.filter
def full_site_url(url, site):
    with tenant_context(site):
        return get_full_url(url, __auth=True)


@register.filter
def log_action(action):
    if action == 0:
        return "CREATE"
    elif action == 1:
        return "UPDATE"
    elif action == 2:
        return "DELETE"
    else:
        return "UNKNOWN"


@register.filter
def log_actor(actor):
    return actor if actor else "SYSTEM"


@register.inclusion_tag("elements/link_if_is_site.html")
def link_if_is_site(site, route, *args):
    return {
        "site": site,
        "url": reverse(route, args=args),
    }


@register.filter
def format_duration(duration):
    if duration is None:
        return ""

    if isinstance(duration, timedelta):
        total_seconds = int(duration.total_seconds())
    else:
        total_seconds = int(duration)

    hours, remainder = divmod(total_seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    if hours > 0:
        return f"{hours}h {minutes}m {seconds}s"
    elif minutes > 0:
        return f"{minutes}m {seconds}s"
    else:
        return f"{seconds}s"
