from django import template

from control.utils.render_configuration.decorators import ContentType

register = template.Library()


@register.simple_tag
def header_tag(level):
    return "h{}".format(level + 1)


@register.filter
def content_type(value):
    decorator = ContentType()
    return decorator.render(value)
