import logging
import math

from celery import shared_task
from django_tenants.utils import schema_context

from control.lib import all_site_schemas
from control.models import UserSyncController

logger = logging.getLogger(__name__)


@shared_task
def update_items():
    # Make sure all tasks are up to date.
    for controller in UserSyncController.objects.all():
        controller.update_item_status()


@shared_task
def sync_users():
    # Select the controller
    if not (controller := _get_active_controller()):
        return

    logger.info("running sync_users for controller %s", controller)

    total_scheduled = 0
    for schema in all_site_schemas():
        for user_id in _get_unscheduled_users(controller, schema):
            # Schedule sync for users that are not yet scheduled.
            _submit_profile_update_task(controller, schema, user_id)
            total_scheduled += 1

    if total_scheduled == 0:
        logger.info(
            "core.tasks.user_sync.sync_users did not schedule any users. The job is done."
        )
        controller.active = False
        controller.save()


def _submit_profile_update_task(controller, schema, user_id):
    from concierge.tasks import profile_updated_signal

    logger.info("schedule sync for user %s in schema %s", user_id, schema)
    task = profile_updated_signal.delay(schema, str(user_id))
    controller.items.create(
        schema=schema,
        user_id=user_id,
        task_id=task.id,
    )


def _get_unscheduled_users(controller, schema):
    from user.models import User

    """ Verzamel hier de ids van gebruikers die al gescheduled zijn om te syncen. """
    scheduled_users = _get_scheduled_users(controller, schema)

    with schema_context(schema):
        """Verzamel hier de ids van gebruikers die nog niet gescheduled zijn om te syncen."""
        subset_size = math.ceil(User.objects.count() / 7)

        users = User.objects.exclude(id__in=scheduled_users)
        users = users.order_by("-last_login")

        user_ids = [*users.values_list("id", flat=True)[:subset_size]]

        logger.info(
            "_get_unscheduled_users Found %s users to be scheduled", len(user_ids)
        )

        return user_ids


def _get_active_controller() -> UserSyncController:
    return UserSyncController.objects.filter(active=True).first()


def _get_scheduled_users(controller, schema):
    user_ids = [
        *controller.items.filter(schema=schema).values_list("user_id", flat=True)
    ]

    logger.info(
        "_get_scheduled_users Found %s users that are already scheduled", len(user_ids)
    )
    return user_ids
