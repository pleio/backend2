import os

from celery import chord, shared_task
from celery.utils.log import get_task_logger
from django.utils import timezone
from django_tenants.utils import schema_context

from core import config
from core.models import Comment, Entity, Group
from tenants.models import Client

logger = get_task_logger(__name__)


@shared_task(bind=True, ignore_result=True)
def replace_domain_links(self, schema_name, replace_domain=None):  # noqa: C901
    """
    Replace all links with old domain to new
    """
    start_date = timezone.now().timestamp()

    with schema_context(schema_name):
        tenant = Client.objects.get(schema_name=schema_name)
        tenant_domain = tenant.get_primary_domain().domain

        if not replace_domain:
            replace_domain = tenant_domain

        def _replace_links(text):
            if not isinstance(text, (str,)):
                return text

            # make absolute links relative
            text = text.replace(f"https://{replace_domain}/", "/")

            # replace link without path
            text = text.replace(f"https://{replace_domain}", f"https://{tenant_domain}")
            return text

        logger.info(
            "Start replace links on %s from %s to %s",
            tenant,
            replace_domain,
            tenant_domain,
        )

        # -- Replace MENU items
        menu_items = config.MENU
        for item in menu_items:
            if item.get("link"):
                item["link"] = _replace_links(item["link"])

            for child in item.get("children", []):
                if child.get("link"):
                    child["link"] = _replace_links(child["link"])

        config.MENU = menu_items

        # -- Replace DIRECT_LINKS
        direct_links = config.DIRECT_LINKS
        for item in direct_links:
            if item.get("link"):
                item["link"] = _replace_links(item["link"])

        config.DIRECT_LINKS = direct_links

        # -- Replace REDIRECTS items
        redirects = {}
        for k, v in config.REDIRECTS.items():
            redirects[k] = _replace_links(v)

        config.REDIRECTS = redirects

        # -- Replace group description
        groups = Group.objects.all()

        for group in groups:
            snapshot = group.serialize()
            group.map_rich_text_fields(_replace_links)
            if snapshot != group.serialize():
                group.save()

        # -- Replace entity descriptions
        entities = Entity.objects.all()

        entity_tasks, comment_tasks = [], []
        entity_task_arguments, comment_task_arguments = [], []
        for entity in entities.iterator(chunk_size=1000):
            entity_task_arguments.append(
                (tenant.schema_name, entity.id, replace_domain, tenant_domain)
            )

        entity_tasks = replace_links_for_entity.chunks(
            entity_task_arguments, 1000
        ).group()
        if os.environ.get("CELERY_CONTROL_QUEUE") == "True":
            [
                t.set(queue="control", acks_late=True, max_retries=4)
                for t in entity_tasks.tasks
            ]

        # -- Replace comment description
        comments = Comment.objects.all()

        for comment in comments.iterator(chunk_size=1000):
            comment_task_arguments.append(
                (tenant.schema_name, comment.id, replace_domain, tenant_domain)
            )

        comment_tasks = replace_links_for_comment.chunks(
            comment_task_arguments, 1000
        ).group()
        if os.environ.get("CELERY_CONTROL_QUEUE") == "True":
            [
                t.set(queue="control", acks_late=True, max_retries=4)
                for t in comment_tasks.tasks
            ]

    chord([entity_tasks, comment_tasks])(
        replace_domain_links_finish.s(schema_name=schema_name, timestamp=start_date)
    )


@shared_task(max_retries=4, acks_late=True, ignore_result=True)
def replace_links_for_entity(schema_name, entity_id, replace_domain, tenant_domain):
    def _replace_links(text):
        if not isinstance(text, (str,)):
            return text

        # make absolute links relative
        text = text.replace(f"https://{replace_domain}/", "/")

        # replace link without path
        text = text.replace(f"https://{replace_domain}", f"https://{tenant_domain}")
        return text

    with schema_context(schema_name):
        try:
            entity = Entity.objects.get_subclass(id=entity_id)
            snapshot = entity.serialize()
            entity.map_rich_text_fields(_replace_links)
            if snapshot != entity.serialize():
                entity.save()
        except Exception as e:
            logger.error(
                "Error while replacing links for entity %s: %s",
                entity_id,
                str(e),
            )


@shared_task(max_retries=4, acks_late=True, ignore_result=True)
def replace_links_for_comment(schema_name, comment_id, replace_domain, tenant_domain):
    def _replace_links(text):
        if not isinstance(text, (str,)):
            return text

        # make absolute links relative
        text = text.replace(f"https://{replace_domain}/", "/")

        # replace link without path
        text = text.replace(f"https://{replace_domain}", f"https://{tenant_domain}")
        return text

    with schema_context(schema_name):
        try:
            comment = Comment.objects.get(id=comment_id)
            snapshot = comment.serialize()
            comment.map_rich_text_fields(_replace_links)
            if snapshot != comment.serialize():
                comment.save()
        except Exception as e:
            logger.error(
                "Error while replacing links for comment %s: %s",
                comment_id,
                str(e),
            )


@shared_task(bind=True, ignore_result=True)
def replace_domain_links_finish(self, results, schema_name, timestamp):
    return_message = "Finish replace links on %s in %s seconds" % (
        schema_name,
        timezone.now().timestamp() - timestamp,
    )

    logger.info(return_message)

    from control.models import Task

    with schema_context("public"):
        # somehow chord of the main task does not return the result so we update it here
        Task.objects.filter(task_id=self.request.root_id).update(
            response=return_message
        )

    return return_message
