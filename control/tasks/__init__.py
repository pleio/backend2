from __future__ import absolute_import, unicode_literals

import gc
import json
import os
import shutil
import subprocess
import time
from datetime import timedelta
from traceback import format_exc

import pytz
import restic
from celery import chain, chord, shared_task
from celery.result import AsyncResult
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.files.storage import FileSystemStorage, default_storage, storages
from django.core.mail import send_mail
from django.core.management import call_command
from django.db import connection
from django.db.models import Q
from django.template.loader import render_to_string
from django.utils import timezone
from django_tenants.utils import schema_context

from control.lib import get_full_url, reverse
from control.models import AccessCategory, AccessLog, Snapshot, Task
from control.utils.group_copy import GroupCopyRunner
from core import config
from core.constances import USER_ROLES
from core.elasticsearch import elasticsearch_status_report
from core.exceptions import ExceptionDuringQueryIndex, UnableToTestIndex
from core.lib import safe_file_path, safe_open_file, test_elasticsearch_index
from core.tasks.elasticsearch_tasks import (
    all_indexes,
    elasticsearch_delete_data_for_tenant,
)
from core.utils.export import compress_path, decompress_path
from core.utils.object_storage import ObjectStorageClient
from tenants.models import Client, Domain
from user.models import User

from .links import replace_domain_links
from .user_sync import sync_users
from .user_sync import update_items as update_user_sync_items

logger = get_task_logger(__name__)


def _get_files_for_current_site(created_after=None):
    from core.models import CustomAgreement, ResizedImage
    from entities.file.models import FileFolder

    filter_updated_at = Q()
    filter_created_at = Q()
    if created_after:
        filter_updated_at = Q(updated_at__gt=created_after)
        filter_created_at = Q(created_at__gt=created_after)

    for item in FileFolder.objects.filter(filter_updated_at).iterator(chunk_size=100):
        if item.upload:
            yield item.upload
        if item.thumbnail:
            yield item.thumbnail

    for item in ResizedImage.objects.filter(filter_updated_at).iterator(chunk_size=100):
        if item.upload:
            yield item.upload

    for item in CustomAgreement.objects.filter(filter_created_at).iterator(
        chunk_size=100
    ):
        if item.document:
            yield item.document


@shared_task(bind=True, ignore_result=True)
def followup_task_complete(self, *args):
    """
    Poll task status
    """
    from control.models import Task

    with schema_context("public"):
        tasks = Task.objects.exclude(state__in=["SUCCESS", "FAILURE"])

        # sleep to give time for results to be updated
        time.sleep(3)

        for task in tasks:
            remote_task = AsyncResult(task.task_id)
            task.state = remote_task.state

            if remote_task.successful():
                if remote_task.result:
                    task.response = remote_task.result

            elif remote_task.failed():
                task.response = {"error": str(remote_task.result)}
            else:
                # timeout task after 1 day
                if task.created_at < (timezone.now() - timedelta(days=1)):
                    task.state = "FAILURE"
                    task.response = "TIMEOUT"

            if task.followup:
                task.run_followup()

            task.save()


def _maybe_json(content, default=None):
    try:
        return json.loads(content)
    except (TypeError, json.JSONDecodeError):
        return default


@shared_task(bind=True)
def add_site(self, schema_name, domain, administrative_details_json=None):
    """
    Create site from control
    """
    with schema_context("public"):
        tenant = Client(
            schema_name=schema_name,
            name=schema_name,
            administrative_details=_maybe_json(administrative_details_json, {}),
            custom_file_storage=settings.DEFAULT_STORAGE,
        )
        tenant.save()

        from tenants.tasks import update_post_deploy_tasks

        update_post_deploy_tasks.delay(schema_name)
        post_process_update_administrative_details.delay(schema_name)

        d = Domain()
        d.domain = domain
        d.tenant = tenant
        d.is_primary = True
        d.save()

        file_path = safe_file_path(settings.MEDIA_ROOT, tenant.schema_name)
        os.makedirs(file_path, exist_ok=True)

        return tenant.id


@shared_task(bind=True)
def delete_site(self, site_id):  # pragma: no cover
    """
    Delete site from control
    """
    with schema_context("public"):
        tenant = Client.objects.get(id=site_id)
        tenant.auto_drop_schema = True

        file_path = safe_file_path(settings.MEDIA_ROOT, tenant.schema_name)
        schema_name = tenant.schema_name
        tenant.delete()

        # remove elasticsearch data
        elasticsearch_delete_data_for_tenant(schema_name)

        # delete files
        if os.path.exists(file_path):
            shutil.rmtree(file_path)

    return True


@shared_task(bind=True)
def backup_site(
    self,
    backup_site_id,
    skip_files=False,
    backup_folder=None,
    compress=False,
    share_backup=False,
):
    """
    Backup site
    """
    with schema_context("public"):
        try:
            # does copy_site_id exist?
            backup_site = Client.objects.get(id=backup_site_id)

        except Exception as e:
            raise Exception(e)

    now = timezone.now()

    if not backup_folder:
        backup_folder = f"{now.strftime('%Y%m%d')}_{backup_site.schema_name}"

    backup_base_path = safe_file_path(settings.BACKUP_PATH, backup_folder)

    # remove folder if exists
    if os.path.exists(backup_base_path):
        shutil.rmtree(backup_base_path)

    backup_data_folder = os.path.join(backup_base_path, "data")
    os.makedirs(backup_data_folder)

    # Use pg_dump to dump schema to file, removing the schema name specifics
    dump_command = (
        f"pg_dump -n {backup_site.schema_name} --host={connection.settings_dict['HOST']} --dbname={connection.settings_dict['NAME']} "
        f"--username={connection.settings_dict['USER']} --no-password --schema-only --quote-all-identifiers --no-owner "
        f"| sed 's/\"{backup_site.schema_name}\"\\.//g' "
        f"| sed '/^CREATE SCHEMA /d' "
        f"| sed '/^SET /d' "
        f"| sed '/^SELECT pg_catalog.set_config/d' "
        f"> {backup_base_path}/schema.sql"
    )

    logger.info(dump_command)

    subprocess.run(
        dump_command,
        shell=True,
        env={"PGPASSWORD": connection.settings_dict["PASSWORD"]},
        check=True,
    )

    # get psycopg2 cursor
    cursor = connection.cursor()

    cursor.execute(
        "SELECT table_name FROM information_schema.tables "
        + "WHERE ( table_schema = %s ) "
        + "ORDER BY table_name;",
        (backup_site.schema_name,),
    )
    tables = cursor.fetchall()

    cursor.execute(f"SET search_path TO '{backup_site.schema_name}';")

    # dump data for all tables
    for row in tables:
        table = f"{row[0]}"
        filename = safe_file_path(backup_data_folder, "%s.csv" % table)
        with open(filename, mode="wb+") as f:
            cursor.copy_to(f, table)
        logger.info("Copy %s data to %s", table, filename)

    # copy files
    if not skip_files:
        backup_files_folder = os.path.join(backup_base_path, "files")
        backup_storage = FileSystemStorage()
        backup_storage.base_location = backup_files_folder

        with schema_context(backup_site.schema_name):
            for file in _get_files_for_current_site():
                try:
                    backup_storage.save(file.name, file.open())
                    print(f"Copy {file.name} to {backup_files_folder}")
                except Exception as e:
                    print(e)

    if not compress:
        return backup_folder

    archive_file_path = compress_path(backup_base_path)
    shutil.rmtree(backup_base_path, ignore_errors=True)

    archive_file = os.path.basename(archive_file_path)
    if share_backup and ObjectStorageClient.has_settings():
        object_storage = ObjectStorageClient(settings.BACKUP_BUCKET)
        object_storage.upload(settings.BACKUP_PATH, archive_file)

    return archive_file


@shared_task
def followup_backup_complete(backup_result, site_id, owner_guid):
    with schema_context("public"):
        user = User.objects.get(id=owner_guid)
        backup_url = reverse("site_backup", args=[site_id])

        site = Client.objects.get(id=site_id)
        download_url = reverse("download_backup", args=[site.id, backup_result])

        AccessLog.objects.create(
            category=AccessLog.custom_category(AccessCategory.SITE_BACKUP, site_id),
            user=user,
            item_id=backup_result,
            type=AccessLog.AccessTypes.CREATE,
            site=site,
        )

    with schema_context(site.schema_name):
        context = {
            "site_name": config.NAME,
            "backup_page": get_full_url(backup_url),
            "download": download_url.endswith(".zip"),
            "download_url": get_full_url(download_url),
        }

    content = render_to_string("mail/backup_success.txt", context)

    send_mail(
        "Website backup complete [Control2]",
        message=content,
        from_email="info@pleio.nl",
        recipient_list=[user.email],
        fail_silently=False,
    )

    logger.warning("SENT MAIL TO %s", user.email)

    return user.email


@shared_task
def restore_site(  # noqa: C901
    restore_folder, schema_name, domain, administrative_details=None
):
    """
    Restore backup from path to new tenant
    """

    # Equal treatment for backup in the zipfile and backup in the folder.
    if restore_folder.endswith(".zip"):
        restore_archive = str(restore_folder)
        restore_folder = restore_folder[:-4]
    else:
        restore_archive = restore_folder + ".zip"

    backup_base_path = safe_file_path(settings.BACKUP_PATH, restore_folder)
    backup_base_archive = safe_file_path(settings.BACKUP_PATH, restore_archive)

    if not os.path.exists(backup_base_path):
        # try a zip file
        if (
            not os.path.exists(backup_base_archive)
            and ObjectStorageClient.has_settings()
        ):
            # try a shared archive
            client = ObjectStorageClient(settings.BACKUP_BUCKET)
            assert client.exists(restore_archive), "Backup not found"

            client.download(settings.BACKUP_PATH, restore_archive)
        assert os.path.exists(backup_base_archive), "Backup not found"
        decompress_path(backup_base_archive)

    assert os.path.exists(backup_base_path), "Backup not found"

    with schema_context("public"):
        try:
            # is schema_name available ?
            if Client.objects.filter(schema_name=schema_name).first():
                msg = "Target schema already exists!"
                raise Exception(msg)

            # is domain available ?
            if Domain.objects.filter(domain=domain).first():
                msg = "Target domain already exists!"
                raise Exception(msg)

            # test if media folder exists
            if os.path.exists(safe_file_path(settings.MEDIA_ROOT, schema_name)):
                msg = f"Target file path for {schema_name} already exists, please clean up first."
                raise Exception(msg)

            # test if backup_folder exists
            if not os.path.exists(backup_base_path):
                msg = f"Backup folder {backup_base_path} does not exist"
                raise Exception(msg)

        except Exception as e:
            raise Exception(e)

    # get psycopg2 cursor
    cursor = connection.cursor()

    cursor.execute(f'CREATE SCHEMA "{schema_name}";')
    cursor.execute(f'SET search_path TO "{schema_name}";')

    # CREATE schema
    schema_file = open(f"{backup_base_path}/schema.sql", "r")
    sql_create_schema = schema_file.read()

    cursor.execute(sql_create_schema)

    # temporary remove foreign key constraints
    sql_drop_key_contraints = """create table if not exists dropped_foreign_keys (
    seq bigserial primary key,
    sql text
);

do $$ declare t record;
begin
for t in select conrelid::regclass::varchar table_name, conname constraint_name,
        pg_catalog.pg_get_constraintdef(r.oid, true) constraint_definition
        from pg_catalog.pg_constraint r
        where r.contype = 'f'
        -- current schema only:
        and r.connamespace = (select n.oid from pg_namespace n where n.nspname = current_schema())
    loop

    insert into dropped_foreign_keys (sql) values (
        format('alter table %s add constraint %s %s',
            quote_ident(t.table_name), quote_ident(t.constraint_name), t.constraint_definition));

    execute format('alter table %s drop constraint %s', quote_ident(t.table_name), quote_ident(t.constraint_name));

end loop;
end $$;"""

    cursor.execute(sql_drop_key_contraints)

    restore_data_path = os.path.join(backup_base_path, "data")

    # read data from dumped tables
    for file in os.listdir(restore_data_path):
        ext = file.split(".")
        table = f"{ext[0]}"
        file_path = safe_file_path(restore_data_path, file)
        f = open(file_path, "r")

        # check if table exists
        cursor.copy_from(f, table)

        logger.info("read %s to %s", file_path, table)

    # restore foreign key constraints
    sql_restore_key_constraints = """do $$ declare t record;
begin
-- order by seq for easier troubleshooting when data does not satisfy FKs
for t in select * from dropped_foreign_keys order by seq loop
execute t.sql;
delete from dropped_foreign_keys where seq = t.seq;
end loop;
end $$;"""
    cursor.execute(sql_restore_key_constraints)

    # reset sql sequences (needed after reading data with copy_from)
    sql_reset_sql_sequences = """do $$ declare rec record;
begin
for rec in SELECT 'SELECT SETVAL(' ||quote_literal(S.relname)|| ', MAX(' ||quote_ident(C.attname)|| ') ) FROM ' ||quote_ident(T.relname)|| ';' as sql
    FROM pg_class AS S, pg_depend AS D, pg_class AS T, pg_attribute AS C, pg_tables AS PGT
    WHERE S.relkind = 'S'
        AND S.oid = D.objid
        AND D.refobjid = T.oid
        AND D.refobjid = C.attrelid
        AND D.refobjsubid = C.attnum
        AND T.relname = PGT.tablename
        AND PGT.schemaname = current_schema()
    ORDER BY S.relname loop
        execute rec.sql;
    end loop;
end $$;"""

    cursor.execute(sql_reset_sql_sequences)

    # create new tenant
    with schema_context("public"):
        tenant = Client(schema_name=schema_name, name=schema_name)
        tenant.is_active = False
        tenant.custom_file_storage = settings.DEFAULT_STORAGE
        tenant.save()

        d = Domain()
        d.domain = domain
        d.tenant = tenant
        d.is_primary = True
        d.save()

    # copy files (after tenant is created so we can use default_storage)
    backup_files_folder = os.path.join(backup_base_path, "files")
    if os.path.exists(backup_files_folder):
        # walk through files and copy to default_storage
        with schema_context(schema_name):
            for root, _, files in os.walk(backup_files_folder):
                for file in files:
                    file_path = safe_file_path(root, file)
                    file_name = os.path.relpath(file_path, backup_files_folder)

                    with open(file_path, "rb") as f:
                        default_storage.save(file_name, f)

    # activate tenant when files are copied
    with schema_context("public"):
        tenant.is_active = True
        tenant.save()

    call_command("migrate_schemas", f"--schema={schema_name}")

    return tenant.id


@shared_task(bind=True)
def update_site(self, site_id, data):
    """
    Update site data
    """
    with schema_context("public"):
        try:
            tenant = Client.objects.get(id=site_id)

            if data.get("is_active", None) in [True, False]:
                tenant.is_active = data.get("is_active")

            tenant.save()

        except Exception as e:
            # raise general exception because remote doenst have Client exception
            raise Exception(e)

    return True


@shared_task(bind=False)
def copy_group(
    source_schema,
    action_user_id,
    group_id,
    target_schema=None,
    copy_members=False,
    target_group_name=None,
):
    """
    Copy group
    """
    runner = GroupCopyRunner()
    runner.run(
        copy_group.request.id,
        source_schema,
        action_user_id,
        group_id,
        target_schema,
        copy_members,
        target_group_name,
    )

    return runner.state.id


@shared_task(bind=False)
def copy_file_from_source_tenant(copy_id, source_file_id):
    """
    Separate load heavy task to copy file from source tenant to target in group copy
    """
    runner = GroupCopyRunner(copy_id)
    runner.copy_file_data(source_file_id)


@shared_task
def update_elasticsearch_status():
    for client in Client.objects.active_clients():
        update_elasticsearch_status_for_tenant.delay(client.id)


@shared_task
def update_elasticsearch_status_for_tenant(client_id):
    client = Client.objects.get(id=client_id)
    with schema_context(client.schema_name):
        try:
            index_status_result = {
                "result": elasticsearch_status_report(report_on_alert=True)
            }
        except Exception as e:
            index_status_result = {
                "exception": e.__class__,
                "message": str(e),
                "backtrace": format_exc(),
            }

    from control.models import ElasticsearchStatus

    ElasticsearchStatus.objects.cleanup(client=client)
    ElasticsearchStatus.objects.create(client=client, index_status=index_status_result)


@shared_task
def post_process_update_administrative_details(schema_name):
    with schema_context(schema_name):
        client = Client.objects.get(schema_name=schema_name)
        if client.administrative_details.get("processed", False):
            return

        config.REQUIRE_2FA = "admin"
        site_profile = client.administrative_details.get("site_profile", "")
        if site_profile == "closed":
            config.IS_CLOSED = True
            config.ALLOW_REGISTRATION = False
        elif site_profile == "open":
            config.IS_CLOSED = False
            config.ALLOW_REGISTRATION = True

        if site_admin := client.administrative_details.get("initial_site_admin_email"):
            try:
                user = User.objects.get(email__iexact=site_admin)
            except User.DoesNotExist:
                user = User.objects.create(
                    email=site_admin.strip(),
                    name=site_admin.strip(),
                    is_active=True,
                )
            if not user.has_role(USER_ROLES.ADMIN):
                user.roles.append(USER_ROLES.ADMIN)
                user.save()

        client.administrative_details["processed"] = True
        client.save()


def _external_content_update_item_matching_category_tag(item, category, tag, processor):
    for category_tags in item.category_tags:
        if category_tags["name"] == category:
            for assigned_tag in category_tags["values"]:
                if assigned_tag == tag:
                    processor(item)
                    return


@shared_task
def external_content_reset_featured_image(
    schema_name, source_id, category, tag, image_guid
):
    def _reset_featured_image(item):
        if str(item.featured_image_id) == image_guid:
            logger.error("Clearing the featured image")
            item.featured_image_id = None
            item.save()

    with schema_context(schema_name):
        from entities.external_content.models import ExternalContent

        for item in ExternalContent.objects.filter(source_id=source_id):
            _external_content_update_item_matching_category_tag(
                item, category, tag, _reset_featured_image
            )


@shared_task
def external_content_set_featured_image(
    schema_name, source_id, category, tag, new_image, previous_image_guid=None
):
    if not new_image or not new_image.get("image_guid"):
        return

    def _set_featured_image(item):
        is_previous = (
            previous_image_guid and str(item.featured_image_id) == previous_image_guid
        )
        is_current = str(item.featured_image_id) == new_image["image_guid"]
        if not item.featured_image_id or is_previous or is_current:
            item.featured_image_id = new_image["image_guid"]
            item.featured_position_y = new_image.get("y_position", 66)
            item.featured_alt = new_image.get("alt_text") or ""
            item.save()

    with schema_context(schema_name):
        from entities.external_content.models import ExternalContent

        for item in ExternalContent.objects.filter(source_id=source_id):
            _external_content_update_item_matching_category_tag(
                item, category, tag, _set_featured_image
            )


@shared_task
def external_content_update_all_featured_images(schema_name):
    with schema_context(schema_name):
        from entities.external_content.models import ExternalContentSource

        for source in ExternalContentSource.objects.filter(handler_id="rss"):
            for tag_image in source.settings.get("tag_images", []) or []:
                external_content_set_featured_image.delay(
                    schema_name=schema_name,
                    source_id=source.id,
                    category=tag_image["category"],
                    tag=tag_image["tag"],
                    new_image=tag_image,
                    previous_image_guid=tag_image["image_guid"],
                )


@shared_task
def migrate_storage(site_id):
    target_storage = "file_storage"

    with schema_context("public"):
        tenant = Client.objects.get(id=site_id)

    if tenant.custom_file_storage == "file_storage":
        msg = "Tenant is already on the target storage"
        raise ValueError(msg)

    tasks = []
    start_date = timezone.now().timestamp()
    task_arguments = []
    with schema_context(tenant.schema_name):
        for file in _get_files_for_current_site():
            task_arguments.append((tenant.schema_name, file.name, target_storage))

    tasks = migrate_storage_file.chunks(task_arguments, 10).group()
    if os.environ.get("CELERY_CONTROL_QUEUE") == "True":
        [t.set(queue="control", acks_late=True, max_retries=4) for t in tasks.tasks]

    chord(tasks)(
        migrate_storage_finish.s(
            site_id=site_id, timestamp=start_date, target_storage=target_storage
        )
    )


@shared_task(max_retries=4, acks_late=True)
def migrate_storage_file(schema_name, file_path, target_storage):
    with schema_context(schema_name):
        storage_class = storages[target_storage]
        try:
            with default_storage.open(file_path) as f:
                storage_class.save(file_path, f)
            return 0
        except (FileNotFoundError, IsADirectoryError):
            logger.error("File not found (%s) %s", schema_name, file_path)
            return 1
        except Exception as e:
            logger.error("Error migrating file (%s) %s: %s", schema_name, file_path, e)
            raise e


@shared_task(bind=True)
def migrate_storage_finish(
    self, results, site_id=None, timestamp=None, target_storage=None
):
    with schema_context("public"):
        tenant = Client.objects.get(id=site_id)

    totalnotfound = 0
    for result in results:
        totalnotfound += sum(result)

    start_date = timezone.datetime.fromtimestamp(timestamp, pytz.timezone("CET"))

    # deactivate tenant and sync files that changed or where added during migration
    with schema_context("public"):
        logger.info("Disable site and sync any last changes during migration")
        tenant.is_active = False
        tenant.save()

    with schema_context(tenant.schema_name):
        storage_class = storages[target_storage]

        for file in _get_files_for_current_site(start_date):
            try:
                with file.open() as f:
                    storage_class.save(file.name, f)
            except (FileNotFoundError, IsADirectoryError):
                logger.error("File not found (%s) %s", tenant.schema_name, file.name)
                totalnotfound += 1
            _ = gc.collect()

    with schema_context("public"):
        logger.info("Enable site with new storage")
        tenant.custom_file_storage = target_storage
        tenant.is_active = True
        tenant.save()

    elapsed_time_in_seconds = (timezone.now() - start_date).total_seconds()

    return_message = "Migration for %s successful [not found: %i - time: %fs]" % (
        tenant.schema_name,
        totalnotfound,
        elapsed_time_in_seconds,
    )

    logger.info(return_message)

    from control.models import Task

    with schema_context("public"):
        # somehow chord of the main task does not return the result so we update it here
        Task.objects.filter(task_id=self.request.root_id).update(
            response=return_message
        )

    return return_message


@shared_task(bind=True)
def snapshot(self, site_id):
    with schema_context("public"):
        try:
            # does site_id exist?
            snapshot_site = Client.objects.get(id=site_id)

            # Create a new Snapshot record
            snapshot_record = Snapshot.objects.create(
                client=snapshot_site,
                started_at=timezone.now(),
                status=Snapshot.Status.STARTED,
            )
        except Exception as e:
            raise Exception(e)

    start_time = timezone.now()

    backup_source = safe_file_path(settings.MEDIA_ROOT, snapshot_site.schema_name)

    if not os.path.exists(backup_source):
        with schema_context("public"):
            snapshot_record.set_error("Tenant folder does not exist")
        msg = "Tenant folder does not exist"
        raise Exception(msg)

    # Set database snapshot folder
    snapshot_database_folder = os.path.join(backup_source, "database")
    # Make sure the folder exists
    os.makedirs(snapshot_database_folder, exist_ok=True)

    # Use pg_dump to dump schema to file, removing the schema name specifics
    dump_command = (
        f"pg_dump -n {snapshot_site.schema_name} --host={connection.settings_dict['HOST']} --dbname={connection.settings_dict['NAME']} "
        f"--username={connection.settings_dict['USER']} --no-password --schema-only --quote-all-identifiers --no-owner "
        f"| sed 's/\"{snapshot_site.schema_name}\"\\.//g' "
        f"| sed '/^CREATE SCHEMA /d' "
        f"| sed '/^SET /d' "
        f"| sed '/^SELECT pg_catalog.set_config/d' "
        f"> {snapshot_database_folder}/schema.sql"
    )

    logger.info(snapshot_database_folder)

    subprocess.run(
        dump_command,
        shell=True,
        env={"PGPASSWORD": connection.settings_dict["PASSWORD"]},
        check=True,
    )

    # get psycopg2 cursor
    cursor = connection.cursor()

    cursor.execute(
        "SELECT table_name FROM information_schema.tables "
        + "WHERE ( table_schema = %s ) "
        + "ORDER BY table_name;",
        (snapshot_site.schema_name,),
    )
    tables = cursor.fetchall()

    cursor.execute(f"SET search_path TO '{snapshot_site.schema_name}';")

    # dump data to csv for all tables
    for row in tables:
        table = f"{row[0]}"
        file_name = safe_file_path(snapshot_database_folder, "%s.csv" % table)
        with open(file_name, "wb+") as f:
            cursor.copy_to(f, table)

    # delete csv files that are not in the database anymore (exclude schema.sql)
    for file in os.listdir(snapshot_database_folder):
        table_name = file.split(".")[0]
        if table_name not in [row[0] for row in tables]:
            if file != "schema.sql":
                os.remove(safe_file_path(snapshot_database_folder, file))

    dump_time_in_seconds = (timezone.now() - start_time).total_seconds()
    logger.info("Database dump time: %fs", dump_time_in_seconds)

    start_backup_time = timezone.now()

    try:
        results = restic.backup(paths=[backup_source], host=settings.ENV)
    except Exception as e:
        with schema_context("public"):
            snapshot_record.set_error(str(e))
        raise Exception(e)

    logger.info("Backup results: %s", results)

    backup_time_in_seconds = (timezone.now() - start_backup_time).total_seconds()
    logger.info("Backup time: %fs", backup_time_in_seconds)

    total_time_in_seconds = (timezone.now() - start_time).total_seconds()
    logger.info("Total time: %fs", total_time_in_seconds)

    with schema_context("public"):
        snapshot_record.set_finished(results)

    return results


@shared_task
def followup_snapshot_complete(task_id):
    with schema_context("public"):
        task = Task.objects.get(id=task_id)

        user = task.author if task.author else None
        snapshot_url = reverse("site_snapshot", args=[task.client.id])

        site = Client.objects.get(id=task.client.id)

    if user:
        with schema_context(site.schema_name):
            context = {
                "site_name": config.NAME,
                "snapshot_page": get_full_url(snapshot_url),
                "snapshot_result": task.response,
            }

        content = render_to_string("mail/snapshot_success.txt", context)

        send_mail(
            "Website snapshot complete [Control2]",
            message=content,
            from_email="info@pleio.nl",
            recipient_list=[user.email],
            fail_silently=False,
        )

        logger.info("SENT MAIL TO %s", user.email)


@shared_task(bind=True)
def restore_snapshot_as_backup(
    self,
    site_id,
    snapshot_id,
    skip_files=False,
    backup_folder=None,
    compress=False,
    share_backup=False,
):
    with schema_context("public"):
        try:
            # does copy_site_id exist?
            backup_site = Client.objects.get(id=site_id)

        except Exception as e:
            raise Exception(e)

    snapshot = restic.snapshots(snapshot_id)

    if not snapshot:
        msg = f"Snapshot {snapshot_id} not found"
        raise Exception(msg)

    now = timezone.now()

    if not backup_folder:
        backup_folder = f"{now.strftime('%Y%m%d')}_{backup_site.schema_name}"

    backup_base_path = safe_file_path(settings.BACKUP_PATH, backup_folder)

    # remove folder if exists
    if os.path.exists(backup_base_path):
        shutil.rmtree(backup_base_path)

    include_path = snapshot[0].get("paths")[0] + "/database" if skip_files else None

    # docker image has old restic so we can't use <snapshot_id>:/subfolder to restore
    restic.restore(snapshot_id, target_dir=backup_base_path, include=include_path)

    source_restore_folder = safe_file_path(
        backup_base_path, *snapshot[0].get("paths")[0].split("/")
    )
    target_backup_data_folder = os.path.join(backup_base_path, "data")
    target_backup_files_folder = os.path.join(backup_base_path, "files")

    # move everything to files folder
    shutil.move(source_restore_folder, target_backup_files_folder)
    # move database folder to data folder
    shutil.move(
        os.path.join(target_backup_files_folder, "database"), target_backup_data_folder
    )
    # move schema.sql to backup root
    shutil.move(
        os.path.join(target_backup_data_folder, "schema.sql"),
        os.path.join(backup_base_path, "schema.sql"),
    )

    if not compress:
        return backup_folder

    filename = compress_path(backup_base_path)
    shutil.rmtree(backup_base_path, ignore_errors=True)

    if share_backup and ObjectStorageClient.has_settings():
        client = ObjectStorageClient(settings.BACKUP_BUCKET)
        client.upload(settings.BACKUP_PATH, filename)

    return os.path.basename(filename)


@shared_task
def sync_snapshots_with_restic(site_id):
    if not settings.RESTIC_REPOSITORY_CONFIGURED:
        logger.info("Restic not configured, skipping sync")
        return

    with schema_context("public"):
        site = Client.objects.get(id=site_id)

    resource_path = safe_file_path(settings.MEDIA_ROOT, site.schema_name)
    snapshots = restic.snapshots(path=resource_path, host=settings.ENV)

    indexed_snapshots = {}

    for snapshot in snapshots:
        snapshot_id = snapshot.get("short_id")
        indexed_snapshots[snapshot_id] = snapshot

    with schema_context("public"):
        for snapshot in site.snapshots.all():
            if not snapshot.snapshot_id:
                continue
            if snapshot.snapshot_id[:8] not in indexed_snapshots:
                # cleanup old snapshots
                snapshot.delete()
            else:
                # pop from indexed_snapshots so we can see which ones are not indexed anymore
                indexed_snapshots.pop(snapshot.snapshot_id)

        # now add snapshots that are not indexed yet
        for _, snapshot in indexed_snapshots.items():
            print(snapshot)
            Snapshot.objects.create(
                client=site,
                result={"message": {"snapshot_id": snapshot.get("short_id")}},
                status=Snapshot.Status.FINISHED,
                started_at=snapshot.get("time"),
                finished_at=snapshot.get("time"),
            )


@shared_task
def schedule_daily_snapshot():
    if not settings.RESTIC_REPOSITORY_CONFIGURED:
        logger.info("Restic not configured, skipping daily snapshot")
        return

    try:
        import restic

        restic.forget(keep_within="31d", prune=True)
    except Exception as e:
        logger.error("Error during forget/prune: %s", e)

    for site in Client.objects.exclude(schema_name="public").filter(is_active=True):
        with schema_context(site.schema_name):
            if config.DAILY_SNAPSHOT_ENABLED:
                chain(
                    snapshot.si(site.id),
                    sync_snapshots_with_restic.si(site.id),
                ).apply_async()

                logger.info("Scheduled snapshot for %s started", site.schema_name)
