from django.views.generic import FormView

from control.lib import reverse
from control.views.mixins import SiteDetailsViewMixin


class ProfileFormBaseView(SiteDetailsViewMixin, FormView):
    def __init__(self, *args, **kwargs):
        self.saved_form = None
        super().__init__(*args, **kwargs)

    def form_valid(self, form):
        self.saved_form = form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("site_profile", kwargs={"site_id": self.site.id})

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "site": self.site,
        }
