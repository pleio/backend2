import logging

from django import forms
from django.contrib import messages
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.utils.translation import gettext_lazy as _
from django_tenants.utils import tenant_context

from control.views.profile_forms.mixin import ProfileFormBaseView
from core.models import CustomAgreement

logger = logging.getLogger(__name__)


class AddAgreementForm(forms.Form):
    name = forms.CharField(max_length=100, required=True, label=_("Name"))
    document = forms.FileField(required=True, label=_("Document"))

    def __init__(self, *args, site, request, **kwargs):
        self.site = site
        self.request = request
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            try:
                CustomAgreement.objects.create(
                    name=self.cleaned_data["name"],
                    document=self.cleaned_data["document"],
                )
            except Exception:
                messages.error(self.request, _("Error saving agreement"))
            pass

    def valid_name(self):
        name = self.cleaned_data["name"]
        with tenant_context(self.site):
            if CustomAgreement.objects.filter(name=name).exists():
                self.add_error("name", _("An agreement with this name already exists"))
                return False
        return True


class AddAgreementView(ProfileFormBaseView):
    template_name = "sites/profile/custom_agreements_add_form.html"
    form_class = AddAgreementForm

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "request": self.request,
        }


class DeleteAgreementForm(forms.Form):
    def __init__(self, *args, site, agreement, request, **kwargs):
        self.site = site
        self.agreement = agreement
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            self.agreement.delete()


class DeleteAgreementView(ProfileFormBaseView):
    template_name = "sites/profile/custom_agreements_delete_form.html"
    form_class = DeleteAgreementForm

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.agreement = None

    def dispatch(self, request, *args, **kwargs):
        self.get_site(kwargs["site_id"])
        if not self.site:
            return HttpResponseNotFound(_("Site not found"))

        with tenant_context(self.site):
            self.agreement = CustomAgreement.objects.filter(
                id=kwargs["agreement_id"]
            ).first()
        if not self.agreement:
            messages.error(self.request, _("Agreement not found"))
            return HttpResponseRedirect(self.get_success_url())
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "agreement": self.agreement,
        }

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "request": self.request,
            "agreement": self.agreement,
        }
