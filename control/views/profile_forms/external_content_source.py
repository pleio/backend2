import logging
import uuid

from django import forms
from django.core import paginator as django_paginator
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View
from django_tenants.utils import tenant_context

from control.lib import reverse
from control.tasks import (
    external_content_reset_featured_image,
    external_content_set_featured_image,
)
from control.views.mixins import SiteDetailsViewMixin
from control.views.profile_forms.mixin import ProfileFormBaseView
from core.utils.tags import TagCategoryStorage
from entities.external_content.models import ExternalContent, ExternalContentSource
from entities.external_content.utils import get_or_create_default_author
from entities.file.models import FileFolder

logger = logging.getLogger(__name__)


class AddSourceForm(forms.Form):
    type = forms.ChoiceField(
        label=_("Source type"),
        choices=[("rss", "Rss"), ("moodle", "Moodle")],
        widget=forms.RadioSelect(),
        required=True,
    )
    name = forms.CharField(max_length=256, required=True, label=_("Name"))
    plural_name = forms.CharField(max_length=256, required=True, label=_("Plural"))

    def __init__(self, *args, site, **kwargs):
        self.site = site
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            return ExternalContentSource.objects.create(
                name=self.cleaned_data["name"],
                plural_name=self.cleaned_data["plural_name"],
                handler_id=self.cleaned_data["type"],
                settings={},
            )


class AddExternalContentSourceView(ProfileFormBaseView):
    form_class = AddSourceForm
    template_name = "sites/profile/external_content_source_add_form.html"

    def get_success_url(self):
        new_rss_source = self.saved_form
        return reverse(
            "site_profile_external_content_rss_source",
            kwargs={
                "site_id": self.site.id,
                "source_id": new_rss_source.id,
            },
        )


class SourceForm(forms.Form):
    name = forms.CharField(max_length=256, required=True, label=_("Name"))
    plural_name = forms.CharField(max_length=256, required=True, label=_("Plural"))
    feed_url = forms.URLField(required=True, label=_("Feed URL"))
    feed_token = forms.CharField(max_length=256, required=False, label=_("Feed token"))
    author_category_name = forms.CharField(
        max_length=256, required=False, label=_("<dc:creator> category name")
    )
    subject_category_name = forms.CharField(
        max_length=256, required=False, label=_("<dc:subject> category name")
    )

    def __init__(self, *args, site, source_id, **kwargs):
        self.site = site
        self.source = None
        with tenant_context(site):
            self.source: ExternalContentSource = ExternalContentSource.objects.get(
                id=source_id
            )
            if not kwargs.get("initial"):
                kwargs["initial"] = {
                    "name": self.source.name,
                    "plural_name": self.source.plural_name,
                    "feed_url": self.source.settings.get("feed_url", ""),
                    "author_category_name": self.source.settings.get(
                        "author_category_name", ""
                    ),
                    "subject_category_name": self.source.settings.get(
                        "subject_category_name", ""
                    ),
                    "feed_token": self.source.settings.get("feed_token", ""),
                }
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            self.source.name = self.cleaned_data["name"]
            self.source.plural_name = self.cleaned_data["plural_name"]
            self.source.settings["feed_url"] = self.cleaned_data["feed_url"]
            self.source.settings["author_category_name"] = self.cleaned_data[
                "author_category_name"
            ]
            self.source.settings["subject_category_name"] = self.cleaned_data[
                "subject_category_name"
            ]
            self.source.settings["feed_token"] = self.cleaned_data["feed_token"]
            self.source.save()


def log_result(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        logger.error(result)
        return result

    return wrapper


class ExternalContentSourceViewMixin(SiteDetailsViewMixin):
    def __init__(self, *args, **kwargs):
        self.source_id = None
        super().__init__(*args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.source_id = kwargs["source_id"]
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            return {
                **super().get_context_data(**kwargs),
                "source": ExternalContentSource.objects.get(id=self.source_id),
            }


class ExternalContentSourceView(ExternalContentSourceViewMixin, ProfileFormBaseView):
    form_class = SourceForm
    template_name = "sites/profile/external_content_source_form.html"

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "source_id": self.source_id,
        }


class ExternalContentRssSourceDownloadSettingsView(ExternalContentSourceView, View):
    def get(self, request, *args, **kwargs):
        with tenant_context(self.site):
            return JsonResponse(
                ExternalContentSource.objects.get(id=self.source_id).settings
            )


@method_decorator(csrf_exempt, name="dispatch")
class ExternalContentRssSourceContentView(ExternalContentSourceViewMixin, TemplateView):
    template_name = "sites/profile/external_content_rss_source_content.html"
    report_template = "elements/external_content/similar_content_report.html"

    def post(self, request, *args, **kwargs):
        with tenant_context(self.site):
            return render(
                request,
                self.report_template,
                self.similarity_report_context(
                    ExternalContent.objects.get(id=request.POST.get("entity_id"))
                ),
            )

    def similarity_report_context(self, entity):
        handler = entity.source.get_handler()
        published = timezone.datetime.fromisoformat(entity.init_data["published_time"])
        similar_content = handler.lookup_similar_external_content(
            entity.title, published, exclude_id=entity.id
        )
        return {
            "similar_content": [
                (ExternalContent.objects.get(id=item["id"]), item)
                for item in similar_content
            ],
        }

    def get(self, request, *args, **kwargs):
        with tenant_context(self.site):
            return render(request, self.template_name, self.get_context_data())

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            context = super().get_context_data(**kwargs)
            content = context["source"].content.all()
            paginator = Paginator(content, 50)
            try:
                items = paginator.page(self.request.GET.get("page", 1))
            except django_paginator.PageNotAnInteger:
                items = paginator.page(1)
            except django_paginator.EmptyPage:
                items = paginator.page(paginator.num_pages)
            return {**context, "items": items}


class RssFilterFormMixin:
    def load_source(self):
        if not self.source:
            with tenant_context(self.site):
                self.source = ExternalContentSource.objects.get(id=self.source_id)
        return self.source

    def select_filter(self):
        self.load_source()
        if self.source:
            for feed_filter in self.source.settings.get("feed_filters", []):
                if feed_filter["id"] == self.filter_id:
                    return feed_filter
        return {}


class RssFilterForm(RssFilterFormMixin, forms.Form):
    filter = forms.CharField(max_length=256, required=False, label=_("Filter"))
    categories = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={"rows": 5}),
        label=_("Categories"),
    )

    def __init__(self, site, source_id, action, filter_id=None, *args, **kwargs):
        self.site = site
        self.source_id = source_id
        self.filter_id = filter_id
        self.action = action
        self.source = None
        self.is_add = action == "add"
        self.load_source()
        if not kwargs.get("initial"):
            if not self.is_add:
                rss_filter = self.select_filter()
                kwargs["initial"] = {
                    "filter": rss_filter.get("filter") or "",
                    "categories": self.initial_categories(rss_filter),
                }
        super().__init__(*args, **kwargs)

    @staticmethod
    def initial_categories(rss_filter):
        lines = []
        for category in rss_filter.get("categories", []):
            lines.append("%s: %s" % (category["name"], ", ".join(category["values"])))
        return "\n".join(lines)

    def clean_categories(self):
        try:
            formdata = self.cleaned_data["categories"]
            categories = []
            for line in formdata.split("\n"):
                if line:
                    name, values = line.split(":")
                    categories.append(
                        {
                            "name": name.strip(),
                            "values": [value.strip() for value in values.split(",")],
                        }
                    )
            return categories
        except Exception:
            raise forms.ValidationError(_("Invalid categories format"))

    def save(self):
        if self.is_add:
            return self.add_filter()
        return self.edit_filter()

    def add_filter(self):
        with tenant_context(self.site):
            self.source.settings["feed_filters"] = [
                *self.source.settings.get("feed_filters", []),
                {
                    "id": uuid.uuid4().hex,
                    "filter": self.cleaned_data["filter"],
                    "categories": self.cleaned_data["categories"],
                },
            ]
            self.source.save()

    def edit_filter(self):
        with tenant_context(self.site):
            new_filters = []
            for feed_filter in self.source.settings.get("feed_filters", []):
                if feed_filter["id"] == self.filter_id:
                    new_filters.append(
                        {
                            "id": self.filter_id,
                            "filter": self.cleaned_data["filter"],
                            "categories": self.cleaned_data["categories"],
                        }
                    )
                else:
                    new_filters.append(feed_filter)
            self.source.settings["feed_filters"] = new_filters
            self.source.save()


class DeleteFilterForm(RssFilterFormMixin, forms.Form):
    def __init__(self, site, source_id, filter_id, action, *args, **kwargs):
        self.site = site
        self.source_id = source_id
        self.action = action
        self.filter_id = filter_id
        with tenant_context(self.site):
            self.source = ExternalContentSource.objects.get(id=source_id)
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            new_filters = []
            for feed_filter in self.source.settings.get("feed_filters", []):
                if feed_filter["id"] != str(self.filter_id):
                    new_filters.append(feed_filter)
            self.source.settings["feed_filters"] = new_filters
            self.source.save()


class ExternalContentRssFilterGenericView(ProfileFormBaseView):
    template_name = "sites/profile/external_content_rss_filter_form.html"
    action = None

    def __init__(self, *args, **kwargs):
        self.source_id = None
        self.filter_id = None
        self.form = None
        super().__init__(*args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.source_id = kwargs.get("source_id")
        self.filter_id = kwargs.get("filter_id")
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse(
            "site_profile_external_content_rss_source",
            kwargs={
                "site_id": self.site.id,
                "source_id": self.source_id,
            },
        )

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "source_id": self.source_id,
            "action": self.action,
            "filter_id": self.filter_id,
        }

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            context = super().get_context_data(**kwargs)
            self.form = context["form"]
            return {
                **context,
                "is_add": self.action == "add",
                "is_delete": self.action == "delete",
                "source": ExternalContentSource.objects.get(id=self.source_id),
                "filter": self.get_filter(),
            }

    def get_filter(self):
        return self.form.select_filter()


class ExternalContentRssFilterAddView(ExternalContentRssFilterGenericView):
    action = "add"
    form_class = RssFilterForm

    def get_filter(self):
        return {}


class ExternalContentRssFilterEditView(ExternalContentRssFilterGenericView):
    action = "edit"
    form_class = RssFilterForm


class ExternalContentRssFilterDeleteView(ExternalContentRssFilterGenericView):
    action = "delete"
    form_class = DeleteFilterForm


class CommonTagImageFormMixin(forms.Form):
    def __init__(self, *args, site, source, tag_image, action, **kwargs):
        self.site = site
        self.source = source
        self.tag_image = tag_image
        self.action = action
        self.initial = {
            "category_tag": "%s|%s"
            % (tag_image.get("category", ""), tag_image.get("tag", "")),
            "category": self.tag_image.get("category", ""),
            "tag": self.tag_image.get("tag", ""),
            "alt_text": self.tag_image.get("alt_text", ""),
            "image_guid": self.tag_image.get("image_guid", ""),
            "y_position": self.tag_image.get("y_position", 66),
        }
        if not kwargs["initial"]:
            kwargs["initial"] = self.initial
        super().__init__(*args, **kwargs)

    def get_category_tags(self, current_value=""):
        result = []
        keys = []
        storage = TagCategoryStorage()
        with tenant_context(self.site):
            for category in storage.all_tag_categories():
                for tag in category["values"]:
                    result.append(
                        [
                            "%s|%s" % (category["name"], tag),
                            "%s: %s" % (category["name"], tag),
                        ]
                    )
                    keys.append("%s|%s" % (category["name"], tag))
        if current_value and current_value not in keys:
            return [(current_value, current_value), *result]
        return result


class TagImageAddForm(CommonTagImageFormMixin):
    category_tag = forms.CharField(
        max_length=256, required=True, label=_("Existing tags"), widget=forms.Select
    )
    category = forms.CharField(max_length=256, required=True, label=_("Category"))
    tag = forms.CharField(max_length=256, required=True, label=_("Tag"))
    alt_text = forms.CharField(max_length=256, required=False, label=_("Alt text"))
    image_guid = forms.CharField(
        max_length=256, required=False, label=_("Use the GUID of an existing image")
    )
    y_position = forms.IntegerField(
        required=True, label=_("Y position"), min_value=0, max_value=100
    )
    file_upload = forms.FileField(required=False, label=_("Upload a new image"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["category_tag"].widget.choices = self.get_category_tags()

    def save(self):
        with tenant_context(self.site):
            image_guid = self.cleaned_data["image_guid"]
            if self.cleaned_data.get("file_upload"):
                file = FileFolder.objects.create(
                    type=FileFolder.Types.FILE,
                    upload=self.cleaned_data["file_upload"],
                    read_access=self.source.get_read_access(),
                    owner=get_or_create_default_author(),
                )
                image_guid = file.guid
            new_image = {
                "id": uuid.uuid4().hex,
                "category": self.cleaned_data["category"],
                "tag": self.cleaned_data["tag"],
                "alt_text": self.cleaned_data["alt_text"],
                "y_position": self.cleaned_data["y_position"],
                "image_guid": image_guid,
            }
            self.source.settings["tag_images"] = [
                *self.source.settings.get("tag_images", []),
                new_image,
            ]
            external_content_set_featured_image.delay(
                schema_name=self.site.schema_name,
                source_id=str(self.source.id),
                category=self.cleaned_data.get("category"),
                tag=self.cleaned_data.get("tag"),
                new_image=new_image,
            )
            self.source.save()


class TagImageEditForm(CommonTagImageFormMixin):
    category_tag = forms.CharField(
        max_length=256, required=True, label=_("Existing tags"), widget=forms.Select
    )
    category = forms.CharField(max_length=256, required=True, label=_("Category"))
    tag = forms.CharField(max_length=256, required=True, label=_("Tag"))
    image_guid = forms.CharField(max_length=256, required=False, label=_("Image GUID"))
    alt_text = forms.CharField(max_length=256, required=False, label=_("Alt text"))
    y_position = forms.IntegerField(
        required=True, label=_("Y position"), min_value=0, max_value=100
    )
    replacement_file = forms.FileField(required=False, label=_("Replace image"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["category_tag"].widget.choices = self.get_category_tags(
            self.initial.get("category_tag")
        )

    def save(self):
        with tenant_context(self.site):
            new_images = []
            for tag_image in self.source.settings.get("tag_images", []):
                if tag_image["id"] == self.tag_image["id"]:
                    previous_image_guid = tag_image.get("image_guid")
                    logger.error("Previous image guid: %s", previous_image_guid)
                    if self.cleaned_data.get("replacement_file"):
                        file = FileFolder.objects.create(
                            type=FileFolder.Types.FILE,
                            upload=self.cleaned_data["replacement_file"],
                            read_access=self.source.get_read_access(),
                            owner=get_or_create_default_author(),
                        )
                        tag_image["image_guid"] = file.guid
                    else:
                        tag_image["image_guid"] = self.cleaned_data["image_guid"]
                    tag_image["category"] = self.cleaned_data["category"]
                    tag_image["tag"] = self.cleaned_data["tag"]
                    tag_image["alt_text"] = self.cleaned_data["alt_text"]
                    tag_image["y_position"] = self.cleaned_data["y_position"]
                    external_content_set_featured_image.delay(
                        schema_name=self.site.schema_name,
                        source_id=str(self.source.id),
                        category=tag_image.get("category"),
                        tag=tag_image.get("tag"),
                        new_image=tag_image,
                        previous_image_guid=previous_image_guid,
                    )
                new_images.append(tag_image)
            self.source.settings["tag_images"] = new_images
            self.source.save()


class TagImageDeleteForm(CommonTagImageFormMixin):
    def save(self):
        with tenant_context(self.site):
            new_images = []
            for tag_image in self.source.settings.get("tag_images", []):
                if tag_image["id"] != self.tag_image["id"]:
                    new_images.append(tag_image)
                else:
                    external_content_reset_featured_image.delay(
                        schema_name=self.site.schema_name,
                        source_id=str(self.source.id),
                        category=tag_image.get("category"),
                        tag=tag_image.get("tag"),
                        image_guid=tag_image.get("image_guid"),
                    )
            self.source.settings["tag_images"] = new_images
            self.source.save()


class CommonTagImageViewMixin(ProfileFormBaseView):
    action = None
    form_class = None

    def __init__(self, *args, **kwargs):
        self._source = None
        self._tag_image = None
        self.form = None
        super().__init__(*args, **kwargs)

    @property
    def source(self):
        if not self._source:
            with tenant_context(self.site):
                self._source = ExternalContentSource.objects.get(
                    id=self.kwargs.get("source_id")
                )
        return self._source

    @property
    def tag_image(self):
        if not self._tag_image:
            with tenant_context(self.site):
                for tag_image in self.source.settings.get("tag_images", []):
                    if tag_image["id"] == self.kwargs.get("tag_id"):
                        image = tag_image.get("image_guid")
                        self._tag_image = {
                            **tag_image,
                            "url": (
                                reverse(
                                    "embed_media", args=[self.site.schema_name, image]
                                )
                                if image
                                else ""
                            ),
                        }
                        break

        return self._tag_image or {}

    def get_success_url(self):
        return reverse(
            "site_profile_external_content_rss_source",
            kwargs={
                "site_id": self.site.id,
                "source_id": self.source.id,
            },
        )

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "source": self.source,
            "action": self.action,
            "tag_image": self.tag_image,
        }

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            context = super().get_context_data(**kwargs)
            self.form = context["form"]
            return {
                **context,
                "is_add": self.action == "add",
                "is_delete": self.action == "delete",
                "source": self.source,
                "tag_image": self.tag_image,
            }


class ExternalContentTagImageAddView(CommonTagImageViewMixin):
    template_name = "sites/profile/external_content_tag_image_add_form.html"
    action = "add"
    form_class = TagImageAddForm


class ExternalContentTagImageEditView(CommonTagImageViewMixin):
    template_name = "sites/profile/external_content_tag_image_form.html"
    action = "edit"
    form_class = TagImageEditForm


class ExternalContentTagImageDeleteView(CommonTagImageViewMixin):
    template_name = "sites/profile/external_content_tag_image_delete_form.html"
    action = "delete"
    form_class = TagImageDeleteForm
