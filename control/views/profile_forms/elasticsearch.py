import json
import logging

from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import gettext as _
from django.views.generic import TemplateView
from django_tenants.utils import tenant_context
from elasticsearch_dsl import Search
from elasticsearch_dsl.connections import get_connection
from elasticsearch_dsl.query import Q

from control.views.mixins import SiteDetailsViewMixin
from core.elasticsearch import elasticsearch_status_report
from core.tasks import elasticsearch_tasks
from core.utils.elasticsearch import list_documents

logger = logging.getLogger(__name__)


class ElasticsearchView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/profile/elasticsearch.html"

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            report = elasticsearch_status_report()

        return {
            **super().get_context_data(**kwargs),
            "indices": [(i["index"], i) for i in report],
        }

    def post(self, request, *args, **kwargs):
        if "rebuild_index" in request.POST:
            self._rebuild_index(request, request.POST.get("index_name"))
        elif "update_index" in request.POST:
            self._update_index(request, request.POST.get("index_name"))
        elif "complement_index" in request.POST:
            self._complement_index(request, request.POST.get("index_name"))
        return redirect("site_elasticsearch", site_id=self.site.id)

    def _rebuild_index(self, request, index_name):
        elasticsearch_tasks.elasticsearch_rebuild_for_tenant.delay(
            self.site.schema_name, self._index_key(index_name)
        )
        messages.success(
            request,
            _("Rebuilding index started for %(index_name)s")
            % {"index_name": self._index_name(index_name)},
        )

    def _update_index(self, request, index_name):
        elasticsearch_tasks.elasticsearch_index_data_for_tenant.delay(
            self.site.schema_name, self._index_key(index_name)
        )
        messages.success(
            request,
            _("Rebuilding index started for %(index_name)s")
            % {"index_name": self._index_name(index_name)},
        )

    def _complement_index(self, request, index_name):
        elasticsearch_tasks.elasticsearch_complement_index.delay(
            self.site.schema_name, self._index_key(index_name)
        )
        messages.success(
            request,
            _("Complement index started for %(index_name)s")
            % {"index_name": self._index_name(index_name)},
        )

    def _index_name(self, index_name):
        if index_name == "*":
            return _("all indices")
        return index_name

    def _index_key(self, index_name):
        if index_name == "*":
            return None
        return index_name


class ElasticsearchDiagnosticsView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/profile/elasticsearch_diagnostics.html"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "indices": [d.Index.name for d in list_documents()],
            "result": self.submit_query(json.dumps, indent=4),
        }

    def submit_query(self, callback=None, *args, **kwargs):
        try:
            index = self.request.GET.get("index") or "_all"
            query = self.request.GET.get("query") or None
            size = int(self.request.GET.get("size") or 10)
            offset = size * (int(self.request.GET.get("page") or 1) - 1)
            assert offset >= 0
            assert size >= 1

            search = Search(index=index).filter(
                "term", tenant_name=self.site.schema_name
            )
            if query:
                search = search.query(Q("simple_query_string", query=query))

            response = search[offset : offset + size].execute()
            total = response.hits.total.value

            config = None
            if index != "_all":
                es = get_connection()
                try:
                    config = es.indices.get(index=index)
                except Exception:
                    pass

            if result := [record.to_dict() for record in response.hits]:
                return {
                    "config": callback(config, *args, **kwargs) if config else None,
                    "data": callback(result, *args, **kwargs),
                    "first": offset + 1,
                    "last": min(offset + size, total),
                    "total": total,
                }
            elif config:
                return {"config": callback(config, *args, **kwargs)}
        except AssertionError:
            pass
        return None
