import logging
import traceback

from django.contrib import auth, messages
from django.shortcuts import redirect
from django.utils.module_loading import import_string
from django.utils.translation import gettext as _
from django.views.generic import TemplateView

from control.lib import reverse
from control.models import Task
from control.views.mixins import SiteDetailsViewMixin
from core.lib import is_valid_domain

logger = logging.getLogger(__name__)


class BackgroundTasksView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/profile/background_tasks.html"

    BACKGROUND_TASKS = (
        ("core.tasks.cronjobs.depublicate_content", _("Scheduled archive / delete")),
        (
            "core.tasks.cronjobs.make_publication_revisions",
            _("Make publication revisions"),
        ),
        ("external_content.tasks.fetch_external_content", _("Fetch external content")),
        (
            "control.tasks.external_content_update_all_featured_images",
            _("Refresh cover images on external content"),
        ),
        ("core.tasks.check_links.check_internal_links", _("Check internal links")),
    )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "background_tasks": self.BACKGROUND_TASKS,
        }

    def get_success_url(self):
        return reverse("site_backgroundtasks", args=[self.site.id])

    def post(self, request, *args, **kwargs):
        try:
            for task, label in self.BACKGROUND_TASKS:
                if task in request.POST:
                    method = import_string(task)
                    method.delay(schema_name=self.site.schema_name)
                    messages.success(
                        request, _("Task %(task)s started") % {"task": label}
                    )
                    return redirect(self.get_success_url())

            if "replace_links" in request.POST:
                replace_domain = request.POST.get("old_domain")
                if not replace_domain or not is_valid_domain(replace_domain):
                    messages.error(
                        request,
                        _("The domain %(replace_domain)s is not a valid domain")
                        % {"replace_domain": replace_domain or ""},
                    )
                    return redirect(self.get_success_url())

                task = Task.objects.create_task(
                    "control.tasks.links.replace_domain_links",
                    (
                        self.site.schema_name,
                        request.POST.get("old_domain"),
                    ),
                    client=self.site,
                    author=auth.get_user(self.request),
                )

                messages.info(
                    request,
                    _(
                        "Task %(task)s started with %(parameter)s. Task id is %(task_id)s"
                    )
                    % {
                        "task": _("Replace domain links"),
                        "parameter": request.POST.get("old_domain"),
                        "task_id": task.id,
                    },
                )
        except Exception:
            logger.error(traceback.format_exc())
            messages.error(request, _("An error occurred while starting the task"))

        return redirect(self.get_success_url())
