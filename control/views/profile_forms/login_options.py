import logging

from django import forms
from django_tenants.utils import tenant_context

from control.views.profile_forms.mixin import ProfileFormBaseView
from core import config, constances

logger = logging.getLogger(__name__)


class SettingsForm(forms.Form):
    LOGIN_CHOICES = [
        *((c["value"], c["label"]) for c in constances.OIDC_PROVIDER_OPTIONS)
    ]
    login_options = forms.MultipleChoiceField(
        label="",
        required=False,
        choices=LOGIN_CHOICES,
        widget=forms.CheckboxSelectMultiple(),
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        with tenant_context(site):
            kwargs["initial"]["login_options"] = config.OIDC_PROVIDERS
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            config.OIDC_PROVIDERS = self.cleaned_data["login_options"]


class SettingsView(ProfileFormBaseView):
    template_name = "sites/profile/login_options_form.html"
    form_class = SettingsForm
