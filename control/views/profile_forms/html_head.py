import json
import logging

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django_tenants.utils import tenant_context

from core import config
from core.lib import CspHeaderExceptionConfig
from core.utils import nested_form_input

from .mixin import ProfileFormBaseView

logger = logging.getLogger(__name__)


class InputBase:
    def __init__(self, data):
        self.data = data

    def get_value(self):
        if self.data:
            return self.get_from_post()
        return self.get_from_config()

    def get_from_config(self):
        raise NotImplementedError()

    def get_from_post(self):
        raise NotImplementedError()


class CSPHeaderInput(InputBase):
    def get_from_config(self):
        return CspHeaderExceptionConfig().exceptions

    def get_from_post(self):
        return self.data.get("csp_header_exceptions", [])


class CustomJavascriptInput(InputBase):
    def get_from_post(self):
        return self.data.get("custom_javascript", "").strip()

    def get_from_config(self):
        return config.CUSTOM_JAVASCRIPT


class SettingsForm(forms.Form):
    custom_javascript = forms.CharField(
        label=_("Custom Javascript"),
        required=False,
        widget=forms.Textarea(attrs={"rows": "6"}),
        help_text=_(
            """Add custom javascript that will be captured within a <script> tag.
                        There is no syntax check, so use a tool like <a target="_blank" href="https://jshint.com/">jshint</a>
                        to check the validity."""
        ),
    )
    csp_header_exceptions = forms.JSONField(
        required=False, label=_("CSP header exceptions")
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        self.nested_form_input = nested_form_input.parse(kwargs.get("data", {}))
        super().__init__(*args, **kwargs)

    def clean_csp_header_exceptions(self):
        csp_header_exceptions = CSPHeaderInput(self.nested_form_input).get_value()
        for exception in csp_header_exceptions:
            if not CspHeaderExceptionConfig.is_valid_url(exception["url"]):
                raise ValidationError("URL %s is not a valid url." % exception["url"])
            invalid_types = [
                x
                for x in exception["types"]
                if x not in CspHeaderExceptionConfig.SOURCES
            ]
            if len(invalid_types) > 0:
                raise ValidationError("Invalid type %s" % invalid_types)
        return csp_header_exceptions

    def clean_custom_javascript(self):
        return CustomJavascriptInput(self.nested_form_input).get_value()

    def save(self):
        with tenant_context(self.site):
            config.CUSTOM_JAVASCRIPT = self.cleaned_data["custom_javascript"]
            config.CSP_HEADER_EXCEPTIONS = self.cleaned_data["csp_header_exceptions"]


class SettingsView(ProfileFormBaseView):
    template_name = "sites/profile/html_head_form.html"
    form_class = SettingsForm

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "form_values": json.dumps(self._form_values()),
        }

    def _form_values(self):
        parsed_data = (
            nested_form_input.parse(self.request.POST)
            if self.request.method == "POST"
            else None
        )
        with tenant_context(self.site):
            return {
                "custom_javascript": CustomJavascriptInput(parsed_data).get_value(),
                "csp_header_exceptions": CSPHeaderInput(parsed_data).get_value(),
            }
