import logging

from django import forms
from django.contrib import messages
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.utils.translation import gettext_lazy as _
from django_tenants.utils import tenant_context

from control.views.profile_forms.mixin import ProfileFormBaseView
from core.models import ProfileField, ProfileSet

logger = logging.getLogger(__name__)


class SettingsForm(forms.Form):
    name = forms.CharField(label=_("Name"), max_length=255, required=True)
    field = forms.ChoiceField(
        label=_("Profile field"),
        widget=forms.Select,
    )

    def __init__(self, *args, site, profile_set, action, request, **kwargs):
        self.site = site
        self.profile_set = profile_set
        self.action = action
        self.request = request

        with tenant_context(site):
            if self.profile_set and not kwargs.get("initial"):
                kwargs["initial"] = {
                    "name": self.profile_set.name,
                    "field": self.profile_set.field.id,
                }

        super().__init__(*args, **kwargs)

        with tenant_context(site):
            self.fields["field"].choices = (
                (x.id, x.name) for x in ProfileField.objects.all()
            )
            if self.action == "delete":
                for field in self.fields.values():
                    field.required = False

    def save(self):
        with tenant_context(self.site):
            if self.action == "delete":
                self.profile_set.delete()
                messages.success(self.request, _("Profile set deleted"))
            elif self.action == "add":
                ProfileSet.objects.create(
                    name=self.cleaned_data["name"],
                    field_id=self.cleaned_data["field"],
                )
                messages.success(self.request, _("Profile set added"))
                pass
            elif self.action == "edit":
                self.profile_set.name = self.cleaned_data["name"]
                self.profile_set.field_id = self.cleaned_data["field"]
                self.profile_set.save()
                messages.success(self.request, _("Profile set updated"))
                pass


class FormSpecifics:
    action = None
    form_title = None
    form_submit = None

    def build_match(self, action, specifics_classes):
        for class_object in specifics_classes:
            if class_object.action == action:
                return class_object()

    def get_context(self):
        return {}


class FormSpecificsAdd(FormSpecifics):
    action = "add"

    def get_context(self):
        return {
            "form_title": _("Add profile set"),
            "form_submit_label": _("Add"),
            "form_submit_style": "primary",
        }


class FormSpecificsEdit(FormSpecifics):
    action = "edit"

    def get_context(self):
        return {
            "form_title": _("Edit profile set"),
            "form_submit_label": _("Save"),
            "form_submit_style": "primary",
        }


class FormSpecificsDelete(FormSpecifics):
    action = "delete"

    def get_context(self):
        return {
            "form_title": _("Delete profile set"),
            "form_submit_label": _("Delete"),
            "form_submit_style": "danger",
        }


class SettingsView(ProfileFormBaseView):
    template_name = "sites/profile/profilefieldset_form.html"
    form_class = SettingsForm

    def __init__(self, *args, **kwargs):
        self.action = None
        self.profile_set = None
        self.id = None
        super().__init__(*args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.get_site(kwargs.get("site_id"))
        if not self.site:
            return HttpResponseNotFound(_("Site not found"))

        with tenant_context(self.site):
            if kwargs.get("action") != "add" and ":" in kwargs.get("action"):
                self.action, self.id = kwargs.get("action").split(":")
                self.profile_set = ProfileSet.objects.filter(id=self.id).first()
            else:
                self.action = kwargs.get("action")
                self.id = None

            if self.action not in ["add", "edit", "delete"]:
                messages.error(request, _("Invalid action"))
                return HttpResponseRedirect(self.get_success_url())
            if self.id and not self.profile_set:
                messages.error(request, _("Invalid profile set"))
                return HttpResponseRedirect(self.get_success_url())
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        specific_context = (
            FormSpecifics()
            .build_match(
                self.action, [FormSpecificsAdd, FormSpecificsEdit, FormSpecificsDelete]
            )
            .get_context()
        )
        return {
            **super().get_context_data(**kwargs),
            "action": self.action,
            "id": self.id,
            "profile_set": self.profile_set,
            **specific_context,
        }

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "action": self.action,
            "profile_set": self.profile_set,
            "request": self.request,
        }
