import logging

from celery.result import AsyncResult
from django import forms
from django.contrib import messages
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView
from django_tenants.utils import schema_context, tenant_context

from control.lib import reverse
from control.tasks import copy_group
from control.views.mixins import SiteDetailsViewMixin
from core import config
from core.lib import get_base_url
from core.models import Group
from tenants.models import Client, GroupCopy

logger = logging.getLogger(__name__)


class CopyGroupForm(forms.Form):
    source_group = forms.ChoiceField(
        label=_("Group"),
        choices=[],
        help_text=_("Select the group to copy."),
        required=True,
    )
    target_schema = forms.ChoiceField(
        label=_("Target website"),
        choices=[],
        help_text=_("Select the site to copy the group to."),
        required=True,
    )
    target_group_name = forms.CharField(
        label=_("New group name"),
        required=True,
    )
    copy_members = forms.BooleanField(
        label=_("Copy members"),
        required=False,
        help_text=_(
            "Make a copy of all group members. This creates users on the target site if they do not exist!"
        ),
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        super().__init__(*args, **kwargs)
        self.fields["source_group"].choices = self._source_group_choices()
        self.fields["target_schema"].choices = self._target_schema_choices()

    def _source_group_choices(self):
        with tenant_context(self.site):

            def group_description(group):
                return f"{group.name} - {group.members.count()} members"

            groups = sorted(
                Group.objects.all(), key=lambda x: group_description(x).lower()
            )
            return [
                ("", _("Select a group from this list.")),
                *[(g.id, group_description(g)) for g in groups],
            ]

    def _target_schema_choices(self):
        def site_description(site):
            with tenant_context(site):
                return f"{config.NAME} - {get_base_url()}"

        sites = Client.objects.exclude(
            schema_name__in=["public", self.site.schema_name]
        )
        sites = sorted(sites, key=lambda x: site_description(x).lower())
        return [
            (
                self.site.schema_name,
                _("%(site)s (This site)") % {"site": site_description(self.site)},
            ),
            *[(s.schema_name, site_description(s)) for s in sites],
        ]

    def clean_target_group_name(self):
        with schema_context(self.cleaned_data["target_schema"]):
            if Group.objects.filter(
                name=self.cleaned_data["target_group_name"]
            ).exists():
                raise forms.ValidationError(_("A group with this name already exists."))

        incomplete_tasks = GroupCopy.objects.filter(
            source_tenant=self.site.schema_name,
            target_tenant=self.cleaned_data["target_schema"],
            target_group_name=self.cleaned_data["target_group_name"],
            task_state__in=["PENDING", "STARTED"],
            created_at__gt=timezone.now() - timezone.timedelta(days=1),
        )
        if incomplete_tasks.count():
            raise forms.ValidationError(
                _("A copy task for this group is already in progress.")
            )

        return self.cleaned_data["target_group_name"]


class CopyGroupView(SiteDetailsViewMixin, FormView):
    template_name = "sites/profile/copy_group.html"
    form_class = CopyGroupForm

    def get_success_url(self):
        return reverse("site_copy_group", args=[self.site.id])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["site"] = self.site
        return kwargs

    def form_valid(self, form):
        source_group = form.cleaned_data["source_group"]
        target_schema = form.cleaned_data["target_schema"]
        copy_members = bool(form.cleaned_data["copy_members"])
        target_group_name = form.cleaned_data["target_group_name"]

        copy_group.delay(
            source_schema=self.site.schema_name,
            action_user_id=str(self.request.user.id),
            group_id=source_group,
            target_schema=target_schema,
            copy_members=copy_members,
            target_group_name=target_group_name,
        )
        messages.success(self.request, _("Group copy has started."))

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        def update_status(task):
            if task.task_state not in ["PENDING", "STARTED"]:
                return task
            async_result = AsyncResult(task.task_id)
            if async_result.state != "PENDING":
                try:
                    task.task_state = async_result.state
                    task.task_response = async_result.result
                    task.save()
                except TypeError:
                    task.task_response = str(async_result.result)
                    task.save()
            return task

        return {
            **super().get_context_data(**kwargs),
            "background_tasks": [
                update_status(s)
                for s in GroupCopy.objects.filter(source_tenant=self.site.schema_name)
            ],
        }
