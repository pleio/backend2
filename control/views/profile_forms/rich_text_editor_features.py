import logging

from django import forms
from django_tenants.utils import tenant_context

from control.views.profile_forms.mixin import ProfileFormBaseView
from core import config
from core.utils.rich_text_editor_features import FEATURES_AVAILABLE

logger = logging.getLogger(__name__)


class SettingsForm(forms.Form):
    RTE_FEATURE_CHOICES = [*((c.key, c.label) for c in FEATURES_AVAILABLE)]
    rich_text_editor_features = forms.MultipleChoiceField(
        label="",
        required=False,
        choices=RTE_FEATURE_CHOICES,
        widget=forms.CheckboxSelectMultiple(),
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        with tenant_context(site):
            kwargs["initial"]["rich_text_editor_features"] = (
                config.RICH_TEXT_EDITOR_FEATURES
            )
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            config.RICH_TEXT_EDITOR_FEATURES = self.cleaned_data[
                "rich_text_editor_features"
            ]


class SettingsView(ProfileFormBaseView):
    template_name = "sites/profile/rich_text_editor_features_form.html"
    form_class = SettingsForm
