import json
import logging

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django_tenants.utils import tenant_context

from control.views.profile_forms.mixin import ProfileFormBaseView
from core import config
from core.resolvers.shared import resolve_load_appointment_types

logger = logging.getLogger(__name__)


class SettingsForm(forms.Form):
    onlineafspraken_enabled = forms.BooleanField(
        label=_("Enable onlineafspraken.nl"), required=False
    )
    onlineafspraken_key = forms.CharField(label=_("Api key"), required=False)
    onlineafspraken_secret = forms.CharField(label=_("Api secret"), required=False)
    onlineafspraken_url = forms.CharField(
        label=_("Override default api url"), required=False
    )
    videocall_appointment_type = forms.CharField(
        label=_("Configure appointment types"), required=False, widget=forms.Textarea()
    )

    videocall_enabled = forms.BooleanField(label=_("Enable videocalls"), required=False)
    videocall_api_url = forms.CharField(label=_("Override api url"), required=False)
    videocall_profilepage = forms.BooleanField(
        required=False, label=_("Enable videocalls at profile page")
    )
    videocall_throttle = forms.IntegerField(
        label=_("Maximum number of room reservations per hour"), required=False
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        with tenant_context(site):
            kwargs["initial"] = {
                "onlineafspraken_enabled": config.ONLINEAFSPRAKEN_ENABLED,
                "onlineafspraken_key": config.ONLINEAFSPRAKEN_KEY or "",
                "onlineafspraken_secret": config.ONLINEAFSPRAKEN_SECRET or "",
                "onlineafspraken_url": config.ONLINEAFSPRAKEN_URL or "",
                "videocall_appointment_type": self.load_appointment_type_config(),
                "videocall_enabled": config.VIDEOCALL_ENABLED,
                "videocall_api_url": config.VIDEOCALL_API_URL or "",
                "videocall_profilepage": config.VIDEOCALL_PROFILEPAGE or "",
                "videocall_throttle": config.VIDEOCALL_THROTTLE or 0,
            }
            pass
        super().__init__(*args, **kwargs)

    @staticmethod
    def load_appointment_type_config():
        try:
            return json.dumps(resolve_load_appointment_types(), indent=2)
        except Exception:
            return "[]"

    def save(self):
        with tenant_context(self.site):
            config.ONLINEAFSPRAKEN_ENABLED = bool(
                self.cleaned_data["onlineafspraken_enabled"]
            )
            config.ONLINEAFSPRAKEN_KEY = (
                self.cleaned_data["onlineafspraken_key"] or None
            )
            config.ONLINEAFSPRAKEN_SECRET = (
                self.cleaned_data["onlineafspraken_secret"] or None
            )
            config.ONLINEAFSPRAKEN_URL = (
                self.cleaned_data["onlineafspraken_url"] or None
            )
            config.VIDEOCALL_APPOINTMENT_TYPE = (
                self.cleaned_data["videocall_appointment_type"] or []
            )
            config.VIDEOCALL_ENABLED = bool(self.cleaned_data["videocall_enabled"])
            config.VIDEOCALL_API_URL = self.cleaned_data["videocall_api_url"] or None
            config.VIDEOCALL_PROFILEPAGE = (
                self.cleaned_data["videocall_profilepage"] or None
            )
            config.VIDEOCALL_THROTTLE = self.cleaned_data["videocall_throttle"] or 0

    def clean_videocall_appointment_type(self):
        try:
            return json.loads(self.cleaned_data["videocall_appointment_type"])
        except json.JSONDecodeError as e:
            raise ValidationError(str(e))


class SettingsView(ProfileFormBaseView):
    template_name = "sites/profile/meetings_settings_form.html"
    form_class = SettingsForm
