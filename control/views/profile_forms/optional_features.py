import logging

from django import forms
from django.utils.translation import gettext_lazy as _
from django_tenants.utils import tenant_context

from control.views.profile_forms.mixin import ProfileFormBaseView
from core import config
from core.utils.rich_text_editor_features import FEATURES_AVAILABLE

logger = logging.getLogger(__name__)


class SettingsForm(forms.Form):
    OPTIONAL_FEATURES = (
        ("COLLAB_EDITING_ENABLED", _("Collaborative editing")),
        ("EDIT_USER_NAME_ENABLED", _("Allow for a custom username")),
        ("PUSH_NOTIFICATIONS_ENABLED", _("Push notifications")),
        ("DATAHUB_EXTERNAL_CONTENT_ENABLED", _("Datahub content")),
        ("RECURRING_EVENTS_ENABLED", _("Recurring events (Beta)")),
        ("CHAT_ENABLED", _("Chat (Beta)")),
        ("INTEGRATED_VIDEOCALL_ENABLED", _("Integrated videocall (Beta)")),
        ("SKIP_CLAMAV", _("Skip CLAMAV")),
        (
            "GET_CREATE_GROUP_MEMBERS",
            _("Automatically create user accounts for new group members"),
        ),
        ("EMAIL_DISABLED", _("Prevent all mail sending")),
        ("CONTENT_TRANSLATION", _("Translate content")),
        ("CONTENT_MODERATION_ENABLED", _("Enable content moderation")),
        ("COMMENT_MODERATION_ENABLED", _("Enable comment moderation")),
        ("DAILY_SNAPSHOT_ENABLED", _("Enable daily snapshot")),
        ("PREVENT_INSERT_MEDIA_AT_QUESTIONS", _("Prevent insert media at questions")),
    )
    optional_features = forms.MultipleChoiceField(
        label="",
        choices=OPTIONAL_FEATURES,
        widget=forms.CheckboxSelectMultiple(),
        required=False,
        validators=[],
    )
    EVENT_ADD_EMAIL_ATTENDEE_CHOICES = (
        ("admin", "Site admins"),
        ("owner", "Everyone that can edit"),
    )
    event_add_email_attendee = forms.ChoiceField(
        label=_("Add attendee to event"),
        required=False,
        widget=forms.RadioSelect(),
        choices=EVENT_ADD_EMAIL_ATTENDEE_CHOICES,
    )
    search_schemas = forms.CharField(label=_("Search schemas"), required=False)
    rich_text_editor_features = forms.MultipleChoiceField(
        label="",
        required=False,
        choices=[(c.key, c.label) for c in FEATURES_AVAILABLE],
        widget=forms.CheckboxSelectMultiple(),
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        with tenant_context(site):
            kwargs["initial"]["optional_features"] = [
                x[0] for x in self.OPTIONAL_FEATURES if getattr(config, x[0])
            ]
            kwargs["initial"]["event_add_email_attendee"] = (
                config.EVENT_ADD_EMAIL_ATTENDEE
            )
            kwargs["initial"]["search_schemas"] = (
                ", ".join(config.SEARCH_RELATED_SCHEMAS)
                if config.SEARCH_RELATED_SCHEMAS
                else ""
            )
        super().__init__(*args, **kwargs)

    def save(self):
        with tenant_context(self.site):
            for option, _label in self.OPTIONAL_FEATURES:
                setattr(
                    config, option, option in self.cleaned_data["optional_features"]
                )
            config.EVENT_ADD_EMAIL_ATTENDEE = self.cleaned_data[
                "event_add_email_attendee"
            ]
            config.SEARCH_RELATED_SCHEMAS = (
                [x.strip() for x in self.cleaned_data["search_schemas"].split(",")]
                if self.cleaned_data["search_schemas"]
                else []
            )


class SettingsView(ProfileFormBaseView):
    template_name = "sites/profile/optional_features_form.html"
    form_class = SettingsForm
