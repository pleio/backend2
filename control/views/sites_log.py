import json
import logging

import django_filters
from auditlog.models import LogEntry
from django import forms
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django_tenants.utils import tenant_context
from post_deploy.models import PostDeployLog

from control.lib import reverse
from control.utils.post_deploy import serialize_log_record
from control.views.mixins import SiteDetailsViewMixin
from core.lib import get_full_url
from core.models import FloodLog, MailLog
from core.models.search import SearchIndexLog
from entities.file.models import ScanIncident

logger = logging.getLogger(__name__)


class AuditLogFilter(django_filters.FilterSet):
    object_pk = django_filters.CharFilter(
        label=_("Object ID"),
        lookup_expr="iexact",
        widget=forms.TextInput(attrs={"placeholder": "Object ID"}),
    )
    content_type = django_filters.ChoiceFilter(field_name="content_type")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_filters["content_type"].extra.update(
            {"choices": self._content_type_changes()}
        )

    def _content_type_changes(self):
        return [
            (ct.id, str(ct))
            for ct in ContentType.objects.order_by("app_label")
            if LogEntry.objects.filter(content_type=ct.id).exists()
        ]

    class Meta:
        model = LogEntry
        fields = []


class AuditLogView(SiteDetailsViewMixin, TemplateView):
    http_method_names = ["get"]

    template_name = "sites/auditlog.html"

    @staticmethod
    def _extract_changes(log_changes):
        for key, changes in log_changes:
            yield key, changes[0], changes[1]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        request = self.request

        page_param = request.GET.get("page", "1")
        page = max(int(page_param) - 1, 0) if page_param.isnumeric() else 0
        page_size = 100
        offset = page * page_size

        with tenant_context(self.site):
            filtered_qs = AuditLogFilter(request.GET, LogEntry.objects.all())
            logs = filtered_qs.qs[
                offset : offset + page_size + 1
            ]  # grab one extra so we can check if there are more pages
            for log in logs:
                log.changes_obj = json.loads(log.changes)

            next_page = request.GET.copy()
            next_page["page"] = page + 2
            previous_page = request.GET.copy()
            previous_page["page"] = page

            has_next = len(logs) > page_size
            has_previous = page > 0

            return {
                **context,
                "logs": [
                    {
                        "action": log.action,
                        "timestamp": log.timestamp,
                        "actor": log.actor.email if log.actor else None,
                        "content_type": str(log.content_type),
                        "object_pk": log.object_pk,
                        "changes": [*self._extract_changes(log.changes_obj.items())],
                    }
                    for log in logs[:page_size]
                ],
                "form": filtered_qs.form,
                "previous_page": previous_page.urlencode() if has_previous else None,
                "next_page": next_page.urlencode() if has_next else None,
            }


class ScanIncidentFilter(django_filters.FilterSet):
    blocked = django_filters.BooleanFilter(
        field_name="file",
        lookup_expr="isnull",
        widget=forms.Select(
            attrs={"class": "form-select"},
            choices=(
                (None, "Alles"),
                (
                    True,
                    "Verwijderd",
                ),
                (False, "Geblokkeerd"),
            ),
        ),
    )
    filename = django_filters.CharFilter(
        field_name="file_title",
        lookup_expr="icontains",
        widget=forms.TextInput(
            attrs={"class": "form-control", "placeholder": _("Filename")}
        ),
    )

    class Meta:
        model = ScanIncident
        fields = []


class ScanLogView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/scanlog.html"

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            filtered_qs = ScanIncidentFilter(
                self.request.GET, queryset=ScanIncident.objects.all()
            )

            return {
                **super().get_context_data(**kwargs),
                "items": [
                    {
                        "date": r.date,
                        "title": r.file_title,
                        "message": r.message,
                        "owner_url": get_full_url(r.file_owner.url),
                        "owner_name": r.file_owner.name,
                    }
                    for r in filtered_qs.qs[:100]
                ],
                "form": filtered_qs.form,
            }


class SearchIndexLogFilterForm(forms.Form):
    object_id = forms.CharField(label=_("Object ID"), required=False)
    query = forms.CharField(label=_("Query"), required=False)

    def filter(self, qs):
        if self.cleaned_data["object_id"]:
            qs = qs.filter(object_id=self.cleaned_data["object_id"])
        if self.cleaned_data["query"]:
            qs = qs.filter(
                Q(message__icontains=self.cleaned_data["query"])
                | Q(traceback__icontains=self.cleaned_data["query"])
                | Q(object_type__icontains=self.cleaned_data["query"])
            )
        return qs


class SearchIndexLogView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/search_index_log.html"

    def _get_form(self):
        if self.request.GET.items():
            form = SearchIndexLogFilterForm(self.request.GET)
            form.is_valid()
            return form
        return SearchIndexLogFilterForm()

    def _get_paginator(self, qs):
        paginator = Paginator(qs, 30)
        try:
            return paginator.page(self.request.GET.get("page", 1))
        except PageNotAnInteger:
            return paginator.page(1)
        except EmptyPage:
            return paginator.page(paginator.num_pages)

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            form = self._get_form()
            log_qs = SearchIndexLog.objects.order_by("-created_at")

            if self.request.GET.items() and form.is_valid():
                log_qs = form.filter(log_qs)

            log_qs = self._get_paginator(log_qs)

            return {
                **super().get_context_data(**kwargs),
                "form": form,
                "items": [
                    {
                        "created_at": r.created_at,
                        "message": r.message,
                        "object_type": r.object_type,
                        "object_id": r.object_id,
                        "traceback": r.traceback,
                    }
                    for r in log_qs
                ],
                "pages": log_qs,
            }


class DeployTaskLogView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/deploy_task_logs.html"

    def post(self, request, *args, **kwargs):
        with tenant_context(self.site):
            from post_deploy.utils import run_task

            run_task(request.POST["retry"])
            messages.info(request, "Retry {}".format(request.POST["retry"]))
            return redirect(reverse("site_post_deploy_report", args=[self.site.id]))

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            return {
                **super().get_context_data(**kwargs),
                "items": [
                    serialize_log_record(record)
                    for record in PostDeployLog.objects.order_by("-created_at")
                ],
            }


class MailLogFilterForm(forms.Form):
    email = forms.EmailField(label=_("Recipient"), required=False)
    query = forms.CharField(label=_("Query"), required=False)
    with_feedback = forms.BooleanField(
        widget=forms.CheckboxInput(), label=_("With feedback"), required=False
    )

    def filter(self, qs):
        if self.cleaned_data["with_feedback"]:
            qs = qs.filter(result__isnull=False)

        if self.cleaned_data["email"]:
            qs = qs.filter(
                Q(receiver__email=self.cleaned_data["email"])
                | Q(sender__email=self.cleaned_data["email"])
            )
        if self.cleaned_data["query"]:
            qs = qs.filter(
                Q(sender__name__icontains=self.cleaned_data["query"])
                | Q(receiver__name__icontains=self.cleaned_data["query"])
                | Q(subject__icontains=self.cleaned_data["query"])
                | Q(result__icontains=self.cleaned_data["query"])
                | Q(mail_instance__mailer__icontains=self.cleaned_data["query"])
            )
        return qs


class MailLogView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/mail_log.html"

    def _get_form(self):
        if self.request.GET.items():
            form = MailLogFilterForm(self.request.GET)
            form.is_valid()
            return form
        return MailLogFilterForm()

    def _get_paginator(self, qs):
        paginator = Paginator(qs, 30)
        try:
            return paginator.page(self.request.GET.get("page", 1))
        except PageNotAnInteger:
            return paginator.page(1)
        except EmptyPage:
            return paginator.page(paginator.num_pages)

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            items = MailLog.objects.order_by("-created_at")

            form = self._get_form()

            items = form.filter(items)
            pages = self._get_paginator(items)

            return {
                **super().get_context_data(**kwargs),
                "items": [
                    {
                        "created_at": r.created_at,
                        "recipient": r.receiver_email,
                        "mailer": r.mail_instance.mailer,
                        "subject": r.subject,
                        "result": r.result,
                    }
                    for r in pages
                ],
                "pages": pages,
                "form": form,
            }


class FloodLogFilterForm(forms.Form):
    email = forms.CharField(label=_("User"), required=False)
    ip = forms.GenericIPAddressField(label=_("IP address"), required=False)

    def filter(self, qs):
        if self.cleaned_data["email"]:
            qs = qs.filter(Q(user_identifier__icontains=self.cleaned_data["email"]))
        if self.cleaned_data["ip"]:
            qs = qs.filter(Q(ip=self.cleaned_data["ip"]))
        return qs


class FloodLogView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/floodlog.html"

    def post(self, request, *args, **kwargs):
        with tenant_context(self.site):
            user = FloodLog.objects.filter(user_identifier=request.POST["delete"])
            user.delete()
            messages.info(request, "{} FloodLog deleted".format(request.POST["delete"]))
            return redirect(reverse("floodlog", args=[self.site.id]))

    def _get_form(self):
        if self.request.GET.items():
            form = FloodLogFilterForm(self.request.GET)
            form.is_valid()
            return form
        return FloodLogFilterForm()

    def _get_paginator(self, qs):
        paginator = Paginator(qs, 30)
        try:
            return paginator.page(self.request.GET.get("page", 1))
        except PageNotAnInteger:
            return paginator.page(1)
        except EmptyPage:
            return paginator.page(paginator.num_pages)

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            items = FloodLog.objects.order_by("user_identifier")

            form = self._get_form()

            items = form.filter(items)
            pages = self._get_paginator(items)

            return {
                **super().get_context_data(**kwargs),
                "items": [
                    {
                        "user": r.user_identifier,
                        "ip": r.ip,
                        "expires": r.expires,
                    }
                    for r in pages
                ],
                "pages": pages,
                "form": form,
            }


class LogDashboardView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/log_dashboard.html"
