import logging
import os

import botocore.exceptions
from django.conf import settings
from django.contrib import auth, messages
from django.db import DatabaseError
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import FormView, TemplateView
from django_tenants.utils import tenant_context
from pip._internal.utils.filesystem import format_file_size

from control.forms import (
    AddSiteForm,
    ConfirmSiteBackupForm,
    ConfirmSnapshotForm,
    DeleteSiteForm,
    MigrateStorageForm,
    SiteUpdateForm,
)
from control.lib import reverse
from control.models import AccessCategory, AccessLog, Snapshot, Task
from control.site_reports.group_admins import GroupAdminsReport
from control.site_reports.site_admins import SiteAdminsReport
from control.utils.backup import schedule_backup
from control.views.mixins import SecureControlViewMixin, SiteDetailsViewMixin
from core import config
from core.constances import SiteCategoryChoices, SitePlanChoices
from core.lib import get_full_url, safe_file_path
from core.models import SiteStat, WebPushSubscription
from core.utils.object_storage import ObjectStorageClient
from user.models import User

logger = logging.getLogger(__name__)


class SiteDetailsView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/details.html"

    class SupportContract:
        def value(self):
            return config.SUPPORT_CONTRACT_ENABLED

    class SupportHours:
        def value(self):
            return config.SUPPORT_CONTRACT_HOURS_REMAINING

    class SiteCategory:
        def value(self):
            try:
                return SiteCategoryChoices[config.SITE_CATEGORY].value
            except Exception:
                pass
            return ""

    class SitePlan:
        def value(self):
            try:
                return SitePlanChoices[config.SITE_PLAN].value
            except Exception:
                pass
            return ""

    class PushSubscriptionUsers:
        def value(self):
            try:
                return WebPushSubscription.objects.values("user_id").distinct().count()
            except DatabaseError:
                pass
            return 0

    class BlockedUsers:
        def value(self):
            return User.objects.filter(is_active=False).count()

    class ActiveUsers:
        def value(self):
            return User.objects.filter(is_active=True).count()

    class DiskStatDays:
        def value(self):
            qs = SiteStat.objects.filter(stat_type="DISK_SIZE")
            result = []
            for disk_size_day in [*qs.values_list("created_at", flat=True)][-50:]:
                result.append(disk_size_day.strftime("%d-%b-%Y"))
            return result

    class DiskStatData:
        def value(self):
            qs = SiteStat.objects.filter(stat_type="DISK_SIZE")
            return [*qs.values_list("value", flat=True)][-50:]

    class DBStatDays:
        def value(self):
            qs = SiteStat.objects.filter(stat_type="DB_SIZE")
            result = []
            for db_size_day in [*qs.values_list("created_at", flat=True)][-50:]:
                result.append(db_size_day.strftime("%d-%b-%Y"))
            return result

    class DBStatData:
        def value(self):
            qs = SiteStat.objects.filter(stat_type="DB_SIZE")
            return [*qs.values_list("value", flat=True)][-50:]

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            return {
                **super().get_context_data(**kwargs),
                "support_contract_enabled": self.SupportContract().value(),
                "support_hours_remaining": self.SupportHours().value(),
                "site_category": self.SiteCategory().value(),
                "site_plan": self.SitePlan().value(),
                "db_stat_days": self.DBStatDays().value(),
                "db_stat_data": self.DBStatData().value(),
                "disk_stat_days": self.DiskStatDays().value(),
                "disk_stat_data": self.DiskStatData().value(),
                "nr_active_users": self.ActiveUsers().value(),
                "nr_blocked_users": self.BlockedUsers().value(),
                "nr_push_device_configured_users": self.PushSubscriptionUsers().value(),
            }


class UpdateProfileView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/profile/index.html"


class SitesAddView(SecureControlViewMixin, FormView):
    form_class = AddSiteForm
    template_name = "sites/add.html"

    def get_success_url(self):
        return reverse("site_index")

    def form_valid(self, form):
        data = form.cleaned_data

        schema_name = data.get("schema_name")
        domain = data.get("domain")
        backup = data.get("backup")

        if backup:
            task = Task.objects.create_task(
                "control.tasks.restore_site",
                (backup, schema_name, domain, form.administrative_details),
            )
        else:
            task = Task.objects.create_task(
                "control.tasks.add_site",
                (schema_name, domain, form.administrative_details),
            )
        messages.info(
            self.request,
            "Add site {%s} gestart in achtergrond (task.id=%s)" % (domain, task.id),
        )
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "has_object_storage": ObjectStorageClient.has_settings(),
            "backup_items": [*BackupItemRepository().backup_items()],
        }


class BackupItemRepository:
    @staticmethod
    def _name(item):
        if item.endswith(".zip"):
            return item[:-4]
        return item

    def backup_items(self):
        shared_backups = set()
        try:
            if ObjectStorageClient.has_settings():
                client = ObjectStorageClient(settings.BACKUP_BUCKET)
                shared_backups = {self._name(item) for item in client.list_objects()}
        except botocore.exceptions.ClientError:
            pass

        local_backups = set()
        # list the first level of the backup folder and the files
        for item in os.listdir(settings.BACKUP_PATH):
            if os.path.isdir(os.path.join(settings.BACKUP_PATH, item)):
                local_backups.add(item)
            elif item.endswith(".zip"):
                local_backups.add(self._name(item))

        all_backups = local_backups.union(shared_backups)

        for item in sorted(all_backups):
            yield item, item in local_backups, item in shared_backups


class SiteDeleteView(SiteDetailsViewMixin, FormView):
    template_name = "sites/delete.html"
    form_class = DeleteSiteForm

    def get_success_url(self):
        return reverse("site_index")

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "initial": {
                "site_id": self.site.id,
            },
        }

    def form_valid(self, form):
        task = Task.objects.create_task("control.tasks.delete_site", (self.site.id,))
        messages.info(
            self.request,
            _(
                "Website %(site)s is being removed in a background task. Task id is %(task_id)s"
            )
            % {"site": self.site.primary_domain, "task_id": task.id},
        )
        return super().form_valid(form)


class SiteDisableView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/disable.html"

    def post(self, request, *args, **kwargs):
        task = Task.objects.create_task(
            "control.tasks.update_site", (self.site.id, {"is_active": False})
        )
        messages.success(
            request,
            _(
                "Website %(site)s is being deactivated in a background task. Task id is %(task_id)s"
            )
            % {"site": self.site.primary_domain, "task_id": task.id},
        )
        return redirect(reverse("site_index"))


class SiteReportView(SiteDetailsViewMixin, View):
    def get(self, request, site_id, report_id, *args, **kwargs):
        reports = {
            "group_admins": GroupAdminsReport(),
            "site_admins": SiteAdminsReport(),
        }

        with tenant_context(self.site):
            report = reports.get(report_id)
            if not report:
                messages.error(request, _("Report not found"))
                return redirect(reverse("site_details", args=(site_id,)))

            return HttpResponse(report.render(), content_type="text/csv")


class SiteEnableView(SiteDetailsViewMixin, TemplateView):
    template_name = "sites/enable.html"

    def post(self, request, *args, **kwargs):
        task = Task.objects.create_task(
            "control.tasks.update_site", (self.site.id, {"is_active": True})
        )
        messages.success(
            request,
            _(
                "Website %(site)s is being enabled in a background task. Task id is %(task_id)s"
            )
            % {"site": self.site.primary_domain, "task_id": task.id},
        )
        return redirect(reverse("site_index"))


class SiteBackupView(SiteDetailsViewMixin, FormView):
    template_name = "sites/backup.html"
    form_class = ConfirmSiteBackupForm

    def get_success_url(self):
        return reverse("site_backup", args=(self.site.id,))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["site"] = self.site
        return kwargs

    def form_valid(self, form):
        task = schedule_backup(
            self.site,
            auth.get_user(self.request),
            form.cleaned_data["include_files"],
            form.cleaned_data["share_backup"],
            (
                form.cleaned_data["from_snapshot"]
                if form.cleaned_data["from_snapshot"]
                else None
            ),
        )
        messages.success(
            self.request,
            _(
                "Website backup for %(site_name)s is started in a background task. Task id is %(task_id)s"
            )
            % {"site_name": self.site.primary_domain, "task_id": str(task.id)},
        )
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "access_logs": AccessLog.objects.filter(site=self.site),
            "snapshots_enabled": settings.RESTIC_REPOSITORY_CONFIGURED,
            "backups": self.serialize_access_logs(self.get_access_logs()),
            "has_object_storage": ObjectStorageClient.has_settings(),
        }

    def serialize_access_logs(self, qs):
        for access_log in qs:
            resource_path = safe_file_path(settings.BACKUP_PATH, access_log.item_id)
            is_file = access_log.item_id.endswith(".zip") and os.path.exists(
                resource_path
            )
            yield {
                "created_at": access_log.created_at,
                "download": is_file,
                "author": access_log.user.name,
                "download_url": (
                    get_full_url(
                        reverse(
                            "download_backup", args=[self.site.id, access_log.item_id]
                        )
                    )
                    if is_file
                    else ""
                ),
                "filesize": format_file_size(resource_path) if is_file else "",
                "filename": access_log.item_id,
            }

    def get_access_logs(self):
        return AccessLog.objects.filter(
            type=AccessLog.AccessTypes.CREATE,
            category=AccessLog.custom_category(
                AccessCategory.SITE_BACKUP, self.site.id
            ),
        )[:5]


class SiteUpdateView(SiteDetailsViewMixin, FormView):
    template_name = "sites/update.html"
    form_class = SiteUpdateForm

    def get_success_url(self):
        return reverse("site_details", args=(self.site.id,))

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "site": self.site,
        }

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _("Website updated successfully"))
        return super().form_valid(form)


class SiteMigrateStorageView(SiteDetailsViewMixin, FormView):
    template_name = "sites/migrate_storage.html"
    form_class = MigrateStorageForm

    def get_success_url(self):
        return reverse("site_index")

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "initial": {
                "site_id": self.site.id,
            },
        }

    def form_valid(self, form):
        task = Task.objects.create_task(
            "control.tasks.migrate_storage", (self.site.id,)
        )
        messages.info(
            self.request,
            _(
                "Website %(site)s storage is being migrated in a background task. Task id is %(task_id)s"
            )
            % {"site": self.site.primary_domain, "task_id": task.id},
        )
        return super().form_valid(form)


class SiteSnapshotView(SiteDetailsViewMixin, FormView):
    template_name = "sites/snapshot.html"
    form_class = ConfirmSnapshotForm

    def get_success_url(self):
        return reverse("site_snapshot", args=(self.site.id,))

    def form_valid(self, form):
        task = Task.objects.create_task(
            "control.tasks.snapshot",
            (self.site.id,),
            followup="control.tasks.followup_snapshot_complete",
            client=self.site,
            author=auth.get_user(self.request),
        )

        messages.success(
            self.request,
            _(
                "Website snapshot for %(site_name)s is started in a background task. Task id is %(task_id)s"
            )
            % {"site_name": self.site.primary_domain, "task_id": str(task.id)},
        )
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        snapshots = Snapshot.objects.filter(client=self.site).order_by("-started_at")

        return {
            **super().get_context_data(**kwargs),
            "access_logs": [],
            "snapshots_enabled": settings.RESTIC_REPOSITORY_CONFIGURED,
            "snapshots": snapshots,
        }
