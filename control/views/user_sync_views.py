import logging

from botocore.exceptions import ValidationError
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django_tenants.utils import schema_context

from control.lib import all_site_schemas
from control.models import UserSyncController

logger = logging.getLogger(__name__)


class UserSyncIndexView(LoginRequiredMixin, TemplateView):
    template_name = "user_sync/index.html"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "controllers": UserSyncController.objects.all(),
        }


class UserSyncDetailView(LoginRequiredMixin, TemplateView):
    template_name = "user_sync/detail.html"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "controller": self._get_controller(),
            "schemas": self._list_schemas(),
        }

    def _get_controller(self):
        try:
            return UserSyncController.objects.get(pk=self.kwargs["controller_id"])
        except (UserSyncController.DoesNotExist, ValidationError):
            pass

    def _list_schemas(self):
        for schema in all_site_schemas():
            schema_summary = {
                **self._local_schema_properties(schema),
                **self._remote_schema_properties(schema),
            }
            yield {
                **schema_summary,
                "todo_count": schema_summary["total_count"]
                - schema_summary["scheduled_count"],
            }

    def _local_schema_properties(self, schema):
        controller = self._get_controller()
        return {
            "schema_name": schema,
            "scheduled_count": controller.items.filter(schema=schema).count(),
            "completed_count": controller.completed_items(schema=schema).count(),
            "last_completed": controller.completed_items(schema=schema).last(),
            "error_count": controller.items_with_errors(schema=schema).count(),
        }

    def _remote_schema_properties(self, schema):
        with schema_context(schema):
            from user.models import User

            return {"total_count": User.objects.count()}
