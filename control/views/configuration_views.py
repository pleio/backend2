from django.shortcuts import render
from django.utils.translation import gettext as _
from django.utils.translation import pgettext_lazy
from django_tenants.utils import get_tenant_model, tenant_context

from control.utils.render_configuration import SettingsSection
from control.utils.render_configuration.decorators import (
    AccessLevel,
    Boolean,
    ContentType,
    Decorator,
    Json,
    Language,
    Require2FA,
    Timestamp,
    TipTap,
)
from control.utils.render_configuration.settings import (
    KeyValue,
    ListSetting,
    ProfileSections,
    RawSetting,
    Setting,
    TagCategories,
    TagContentType,
    TagSynonyms,
    UserProfile,
)
from core import config


def site_custom_configuration_view(request, site_id):
    from core.base_config import DEFAULT_SITE_CONFIG

    site = get_tenant_model().objects.get(id=site_id)
    with tenant_context(site):
        sections = build_configuration_sections()

        rendered_keys = [
            *sections.setting_keys(),
            *ALREADY_CONTROLLED_BY_CONTROL_KEYS,
        ]
        all_keys = [k for k, _init_and_descr in DEFAULT_SITE_CONFIG.items()]
        missing_keys = [k for k in all_keys if k not in rendered_keys]
        if missing_keys:
            sections.append(
                SettingsSection(
                    _("Recently added settings"),
                    content=[Setting(k) for k in missing_keys],
                )
            )

        site_name = config.NAME

    return render(
        request,
        "sites/admin_settings.html",
        context={
            "site": site,
            "site_name": site_name,
            "site_id": site_id,
            "sections": sections,
        },
    )


def build_configuration_sections():
    return SettingsSection(
        title=_("Settings"),
        content=[
            SettingsSection(
                _("Site"),
                content=[
                    SettingsSection(
                        _("General"),
                        content=[
                            Setting("NAME"),
                            Setting("DESCRIPTION"),
                            Setting("LANGUAGE", Language()),
                            ListSetting("EXTRA_LANGUAGES", Language()),
                            Setting("PIWIK_URL"),
                            Setting("PIWIK_ID"),
                        ],
                    ),
                    SettingsSection(
                        _("Access"),
                        content=[
                            Setting("IS_CLOSED", Boolean()),
                            Setting("LOGIN_INTRO", TipTap()),
                            Setting("WALLED_GARDEN_BY_IP_ENABLED", Boolean()),
                            ListSetting("WHITELISTED_IP_RANGES"),
                            Setting("ENABLE_SEARCH_ENGINE_INDEXING", Boolean()),
                            Setting("SHOW_LOGIN_REGISTER", Boolean()),
                            Setting("REQUIRE_2FA", Require2FA()),
                            Setting("ALLOW_REGISTRATION", Boolean()),
                            ListSetting("DIRECT_REGISTRATION_DOMAINS"),
                            Setting("DEFAULT_ACCESS_ID", AccessLevel()),
                            Setting("HIDE_ACCESS_LEVEL_SELECT", Boolean()),
                            Setting("IDP_ID"),
                            Setting("IDP_NAME"),
                            Setting("AUTO_APPROVE_SSO", Boolean()),
                            Setting("BLOCKED_USER_INTRO_MESSAGE", TipTap()),
                        ],
                    ),
                    SettingsSection(
                        _("Appearance"),
                        content=[
                            Setting("THEME"),
                            Setting("FAVICON"),
                            Setting("FONT_BODY"),
                            Setting("FONT"),
                            Setting("COLOR_HEADER"),
                            Setting("COLOR_PRIMARY"),
                            Setting("COLOR_SECONDARY"),
                            Setting("LOGO"),
                            Setting("LOGO_ALT"),
                            Setting("LIKE_ICON"),
                        ],
                    ),
                    SettingsSection(
                        _("Navigation"),
                        content=[
                            SettingsSection(
                                None,
                                [
                                    Setting("STARTPAGE"),
                                    Setting("STARTPAGE_CMS"),
                                    Setting("ANONYMOUS_START_PAGE"),
                                    Setting("ANONYMOUS_START_PAGE_CMS"),
                                    Setting("ICON_ENABLED", Boolean()),
                                    Setting("ICON"),
                                    Setting("ICON_ALT"),
                                    RawSetting("MENU", Json()),
                                    Setting("MENU_STATE"),
                                ],
                            ),
                            SettingsSection(
                                _("Activity stream"),
                                content=[
                                    Setting("NUMBER_OF_FEATURED_ITEMS"),
                                    Setting("ENABLE_FEED_SORTING", Boolean()),
                                    Setting(
                                        "ACTIVITY_FEED_FILTERS_ENABLED",
                                        Boolean(),
                                    ),
                                ],
                            ),
                            SettingsSection(
                                _("Activity stream leader"),
                                content=[
                                    Setting("LEADER_ENABLED", Boolean()),
                                    Setting("LEADER_BUTTONS_ENABLED", Boolean()),
                                    Setting("SUBTITLE"),
                                    Setting("LEADER_IMAGE"),
                                ],
                            ),
                            SettingsSection(
                                _("Initiative widget"),
                                content=[
                                    Setting("INITIATIVE_ENABLED", Boolean()),
                                    Setting("INITIATIVE_TITLE"),
                                    Setting("INITIATIVE_IMAGE"),
                                    Setting("INITIATIVE_IMAGE_ALT"),
                                    Setting("INITIATIVE_DESCRIPTION"),
                                    Setting("INITIATIVE_LINK"),
                                ],
                            ),
                            SettingsSection(
                                _("Direct links"),
                                content=[
                                    RawSetting("DIRECT_LINKS", Json()),
                                ],
                            ),
                            SettingsSection(
                                _("Footer"),
                                content=[
                                    RawSetting("FOOTER", Json()),
                                ],
                            ),
                        ],
                    ),
                    SettingsSection(
                        _("Onboarding"),
                        content=[
                            Setting("ONBOARDING_ENABLED", Boolean()),
                            Setting("ONBOARDING_FORCE_EXISTING_USERS", Boolean()),
                            Setting("ONBOARDING_INTRO", TipTap()),
                        ],
                    ),
                    SettingsSection(
                        _("Tags"),
                        content=[
                            TagCategories("TAG_CATEGORIES"),
                            Setting("SHOW_TAGS_IN_FEED", Boolean()),
                            Setting("SHOW_TAGS_IN_DETAIL", Boolean()),
                            TagContentType("PAGE_TAG_FILTERS"),
                            Setting("CUSTOM_TAGS_ENABLED", Boolean()),
                            Setting("SHOW_CUSTOM_TAGS_IN_FEED", Boolean()),
                            Setting("SHOW_CUSTOM_TAGS_IN_DETAIL", Boolean()),
                            TagSynonyms("CUSTOM_TAGS_ENABLED"),
                        ],
                    ),
                    SettingsSection(
                        _("Mailing"),
                        content=[
                            Setting("EMAIL_NOTIFICATION_SHOW_EXCERPT", Boolean()),
                            Setting("EMAIL_OVERVIEW_SUBJECT"),
                            Setting("EMAIL_OVERVIEW_TITLE"),
                            Setting("EMAIL_OVERVIEW_INTRO"),
                            Setting("EMAIL_OVERVIEW_ENABLE_FEATURED", Boolean()),
                            Setting("EMAIL_OVERVIEW_FEATURED_TITLE"),
                            Setting("SITE_MEMBERSHIP_ACCEPTED_SUBJECT"),
                            Setting("SITE_MEMBERSHIP_ACCEPTED_INTRO"),
                            Setting("SITE_MEMBERSHIP_DENIED_SUBJECT"),
                            Setting("SITE_MEMBERSHIP_DENIED_INTRO"),
                        ],
                    ),
                    SettingsSection(
                        _("Advanced"),
                        content=[
                            SettingsSection(
                                _("General"),
                                content=[
                                    Setting("SHOW_UP_DOWN_VOTING", Boolean()),
                                    Setting("ENABLE_SHARING", Boolean()),
                                    Setting("SHOW_VIEW_COUNT", Boolean()),
                                    Setting("SHOW_SUGGESTED_ITEMS", Boolean()),
                                    Setting("COOKIE_CONSENT", Boolean()),
                                    Setting(
                                        "COMMENT_WITHOUT_ACCOUNT_ENABLED",
                                        Boolean(),
                                    ),
                                    Setting("MAX_CHARACTERS_IN_ABSTRACT"),
                                    ListSetting(
                                        "OPEN_FOR_CREATE_CONTENT_TYPES",
                                        ContentType(),
                                    ),
                                    ListSetting(
                                        "REQUIRE_CONTENT_MODERATION_FOR",
                                        ContentType(),
                                    ),
                                    ListSetting(
                                        "REQUIRE_COMMENT_MODERATION_FOR",
                                        ContentType(),
                                    ),
                                    ListSetting("HIDE_CONTENT_OWNER", ContentType()),
                                ],
                            ),
                            SettingsSection(
                                _("Search"),
                                content=[
                                    Setting(
                                        "SEARCH_PUBLISHED_FILTER_ENABLED",
                                        Boolean(),
                                    ),
                                    Setting("RECOMMENDED_TYPE"),
                                    Setting("SEARCH_ARCHIVE_OPTION"),
                                ],
                            ),
                            SettingsSection(
                                pgettext_lazy("plural", "Users"),
                                content=[
                                    Setting("NEWSLETTER", Boolean()),
                                    Setting("EMAIL_OVERVIEW_DEFAULT_FREQUENCY"),
                                    Setting("CANCEL_MEMBERSHIP_ENABLED", Boolean()),
                                ],
                            ),
                            SettingsSection(
                                pgettext_lazy("plural", "News"),
                                content=[
                                    Setting("SHOW_EXCERPT_IN_NEWS_CARD", Boolean()),
                                    Setting("COMMENT_ON_NEWS", Boolean()),
                                ],
                            ),
                            SettingsSection(
                                pgettext_lazy("plural", "Events"),
                                content=[
                                    Setting("EVENT_EXPORT", Boolean()),
                                    Setting("EVENT_TILES", Boolean()),
                                ],
                            ),
                            SettingsSection(
                                pgettext_lazy("plural", "Questions"),
                                content=[
                                    Setting(
                                        "QUESTIONER_CAN_CHOOSE_BEST_ANSWER",
                                        Boolean(),
                                    ),
                                    Setting(
                                        "QUESTION_LOCK_AFTER_ACTIVITY",
                                        Boolean(),
                                    ),
                                    Setting("QUESTION_LOCK_AFTER_ACTIVITY_LINK"),
                                ],
                            ),
                            SettingsSection(
                                pgettext_lazy("plural", "Groups"),
                                content=[
                                    Setting("LIMITED_GROUP_ADD", Boolean()),
                                    Setting("STATUS_UPDATE_GROUPS", Boolean()),
                                    Setting("SUBGROUPS", Boolean()),
                                    Setting("GROUP_MEMBER_EXPORT", Boolean()),
                                ],
                            ),
                            SettingsSection(
                                pgettext_lazy("plural", "Files"),
                                content=[
                                    RawSetting("FILE_OPTIONS", Json()),
                                    Setting("PDF_CHECKER_ENABLED", Boolean()),
                                    Setting("SHOW_ORIGINAL_FILE_NAME", Boolean()),
                                    Setting("PRESERVE_FILE_EXIF", Boolean()),
                                    Setting("COLLAB_EDITING_ENABLED", Boolean()),
                                ],
                            ),
                            SettingsSection(
                                _("Kaltura"),
                                content=[
                                    Setting("KALTURA_VIDEO_ENABLED", Boolean()),
                                    Setting("KALTURA_VIDEO_PARTNER_ID"),
                                    Setting("KALTURA_VIDEO_PLAYER_ID"),
                                ],
                            ),
                            SettingsSection(
                                _("Custom CSS"),
                                content=[
                                    RawSetting("CUSTOM_CSS", Decorator()),
                                    Setting("CUSTOM_CSS_TIMESTAMP", Timestamp()),
                                ],
                            ),
                            SettingsSection(
                                _("Security.txt"),
                                content=[
                                    Setting("SECURITY_TEXT_ENABLED", Boolean()),
                                    Setting("SECURITY_TEXT"),
                                    Setting("SECURITY_TEXT_PGP"),
                                    Setting(
                                        "SECURITY_TEXT_REDIRECT_ENABLED",
                                        Boolean(),
                                    ),
                                    Setting("SECURITY_TEXT_REDIRECT_URL"),
                                ],
                            ),
                        ],
                    ),
                ],
            ),
            SettingsSection(
                _("Profile"),
                content=[
                    UserProfile("PROFILE"),
                    ProfileSections("PROFILE_SECTIONS"),
                ],
            ),
            SettingsSection(
                _("Tools"),
                content=[
                    SettingsSection(
                        _("Flow"),
                        content=[
                            Setting("FLOW_ENABLED", Boolean()),
                            ListSetting("FLOW_SUBTYPES", ContentType()),
                            Setting("FLOW_APP_URL"),
                            Setting("FLOW_TOKEN"),
                            Setting("FLOW_CASE_ID"),
                            Setting("FLOW_USER_GUID"),
                        ],
                    ),
                    SettingsSection(
                        _("Redirects"),
                        content=[
                            KeyValue("REDIRECTS"),
                        ],
                    ),
                    SettingsSection(
                        _("Profile Sync"),
                        content=[
                            Setting("PROFILE_SYNC_ENABLED", Boolean()),
                            Setting("PROFILE_SYNC_TOKEN"),
                        ],
                    ),
                ],
            ),
            SettingsSection(
                _("Otherwise hidden settings"),
                [
                    Setting("BACKEND_VERSION"),
                    Setting("LAST_RECEIVED_BOUNCING_EMAIL"),
                    Setting("LAST_RECEIVED_DELETED_USER"),
                ],
            ),
        ],
    )


ALREADY_CONTROLLED_BY_CONTROL_KEYS = [
    "CHAT_ENABLED",
    "COLLAB_EDITING_ENABLED",
    "COLLAB_EDITING_ENABLED",
    "COMMENT_MODERATION_ENABLED",
    "CONTENT_MODERATION_ENABLED",
    "CONTENT_TRANSLATION",
    "CSP_HEADER_EXCEPTIONS",
    "CUSTOM_JAVASCRIPT",
    "DAILY_SNAPSHOT_ENABLED",
    "DATAHUB_EXTERNAL_CONTENT_ENABLED",
    "EDIT_USER_NAME_ENABLED",
    "EMAIL_DISABLED",
    "EVENT_ADD_EMAIL_ATTENDEE",
    "GET_CREATE_GROUP_MEMBERS",
    "INTEGRATED_VIDEOCALL_ENABLED",
    "MAIL_REPLY_TO",
    "OIDC_PROVIDERS",
    "ONLINEAFSPRAKEN_ENABLED",
    "ONLINEAFSPRAKEN_KEY",
    "ONLINEAFSPRAKEN_SECRET",
    "ONLINEAFSPRAKEN_URL",
    "PREVENT_INSERT_MEDIA_AT_QUESTIONS",
    "PROFILE",
    "PUSH_NOTIFICATIONS_ENABLED",
    "RECURRING_EVENTS_ENABLED",
    "RICH_TEXT_EDITOR_FEATURES",
    "SEARCH_RELATED_SCHEMAS",
    "SITE_CATEGORY",
    "SITE_NOTES",
    "SITE_PLAN",
    "SKIP_CLAMAV",
    "SUPPORT_CONTRACT_ENABLED",
    "SUPPORT_CONTRACT_HOURS_REMAINING",
    "TENANT_API_TOKEN",
    "THEME_OPTIONS",
    "VIDEOCALL_API_URL",
    "VIDEOCALL_APPOINTMENT_TYPE",
    "VIDEOCALL_ENABLED",
    "VIDEOCALL_PROFILEPAGE",
    "VIDEOCALL_THROTTLE",
]
