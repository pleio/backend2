import csv
import logging
import mimetypes
import os
from datetime import timedelta
from io import StringIO
from wsgiref.util import FileWrapper

from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.http import FileResponse, HttpResponseNotFound, StreamingHttpResponse
from django.shortcuts import redirect, render, reverse
from django.utils import timezone
from django_tenants.utils import schema_context, tenant_context
from post_deploy.models import PostDeployLog

from control.forms import AgreementAddForm, AgreementAddVersionForm
from control.models import (
    AccessCategory,
    AccessLog,
    Configuration,
    ElasticsearchStatus,
    SiteFilter,
    Snapshot,
    Task,
)
from core import config
from core.constances import SiteCategoryChoices, SitePlanChoices
from core.lib import datetime_format, get_base_url, safe_file_path
from core.lib import get_full_url as subsite_url
from entities.file.models import FileFolder, FileReference
from tenants.models import Agreement, AgreementVersion, Client
from user.models import User

from ..utils.post_deploy import serialize_log_record
from .sites_crud import (
    SiteBackupView,
    SiteDeleteView,
    SiteDetailsView,
    SiteDisableView,
    SiteEnableView,
    SiteMigrateStorageView,
    SiteReportView,
    SitesAddView,
    SiteSnapshotView,
    SiteUpdateView,
    UpdateProfileView,
)
from .sites_log import (
    AuditLogView,
    DeployTaskLogView,
    FloodLogView,
    LogDashboardView,
    MailLogView,
    ScanLogView,
    SearchIndexLogView,
)

logger = logging.getLogger(__name__)


def is_admin(user):
    return user.is_superadmin


@login_required
@user_passes_test(is_admin)
def home(request):
    yesterday = timezone.now() - timedelta(days=1)
    # count snapshots yesterday with status finished
    snapshots_yesterday_finisedh_count = Snapshot.objects.filter(
        started_at__date=yesterday, status=Snapshot.Status.FINISHED
    ).count()
    # count snapshots yesterday
    snapshots_yesterday_total = Snapshot.objects.filter(
        started_at__date=yesterday
    ).count()

    # snapshots yesterday with status not finished
    snapshots_yesterday_not_finished = Snapshot.objects.filter(
        started_at__date=yesterday,
        status__in=[Snapshot.Status.FAILED, Snapshot.Status.STARTED],
    )

    context = {
        "name": settings.ENV,
        "site_count": Client.objects.exclude(schema_name="public")
        .filter(is_active=True)
        .count(),
        "snapshots_yesterday_finisedh_count": snapshots_yesterday_finisedh_count,
        "snapshots_yesterday_total": snapshots_yesterday_total,
        "snapshots_yesterday_not_finished": snapshots_yesterday_not_finished,
    }

    return render(request, "home.html", context)


@login_required
@user_passes_test(is_admin)
def sites(request):
    qs = Client.objects.exclude(schema_name="public").order_by("-created_on")
    f = SiteFilter(request.GET, queryset=qs)

    paginator = Paginator(qs.filter(id__in=f.qs.values_list("id")), 50)

    page = request.GET.get("page", 1)
    try:
        sites = paginator.page(page)
    except PageNotAnInteger:
        sites = paginator.page(1)
    except EmptyPage:
        sites = paginator.page(paginator.num_pages)

    context = {
        "filter": f,
        "sites": sites,
    }

    return render(request, "sites/index.html", context)


@login_required
@user_passes_test(is_admin)
def download_backup(request, site_id, backup_name):
    try:
        site = Client.objects.get(id=site_id)

        assert backup_name.endswith(f"_{site.schema_name}.zip"), (
            "Invalid backup file name"
        )

        filepath = safe_file_path(settings.BACKUP_PATH, backup_name)
        if not os.path.isfile(filepath):
            return HttpResponseNotFound()

        AccessLog.objects.create(
            type=AccessLog.AccessTypes.DOWNLOAD,
            category=AccessLog.custom_category(AccessCategory.SITE_BACKUP, site_id),
            user=auth.get_user(request),
            item_id=backup_name,
            site=site,
        )

        chunk_size = 8192
        response = FileResponse(
            FileWrapper(open(filepath, "rb"), chunk_size),
            content_type=mimetypes.guess_type(filepath)[0],
        )
        response["Content-Length"] = os.path.getsize(filepath)
        response["Content-Disposition"] = "attachment; filename=%s" % os.path.basename(
            filepath
        )

        return response
    except Client.DoesNotExist:
        return HttpResponseNotFound("Client does not exist")
    except AssertionError as e:
        return HttpResponseNotFound(e)


@login_required
@user_passes_test(is_admin)
def tasks(request):
    page = request.GET.get("page", 1)

    paginator = Paginator(Task.objects.all(), 50)
    try:
        tasks = paginator.page(page)
    except PageNotAnInteger:
        tasks = paginator.page(1)
    except EmptyPage:
        tasks = paginator.page(paginator.num_pages)

    context = {
        "tasks": tasks,
    }

    return render(request, "tasks.html", context)


@login_required
@user_passes_test(is_admin)
def download_site_admins(request):
    clients = Client.objects.exclude(schema_name="public", is_active=True)

    admins = []
    for client in clients:
        with tenant_context(client):
            users = User.objects.filter(roles__contains=["ADMIN"], is_active=True)
            for user in users:
                admins.append(
                    {
                        "name": user.name,
                        "email": user.email,
                        "client_id": client.id,
                        "client_domain": client.primary_domain,
                        "created_at": datetime_format(user.created_at),
                    }
                )

    def stream():
        buffer = StringIO()
        writer = csv.writer(buffer, delimiter=";", quotechar='"')
        writer.writerow(["name", "email", "site", "created_at"])
        yield buffer.getvalue()

        for admin in admins:
            buffer = StringIO()
            writer = csv.writer(buffer, delimiter=";", quotechar='"')
            writer.writerow(
                [
                    admin["name"],
                    admin["email"],
                    admin["client_domain"],
                    admin["created_at"],
                ]
            )
            yield buffer.getvalue()

    response = StreamingHttpResponse(stream(), content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="site_admins.csv"'

    return response


@login_required
@user_passes_test(is_admin)
def download_site_owners(request):
    clients = Client.objects.exclude(schema_name="public", is_active=True)

    admins = []
    for client in clients:
        with tenant_context(client):
            admins.append(
                {
                    "name": client.administrative_details.get("site_owner"),
                    "email": client.administrative_details.get("site_owner_email"),
                    "category": (
                        SiteCategoryChoices[config.SITE_CATEGORY]
                        if config.SITE_CATEGORY
                        else ""
                    ),
                    "plan": (
                        SitePlanChoices[config.SITE_PLAN] if config.SITE_PLAN else ""
                    ),
                    "site": get_base_url(),
                }
            )

    def stream():
        buffer = StringIO()
        writer = csv.writer(buffer, delimiter=";", quotechar='"')
        writer.writerow(["name", "email", "category", "plan", "site"])
        yield buffer.getvalue()

        for admin in admins:
            buffer = StringIO()
            writer = csv.writer(buffer, delimiter=";", quotechar='"')
            writer.writerow(
                [
                    admin["name"],
                    admin["email"],
                    admin["category"],
                    admin["plan"],
                    admin["site"],
                ]
            )
            yield buffer.getvalue()

    response = StreamingHttpResponse(stream(), content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="site_contact_persons.csv"'

    return response


def orphaned_files(request, site_id):
    try:
        site = Client.objects.get(id=site_id)
        with tenant_context(site):
            from entities.file.models import FileFolder

            orphaned_files = FileFolder.objects.filter_orphaned_files()
            orphaned_files = orphaned_files.order_by("-last_action")

            return render(
                request,
                "sites/orphaned_files.html",
                context={
                    "files": orphaned_files,
                    "site_id": site_id,
                    "site": site,
                    "site_name": config.NAME,
                },
            )
    except Client.DoesNotExist:
        return HttpResponseNotFound("Client does not exist")


@login_required
@user_passes_test(is_admin)
def search_user(request):
    data = []
    if email := request.GET.get("email"):
        clients = Client.objects.exclude(schema_name="public")
        external_ids = set()

        # Get corresponding external ids for the given email address.
        for client in clients:
            with tenant_context(client):
                user = User.objects.filter(email=email, is_active=True).first()
                if user and user.external_id:
                    external_ids.add(user.external_id)

        # Search for users with the given email address or external id.
        for client in clients:
            with tenant_context(client):
                users = User.objects.filter(is_active=True)
                if external_ids:
                    users = users.filter(
                        Q(email=email) | Q(external_id__in=external_ids)
                    )
                else:
                    users = users.filter(email=email)

                if user := users.first():
                    data.append(
                        {
                            "user_name": user.name,
                            "user_email": user.email,
                            "user_external_id": user.external_id,
                            "id": client.id,
                            "schema": client.schema_name,
                            "domain": client.primary_domain,
                            "url": get_base_url(),
                            "last_login": user.last_login,
                            "sort_order": user.last_login or user.created_at,
                        }
                    )

    return render(
        request,
        "tools/search_user.html",
        {"data": sorted(data, key=lambda x: x["sort_order"], reverse=True)},
    )


@login_required
@user_passes_test(is_admin)
def agreements(request, cluster_id=None):
    context = {"agreements": Agreement.objects.all()}

    return render(request, "agreements.html", context)


@login_required
@user_passes_test(is_admin)
def agreements_add(request):
    if request.method == "POST":
        form = AgreementAddForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            name = data.get("name")
            Agreement.objects.create(name=name, description="")

            messages.info(request, f"Added agreement {name}")

            return redirect(reverse("agreements"))
    else:
        form = AgreementAddForm(initial={})

    context = {"form": form}

    return render(request, "agreements_add.html", context)


@login_required
@user_passes_test(is_admin)
def agreements_add_version(request, agreement_id):
    if not Agreement.objects.filter(id=agreement_id).exists():
        return HttpResponseNotFound("Agreement not found")

    if request.method == "POST":
        form = AgreementAddVersionForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data

            version = data.get("version")
            agreement_id = data.get("agreement_id")
            uploaded_file = request.FILES["document"]

            agreement = Agreement.objects.get(id=agreement_id)
            version = AgreementVersion.objects.create(
                agreement=agreement, version=version, document=uploaded_file
            )

            messages.info(
                request,
                "Added version %s to agreement %s" % (version.version, agreement.name),
            )

            return redirect(reverse("agreements"))
    else:
        form = AgreementAddVersionForm(initial={"agreement_id": agreement_id})

    context = {"form": form}

    return render(request, "agreements_add.html", context)


@login_required
@user_passes_test(is_admin)
def elasticsearch_status(request):
    clients = Client.objects.exclude(schema_name="public")
    rows = []
    for client in clients:
        last_record = (
            ElasticsearchStatus.objects.order_by("-created_at")
            .filter(client=client)
            .first()
        )
        status = {}
        with schema_context(client.schema_name):
            status["name"] = config.NAME
        status["url"] = reverse("site_elasticsearch", args=[client.id])

        if last_record:
            status["created_at"] = last_record.created_at
            status["index_status"] = last_record.index_status_summary()
            status["details_url"] = reverse("elasticsearch_status", args=[client.id])
        rows.append(status)

    # render summary of all sites with elasticsearch issues
    return render(request, "tools/elasticsearch_summary.html", {"rows": rows})


@login_required
@user_passes_test(is_admin)
def elasticsearch_status_details(request, client_id, record_id=None):
    client = Client.objects.get(id=client_id)
    if not record_id:
        record = ElasticsearchStatus.objects.filter(client=client).first()
    else:
        record = ElasticsearchStatus.objects.get(client=client, id=record_id)
    if not record:
        return HttpResponseNotFound("Invalid parameters")

    with schema_context(client.schema_name):
        from core import config

        site_name = config.NAME
        site_url = subsite_url("/login") + "?next=/superadmin/tasks"

    context = {
        "record": record,
        "site_name": site_name,
        "site_url": site_url,
        "site_elasticsearch": reverse("site_elasticsearch", args=[client.id]),
        "previous": ElasticsearchStatus.objects.previous(
            record.client, record.created_at
        ),
        "next": ElasticsearchStatus.objects.next(record.client, record.created_at),
    }

    # render elasticsearch status of one site
    return render(request, "tools/elasticsearch_details.html", context)


def avatar_permission_status(request):
    context = {"client_status_list": []}
    for client in Client.objects.exclude(schema_name="public"):
        with schema_context(client.schema_name):
            from core import config

            qs = FileReference.objects.filter(configuration__startswith="user")
            context["client_status_list"].append(
                {
                    "site_name": config.NAME,
                    "all": qs.count(),
                    "private": qs.exclude(
                        file__read_access__icontains="public"
                    ).count(),
                    "public": qs.filter(file__read_access__icontains="public").count(),
                    "personal": qs.filter(file__read_access__icontains="user:").count(),
                }
            )
    return render(request, "tools/avatar_permission_status.html", context)


def security_txt(request):
    storage = Configuration.objects.filter(id="security_txt").first()
    if not storage:
        storage = Configuration(id="security_txt")

    if request.method == "POST":
        if "button_apply" in request.POST:
            messages.info(request, "Text updated.")
            storage.value = request.POST.get("content")
            storage.save()
        return redirect(reverse("security_txt"))

    return render(request, "tools/security_txt.html", {"storage": storage})


@login_required
@user_passes_test(is_admin)
def stats_container_url(request):
    KEY = "piwik_container_url_https://stats.pleio.nl/"
    storage = Configuration.objects.filter(id=KEY).first()
    if not storage:
        storage = Configuration(id=KEY)

    if request.method == "POST":
        if "button_apply" in request.POST:
            messages.info(request, "Stats container url updated.")
            storage.value = request.POST.get("content")
            storage.save()
        return redirect(reverse("stats_container_url"))

    return render(request, "tools/stats_container_url.html", {"storage": storage})


@login_required
@user_passes_test(is_admin)
def embed_media(request, schema_name, file_id):
    with schema_context(schema_name):
        file = FileFolder.objects.get(id=file_id)
        chunk_size = 8192
        response = FileResponse(
            FileWrapper(file.upload.open(), chunk_size),
            content_type=mimetypes.guess_type(file.upload.name)[0],
        )
        response["Content-Length"] = file.upload.size
        return response


@login_required
@user_passes_test(is_admin)
def post_deploy_report(request):
    def _site_summary(site):
        with tenant_context(site):
            return {
                "site": site,
                "site_name": "Public" if site.schema_name == "public" else config.NAME,
                "records": [
                    serialize_log_record(record)
                    for record in PostDeployLog.objects.filter(
                        Q(message__isnull=False) | Q(completed_at__isnull=True)
                    )
                ],
            }

    deploy_tasks = {}
    for site in Client.objects.all():
        site_summary = _site_summary(site)
        for record in site_summary["records"]:
            key = "failed" if record["completed"] else "busy"
            if record["task"] not in deploy_tasks:
                deploy_tasks[record["task"]] = {"busy": [], "failed": []}
            deploy_tasks[record["task"]][key].append(
                {
                    "site": site,
                    "record": record,
                }
            )

    def _report_weight(item):
        task, report = item
        return 0 - len(report["busy"])

    context_data = {"task_summary": sorted(deploy_tasks.items(), key=_report_weight)}
    return render(request, "tools/post_deploy_report.html", context_data)
