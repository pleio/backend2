from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponseNotFound
from django.utils.translation import gettext as _
from django_tenants.utils import tenant_context

from core import config
from core.lib import get_base_url
from tenants.models import Client


class SecureControlViewMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_authenticated and self.request.user.is_superadmin


class SiteDetailsViewMixin(SecureControlViewMixin):
    def __init__(self, *args, **kwargs):
        self.site = None
        super().__init__(*args, **kwargs)

    def get_site(self, site_id):
        self.site = (
            Client.objects.exclude(schema_name="public").filter(id=site_id).first()
        )

    def dispatch(self, request, *args, **kwargs):
        try:
            self.get_site(kwargs.get("site_id"))
            assert self.site, _("Site not found")

            return super().dispatch(request, *args, **kwargs)
        except (AssertionError, ObjectDoesNotExist, ValidationError):
            return HttpResponseNotFound()

    def get_context_data(self, **kwargs):
        with tenant_context(self.site):
            return {
                **super().get_context_data(**kwargs),
                "site": self.site,
                "site_id": self.site.id,
                "schema_name": self.site.schema_name,
                "site_url": get_base_url(),
                "site_name": config.NAME,
            }
