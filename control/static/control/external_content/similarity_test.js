(function ($) {
    $(".button-placeholder").each((i, element) => {
        const placeholder = $(element);
        const title = placeholder.data('button-title');
        const entity_id = placeholder.data('entity-id');
        const button = $(`<button data-fetch-common-content-for="${entity_id}" class="btn btn-primary">${title}</button>`);
        const target_div = $(`<div data-result-target-for="${entity_id}"></div>`);
        button.click((element) => {
            const entity_id = button.data('fetch-common-content-for');
            const target_div = $(`div[data-result-target-for="${entity_id}"]`);
            const url = `${window.location.pathname}`;
            target_div.html("Loading...");
            $.post(url, {entity_id}, (data) => {
                target_div.html(data);
            });
        })
        placeholder.html("");
        placeholder.append(button);
        placeholder.append(target_div);
    });
})(jQuery);
