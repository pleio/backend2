(function ($) {
    /**
     * Respond to changes of the category_tag select box
     */
    $("#id_category_tag").change(function () {
        const category_tag = $(this).val();
        const [category, tag] = category_tag.split('|');

        $("#id_category").val(category);
        $("#id_tag").val(tag);
    });
})(jQuery);
