(function () {
    /**
     * Fix side effects from form/field/render_field.html
     *
     * Too many form-control classes are added.
     *
     * @TODO: Update this quick and dirty fix to a better solution.
     *   A better solution would be add the proper classes at render time.
     */
    let radioButtons = $('.form-control input[type=radio].form-control, input[type=checkbox].form-control');

    radioButtons.each(function (index, radioButton) {
        $(this).parents('.form-control').removeClass('form-control');
        $(this).removeClass('form-control');
        $(this).addClass('form-check-input');
    })

    $('[data-effect-mapping]').each(function (index, element) {
        const mapping = $(this).data('effect-mapping');
        const source_id = $(this).data('effect-source');
        const target_id = $(this).data('effect-target');
        const source = $(`#${source_id}`);
        const target = $(`#${target_id}`);
        source.change(function () {
            const value = source.val();
            if (mapping[value]) {
                target.val(mapping[value]);
            }
        });
    })
})();
