from django.urls import path

from control import views
from control.views import profile_forms as profile_form_views
from control.views import user_sync_views
from control.views.configuration_views import site_custom_configuration_view

urlpatterns = [
    path("", views.home, name="home"),
    path("sites/", views.sites, name="site_index"),
    path("sites/add/", views.SitesAddView.as_view(), name="site_add"),
    path("site/<int:site_id>/", views.SiteDetailsView.as_view(), name="site_details"),
    path(
        "site/<int:site_id>/update/", views.SiteUpdateView.as_view(), name="site_update"
    ),
    path(
        "site/<int:site_id>/elasticsearch/",
        profile_form_views.ElasticsearchView.as_view(),
        name="site_elasticsearch",
    ),
    path(
        "site/<int:site_id>/elasticsearch/diagnostics/",
        profile_form_views.ElasticsearchDiagnosticsView.as_view(),
        name="site_elasticsearch_index_diagnostics",
    ),
    path(
        "site/<int:site_id>/orphaned_files/",
        views.orphaned_files,
        name="site_orphaned_files",
    ),
    path(
        "site/<int:site_id>/backgroundtasks/",
        profile_form_views.BackgroundTasksView.as_view(),
        name="site_backgroundtasks",
    ),
    path(
        "site/<int:site_id>/copy_group/",
        profile_form_views.CopyGroupView.as_view(),
        name="site_copy_group",
    ),
    path(
        "site/<int:site_id>/backup/", views.SiteBackupView.as_view(), name="site_backup"
    ),
    path(
        "site/<int:site_id>/snapshot/",
        views.SiteSnapshotView.as_view(),
        name="site_snapshot",
    ),
    path(
        "site/<int:site_id>/delete/", views.SiteDeleteView.as_view(), name="site_delete"
    ),
    path(
        "site/<int:site_id>/logs/",
        views.LogDashboardView.as_view(),
        name="site_log_dashboard",
    ),
    path(
        "site/<int:site_id>/logs/auditlog/",
        views.AuditLogView.as_view(),
        name="site_log_auditlog",
    ),
    path(
        "site/<int:site_id>/logs/post_deploy_report/",
        views.DeployTaskLogView.as_view(),
        name="site_post_deploy_report",
    ),
    path(
        "site/<int:site_id>/logs/scanlog/",
        views.ScanLogView.as_view(),
        name="site_log_scanlog",
    ),
    path(
        "site/<int:site_id>logs/search_index/",
        views.SearchIndexLogView.as_view(),
        name="search_index_log",
    ),
    path(
        "site/<int:site_id>/logs/mail_log/",
        views.MailLogView.as_view(),
        name="mail_log",
    ),
    path(
        "site/<int:site_id>/logs/floodlog/",
        views.FloodLogView.as_view(),
        name="floodlog",
    ),
    path(
        "site/<int:site_id>/migrate/",
        views.SiteMigrateStorageView.as_view(),
        name="site_migrate",
    ),
    path(
        "site/<int:site_id>/disable/",
        views.SiteDisableView.as_view(),
        name="site_disable",
    ),
    path(
        "site/<int:site_id>/enable/", views.SiteEnableView.as_view(), name="site_enable"
    ),
    path(
        "site/<int:site_id>/reports/<str:report_id>/",
        views.SiteReportView.as_view(),
        name="site_report",
    ),
    path(
        "site/embed_media/<str:schema_name>/<str:file_id>",
        views.embed_media,
        name="embed_media",
    ),
    path(
        "site/<int:site_id>/profile/",
        views.UpdateProfileView.as_view(),
        name="site_profile",
    ),
    path(
        "site/<int:site_id>/profile/custom_agreement/add",
        profile_form_views.AddAgreementView.as_view(),
        name="site_profile_add_custom_agreement",
    ),
    path(
        "site/<int:site_id>/profile/custom_agreement/<str:agreement_id>/delete",
        profile_form_views.DeleteAgreementView.as_view(),
        name="site_profile_delete_custom_agreement",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/add",
        profile_form_views.AddExternalContentSourceView.as_view(),
        name="site_profile_external_content_add_source",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/edit",
        profile_form_views.ExternalContentSourceView.as_view(),
        name="site_profile_external_content_rss_source",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/settings",
        profile_form_views.ExternalContentRssSourceDownloadSettingsView.as_view(),
        name="site_profile_external_content_rss_source_settings",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/filter/add/",
        profile_form_views.ExternalContentRssFilterAddView.as_view(),
        name="site_profile_external_content_rss_filter_add",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/filter/<str:filter_id>/edit/",
        profile_form_views.ExternalContentRssFilterEditView.as_view(),
        name="site_profile_external_content_rss_filter_edit",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/content/",
        profile_form_views.ExternalContentRssSourceContentView.as_view(),
        name="site_profile_external_content_rss_content",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/filter/<str:filter_id>/delete/",
        profile_form_views.ExternalContentRssFilterDeleteView.as_view(),
        name="site_profile_external_content_rss_filter_delete",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/tag_image/add/",
        profile_form_views.ExternalContentTagImageAddView.as_view(),
        name="site_profile_external_content_tag_image_add",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/tag_image/<str:tag_id>/edit/",
        profile_form_views.ExternalContentTagImageEditView.as_view(),
        name="site_profile_external_content_tag_image_edit",
    ),
    path(
        "site/<int:site_id>/profile/external_content_source/<str:source_id>/tag_image/<str:tag_id>/delete/",
        profile_form_views.ExternalContentTagImageDeleteView.as_view(),
        name="site_profile_external_content_tag_image_delete",
    ),
    path(
        "site/<int:site_id>/profile/html_head/",
        profile_form_views.HtmlHeadView.as_view(),
        name="site_profile_html_head",
    ),
    path(
        "site/<int:site_id>/profile/login_options/",
        profile_form_views.LoginOptionsView.as_view(),
        name="site_profile_login_options",
    ),
    path(
        "site/<int:site_id>/profile/meetings_settings/",
        profile_form_views.MeetingsSettingsView.as_view(),
        name="site_profile_meetings_settings",
    ),
    path(
        "site/<int:site_id>/profile/optional_features/",
        profile_form_views.OptionalFeaturesView.as_view(),
        name="site_profile_optional_features",
    ),
    path(
        "site/<int:site_id>/profile/profilefieldset/<str:action>",
        profile_form_views.ProfileFieldsetView.as_view(),
        name="site_profile_profilefieldset",
    ),
    path(
        "site/<int:site_id>/profile/rich_text_editor_features/",
        profile_form_views.RichTextEditorFeaturesView.as_view(),
        name="site_profile_rich_text_editor_features",
    ),
    path(
        "site/<int:site_id>/custom_configuration/",
        site_custom_configuration_view,
        name="site_custom_configuration",
    ),
    path("tasks/", views.tasks, name="tasks"),
    path(
        "site/<int:site_id>/download-backup/<str:backup_name>",
        views.download_backup,
        name="download_backup",
    ),
    path(
        "tools/download_site_admins/",
        views.download_site_admins,
        name="download_site_admins",
    ),
    path(
        "tools/download_site_owners/",
        views.download_site_owners,
        name="download_site_owners",
    ),
    path(
        "tools/security-txt/",
        views.security_txt,
        name="security_txt",
    ),
    path(
        "tools/stats_container_url",
        views.stats_container_url,
        name="stats_container_url",
    ),
    path("tools/search_user/", views.search_user, name="search_user"),
    path(
        "tools/post_deploy_report/",
        views.post_deploy_report,
        name="post_deploy_report",
    ),
    path("agreements/", views.agreements, name="agreements"),
    path("agreements/add/", views.agreements_add, name="agreement_add"),
    path(
        "agreements/<int:agreement_id>/add/",
        views.agreements_add_version,
        name="agreement_add_version",
    ),
    path(
        "tools/elasticsearch/<int:client_id>/<int:record_id>/",
        views.elasticsearch_status_details,
        name="elasticsearch_status",
    ),
    path(
        "tools/elasticsearch/<int:client_id>/",
        views.elasticsearch_status_details,
        name="elasticsearch_status",
    ),
    path(
        "tools/elasticsearch/", views.elasticsearch_status, name="elasticsearch_status"
    ),
    path(
        "tools/avatar_permission_status/",
        views.avatar_permission_status,
        name="avatar_permission_status",
    ),
    path(
        "tools/user_sync/",
        user_sync_views.UserSyncIndexView.as_view(),
        name="user_sync_index",
    ),
    path(
        "tools/user_sync/<str:controller_id>/",
        user_sync_views.UserSyncDetailView.as_view(),
        name="user_sync_detail",
    ),
]
