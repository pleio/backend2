import logging
import uuid
from enum import Enum

import django_filters
from celery import chain, signature
from celery.result import AsyncResult
from celery.utils.time import make_aware
from django.conf import settings
from django.db import models
from django.forms import Select, TextInput
from django.utils import timezone
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy
from django_tenants.utils import schema_context
from pytz import utc

from core.lib import tenant_schema
from tenants.models import Client

logger = logging.getLogger(__name__)


class SiteFilter(django_filters.FilterSet):
    def __init__(self, data=None, *args, **kwargs):
        # if filterset is bound, use initial values as defaults
        if data is not None:
            # get a mutable copy of the QueryDict
            data = data.copy()

            for name, f in self.base_filters.items():
                initial = f.extra.get("initial")

                # filter param is either missing or empty, use initial as default
                if not data.get(name) and initial:
                    data[name] = initial

        super().__init__(data, *args, **kwargs)

    domain = django_filters.CharFilter(
        field_name="domains__domain",
        lookup_expr="icontains",
        widget=TextInput(attrs={"class": "form-control", "placeholder": _("Domain")}),
    )

    schema_name = django_filters.CharFilter(
        field_name="schema_name",
        lookup_expr="icontains",
        widget=TextInput(attrs={"class": "form-control", "placeholder": _("Schema")}),
    )

    is_active = django_filters.BooleanFilter(
        widget=Select(
            attrs={"class": "form-control"},
            choices=[(True, _("Active websites")), (False, _("Deactivated websites"))],
        ),
        initial=True,
    )

    storage_options = list(settings.STORAGES.keys())
    storage_options.remove("default")
    storage_options.remove("staticfiles")
    choices = [(item, item) for item in storage_options]
    choices.insert(0, (None, "---"))

    custom_file_storage = django_filters.CharFilter(
        widget=Select(
            attrs={"class": "form-control"},
            choices=choices,
        ),
    )

    class Meta:
        model = Client
        fields = ["schema_name", "domain", "is_active", "custom_file_storage"]


class TaskManager(models.Manager):
    def create_task(self, name, arguments=None, **kwargs):
        task_id = uuid.uuid4()
        chain(
            signature(name, arguments).set(task_id=str(task_id)),
            signature("control.tasks.followup_task_complete"),
        ).apply_async(link_error=signature("control.tasks.followup_task_complete"))

        task = self.model(
            task_id=task_id,
            state="PENDING",
            name=name,
            arguments=arguments,
            **kwargs,
        )

        task.save()

        return task


class Task(models.Model):
    STATE_TYPES = (
        ("PENDING", "PENDING"),
        ("STARTED", "STARTED"),
        ("RETRY", "RETRY"),
        ("FAILURE", "FAILURE"),
        ("SUCCESS", "SUCCESS"),
    )

    objects = TaskManager()

    class Meta:
        ordering = ["-created_at"]

    author = models.ForeignKey(
        "user.User", null=True, on_delete=models.CASCADE, related_name="tasks"
    )
    client = models.ForeignKey(
        "tenants.Client", null=True, on_delete=models.CASCADE, related_name="tasks"
    )

    task_id = models.CharField(max_length=255)
    name = models.CharField(max_length=255)

    origin = models.CharField(max_length=255, default="")
    followup = models.CharField(max_length=255, null=True)

    arguments = models.JSONField(null=True, blank=True)
    response = models.JSONField(null=True, blank=True)

    state = models.CharField(max_length=16, choices=STATE_TYPES, default="PENDING")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def run_followup(self):
        try:
            if not self.followup:
                return

            followup = import_string(self.followup)
            followup.delay(self.id)
        except Exception:
            pass


class AccessCategory(Enum):
    SITE_BACKUP = _("Website backup")


class AccessLogManager(models.Manager):
    """Separate Manager for testing purposes."""


class AccessLog(models.Model):
    objects = AccessLogManager()

    class Meta:
        ordering = ("-created_at",)

    class AccessTypes(models.TextChoices):
        CREATE = "create", pgettext_lazy("Past Perfect", "Created")
        READ = "read", pgettext_lazy("Past Perfect", "Read")
        DOWNLOAD = "download", pgettext_lazy("Past Perfect", "Downloaded")
        UPDATE = "update", pgettext_lazy("Past Perfect", "Updated")
        DELETE = "delete", pgettext_lazy("Past Perfect", "Deleted")

    created_at = models.DateTimeField(default=timezone.now)
    category = models.CharField(max_length=256)
    user = models.ForeignKey("user.User", on_delete=models.CASCADE)
    item_id = models.CharField(max_length=256)
    type = models.CharField(max_length=128, choices=AccessTypes.choices)
    site = models.ForeignKey(
        "tenants.Client", on_delete=models.CASCADE, default=None, null=True, blank=True
    )

    @staticmethod
    def custom_category(category, suffix):
        return "%s:%s" % (category.name, suffix)

    @property
    def type_label(self):
        return AccessLog.AccessTypes(self.type).label


class ElasticsearchStatusManager(models.Manager):
    def cleanup(self, **filter_kwargs):
        existing = self.get_queryset().filter(**filter_kwargs)
        for item in existing[2:]:
            item.delete()

    def previous(self, client, reference_date):
        qs = self.get_queryset()
        qs = qs.filter(client=client, created_at__lt=reference_date)
        return qs.first()

    def next(self, client, reference_date):
        qs = self.get_queryset().order_by("created_at")
        qs = qs.filter(client=client, created_at__gt=reference_date)
        return qs.first()


class ElasticsearchStatus(models.Model):
    class Meta:
        ordering = ("-created_at",)

    objects = ElasticsearchStatusManager()

    client = models.ForeignKey(
        "tenants.Client",
        null=True,
        on_delete=models.CASCADE,
        related_name="elasticsearch_status",
    )
    index_status = models.JSONField(null=True)
    access_status = models.JSONField(null=True)
    created_at = models.DateTimeField(default=timezone.now)

    def index_status_summary(self):
        if self.index_status.get("result"):
            return _("Index not up to date")
        if "message" in self.index_status:
            return self.index_status["message"]
        return ""


class FileOperationManager(models.Manager):
    def add_log(self, operation, result):
        current_schema = tenant_schema()

        with schema_context("public"):
            self.create(
                client=Client.objects.get(schema_name=current_schema),
                operation=operation,
                result=result,
            )


class FileOperationLog(models.Model):
    class Meta:
        ordering = (
            "operation",
            "-created_at",
        )

    objects = FileOperationManager()

    created_at = models.DateTimeField(default=timezone.now)
    client = models.ForeignKey(
        "tenants.Client",
        null=True,
        on_delete=models.CASCADE,
        related_name="file_operation_log",
    )
    operation = models.CharField(max_length=255)
    result = models.JSONField(default=dict)


class Configuration(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    value = models.TextField()


class Snapshot(models.Model):
    class Meta:
        ordering = ("-started_at",)

    class Status(models.TextChoices):
        STARTED = "started", pgettext_lazy("Past Perfect", "Started")
        FINISHED = "finished", pgettext_lazy("Past Perfect", "Finished")
        FAILED = "failed", pgettext_lazy("Past Perfect", "Failed")

    client = models.ForeignKey(
        "tenants.Client",
        null=True,
        on_delete=models.CASCADE,
        related_name="snapshots",
    )
    started_at = models.DateTimeField(default=timezone.now)
    finished_at = models.DateTimeField(null=True, blank=True)
    status = models.CharField(
        max_length=255, default=Status.STARTED, choices=Status.choices
    )
    result = models.JSONField(default=dict)
    hash = models.CharField(max_length=255, null=True, blank=True)

    @property
    def duration(self):
        if self.finished_at:
            return self.finished_at - self.started_at
        return None

    @property
    def snapshot_id(self):
        if self.result and self.result.get("message"):
            return self.result.get("message").get("snapshot_id")
        return ""

    @property
    def error_message(self):
        if self.result and self.result.get("error"):
            return self.result.get("error")
        return ""

    def set_error(self, message):
        self.result = {"error": message}
        self.finished_at = timezone.now()
        self.status = Snapshot.Status.FAILED
        self.save()

    def set_finished(self, message):
        self.result = {"message": message}
        self.finished_at = timezone.now()
        self.status = Snapshot.Status.FINISHED
        self.save()


class UserSyncController(models.Model):
    class Meta:
        ordering = ("-created_at",)

    """
    The controller holds a date of the least sync date.
    * At this date the sync process task starts to add users to a queue.
    * Users that have a last_sync_at or a created_at date greater than the least sync date are not added to the queue.
    """
    created_at = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)

    def schedule_sync(self, schema):
        """Dit moet niet hier... schema conflict"""
        from concierge.tasks import profile_updated_signal
        from user.models import User

        limit = User.objects.count() / 7

        qs = self.get_remaining(schema)
        qs = qs.order_by("created_at")

        for user_id in qs[:limit]:
            task = profile_updated_signal.delay(schema, str(user_id))
            self.items.create(
                schema=schema,
                user_id=user_id,
                task_id=task.id,
            )

    def __str__(self):
        return f"{self.created_at} (active=%s)" % int(self.active)

    def all_items(self):
        return self.items.order_by("created_at")

    def items_with_errors(self, **kwargs):
        return self.items.filter(result__isnull=False, **kwargs)

    def completed_items(self, **kwargs):
        return self.items.filter(completed_at__isnull=False, **kwargs).order_by(
            "completed_at"
        )

    def update_item_status(self):
        """Update the status of all open tasks"""
        for task in self.items.filter(completed_at__isnull=True):
            original_task = task.serialize()
            async_result = AsyncResult(task.task_id)
            task.state = async_result.state
            if async_result.ready():
                task.completed_at = make_aware(async_result.date_done, utc)
                task.result = str(async_result.result) if async_result.result else None
            else:
                if task.created_at < timezone.now() - timezone.timedelta(hours=48):
                    task.completed_at = timezone.now()
                    task.state = "LOST"
            if task.serialize() != original_task:
                task.save()


class UserSyncControllerItem(models.Model):
    """
    * The status of the scheduled sync process is stored in the controller as a UserSyncControllerItem.
    """

    controller = models.ForeignKey(
        "control.UserSyncController", on_delete=models.CASCADE, related_name="items"
    )
    created_at = models.DateTimeField(default=timezone.now)
    completed_at = models.DateTimeField(null=True)

    schema = models.CharField(max_length=255)
    user_id = models.UUIDField(null=False)

    task_id = models.CharField(max_length=255, null=True)
    state = models.CharField(max_length=255, default="PENDING")
    result = models.TextField(null=True)

    def serialize(self):
        return {
            "created_at": self.created_at,
            "completed_at": self.completed_at,
            "user_id": str(self.user_id),
            "task_id": self.task_id,
            "state": self.state,
            "result": self.result,
        }
