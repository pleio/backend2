from celery import chain
from django.conf import settings
from django.utils import timezone
from django.utils.text import slugify


def schedule_backup(site, actor, include_files, share_backup, snapshot=None):
    from control.tasks import (
        backup_site,
        followup_backup_complete,
        restore_snapshot_as_backup,
    )

    snapshot_or_live = (
        backup_site.s(
            site.id,
            skip_files=not bool(include_files),
            backup_folder=_backup_folder(site.schema_name),
            compress=True,
            share_backup=share_backup,
        )
        if snapshot is None
        else restore_snapshot_as_backup.s(
            site.id,
            snapshot,
            skip_files=not bool(include_files),
            backup_folder=_backup_folder(site.schema_name),
            compress=True,
            share_backup=share_backup,
        )
    )

    task_spec = chain(
        snapshot_or_live,
        followup_backup_complete.s(site.id, actor.guid),
    )
    return task_spec.apply_async()


def _backup_folder(schema_name):
    return slugify(
        "%s_%s_%s"
        % (
            timezone.localtime().strftime("%Y%m%d%H%M%S"),
            schema_name,
            settings.ENV,
        )
    )
