from django.utils.module_loading import import_string


def serialize_log_record(record):
    return {
        "created": record.created_at or "",
        "task": record.import_name or "",
        "started": record.started_at or "",
        "completed": record.completed_at or "",
        "status": record.message or "",
        "task_exists": _task_exists(record.import_name),
    }


def _task_exists(task):
    try:
        import_string(task)
        return True
    except ImportError:
        return False
