from django.utils.translation import gettext as _
from django_tenants.utils import tenant_context

from control.utils.render_configuration import SettingsBase
from control.utils.render_configuration.decorators import Decorator
from core import config
from core.lib import tenant_instance
from core.models import ProfileField, Tag


class Setting(SettingsBase):
    TEMPLATE = "control/config/value_base.html"

    def __init__(self, key, decorator=None):
        super().__init__()
        self.key = key
        self.site = tenant_instance()
        self.decorator = decorator or Decorator()

    def title(self):
        from core.base_config import DEFAULT_SITE_CONFIG

        _, description = DEFAULT_SITE_CONFIG[self.key]
        return description

    def raw_value(self):
        with tenant_context(self.site):
            return getattr(config, self.key, "")

    def is_empty(self):
        return not self.raw_value()

    def value(self):
        with tenant_context(self.site):
            return self.decorator.render(self.raw_value())

    def setting_keys(self):
        yield self.key


class ListSetting(Setting):
    def value(self):
        with tenant_context(self.site):
            formatted_values = [
                str(self.decorator.render(item)) for item in (self.raw_value() or [])
            ]
            return ", ".join(formatted_values) or "-"


class RawSetting(Setting):
    TEMPLATE = "control/config/value_fixed_format.html"


class UserProfile(Setting):
    TEMPLATE = "control/config/value_user_profile.html"

    def setting_keys(self):
        return ["DB"]

    def is_empty(self):
        try:
            with tenant_context(self.site):
                return not ProfileField.objects.exists()
        except Exception:
            pass

    def value(self):
        try:
            with tenant_context(self.site):
                return [*ProfileField.objects.all()]
        except Exception:
            pass


class TagCategories(Setting):
    TEMPLATE = "control/config/value_tag_categories.html"


class TagContentType(Setting):
    TEMPLATE = "control/config/value_tag_content_type.html"


class KeyValue(Setting):
    TEMPLATE = "control/config/value_key_value.html"

    def value(self):
        return super().value().items()


class ProfileSections(Setting):
    TEMPLATE = "control/config/value_profile_sections.html"

    def value(self):
        with tenant_context(self.site):
            result = []
            for section in config.PROFILE_SECTIONS:
                section["profileFields"] = self._load_profile_fields(
                    section["profileFieldGuids"]
                )
                result.append(section)
            return result

    @staticmethod
    def _load_profile_fields(guids):
        try:
            return [*ProfileField.objects.filter(id__in=guids)]
        except Exception:
            pass


class TagSynonyms(Setting):
    TEMPLATE = "control/config/value_custom_tag_synonyms.html"

    def title(self):
        return _("Custom Tag Synonyms")

    def setting_keys(self):
        return ["DB"]

    def is_empty(self):
        try:
            with tenant_context(self.site):
                return not Tag.objects.exists()
        except Exception:
            pass

    def value(self):
        try:
            with tenant_context(self.site):
                result = []
                for tag in Tag.objects.all():
                    result.append(
                        (
                            tag.label,
                            ", ".join([s.label for s in tag.synonyms.all()]) or "-",
                        )
                    )
                return result
        except Exception:
            pass
