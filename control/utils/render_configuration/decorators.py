import json

from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from core.lib import get_exportable_content_types
from core.utils.convert import tiptap_to_html


class Decorator:
    def __init__(self, parent=None):
        self.parent = parent

    def render(self, value):
        """
        Allow a chain of decorators to render a value.
        Do not override this method, override _render instead.
        """
        if self.parent:
            value = self.parent.render(value)
        return self._render(value)

    def _render(self, value):
        return value


class TipTap(Decorator):
    def _render(self, value):
        try:
            text = (tiptap_to_html(value) or "").strip()
            return mark_safe(text) or "-"
        except Exception:
            pass
        return "-"


class Boolean(Decorator):
    def _render(self, value):
        return "✅" if value else "-"


class AccessLevel(Decorator):
    def _render(self, value):
        if value == 0:
            return _("Only the author has access")
        elif value == 1:
            return _("Authenticated users have access")
        elif value == 2:
            return _("Public access")
        elif value == 4:
            return _("Group members have access")
        return "-"


class Language(Decorator):
    def _render(self, value):
        if value == "nl":
            return _("Dutch")
        elif value == "en":
            return _("English")
        elif value == "de":
            return _("German")
        elif value == "fr":
            return _("French")
        return ""


class Json(Decorator):
    def _render(self, value):
        try:
            return json.dumps(value, indent=2)
        except Exception:
            return "-"


class ContentType(Decorator):
    def _render(self, value):
        types = {t["value"]: t["label"] for t in get_exportable_content_types()}
        return types.get(value) or value


class Timestamp(Decorator):
    def _render(self, value):
        if value:
            return timezone.datetime.fromtimestamp(
                value, tz=timezone.get_current_timezone()
            )
        return "-"


class Require2FA(Decorator):
    def _render(self, value):
        if value == "admin":
            return _("Site administrators")
        if value == "none" or not value:
            return _("Nobody")
        elif value == "all" or value:
            return _("All users")
