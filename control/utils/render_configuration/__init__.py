"""
How does it work?
=======================

To group settings together, you use SettingsSection. A SettingsSection is given a title and SettingsSection
instances and/or Setting instances. Setting expects the raw setting to be in a specific format. A Setting can be
given a Decorator to alter the value before rendering it.

* Extend Setting if you want to render a setting using a different template or when a setting is not stored in the
  core.config but somewhere else; like in the database.
* Add another decorator (close to the others) if you need to alter the value differently.

"""


class SettingsBase:
    TEMPLATE = None

    def template(self):
        return self.TEMPLATE

    def __init__(self):
        self.parent = None

    def level(self, count=0):
        if self.parent is None:
            return count
        return self.parent.level(count + 1)

    def setting_keys(self):
        return []


class SettingsSection(SettingsBase):
    TEMPLATE = "control/config/section.html"

    def __init__(self, title, content):
        super().__init__()
        self.title = title
        self.content = []
        for item in content:
            self.append(item)

    def append(self, item):
        item.parent = self
        self.content.append(item)

    def setting_keys(self):
        for item in self.content:
            yield from item.setting_keys()
