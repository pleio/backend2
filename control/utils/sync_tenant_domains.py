import logging
import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible
from django.utils.regex_helper import _lazy_re_compile
from django.utils.translation import gettext as _
from django_tenants.utils import get_tenant_model

logger = logging.getLogger(__name__)


class SyncTenantDomains:
    def __init__(self, tenant):
        self.tenant = tenant
        self.validator = DomainNameValidator()

    def validate_domain(self, domain):
        """
        Test that the domain is not used by other tenants
        """
        available_domains = get_tenant_model().objects.active_clients()
        other_tenants = available_domains.exclude(id=self.tenant.id)

        if other_tenants.filter(domains__domain=domain).exists():
            logger.error("Domain %s is not available", domain)
            raise ValidationError(
                _("Domain %(domain)s already in use by another tenant")
                % {"domain": domain}
            )
        else:
            logger.error("Domain %s is available", domain)

        self.validator(domain)

    def update_domains(self, primary_domain, secondary_domains):
        all_domains = [primary_domain, *secondary_domains]

        # Create and delete domains.
        for domain in all_domains:
            logger.error("Get or create domain %s", domain)
            self.tenant.domains.get_or_create(domain=domain)

        self.tenant.domains.exclude(domain__in=all_domains).delete()

        # Update Domain.is_primary
        self.tenant.domains.filter(domain=primary_domain).update(is_primary=True)
        self.tenant.domains.exclude(domain=primary_domain).update(is_primary=False)


@deconstructible
class DomainNameValidator(RegexValidator):
    """
    Copied code from django5.1 to support validate domains names.
    """

    message = _("Enter a valid domain name.")
    ul = "\u00a1-\uffff"  # Unicode letters range (must not be a raw string).
    # Host patterns.
    hostname_re = (
        r"[a-z" + ul + r"0-9](?:[a-z" + ul + r"0-9-]{0,61}[a-z" + ul + r"0-9])?"
    )
    # Max length for domain name labels is 63 characters per RFC 1034 sec. 3.1.
    domain_re = r"(?:\.(?!-)[a-z" + ul + r"0-9-]{1,63}(?<!-))*"
    # Top-level domain.
    tld_re = (
        r"\."  # dot
        r"(?!-)"  # can't start with a dash
        r"(?:[a-z" + ul + "-]{2,63}"  # domain label
        r"|xn--[a-z0-9]{1,59})"  # or punycode label
        r"(?<!-)"  # can't end with a dash
        r"\.?"  # may have a trailing dot
    )
    ascii_only_hostname_re = r"[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?"
    ascii_only_domain_re = r"(?:\.(?!-)[a-zA-Z0-9-]{1,63}(?<!-))*"
    ascii_only_tld_re = (
        r"\."  # dot
        r"(?!-)"  # can't start with a dash
        r"(?:[a-zA-Z0-9-]{2,63})"  # domain label
        r"(?<!-)"  # can't end with a dash
        r"\.?"  # may have a trailing dot
    )

    max_length = 255

    def __init__(self, **kwargs):
        self.accept_idna = kwargs.pop("accept_idna", True)

        if self.accept_idna:
            self.regex = _lazy_re_compile(
                self.hostname_re + self.domain_re + self.tld_re, re.IGNORECASE
            )
        else:
            self.regex = _lazy_re_compile(
                self.ascii_only_hostname_re
                + self.ascii_only_domain_re
                + self.ascii_only_tld_re,
                re.IGNORECASE,
            )
        super().__init__(**kwargs)

    def __call__(self, value):
        if not isinstance(value, str) or len(value) > self.max_length:
            raise ValidationError(self.message, code=self.code, params={"value": value})
        if not self.accept_idna and not value.isascii():
            raise ValidationError(self.message, code=self.code, params={"value": value})
        super().__call__(value)
