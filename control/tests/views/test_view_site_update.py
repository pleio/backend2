from unittest import mock

from django.views.generic import FormView

from control.views import SiteUpdateView
from control.views.sites_crud import SiteUpdateForm
from core.tests.helpers import PleioTenantTestCase


def build_view():
    view = SiteUpdateView()
    view.site = mock.MagicMock()
    view.request = mock.MagicMock()
    return view


class TestViewSiteUpdateTestCase(PleioTenantTestCase):
    def test_constructor(self):
        view = build_view()
        self.assertEqual(view.template_name, "sites/update.html")
        self.assertEqual(view.form_class, SiteUpdateForm)
        self.assertTrue(isinstance(view, FormView))

    @mock.patch("control.views.sites_crud.reverse")
    def test_get_success_url(self, reverse):
        view = build_view()
        reverse.return_value = mock.MagicMock()

        response = view.get_success_url()

        self.assertEqual(response, reverse.return_value)
        reverse.assert_called_with("site_details", args=(view.site.id,))

    @mock.patch("control.views.sites_crud.FormView.get_form_kwargs")
    def test_get_form_kwargs(self, super_get_form_kwargs):
        super_get_form_kwargs.return_value = {}
        view = build_view()
        response = view.get_form_kwargs()

        self.assertEqual(response, {"site": view.site})

    @mock.patch("control.views.sites_crud.messages")
    @mock.patch("control.views.sites_crud.FormView.form_valid")
    def test_form_valid(self, super_form_valid, messages):
        view = build_view()
        form = mock.MagicMock()
        super_form_valid.return_value = mock.MagicMock()

        response = view.form_valid(form)

        self.assertEqual(response, super_form_valid.return_value)
        self.assertTrue(form.save.called)
        self.assertTrue(messages.success.called)
