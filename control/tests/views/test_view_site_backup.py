from unittest import mock

from django.db import connection
from django.test import override_settings

from control.models import AccessCategory, AccessLog
from control.views import SiteBackupView
from control.views.sites_crud import ConfirmSiteBackupForm, FormView
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


def build_view():
    view = SiteBackupView()
    view.site = mock.MagicMock()
    view.site.id = 1
    view.request = mock.MagicMock()
    return view


class TestViewSiteBackupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        connection.set_schema_to_public()
        self.admin = UserFactory(is_superadmin=True)
        self.override_setting(
            AWS_ACCESS_KEY_ID="key",
            AWS_SECRET_ACCESS_KEY="secret",
            AWS_S3_ENDPOINT_URL="http://minio:9000",
        )

    def test_constructor(self):
        view = build_view()
        self.assertEqual(view.template_name, "sites/backup.html")
        self.assertEqual(view.form_class, ConfirmSiteBackupForm)
        self.assertTrue(isinstance(view, FormView))

    @mock.patch("control.views.sites_crud.reverse")
    def test_get_success_url(self, mocked_reverse):
        mocked_reverse.return_value = mock.MagicMock()

        view = build_view()

        self.assertEqual(mocked_reverse.return_value, view.get_success_url())
        mocked_reverse.assert_called_with("site_backup", args=(view.site.id,))

    @mock.patch("control.views.sites_crud.schedule_backup")
    @mock.patch("control.views.sites_crud.auth.get_user")
    @mock.patch("control.views.sites_crud.FormView.form_valid")
    @mock.patch("control.views.sites_crud.messages.success")
    def test_form_valid(
        self, mocked_success, super_form_valid, mocked_get_user, mocked_schedule_backup
    ):
        mocked_get_user.return_value = self.admin
        task = mock.MagicMock()
        task.id = 1
        mocked_schedule_backup.return_value = task
        super_form_valid.return_value = mock.MagicMock()
        form = mock.MagicMock()
        form.cleaned_data = {
            "include_files": mock.MagicMock(),
            "share_backup": mock.MagicMock(),
            "from_snapshot": mock.MagicMock(),
        }

        view = build_view()
        response = view.form_valid(form)

        self.assertEqual(response, super_form_valid.return_value)
        mocked_schedule_backup.assert_called_with(
            view.site,
            mocked_get_user.return_value,
            form.cleaned_data["include_files"],
            form.cleaned_data["share_backup"],
            form.cleaned_data["from_snapshot"],
        )
        mocked_success.assert_called()

    @override_settings(RESTIC_REPOSITORY_CONFIGURED=False)
    @mock.patch("control.views.mixins.SiteDetailsViewMixin.get_context_data")
    @mock.patch("control.models.AccessLogManager.filter")
    @mock.patch("control.views.sites_crud.SiteBackupView.serialize_access_logs")
    @mock.patch("control.views.sites_crud.SiteBackupView.get_access_logs")
    def test_get_context_data(
        self,
        mocked_get_access_logs,
        mocked_serialize_access_logs,
        mocked_filter,
        super_get_context_data,
    ):
        mocked_get_access_logs.return_value = mock.MagicMock()
        mocked_serialize_access_logs.return_value = mock.MagicMock()
        super_get_context_data.return_value = {}
        mocked_filter.return_value = mock.MagicMock()

        view = build_view()
        self.assertEqual(
            view.get_context_data(),
            {
                "access_logs": mocked_filter.return_value,
                "snapshots_enabled": False,
                "has_object_storage": True,
                "backups": mocked_serialize_access_logs.return_value,
            },
        )
        mocked_serialize_access_logs.assert_called_with(
            mocked_get_access_logs.return_value
        )

    @mock.patch("os.path.exists")
    @mock.patch("control.views.sites_crud.get_full_url")
    @mock.patch("control.views.sites_crud.format_file_size")
    @mock.patch("control.views.sites_crud.reverse")
    def test_serialize(self, reverse, format_file_size, get_full_url, path_exists):
        path_exists.return_value = True
        get_full_url.return_value = "http://example.com"
        format_file_size.return_value = "1.0 MB"
        reverse.return_value = mock.MagicMock()

        logs = [
            AccessLog.objects.create(
                category=AccessLog.custom_category(AccessCategory.SITE_BACKUP, 1),
                user=self.admin,
                item_id="backup_folder",
                type=AccessLog.AccessTypes.CREATE,
                site=self.tenant,
            ),
            AccessLog.objects.create(
                category=AccessLog.custom_category(AccessCategory.SITE_BACKUP, 1),
                user=self.admin,
                item_id="backup_file.zip",
                type=AccessLog.AccessTypes.CREATE,
                site=self.tenant,
            ),
        ]

        view = build_view()

        self.assertEqual(
            [*view.serialize_access_logs(logs)],
            [
                {
                    "created_at": logs[0].created_at,
                    "download": False,
                    "author": self.admin.name,
                    "download_url": "",
                    "filesize": "",
                    "filename": "backup_folder",
                },
                {
                    "created_at": logs[1].created_at,
                    "download": True,
                    "author": self.admin.name,
                    "download_url": get_full_url.return_value,
                    "filesize": format_file_size.return_value,
                    "filename": "backup_file.zip",
                },
            ],
        )

    def test_get_access_logs(self):
        kwargs = {
            "user": self.admin,
            "item_id": "backup",
            "type": AccessLog.AccessTypes.CREATE,
            "site": self.tenant,
        }
        logs = [
            AccessLog.objects.create(
                **kwargs,
                category=AccessLog.custom_category(AccessCategory.SITE_BACKUP, 1),
            ),
            AccessLog.objects.create(
                **kwargs,
                category=AccessLog.custom_category(AccessCategory.SITE_BACKUP, 2),
            ),
        ]

        view = build_view()
        self.assertEqual([*view.get_access_logs()], [logs[0]])
