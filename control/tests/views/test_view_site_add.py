from unittest import mock

from django.views.generic import FormView

from control.forms import AddSiteForm
from control.views import SitesAddView
from core.tests.helpers import PleioTenantTestCase


class TestViewSiteAddTestCase(PleioTenantTestCase):
    def build_view(self):
        view = SitesAddView()
        view.request = mock.MagicMock()
        return view

    def test_consructor(self):
        view = self.build_view()

        self.assertEqual(view.form_class, AddSiteForm)
        self.assertEqual(view.template_name, "sites/add.html")
        self.assertTrue(isinstance(view, FormView))

    @mock.patch("control.views.sites_crud.reverse")
    def test_success_url(self, mocked_reverse):
        mocked_reverse.return_value = mock.MagicMock()
        view = self.build_view()
        response = view.get_success_url()

        self.assertTrue(mocked_reverse.called)
        self.assertEqual(mocked_reverse.call_args[0][0], "site_index")
        self.assertEqual(response, mocked_reverse.return_value)

    @mock.patch("control.views.sites_crud.FormView.form_valid")
    @mock.patch("control.models.TaskManager.create_task")
    def test_form_valid_restore_site(self, create_task, super_form_valid):
        form = mock.MagicMock()
        form.cleaned_data = {
            "schema_name": "demo_schema",
            "domain": "http://example.com",
            "backup": "backup_name",
        }
        form.administrative_details = mock.MagicMock()

        view = self.build_view()
        view.form_valid(form)

        self.assertTrue(super_form_valid.called)
        self.assertTrue(create_task.called)
        self.assertEqual(create_task.call_args[0][0], "control.tasks.restore_site")
        self.assertEqual(
            create_task.call_args[0][1],
            (
                "backup_name",
                "demo_schema",
                "http://example.com",
                form.administrative_details,
            ),
        )

    @mock.patch("control.views.sites_crud.FormView.form_valid")
    @mock.patch("control.models.TaskManager.create_task")
    def test_form_valid_add_site(self, create_task, super_form_valid):
        form = mock.MagicMock()
        form.cleaned_data = {
            "schema_name": "demo_schema",
            "domain": "http://example.com",
            "backup": None,
        }
        form.administrative_details = mock.MagicMock()

        view = self.build_view()
        view.form_valid(form)

        self.assertTrue(super_form_valid.called)
        self.assertTrue(create_task.called)
        self.assertEqual(create_task.call_args[0][0], "control.tasks.add_site")
        self.assertEqual(
            create_task.call_args[0][1],
            (
                "demo_schema",
                "http://example.com",
                form.administrative_details,
            ),
        )
