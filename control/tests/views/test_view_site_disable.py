from unittest import mock

from control.views import SiteDisableView
from control.views.sites_crud import TemplateView
from core.tests.helpers import PleioTenantTestCase


def build_view():
    view = SiteDisableView()
    view.request = mock.MagicMock()
    view.site = mock.MagicMock()
    view.site.id = 1
    return view


class TestViewSiteDisableTestCase(PleioTenantTestCase):
    def test_constructor(self):
        view = build_view()
        self.assertEqual(view.template_name, "sites/disable.html")
        self.assertTrue(isinstance(view, TemplateView))

    @mock.patch("control.views.sites_crud.redirect")
    @mock.patch("control.views.sites_crud.reverse")
    @mock.patch("control.views.sites_crud.messages")
    @mock.patch("control.models.TaskManager.create_task")
    def test_dispatch_post(
        self, mocked_create_task, mocked_messages, mocked_reverse, mocked_redirect
    ):
        view = build_view()
        mocked_create_task.return_value = mock.MagicMock()
        mocked_create_task.return_value.id = 1
        mocked_reverse.return_value = mock.MagicMock()
        mocked_redirect.return_value = mock.MagicMock()

        response = view.post(view.request)

        self.assertEqual(response, mocked_redirect.return_value)
        self.assertEqual(
            mocked_create_task.call_args[0],
            ("control.tasks.update_site", (view.site.id, {"is_active": False})),
        )
        self.assertTrue(mocked_messages.success.called)
        mocked_reverse.assert_called_with("site_index")
        mocked_redirect.assert_called_with(mocked_reverse.return_value)
