from unittest import mock

from django.http import HttpResponse

from control.views.mixins import SiteDetailsViewMixin
from core.tests.helpers import PleioTenantTestCase


def build_view():
    from django.views.generic import TemplateView

    class TestSiteDetailView(SiteDetailsViewMixin, TemplateView):
        pass

    return TestSiteDetailView()


class TestViewSiteDetailsViewMixin(PleioTenantTestCase):
    def test_constructor(self):
        view = build_view()
        self.assertEqual(view.site, None)

    @mock.patch("tenants.models.ClientQuerySet.first")
    @mock.patch("control.views.mixins.SecureControlViewMixin.dispatch")
    def test_dispatch_client_not_found(self, mocked_dispatch, mocked_first):
        mocked_first.return_value = None

        view = build_view()
        request = mock.MagicMock()
        result = view.dispatch(request, site_id=1)

        self.assertFalse(mocked_dispatch.called)
        self.assertEqual(result.status_code, 404)

    @mock.patch("tenants.models.ClientQuerySet.first")
    @mock.patch("control.views.mixins.SecureControlViewMixin.dispatch")
    def test_dispatch_client_result(self, mocked_dispatch, mocked_first):
        view = build_view()
        request = mock.MagicMock()
        mocked_first.return_value = self.tenant
        mocked_dispatch.return_value = mock.MagicMock(spec=HttpResponse)

        result = view.dispatch(request, site_id=1)

        self.assertEqual(result, mocked_dispatch.return_value)
        self.assertEqual(view.site, self.tenant)


class TestViewSiteDetailsViewContextDataMixin(PleioTenantTestCase):
    @mock.patch("control.views.mixins.get_base_url")
    @mock.patch("django.views.generic.TemplateView.get_context_data")
    def test_get_context_data(self, get_context_data, get_base_url):
        view = build_view()
        view.site = self.tenant
        get_context_data.return_value = {}
        get_base_url.return_value = "http://example.com"

        context = view.get_context_data()
        self.assertEqual(
            context,
            {
                "schema_name": self.tenant.schema_name,
                "site": self.tenant,
                "site_id": self.tenant.id,
                "site_name": "Pleio 2.0",
                "site_url": "http://example.com",
            },
        )
