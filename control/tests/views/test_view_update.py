from http import HTTPStatus

from control.tests.helpers import Control as _
from tenants.models import Client


class TestViewToolsTestCase(_.BaseTestCase):
    def setUp(self):
        super().setUp()
        if not Client.objects.first():
            Client.objects.create(schema_name="test", name="test")
        self.test_tenant = Client.objects.first()

    def test_get_form(self):
        self.client.force_login(self.admin)
        response = self.client.get(_.reverse("site_update", args=[self.test_tenant.id]))

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "sites/update.html")
