from unittest import mock

from django.views.generic import FormView

from control.forms import DeleteSiteForm
from control.views import SiteDeleteView
from core.tests.helpers import PleioTenantTestCase


def build_view():
    view = SiteDeleteView()
    view.site = mock.MagicMock()
    view.request = mock.MagicMock()
    return view


class TestViewSiteDeleteTestCase(PleioTenantTestCase):
    def test_constructor(self):
        view = build_view()
        self.assertEqual(view.template_name, "sites/delete.html")
        self.assertEqual(view.form_class, DeleteSiteForm)
        self.assertTrue(isinstance(view, FormView))

    @mock.patch("control.views.sites_crud.reverse")
    def test_get_success_url(self, reverse):
        view = build_view()
        reverse.return_value = mock.MagicMock()

        response = view.get_success_url()

        self.assertEqual(response, reverse.return_value)
        reverse.assert_called_with("site_index")

    @mock.patch("control.views.sites_crud.FormView.get_form_kwargs")
    def test_get_form_kwargs(self, super_get_form_kwargs):
        super_get_form_kwargs.return_value = {}
        view = build_view()
        response = view.get_form_kwargs()

        self.assertEqual(
            response,
            {
                "initial": {
                    "site_id": view.site.id,
                },
            },
        )

    @mock.patch("control.views.sites_crud.messages")
    @mock.patch("control.models.TaskManager.create_task")
    @mock.patch("control.views.sites_crud.FormView.form_valid")
    def test_form_valid(self, super_form_valid, create_task, messages):
        view = build_view()
        create_task.return_value = mock.MagicMock()
        create_task.return_value.id = 1
        form = mock.MagicMock()
        super_form_valid.return_value = mock.MagicMock()

        response = view.form_valid(form)

        self.assertEqual(response, super_form_valid.return_value)
        self.assertEqual(
            create_task.call_args[0], ("control.tasks.delete_site", (view.site.id,))
        )
        self.assertTrue(messages.info.called)
