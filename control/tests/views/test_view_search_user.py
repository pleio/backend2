from http import HTTPStatus
from unittest import mock

from django.http import HttpResponse

from control.tests.helpers import Control as _


class TestViewSearchUserTestCase(_.BaseTestCase):
    def test_anonymous_visitor(self):
        response = self.client.get(_.reverse("search_user"))

        self.assertNotEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateNotUsed(response, "tools/search_user.html")

    def test_search_user_index_page(self):
        self.client.force_login(self.admin)
        response = self.client.get(_.reverse("search_user"))

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "tools/search_user.html")

    def test_search_user(self):
        self.client.force_login(self.admin)
        response = self.client.get(
            _.reverse("search_user"), data={"email": self.admin.email}
        )

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "tools/search_user.html")

    @mock.patch("control.views.render")
    @mock.patch("tenants.models.ClientQuerySet.exclude")
    @mock.patch("control.views.get_base_url")
    def test_search_known_user(self, get_base_url, manager_exclude, render):
        manager_exclude.return_value = [self.public_tenant]
        get_base_url.return_value = "https://example.com"
        render.return_value = HttpResponse("Demo")

        self.client.force_login(self.admin)
        self.client.get(_.reverse("search_user"), data={"email": self.admin.email})

        request, template, context = render.call_args.args
        self.assertEqual(
            context["data"],
            [
                {
                    "schema": "public",
                    "domain": self.public_tenant.primary_domain,
                    "last_login": self.admin.last_login,
                    "id": self.public_tenant.id,
                    "url": "https://example.com",
                    "user_email": self.admin.email,
                    "user_external_id": None,
                    "user_name": self.admin.name,
                    "sort_order": self.admin.last_login,
                }
            ],
        )
