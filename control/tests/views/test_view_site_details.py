from unittest import mock

from django.utils.timezone import timedelta

from control.views import SiteDetailsView
from control.views.sites_crud import SiteDetailsViewMixin, TemplateView
from core.lib import early_this_morning
from core.models import SiteStat
from core.tests.helpers import PleioTenantTestCase


class TestViewSiteDetailsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.view = SiteDetailsView()
        self.view.site = self.tenant
        self.view.request = mock.MagicMock()

    def test_constructor(self):
        self.assertTrue(isinstance(self.view, TemplateView))
        self.assertTrue(isinstance(self.view, SiteDetailsViewMixin))
        self.assertEqual(self.view.template_name, "sites/details.html")

    @mock.patch("control.views.mixins.SiteDetailsViewMixin.get_context_data")
    @mock.patch("control.views.sites_crud.SiteDetailsView.SupportContract.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.SupportHours.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.SiteCategory.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.SitePlan.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.PushSubscriptionUsers.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.BlockedUsers.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.ActiveUsers.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.DiskStatDays.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.DiskStatData.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.DBStatDays.value")
    @mock.patch("control.views.sites_crud.SiteDetailsView.DBStatData.value")
    def test_get_context_data(
        self,
        db_size_data,
        db_stat_days,
        disk_size_data,
        disk_stat_days,
        active_users,
        blocked_users,
        push_users,
        site_plan,
        site_category,
        support_hours,
        support_contract,
        super_get_context_data,
    ):
        super_get_context_data.return_value = {}

        result = self.view.get_context_data()

        self.assertEqual(
            result,
            {
                "support_contract_enabled": support_contract.return_value,
                "support_hours_remaining": support_hours.return_value,
                "site_category": site_category.return_value,
                "site_plan": site_plan.return_value,
                "db_stat_days": db_stat_days.return_value,
                "db_stat_data": db_size_data.return_value,
                "disk_stat_days": disk_stat_days.return_value,
                "disk_stat_data": disk_size_data.return_value,
                "nr_active_users": active_users.return_value,
                "nr_blocked_users": blocked_users.return_value,
                "nr_push_device_configured_users": push_users.return_value,
            },
        )


class SiteDetailPagePropertyTestCase(PleioTenantTestCase):
    def test_support_contract(self):
        self.override_config(SUPPORT_CONTRACT_ENABLED=True)
        self.assertTrue(SiteDetailsView.SupportContract().value())

        self.override_config(SUPPORT_CONTRACT_ENABLED=False)
        self.assertFalse(SiteDetailsView.SupportContract().value())

    def test_support_hours(self):
        self.override_config(SUPPORT_CONTRACT_HOURS_REMAINING=1)
        self.assertEqual(SiteDetailsView.SupportHours().value(), 1)

        self.override_config(SUPPORT_CONTRACT_HOURS_REMAINING=10)
        self.assertEqual(SiteDetailsView.SupportHours().value(), 10)

    def SiteCategory(self):
        self.override_config(SITE_CATEGORY="foo")
        self.assertEqual(SiteDetailsView.SiteCategory().value(), "")

        self.override_config(SITE_CATEGORY="so_in")
        self.assertEqual(SiteDetailsView.SiteCategory().value(), "so_in")

    def test_site_plan(self):
        self.override_config(SITE_PLAN="foo")
        self.assertEqual(SiteDetailsView.SitePlan().value(), "")

        self.override_config(SITE_PLAN="basic")
        self.assertEqual(SiteDetailsView.SitePlan().value(), "Basis")

    @mock.patch("core.models.push_notification.WebPushSubscriptionQuerySet.values")
    def test_push_subscription_users(self, mocked_values):
        expected_result = mock.MagicMock()
        count_container = mock.MagicMock()
        count_container.count.return_value = expected_result
        distinct_container = mock.MagicMock()
        distinct_container.distinct.return_value = count_container
        mocked_values.return_value = distinct_container

        self.assertEqual(
            SiteDetailsView.PushSubscriptionUsers().value(), expected_result
        )
        mocked_values.assert_called_with("user_id")

    @mock.patch("user.models.UserQuerySet.filter")
    def test_blocked_users(self, mocked_filter):
        result = SiteDetailsView.BlockedUsers().value()
        self.assertEqual(result, mocked_filter.return_value.count.return_value)
        mocked_filter.assert_called_with(is_active=False)

    @mock.patch("user.models.UserQuerySet.filter")
    def test_active_users(self, mocked_filter):
        result = SiteDetailsView.ActiveUsers().value()
        self.assertEqual(result, mocked_filter.return_value.count.return_value)
        mocked_filter.assert_called_with(is_active=True)


class SiteDetailsPageUsagePropertyTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        reference = early_this_morning() + timedelta(
            hours=5
        )  # add 5 to avoid daylight saving time issues
        for value, created_at in [
            (index, reference + timedelta(days=(-51 + index))) for index in range(0, 51)
        ]:
            stat = SiteStat.objects.create(stat_type="DISK_SIZE", value=value)
            SiteStat.objects.filter(id=stat.id).update(created_at=created_at)
            stat = SiteStat.objects.create(stat_type="DB_SIZE", value=value)
            SiteStat.objects.filter(id=stat.id).update(created_at=created_at)

        self.first_db_record = SiteStat.objects.filter(stat_type="DB_SIZE").first()
        self.last_db_record = SiteStat.objects.filter(stat_type="DB_SIZE").last()
        self.first_disk_record = SiteStat.objects.filter(stat_type="DISK_SIZE").first()
        self.last_disk_record = SiteStat.objects.filter(stat_type="DISK_SIZE").last()

    def test_disk_days(self):
        result = SiteDetailsView.DiskStatDays().value()
        self.assertEqual(len(result), 50)
        self.assertIn(self.last_disk_record.created_at.strftime("%d-%b-%Y"), result)
        self.assertNotIn(self.first_disk_record.created_at.strftime("%d-%b-%Y"), result)

    def test_disk_data(self):
        result = SiteDetailsView.DiskStatData().value()
        self.assertEqual(len(result), 50)
        self.assertIn(self.last_disk_record.value, result)
        self.assertNotIn(self.first_disk_record.value, result)

    def test_db_days(self):
        result = SiteDetailsView.DBStatDays().value()
        self.assertEqual(len(result), 50)
        self.assertIn(self.last_db_record.created_at.strftime("%d-%b-%Y"), result)
        self.assertNotIn(self.first_db_record.created_at.strftime("%d-%b-%Y"), result)

    def test_db_data(self):
        result = SiteDetailsView.DBStatData().value()
        self.assertEqual(len(result), 50)
        self.assertIn(self.last_db_record.value, result)
        self.assertNotIn(self.first_db_record.value, result)
