from http import HTTPStatus

from lxml.html import fromstring

from control.tests.helpers import Control as _
from core.tests.helpers import suppress_stdout
from tenants.models import Client, Domain


class TestViewSitesTestCase(_.BaseTestCase):
    @suppress_stdout()
    def setUp(self):
        super().setUp()
        self.demo, _ = Client.objects.get_or_create(
            schema_name="demo1", name="A test tenant"
        )

    def test_anonymous_visitor(self):
        response = self.client.get(_.reverse("site_index"))

        self.assertNotEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateNotUsed(response, "sites/index.html")

    def test_normal_operation(self):
        self.client.force_login(self.admin)
        response = self.client.get(_.reverse("site_index"))
        content = response.getvalue()

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "sites/index.html")
        self.assertIn(self.demo.schema_name, content.decode())

    def test_invalid_paginator(self):
        self.client.force_login(self.admin)
        response = self.client.get(_.reverse("site_index"), data={"page": "foo"})
        content = response.getvalue()

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(self.demo.schema_name, content.decode())

    def test_paginator_overflow(self):
        self.client.force_login(self.admin)
        response = self.client.get(_.reverse("site_index"), data={"page": 100})
        content = response.getvalue()

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(self.demo.schema_name, content.decode())

    def test_search(self):
        DOMAIN_NAME = self.fake.url()
        primary, created = Domain.objects.get_or_create(
            tenant=self.demo, is_primary=True
        )
        primary.domain = DOMAIN_NAME
        primary.save()
        secondary, created = Domain.objects.get_or_create(
            tenant=self.demo, is_primary=False
        )
        secondary.domain = DOMAIN_NAME + ".com"
        secondary.save()

        self.client.force_login(self.admin)
        response = self.client.get(
            _.reverse("site_index"), data={"domain": DOMAIN_NAME}
        )
        content = response.getvalue()
        tree = fromstring(content)
        query = '//tr[@data-schema-name="{}"]'.format(self.demo.schema_name)

        rows = tree.xpath(query)
        self.assertEqual(len(rows), 1, msg=rows)
