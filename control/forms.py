import json
import logging
import re

import validators
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django_tenants.utils import tenant_context

from control.models import Snapshot
from control.utils.sync_tenant_domains import SyncTenantDomains
from core import config, constances
from tenants.models import Client, Domain

logger = logging.getLogger(__name__)

# from django-tenants
SQL_IDENTIFIER_RE = re.compile(r"^[_a-zA-Z0-9]{1,63}$")
SQL_SCHEMA_NAME_RESERVED_RE = re.compile(r"^pg_", re.IGNORECASE)


def validate_domain(value):
    exists = Domain.objects.filter(domain=value).first()
    if exists:
        msg = "Domain already exists"
        raise ValidationError(msg)
    if not validators.domain(value):
        msg = "Invalid domain"
        raise ValidationError(msg)


def validate_schema(value):
    valid = SQL_IDENTIFIER_RE.match(value) and not SQL_SCHEMA_NAME_RESERVED_RE.match(
        value
    )
    if not valid:
        msg = "Invalid schema name"
        raise ValidationError(msg)


class AddSiteForm(forms.Form):
    schema_name = forms.CharField(
        label=_("Database schema"),
        max_length=100,
        required=True,
        validators=[validate_schema],
    )
    domain = forms.CharField(
        label=_("Domain name"),
        max_length=250,
        required=True,
        validators=[validate_domain],
    )
    backup = forms.CharField(label=_("From backup"), max_length=250, required=False)

    site_profile = forms.ChoiceField(
        label=_("Website profile"),
        choices=[
            ("default", _("Keep default")),
            ("closed", _("Closed")),
            ("open", _("Open")),
        ],
        required=True,
        widget=forms.RadioSelect(),
    )
    site_owner = forms.CharField(
        label=_("Contact person"), max_length=250, required=False
    )
    site_owner_email = forms.EmailField(
        label=_("Email address"), max_length=250, required=False
    )
    initial_site_admin_email = forms.EmailField(
        label=_("Initial site administrator"), max_length=250, required=False
    )
    notes = forms.CharField(
        label=_("Notes"),
        required=False,
        widget=forms.Textarea(attrs={"rows": 5}),
    )

    def clean(self):
        cleaned_data = super().clean()

        schema_name = cleaned_data.get("schema_name")

        exists = Client.objects.filter(schema_name=schema_name).first()
        if exists:
            self.add_error("schema_name", _("Schema name already exists"))

    def clean_initial_site_admin_email(self):
        return (self.cleaned_data.get("initial_site_admin_email") or "").lower().strip()

    def clean_site_owner_email(self):
        return (self.cleaned_data.get("site_owner_email") or "").lower().strip()

    @property
    def administrative_details(self):
        return json.dumps(
            {
                "site_profile": self.cleaned_data.get("site_profile"),
                "site_owner": self.cleaned_data.get("site_owner"),
                "site_owner_email": self.cleaned_data.get("site_owner_email"),
                "initial_site_admin_email": self.cleaned_data.get(
                    "initial_site_admin_email"
                ),
                "notes": self.cleaned_data.get("notes"),
            }
        )


class DeleteSiteForm(forms.Form):
    site_id = forms.IntegerField(widget=forms.HiddenInput())
    check = forms.CharField(
        label=_("Re-type the schema name to delete"), max_length=255, required=True
    )

    def clean(self):
        cleaned_data = super().clean()

        site_id = cleaned_data.get("site_id")
        check = cleaned_data.get("check")

        site = Client.objects.filter(id=site_id).first()
        if site.schema_name != check:
            self.add_error("check", _("Type schema name to delete this site"))


class ConfirmSiteBackupForm(forms.Form):
    include_files = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Include files"),
        widget=forms.CheckboxInput(),
    )
    share_backup = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Share backup"),
        widget=forms.CheckboxInput(),
    )
    from_snapshot = forms.ChoiceField(
        label=_("From snapshot"),
        choices=[],
        help_text=_("Select snapshot to create backup from."),
        required=False,
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        super().__init__(*args, **kwargs)
        self.fields["from_snapshot"].choices = self._from_snapshot_choices()

    def _from_snapshot_choices(self):
        snapshots = Snapshot.objects.filter(client=self.site).order_by("-started_at")

        def snapshot_description(snapshot):
            return f"{snapshot.started_at.strftime('%Y-%m-%d %H:%M')} - {snapshot.snapshot_id}"

        return [
            ("", _("Current state")),
            *[(s.snapshot_id, snapshot_description(s)) for s in snapshots],
        ]


class ConfirmSnapshotForm(forms.Form):
    pass


class AgreementAddForm(forms.Form):
    name = forms.CharField(label="Name", max_length=255, required=True)


class AgreementAddVersionForm(forms.Form):
    agreement_id = forms.IntegerField(widget=forms.HiddenInput())

    version = forms.CharField(label="Version", max_length=255, required=True)
    document = forms.FileField(
        label="Document",
        required=True,
        widget=forms.FileInput(attrs={"accept": "application/pdf"}),
    )


class SiteUpdateForm(forms.Form):
    site_owner = forms.CharField(
        label=_("Contact person"), max_length=250, required=False
    )
    site_owner_email = forms.EmailField(
        label=_("Email address"), max_length=250, required=False
    )
    notes = forms.CharField(
        label=_("Notes"),
        required=False,
        widget=forms.Textarea(attrs={"rows": 5}),
    )
    primary_domain = forms.CharField(
        label=_("Primary domain name"),
        help_text=_("Don't include http:// or https://."),
        required=True,
    )
    secondary_domains = forms.CharField(
        label=_("Redirect domain names"),
        help_text=_("One domain name per line. Don't include http:// or https://."),
        widget=forms.Textarea(attrs={"rows": 3}),
        required=False,
    )

    support_site_plan = forms.ChoiceField(
        label=_("Website plan"),
        required=False,
        widget=forms.RadioSelect(),
        choices=[(c.name, c.value) for c in constances.SitePlanChoices],
    )
    support_site_category = forms.ChoiceField(
        label=_("Website category"),
        required=False,
        widget=forms.RadioSelect(),
        choices=[(c.name, c.value) for c in constances.SiteCategoryChoices],
    )
    support_contract_enabled = forms.BooleanField(
        label=_("Support contract applies"), required=False
    )
    support_contract_hours_remaining = forms.FloatField(
        label=_("Support hours remaining"), required=False
    )

    def __init__(self, *args, site, **kwargs):
        self.site = site
        self.domain_updater = SyncTenantDomains(site)

        kwargs["initial"]["primary_domain"] = (
            self.site.domains.filter(is_primary=True).first().domain
        )
        kwargs["initial"]["secondary_domains"] = "\n".join(self.site.secondary_domains)

        kwargs["initial"]["site_owner"] = self.site.administrative_details.get(
            "site_owner"
        )
        kwargs["initial"]["site_owner_email"] = self.site.administrative_details.get(
            "site_owner_email"
        )

        with tenant_context(site):
            kwargs["initial"]["support_site_category"] = config.SITE_CATEGORY
            kwargs["initial"]["support_site_plan"] = config.SITE_PLAN
            kwargs["initial"]["notes"] = config.SITE_NOTES
            kwargs["initial"]["support_contract_enabled"] = (
                config.SUPPORT_CONTRACT_ENABLED
            )
            kwargs["initial"]["support_contract_hours_remaining"] = (
                config.SUPPORT_CONTRACT_HOURS_REMAINING
            )

        super().__init__(*args, **kwargs)

    def clean_primary_domain(self):
        primary_domain = (self.cleaned_data["primary_domain"] or "").strip()
        self.domain_updater.validate_domain(primary_domain)
        return primary_domain

    def clean_secondary_domains(self):
        secondary_domains = [
            sd.strip()
            for sd in (self.cleaned_data["secondary_domains"] or "").split("\n")
        ]
        secondary_domains = [sd for sd in secondary_domains if sd]
        for domain in secondary_domains:
            self.domain_updater.validate_domain(domain)

        return secondary_domains

    def save(self):
        with tenant_context(self.site):
            config.SITE_CATEGORY = self.cleaned_data["support_site_category"]
            config.SITE_NOTES = self.cleaned_data["notes"]
            config.SITE_PLAN = self.cleaned_data["support_site_plan"]
            config.SUPPORT_CONTRACT_ENABLED = self.cleaned_data[
                "support_contract_enabled"
            ]
            config.SUPPORT_CONTRACT_HOURS_REMAINING = self.cleaned_data[
                "support_contract_hours_remaining"
            ]
        self.site.administrative_details = self.site.administrative_details or {}
        self.site.administrative_details["site_owner"] = self.cleaned_data["site_owner"]
        self.site.administrative_details["site_owner_email"] = self.cleaned_data[
            "site_owner_email"
        ]
        self.domain_updater.update_domains(
            self.cleaned_data["primary_domain"],
            self.cleaned_data["secondary_domains"],
        )

        self.site.save()


class MigrateStorageForm(forms.Form):
    site_id = forms.IntegerField(widget=forms.HiddenInput())
    check = forms.CharField(
        label=_("Re-type the schema name to migrate storage"),
        max_length=255,
        required=True,
    )

    def clean(self):
        cleaned_data = super().clean()

        site_id = cleaned_data.get("site_id")
        check = cleaned_data.get("check")

        site = Client.objects.filter(id=site_id).first()
        if site.schema_name != check:
            self.add_error("check", _("Type schema name to delete this site"))
