from mixer.backend.django import mixer


def ChatFactory(**kwargs):
    return mixer.blend("chat.Chat", **kwargs)


def ChatMessageFactory(**kwargs):
    return mixer.blend("chat.ChatMessage", **kwargs)
