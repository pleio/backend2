from unittest import mock

from django.db import connection
from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage, ChatUser
from core.factories import GroupFactory
from core.models.push_notification import WebPushSubscription
from core.tasks.notification_tasks import create_chat_webpush_notification
from core.tests.helpers import PleioTenantTestCase, override_config
from user.factories import UserFactory


class DirectNotificationsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.user3 = UserFactory()
        self.group = GroupFactory(owner=self.user1, auto_notification=True)

        self.group.join(self.user2, "member")
        self.chat = mixer.blend(Chat, group=self.group, type="GROUP")
        ChatUser(chat=self.chat, user=self.user1).save()
        ChatUser(chat=self.chat, user=self.user2).save()
        self.message = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, richDescription="Test"
        )

        self.chat2 = mixer.blend(Chat, group=None, type="USER")
        ChatUser(chat=self.chat2, user=self.user1).save()
        ChatUser(chat=self.chat2, user=self.user2).save()
        ChatUser(chat=self.chat2, user=self.user3).save()
        self.message2 = mixer.blend(
            ChatMessage, chat=self.chat2, sender=self.user1, richDescription="Test2"
        )

        self.subscription = mixer.blend(WebPushSubscription, user=self.user2)

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_group_notification_push_send(self, send_web_push_notification):
        self.user2.profile.is_chat_group_notification_push_enabled = True
        self.user2.profile.save()
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message.id,
            self.user1.id,
        ).apply()
        send_web_push_notification.assert_called_once()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_group_notification_push_not_send(self, send_web_push_notification):
        self.user2.profile.is_chat_group_notification_push_enabled = False
        self.user2.profile.save()
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message.id,
            self.user1.id,
        ).apply()
        send_web_push_notification.assert_not_called()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_group_notification_push_not_send_to_self(
        self, send_web_push_notification
    ):
        self.user2.profile.is_chat_group_notification_push_enabled = True
        self.user2.profile.save()
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message.id,
            self.user2.id,
        ).apply()
        send_web_push_notification.assert_not_called()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_group_notification_push_not_send_to_muted_chat_group(
        self, send_web_push_notification
    ):
        self.user2.profile.is_chat_group_notification_push_enabled = True
        self.user2.profile.save()
        self.chat.set_is_notification_push_enabled(self.user2, False)
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message.id,
            self.user1.id,
        ).apply()
        send_web_push_notification.assert_not_called()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_users_notification_push_send(self, send_web_push_notification):
        self.user2.profile.is_chat_users_notification_push_enabled = True
        self.user2.profile.save()
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message2.id,
            self.user1.id,
        ).apply()
        send_web_push_notification.assert_called_once()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_users_notification_push_not_send(self, send_web_push_notification):
        self.user2.profile.is_chat_users_notification_push_enabled = False
        self.user2.profile.save()
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message2.id,
            self.user1.id,
        ).apply()
        send_web_push_notification.assert_not_called()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_users_notification_push_not_send_to_self(
        self, send_web_push_notification
    ):
        self.user2.profile.is_chat_users_notification_push_enabled = True
        self.user2.profile.save()
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message2.id,
            self.user2.id,
        ).apply()
        send_web_push_notification.assert_not_called()

    @override_config(PUSH_NOTIFICATIONS_ENABLED=True)
    @mock.patch("core.tasks.notification_tasks.send_web_push_notification")
    def test_chat_users_notification_push_not_send_to_muted_chat_group(
        self, send_web_push_notification
    ):
        self.user2.profile.is_chat_users_notification_push_enabled = True
        self.user2.profile.save()
        self.chat2.set_is_notification_push_enabled(self.user2, False)
        create_chat_webpush_notification.s(
            connection.schema_name,
            "chat_messaged",
            "chat.chatMessage",
            self.message2.id,
            self.user1.id,
        ).apply()
        send_web_push_notification.assert_not_called()
