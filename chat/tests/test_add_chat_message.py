from unittest import mock
from unittest.mock import call, patch

from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class AddChatMessageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(Group, owner=self.user1, name="Group 1")
        self.group1.join(self.user1, "member")
        self.chat = mixer.blend(Chat, group=self.group1)

        self.mutation = """
            mutation ($input: addChatMessageInput!) {
                addChatMessage(input: $input) {
                    chatMessage {
                        guid
                        richDescription
                        sender {
                            guid
                            name
                        }
                        chat {
                            guid
                        }
                        replyCount
                    }
                }
            }
        """

        self.data = {
            "input": {
                "chatGuid": self.chat.guid,
                "richDescription": "Test",
            }
        }

    def test_add_chat_message_anon(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_add_chat_message_not_authorized(self):
        self.graphql_client.force_login(self.user2)
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(self.mutation, variables=self.data)

    @mock.patch("core.tasks.create_notification.delay")
    def test_add_chat_message_user(self, create_notification):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)
        chat_message = response["data"]["addChatMessage"]["chatMessage"]
        expected = self.data["input"]

        self.assertEqual(chat_message["richDescription"], expected["richDescription"])
        self.assertEqual(chat_message["sender"]["guid"], self.user1.guid)
        self.assertEqual(chat_message["chat"]["guid"], self.chat.guid)
        self.assertFalse(create_notification.called)

    @mock.patch(
        "chat.models.ChatMessage.mentioned_users", new_callable=mock.PropertyMock
    )
    @mock.patch("core.tasks.create_notification.delay")
    def test_add_chat_message_with_mention(self, create_notification, mentioned_users):
        mentioned_user = UserFactory()
        mentioned_users.return_value = [mentioned_user.guid]

        self.graphql_client.force_login(self.user1)
        self.graphql_client.post(self.mutation, variables=self.data)

        self.assertTrue(create_notification.called)

    @patch("chat.resolvers.mutation.redis")
    def test_add_subscription_message(self, mock_redis):
        mock_redis_instance = mock_redis.from_url.return_value

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["addChatMessage"]["chatMessage"]["richDescription"],
            self.data["input"]["richDescription"],
        )
        self.assertEqual(
            response["data"]["addChatMessage"]["chatMessage"]["sender"]["guid"],
            self.user1.guid,
        )

        mock_redis_instance.publish.assert_called_once_with(
            "%s.chat.%s" % (self.tenant.schema_name, self.chat.guid),
            '{"action": "add", "guid": "%s"}'
            % response["data"]["addChatMessage"]["chatMessage"]["guid"],
        )


class AddChatMessageReactionTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(Group, owner=self.user1, name="Group 1")
        self.group1.join(self.user1, "member")
        self.chat = mixer.blend(Chat, group=self.group1)
        self.chat_message = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, rich_description="Test"
        )
        self.chat_message1 = mixer.blend(
            ChatMessage,
            chat=self.chat,
            sender=self.user1,
            rich_description="Test",
            parent=self.chat_message,
        )

        self.mutation = """
            mutation ($input: addChatMessageInput!) {
                addChatMessage(input: $input) {
                    chatMessage {
                        guid
                        richDescription
                        sender {
                            guid
                            name
                        }
                        replyCount
                    }
                }
            }
        """

        self.data = {
            "input": {
                "chatMessageGuid": self.chat_message.guid,
                "richDescription": "Test reactie",
            }
        }

    def test_add_chat_message_anon(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_add_chat_message_not_authorized(self):
        self.graphql_client.force_login(self.user2)
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_add_chat_message_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["addChatMessage"]["chatMessage"]["richDescription"],
            self.data["input"]["richDescription"],
        )
        self.assertEqual(
            response["data"]["addChatMessage"]["chatMessage"]["sender"]["guid"],
            self.user1.guid,
        )

        self.chat_message.refresh_from_db()
        self.assertEqual(self.chat_message.replies.count(), 2)

    def test_add_chat_message_parent_parent(self):
        self.graphql_client.force_login(self.user1)
        data = self.data
        data["input"]["chatMessageGuid"] = self.chat_message1.guid
        with self.assertGraphQlError("invalid_parent"):
            self.graphql_client.post(self.mutation, variables=data)

    @patch("chat.resolvers.mutation.redis")
    def test_add_subscription_message(self, mock_redis):
        mock_redis_instance = mock_redis.from_url.return_value

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["addChatMessage"]["chatMessage"]["richDescription"],
            self.data["input"]["richDescription"],
        )
        self.assertEqual(
            response["data"]["addChatMessage"]["chatMessage"]["sender"]["guid"],
            self.user1.guid,
        )

        expected_calls = [
            call(
                "%s.chat.%s" % (self.tenant.schema_name, self.chat_message.guid),
                '{"action": "add", "guid": "%s"}'
                % response["data"]["addChatMessage"]["chatMessage"]["guid"],
            ),
            call(
                "%s.chat.%s" % (self.tenant.schema_name, self.chat_message.chat.guid),
                '{"action": "edit", "guid": "%s"}' % self.chat_message.guid,
            ),
        ]
        mock_redis_instance.publish.assert_has_calls(expected_calls)
