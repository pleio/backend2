from chat.factories import ChatFactory
from chat.models import ChatMessage
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestChatMassageAttachment(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.chat = ChatFactory(group=self.group)
        self.chat_agent = UserFactory()
        self.group.join(self.chat_agent)
        self.attachment = FileFactory(owner=self.chat_agent)

        self.mutation = """
        mutation CreateChatMessageWithAttachment($input: addChatMessageInput!) {
            addChatMessage(input: $input) {
                chatMessage {
                    guid
                    sender { guid }
                    chat { guid }
                }
            }
        }
        """

        self.variables = {
            "input": {
                "chatGuid": self.chat.guid,
                "richDescription": self.tiptap_attachment(self.attachment),
            }
        }

    def test_attach_file_to_chat_message(self):
        self.graphql_client.force_login(self.chat_agent)
        response = self.graphql_client.post(self.mutation, variables=self.variables)
        chat_message_data = response["data"]["addChatMessage"]["chatMessage"]

        self.assertIsNotNone(chat_message_data["guid"])
        chat_message = ChatMessage.objects.get(id=chat_message_data["guid"])

        self.assertEqual(chat_message_data["sender"]["guid"], self.chat_agent.guid)
        self.assertEqual(chat_message_data["chat"]["guid"], self.chat.guid)
        self.assertEqual(
            [a.file for a in chat_message.attachments.all()], [self.attachment]
        )
