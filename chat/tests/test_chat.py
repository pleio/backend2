from django.test import override_settings
from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage, ChatReaction, ChatUser
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class ChatTestCase(PleioTenantTestCase):
    def setUp(self):
        super(ChatTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(
            Group, owner=self.user1, name="Group 1", is_chat_enabled=True
        )
        self.group1.join(self.user1, "owner")
        self.group1.join(self.user2, "member")

        self.chat1 = mixer.blend(Chat, group=self.group1)
        ChatUser(chat=self.chat1, user=self.user1).save()
        ChatUser(chat=self.chat1, user=self.user2).save()
        self.chat_messages = mixer.cycle(3).blend(
            ChatMessage, chat=self.chat1, sender=self.user1, rich_description="test"
        )
        self.chat_messages += mixer.cycle(2).blend(
            ChatMessage,
            chat=self.chat1,
            sender=self.user2,
            rich_description="test user 2",
        )
        self.thread1 = mixer.cycle(2).blend(
            ChatMessage,
            chat=self.chat1,
            parent=self.chat_messages[1],
            sender=self.user1,
            rich_description="test",
        )
        self.thread2 = mixer.cycle(3).blend(
            ChatMessage,
            chat=self.chat1,
            parent=self.chat_messages[3],
            sender=self.user1,
            rich_description="test",
        )

        self.query = """
            query Chat($chatGuid: String, $chatMessageGuid: String) {
                    chat(chatGuid: $chatGuid, chatMessageGuid: $chatMessageGuid) {
                        guid
                        type
                        container {
                            group {
                                guid
                                name
                            }
                            message {
                                guid
                            }
                        }
                        videoCallToken
                        messages(offset: 0, limit: 4) {
                            total
                            edges {
                                guid
                                richDescription
                                reactions {
                                    unicode
                                    users
                                    count
                                }
                                sender {
                                    guid
                                    name
                                }
                                replyCount
                                canEdit
                            }
                        }
                        isNotificationPushEnabled
                    __typename
                }
            }
        """

    def test_chat_anon(self):
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(
                self.query, variables={"chatGuid": self.chat1.guid}
            )

    @override_settings(INTEGRATED_TOKEN_ONLY_VIDEO_CALL_URL="domain@url.nl")
    @override_settings(INTEGRATED_TOKEN_ONLY_VIDEO_CALL_SECRET="fake_token")
    def test_chat_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.query, variables={"chatGuid": self.chat1.guid}
        )

        self.assertEqual(response["data"]["chat"]["guid"], self.chat1.guid)
        self.assertEqual(response["data"]["chat"]["type"], "group")
        self.assertEqual(
            response["data"]["chat"]["container"]["group"]["guid"],
            self.chat1.group.guid,
        )
        self.assertEqual(response["data"]["chat"]["messages"]["total"], 5)
        self.assertEqual(
            response["data"]["chat"]["messages"]["edges"][0]["guid"],
            self.chat_messages[4].guid,
        )
        self.assertEqual(
            response["data"]["chat"]["messages"]["edges"][3]["replyCount"],
            len(self.thread1),
        )
        self.assertEqual(
            response["data"]["chat"]["messages"]["edges"][1]["replyCount"],
            len(self.thread2),
        )
        self.assertEqual(
            response["data"]["chat"]["messages"]["edges"][1]["reactions"], []
        )
        self.assertEqual(len(response["data"]["chat"]["messages"]["edges"]), 4)
        self.assertEqual(response["data"]["chat"]["isNotificationPushEnabled"], True)
        self.assertNotEqual(response["data"]["chat"]["videoCallToken"], "")

    def test_chat_replies_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.query, variables={"chatMessageGuid": self.chat_messages[1].guid}
        )

        self.assertEqual(response["data"]["chat"]["guid"], self.chat_messages[1].guid)
        self.assertEqual(response["data"]["chat"]["type"], "message")
        self.assertEqual(response["data"]["chat"]["container"]["group"], None)
        self.assertEqual(
            response["data"]["chat"]["container"]["message"]["guid"],
            self.chat_messages[1].guid,
        )
        self.assertEqual(
            response["data"]["chat"]["messages"]["edges"][0]["guid"],
            self.thread1[1].guid,
        )
        self.assertEqual(
            response["data"]["chat"]["messages"]["total"], len(self.thread1)
        )
        self.assertEqual(len(response["data"]["chat"]["messages"]["edges"]), 2)
        self.assertEqual(response["data"]["chat"]["isNotificationPushEnabled"], None)

    @override_settings(INTEGRATED_TOKEN_ONLY_VIDEO_CALL_URL="domain@url.nl")
    @override_settings(INTEGRATED_TOKEN_ONLY_VIDEO_CALL_SECRET="fake_token")
    def test_chat_user_reaction(self):
        ChatReaction.objects.create(
            chat_message=self.chat_messages[1], user=self.user1, unicode="U+1F600"
        )
        ChatReaction.objects.create(
            chat_message=self.chat_messages[1], user=self.user2, unicode="U+1F600"
        )
        ChatReaction.objects.create(
            chat_message=self.chat_messages[1], user=self.user1, unicode="U+1F602"
        )
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.query, variables={"chatGuid": self.chat1.guid}
        )
        self.assertEqual(
            response["data"]["chat"]["messages"]["edges"][3]["reactions"],
            [
                {
                    "count": 2,
                    "unicode": "U+1F600",
                    "users": [self.user1.name, self.user2.name],
                },
                {"count": 1, "unicode": "U+1F602", "users": [self.user1.name]},
            ],
        )


class GroupChatTestCase(PleioTenantTestCase):
    def setUp(self):
        super(GroupChatTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(
            Group, owner=self.user1, name="Group 1", is_chat_enabled=True
        )
        self.group1.join(self.user1, "owner")
        self.group1.join(self.user2, "member")

        self.chat1 = mixer.blend(Chat, group=self.group1)
        ChatUser(chat=self.chat1, user=self.user1).save()
        ChatUser(chat=self.chat1, user=self.user2).save()
        self.chat_messages = mixer.cycle(3).blend(
            ChatMessage, chat=self.chat1, sender=self.user1, rich_description="test"
        )
        self.chat_messages += mixer.cycle(2).blend(
            ChatMessage,
            chat=self.chat1,
            sender=self.user2,
            rich_description="test user 2",
        )
        self.thread1 = mixer.cycle(2).blend(
            ChatMessage,
            chat=self.chat1,
            parent=self.chat_messages[1],
            sender=self.user1,
            rich_description="test",
        )
        self.thread2 = mixer.cycle(3).blend(
            ChatMessage,
            chat=self.chat1,
            parent=self.chat_messages[3],
            sender=self.user1,
            rich_description="test",
        )

        self.group_query = """
            query GroupChat($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ... on Group {
                        name
                        chat {
                            guid
                            messages(offset: 1, limit: 3) {
                                total
                                edges {
                                    guid
                                    richDescription
                                    sender {
                                        guid
                                        name
                                    }
                                    replyCount
                                    canEdit
                                }
                            }
                            unread
                            isNotificationPushEnabled
                        }
                    }
                    __typename
                }
            }
        """

    def test_group_chat_anon(self):
        self.override_config(IS_CLOSED=False)
        response = self.graphql_client.post(
            self.group_query, variables={"guid": self.chat1.group.guid}
        )
        self.assertEqual(response["data"]["entity"]["guid"], self.chat1.group.guid)
        self.assertEqual(response["data"]["entity"]["chat"], None)

    def test_group_chat_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.group_query, variables={"guid": self.chat1.group.guid}
        )

        self.assertEqual(response["data"]["entity"]["chat"]["guid"], self.chat1.guid)
        self.assertEqual(response["data"]["entity"]["guid"], self.chat1.group.guid)
        self.assertEqual(response["data"]["entity"]["chat"]["messages"]["total"], 5)
        self.assertEqual(
            response["data"]["entity"]["chat"]["isNotificationPushEnabled"], True
        )

        self.assertEqual(
            response["data"]["entity"]["chat"]["messages"]["edges"][0]["guid"],
            self.chat_messages[3].guid,
        )
        self.assertEqual(
            response["data"]["entity"]["chat"]["messages"]["edges"][2]["replyCount"],
            len(self.thread1),
        )
        self.assertEqual(
            response["data"]["entity"]["chat"]["messages"]["edges"][0]["replyCount"],
            len(self.thread2),
        )
        self.assertEqual(
            len(response["data"]["entity"]["chat"]["messages"]["edges"]), 3
        )

    def test_user_can_edit(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.group_query, variables={"guid": self.chat1.group.guid}
        )

        self.assertFalse(
            response["data"]["entity"]["chat"]["messages"]["edges"][0]["canEdit"]
        )
        self.assertTrue(
            response["data"]["entity"]["chat"]["messages"]["edges"][1]["canEdit"]
        )
        self.assertTrue(
            response["data"]["entity"]["chat"]["messages"]["edges"][2]["canEdit"]
        )

    def test_group_chat_unread(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.group_query, variables={"guid": self.chat1.group.guid}
        )

        self.assertEqual(response["data"]["entity"]["chat"]["guid"], self.chat1.guid)
        self.assertEqual(response["data"]["entity"]["guid"], self.chat1.group.guid)
        self.assertEqual(response["data"]["entity"]["chat"]["unread"], 10)
        self.assertEqual(
            response["data"]["entity"]["chat"]["isNotificationPushEnabled"], True
        )

        self.chat_messages[0].mark_read(self.user1)

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.group_query, variables={"guid": self.chat1.group.guid}
        )

        self.assertEqual(response["data"]["entity"]["chat"]["guid"], self.chat1.guid)
        self.assertEqual(response["data"]["entity"]["guid"], self.chat1.group.guid)
        self.assertEqual(response["data"]["entity"]["chat"]["unread"], 9)
        self.assertEqual(
            response["data"]["entity"]["chat"]["isNotificationPushEnabled"], True
        )
        self.chat1.mark_chat_messages_read(self.user1)

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.group_query, variables={"guid": self.chat1.group.guid}
        )

        self.assertEqual(response["data"]["entity"]["chat"]["guid"], self.chat1.guid)
        self.assertEqual(response["data"]["entity"]["guid"], self.chat1.group.guid)
        self.assertEqual(response["data"]["entity"]["chat"]["unread"], 0)
        self.assertEqual(
            response["data"]["entity"]["chat"]["isNotificationPushEnabled"], True
        )
