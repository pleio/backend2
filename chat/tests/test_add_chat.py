from chat.models import Chat, ChatUser
from core import override_local_config
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class AddChatTestCase(PleioTenantTestCase):
    def setUp(self):
        super(AddChatTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.user3 = UserFactory()
        self.user4 = UserFactory()

        self.mutation = """
            mutation ($input: addChatInput!) {
                addChat(input: $input) {
                    chat {
                        guid
                        type
                        container {
                            group {
                                guid
                                name
                            }
                            message {
                                guid
                            }
                            users {
                                guid
                            }
                        }
                        __typename
                    }
                }
            }
        """

        self.chat = Chat(type=Chat.Types.USER)
        self.chat.save()
        ChatUser(chat=self.chat, user=self.user1).save()
        ChatUser(chat=self.chat, user=self.user4).save()

    @override_local_config(CHAT_ENABLED=True)
    def test_add_chat_anon(self):
        data = {"input": {"userGuids": [self.user2.guid, self.user3.guid]}}
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=data)

    @override_local_config(CHAT_ENABLED=False)
    def test_add_chat_not_authorized(self):
        data = {"input": {"userGuids": [self.user2.guid, self.user3.guid]}}
        self.graphql_client.force_login(self.user1)
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(self.mutation, variables=data)

    @override_local_config(CHAT_ENABLED=True)
    def test_add_chat_user(self):
        data = {"input": {"userGuids": [self.user2.guid, self.user3.guid]}}
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=data)

        self.assertEqual(response["data"]["addChat"]["chat"]["type"], "user")

        expected_guids = [self.user2.guid, self.user3.guid]
        response_guids = []
        for item in response["data"]["addChat"]["chat"]["container"]["users"]:
            response_guids.append(item["guid"])

        self.assertCountEqual(expected_guids, response_guids)

    @override_local_config(CHAT_ENABLED=True)
    def test_add_chat_user_existing(self):
        data = {"input": {"userGuids": [self.user4.guid]}}
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=data)

        self.assertEqual(response["data"]["addChat"]["chat"]["type"], "user")
        self.assertEqual(response["data"]["addChat"]["chat"]["guid"], self.chat.guid)

        expected_guids = [self.user4.guid]
        response_guids = []
        for item in response["data"]["addChat"]["chat"]["container"]["users"]:
            response_guids.append(item["guid"])

        self.assertCountEqual(expected_guids, response_guids)

    @override_local_config(CHAT_ENABLED=True)
    def test_add_chat_user_remove_duplicate(self):
        data = {
            "input": {"userGuids": [self.user2.guid, self.user3.guid, self.user2.guid]}
        }
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=data)

        self.assertEqual(response["data"]["addChat"]["chat"]["type"], "user")

        expected_guids = [self.user2.guid, self.user3.guid]
        response_guids = []
        for item in response["data"]["addChat"]["chat"]["container"]["users"]:
            response_guids.append(item["guid"])

        self.assertCountEqual(expected_guids, response_guids)
