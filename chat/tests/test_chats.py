from mixer.backend.django import mixer

from chat.models import Chat, ChatMessageView, ChatUser
from core import override_local_config
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class ChatsTestCase(PleioTenantTestCase):
    def setUp(self):
        super(ChatsTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.user3 = UserFactory()
        self.user4 = UserFactory()

        self.group1 = mixer.blend(Group, owner=self.user1, name="Group 1")
        self.group2 = mixer.blend(
            Group, owner=self.user2, name="Group 2", is_chat_enabled=True
        )
        self.group3 = mixer.blend(
            Group, owner=self.user2, name="Group 3", is_chat_enabled=True
        )
        self.group4 = mixer.blend(
            Group, owner=self.user2, name="Group 4", is_chat_enabled=False
        )
        self.group5 = mixer.blend(
            Group, owner=self.user2, name="Group 5", is_chat_enabled=True
        )
        self.group1.join(self.user1, "member")
        self.group1.join(self.user2, "member")
        self.group2.join(self.user2, "member")
        self.group3.join(self.user2, "member")
        self.group4.join(self.user2, "member")
        self.group5.join(self.user2, "pending")

        self.chat1 = mixer.blend(Chat, group=self.group1)
        self.chat2 = mixer.blend(Chat, group=self.group2)
        self.chat3 = mixer.blend(Chat, group=self.group3)
        self.chat4 = mixer.blend(Chat, group=self.group4)
        self.chat5 = mixer.blend(Chat, group=self.group5)

        self.query = """
            query ChatList(
                    $offset: Int
                    $limit: Int
                    $unread: Boolean,
                    $type: ChatTypeFilter) {
                    chats(
                        offset: $offset
                        limit: $limit
                        unread: $unread,
                        type: $type) {
                    total
                    totalUnread
                    edges {
                        guid
                        type
                        container {
                            group {
                                guid
                                name
                            }
                            users {
                                guid
                            }
                        }
                        __typename
                    }
                    __typename
                }
            }
        """

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_anon(self):
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 0)

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_user(self):
        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 2)
        self.assertEqual(response["data"]["chats"]["edges"][0]["type"], "group")
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 2",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][1]["container"]["group"]["name"],
            "Group 3",
        )

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 0)

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_enable_group_chat(self):
        self.group1.is_chat_enabled = True
        self.group1.save()
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 1)
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 1",
        )
        self.group1.is_chat_enabled = False
        self.group1.save()

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_limit_offset(self):
        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 1}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 1)
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 2",
        )

        response = self.graphql_client.post(
            self.query, variables={"offset": 1, "limit": 1}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 1)
        self.assertEqual(response["data"]["chats"]["totalUnread"], 0)
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 3",
        )

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_order(self):
        self.chat3.messages.create(
            sender=self.user2, rich_description="Test message", chat=self.chat3
        )

        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 2)
        self.assertEqual(response["data"]["chats"]["totalUnread"], 1)
        self.assertEqual(response["data"]["chats"]["edges"][0]["type"], "group")
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 3",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][1]["container"]["group"]["name"],
            "Group 2",
        )

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_unread(self):
        self.chat3.messages.create(
            sender=self.user2, rich_description="Test message 1", chat=self.chat3
        )
        self.chat3.messages.create(
            sender=self.user2, rich_description="Test message 2", chat=self.chat3
        )

        ChatMessageView.objects.create(
            viewer=self.user2, chat_message=self.chat3.messages.first()
        )

        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20, "unread": True}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 1)
        self.assertEqual(response["data"]["chats"]["totalUnread"], 1)
        self.assertEqual(response["data"]["chats"]["edges"][0]["type"], "group")
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 3",
        )

    @override_local_config(CHAT_ENABLED=True)
    def test_user_chats_mixed(self):
        # create user chat
        self.chat6 = mixer.blend(Chat, type=Chat.Types.USER)
        ChatUser(chat=self.chat6, user=self.user2).save()
        ChatUser(chat=self.chat6, user=self.user3).save()

        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 3)
        self.assertEqual(response["data"]["chats"]["edges"][0]["type"], "group")
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 2",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][1]["container"]["group"]["name"],
            "Group 3",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][2]["container"]["group"],
            None,
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][2]["type"],
            "user",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][2]["container"]["users"][0]["guid"],
            self.user3.guid,
        )

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 0)

    @override_local_config(CHAT_ENABLED=True)
    def test_chats_type_filter(self):
        # create user chat
        self.chat6 = mixer.blend(Chat, type=Chat.Types.USER)
        ChatUser(chat=self.chat6, user=self.user2).save()
        ChatUser(chat=self.chat6, user=self.user3).save()

        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20, "type": "user"}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 1)
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"],
            None,
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["type"],
            "user",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["users"][0]["guid"],
            self.user3.guid,
        )

        self.graphql_client.force_login(self.user2)
        response = self.graphql_client.post(
            self.query, variables={"offset": 0, "limit": 20, "type": "group"}
        )

        self.assertEqual(len(response["data"]["chats"]["edges"]), 2)
        self.assertEqual(response["data"]["chats"]["edges"][0]["type"], "group")
        self.assertEqual(
            response["data"]["chats"]["edges"][0]["container"]["group"]["name"],
            "Group 2",
        )
        self.assertEqual(
            response["data"]["chats"]["edges"][1]["container"]["group"]["name"],
            "Group 3",
        )
