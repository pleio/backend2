from unittest.mock import patch

from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class EditChatMessageTestCase(PleioTenantTestCase):
    def setUp(self):
        super(EditChatMessageTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(Group, owner=self.user1, name="Group 1")
        self.group1.join(self.user1, "member")
        self.chat = mixer.blend(Chat, group=self.group1)
        self.message = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, richDescription="Test"
        )
        self.message_reply = mixer.blend(
            ChatMessage,
            chat=self.chat,
            sender=self.user1,
            richDescription="Test",
            parent=self.message,
        )

        self.mutation = """
            mutation ($input: editChatMessageInput!) {
                editChatMessage(input: $input) {
                    chatMessage {
                        guid
                        richDescription
                        sender {
                            guid
                            name
                        }
                        canEdit
                    }
                }
            }
        """

        self.data = {
            "input": {
                "guid": self.message.guid,
                "richDescription": "Test 123",
            }
        }

    def test_edit_chat_message_anon(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_edit_chat_message_not_authorized(self):
        self.graphql_client.force_login(self.user2)
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_edit_chat_message_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["editChatMessage"]["chatMessage"]["richDescription"],
            self.data["input"]["richDescription"],
        )
        self.assertEqual(
            response["data"]["editChatMessage"]["chatMessage"]["sender"]["guid"],
            self.user1.guid,
        )

    @patch("chat.resolvers.mutation.redis")
    def test_edit_subscription_message(self, mock_redis):
        mock_redis_instance = mock_redis.from_url.return_value

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["editChatMessage"]["chatMessage"]["richDescription"],
            self.data["input"]["richDescription"],
        )
        self.assertEqual(
            response["data"]["editChatMessage"]["chatMessage"]["sender"]["guid"],
            self.user1.guid,
        )

        mock_redis_instance.publish.assert_called_once_with(
            "%s.chat.%s" % (self.tenant.schema_name, self.chat.guid),
            '{"action": "edit", "guid": "%s"}' % self.message.guid,
        )

    @patch("chat.resolvers.mutation.redis")
    def test_edit_reply_subscription_message(self, mock_redis):
        mock_redis_instance = mock_redis.from_url.return_value

        data = {
            "input": {
                "guid": self.message_reply.guid,
                "richDescription": "Test 321",
            }
        }

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=data)

        self.assertEqual(
            response["data"]["editChatMessage"]["chatMessage"]["richDescription"],
            data["input"]["richDescription"],
        )
        self.assertEqual(
            response["data"]["editChatMessage"]["chatMessage"]["sender"]["guid"],
            self.user1.guid,
        )

        mock_redis_instance.publish.assert_called_once_with(
            "%s.chat.%s" % (self.tenant.schema_name, self.message.guid),
            '{"action": "edit", "guid": "%s"}' % self.message_reply.guid,
        )
