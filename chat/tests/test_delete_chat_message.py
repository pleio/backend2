from unittest.mock import call, patch

from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class DeleteChatMessageTestCase(PleioTenantTestCase):
    def setUp(self):
        super(DeleteChatMessageTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(Group, owner=self.user1, name="Group 1")
        self.group1.join(self.user1, "member")
        self.chat = mixer.blend(Chat, group=self.group1)
        self.message = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, richDescription="Test"
        )
        self.message2 = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, richDescription="Test"
        )
        self.message_reply = mixer.blend(
            ChatMessage,
            chat=self.chat,
            sender=self.user1,
            richDescription="Test",
            parent=self.message2,
        )

        self.mutation = """
            mutation ($input: deleteChatMessageInput!) {
                deleteChatMessage(input: $input) {
                    success
                }
            }
        """

        self.data = {"input": {"guid": self.message.guid}}

    def test_delete_chat_message_anon(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_delete_chat_message_not_authorized(self):
        self.graphql_client.force_login(self.user2)
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(self.mutation, variables=self.data)

    def test_delete_chat_message_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(response["data"]["deleteChatMessage"]["success"], True)

        with self.assertRaises(ChatMessage.DoesNotExist):
            ChatMessage.objects.get(id=self.message.id)

    @patch("chat.resolvers.mutation.redis")
    def test_delete_reply_subscription_message(self, mock_redis):
        mock_redis_instance = mock_redis.from_url.return_value

        data = {"input": {"guid": self.message_reply.guid}}

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=data)

        self.assertEqual(response["data"]["deleteChatMessage"]["success"], True)

        expected_calls = [
            call(
                "%s.chat.%s" % (self.tenant.schema_name, self.message2.guid),
                '{"action": "delete", "guid": "%s"}' % self.message_reply.guid,
            ),
            call(
                "%s.chat.%s" % (self.tenant.schema_name, self.message2.chat.guid),
                '{"action": "edit", "guid": "%s"}' % self.message2.guid,
            ),
        ]
        mock_redis_instance.publish.assert_has_calls(expected_calls)

    @patch("chat.resolvers.mutation.redis")
    def test_delete_subscription_message(self, mock_redis):
        mock_redis_instance = mock_redis.from_url.return_value

        data = {"input": {"guid": self.message2.guid}}

        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=data)

        self.assertEqual(response["data"]["deleteChatMessage"]["success"], True)

        mock_redis_instance.publish.assert_called_once_with(
            "%s.chat.%s" % (self.tenant.schema_name, self.chat.guid),
            '{"action": "delete", "guid": "%s"}' % self.message2.guid,
        )
