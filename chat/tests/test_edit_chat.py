from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage
from core import override_local_config
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class EditChatTestCase(PleioTenantTestCase):
    def setUp(self):
        super(EditChatTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.group1 = mixer.blend(Group, owner=self.user1, name="Group 1")
        self.group1.join(self.user1, "member")
        self.chat = mixer.blend(Chat, group=self.group1)

        self.message = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, richDescription="Test"
        )
        self.message_reply = mixer.blend(
            ChatMessage,
            chat=self.chat,
            sender=self.user1,
            richDescription="Test",
            parent=self.message,
        )

        self.mutation = """
            mutation ($input: editChatInput!) {
                editChat(input: $input) {
                chat {
                    isNotificationPushEnabled
                    }
                }
            }
        """

        self.data = {
            "input": {
                "guid": self.chat.guid,
                "isNotificationPushEnabled": False,
            }
        }

    @override_local_config(CHAT_ENABLED=True)
    def test_edit_chat_anon(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=self.data)

    @override_local_config(CHAT_ENABLED=True)
    def test_edit_chat_user(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["editChat"]["chat"]["isNotificationPushEnabled"],
            False,
        )
