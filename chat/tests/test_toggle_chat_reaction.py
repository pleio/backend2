from mixer.backend.django import mixer

from chat.models import Chat, ChatMessage, ChatReaction
from core import override_local_config
from core.models.group import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class ToggleChatReactionTestCase(PleioTenantTestCase):
    def setUp(self):
        super(ToggleChatReactionTestCase, self).setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.user3 = UserFactory()
        self.user4 = UserFactory()

        self.group1 = mixer.blend(
            Group, owner=self.user1, name="Group 1", is_chat_enabled=True
        )
        self.group1.join(self.user1, "owner")
        self.group1.join(self.user2, "member")
        self.group1.join(self.user3, "member")

        self.chat = mixer.blend(Chat, group=self.group1)
        self.chat_message = mixer.blend(
            ChatMessage, chat=self.chat, sender=self.user1, rich_description="test"
        )

        ChatReaction.objects.create(
            chat_message=self.chat_message, user=self.user1, unicode="U+1F600"
        )
        ChatReaction.objects.create(
            chat_message=self.chat_message, user=self.user2, unicode="U+1F600"
        )
        ChatReaction.objects.create(
            chat_message=self.chat_message, user=self.user1, unicode="U+1F602"
        )

        self.mutation = """
            mutation ($input: toggleChatReactionInput!) {
                toggleChatReaction(input: $input) {
                    chatMessage {
                        guid
                        reactions {
                            unicode
                            users
                            count
                        }
                    }
                }
            }
        """

        self.data = {"input": {"guid": self.chat_message.guid, "unicode": "U+1F600"}}

    @override_local_config(CHAT_ENABLED=True)
    def test_toggle_chat_reaction_anon(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables=self.data)

    @override_local_config(CHAT_ENABLED=True)
    def test_toggle_chat_reaction_not_authorized(self):
        self.graphql_client.force_login(self.user4)
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.post(self.mutation, variables=self.data)

    @override_local_config(CHAT_ENABLED=True)
    def test_toggle_chat_reaction_add(self):
        self.graphql_client.force_login(self.user3)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["toggleChatReaction"]["chatMessage"]["reactions"],
            [
                {
                    "count": 3,
                    "unicode": "U+1F600",
                    "users": [self.user1.name, self.user2.name, self.user3.name],
                },
                {"count": 1, "unicode": "U+1F602", "users": [self.user1.name]},
            ],
        )

    @override_local_config(CHAT_ENABLED=True)
    def test_toggle_chat_reaction_remove(self):
        self.graphql_client.force_login(self.user1)
        response = self.graphql_client.post(self.mutation, variables=self.data)

        self.assertEqual(
            response["data"]["toggleChatReaction"]["chatMessage"]["reactions"],
            [
                {
                    "count": 1,
                    "unicode": "U+1F600",
                    "users": [self.user2.name],
                },
                {"count": 1, "unicode": "U+1F602", "users": [self.user1.name]},
            ],
        )
