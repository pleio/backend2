# Generated by Django 4.1.5 on 2023-05-08 10:13

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("chat", "0002_chat_chatmessage_delete_message"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="chat",
            options={"ordering": ["group__name"]},
        ),
        migrations.AlterModelOptions(
            name="chatmessage",
            options={"ordering": ["-created_at"]},
        ),
    ]
