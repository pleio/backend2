# Generated by Django 4.2.8 on 2024-01-31 12:28

import django.db.models.deletion
import django.utils.timezone
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0108_group_is_join_button_hidden"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("chat", "0005_chatmessageview"),
    ]

    operations = [
        migrations.AddField(
            model_name="chat",
            name="type",
            field=models.CharField(
                choices=[("GROUP", "Group"), ("USER", "User")],
                default="GROUP",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="chat",
            name="group",
            field=models.OneToOneField(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="chat",
                to="core.group",
            ),
        ),
        migrations.CreateModel(
            name="ChatUser",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(default=django.utils.timezone.now)),
                (
                    "chat",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="users",
                        to="chat.chat",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="chats",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "unique_together": {("chat", "user")},
            },
        ),
    ]
