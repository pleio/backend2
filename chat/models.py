import uuid

from django.db import models
from django.db.models import Count, Q
from django.utils import timezone

from core import config
from core.constances import ACCESS_TYPE
from core.lib import get_base_url, tenant_schema
from core.models import AttachmentMixin, MentionMixin


class ChatMessage(MentionMixin, AttachmentMixin, models.Model):
    # Don't notify on create.
    should_notify_on_create = False

    class Meta:
        ordering = ["-created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rich_description = models.TextField(null=True, blank=True)
    sender = models.ForeignKey(
        "user.User", related_name="chat_messages", on_delete=models.PROTECT
    )
    chat = models.ForeignKey(
        "chat.Chat", related_name="messages", on_delete=models.CASCADE
    )
    parent = models.ForeignKey(
        "self", blank=True, null=True, related_name="replies", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def guid(self):
        return str(self.id)

    def __str__(self):
        return f"ChatMessage[{self.guid}]"

    def save(self, *args, **kwargs):
        created = self._state.adding
        super(ChatMessage, self).save(*args, **kwargs)
        if created:
            self.create_webpush_notifications()

    def create_webpush_notifications(self):
        from core.tasks import create_chat_webpush_notification

        create_chat_webpush_notification.delay(
            tenant_schema(),
            "chat_messaged",
            "chat.chatMessage",
            self.id,
            self.sender.id,
        )

    def can_read(self, user):
        return self.chat.can_read(user)

    def can_edit(self, user):
        return self.sender == user

    @property
    def rich_fields(self):
        return [self.rich_description]

    @property
    def owner(self):
        return self.sender

    @property
    def group(self):
        return self.chat.group

    @property
    def type_to_string(self):
        return "chat_message"

    @property
    def url(self):
        return self.chat.url

    def mark_read(self, user):
        ChatMessageView.objects.get_or_create(chat_message=self, viewer=user)

    def get_read_access(self):
        if self.chat.group:
            return self.chat.group.get_read_access()
        if config.IS_CLOSED:
            return ACCESS_TYPE.logged_in
        return ACCESS_TYPE.public


class ChatManager(models.Manager):
    def visible(self, user):
        if not user.is_authenticated:
            return self.none()

        return self.get_queryset().filter(
            Q(
                type=Chat.Types.GROUP,
                group__members__user=user,
                group__is_chat_enabled=True,
                group__members__type="member",
            )
            | Q(
                users__user=user,
                type=Chat.Types.USER,
            )
        )


class Chat(models.Model):
    class Meta:
        ordering = ["group__name"]

    class Types(models.TextChoices):
        GROUP = "GROUP"
        USER = "USER"

    objects = ChatManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=20, default="GROUP", choices=Types.choices)
    group = models.OneToOneField(
        "core.Group",
        related_name="chat",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    created_at = models.DateTimeField(default=timezone.now)

    @property
    def guid(self):
        return str(self.id)

    def __str__(self):
        return f"Chat[{self.guid}]"

    def can_read(self, user):
        if self.type == Chat.Types.USER:
            return self.users.filter(user=user).exists()
        elif self.type == Chat.Types.GROUP:
            return self.group.is_full_member(user)

        return False

    @property
    def url(self):
        return f"/chat/{self.guid}"

    @property
    def full_url(self):
        base_url = get_base_url()
        return f"{base_url}/chat/{self.guid}"

    @property
    def title(self):
        if self.type == "GROUP":
            return self.group.name
        if self.type == "USER":
            return ",".join(self.users.all().values_list("user__name", flat=True))
        return None

    def get_is_notification_push_enabled(self, user):
        chat_user = ChatUser.objects.filter(chat=self, user=user).first()
        if chat_user:
            return chat_user.is_notification_push_enabled
        return True

    def set_is_notification_push_enabled(self, user, value):
        chat_user = ChatUser.objects.filter(chat=self, user=user).first()
        if chat_user:
            chat_user.is_notification_push_enabled = value
            chat_user.save()
        else:
            ChatUser(chat=self, user=user, is_notification_push_enabled=value).save()

        return None

    def mark_chat_messages_read(self, user):
        unread_messages = self.messages.exclude(read_by__viewer=user)
        for message in unread_messages:
            message.mark_read(user)

    def unread_messages_count(self, user):
        return self.messages.exclude(read_by__viewer=user).count()

    @staticmethod
    def get_or_create_user_chat(users: set):
        existing_chat = (
            Chat.objects.filter(type=Chat.Types.USER, users__user__in=users)
            .annotate(match_count=Count("users"))
            .filter(match_count=len(users))
            .exclude(users__in=ChatUser.objects.exclude(user__in=users))
            .first()
        )

        if existing_chat:
            return existing_chat

        # Make new chat
        chat = Chat(type=Chat.Types.USER)
        chat.save()
        for user in users:
            ChatUser(chat=chat, user=user).save()
        return chat


class ChatMessageView(models.Model):
    class Meta:
        unique_together = ("chat_message", "viewer")

    chat_message = models.ForeignKey(
        "chat.ChatMessage", on_delete=models.CASCADE, related_name="read_by"
    )
    viewer = models.ForeignKey(
        "user.User",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
    )
    created_at = models.DateTimeField(default=timezone.now)


class ChatUser(models.Model):
    class Meta:
        unique_together = ("chat", "user")

    chat = models.ForeignKey(
        "chat.Chat", related_name="users", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        "user.User", related_name="chats", on_delete=models.CASCADE
    )
    is_notification_push_enabled = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)


class ChatReaction(models.Model):
    class Meta:
        unique_together = ("unicode", "chat_message", "user")
        ordering = ["created_at"]

    unicode = models.CharField(max_length=50)
    chat_message = models.ForeignKey(
        "chat.ChatMessage", on_delete=models.CASCADE, related_name="reactions"
    )
    user = models.ForeignKey(
        "user.User", related_name="chat_reactions", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(default=timezone.now)
