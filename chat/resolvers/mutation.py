import json
import logging

import redis
from ariadne import ObjectType
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from chat.models import Chat, ChatMessage, ChatReaction
from core import config, constances
from core.lib import clean_graphql_input, tenant_schema
from core.resolvers import shared
from user.models import User

logger = logging.getLogger(__name__)

mutation = ObjectType("Mutation")


@mutation.field("addChatMessage")
def resolve_add_chat_message(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    parent = None

    chatGuid = clean_input.get("chatGuid", None)
    chatMessageGuid = clean_input.get("chatMessageGuid", None)

    if chatGuid:
        try:
            chat = Chat.objects.get(id=chatGuid)
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)
    elif chatMessageGuid:
        try:
            chatMessage = ChatMessage.objects.get(id=chatMessageGuid)
            chat = chatMessage.chat
            parent = chatMessage
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)
    else:
        raise GraphQLError(constances.COULD_NOT_FIND)

    if parent and parent.parent:
        raise GraphQLError(constances.INVALID_PARENT)

    if not chat.can_read(user):
        raise GraphQLError(constances.NOT_AUTHORIZED)

    message = ChatMessage()

    message.rich_description = clean_input.get("richDescription")
    message.sender = user
    message.chat = chat
    message.parent = parent
    message.save()

    chat.mark_chat_messages_read(user)

    try:
        r = redis.from_url(
            settings.CELERY_RESULT_BACKEND, encoding="utf-8", decode_responses=True
        )
        channel = "%s.chat.%s" % (tenant_schema(), chatGuid or chatMessageGuid)
        r.publish(channel, json.dumps({"action": "add", "guid": message.guid}))

        if parent:
            channel = "%s.chat.%s" % (tenant_schema(), parent.chat.guid)
            r.publish(channel, json.dumps({"action": "edit", "guid": parent.guid}))
    except Exception as e:
        logger.error(e)

    return {"chatMessage": message}


@mutation.field("editChatMessage")
def resolve_edit_chat_message(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        message = ChatMessage.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(constances.COULD_NOT_FIND)

    if not message.can_edit(user):
        raise GraphQLError(constances.NOT_AUTHORIZED)

    message.rich_description = clean_input.get("richDescription")
    message.save()

    try:
        r = redis.from_url(
            settings.CELERY_RESULT_BACKEND, encoding="utf-8", decode_responses=True
        )
        channel = "%s.chat.%s" % (
            tenant_schema(),
            message.parent.guid if message.parent else message.chat.guid,
        )
        r.publish(channel, json.dumps({"action": "edit", "guid": message.guid}))
    except Exception as e:
        logger.error(e)

    return {"chatMessage": message}


@mutation.field("deleteChatMessage")
def resolve_delete_chat_message(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        message = ChatMessage.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(constances.COULD_NOT_FIND)

    if not message.can_edit(user):
        raise GraphQLError(constances.NOT_AUTHORIZED)

    chat_guid = message.chat.guid
    message_guid = message.guid
    parent = None if not message.parent else message.parent
    message.delete()

    try:
        r = redis.from_url(
            settings.CELERY_RESULT_BACKEND, encoding="utf-8", decode_responses=True
        )
        channel = "%s.chat.%s" % (tenant_schema(), parent.guid if parent else chat_guid)
        r.publish(channel, json.dumps({"action": "delete", "guid": message_guid}))

        if parent:
            channel = "%s.chat.%s" % (tenant_schema(), parent.chat.guid)
            r.publish(channel, json.dumps({"action": "edit", "guid": parent.guid}))
    except Exception as e:
        logger.error(e)

    return {"success": True}


@mutation.field("addChat")
def resolve_create_chat(_, info, input):
    acting_user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(acting_user)

    if not config.CHAT_ENABLED:
        raise GraphQLError(constances.NOT_AUTHORIZED)

    user_guids = clean_input.get("userGuids", [])

    if not user_guids:
        raise GraphQLError(constances.COULD_NOT_FIND)

    users = [acting_user]
    for user_guid in user_guids:
        try:
            users.append(User.objects.get(id=user_guid))
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)

    chat = Chat.get_or_create_user_chat(set(users))

    return {"chat": chat}


@mutation.field("toggleChatReaction")
def resolve_toggle_chat_reaction(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    if not config.CHAT_ENABLED:
        raise GraphQLError(constances.NOT_AUTHORIZED)

    try:
        message = ChatMessage.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(constances.COULD_NOT_FIND)

    if not message.can_read(user):
        raise GraphQLError(constances.NOT_AUTHORIZED)

    unicode = clean_input.get("unicode", None)

    if unicode:
        chat_reaction = ChatReaction.objects.filter(
            chat_message=message, user=user, unicode=unicode
        ).first()
        if chat_reaction:
            chat_reaction.delete()
        else:
            ChatReaction.objects.create(
                chat_message=message, user=user, unicode=unicode
            )

        try:
            r = redis.from_url(
                settings.CELERY_RESULT_BACKEND, encoding="utf-8", decode_responses=True
            )
            channel = "%s.chat.%s" % (
                tenant_schema(),
                message.parent.guid if message.parent else message.chat.guid,
            )
            r.publish(channel, json.dumps({"action": "edit", "guid": message.guid}))
        except Exception as e:
            logger.error(e)

    return {"chatMessage": message}


@mutation.field("editChat")
def resolve_edit_chat(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    if not config.CHAT_ENABLED:
        raise GraphQLError(constances.NOT_AUTHORIZED)

    try:
        chat = Chat.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(constances.COULD_NOT_FIND)

    if "isNotificationPushEnabled" in clean_input:
        chat.set_is_notification_push_enabled(
            user, clean_input.get("isNotificationPushEnabled")
        )

    return {"chat": chat}
