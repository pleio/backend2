from ariadne import ObjectType

from chat.models import Chat, ChatMessage

chat_container = ObjectType("ChatContainer")


@chat_container.field("group")
def resolve_group(obj, info):
    if isinstance(obj, Chat):
        return obj.group
    return None


@chat_container.field("message")
def resolve_chat_message(obj, info):
    if isinstance(obj, ChatMessage):
        return obj
    return None


@chat_container.field("users")
def resolve_chat_users(obj, info):
    acting_user = info.context["request"].user
    if isinstance(obj, Chat):
        return [member.user for member in obj.users.exclude(user=acting_user)]
    return None
