from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count, F, Max, OuterRef, Subquery
from graphql import GraphQLError

from chat.models import Chat, ChatMessage
from core import config, constances

query = ObjectType("Query")


@query.field("chats")
def resolve_chats(_, info, offset=0, limit=20, unread=False, type=None):
    user = info.context["request"].user

    if not user.is_authenticated or not config.CHAT_ENABLED:
        return {
            "total": 0,
            "totalUnread": 0,
            "edges": [],
        }

    qs = Chat.objects.visible(user).annotate(
        last_message_date=Max("messages__created_at")
    )

    qs = qs.annotate(
        unread=Subquery(
            ChatMessage.objects.filter(chat=OuterRef("id"))
            .exclude(read_by__viewer=user)
            .values("chat")
            .annotate(unread=Count("id"))
            .values("unread")
        )
    )

    if unread:
        qs = qs.filter(unread__gt=0)

    if type:
        qs = qs.filter(type=type.upper())

    qs = qs.order_by(F("last_message_date").desc(nulls_last=True), "group__name")

    total_unread = qs.aggregate(total_unread=Count("unread"))["total_unread"]

    return {
        "total": qs.count(),
        "totalUnread": total_unread or 0,
        "edges": qs[offset : offset + limit],
    }


@query.field("chat")
def resolve_chat(_, info, chatGuid=None, chatMessageGuid=None):
    user = info.context["request"].user
    chat = None

    if chatGuid:
        try:
            chat = Chat.objects.get(id=chatGuid)
            if not chat.can_read(user):
                raise GraphQLError(constances.NOT_AUTHORIZED)

            chat.mark_chat_messages_read(user)

            # trigger chats refetch for specific user

        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)
    elif chatMessageGuid:
        try:
            chat_message = ChatMessage.objects.get(id=chatMessageGuid)
            if not chat_message.chat.can_read(user):
                raise GraphQLError(constances.NOT_AUTHORIZED)
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)
    else:
        raise GraphQLError(constances.COULD_NOT_FIND)

    return chat or chat_message
