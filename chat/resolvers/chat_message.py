from ariadne import ObjectType

chat_message = ObjectType("ChatMessage")


@chat_message.field("richDescription")
def resolve_rich_description(obj, info):
    return obj.rich_description


@chat_message.field("sender")
def resolve_sender(obj, info):
    return obj.sender


@chat_message.field("timeCreated")
def resolve_time_created(obj, info):
    return obj.created_at


@chat_message.field("replyCount")
def resolve_reply_count(obj, info):
    return obj.replies.count()


@chat_message.field("canEdit")
def resolve_can_edit(obj, info):
    return obj.can_edit(info.context["request"].user)


@chat_message.field("chat")
def resolve_chat(obj, info):
    return obj.chat


@chat_message.field("reactions")
def resolve_reactions(obj, info):
    reactions = obj.reactions.all()
    reaction_dict = {}

    # We cannot use database queries here because this code is called asynchronously in subscriptions
    for reaction in reactions:
        unicode = reaction.unicode
        if unicode not in reaction_dict:
            reaction_dict[unicode] = {
                "unicode": unicode,
                "count": 0,
                "min_created_at": reaction.created_at,
                "users": [],
            }
        reaction_dict[unicode]["count"] += 1
        reaction_dict[unicode]["users"].append(reaction.user.name)
        if reaction.created_at < reaction_dict[unicode]["min_created_at"]:
            reaction_dict[unicode]["min_created_at"] = reaction.created_at

    return sorted(reaction_dict.values(), key=lambda x: x["min_created_at"])
