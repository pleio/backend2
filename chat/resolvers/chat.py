from ariadne import ObjectType

from chat.models import Chat, ChatMessage
from core.lib import get_video_call_jwt_token

chat = ObjectType("Chat")


@chat.field("type")
def resolve_type(obj, info):
    if isinstance(obj, ChatMessage):
        return "message"

    return obj.type.lower()


@chat.field("container")
def resolve_container(obj, info):
    return obj


@chat.field("unread")
def resolve_unread(obj, info):
    if isinstance(obj, Chat):
        return obj.unread_messages_count(info.context["request"].user)
    return 0


@chat.field("messages")
def resolve_chats(obj, info, offset=0, limit=20):
    if isinstance(obj, Chat):
        messages = obj.messages.filter(parent=None)
    else:
        messages = obj.replies.all()

    return {"total": messages.count(), "edges": messages[offset : offset + limit]}


@chat.field("isNotificationPushEnabled")
def resolve_is_notification_push_enabled(obj, info):
    if isinstance(obj, Chat):
        return obj.get_is_notification_push_enabled(info.context["request"].user)
    return None


@chat.field("videoCallToken")
def resolve_video_call_token(obj, info):
    if isinstance(obj, Chat):
        return get_video_call_jwt_token(obj.id)
    return ""
