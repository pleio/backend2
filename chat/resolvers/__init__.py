from .chat import chat
from .chat_container import chat_container
from .chat_message import chat_message
from .mutation import mutation as mutations
from .mutation import (
    resolve_add_chat_message,
    resolve_delete_chat_message,
    resolve_edit_chat_message,
)
from .query import query
from .subscription import subscription

resolvers = [query, chat, chat_message, chat_container, mutations, subscription]
