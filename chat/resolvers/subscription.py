import json
import logging

import redis.asyncio as redis
from ariadne import SubscriptionType
from asgiref.sync import sync_to_async
from django.conf import settings
from django_tenants.utils import tenant_context
from graphql import GraphQLError

from chat.models import Chat, ChatMessage
from core import constances

logger = logging.getLogger(__name__)


def get_message(tenant, messageGuid, user):
    with tenant_context(tenant):
        message = (
            ChatMessage.objects.filter(id=messageGuid)
            .prefetch_related(
                "sender",
                "sender___profile",
                "sender___profile__user_profile_fields",
                "sender___profile__user_profile_fields__profile_field",
                "sender___profile__avatar_managed_file",
                "chat",
                "chat__group",
                "replies",
                "reactions",
                "reactions__user",
            )
            .first()
        )
        if message:
            message.mark_read(user)

        if message and message.can_read(user):
            return message


def get_chat(tenant, chatGuid, user):
    with tenant_context(tenant):
        chat = (
            Chat.objects.filter(id=chatGuid)
            .prefetch_related("group", "users", "users__user")
            .first()
        )
        if chat.can_read(user):
            return chat


def get_chat_message(tenant, chatMessageGuid, user):
    with tenant_context(tenant):
        chat_message = (
            ChatMessage.objects.filter(id=chatMessageGuid)
            .prefetch_related("chat", "chat__group", "chat__users", "chat__users__user")
            .first()
        )
        if chat_message.chat.can_read(user):
            return chat_message


async def on_chat_update_generator(obj, info, chatGuid=None, chatMessageGuid=None):
    tenant = info.context["request"].scope["tenant"]
    user = info.context["request"].scope["user"]

    if chatGuid:
        chat = await sync_to_async(get_chat)(tenant, chatGuid, user)
        if not chat:
            raise GraphQLError(constances.COULD_NOT_FIND)
    elif chatMessageGuid:
        chat_message = await sync_to_async(get_chat_message)(
            tenant, chatMessageGuid, user
        )
        if not chat_message:
            raise GraphQLError(constances.COULD_NOT_FIND)
    else:
        raise GraphQLError(constances.COULD_NOT_FIND)

    channel = "%s.chat.%s" % (tenant.schema_name, chatGuid or chatMessageGuid)

    r = await redis.from_url(
        settings.CELERY_RESULT_BACKEND, encoding="utf-8", decode_responses=True
    )

    async with r.pubsub() as pubsub:
        await pubsub.subscribe(channel)
        async for message in pubsub.listen():
            if message.get("type", "") == "subscribe":
                logger.info("subscribed to: %s", channel)
            else:
                data = json.loads(message.get("data", "{}"))
                if data.get("action", None):
                    yield {
                        "guid": data.get("guid"),
                        "action": data.get("action"),
                        "message": await sync_to_async(get_message)(
                            tenant, data.get("guid", None), user
                        ),
                    }


def on_chat_update_resolver(message, info, chatGuid=None, chatMessageGuid=None):
    return message


async def on_chats_update_generator(obj, info):
    tenant = info.context["request"].scope["tenant"]
    user = info.context["request"].scope["user"]

    channel = "%s.chat.*" % (tenant.schema_name)

    r = await redis.from_url(
        settings.CELERY_RESULT_BACKEND, encoding="utf-8", decode_responses=True
    )

    async with r.pubsub() as pubsub:
        await pubsub.psubscribe(channel)
        async for message in pubsub.listen():
            if message.get("type", "") == "psubscribe":
                logger.info("subscribed to: %s", channel)
            else:
                data = json.loads(message.get("data", "{}"))
                if data.get("action", None) in ["add", "delete"]:
                    message_for_user = await sync_to_async(get_chat_message)(
                        tenant, data.get("guid", None), user
                    )
                    if message_for_user:
                        yield {"guid": message_for_user.chat.id, "action": "refetch"}


def on_chats_update_resolver(message, info):
    return message


subscription = SubscriptionType()
subscription.set_field("onChatUpdate", on_chat_update_resolver)
subscription.set_source("onChatUpdate", on_chat_update_generator)

subscription.set_field("onChatsUpdate", on_chats_update_resolver)
subscription.set_source("onChatsUpdate", on_chats_update_generator)
