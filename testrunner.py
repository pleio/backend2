import logging
import multiprocessing
import time

import django
from django.conf import settings
from django.db import connections
from django.test.runner import DiscoverRunner, ParallelTestSuite
from django.test.utils import setup_test_environment

_worker_id = 0


def _init_worker(
    counter,
    initial_settings=None,
    serialized_contents=None,
    process_setup=None,
    process_setup_args=None,
    debug_mode=None,
):
    """
    Switch to databases dedicated to this worker.

    This helper lives at module-level because of the multiprocessing module's
    requirements.
    """

    global _worker_id

    with counter.get_lock():
        counter.value += 1
        _worker_id = counter.value

    start_method = multiprocessing.get_start_method()

    if start_method == "spawn":
        if process_setup and callable(process_setup):
            if process_setup_args is None:
                process_setup_args = ()
            process_setup(*process_setup_args)
        django.setup()
        setup_test_environment(debug=debug_mode)

    for alias in connections:
        connection = connections[alias]
        if start_method == "spawn":
            # Restore initial settings in spawned processes.
            connection.settings_dict.update(initial_settings[alias])
            if value := serialized_contents.get(alias):
                connection._test_serialized_contents = value
        connection.creation.setup_worker_connection(_worker_id)

    # below is the code that was added to the original django testrunner

    from elasticsearch_dsl.connections import connections as es_connections

    # each worker needs its own connection to elasticsearch, the ElasticsearchClient uses
    # global connection objects that do not play nice otherwise

    es_connections.create_connection(
        hosts=[settings.ELASTICSEARCH_DSL["default"]["hosts"]],
        alias="default",
        maxsize=15,
    )
    print("Elasticsearch connection created for worker %s" % _worker_id)


class ElasticsearchParallelTestSuite(ParallelTestSuite):
    init_worker = _init_worker


class PleioTestRunner(DiscoverRunner):
    parallel_test_suite = ElasticsearchParallelTestSuite

    # This method sets up the test environment before the tests are run.
    def setup_test_environment(self, *args, **kwargs):
        super().setup_test_environment(*args, **kwargs)

        # disable elasticsearch autosync, we populate the index manually
        settings.ELASTICSEARCH_DSL_AUTOSYNC = False

        logging.disable(logging.CRITICAL)

        # make sure tests start with clean index
        from core.tasks.elasticsearch_tasks import elasticsearch_recreate_indices
        from core.tests.helpers import suppress_stdout

        with suppress_stdout():
            elasticsearch_recreate_indices()
            time.sleep(0.1)

        print("Elasticsearch indices recreated")
