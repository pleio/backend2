import logging
from hashlib import md5

from django.conf import settings
from django.utils import timezone

from core.lib import tenant_api_token

logger = logging.getLogger(__name__)


class ApiTokenData:
    """
    This class is used to validate the checksum and timestamp of the incoming API calls from concierge.
    """

    def __init__(self, request):
        self.request = request
        self._data = None

    @staticmethod
    def flat_data(data):
        return dict(data.items())

    @property
    def data(self):
        if not self._data:
            if self.request.method == "POST":
                self._data = self.flat_data(self.request.POST)
            else:
                self._data = self.flat_data(self.request.GET)
        return self._data

    def assert_valid_checksum(self):
        expected_checksum = md5(tenant_api_token().encode())
        for k, v in sorted(
            self.data.items(), key=lambda x: [str(y).lower() for y in x]
        ):
            if k == "checksum":
                # Checksum is not included in the checksum.
                continue

            expected_checksum.update(str(k).encode())
            expected_checksum.update(str(v).encode())
        assert self.data.get("checksum") == expected_checksum.hexdigest()[:12], (
            "Invalid checksum"
        )

    def assert_valid_timestamp(self):
        assert self.data.get("timestamp"), "Timestamp is missing"
        try:
            due = timezone.now() - timezone.timedelta(
                minutes=int(settings.ACCOUNT_DATA_EXPIRE)
            )
            timestamp = int(self.data.get("timestamp", due.timestamp() - 1))
            assert timestamp > due.timestamp(), "Data expired"
        except ValueError:
            msg = "Invalid timestmap format."
            raise AssertionError(msg)

    def assert_valid(self):
        self.assert_valid_checksum()
        self.assert_valid_timestamp()
