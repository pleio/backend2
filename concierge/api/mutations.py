import logging
import uuid

from concierge.api.client import ConciergeClient
from concierge.constances import REGISTER_ORIGIN_SITE_URL, UPDATE_ORIGIN_SITE_URL
from core.lib import tenant_schema, tenant_summary

logger = logging.getLogger(__name__)


def sync_site():
    client = ConciergeClient("update_origin_site")
    return client.post(
        UPDATE_ORIGIN_SITE_URL,
        {f"origin_site_{key}": value for key, value in tenant_summary().items()},
    )


def submit_user_token(user):
    from concierge.tasks import profile_updated_signal

    client = ConciergeClient("register_origin_site")
    token = uuid.uuid4()
    user.profile.update_origin_token(token)
    url = REGISTER_ORIGIN_SITE_URL.format(user.external_id)

    data = {"origin_token": token}
    data.update(
        {f"origin_site_{key}": value for key, value in tenant_summary().items()}
    )

    client.post(url, data)
    if client.is_ok():
        profile_updated_signal.delay(tenant_schema(), token)
    else:
        user.profile.update_origin_token(None)
        logger.warning(
            "Failed to sync a user origin_token for reason '%s'", client.reason
        )
