import json
import logging
from urllib.parse import urlparse

import requests
from django.conf import settings
from django.core.files.base import ContentFile
from requests import ConnectionError as RequestConnectionError
from requests.exceptions import InvalidSchema, MissingSchema

from core import config
from core.lib import get_account_url

logger = logging.getLogger(__name__)


class ConciergeClient:
    known_errors = (
        AssertionError,
        RequestConnectionError,
        MissingSchema,
        InvalidSchema,
    )

    def __init__(self, resource_id):
        self.method = resource_id
        self.response = None

        self.headers = {
            "x-oidc-client-id": settings.OIDC_RP_CLIENT_ID,
            "x-oidc-client-secret": settings.OIDC_RP_CLIENT_SECRET,
            "accept-language": config.LANGUAGE,
        }

    def fetch(self, resource):
        self.response = None
        try:
            self.response = requests.get(
                get_account_url(resource), headers=self.headers, timeout=30
            )

            assert self.response.ok, self.response.reason

            return self.response.json()
        except self.known_errors as e:
            logger.warning(
                "Error during api call to concierge: %s; %s; %s",
                e.__class__,
                repr(e),
                self.method,
            )
            return {
                "error": str(e),
                "status_code": self.response.status_code if self.response else None,
            }

    def get_file(self, resource):
        self.response = None
        try:
            self.response = requests.get(resource, headers=self.headers, timeout=30)

            if self.response.ok:
                parsed_url = urlparse(self.response.url)
                filename = parsed_url.path.split("/")[-1]
                return ContentFile(self.response.content, name=filename)

            return None
        except self.known_errors as e:
            logger.warning(
                "Error during api call to concierge: %s; %s; %s",
                e.__class__,
                repr(e),
                self.method,
            )
            return {
                "error": str(e),
                "status_code": self.response.status_code if self.response else None,
            }

    def post(self, resource, data, post_json=False):
        self.response = None
        try:
            headers = {**self.headers}
            if post_json:
                data = json.dumps(data)
                headers["content_type"] = "application/json"

            self.response = requests.post(
                get_account_url(resource), data=data, headers=headers, timeout=30
            )

            assert self.response.ok, self.response.reason

            return self.response.json()
        except self.known_errors as e:
            logger.warning(
                "Error during api call to concierge: %s; %s; %s",
                e.__class__,
                repr(e),
                self.method,
            )
            return {
                "error": str(e),
                "status_code": self.response.status_code if self.response else None,
            }

    def is_ok(self):
        return self.response.ok if self.response else False

    @property
    def reason(self):
        return self.response.reason or "" if self.response else ""
