import logging

from concierge.api.client import ConciergeClient
from concierge.constances import (
    FETCH_AVATAR_URL,
    FETCH_MAIL_PROFILE_URL,
    FETCH_PROFILE_URL,
    GET_OR_CREATE_USER,
)
from core.lib import get_base_url
from user.models import User

logger = logging.getLogger(__name__)


def fetch_avatar(user: User):
    client = ConciergeClient("fetch_avatar")
    return client.fetch(FETCH_AVATAR_URL.format(user.email))


def fetch_mail_profile(email):
    client = ConciergeClient("fetch_mail_profile")
    return client.fetch(FETCH_MAIL_PROFILE_URL.format(email))


def get_or_create_user(email, actor_name, next=None):
    client = ConciergeClient("get_or_create")
    return client.post(
        GET_OR_CREATE_USER,
        post_json=True,
        data={
            "email": email,
            "next": next,
            "actor_name": actor_name,
            "origin_site_url": get_base_url(),
        },
    )


def fetch_profile(user: User):
    try:
        assert user.external_id, "No external ID found yet"

        client = ConciergeClient("fetch_profile")
        return client.fetch(FETCH_PROFILE_URL.format(user.external_id))

    except AssertionError as e:
        logger.warning("Error during fetch_profile: %s; %s", e.__class__, repr(e))
        return {
            "error": str(e),
        }
