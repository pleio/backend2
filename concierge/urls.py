from django.urls import path

from concierge import views

# Prefix for the django.urls.reverse method.
app_name = "api"

urlpatterns = [
    path(
        "site_info/",
        views.get_site_info,
        name="site_info",
    ),
    path(
        "profile/update/",
        views.profile_updated,
        name="profile_updated",
    ),
    path(
        "profile/ban/",
        views.ban_user,
        name="profile_banned",
    ),
    path(
        "profile/terminate_session/",
        views.terminate_session,
        name="terminate_session",
    ),
]
