from django.urls import reverse

from core.tests.helpers import PleioTenantTestCase


class TestReverseUrlTestCase(PleioTenantTestCase):
    def test_reverse_url(self):
        self.assertEqual(reverse("api:site_info"), "/api/site_info/")
        self.assertEqual(reverse("api:profile_updated"), "/api/profile/update/")
        self.assertEqual(reverse("api:profile_banned"), "/api/profile/ban/")
        self.assertEqual(
            reverse("api:terminate_session"), "/api/profile/terminate_session/"
        )
