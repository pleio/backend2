#!/usr/bin/env bash

# Cleanup static folder
rm -rf static/*

# Create expected static-frontend files if not exist (Fixes ManifestStaticFilesStorage ValueError in local development)
touch static-frontend/web.css static-frontend/vendor.js static-frontend/web.js

# Collect static
python /app/manage.py collectstatic --noinput

# Run migrations for dev
python /app/manage.py migrate_schemas --executor multiprocessing
python manage.py all_tenants_command deploy --auto

# Gzip js and css
yes n | gzip -k static/**/*.{js,css}

# Start Gunicorn processes
echo Starting uwsgi
uwsgi --ini /uwsgi-dev.ini
