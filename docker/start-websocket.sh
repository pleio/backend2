#!/usr/bin/env bash

# Start uvicorn processes
echo Starting websocket server

gunicorn backend2.websocket:app --workers 2 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:8989 --log-level info
