#!/usr/bin/env bash

python manage.py migrate_schemas --executor multiprocessing
python manage.py all_tenants_command deploy --auto
