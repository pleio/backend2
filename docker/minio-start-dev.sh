#!/usr/bin/env bash

/docker/minio-setup-dev.sh &

minio server /data --console-address ":9001"
