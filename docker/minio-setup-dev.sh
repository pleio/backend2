#!/usr/bin/env bash

sleep 3

mc alias set local http://127.0.0.1:9000 pleio_dev pleio_dev

mc admin accesskey create local pleio_dev --access-key=$MINIO_ACCESS_KEY --secret-key=$MINIO_ACCESS_SECRET

mc mb local/backend-shared-backups
