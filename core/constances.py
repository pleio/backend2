from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from core.utils.enum import Enum

ALREADY_CHECKED_IN = "already_checked_in"
ALREADY_MEMBER_OF_GROUP = "already_member_of_group"
ALREADY_REGISTERED = "already_registered"
ALREADY_VOTED = "already_voted"
COULD_NOT_ADD = "could_not_add"
COULD_NOT_CHANGE = "could_not_change"
COULD_NOT_CONVERT = "could_not_convert"
COULD_NOT_DELETE = "could_not_delete"
COULD_NOT_FIND = "could_not_find"
COULD_NOT_FIND_CONTAINER = "could_not_find_container"
COULD_NOT_FIND_ICON_FILE = "could_not_find_icon_file"
COULD_NOT_FIND_GROUP = "could_not_find_group"
COULD_NOT_FIND_MEMBERSHIP_REQUEST = "could_not_find_membership_request"
COULD_NOT_FIND_START_PAGE = "could_not_find_start_page"
COULD_NOT_FIND_USER = "could_not_find_user"
COULD_NOT_INVITE = "could_not_invite"
COULD_NOT_LEAVE = "could_not_leave"
COULD_NOT_LOGIN = "could_not_login"
COULD_NOT_LOGOUT = "could_not_logout"
COULD_NOT_ORDER_BY_START_DATE = "order_by_start_date_is_only_for_events"
COULD_NOT_ORDER_BY_SIZE = "order_by_size_is_only_for_files"
COULD_NOT_REGISTER = "could_not_register"
COULD_NOT_SAVE = "could_not_save"
COULD_NOT_SEND = "could_not_send"
COULD_NOT_USE_EVENT_FILTER = "event_filter_is_only_for_events"
DUPLICATE_CATEGORY_NAME = "duplicate_category_name"
DUPLICATE_TAG_NAME = "duplicate_tag_name"
EMAIL_ALREADY_IN_USE = "email_already_in_use"
EMAIL_ALREADY_USED = "email_already_used"
EVENT_INVALID_REPEAT_INSTANCE_LIMIT = "event_invalid_repeat_instance_limit"
EVENT_INVALID_REPEAT_UNTIL_DATE = "event_invalid_repeat_until_date"
EVENT_INVALID_STATE = "event_invalid_state"
EVENT_IS_FULL = "event_is_full"
EVENT_RANGE_IMMUTABLE = "event_range_immutable"
EVENT_RANGE_NOT_POSSIBLE = "event_range_not_possible"
EXPECT_FUTURE_DATE = "expect_future_date"
FILE_HAS_REFERENCES = "file_has_references"
FILE_NOT_CLEAN = "file_not_clean:{}"
INVALID_ANSWER = "invalid_answer"
INVALID_ARCHIVE_AFTER_DATE = "invalid_archive_after_date"
INVALID_CODE = "invalid_code"
INVALID_CONTENT_GUID = "invalid_content_guid"
INVALID_DATE = "invalid_date"
INVALID_EMAIL = "invalid_email"
INVALID_FILESIZE = "invalid_filesize"
INVALID_FILTER = "invalid_filter"
INVALID_FORM = "invalid_form"
INVALID_KEY = "invalid_key"
INVALID_NAME = "invalid_name"
INVALID_NEW_CONTAINER = "invalid_new_container"
INVALID_NEW_PASSWORD = "invalid_new_password"
INVALID_OBJECT_SUBTYPE = "invalid_object_subtype"
INVALID_OBJECT_TYPE = "invalid_object_type"
INVALID_OLD_PASSWORD = "invalid_old_password"
INVALID_PARENT = "invalid_parent"
INVALID_PROFILE_FIELD_GUID = "invalid_profile_field_guid"
INVALID_SUBTYPE = "invalid_subtype"
INVALID_TYPE = "invalid_type"
INVALID_VALUE = "invalid_value"
KEY_ALREADY_IN_USE = "key_already_in_use"
LEAVING_GROUP_IS_DISABLED = "leaving_group_is_disabled"
MAX_DEPTH_PARENT_REACHED = "max_depth_parent_reached"
MEETINGS_NOT_ENABLED = "meetings_not_enabled"
MISSING_REQUIRED_FIELD = "missing_required_field:%s"
NO_FILE = "no_file"
NON_SUBEVENT_OPERATION = "non_subevent_operation"
NOT_A_USER = "not_a_user"
NOT_AN_IMAGE = "not_an_image"
NOT_AUTHORIZED = "not_authorized"
NOT_LOGGED_IN = "not_logged_in"
NOT_MEMBER_OF_SITE = "not_member_of_site"
PARENT_ALREADY_IN_ANCESTORS = "parent_already_in_ancestors"
RECURSIVE_OPTION_REQUIRED = "recursive_option_is_mandatory"
REDIRECTS_HAS_DUPLICATE_SOURCE = "redirects_has_duplicate_source"
REDIRECTS_HAS_LOOP = "redirects_has_loop"
SUBEVENT_OPERATION = "subevent_only_operation"
TEXT_TOO_LONG = "text_too_long"
UNKNOWN_ERROR = "unknown_error"
USER_NOT_GROUP_OWNER_OR_SITE_ADMIN = "user_not_group_owner_or_site_admin"
USER_NOT_MEMBER_OF_GROUP = "user_not_member_of_group"
USER_NOT_SITE_ADMIN = "user_not_site_admin"
USER_NOT_SUPERADMIN = "user_not_superadmin"
VIDEOCALL_LIMIT_REACHED = "videocall_limit_reached"
VIDEOCALL_NOT_ENABLED = "videocall_not_enabled"
VIDEOCALL_PROFILEPAGE_NOT_AVAILABLE = "videocall_profilepage_not_available"
GROUP_MENU_MISSING_LABEL = "group_menu_missing_label"
GROUP_MENU_MISSING_ID = "group_menu_missing_id"
GROUP_MENU_INVALID_PLUGIN = "group_menu_invalid_plugin"
GROUP_MENU_INVALID_PAGE = "group_menu_invalid_page"
GROUP_MENU_INVALID_LINK = "group_menu_invalid_link"

OIDC_PROVIDER_OPTIONS = [
    {"value": "pleio", "label": "Pleio Account", "isDefault": True},
    {"value": "fnv", "label": "Mijn FNV"},
    {"value": "knb", "label": "Notariaat"},
    {"value": "knb-test", "label": "Notariaat Test"},
    {"value": "govconext", "label": "govconext"},
    {"value": "surf", "label": "SURFconext"},
    {"value": "surf-test", "label": "SURFconext Test"},
    {"value": "reclassering-digiplein", "label": "SSO Reclassering - Digiplein"},
    {"value": "reclassering-kennisbank", "label": "SSO Reclassering - Kennisbank"},
    {"value": "reclassering-nederland", "label": "Reclassering Nederland"},
    {"value": "dijkenwaard", "label": "Gemeente Dijk en Waard"},
    {"value": "okta-test", "label": "Okta Test"},
    {"value": "douane", "label": "Belastingdienst (Douane)"},
]

DOWNLOAD_AS_OPTIONS = ["odt", "html"]

PERSONAL_FILE = "__personal_file__"


class ReflectingEnum(Enum):
    def __str__(self):
        return str(self.value)


class GroupPlugin(ReflectingEnum, Enum):
    events = pgettext_lazy("plural", "Events")
    blog = pgettext_lazy("plural", "Blogs")
    discussion = pgettext_lazy("plural", "Discussions")
    questions = pgettext_lazy("plural", "Questions")
    news = pgettext_lazy("plural", "News")
    files = pgettext_lazy("plural", "Files")
    wiki = pgettext_lazy("plural", "Wikis")
    tasks = pgettext_lazy("plural", "Tasks")
    members = pgettext_lazy("plural", "Members")
    podcasts = pgettext_lazy("plural", "Podcasts")


class SiteCategoryChoices(ReflectingEnum, Enum):
    so_in = pgettext_lazy("category_choice", "Social Intranet")
    sa_ru = pgettext_lazy("category_choice", "Collaboration space")
    co_fo = pgettext_lazy("category_choice", "Community / forum")
    co_si = pgettext_lazy("category_choice", "Communication site")
    pa_pl = pgettext_lazy("category_choice", "Participation platform")
    ov = pgettext_lazy("category_choice", "Other")


class SitePlanChoices(ReflectingEnum, Enum):
    basic = pgettext_lazy("plan_choice", "Basic")
    plus = pgettext_lazy("plan_choice", "Plus")
    pro = pgettext_lazy("plan_choice", "Pro")
    pro_plus = pgettext_lazy("plan_choice", "Pro+")


class ConfigurationChoices(Enum):
    LOGO = _("Configured as 'Home' image")
    ICON = _("Configured as 'like' icon")
    FAVICON = _("Configured as favicon")


class ATTENDEE_ORDER_BY:
    name = "name"
    email = "email"
    timeUpdated = "timeUpdated"
    timeCheckedIn = "timeCheckedIn"


class ATTENDEE_REQUEST_ORDER_BY:
    name = "name"
    email = "email"
    timeCreated = "timeCreated"


class ORDER_DIRECTION:
    asc = "asc"
    desc = "desc"


class ORDER_BY:
    timeCreated = "timeCreated"
    timeUpdated = "timeUpdated"
    timePublished = "timePublished"
    lastAction = "lastAction"
    startDate = "startDate"
    lastSeen = "lastSeen"
    title = "title"
    groupName = "groupName"
    ownerName = "ownerName"
    fileSize = "fileSize"
    contentType = "contentType"
    readAccess = "readAccess"


class SEARCH_ORDER_BY:
    timeCreated = "timeCreated"
    timePublished = "timePublished"
    title = "title"


class ACCESS_TYPE:
    logged_in = "logged_in"
    public = "public"
    user = "user:{}"
    group = "group:{}"
    subgroup = "subgroup:{}"


class MEMBERSHIP:
    not_joined = "not_joined"
    requested = "requested"
    invited = "invited"
    joined = "joined"


class USER_ROLES:
    ADMIN = "ADMIN"
    EDITOR = "EDITOR"
    QUESTION_MANAGER = "QUESTION_MANAGER"
    NEWS_EDITOR = "NEWS_EDITOR"
    USER_ADMIN = "USER_ADMIN"
    REQUEST_MANAGER = "REQUEST_MANAGER"


class GROUP_MEMBER_ROLES:
    GROUP_ADMIN = "GROUP_ADMIN"
    GROUP_NEWS_EDITOR = "GROUP_NEWS_EDITOR"


class ENTITY_STATUS(str, Enum):
    DRAFT = "draft"
    PUBLISHED = "published"
    ARCHIVED = "archived"


class BROKEN_LINK_REASON(str, Enum):
    DRAFT = "draft"
    ARCHIVED = "archived"
    DOES_NOT_EXIST = "doesNotExist"
    SOURCE_MORE_PRIVILEGES = "sourceMorePrivileges"


""" Create-protected content types
"""
PROTECTED_CONTENT_TYPES = (
    "page",
    "magazine",
    "news",
)

ALL_CONTENT_TYPES = [
    # activity.models
    "statusupdate",
    # blog.models
    "blog",
    # core.models
    "chat_message",
    "comment",
    "group",
    # cms.models
    "page",
    # discussion.models
    "discussion",
    # event.models
    "event",
    # file.models
    "file",
    "folder",
    "pad",
    # magazine.models
    "magazine",
    "magazine_issue",
    # news.models
    "news",
    # podcast.models
    "episode",
    "podcast",
    # poll.models
    "poll",
    # question.models
    "question",
    # task.models
    "task",
    # user.models
    "user",
    # wiki.models
    "wiki",
]
