from io import BytesIO
from unittest import mock

from django.core.files.base import ContentFile
from PIL import Image

from core.models import ResizedImage
from core.tasks import image_resize
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestImageResizeTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(IS_CLOSED=False)
        self.owner = UserFactory()

    def get_image(self, filename="test.jpg", size=(800, 1280)):
        output = BytesIO()
        img = Image.new("RGB", size, (255, 255, 255))
        img.save(output, "JPEG")

        contents = output.getvalue()

        return ContentFile(contents, filename)

    def test_redirect(self):
        attachment = FileFactory(
            owner=self.owner, upload=self.get_image("some-image.jpg")
        )

        with mock.patch("celery.current_app.send_task") as mock_send_task:
            response = self.client.get(
                attachment.attachment_url + "?size=414", follow=True
            )

        mock_send_task.assert_called_once()
        self.assertRedirects(response, attachment.attachment_url)

    def test_resize_resolve(self):
        attachment = FileFactory(
            owner=self.owner, upload=self.get_image("some-image.jpg")
        )
        ResizedImage.objects.create(
            original=attachment,
            size=414,
            upload=self.get_image("testfile2.jpg"),
            status="OK",
        )
        with mock.patch("celery.current_app.send_task") as mock_send_task:
            response = self.client.get(attachment.url + "?size=414")

        mock_send_task.assert_not_called()

        self.assertEqual(response.status_code, 200)

    def resize(self, size, original_size):
        resized_image = ResizedImage.objects.create(
            original=FileFactory(
                owner=self.owner, upload=self.get_image(size=original_size)
            ),
            size=size,
        )
        image_resize(self.tenant.schema_name, resized_image.id)
        resized_image.refresh_from_db()
        return resized_image

    def test_resize_landscape_1000_file(self):
        ri = self.resize(250, (1000, 100))

        self.assertImageWidth(ri.original.upload_field.open(), 1000)
        self.assertImageWidth(ri.upload.open(), 250)

    def test_resize_landscape_100_file(self):
        ri = self.resize(250, (100, 10))

        self.assertImageWidth(ri.original.upload_field.open(), 100)
        self.assertImageWidth(ri.upload.open(), 100)

    def test_resize_portrait_1000_file(self):
        ri = self.resize(250, (1000, 5000))

        self.assertImageWidth(ri.original.upload_field.open(), 1000)
        self.assertImageWidth(ri.upload.open(), 250)

    def test_resize_portrait_100_file(self):
        ri = self.resize(250, (100, 500))

        self.assertImageWidth(ri.original.upload_field.open(), 100)
        self.assertImageWidth(ri.upload.open(), 100)
