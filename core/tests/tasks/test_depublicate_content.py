from unittest import mock

from django.utils import timezone
from django.utils.timezone import timedelta

from core.tasks import depublicate_content
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestArchiveScheduledContent(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.content = BlogFactory(owner=self.owner)

    @mock.patch("core.resolvers.shared.resolve_toggle_archived_at_subpages")
    @mock.patch("core.resolvers.shared.resolve_toggle_archived_with_revision")
    def test_archive_scheduled_content(
        self, resolve_toggle_archived_with_revision, resolve_toggle_archived_at_subpages
    ):
        self.content.schedule_archive_after = timezone.now() - timedelta(days=1)
        self.content.save()

        depublicate_content(self.tenant.schema_name)

        resolve_toggle_archived_at_subpages.assert_called_once_with(
            self.content, None, self.content.schedule_archive_after
        )
        resolve_toggle_archived_with_revision.assert_called_once_with(
            self.content, None, self.content.schedule_archive_after
        )
