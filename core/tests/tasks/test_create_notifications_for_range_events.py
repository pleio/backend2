from unittest import mock

from django.utils import timezone

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.event.factories import EventFactory
from user.factories import UserFactory


class TestCreateRangeEventsNotificationsAfterTheFactTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.group_owner = UserFactory()
        self.owner = UserFactory()
        self.member = UserFactory()
        self.group = GroupFactory(owner=self.group_owner)
        self.group.join(self.owner)
        self.group.join(self.member)

        # prevent automatically calling create_notification on save
        self.mock_send_notifications_on_create = mock.patch(
            "core.models.entity.Entity.send_notifications_on_create"
        ).start()

        self.create_notification = mock.patch(
            "core.tasks.create_notification.delay"
        ).start()
        self.notification_exists = mock.patch(
            "notifications.base.models.NotificationQuerySet.exists"
        ).start()

        self.event = EventFactory(
            owner=self.owner,
            group=self.group,
            range_settings={"type": "daily", "interval": 1},
            range_starttime=timezone.now(),
        )

    def run_create_notifications_for_range_events(self):
        from core.tasks.notification_tasks import create_notifications_for_range_events

        create_notifications_for_range_events(self.tenant.schema_name)

    def test_already_processed_events_are_not_processed_again(self):
        self.notification_exists.return_value = True

        self.run_create_notifications_for_range_events()

        self.create_notification.assert_not_called()

    def test_not_already_processed_events_are_processed(self):
        self.notification_exists.return_value = False

        self.run_create_notifications_for_range_events()

        self.create_notification.assert_called()

    def test_future_events_are_not_processed(self):
        self.notification_exists.return_value = False
        self.event.range_starttime = timezone.now() + timezone.timedelta(days=8)
        self.event.save()

        self.run_create_notifications_for_range_events()

        self.create_notification.assert_not_called()
