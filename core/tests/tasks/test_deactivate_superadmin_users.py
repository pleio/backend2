from unittest import mock

from django.test import override_settings

from core.tasks import deactivate_superadmin_users, dispatch_daily_cron
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


def create_user(interval="never", factory=UserFactory, **kwargs):
    user = factory(**kwargs)
    user.profile.receive_notification_email = False
    user.profile.overview_email_interval = interval
    user.profile.save()
    return user


class TestArchiveScheduledContent(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.superuser1 = create_user(interval="daily", is_superadmin=True)
        self.superuser2 = create_user(
            interval="daily", is_superadmin=True, ROLES=["ADMIN"]
        )
        self.superuser3 = create_user(interval="daily", is_superadmin=False)

    def test_deactivate_superadmin_users(self):
        deactivate_superadmin_users(self.tenant.schema_name)

        self.superuser1.refresh_from_db()
        self.superuser2.refresh_from_db()
        self.superuser3.refresh_from_db()

        self.assertFalse(self.superuser1.is_active)
        self.assertFalse(self.superuser2.is_active)
        self.assertEqual(self.superuser2.roles, [])
        self.assertTrue(self.superuser3.is_active)

    @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    @mock.patch("core.tasks.cronjobs.schedule_frequent_overview_mail")
    def test_daily_cron(self, mocked_schedule_mail):
        dispatch_daily_cron.apply().get()

        self.superuser1.refresh_from_db()
        self.superuser2.refresh_from_db()
        self.superuser3.refresh_from_db()

        self.assertFalse(self.superuser1.is_active)
        self.assertFalse(self.superuser2.is_active)
        self.assertEqual(self.superuser2.roles, [])
        self.assertTrue(self.superuser3.is_active)

        self.assertEqual(1, mocked_schedule_mail.call_count)
