from unittest import mock

from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class InviteToSiteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.admin = UserFactory(roles=[USER_ROLES.ADMIN])
        self.uadmn = UserFactory(roles=[USER_ROLES.USER_ADMIN])

        self.mutation = """
        mutation InviteItem($input: inviteToSiteInput!) {
            inviteToSite(input: $input) { success }
        }
        """

        self.variables = {
            "input": {
                "emailAddresses": ["a@a.nl", "b@b.nl", "c@c.nl"],
                "message": "<p>testMessageContent</p>",
            }
        }

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_site.generate_code",
        return_value="6df8cdad5582833eeab4",
    )
    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_site.schedule_invite_to_site_mail"
    )
    def test_invite_to_site_by_admin(self, mocked_mail, mocked_generate_code):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.assertEqual(data["inviteToSite"]["success"], True)
        self.assertEqual(mocked_mail.call_count, 3)

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_site.generate_code",
        return_value="6df8cdad5582833eeab4",
    )
    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_site.schedule_invite_to_site_mail"
    )
    def test_invite_to_site_by_user_admin(self, mocked_mail, mocked_generate_code):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.assertEqual(data["inviteToSite"]["success"], True)
        self.assertEqual(mocked_mail.call_count, 3)

    def test_invite_to_site_by_user(self):
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(self.user1)
            self.graphql_client.post(self.mutation, self.variables)

    def test_invite_to_site_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)
