import uuid
from unittest.mock import Mock, patch

from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE, INVALID_VALUE
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory
from user.models import User


class TestMutationEditUsername(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.mutation = """
        mutation EditUserName($input: editUserNameInput!) {
                editUserName(input: $input) {
                    user {
                        guid
                    }
                }
            }
        """

    @patch("core.resolvers.mutations.mutation_edit_user.shared.assert_authenticated")
    @patch(
        "core.resolvers.mutations.mutation_edit_user.shared.assert_user_match_or_admin"
    )
    @patch("core.resolvers.mutations.mutation_edit_user.assert_edit_username_enabled")
    @patch("core.resolvers.mutations.mutation_edit_user.resolve_update_username")
    @patch("core.resolvers.mutations.mutation_edit_user.resolve_user_guid")
    def test_edit_user_name(
        self,
        resolve_user_guid,
        resolve_update_username,
        assert_edit_username_enabled,
        assert_user_match_or_admin,
        assert_authenticated,
    ):
        acting_user = UserFactory(email="acting@user.com")
        update_user = Mock(spec=User)
        input_data = {"name": "New Name", "guid": str(uuid.uuid4())}
        resolve_user_guid.return_value = update_user

        self.graphql_client.force_login(acting_user)
        self.graphql_client.post(self.mutation, {"input": input_data})

        assert_edit_username_enabled.assert_called_once_with()
        assert_authenticated.assert_called_once_with(acting_user)
        resolve_user_guid.assert_called_once_with(input_data)
        assert_user_match_or_admin.assert_called_once_with(acting_user, update_user)
        resolve_update_username.assert_called_once_with(update_user, input_data)
        update_user.save.assert_called_once_with()

        update_user = UserFactory(email="updating_user@example.com")
        resolve_user_guid.return_value = update_user
        result = self.graphql_client.post(self.mutation, {"input": input_data})
        self.assertEqual(
            result["data"]["editUserName"], {"user": {"guid": update_user.guid}}
        )


class TestAssertEditUsernameEnabled(PleioTenantTestCase):
    def test_assert_edit_username_enabled(self):
        self.override_config(EDIT_USER_NAME_ENABLED=True)

        try:
            from core.resolvers.mutations.mutation_edit_user import (
                assert_edit_username_enabled,
            )

            assert_edit_username_enabled()
        except Exception as e:
            self.fail(f"Unexpected exception: {e} {type(e)}")

    def test_assert_edit_username_disabled(self):
        from core.resolvers.mutations.mutation_edit_user import (
            assert_edit_username_enabled,
        )

        with self.assertRaisesMessage(GraphQLError, COULD_NOT_SAVE):
            assert_edit_username_enabled()


class TestResolveUpdateUsername(PleioTenantTestCase):
    def test_resolve_update_username(self):
        from core.resolvers.mutations.mutation_edit_user import resolve_update_username

        user = UserFactory()
        clean_input = {"name": "New Name"}

        resolve_update_username(user, clean_input)
        self.assertEqual(user.name, clean_input["name"])

    def test_resolve_update_username_invalid_value(self):
        from core.resolvers.mutations.mutation_edit_user import resolve_update_username

        user = UserFactory()

        with self.assertRaisesMessage(GraphQLError, INVALID_VALUE):
            resolve_update_username(user, {"name": "a" * 101})

        with self.assertRaisesMessage(GraphQLError, INVALID_VALUE):
            resolve_update_username(user, {"name": ""})


class TestResolveUserGuid(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()

    def resolve_user_guid(self, input):
        from core.resolvers.mutations.mutation_edit_user import resolve_user_guid

        return resolve_user_guid(input)

    def test_resolve_user_guid(self):
        input = {"guid": self.user.guid}
        user = self.resolve_user_guid(input)

        self.assertEqual(user, self.user)

    def test_resolve_not_a_guid(self):
        input = {"guid": "not-existing-guid"}
        with self.assertRaisesMessage(GraphQLError, COULD_NOT_FIND):
            self.resolve_user_guid(input)

    def test_resolve_not_existing_user_guid(self):
        input = {"guid": str(uuid.uuid4())}
        with self.assertRaisesMessage(GraphQLError, COULD_NOT_FIND):
            self.resolve_user_guid(input)
