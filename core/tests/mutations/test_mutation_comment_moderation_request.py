from unittest.mock import patch

from django.contrib.auth.models import AnonymousUser

from core.factories import CommentFactory
from core.models.comment import CommentModerationRequest
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


def unauthorized_visitors():
    return (
        (UserFactory(), "User"),
        (AnonymousUser(), "AnonymousUser"),
    )


class TestAssignCommentModerationRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.article = BlogFactory(owner=self.owner)

        self.moderation_request = CommentModerationRequest.objects.create(
            entity=self.article
        )

        self.mutation = """
        mutation AssignCommentModerationRequest($guid: String!) {
            assignCommentModerationRequest(guid: $guid) {
                success
                node { guid }
            }
        }
        """
        self.variables = {"guid": str(self.moderation_request.id)}

        self.assign_to = patch(
            "core.models.comment.CommentModerationRequest.assign_to"
        ).start()

    def test_assign_comment_moderation_article(self):
        """Test that the CommentModerationRequest.set_assignee method is called."""
        for user, _msg in (
            (AdminFactory(), "as admin"),
            (EditorFactory(), "as editor"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, variables=self.variables)

            self.assertEqual(
                result["data"]["assignCommentModerationRequest"],
                {
                    "success": True,
                    "node": {"guid": str(self.moderation_request.id)},
                },
            )
        self.assertEqual(self.assign_to.call_count, 2)

    def test_assign_comment_moderation_article_unauthorized(self):
        """Test that the CommentModerationRequest.set_assignee method is called."""
        for user, user_type in unauthorized_visitors():
            with self.assertGraphQlError(
                "not_authorized", msg=f"user type = {user_type}"
            ):
                self.graphql_client.force_login(user)
                self.graphql_client.post(self.mutation, variables=self.variables)

            self.assertFalse(self.assign_to.called, msg=f"user type = {user_type}")


class TestResetCommentModerationRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.admin = AdminFactory()
        self.article = BlogFactory(owner=self.owner)

        self.moderation_request = CommentModerationRequest.objects.create(
            entity=self.article,
        )
        self.moderation_request.assign_to(self.admin)

        self.mutation = """
        mutation ResetCommentModerationRequest($guid: String!) {
            resetCommentModerationRequest(guid: $guid) {
                success
                node { guid }
            }
        }
        """
        self.variables = {"guid": str(self.moderation_request.id)}

        self.reset_assignee = patch(
            "core.models.comment.CommentModerationRequest.reset_assignee"
        ).start()

    def test_reset_comment_moderation_article(self):
        """Test that the CommentModerationRequest.reset_assignee method is called."""
        for user, _msg in (
            (AdminFactory(), "as admin"),
            (EditorFactory(), "as editor"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, variables=self.variables)

            self.assertEqual(
                result["data"]["resetCommentModerationRequest"],
                {
                    "success": True,
                    "node": {"guid": str(self.moderation_request.id)},
                },
            )
        self.assertEqual(self.reset_assignee.call_count, 2)

    def test_unassign_comment_moderation_article_unauthorized(self):
        """Test that the CommentModerationRequest.reset_assignee method is not called."""
        for user, user_type in unauthorized_visitors():
            msg = f"user type = {user_type}"
            with self.assertGraphQlError("not_authorized", msg=msg):
                self.graphql_client.force_login(user)
                self.graphql_client.post(self.mutation, variables=self.variables)

            self.assertFalse(self.reset_assignee.called, msg=msg)


class TestCommentModerationProcessRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.article = BlogFactory(owner=self.owner)
        self.comment = CommentFactory(container=self.article, owner=self.owner)
        self.moderation_request = CommentModerationRequest.objects.create(
            entity=self.article
        )
        self.moderation_request.comments.add(self.comment)

        self.mutation_confirm = """
        mutation ConfirmComment($guid: String!) {
            confirmComment(guid: $guid) {
                success
            }
        }
        """
        self.mutation_reject = """
        mutation RejectComment($guid: String!) {
            rejectComment(guid: $guid) {
                success
            }
        }
        """
        self.variables = {"guid": str(self.comment.id)}

        self.reset_if_no_comments = patch(
            "core.models.comment.CommentModerationRequest.reset_if_no_comments"
        ).start()

    def assertCommentExists(self, comment, msg=None):
        try:
            comment.refresh_from_db()
        except comment.DoesNotExist:
            self.fail(msg)

    def assertNotCommentExists(self, comment, msg=None):
        try:
            comment.refresh_from_db()
            self.fail(msg)
        except (AttributeError, comment.DoesNotExist):
            pass

    def test_confirm_comment(self):
        """Test that the comment has no moderation request after calling the confirmComment call."""
        for user, _msg in (
            (AdminFactory(), "as admin"),
            (EditorFactory(), "as editor"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(
                self.mutation_confirm, variables=self.variables
            )

            self.assertCommentExists(self.comment)
            self.assertEqual(result["data"]["confirmComment"], {"success": True})
            self.assertIsNone(self.comment.comment_moderation_request)
            self.reset_if_no_comments.assert_called_once()

    def test_confirm_comment_unauthorized(self):
        """
        Test that the comment a moderation request after calling the confirmComment call by unauthorized visitor.
        """
        for visitor, visitor_type in unauthorized_visitors():
            with self.assertGraphQlError(
                "not_authorized", msg=f"user type = {visitor_type}"
            ):
                self.graphql_client.force_login(visitor)
                self.graphql_client.post(
                    self.mutation_confirm, variables=self.variables
                )

            self.assertCommentExists(self.comment, msg=f"user type = {visitor_type}")
            self.assertIsNotNone(self.comment.comment_moderation_request)

    def test_reject_comment(self):
        """Test that the comment is deleted after calling the rejectComment call."""
        for user, _msg in (
            (AdminFactory(), "as admin"),
            (EditorFactory(), "as editor"),
        ):
            article = BlogFactory(owner=self.owner)
            comment = CommentFactory(container=article, owner=self.owner)
            moderation_request = CommentModerationRequest.objects.create(entity=article)
            moderation_request.comments.add(comment)
            variables = {"guid": str(comment.id)}
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation_reject, variables=variables)

            self.assertEqual(result["data"]["rejectComment"], {"success": True})
            self.assertNotCommentExists(comment)
        self.assertEqual(self.reset_if_no_comments.call_count, 2)

    def test_reject_comment_unauthorized(self):
        """
        Test that the comment is not deleted after calling the rejectComment call by unauthorized visitor.
        """
        for visitor, visitor_type in unauthorized_visitors():
            with self.assertGraphQlError(
                "not_authorized", msg=f"user type = {visitor_type}"
            ):
                self.graphql_client.force_login(visitor)
                self.graphql_client.post(self.mutation_reject, variables=self.variables)

            self.assertCommentExists(self.comment, msg=f"user type = {visitor_type}")
            self.assertIsNotNone(self.comment.comment_moderation_request)
