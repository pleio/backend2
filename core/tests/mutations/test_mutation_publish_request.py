from unittest.mock import patch

from django.utils import timezone

from core.models import PublishRequest
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestMutationAssignPublishRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.admin = AdminFactory()
        self.editor = EditorFactory()
        self.request = PublishRequest.objects.create(
            entity=BlogFactory(owner=UserFactory(), published=None),
        )
        self.mutation = """
        mutation AssignPublishRequest($guid: String!) {
            assignPublishRequest(guid: $guid) {
                success
                node {
                    assignedTo {
                        guid
                    }
                }
            }
        }
        """

    def test_assign_publish_request_as_admin(self):
        for user, _msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            response = self.graphql_client.post(
                self.mutation, {"guid": self.request.guid}
            )

            self.assertEqual(
                response["data"]["assignPublishRequest"],
                {
                    "success": True,
                    "node": {
                        "assignedTo": {
                            "guid": user.guid,
                        },
                    },
                },
            )


class TestMutationResetPublishRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.admin = AdminFactory()
        self.editor = EditorFactory()
        self.request = PublishRequest.objects.create(
            entity=BlogFactory(
                owner=UserFactory(), published=None, assigned_to=AdminFactory()
            ),
        )
        self.mutation = """
        mutation ResetPublishRequest($guid: String!) {
            resetPublishRequest(guid: $guid) {
                success
                node {
                    assignedTo {
                        guid
                    }
                }
            }
        }
        """

    def test_reset_publish_request_as_admin(self):
        for user, _msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            response = self.graphql_client.post(
                self.mutation, {"guid": self.request.guid}
            )

            self.assertEqual(
                response["data"]["resetPublishRequest"],
                {
                    "success": True,
                    "node": {
                        "assignedTo": None,
                    },
                },
            )


class TestMutationConfirmPublishRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.admin = AdminFactory(email="admin@example.com")
        self.editor = EditorFactory()
        self.owner = UserFactory(email="owner@example.com")
        self.article = BlogFactory(owner=self.owner, published=None)
        self.request = PublishRequest.objects.create(
            entity=self.article, time_published=timezone.now()
        )
        self.mutation = """
        mutation ConfirmPublishRequest($guid: String!) {
            confirmPublishRequest(guid: $guid) {
                success
                node {
                    entity {
                        ... on Blog { guid }
                    }
                }
            }
        }
        """

    @patch("core.models.entity.PublishRequest.confirm_request")
    def test_confirm_publish_request_as_owner(self, confirm_request):
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, {"guid": self.request.guid})

        self.assertFalse(confirm_request.called)

    @patch("core.models.entity.PublishRequest.confirm_request")
    def test_confirm_publish_request_as_admin(self, confirm_request):
        for user, _msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            response = self.graphql_client.post(
                self.mutation, {"guid": self.request.guid}
            )

            self.assertEqual(
                response["data"]["confirmPublishRequest"],
                {
                    "success": True,
                    "node": {
                        "entity": {
                            "guid": self.article.guid,
                        },
                    },
                },
            )
        self.assertEqual(confirm_request.call_count, 2)


class TestMutationRejectPublishRequest(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.admin = AdminFactory(email="admin@example.com")
        self.owner = UserFactory(email="owner@example.com")
        self.editor = EditorFactory()
        self.article = BlogFactory(owner=self.owner, published=None)
        self.request = PublishRequest.objects.create(
            entity=self.article, time_published=timezone.now()
        )
        self.mutation = """
        mutation RejectPublishRequest($guid: String!) {
            rejectPublishRequest(guid: $guid) {
                success
                node {
                    entity {
                        ... on Blog { guid }
                    }
                }
            }
        }
        """

    @patch("core.models.entity.PublishRequest.reject_request")
    def test_confirm_publish_request_as_owner(self, reject_request):
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, {"guid": self.request.guid})

        self.assertFalse(reject_request.called)

    @patch("core.models.entity.PublishRequest.reject_request")
    def test_confirm_publish_request_as_admin(self, reject_request):
        for user, _msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            response = self.graphql_client.post(
                self.mutation, {"guid": self.request.guid}
            )

            self.assertEqual(
                response["data"]["rejectPublishRequest"],
                {
                    "success": True,
                    "node": None,
                },
            )
        self.assertEqual(reject_request.call_count, 2)
