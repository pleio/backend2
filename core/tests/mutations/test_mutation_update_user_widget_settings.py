from unittest.mock import call, patch

from django.test import tag

from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import CampagnePageFactory
from entities.file.factories import FileFactory
from user.factories import EditorFactory, UserFactory


@tag("TestCasesWithWidgets")
@tag("UserWidgetSettings")
class TestMutationUpdateUserWidgetSettingsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.container = CampagnePageFactory(
            owner=EditorFactory(),
            read_access=["logged_in"],
        )
        self.attachment = FileFactory(owner=self.user)

        self.query = """
        mutation updateUserWidgetSettings($widgetSettings: userWidgetSettingsInput!) {
            updateUserWidgetSettings(input: $widgetSettings) {
                success
                entity {
                    guid
                }
            }
        }
        """
        self.variables = {
            "widgetSettings": {
                "guid": "1234",
                "containerGuid": self.container.guid,
                "settings": [
                    {
                        "key": "foo",
                        "value": "bar",
                    }
                ],
            },
        }

    @patch("core.models.user.UserProfile.set_widget_settings")
    def test_update_user_widget_settings(self, set_widget_settings):
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(
            response["data"]["updateUserWidgetSettings"],
            {
                "success": True,
                "entity": {"guid": self.container.guid},
            },
        )

        self.assertEqual(set_widget_settings.called, True)
        self.assertEqual(
            set_widget_settings.mock_calls,
            [call("1234", [{"key": "foo", "value": "bar"}])],
        )

    def test_update_user_widget_settings_on_user_model(self):
        self.graphql_client.force_login(self.user)
        self.graphql_client.post(self.query, self.variables)

        self.user.refresh_from_db()
        self.assertEqual(
            self.user.profile.get_widget_settings("1234"),
            [{"key": "foo", "value": "bar"}],
        )
