from unittest import mock

from django.utils import timezone

from core.factories import GroupFactory, RemoteRssEndpointFactory
from core.models import RemoteRssEndpoint
from core.tests.helpers import PleioTenantTestCase
from user.factories import EditorFactory, UserFactory


class TestRemoteRssEndpointModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.mocked_requests = mock.patch(
            "core.models.remote_rss_endpoint.requests"
        ).start()
        self.mocked_requests.get.return_value.ok = True

        self.mocked_feedparser = mock.patch(
            "core.models.remote_rss_endpoint.feedparser"
        ).start()
        self.mocked_feedparser.parse.return_value.get.return_value = None

        self.endpoint = RemoteRssEndpointFactory(last_content="old content")

    def test_is_valid(self):
        self.assertTrue(self.endpoint.is_valid())

    def test_is_valid_when_http_response_is_not_ok(self):
        self.mocked_requests.get.return_value.ok = False
        self.assertFalse(self.endpoint.is_valid())

    def test_is_valid_when_feedparser_finds_errors(self):
        self.mocked_feedparser.parse.return_value.get.return_value = "error"
        self.assertFalse(self.endpoint.is_valid())

    def test_is_valid_when_requests_raises_exception(self):
        self.mocked_requests.get.side_effect = Exception
        self.assertFalse(self.endpoint.is_valid())

    def test_is_valid_when_feedparser_raises_exception(self):
        self.mocked_requests.get.side_effect = Exception
        self.assertFalse(self.endpoint.is_valid())

    def test_latest_last_content(self):
        self.mocked_requests.get.return_value.text = "new content"
        self.mocked_requests.get.return_value.ok = True

        content = self.endpoint.latest_last_content()

        self.assertEqual(content, "new content")

    def test_latest_last_content_when_http_request_is_not_ok(self):
        self.mocked_requests.get.return_value.ok = False

        content = self.endpoint.latest_last_content()

        self.assertEqual(content, "old content")

    @mock.patch("core.models.remote_rss_endpoint.RemoteRssEndpoint.latest_last_content")
    def test_fetch_first_time(self, latest_last_content):
        latest_last_content.return_value = "new content"
        self.endpoint.last_fetched_at = None
        self.endpoint.save()

        self.endpoint.fetch()

        self.assertTrue(latest_last_content.called)

    @mock.patch("core.models.remote_rss_endpoint.RemoteRssEndpoint.latest_last_content")
    def test_fetch_post_one_hour_later(self, latest_last_content):
        latest_last_content.return_value = "new content"
        self.endpoint.last_fetched_at = timezone.now() - timezone.timedelta(minutes=61)
        self.endpoint.save()

        self.endpoint.fetch()

        self.assertTrue(latest_last_content.called)

    @mock.patch("core.models.remote_rss_endpoint.RemoteRssEndpoint.latest_last_content")
    def test_fetch_within_one_hour(self, latest_last_content):
        self.endpoint.last_fetched_at = timezone.now() - timezone.timedelta(minutes=59)
        self.endpoint.save()

        self.endpoint.fetch()

        self.assertFalse(latest_last_content.called)

    @mock.patch("core.models.remote_rss_endpoint.RemoteRssEndpoint.latest_last_content")
    def test_fetch_when_no_content(self, latest_last_content):
        latest_last_content.return_value = ""
        self.endpoint.last_fetched_at = None
        self.endpoint.save()

        result = self.endpoint.fetch()

        self.assertFalse(self.mocked_feedparser.parse.called)
        self.assertEqual(result, [])


class TestTestRssFeedTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.query = """
            query TestRssEndpoint($url: String!, $groupGuid: String) {
                testRssEndpoint(url: $url, groupGuid: $groupGuid) {
                    isFeed
                }
            }
        """

        self.variables = {"url": "https://www.example.com/rss"}

        self.mocked_is_valid = mock.patch(
            "core.models.remote_rss_endpoint.RemoteRssEndpoint.is_valid"
        ).start()
        self.mocked_is_valid.return_value = True

    def test_valid_access(self):
        self.graphql_client.force_login(EditorFactory())

        response = self.graphql_client.post(self.query, self.variables)

        self.assertTrue(self.mocked_is_valid.called)
        self.assertTrue(response["data"]["testRssEndpoint"]["isFeed"])

    def test_invalid_feed(self):
        self.mocked_is_valid.return_value = False
        self.graphql_client.force_login(EditorFactory())

        response = self.graphql_client.post(self.query, self.variables)

        self.assertTrue(self.mocked_is_valid.called)
        self.assertFalse(response["data"]["testRssEndpoint"]["isFeed"])

    def test_anonymous_access(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query, self.variables)

    def test_user_access(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.query, self.variables)

    def test_group_member_access(self):
        owner = UserFactory()
        member = UserFactory()
        group = GroupFactory(owner=owner)
        group.join(member)
        self.variables["groupGuid"] = group.guid

        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(member)
            self.graphql_client.post(self.query, self.variables)

    def test_group_admin_access(self):
        owner = UserFactory()
        admin = UserFactory()
        group = GroupFactory(owner=owner)
        group.join(admin, "admin")
        self.variables["groupGuid"] = group.guid

        with self.assertNotRaisesException():
            self.graphql_client.force_login(admin)
            self.graphql_client.post(self.query, self.variables)


class TestFetchRssFeedTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.endpoint = RemoteRssEndpointFactory()
        self.query = """
            query FetchRssEndpoint($url: String!, $limit: Int, $offset: Int) {
                fetchRssEndpoint(url: $url, limit: $limit, offset: $offset) {
                    total
                    edges {
                        title
                        link
                        description
                    }
                }
            }
        """
        self.variables = {"url": self.endpoint.url}

        self.mocked_fetch = mock.patch(
            "core.models.remote_rss_endpoint.RemoteRssEndpoint.fetch"
        ).start()
        self.mocked_fetch.return_value = [
            {
                "title": "Title",
                "link": "https://www.example.com",
                "summary": "Description",
            }
        ]

    def test_fetch_known(self):
        response = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(response["data"]["fetchRssEndpoint"]["total"], 1)
        self.assertEqual(
            response["data"]["fetchRssEndpoint"]["edges"],
            [
                {
                    "title": "Title",
                    "link": "https://www.example.com",
                    "description": "Description",
                }
            ],
        )

    def test_fetch_unknown(self):
        self.variables["url"] = "https://www.unknown.com/rss"
        response = self.graphql_client.post(self.query, self.variables)
        self.assertEqual(response["data"]["fetchRssEndpoint"]["total"], 0)
        self.assertEqual(response["data"]["fetchRssEndpoint"]["edges"], [])

    def test_fetch_offset(self):
        self.mocked_fetch.return_value = [
            {"title": "first"},
            {"title": "second"},
            {"title": "third"},
            {"title": "fourth"},
            {"title": "fifth"},
        ]
        self.variables["offset"] = 1

        response = self.graphql_client.post(self.query, self.variables)
        titles = [
            edge["title"] for edge in response["data"]["fetchRssEndpoint"]["edges"]
        ]

        self.assertEqual(response["data"]["fetchRssEndpoint"]["total"], 5)
        self.assertEqual(titles, ["second", "third", "fourth", "fifth"])

    def test_fetch_limit(self):
        self.mocked_fetch.return_value = [
            {"title": "first"},
            {"title": "second"},
            {"title": "third"},
            {"title": "fourth"},
            {"title": "fifth"},
        ]
        self.variables["limit"] = 2

        response = self.graphql_client.post(self.query, self.variables)
        titles = [
            edge["title"] for edge in response["data"]["fetchRssEndpoint"]["edges"]
        ]

        self.assertEqual(response["data"]["fetchRssEndpoint"]["total"], 5)
        self.assertEqual(titles, ["first", "second"])

    def test_fetch_feed_with_html_content(self):
        self.mocked_fetch.return_value = [
            {
                "summary": """
                    <p>
                        <em>The</em>
                        <i>quick</i>
                        <strong>brown</strong>
                        <b>fox</b>
                        <span>jumped</span>
                        <div class="very-strong">and</div>
                        <style>
                        .very-strong:after {
                            content: " was never ever ";
                        }
                        </style>
                        <a href="https://www.example.com" class="visibly-hidden">seen again.</a><br/>
                    </p>
                """,
            },
            {
                "summary": None,
            },
        ]
        response = self.graphql_client.post(self.query, self.variables)
        self.assertEqual(
            response["data"]["fetchRssEndpoint"]["edges"][0]["description"],
            "<em>The</em> <i>quick</i> <strong>brown</strong> <b>fox</b> jumped and .very-strong:after "
            + """{ content: " was never ever "; } <a href="https://www.example.com">seen again.</a>""",
        )
        self.assertEqual(
            response["data"]["fetchRssEndpoint"]["edges"][1]["description"],
            "",
        )

    def test_fetch_offset_limit(self):
        self.mocked_fetch.return_value = [
            {"title": "first"},
            {"title": "second"},
            {"title": "third"},
            {"title": "fourth"},
            {"title": "fifth"},
        ]
        self.variables["offset"] = 1
        self.variables["limit"] = 2

        response = self.graphql_client.post(self.query, self.variables)
        titles = [
            edge["title"] for edge in response["data"]["fetchRssEndpoint"]["edges"]
        ]

        self.assertEqual(titles, ["second", "third"])


class TestAddRssFeedTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.query = """
            mutation AddRssEndpoint($url: String!, $groupGuid: String) {
                addRssEndpoint(url: $url, groupGuid: $groupGuid) {
                    success
                }
            }
        """
        self.variables = {"url": "https://www.example.com/rss"}

        self.mocked_is_valid = mock.patch(
            "core.models.remote_rss_endpoint.RemoteRssEndpoint.is_valid"
        ).start()
        self.mocked_is_valid.return_value = True

    def test_valid_access(self):
        self.graphql_client.force_login(EditorFactory())

        response = self.graphql_client.post(self.query, self.variables)
        feed = RemoteRssEndpoint.objects.filter(url=self.variables["url"]).first()

        self.assertTrue(self.mocked_is_valid.called)
        self.assertTrue(response["data"]["addRssEndpoint"]["success"])
        self.assertIsNotNone(feed)
        self.assertEqual(feed.url, self.variables["url"])

    def test_long_url(self):
        self.graphql_client.force_login(EditorFactory())

        variables = {"url": "https://www.example.com/rss" + "a" * 1000}

        response = self.graphql_client.post(self.query, variables)
        feed = RemoteRssEndpoint.objects.filter(url=variables["url"]).first()

        self.assertTrue(self.mocked_is_valid.called)
        self.assertTrue(response["data"]["addRssEndpoint"]["success"])
        self.assertIsNotNone(feed)
        self.assertEqual(feed.url, variables["url"])

    def test_existing_feed(self):
        self.graphql_client.force_login(EditorFactory())
        RemoteRssEndpointFactory(url=self.variables["url"])

        self.graphql_client.post(self.query, self.variables)
        number_of_feeds = RemoteRssEndpoint.objects.filter(
            url=self.variables["url"]
        ).count()

        self.assertTrue(self.mocked_is_valid.called)
        self.assertEqual(number_of_feeds, 1)

    def test_anonymous_access(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query, self.variables)

    def test_user_access(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.query, self.variables)

    def test_member_access(self):
        owner = UserFactory()
        member = UserFactory()
        group = GroupFactory(owner=owner)
        group.join(member)
        self.variables["groupGuid"] = group.guid

        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(member)
            self.graphql_client.post(self.query, self.variables)

    def test_group_admin_access(self):
        owner = UserFactory()
        admin = UserFactory()
        group = GroupFactory(owner=owner)
        group.join(admin, "admin")
        self.variables["groupGuid"] = group.guid

        with self.assertNotRaisesException():
            self.graphql_client.force_login(admin)
            self.graphql_client.post(self.query, self.variables)
