from unittest import mock

from mixer.backend.django import mixer

from core.lib import ACCESS_TYPE
from core.models import ProfileField, ProfileFieldValidator, Setting, UserProfileField
from core.tests.helpers import PleioTenantTestCase
from user.models import User


class EditProfileFieldTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = mixer.blend(User)
        self.other = mixer.blend(User)
        self.admin = mixer.blend(User)
        self.admin.roles = ["ADMIN"]
        self.admin.save()

        self.profile_field_validator = ProfileFieldValidator.objects.create(
            validator_type="inList",
            validator_data=["aap", "noot", "mies", "text_value"],
        )

        self.profile_field1 = ProfileField.objects.create(
            key="text_key",
            name="text_name",
            field_type="text_field",
            autocomplete="name",
        )
        self.profile_field1.validators.add(self.profile_field_validator)
        self.profile_field1.save()

        self.profile_field2 = ProfileField.objects.create(
            key="html_key", name="html_name", field_type="html_field"
        )
        self.profile_field3 = ProfileField.objects.create(
            key="select_key",
            name="select_name",
            field_type="select_field",
            field_options=["select_value", "select_value_2"],
        )
        self.profile_field4 = ProfileField.objects.create(
            key="date_key", name="date_name", field_type="date_field"
        )
        self.profile_field5 = ProfileField.objects.create(
            key="multi_key",
            name="multi_name",
            field_type="multi_select_field",
            field_options=["select_value_1", "select_value_2", "select_value_3"],
        )
        self.profile_field6 = ProfileField.objects.create(
            key="text_not_editable",
            name="text_not_editable_name",
            field_type="text_field",
            is_editable_by_user=False,
        )
        UserProfileField.objects.get_or_create(
            user_profile=self.user.profile,
            profile_field=self.profile_field6,
            read_access=[ACCESS_TYPE.logged_in],
            value="can_not_edit",
        )
        Setting.objects.create(
            key="PROFILE_SECTIONS",
            value=[
                {
                    "name": "",
                    "profileFieldGuids": [
                        str(self.profile_field1.id),
                        str(self.profile_field2.id),
                        str(self.profile_field3.id),
                        str(self.profile_field4.id),
                        str(self.profile_field5.id),
                        str(self.profile_field6.id),
                    ],
                }
            ],
        )

        self.mutation = """
            mutation editProfileField($input: editProfileFieldInput!) {
                editProfileField(input: $input) {
                    user {
                        guid
                        name
                        profile {
                            key
                            name
                            autocomplete
                            value
                            category
                            accessId
                        }
                    }
                    success
                    fields {
                        key
                        message
                    }
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    @mock.patch("core.utils.auto_member_profile_field.AutoMemberProfileField.apply")
    def test_edit_profile_field_by_user(self, mock_apply):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "text_value",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)
        user = result["data"]["editProfileField"]["user"]

        self.assertEqual(user["guid"], self.user.guid)
        self.assertEqual(user["profile"][0]["key"], "text_key")
        self.assertEqual(user["profile"][0]["name"], "text_name")
        self.assertEqual(user["profile"][0]["value"], "text_value")
        self.assertEqual(user["profile"][0]["accessId"], 2)
        self.assertEqual(user["profile"][0]["autocomplete"], "name")

        self.assertTrue(
            mock_apply.called, msg="AutoMemberProfileField.apply was not called"
        )

    def test_edit_profile_field_by_admin(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "text_value",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)
        user = result["data"]["editProfileField"]["user"]

        self.assertEqual(user["guid"], self.user.guid)
        self.assertEqual(user["profile"][0]["key"], "text_key")
        self.assertEqual(user["profile"][0]["name"], "text_name")
        self.assertEqual(user["profile"][0]["value"], "text_value")
        self.assertEqual(user["profile"][0]["accessId"], 2)
        self.assertEqual(user["profile"][0]["autocomplete"], "name")

    def test_edit_profile_field_by_other_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "text_value",
                    }
                ],
            }
        }

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.other)
            self.graphql_client.post(self.mutation, variables)

    def test_edit_profile_field_by_anonymous(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "text_value",
                    }
                ],
            }
        }

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)

    def test_edit_profile_field_not_html_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "html_key",
                        "value": "html_value",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)
        result = response["data"]["editProfileField"]

        self.assertFalse(result["success"])
        self.assertEqual(result["fields"][0]["key"], "html_key")
        self.assertEqual(result["fields"][0]["message"], "Ongeldig tekstformaat")

    def test_edit_profile_field_invalid_html_field_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "html_key",
                        "value": '{"type":"doc","content":[{"type":"file","attrs":{"name":"panic.jpeg","mimeType":"image/jpeg","url":"http://somesite.com/scam.exe","size":78256}}]}',
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)
        result = response["data"]["editProfileField"]

        self.assertFalse(result["success"])
        self.assertEqual(result["fields"][0]["key"], "html_key")
        self.assertEqual(
            result["fields"][0]["message"],
            "Ongeldige externe bestands- of afbeeldings URL: http://somesite.com/scam.exe",
        )

    def test_edit_profile_field_valid_html_field_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "html_key",
                        "value": '{"type":"doc","content":[{"type":"file","attrs":{"name":"panic.jpeg","mimeType":"image/jpeg","url":"/valid_url","size":78256}}]}',
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][1]["key"], "html_key"
        )

    def test_edit_profile_select_field_not_in_options_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "select_key",
                        "value": "select_value_fault",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)
        result = response["data"]["editProfileField"]

        self.assertFalse(result["success"])
        self.assertEqual(result["fields"][0]["key"], "select_key")
        self.assertEqual(
            result["fields"][0]["message"],
            "select_value_fault staat niet in veldopties. Probeer een van select_value of select_value_2",
        )

    def test_edit_profile_select_field_empty_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "select_key",
                        "value": "",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["editProfileField"]["success"],
            True,
            msg=data["editProfileField"]["fields"],
        )
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][2]["key"], "select_key"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][2]["name"], "select_name"
        )
        self.assertEqual(data["editProfileField"]["user"]["profile"][2]["value"], "")
        self.assertEqual(data["editProfileField"]["user"]["profile"][2]["accessId"], 2)

    def test_edit_profile_select_field_in_options_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 0,
                        "key": "select_key",
                        "value": "select_value_2",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][2]["key"], "select_key"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][2]["name"], "select_name"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][2]["value"], "select_value_2"
        )
        self.assertEqual(data["editProfileField"]["user"]["profile"][2]["accessId"], 0)

    def test_edit_profile_date_field_empty_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 1,
                        "key": "date_key",
                        "value": "",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][3]["key"], "date_key"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][3]["name"], "date_name"
        )
        self.assertEqual(data["editProfileField"]["user"]["profile"][3]["value"], "")
        self.assertEqual(data["editProfileField"]["user"]["profile"][3]["accessId"], 1)

    def test_edit_profile_date_field_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 1,
                        "key": "date_key",
                        "value": "2019-02-02",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][3]["key"], "date_key"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][3]["name"], "date_name"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][3]["value"], "2019-02-02"
        )
        self.assertEqual(data["editProfileField"]["user"]["profile"][3]["accessId"], 1)

    def test_edit_profile_date_field_with_incorrect_date_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "date_key",
                        "value": "20191-02-02",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)
        result = response["data"]["editProfileField"]

        self.assertFalse(result["success"])
        self.assertEqual(result["fields"][0]["key"], "date_key")
        self.assertEqual(
            result["fields"][0]["message"], "Ongeldig datumformaat. Verwacht Y-m-d"
        )

    def test_edit_profile_multi_select_field_fields_not_in_field_options_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "multi_key",
                        "value": "select_value_fault",
                    }
                ],
            }
        }
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)
        result = response["data"]["editProfileField"]

        self.assertFalse(result["success"])
        self.assertEqual(result["fields"][0]["key"], "multi_key")
        self.assertEqual(
            result["fields"][0]["message"],
            "select_value_fault staat niet in veldopties. Probeer een van select_value_1, select_value_2 of select_value_3",
        )

    def test_edit_profile_multi_select_field_fields_empty_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "multi_key",
                        "value": "",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["profile"][4]["value"], "")

    def test_edit_profile_multi_select_field_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "multi_key",
                        "value": "select_value_1",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][4]["key"], "multi_key"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][4]["name"], "multi_name"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][4]["value"], "select_value_1"
        )
        self.assertEqual(data["editProfileField"]["user"]["profile"][4]["accessId"], 2)

        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "multi_key",
                        "value": "select_value_1,select_value_2",
                    }
                ],
            }
        }

        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][4]["value"],
            "select_value_1,select_value_2",
        )

    def test_edit_profile_field_with_validator_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "boom",
                    }
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)
        result = response["data"]["editProfileField"]

        self.assertFalse(result["success"])
        self.assertEqual(result["fields"][0]["key"], "text_key")
        self.assertEqual(
            result["fields"][0]["message"], "Waarde voldoet niet aan validatievereisten"
        )

        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "aap",
                    }
                ],
            }
        }

        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["editProfileField"]["user"]["guid"], self.user.guid)
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][0]["key"], "text_key"
        )
        self.assertEqual(
            data["editProfileField"]["user"]["profile"][0]["name"], "text_name"
        )
        self.assertEqual(data["editProfileField"]["user"]["profile"][0]["value"], "aap")
        self.assertEqual(data["editProfileField"]["user"]["profile"][0]["accessId"], 2)

    def test_edit_ignore_non_editable_profile_field_by_user(self):
        variables = {
            "input": {
                "guid": self.user.guid,
                "fields": [
                    {
                        "accessId": 2,
                        "key": "text_key",
                        "value": "aap",
                    },
                    {
                        "accessId": 2,
                        "key": "text_not_editable",
                        "value": "i_can_edit",
                    },
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, variables)

        result = response["data"]["editProfileField"]

        self.assertTrue(result["success"])
        # fields updated:
        self.assertEqual(result["user"]["profile"][0]["value"], "aap")
        self.assertEqual(result["user"]["profile"][0]["accessId"], 2)
        # fields ignored:
        self.assertEqual(result["user"]["profile"][5]["value"], "can_not_edit")
        self.assertEqual(result["user"]["profile"][5]["accessId"], 1)
