from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestToggleSuspendAdminPrivileges(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.mutation = """
            mutation ToggleSuspendAdminPrivileges {
                toggleSuspendAdminPrivileges {
                    guid
                    showSuspendAdminPrivilegesToggle
                    isSuspendAdminPrivileges
                }
            }
        """

    def run_mutation(self, actor):
        self.graphql_client.force_login(actor)
        return actor, self.graphql_client.post(self.mutation, {})

    def test_toggle_suspend_admin_privileges(self):
        actor, response = self.run_mutation(AdminFactory())

        self.assertDictEqual(
            response["data"]["toggleSuspendAdminPrivileges"],
            {
                "guid": "viewer:%s" % actor.guid,
                "showSuspendAdminPrivilegesToggle": True,
                "isSuspendAdminPrivileges": True,
            },
        )

    def test_toggle_suspend_admin_privileges_as_suspended_admin(self):
        actor, response = self.run_mutation(AdminFactory(suspend_admin_privileges=True))

        self.assertDictEqual(
            response["data"]["toggleSuspendAdminPrivileges"],
            {
                "guid": "viewer:%s" % actor.guid,
                "showSuspendAdminPrivilegesToggle": True,
                "isSuspendAdminPrivileges": False,
            },
        )

    def test_toggle_suspend_admin_privileges_as_user(self):
        """
        Test that the api call has no effect on users without admin rights.
        And test that the toggle visibility is set to false.
        """
        actor, response = self.run_mutation(UserFactory())

        self.assertEqual(
            response["data"]["toggleSuspendAdminPrivileges"],
            {
                "guid": "viewer:%s" % actor.guid,
                "showSuspendAdminPrivilegesToggle": False,
                "isSuspendAdminPrivileges": False,
            },
        )

    def test_toggle_suspend_admin_privileges_as_user_with_suspended_admin_rights(self):
        """
        Test that the api call has no effect on users without admin rights.
        And test that the toggle visibility is set to false.
        """
        actor, response = self.run_mutation(UserFactory(suspend_admin_privileges=True))

        self.assertEqual(
            response["data"]["toggleSuspendAdminPrivileges"],
            {
                "guid": "viewer:%s" % actor.guid,
                "showSuspendAdminPrivilegesToggle": False,
                "isSuspendAdminPrivileges": True,
            },
        )
