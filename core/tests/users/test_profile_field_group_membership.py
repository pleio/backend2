from core.factories import GroupFactory
from core.models import ProfileField, UserProfileField
from core.tests.helpers import PleioTenantTestCase
from core.utils.auto_member_profile_field import AutoMemberProfileField
from user.factories import UserFactory


class TestprofileFieldGroupMembership(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.profile_field = ProfileField.objects.create(
            key="profile_field",
            name="Profile Field",
            field_type="select_field",
            field_options=["option foo", "option bar", "option baz"],
            is_in_auto_group_membership=True,
        )
        self.auto_membership_fields = [
            {"guid": self.profile_field.guid, "value": ["option foo"]}
        ]
        self.owner = UserFactory()
        self.foo_group = GroupFactory(
            owner=self.owner,
            auto_membership_fields=[
                {
                    "guid": self.profile_field.guid,
                    "value": ["option foo"],
                }
            ],
        )
        self.bar_group = GroupFactory(
            owner=self.owner,
            auto_membership_fields=[
                {
                    "guid": self.profile_field.guid,
                    "value": ["option bar"],
                }
            ],
        )
        self.user = UserFactory()

    def test_profile_field_automatically_registered(self):
        field = UserProfileField.objects.create(
            profile_field=self.profile_field,
            value="option foo",
            user_profile=self.user.profile,
        )
        auto_member = AutoMemberProfileField(self.user)
        auto_member.apply(field)
        self.assertTrue(self.foo_group.is_member(self.user))
        self.assertFalse(self.bar_group.is_member(self.user))

    def test_profile_field_not_automatically_registered_at_another_value(self):
        field = UserProfileField.objects.create(
            profile_field=self.profile_field,
            value="option bar",
            user_profile=self.user.profile,
        )
        auto_member = AutoMemberProfileField(self.user)
        auto_member.apply(field)
        self.assertFalse(self.foo_group.is_member(self.user))
        self.assertTrue(self.bar_group.is_member(self.user))

    def test_profile_field_not_changed_does_not_change_membership(self):
        field = UserProfileField.objects.create(
            profile_field=self.profile_field,
            value="option foo",
            user_profile=self.user.profile,
        )
        auto_member = AutoMemberProfileField(self.user)
        auto_member.log_previous_value(field)
        auto_member.apply(field)
        self.assertFalse(self.foo_group.is_member(self.user))
        self.assertFalse(self.bar_group.is_member(self.user))
