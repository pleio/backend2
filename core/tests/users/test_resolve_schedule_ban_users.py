from django.utils import timezone

from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestResolveScheduleBanUsersTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.users = [
            UserFactory(email="one@example.com"),
            UserFactory(email="two@example.com"),
            UserFactory(email="three@example.com"),
            UserFactory(email="four@example.com"),
        ]

        self.mutation = """
        mutation($input: scheduleBanUsersInput!) {
                scheduleBanUsers(input: $input) {
                    users {
                        guid
                        email
                        isBanned
                        banReason
                        banScheduledAt
                        banScheduledReason
                    }
                }
            }
        """

        self.variables = {
            "input": {
                "guids": [user.guid for user in self.users],
                "banAt": (timezone.now() + timezone.timedelta(days=1)).isoformat(),
                "banReason": "Foo",
            }
        }

    def test_execute_as_anonymous(self):
        """
        Should fail for anonymous visitors
        """
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_execute_as_non_manager(self):
        """
        Should fail for unprivileged users
        """
        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_execute_as_user_manager(self):
        """
        Should succeed for user managers
        """
        with self.assertNotRaisesException():
            self.graphql_client.force_login(UserFactory(roles=[USER_ROLES.USER_ADMIN]))
            self.graphql_client.post(self.mutation, self.variables)

    def test_execute_as_manager(self):
        """
        Should fill in the banScheduledAt and banScheduledReason fields
        Should not fill in the isBanned and banReason fields
        """
        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(self.mutation, self.variables)

        for user in result["data"]["scheduleBanUsers"]["users"]:
            self.assertFalse(user["isBanned"])
            self.assertEqual(user["banReason"], "")
            self.assertIsNotNone(user["banScheduledAt"])
            self.assertEqual(user["banScheduledReason"], "Foo")

    def test_reverse_schedule_as_manager(self):
        """
        Should clear banScheduledAt and banScheduledReason fields
        """
        # Given.
        for user in self.users:
            user.ban_scheduled_at = timezone.now()
            user.ban_scheduled_reason = "Foo"
            user.save()
        self.variables["input"]["banAt"] = None
        self.variables["input"]["banReason"] = None

        # When
        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(self.mutation, self.variables)

        # Then
        for user in result["data"]["scheduleBanUsers"]["users"]:
            self.assertFalse(user["isBanned"])
            self.assertEqual(user["banReason"], "")
            self.assertIsNone(user["banScheduledAt"])
            self.assertIsNone(user["banScheduledReason"])

    def test_execute_as_manager_with_past_date(self):
        """
        Should fail if the date is in the past
        """
        self.variables["input"]["banAt"] = (
            timezone.now() - timezone.timedelta(days=1)
        ).isoformat()
        self.graphql_client.force_login(AdminFactory())
        with self.assertGraphQlError("expect_future_date"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_execute_as_manager_with_invalid_reason(self):
        """
        Should fail if fail reason is not given but date is.
        """
        self.variables["input"]["banAt"] = None
        self.graphql_client.force_login(AdminFactory())
        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_execute_as_manager_with_invalid_date(self):
        """
        Should fail if fail reason is not given but date is.
        """
        self.variables["input"]["banReason"] = None
        self.graphql_client.force_login(AdminFactory())
        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_execute_as_manager_with_empty_string_reason(self):
        """
        Should fail if fail reason is an empty string.
        """
        self.variables["input"]["banReason"] = ""
        self.graphql_client.force_login(AdminFactory())
        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.post(self.mutation, self.variables)
