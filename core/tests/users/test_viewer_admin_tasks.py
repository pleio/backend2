from django.core.files.base import ContentFile
from django.utils.timezone import now

from core.factories import SiteAccessRequestFactory
from core.models import (
    Comment,
    CommentModerationRequest,
    Entity,
    PublishRequest,
    SiteAccessRequest,
)
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    QuestionManagerFactory,
    UserFactory,
    UserManagerFactory,
)
from user.models import User


class Template:
    class TestViewerAdminTasks(PleioTenantTestCase):
        KIND_OF_TASK = None

        def setUp(self):
            super().setUp()

            self.query = """
            query GetViewer($kind: KindOfAdminTask) {
                viewer {
                    guid
                    task_count: countAdminTasks(kind: $kind)
                }
            }
            """

        def run_query(self, user):
            if user:
                self.graphql_client.force_login(user)
            response = self.graphql_client.post(
                self.query,
                {
                    "kind": self.KIND_OF_TASK,
                },
            )
            return response["data"]["viewer"]["task_count"]

        def test_as_anonymous(self):
            self.override_config(IS_CLOSED=False)
            self.assertEqual(0, self.run_query(user=None))

        def test_as_user(self):
            self.assertEqual(0, self.run_query(UserFactory()))


class TestViewerDeleteAccountRequestsAdminTasks(Template.TestViewerAdminTasks):
    KIND_OF_TASK = "deleteAccountRequests"

    def setUp(self):
        super().setUp()

        # Given
        UserFactory(is_delete_requested=True)
        UserFactory(is_delete_requested=True)
        UserFactory(is_delete_requested=True)

    def test_as_non_privileged_manager(self):
        # When. Then.
        self.assertEqual(0, self.run_query(EditorFactory()))
        self.assertEqual(0, self.run_query(QuestionManagerFactory()))
        self.assertEqual(0, self.run_query(NewsEditorFactory()))

    def test_as_user_admin(self):
        # When. Then.
        self.assertEqual(3, self.run_query(UserManagerFactory()))
        self.assertEqual(3, self.run_query(AdminFactory()))

    def test_as_user_admin_without_requests(self):
        # Given.
        User.objects.update(is_delete_requested=False)

        # When. Then.
        self.assertEqual(0, self.run_query(UserManagerFactory()))
        self.assertEqual(0, self.run_query(AdminFactory()))


class TestViewerSiteAccessRequestsAdminTasks(Template.TestViewerAdminTasks):
    KIND_OF_TASK = "siteAccessRequests"

    def setUp(self):
        super().setUp()

        # Given
        SiteAccessRequestFactory(processed=False)
        SiteAccessRequestFactory(processed=False)
        SiteAccessRequestFactory(processed=False)

    def test_as_non_privileged_manager(self):
        # When. Then.
        self.assertEqual(0, self.run_query(EditorFactory()))
        self.assertEqual(0, self.run_query(QuestionManagerFactory()))
        self.assertEqual(0, self.run_query(NewsEditorFactory()))

    def test_as_admin(self):
        # When. Then.
        self.assertEqual(3, self.run_query(UserManagerFactory()))
        self.assertEqual(3, self.run_query(AdminFactory()))

    def test_as_admin_with_accepted_requests(self):
        # Given.
        SiteAccessRequest.objects.update(processed=True)

        # When. Then.
        self.assertEqual(0, self.run_query(AdminFactory()))


class TestViewerContentPublicationRequestsAdminTasks(Template.TestViewerAdminTasks):
    KIND_OF_TASK = "contentPublicationRequests"

    def setUp(self):
        super().setUp()
        self.override_config(
            CONTENT_MODERATION_ENABLED=True, REQUIRE_CONTENT_MODERATION_FOR=["blog"]
        )
        self.owner = UserFactory()
        self.create_publish_request()
        self.create_publish_request()
        self.create_publish_request()
        self.create_publish_request(
            factory=FileFactory, upload=ContentFile(b"", "empty.file.txt")
        )

    def create_publish_request(self, factory=None, **kwargs):
        if not factory:
            factory = BlogFactory
        entity = factory(owner=self.owner, published=None, **kwargs)
        PublishRequest.objects.create(entity=entity, time_published=now())

    def test_as_non_privileged_admin(self):
        # When. Then.
        self.assertEqual(0, self.run_query(UserManagerFactory()))
        self.assertEqual(0, self.run_query(QuestionManagerFactory()))
        self.assertEqual(0, self.run_query(NewsEditorFactory()))

    def test_as_editor(self):
        # When. Then.
        self.assertEqual(3, self.run_query(EditorFactory()))
        self.assertEqual(3, self.run_query(AdminFactory()))

    def test_as_editor_without_requests(self):
        # Given.
        Entity.objects.update(published=now())

        # When. Then.
        self.assertEqual(0, self.run_query(EditorFactory()))
        self.assertEqual(0, self.run_query(AdminFactory()))


class TestViewerCommentPublicationRequestsAdminTasks(Template.TestViewerAdminTasks):
    KIND_OF_TASK = "commentPublicationRequests"

    def setUp(self):
        super().setUp()
        # Given.
        self.override_config(
            COMMENT_MODERATION_ENABLED=True,
            REQUIRE_COMMENT_MODERATION_FOR=["blog", "file"],
        )

        # And.
        self.owner = UserFactory()
        self.blog = BlogFactory(owner=self.owner)

        # And.
        self.create_publish_request(owner=UserFactory())
        self.create_publish_request(owner=UserFactory())
        self.create_publish_request(owner=UserFactory())

    def create_publish_request(self, owner):
        # Should create a moderation request with the comment automatically
        Comment.objects.create(
            container=self.blog,
            owner=owner,
        )

    def test_as_non_privileged_admin(self):
        # When. Then.
        self.assertEqual(0, self.run_query(UserManagerFactory()))
        self.assertEqual(0, self.run_query(QuestionManagerFactory()))
        self.assertEqual(0, self.run_query(NewsEditorFactory()))

    def test_as_editor(self):
        # When. Then.
        self.assertEqual(3, self.run_query(EditorFactory()))
        self.assertEqual(3, self.run_query(AdminFactory()))

    def test_as_editor_without_requests(self):
        # Given.
        CommentModerationRequest.objects.all().delete()

        # When. Then.
        self.assertEqual(0, self.run_query(EditorFactory()))
        self.assertEqual(0, self.run_query(AdminFactory()))


class TestViewerFilePublicationRequestsAdminTasks(Template.TestViewerAdminTasks):
    KIND_OF_TASK = "filePublicationRequests"

    def setUp(self):
        super().setUp()
        self.override_config(
            CONTENT_MODERATION_ENABLED=True,
            REQUIRE_CONTENT_MODERATION_FOR=["blog", "file"],
        )
        self.owner = UserFactory()
        self.create_publish_request(upload=ContentFile(b"", "empty.file.1.txt"))
        self.create_publish_request(upload=ContentFile(b"", "empty.file.2.txt"))
        self.create_publish_request(upload=ContentFile(b"", "empty.file.3.txt"))
        self.create_publish_request(factory=BlogFactory)
        self.create_publish_request(factory=BlogFactory)

    def create_publish_request(self, factory=None, **kwargs):
        if not factory:
            factory = FileFactory
        entity = factory(published=None, owner=self.owner, **kwargs)
        PublishRequest.objects.create(entity=entity, time_published=now())

    def test_as_non_privileged_admin(self):
        # When. Then.
        self.assertEqual(0, self.run_query(UserManagerFactory()))
        self.assertEqual(0, self.run_query(QuestionManagerFactory()))
        self.assertEqual(0, self.run_query(NewsEditorFactory()))

    def test_as_editor(self):
        # When. Then.
        self.assertEqual(3, self.run_query(EditorFactory()))
        self.assertEqual(3, self.run_query(AdminFactory()))

    def test_as_editor_without_requests(self):
        # Given.
        Entity.objects.update(published=now())

        # When. Then.
        self.assertEqual(0, self.run_query(EditorFactory()))
        self.assertEqual(0, self.run_query(AdminFactory()))
