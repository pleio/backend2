import json

from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile

from core.tests.helpers import PleioTenantTestCase
from core.utils.tiptap_parser import Tiptap
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TiptapParserTestCase(PleioTenantTestCase):
    def test_mentioned_users(self):
        user_id = "c5617f63-6c98-44b5-a206-ff05eb648e52"
        tiptap_json = {
            "type": "mention",
            "attrs": {
                "id": user_id,
                "label": "Mr user",
            },
        }
        tiptap = Tiptap(tiptap_json)

        self.assertSetEqual(tiptap.mentioned_users, {user_id})

    def test_mentioned_users_duplicates(self):
        user_id = "c5617f63-6c98-44b5-a206-ff05eb648e52"
        tiptap_json = {
            "type": "doc",
            "content": [
                {
                    "type": "mention",
                    "attrs": {
                        "id": user_id,
                        "label": "Mr user",
                    },
                },
                {
                    "type": "mention",
                    "attrs": {
                        "id": user_id,
                        "label": "Mr user",
                    },
                },
            ],
        }
        tiptap = Tiptap(tiptap_json)

        self.assertSetEqual(tiptap.mentioned_users, {user_id})

    def test_mentioned_users_faulty(self):
        tiptap_json = {
            "type": "mention",
            "attrs": {
                "label": "Mr user",
            },
        }
        tiptap = Tiptap(tiptap_json)

        self.assertSetEqual(tiptap.mentioned_users, set())

    def test_mentioned_users_none(self):
        tiptap_json = {}
        tiptap = Tiptap(tiptap_json)

        self.assertSetEqual(tiptap.mentioned_users, set())

    def test_mentioned_users_string(self):
        user_id = "c5617f63-6c98-44b5-a206-ff05eb648e52"
        tiptap_json = '{"type":"doc","content":[{"type":"paragraph","attrs":{"intro":false},"content":[{"type":"mention","attrs":{"id":"c5617f63-6c98-44b5-a206-ff05eb648e52","label":"Kaj"}},{"type":"text","text":" a"}]}]}'
        tiptap = Tiptap(tiptap_json)

        self.assertSetEqual(tiptap.mentioned_users, {user_id})

    def test_replace_attachment(self):
        original = "111111"
        replacement = "99999999"
        tiptap_json = {
            "type": "file",
            "attrs": {
                "url": original,
            },
        }
        tiptap = Tiptap(tiptap_json)

        tiptap.replace_attachment(original, replacement)
        result = tiptap.tiptap_json

        self.assertEqual(result["attrs"]["url"], replacement)

    def test_replace_attachment_empty(self):
        original = ""
        replacement = "8888888"
        tiptap_json = {
            "type": "file",
            "attrs": {
                "url": "1111111",
            },
        }
        tiptap = Tiptap(tiptap_json)

        tiptap.replace_attachment(original, replacement)
        result = tiptap.tiptap_json

        self.assertEqual(result["attrs"]["url"], tiptap_json["attrs"]["url"])

    def test_validate_rich_text_attachments_with_absolute_urls(self):
        file_json = json.dumps(
            {"content": [{"type": "file", "attrs": {"url": "https://example.com"}}]}
        )
        image_json = json.dumps(
            {"content": [{"type": "image", "attrs": {"src": "https://example.com"}}]}
        )

        with self.assertRaises(ValidationError):
            Tiptap(file_json).check_for_external_urls()

        with self.assertRaises(ValidationError):
            Tiptap(image_json).check_for_external_urls()

    def test_validate_rich_text_attachments_with_relative_urls(self):
        file_json = json.dumps(
            {"content": [{"type": "file", "attrs": {"url": "/foo/bar"}}]}
        )
        image_json = json.dumps(
            {"content": [{"type": "image", "attrs": {"src": "no/path/prefix"}}]}
        )
        file_json_localdomain = json.dumps(
            {
                "content": [
                    {
                        "type": "file",
                        "attrs": {
                            "url": "https://%s/foo/bar" % self.tenant.primary_domain
                        },
                    }
                ]
            }
        )
        image_json_localdomain = json.dumps(
            {
                "content": [
                    {
                        "type": "image",
                        "attrs": {
                            "src": "http://%s/no/path/prefix"
                            % self.tenant.primary_domain
                        },
                    }
                ]
            }
        )

        # Expect no ValidationErrors being raised
        Tiptap(file_json).check_for_external_urls()
        Tiptap(image_json).check_for_external_urls()
        Tiptap(file_json_localdomain).check_for_external_urls()
        Tiptap(image_json_localdomain).check_for_external_urls()

    def test_update_file_attributes(self):
        file = self.remember(
            FileFactory(
                owner=self.remember(UserFactory()),
                upload=ContentFile(b"Not a real jpg", "Realy.jpg"),
            )
        )
        original_tiptap = self.tiptap_attachment(file)

        parser = Tiptap(original_tiptap)
        parser.update_file_attrs(file.guid, {"name": "NoReal.jpg", "foo": "bar"})
        new_tiptap = parser.as_string()

        self.assertNotIn("Realy.jpg", new_tiptap)
        self.assertIn("NoReal.jpg", new_tiptap)
        self.assertIn("foo", new_tiptap)
        self.assertIn("bar", new_tiptap)


class TestGetMediaParserTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

    def test_get_media_image(self):
        parser = Tiptap(
            """
        {"type":"doc","content":[
        {"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Afbeelding"}]},
        {"type":"image","attrs":{"src":"/file/download/704a25bb-5110-47a0-abd7-eaaa8d411196/img_5721_gsJ8ZUB.jpeg","size":"large","align":"center","alt":null,"caption":null}}
        ]}
        """
        )
        media = [*parser.get_media()]

        self.assertEqual(len(media), 1)
        self.assertEqual(media[0]["type"], "image")

    def test_get_media_file(self):
        parser = Tiptap(
            """
        {"type":"doc","content":[
        {"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Bijlage"}]},
        {"type":"file","attrs":{"guid":"24b30613-238c-46aa-9f08-515d3c368b18","name":"example.xlsx","subtype":null,"mimeType":"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","url":"/attachment/entity/24b30613-238c-46aa-9f08-515d3c368b18","size":9285,"notAvailable":false}}
        ]}
        """
        )
        media = [*parser.get_media()]

        self.assertEqual(len(media), 1)
        self.assertEqual(media[0]["type"], "file")

    def test_get_media_video(self):
        parser = Tiptap(
            """
        {"type":"doc","content":[
        {"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Video"}]},
        {"type":"video","attrs":{"guid":"XYZ","platform":"youtube","title":null}}
        ]}
        """
        )
        media = [*parser.get_media()]

        self.assertEqual(len(media), 1)
        self.assertEqual(media[0]["type"], "video")

    def test_get_media_has_nothing(self):
        parser = Tiptap(
            """
        {"type":"doc","content":[
        {"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Afbeelding"}]},
        {"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Bijlage"}]},
        {"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Video"}]},
        ]}
        """
        )
        media = [*parser.get_media()]

        self.assertEqual(len(media), 0)
