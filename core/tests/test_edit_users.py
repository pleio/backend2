from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory
from user.models import User


class EditUsersTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.user3 = UserFactory()
        self.user4 = UserFactory()
        self.user5 = UserFactory(is_active=False)
        self.user6 = UserFactory(is_active=False)
        self.admin = AdminFactory()

        self.mutation = """
            mutation editUsers($input: editUsersInput!) {
                editUsers(input: $input) {
                    success
                }
            }
        """
        self.variables = {}

    def tearDown(self):
        super().tearDown()

    def test_edit_users_by_anonymous(self):
        self.variables = {"input": {"guids": [self.user1.guid], "action": "ban"}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_users_by_user(self):
        self.variables = {"input": {"guids": [self.user2.guid], "action": "ban"}}

        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.user1)
            self.graphql_client.post(self.mutation, self.variables)

    def assert_valid_ban_behaviour(self, user):
        self.variables = {
            "input": {"guids": [self.user2.guid, self.user3.guid], "action": "ban"}
        }
        self.assertEqual(User.objects.filter(is_active=False).count(), 2)

        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.mutation, self.variables)
        self.user2.refresh_from_db()
        data = result["data"]
        self.assertEqual(data["editUsers"]["success"], True)
        self.assertEqual(User.objects.filter(is_active=False).count(), 4)
        self.assertIsNotNone(self.user2.banned_at)

    def test_ban_users_by_admin(self):
        self.assert_valid_ban_behaviour(self.admin)

    def test_ban_users_by_user_admin(self):
        self.assert_valid_ban_behaviour(UserFactory(roles=[USER_ROLES.USER_ADMIN]))

    def assert_valid_unban_behaviour(self, user):
        self.variables = {"input": {"guids": [self.user5.guid], "action": "unban"}}

        self.assertEqual(User.objects.filter(is_active=False).count(), 2)

        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.mutation, self.variables)
        self.user5.refresh_from_db()
        data = result["data"]
        self.assertEqual(data["editUsers"]["success"], True)
        self.assertEqual(User.objects.filter(is_active=False).count(), 1)
        self.assertIsNone(self.user5.banned_at)

    def test_unban_users_by_admin(self):
        self.assert_valid_unban_behaviour(self.admin)

    def test_unban_users_by_user_admin(self):
        self.assert_valid_unban_behaviour(UserFactory(roles=[USER_ROLES.USER_ADMIN]))

    def test_ban_yourself(self):
        self.variables = {
            "input": {"guids": [self.user2.guid, self.admin.guid], "action": "ban"}
        }

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(self.mutation, self.variables)

    def test_ban_user_with_reason(self):
        self.variables = {
            "input": {
                "guids": [self.user1.guid, self.user2.guid],
                "action": "ban",
                "banReason": "test",
            }
        }
        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.mutation, self.variables)
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertEqual(self.user1.ban_reason, "test")
        self.assertEqual(self.user2.ban_reason, "test")
