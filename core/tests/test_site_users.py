from django.utils import dateparse, timezone

from core.constances import USER_ROLES
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, EditorFactory, UserFactory


class SiteUsersTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory(name="User1")
        self.user2 = UserFactory(name="Specific_user_name_1", email="specific@test.nl")
        self.user3 = UserFactory(name="User3", is_delete_requested=True)
        self.user4 = UserFactory(name="User4", is_active=False)
        self.user5 = UserFactory(name="Deleted")
        self.user5.delete()
        self.admin1 = AdminFactory(name="Admin1")
        self.admin2 = AdminFactory(name="Admin2")
        self.editor1 = EditorFactory(name="Editor1")
        self.user_admin = UserFactory(name="UserAdmin", roles=[USER_ROLES.USER_ADMIN])
        self.superadmin = UserFactory(
            name="SuperAdmin", is_active=True, is_superadmin=True
        )

        self.user1.profile.last_online = dateparse.parse_datetime(
            "2018-12-10T23:00:00.000Z"
        )
        self.user1.profile.save()
        self.user3.profile.last_online = "2020-12-10T23:00:00.000Z"
        self.user3.profile.save()
        self.user4.profile.last_online = "2020-12-10T23:00:00.000Z"
        self.user4.profile.save()

        self.query = """
            query UsersQuery(
                    $offset: Int,
                    $limit: Int
                    $q: String
                    $role: String
                    $isDeleteRequested: Boolean
                    $isBanned: Boolean
                    $filterScheduledForBan: UserBanScheduledFilter
                    $memberSince: DateTime
                    $lastOnlineBefore: String) {
                siteUsers(
                        offset: $offset
                        limit: $limit
                        q: $q
                        role: $role
                        isDeleteRequested: $isDeleteRequested
                        isBanned: $isBanned
                        filterScheduledForBan: $filterScheduledForBan
                        memberSince: $memberSince
                        lastOnlineBefore: $lastOnlineBefore) {
                    edges {
                        name
                        url
                        email
                        lastOnline
                        roles
                        requestDelete
                        memberSince
                    }
                    total
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_site_users_get_all_by_admin(self):
        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, {})

        self.assertUserNamesMatchResult(
            result["data"]["siteUsers"]["edges"],
            expected_users=[
                self.user1,
                self.user2,
                self.user3,
                self.admin1,
                self.admin2,
                self.editor1,
                self.user_admin,
            ],
        )

    def test_site_users_filter_admins_by_admin(self):
        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, {"role": "admin"})

        data = result["data"]["siteUsers"]
        self.assertEqual(data["total"], 2)
        self.assertEqual(len(data["edges"]), 2)

    def test_site_users_filter_editors_by_admin(self):
        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, {"role": "editor"})

        data = result["data"]
        self.assertEqual(data["siteUsers"]["total"], 1)
        self.assertEqual(len(data["siteUsers"]["edges"]), 1)
        self.assertEqual(data["siteUsers"]["edges"][0]["name"], self.editor1.name)
        self.assertEqual(data["siteUsers"]["edges"][0]["roles"], self.editor1.roles)

    def test_site_users_filter_delete_requested_by_admin(self):
        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, {"isDeleteRequested": True})

        data = result["data"]
        self.assertEqual(data["siteUsers"]["total"], 1)
        self.assertEqual(data["siteUsers"]["edges"][0]["name"], self.user3.name)

    def test_site_users_filter_name_by_admin(self):
        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, {"q": "c_user_nam"})

        data = result["data"]
        self.assertEqual(data["siteUsers"]["total"], 1)
        self.assertEqual(data["siteUsers"]["edges"][0]["name"], self.user2.name)

    def test_site_users_filter_email_guid_by_admin(self):
        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, {"q": "specific@test.nl"})

        data = result["data"]
        self.assertEqual(data["siteUsers"]["total"], 1)
        self.assertEqual(data["siteUsers"]["edges"][0]["name"], self.user2.name)
        self.assertEqual(data["siteUsers"]["edges"][0]["email"], self.user2.email)

        result = self.graphql_client.post(self.query, {"q": "cific@test.nl"})

        data = result["data"]
        self.assertEqual(data["siteUsers"]["total"], 1)
        self.assertEqual(data["siteUsers"]["edges"][0]["name"], self.user2.name)
        self.assertEqual(data["siteUsers"]["edges"][0]["email"], self.user2.email)

        result = self.graphql_client.post(self.query, {"q": self.user2.guid})

        data = result["data"]
        self.assertEqual(data["siteUsers"]["total"], 1)
        self.assertEqual(data["siteUsers"]["edges"][0]["name"], self.user2.name)
        self.assertEqual(data["siteUsers"]["edges"][0]["email"], self.user2.email)

    def test_site_users_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query, {})

    def assertUserNamesMatchResult(self, edges, expected_users):
        """One can see the names of all enabled users, but the superadmin"""
        names = {e["name"] for e in edges}
        expected_names = {e.name for e in expected_users}

        self.assertEqual(len(names), len(expected_users))
        self.assertEqual(names, expected_names)

    def test_site_users_by_user(self):
        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, {})

        """ One can only see the email address of oneself """
        emails = {e["email"] for e in result["data"]["siteUsers"]["edges"]}
        self.assertEqual(len(emails), 2)
        self.assertEqual(emails, {self.user1.email, None})

        """ One can see the names of all enabled users, but the superadmin """
        self.assertUserNamesMatchResult(
            result["data"]["siteUsers"]["edges"],
            expected_users=[
                self.user1,
                self.user2,
                self.user3,
                self.admin1,
                self.admin2,
                self.editor1,
                self.user_admin,
            ],
        )

    def test_site_users_get_all_banned_by_admin(self):
        for user, msg in (
            (self.admin1, "Test with Admin"),
            (self.user_admin, "Test with UserAdmin"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.query, {"isBanned": True})

            data = result["data"]
            self.assertEqual(data["siteUsers"]["total"], 1, msg=msg)
            self.assertEqual(len(data["siteUsers"]["edges"]), 1, msg=msg)
            self.assertEqual(
                data["siteUsers"]["edges"][0]["name"], self.user4.name, msg=msg
            )

    def test_site_users_get_lastonline_before_by_admin(self):
        for user, msg in (
            (self.admin1, "Test with Admin"),
            (self.user_admin, "Test with UserAdmin"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(
                self.query, {"lastOnlineBefore": "2019-12-10T23:00:00.000Z"}
            )

            data = result["data"]
            self.assertEqual(data["siteUsers"]["total"], 1, msg=msg)
            self.assertEqual(
                data["siteUsers"]["edges"][0]["name"], self.user1.name, msg=msg
            )
            self.assertEqual(len(data["siteUsers"]["edges"]), 1, msg=msg)

    def test_site_users_result(self):
        for user, msg in (
            (self.admin1, "Test with Admin"),
            (self.user_admin, "Test with UserAdmin"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.query, {"q": self.user1.name})

            user = result["data"]["siteUsers"]["edges"][0]

            self.assertEqual(user["name"], self.user1.name, msg=msg)
            self.assertEqual(user["url"], self.user1.url, msg=msg)
            self.assertEqual(user["email"], self.user1.email, msg=msg)
            self.assertDateEqual(
                user["lastOnline"], str(self.user1.profile.last_online), msg=msg
            )
            self.assertDateEqual(
                user["memberSince"], str(self.user1.created_at), msg=msg
            )

    def test_site_users_members_since(self):
        for user, msg in (
            (self.admin1, "Test with Admin"),
            (self.user_admin, "Test with UserAdmin"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(
                self.query, {"memberSince": str(timezone.now())}
            )

            self.assertEqual(result["data"]["siteUsers"]["total"], 0, msg=msg)

            result = self.graphql_client.post(
                self.query,
                {"memberSince": str(timezone.now() - timezone.timedelta(days=1))},
            )

            self.assertTrue(result["data"]["siteUsers"]["total"] > 0, msg=msg)

    def test_filter_banned_users_as_user(self):
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(self.user1)
            self.graphql_client.post(self.query, {"isBanned": True})

    def test_filter_banned_users_as_siteadmin(self):
        for user, msg in (
            (self.admin1, "Test with Admin"),
            (self.user_admin, "Test with UserAdmin"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.query, {"isBanned": True})

            names = {e["name"] for e in result["data"]["siteUsers"]["edges"]}
            self.assertEqual(len(names), 1, msg=msg)
            self.assertEqual(names, {self.user4.name}, msg=msg)

    def test_users_scheduled_for_ban(self):
        ban_after = timezone.now() + timezone.timedelta(days=100)
        self.user1.schedule_ban_at = ban_after
        self.user1.save()
        self.user3.schedule_ban_at = ban_after
        self.user3.save()

        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(
            self.query, {"filterScheduledForBan": "isScheduled"}
        )

        users = [u["name"] for u in result["data"]["siteUsers"]["edges"]]
        self.assertEqual(len(users), 2)
        self.assertEqual({*users}, {self.user1.name, self.user3.name})

    def test_users_not_scheduled_for_ban(self):
        ban_after = timezone.now() + timezone.timedelta(days=100)
        self.user1.schedule_ban_at = ban_after
        self.user1.save()
        self.user3.schedule_ban_at = ban_after
        self.user3.save()

        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(
            self.query, {"filterScheduledForBan": "isNotScheduled"}
        )

        users = [u["name"] for u in result["data"]["siteUsers"]["edges"]]
        self.assertNotIn(self.user1.name, users)
        self.assertNotIn(self.user3.name, users)


class TestQuerySiteUsers(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = AdminFactory()
        self.group = GroupFactory(owner=self.owner)
        self.other_group = GroupFactory(owner=self.owner)

        self.member = UserFactory()
        self.pending_member = UserFactory()
        self.non_member = UserFactory()

        self.group.join(self.member)
        self.group.join(self.pending_member, "pending")
        self.other_group.join(self.pending_member)

        self.query = """
        query SiteUsers($groupGuid: String) {
            siteUsers(groupGuid: $groupGuid) {
                edges { guid }
            }
        }
        """

    def test_query_all_users(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {})
        users = {e["guid"] for e in result["data"]["siteUsers"]["edges"]}

        self.assertEqual(
            users,
            {
                self.owner.guid,
                self.member.guid,
                self.pending_member.guid,
                self.non_member.guid,
            },
        )

    def test_query_group_users(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"groupGuid": self.group.guid})
        users = {e["guid"] for e in result["data"]["siteUsers"]["edges"]}

        self.assertEqual(users, {self.owner.guid, self.member.guid})
