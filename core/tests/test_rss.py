from http import HTTPStatus

from django.utils import timezone

from core.tests.helpers import override_config
from entities.blog.factories import BlogFactory
from entities.event.factories import EventFactory
from entities.news.factories import NewsFactory
from tenants.helpers import FastTenantTestCase
from user.factories import AdminFactory


class RSSTests(FastTenantTestCase):
    yesterday = timezone.now() - timezone.timedelta(days=1)

    def setUp(self):
        super().setUp()
        self.owner = AdminFactory()

        self.blog = BlogFactory(
            owner=self.owner,
            title="blog",
            rich_description="content",
            abstract="abstract",
        )
        self.news = NewsFactory(
            owner=self.owner,
            title="news",
            rich_description="content",
            abstract="abstract",
        )
        self.event = EventFactory(
            owner=self.owner,
            title="event",
            rich_description="content",
            abstract="abstract",
        )
        self.event_yesterday = EventFactory(
            owner=self.owner,
            title="event",
            rich_description="content",
            abstract="abstract",
            start_date=self.yesterday,
        )

    @override_config(IS_CLOSED=True)
    def test_closed_site(self):
        response = self.client.get("/rss")

        self.assertEqual(response.status_code, HTTPStatus.UNAUTHORIZED)

    @override_config(IS_CLOSED=False)
    def test_invalid_feed(self):
        response = self.client.get("/rss/file")

        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    @override_config(IS_CLOSED=False)
    def test_main_feed(self):
        response = self.client.get("/rss")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response["content-type"], "application/rss+xml; charset=utf-8")

        self.assertContains(response, "<item>", count=4)

    @override_config(IS_CLOSED=False)
    def test_blog_feed(self):
        response = self.client.get("/rss/blog")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response["content-type"], "application/rss+xml; charset=utf-8")

        self.assertContains(response, "<item>", count=1)

    @override_config(IS_CLOSED=False)
    def test_news_feed(self):
        response = self.client.get("/rss/news")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response["content-type"], "application/rss+xml; charset=utf-8")

        self.assertContains(response, "<item>", count=1)

    @override_config(IS_CLOSED=False)
    def test_event_feed(self):
        response = self.client.get("/rss/event")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response["content-type"], "application/rss+xml; charset=utf-8")

        self.assertContains(response, "<item>", count=1)
