from unittest import mock

from faker import Faker
from mixer.backend.django import mixer

from core import config
from core.lib import get_full_url
from core.mail_builders.invite_to_site import InviteToSiteMailer
from core.models import SiteInvitation
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestMailerInviteToSiteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_setting(ENV="unit-test")

        self.invitation: SiteInvitation = mixer.blend(SiteInvitation)
        self.sender = UserFactory()
        self.MESSAGE = Faker().sentence()

        self.mailer = InviteToSiteMailer(
            sender=self.sender.guid, email=self.invitation.email, message=self.MESSAGE
        )
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "link": get_full_url("/login", invitecode=self.invitation.code),
                "message": self.MESSAGE,
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(add_local_context.call_args.kwargs["user"], self.sender)
        self.assertEqual(self.mailer.get_language(), config.LANGUAGE)
        self.assertEqual(self.mailer.get_template(), "email/invite_to_site.html")
        self.assertEqual(self.mailer.get_receiver(), None)
        self.assertEqual(self.mailer.get_receiver_email(), self.invitation.email)
        self.assertEqual(self.mailer.get_sender(), self.sender)
        self.assertEqual(
            self.mailer.get_subject(), f"You are invited to join site {config.NAME}"
        )
