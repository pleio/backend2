from unittest import mock

from core import config
from core.mail_builders.site_access_changed import SiteAccessChangedMailer
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestMailerSiteAccessChangedTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.admin = AdminFactory()
        self.sender = UserFactory()
        self.is_closed = True

        self.mailer = SiteAccessChangedMailer(
            admin=self.admin.guid, sender=self.sender.guid, is_closed=self.is_closed
        )
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "access_level": "closed",
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(add_local_context.call_args.kwargs["user"], self.sender)
        self.assertEqual(self.mailer.get_language(), self.admin.get_language())
        self.assertEqual(self.mailer.get_template(), "email/site_access_changed.html")
        self.assertEqual(self.mailer.get_receiver(), self.admin)
        self.assertEqual(self.mailer.get_receiver_email(), self.admin.email)
        self.assertEqual(self.mailer.get_sender(), self.sender)
        self.assertEqual(
            self.mailer.get_subject(), f"Site access level changed for {config.NAME}"
        )
