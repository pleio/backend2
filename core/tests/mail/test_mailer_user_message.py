from unittest import mock

from core.lib import get_base_url
from core.mail_builders.user_send_message import UserSendMessageMailer
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestMailerUserOnDeleteTestCase(PleioTenantTestCase):
    MESSAGE = "Demo message content."
    SUBJECT = "Demo subject"
    SENDER_NAME = "Wilfred and Marcel"

    def setUp(self):
        super().setUp()

        self.sender = UserFactory(name=self.SENDER_NAME)
        self.receiver = UserFactory()
        self.mailer = UserSendMessageMailer(
            message=self.MESSAGE,
            subject=self.SUBJECT,
            receiver=self.receiver.guid,
            sender=self.sender.guid,
            copy=False,
        )
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "message": self.MESSAGE,
                "subject": "Message from Wilfred and Marcel: Demo subject",
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(self.mailer.get_language(), self.receiver.get_language())
        self.assertEqual(self.mailer.get_template(), "email/send_message_to_user.html")
        self.assertEqual(self.mailer.get_receiver(), self.receiver)
        self.assertEqual(self.mailer.get_receiver_email(), self.receiver.email)
        self.assertEqual(self.mailer.get_sender(), self.sender)
        self.assertEqual(
            self.mailer.get_subject(), "Message from Wilfred and Marcel: Demo subject"
        )

    def test_copy(self):
        # When
        self.mailer.copy = True

        # Then
        self.assertEqual(
            self.mailer.get_subject(),
            "Copy: Message from Wilfred and Marcel: Demo subject",
        )

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_evil_links(self, add_local_context, get_context):
        local_url = get_base_url() + "/test"

        TEST_LINKS = {
            "Dit is een test met een <a href='http://evil.com'>evil link</a>.": "Dit is een test met een evil link.",
            "Testing https://evil.com": 'Testing <a href="https://evil.com">https://evil.com</a>',
            "Testing %s" % local_url: 'Testing <a href="%s">%s</a>'
            % (local_url, local_url),
        }

        for input, output in TEST_LINKS.items():
            self.mailer.message = input
            add_local_context.return_value = {"add_local_context": "mocked"}
            get_context.return_value = {"get_context": "mocked"}

            self.assertDictEqual(
                self.mailer.get_context(),
                {
                    "message": output,
                    "subject": "Message from Wilfred and Marcel: Demo subject",
                    "add_local_context": "mocked",
                    "get_context": "mocked",
                },
            )
