from unittest import mock

from django.template.loader import get_template

from core.mail_builders.user_import_success_mailer import UserImportSuccessMailer
from core.tests.helpers import PleioTenantTestCase
from core.utils.user_importer import (
    IDP_STATUS_ALREADY_EXISTS,
    IDP_STATUS_CREATED,
    STATUS_CREATED,
    STATUS_INCOMPLETE_INPUT,
    STATUS_UPDATED,
)
from user.factories import UserFactory


class TestMailerImportUsersFailedTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.operating_user = UserFactory()
        self.mailer = UserImportSuccessMailer(
            user=self.operating_user.guid,
            stats={
                "processed": 421 + 1423 + 1427 + 1429 + 2423,
                STATUS_CREATED: 421,
                STATUS_UPDATED: 1423,
                STATUS_INCOMPLETE_INPUT: 1427,
                IDP_STATUS_CREATED: 1429,
                IDP_STATUS_ALREADY_EXISTS: 2423,
                "error: seventy is not 23": 4201,
                "error: 29 is not twenty": 6427,
            },
        )
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_mailer_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "processed": 7123,
                "stats_created": 421,
                "stats_updated": 1423,
                "stats_incomplete": 1427,
                "idp_added": 1429,
                "idp_exists": 2423,
                "errors": {
                    "error: seventy is not 23": 4201,
                    "error: 29 is not twenty": 6427,
                },
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(self.mailer.get_language(), self.operating_user.get_language())
        self.assertEqual(self.mailer.get_template(), "email/user_import_success.html")
        self.assertEqual(self.mailer.get_receiver(), self.operating_user)
        self.assertEqual(self.mailer.get_receiver_email(), self.operating_user.email)
        self.assertEqual(self.mailer.get_sender(), None)
        self.assertEqual(self.mailer.get_subject(), "Import was a success")

    def test_mailer_template(self):
        template_name = self.mailer.get_template()
        template = get_template(template_name)

        result = template.render(context=self.mailer.get_context())
        self.maxDiff = None

        self.assertInHTML(
            """
            <p>
                The import of users succeeded. Here are the stats:
            </p>
            <ul>
                <li>7123 records processed.</li>
                <li>421 users added.</li>
                <li>1423 users updated.</li>
                <li>1427 users with incomplete input.</li>
                <li>2423 users already had a login.</li>
                <li>1429 users will log on for the first time.</li>
            </ul>
                <p>Errors during import:</p>
                <ul>
                    <li>error: seventy is not 23 (4201 users)</li>
                    <li>error: 29 is not twenty (6427 users)</li>
                </ul>
            """,
            result,
        )
