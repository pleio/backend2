from unittest import mock

from mixer.backend.django import mixer

from core.factories import GroupFactory
from core.mail_builders.group_resend_invitation import ResendGroupInvitationMailer
from core.models import GroupInvitation
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestMailerGroupResendInvitationTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.switch_language("en")

        self.ANOTHER_EMAIL = "anonymous@example.com"
        self.user = UserFactory()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.invitation = mixer.blend(
            GroupInvitation,
            acting_user=self.owner,
            group=self.group,
            invited_user=self.user,
        )
        self.anonymous_invitation = mixer.blend(
            GroupInvitation,
            acting_user=self.owner,
            group=self.group,
            email=self.ANOTHER_EMAIL,
        )

        self.add_local_context = mock.patch(
            "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
        ).start()
        self.add_local_context.return_value = {}
        self.get_language = mock.patch("user.models.User.get_language").start()
        self.get_language.return_value = "nl"

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        mailer = ResendGroupInvitationMailer(
            sender=self.owner.guid, invitation=self.invitation.id
        )
        context = mailer.get_context()
        self.assertEqual(len(context), 4)
        self.assertEqual(context["group_name"], self.group.name)
        self.assertIn(self.invitation.code, context["link"])
        self.assertEqual(context["get_context"], "mocked")
        self.assertEqual(context["add_local_context"], "mocked")

        self.assertEqual(mailer.get_language(), "nl")
        self.assertEqual(mailer.get_template(), "email/resend_group_invitation.html")
        self.assertEqual(mailer.get_receiver(), self.user)
        self.assertEqual(mailer.get_receiver_email(), self.user.email)
        self.assertEqual(mailer.get_sender(), self.owner)
        self.assertIn(self.group.name, mailer.get_subject())

    def test_properties_anonymous(self):
        mailer = ResendGroupInvitationMailer(
            sender=self.owner.guid, invitation=self.anonymous_invitation.id
        )

        self.assertEqual(mailer.get_receiver(), None)
        self.assertEqual(mailer.get_receiver_email(), self.anonymous_invitation.email)
        self.assertEqual(mailer.get_language(), "en")
