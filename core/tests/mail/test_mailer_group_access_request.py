from unittest import mock

from core.factories import GroupFactory
from core.lib import get_full_url, obfuscate_email
from core.mail_builders.group_access_request import GroupAccessRequestMailer
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestMailerGroupInviteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.user = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.mailer = GroupAccessRequestMailer(
            user=self.user.guid, receiver=self.owner.guid, group=self.group.guid
        )
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "link": get_full_url(self.group.url),
                "group_name": self.group.name,
                "user_obfuscated_email": obfuscate_email(self.user.email),
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(add_local_context.call_args.kwargs["user"], self.user)
        self.assertEqual(self.mailer.get_language(), self.owner.get_language())
        self.assertEqual(self.mailer.get_template(), "email/group_access_request.html")
        self.assertEqual(self.mailer.get_receiver(), self.owner)
        self.assertEqual(self.mailer.get_receiver_email(), self.owner.email)
        self.assertEqual(self.mailer.get_sender(), None)
        self.assertIn(self.group.name, self.mailer.get_subject())
