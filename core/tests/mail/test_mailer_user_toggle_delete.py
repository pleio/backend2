from unittest import mock

from core.mail_builders.user_cancel_delete_for_admin import (
    UserCancelDeleteToAdminMailer,
)
from core.mail_builders.user_cancel_delete_for_user import UserCancelDeleteToSelfMailer
from core.mail_builders.user_request_delete_for_admin import (
    UserRequestDeleteToAdminMailer,
)
from core.mail_builders.user_request_delete_for_user import (
    UserRequestDeleteToSelfMailer,
)
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestMailerUserOnDeleteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.admin = AdminFactory()

        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_schedule_user_cancel_delete_for_admin_mail(
        self, add_local_context, get_context
    ):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        mailer = UserCancelDeleteToAdminMailer(
            user=self.user.guid, admin=self.admin.guid
        )

        self.assertEqual(
            mailer.get_context(),
            {"add_local_context": "mocked", "get_context": "mocked"},
        )
        self.assertEqual(add_local_context.call_args.kwargs, {"user": self.user})
        self.assertEqual(mailer.get_language(), self.admin.get_language())
        self.assertEqual(
            mailer.get_template(),
            "email/toggle_request_delete_user_cancelled_admin.html",
        )
        self.assertEqual(mailer.get_receiver(), self.admin)
        self.assertEqual(mailer.get_receiver_email(), self.admin.email)
        self.assertEqual(mailer.get_sender(), self.user)
        self.assertEqual(mailer.get_subject(), "Request to remove account cancelled")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_schedule_user_cancel_delete_for_user_mail(
        self, add_local_context, get_context
    ):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        mailer = UserCancelDeleteToSelfMailer(user=self.user.guid)

        self.assertEqual(
            mailer.get_context(),
            {"add_local_context": "mocked", "get_context": "mocked"},
        )
        self.assertEqual(add_local_context.call_args.kwargs, {"user": self.user})
        self.assertEqual(mailer.get_language(), self.user.get_language())
        self.assertEqual(
            mailer.get_template(), "email/toggle_request_delete_user_cancelled.html"
        )
        self.assertEqual(mailer.get_receiver(), self.user)
        self.assertEqual(mailer.get_receiver_email(), self.user.email)
        self.assertEqual(mailer.get_sender(), None)
        self.assertEqual(mailer.get_subject(), "Request to remove account cancelled")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_schedule_user_request_delete_for_admin_mail(
        self, add_local_context, get_context
    ):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        mailer = UserRequestDeleteToAdminMailer(
            user=self.user.guid, admin=self.admin.guid
        )

        self.assertEqual(
            mailer.get_context(),
            {"add_local_context": "mocked", "get_context": "mocked"},
        )
        self.assertEqual(add_local_context.call_args.kwargs, {"user": self.user})
        self.assertEqual(mailer.get_language(), self.admin.get_language())
        self.assertEqual(
            mailer.get_template(),
            "email/toggle_request_delete_user_requested_admin.html",
        )
        self.assertEqual(mailer.get_receiver(), self.admin)
        self.assertEqual(mailer.get_receiver_email(), self.admin.email)
        self.assertEqual(mailer.get_sender(), self.user)
        self.assertEqual(mailer.get_subject(), "Request to remove account")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_schedule_user_request_delete_for_user_mail(
        self, add_local_context, get_context
    ):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        mailer = UserRequestDeleteToSelfMailer(user=self.user.guid)

        self.assertEqual(
            mailer.get_context(),
            {"add_local_context": "mocked", "get_context": "mocked"},
        )
        self.assertEqual(add_local_context.call_args.kwargs, {"user": self.user})
        self.assertEqual(mailer.get_language(), self.user.get_language())
        self.assertEqual(
            mailer.get_template(), "email/toggle_request_delete_user_requested.html"
        )
        self.assertEqual(mailer.get_receiver(), self.user)
        self.assertEqual(mailer.get_receiver_email(), self.user.email)
        self.assertEqual(mailer.get_sender(), None)
        self.assertEqual(mailer.get_subject(), "Request to remove account")
