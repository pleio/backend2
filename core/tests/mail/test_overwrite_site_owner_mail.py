from unittest import mock

from core import config
from core.mail_builders.overwrite_site_owner_email import OverwriteSiteOwnerMailer
from core.mail_builders.template_mailer import TemplateMailerBase
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestOverwriteSiteOwnerMailTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.sender = UserFactory()
        self.current_owner = "test@example.com"
        self.template_name = "email/overwrite_site_owner_email.html"
        self.subject = "Site-eigenaar gewijzigd"

        self.mailer = OverwriteSiteOwnerMailer(
            sender=self.sender.guid,
            current_owner=self.current_owner,
        )

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_mailer(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertIsInstance(self.mailer, TemplateMailerBase)
        self.assertEqual(
            self.mailer.get_context(),
            {"add_local_context": "mocked", "get_context": "mocked"},
        )
        self.assertEqual(self.mailer.get_language(), config.LANGUAGE)
        self.assertEqual(self.mailer.get_template(), self.template_name)
        self.assertEqual(self.mailer.get_receiver(), None)
        self.assertEqual(self.mailer.get_receiver_email(), self.current_owner)
        self.assertEqual(self.mailer.get_sender(), self.sender)
        self.assertEqual(self.mailer.get_subject(), self.subject)

        add_local_context.assert_called_once_with(user=self.sender)
