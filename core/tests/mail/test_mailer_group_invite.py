from unittest import mock

import faker

from core.factories import GroupFactory
from core.lib import get_full_url
from core.mail_builders.group_invite_to_group import InviteToGroupMailer
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestMailerGroupInviteTestCase(PleioTenantTestCase):
    INVITE_CODE = "WD39"

    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}
        EMAIL = faker.Faker().email()
        LANGCODE = faker.Faker().word()

        mailer = InviteToGroupMailer(
            email=EMAIL,
            sender=self.owner.guid,
            language=LANGCODE,
            group=self.group.guid,
            code=self.INVITE_CODE,
        )

        self.assertDictEqual(
            mailer.get_context(),
            {
                "link": get_full_url(
                    "/groups/invitations/", __auth=True, invitecode=self.INVITE_CODE
                ),
                "group_name": self.group.name,
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(add_local_context.call_args.kwargs["user"], self.owner)
        self.assertEqual(mailer.get_language(), LANGCODE)
        self.assertEqual(mailer.get_template(), "email/invite_to_group.html")
        self.assertEqual(mailer.get_receiver(), None)
        self.assertEqual(mailer.get_receiver_email(), EMAIL)
        self.assertEqual(mailer.get_sender(), self.owner)
        self.assertIn(self.group.name, mailer.get_subject())

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_user_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        user = UserFactory()
        mailer = InviteToGroupMailer(
            user=user.guid,
            sender=self.owner.guid,
            group=self.group.guid,
            code=self.INVITE_CODE,
        )

        self.assertDictEqual(
            mailer.get_context(),
            {
                "link": get_full_url(
                    "/groups/invitations/", __auth=True, invitecode=self.INVITE_CODE
                ),
                "group_name": self.group.name,
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(add_local_context.call_args.kwargs["user"], self.owner)
        self.assertEqual(mailer.get_language(), user.get_language())
        self.assertEqual(mailer.get_template(), "email/invite_to_group.html")
        self.assertEqual(mailer.get_receiver(), user)
        self.assertEqual(mailer.get_receiver_email(), user.email)
        self.assertEqual(mailer.get_sender(), self.owner)
        self.assertIn(self.group.name, mailer.get_subject())
