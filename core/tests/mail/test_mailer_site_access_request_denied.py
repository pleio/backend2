from unittest import mock

from faker import Faker

from core import config
from core.mail_builders.site_access_request_denied import SiteAccessRequestDeniedMailer
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory


class TestMailerSiteAccessRequestDeniedTestCase(PleioTenantTestCase):
    INTRO = "Some leaving message."
    MESSAGE = "Some_message"

    def setUp(self):
        super().setUp()
        self.override_config(SITE_MEMBERSHIP_DENIED_INTRO=self.INTRO)

        self.EMAIL = Faker().email()
        self.NAME = Faker().name()
        self.sender = AdminFactory()

        self.mailer = SiteAccessRequestDeniedMailer(
            email=self.EMAIL,
            name=self.NAME,
            sender=self.sender.guid,
            message=self.MESSAGE,
        )
        self.switch_language("en")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_properties(self, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "request_name": self.NAME,
                "intro": self.INTRO,
                "message": self.MESSAGE,
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(add_local_context.call_args.kwargs["user"], self.sender)
        self.assertEqual(self.mailer.get_language(), config.LANGUAGE)
        self.assertEqual(
            self.mailer.get_template(), "email/site_access_request_denied.html"
        )
        self.assertEqual(self.mailer.get_receiver(), None)
        self.assertEqual(self.mailer.get_receiver_email(), self.EMAIL)
        self.assertEqual(self.mailer.get_sender(), self.sender)
        self.assertEqual(
            self.mailer.get_subject(), f"Membership request declined for: {config.NAME}"
        )

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_subject_override(self, add_local_context, get_context):
        """Check if the mail subject can be overridden through site configuration."""
        self.override_config(
            NAME="Site name", SITE_MEMBERSHIP_DENIED_SUBJECT="Denied: [site_name]"
        )
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}
        self.assertEqual(self.mailer.get_subject(), "Denied: Site name")
