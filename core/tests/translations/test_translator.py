from unittest import mock

from django.test import tag

from core.models import TranslationCache
from core.tests.helpers import PleioTenantTestCase
from core.utils.translation.translator import Translator, XMLTranslator


@tag("translation")
class TestTranslatorTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_setting(DEEPL_TOKEN="xyz")
        self.override_setting(DEEPL_URL="https://example.com")

        self.translate_ctl = mock.patch(
            "core.utils.translation.translator.Translator.translate"
        )
        self.translate = self.translate_ctl.start()
        self.translate.return_value = "translated"

    def test_constructor(self):
        translator = Translator(source_language_code="from", target_language_code="to")

        self.assertEqual(translator.source_language_code, "from")
        self.assertEqual(translator.target_language_code, "to")

    def test_get_parameters(self):
        translator = Translator(source_language_code="from", target_language_code="to")
        result = translator.get_parameters()

        self.assertEqual(
            result,
            {
                "target_lang": "TO",
                "source_lang": "FROM",
            },
        )

    def test_translate(self):
        self.translate_ctl.stop()
        self.mocked_deepl.return_value = "translated"

        translator = Translator(source_language_code="from", target_language_code="to")
        result = translator.translate("text")

        self.assertEqual(result, "translated")

    def test_get_translation_when_it_fails(self):
        self.translate_ctl.stop()
        self.mocked_deepl.return_value = ""

        translator = Translator(source_language_code="from", target_language_code="to")
        result = translator.get_translation("text", "reference")

        self.assertEqual(result, "text")
        self.assertFalse(TranslationCache.objects.exists())

    def test_translate_first_time(self):
        self.assertEqual(TranslationCache.objects.count(), 0)

        translator = Translator(source_language_code="from", target_language_code="to")
        result = translator.get_translation(value="text", reference="reference")
        cached = TranslationCache.objects.first()

        self.assertEqual(result, "translated")
        self.assertEqual(TranslationCache.objects.count(), 1)
        self.assertEqual(cached.reference, "reference")
        self.assertEqual(cached.hash, "1cb251ec0d568de6a929b520c4aed8d1")
        self.assertEqual(cached.language_code, "to")
        self.assertEqual(cached.value, "translated")

        pass

    def test_find_existing_translation(self):
        TranslationCache.objects.create(
            reference="reference",
            hash="1cb251ec0d568de6a929b520c4aed8d1",
            language_code="to",
            value="from_cache",
        )

        translator = Translator(source_language_code="from", target_language_code="to")
        result = translator.get_translation(value="text", reference="reference")

        self.assertEqual(result, "from_cache")
        self.assertEqual(TranslationCache.objects.count(), 1)
        self.assertFalse(self.translate.called)

    def test_find_new_translation(self):
        TranslationCache.objects.create(
            reference="reference",
            hash="not-the-same-hash",
            language_code="to",
            value="from_cache",
        )

        translator = Translator(source_language_code="from", target_language_code="to")
        result = translator.get_translation(value="text", reference="reference")

        self.assertEqual(result, "translated")
        self.assertEqual(TranslationCache.objects.count(), 1)
        self.assertTrue(self.translate.called)


@tag("translation")
class TestXmlTranslatorTestCase(PleioTenantTestCase):
    @mock.patch("core.utils.translation.translator.Translator.get_parameters")
    def test_get_parameters(self, get_parameters):
        get_parameters.return_value = {"super": "parameters"}

        result = XMLTranslator(
            source_language_code="from", target_language_code="to"
        ).get_parameters()

        self.assertEqual(
            result,
            {
                "super": "parameters",
                "tag_handling": "xml",
                "splitting_tags": "root,doc,par",
            },
        )
