from unittest import mock
from unittest.mock import MagicMock, call

from django.test import tag

from core.tests.helpers import PleioTenantTestCase
from core.utils.translation.translatables import (
    AbstractTranslatable,
    DescriptionTranslatable,
    RichDescriptionTranslatable,
    SubTitleTranslatable,
    TitleTranslatable,
)
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from user.factories import UserFactory


class Wrapper:
    @tag("translation")
    class BaseTranslationTestCase(PleioTenantTestCase):
        pass

    @tag("translation")
    class TestCustomTextBaseTestCase(PleioTenantTestCase):
        def get_translatable(self):
            raise NotImplementedError()

        EXPECTED_REFERENCE = None

        def setUp(self):
            super().setUp()
            self.override_setting(DEEPL_URL="https://example.com")
            self.override_setting(DEEPL_TOKEN="xyz:fx")
            self.entity = MagicMock(spec=Blog)
            self.entity.guid = "demo-guid"
            self.translatable = self.get_translatable()

        def test_reference(self):
            self.assertEqual(self.translatable.get_reference(), self.EXPECTED_REFERENCE)

        def test_get_translatable_value(self):
            self.assertEqual(self.translatable.get_translatable_value(), "Foo")

        @mock.patch("core.utils.translation.translator.Translator.get_parameters")
        def test_translation(self, get_parameters):
            get_parameters.return_value = {"foo": "bar"}
            translation = self.translatable.translate()
            self.assertEqual(translation, "translated")
            self.assertEqual(
                self.mocked_deepl.call_args,
                call(text="Foo", foo="bar"),
            )


class TestTitleTranslatable(Wrapper.BaseTranslationTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.blog = BlogFactory(owner=self.owner, title="Blog title")
        self.translator = MagicMock()
        self.translatable = TitleTranslatable(entity=self.blog)

    def test_reference(self):
        reference = self.translatable.get_reference()
        self.assertTrue(reference.startswith(self.blog.guid))
        self.assertTrue(reference.endswith(":title"))

    def test_get_translatable_value(self):
        self.assertEqual(self.translatable.get_translatable_value(), "Blog title")

    def test_empty_title(self):
        self.blog.title = ""
        self.blog.save()

        self.assertEqual(self.translatable.get_translatable_value(), "")

    def test_as_translated_value(self):
        self.assertEqual(
            self.translatable.as_translated_value("translated"),
            "translated",
        )


class TestSubTitleTranslatable(Wrapper.BaseTranslationTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.blog = Blog(owner=self.owner, sub_title="Blog subtitle")
        self.translator = MagicMock()
        self.translatable = SubTitleTranslatable(entity=self.blog)

    def test_reference(self):
        reference = self.translatable.get_reference()
        self.assertTrue(reference.startswith(self.blog.guid))
        self.assertTrue(reference.endswith(":sub_title"))

    def test_get_translatable_value(self):
        self.assertEqual(self.translatable.get_translatable_value(), "Blog subtitle")

    def test_empty_title(self):
        self.blog.sub_title = ""
        self.blog.save()

        self.assertEqual(self.translatable.get_translatable_value(), "")

    def test_as_translated_value(self):
        self.assertEqual(
            self.translatable.as_translated_value("translated"),
            "translated",
        )


class TestAbstractTranslatable(Wrapper.BaseTranslationTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.blog = BlogFactory(owner=self.owner, abstract="Blog abstract")
        self.translator = MagicMock()
        self.translatable = AbstractTranslatable(entity=self.blog)

    def test_inheritance(self):
        self.assertIsInstance(self.translatable, AbstractTranslatable)
        self.assertIsInstance(self.translatable, RichDescriptionTranslatable)

    def test_reference(self):
        reference = self.translatable.get_reference()
        self.assertTrue(reference.startswith(self.blog.guid))
        self.assertTrue(reference.endswith("abstract"))

    def test_get_translatable_value(self):
        self.assertEqual(self.translatable._get_value(), "Blog abstract")

    def test_empty_abstract(self):
        self.blog.abstract = None
        self.blog.save()

        self.assertEqual(self.translatable.get_translatable_value(), "")


class TestRichDescriptionTranslatable(Wrapper.BaseTranslationTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.blog = BlogFactory(
            owner=self.owner,
            rich_description=self.tiptap_paragraph(
                "Foo",
                "Bar baz",
            ),
        )
        self.translator = MagicMock()
        self.translatable = RichDescriptionTranslatable(entity=self.blog)
        self.expected_xml = """<root><doc><par name="content.0.content.0.text.">Foo</par><par name="content.1.content.0.text.">Bar baz</par></doc></root>"""

    def test_reference(self):
        reference = self.translatable.get_reference()
        self.assertTrue(reference.startswith(self.blog.guid))
        self.assertTrue(reference.endswith("rich_description"))

    def test_get_translatable_value(self):
        self.assertEqual(
            self.translatable.get_translatable_value(),
            self.expected_xml,
        )

    def test_get_empty_translatable_value(self):
        self.blog.rich_description = None
        self.blog.save()

        self.assertEqual(self.translatable.get_translatable_value(), None)

    def test_as_translated_value(self):
        translated = self.expected_xml.replace("Foo", "Foo translated")
        translated = translated.replace("Bar baz", "Bar baz translated")

        self.assertEqual(
            self.translatable.as_translated_value(translated),
            self.tiptap_paragraph("Foo translated", "Bar baz translated"),
        )


class TestDescriptionTranslatable(Wrapper.TestCustomTextBaseTestCase):
    def get_translatable(self):
        return DescriptionTranslatable(entity=self.entity, value="Foo")

    EXPECTED_REFERENCE = "demo-guid:description"
