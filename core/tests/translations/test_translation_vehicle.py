from django.test import tag

from core.tests.helpers import PleioTenantTestCase
from core.utils.translation.translation_vehicle import XmlTranslationVehicle


@tag("translation")
class TestTranslationVehicleTestCase(PleioTenantTestCase):
    INPUT = {
        "alpha": "Some text",
        "bravo": "Some more text",
        "charlie": "Even more text",
    }
    XML = """<root><doc><par name="alpha">Some text</par><par name="bravo">Some more text</par><par name="charlie">Even more text</par></doc></root>"""
    TARGET_LANGUAGE = "en"

    def test_translation_vehicle_to_xml(self):
        vehicle = XmlTranslationVehicle()
        for key, value in self.INPUT.items():
            vehicle.add_text(key, value)

        self.assertEqual(vehicle.as_text(), self.XML)

    def test_translation_vehicle_from_xml(self):
        vehicle = XmlTranslationVehicle.from_text(self.XML)

        self.assertEqual(vehicle.translations, self.INPUT)
