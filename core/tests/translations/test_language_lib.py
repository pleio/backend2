from unittest import mock

from django.test import tag

from core.tests.helpers import PleioTenantTestCase
from core.utils.translation.lib import get_language_code, should_use_foreign_language


@tag("translation")
class TestLanguageLibTestCase(PleioTenantTestCase):
    @mock.patch("core.utils.translation.lib.get_language")
    def test_get_language_code(self, get_language):
        for language, expected_code in [
            ("alpha", "al"),
            ("bravo", "br"),
            ("charlie", "ch"),
        ]:
            get_language.return_value = language
            self.assertEqual(get_language_code(), expected_code)


@tag("translation")
class TestIsForeignLanguageSelectedTestCase(PleioTenantTestCase):
    @mock.patch("core.utils.translation.lib.get_language")
    def test_foreign_language_is_selected(self, get_language):
        get_language.return_value = "nl-NL"
        self.assertFalse(should_use_foreign_language("nl"))

    @mock.patch("core.utils.translation.lib.get_language")
    def test_foreign_language_is_not_selected(self, get_language):
        get_language.return_value = "nl-NL"
        self.override_config(CONTENT_TRANSLATION=True)

        self.assertTrue(should_use_foreign_language("en"))

    @mock.patch("core.utils.translation.lib.get_language")
    def test_content_translation_is_not_enabled(self, get_language):
        get_language.return_value = "nl-NL"
        self.override_config(CONTENT_TRANSLATION=False)

        self.assertFalse(should_use_foreign_language("en"))
