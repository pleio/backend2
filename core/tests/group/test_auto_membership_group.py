from unittest import mock

from core.factories import GroupFactory
from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase
from core.utils.auto_member_profile_field import (
    AutoMemberProfileFieldAtGroup,
    _is_match,
)
from user.factories import UserFactory


class TestApplyAutoMembershipFields(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.profile_field = ProfileField.objects.create(
            key="profile_field",
            name="Profile Field",
            field_type="select_field",
            field_options=["option foo", "option bar", "option baz"],
        )
        self.another_field = ProfileField.objects.create(
            key="another_field",
            name="Another Field",
            field_type="select_field",
            field_options=["option foo", "option bar", "option baz"],
        )
        self.auto_membership_fields = [
            {"guid": self.profile_field.guid, "value": ["option foo"]}
        ]
        self.owner = UserFactory()
        self.group = GroupFactory(
            owner=self.owner, auto_membership_fields=self.auto_membership_fields
        )
        self.auto_members = AutoMemberProfileFieldAtGroup(self.group)

    def create_user(self, field_value):
        user = UserFactory()
        if field_value:
            user.profile.user_profile_fields.create(
                profile_field=self.profile_field,
                value=field_value,
            )
        return user

    def test_nothing_has_changed(self):
        self.assertEqual(self.auto_members._get_changed_fields(), {})

    def test_fields_have_changed(self):
        self.group.auto_membership_fields[0]["value"] = ["bar"]
        self.assertEqual(
            self.auto_members._get_changed_fields(), {self.profile_field.guid: ["bar"]}
        )

    def test_fields_are_added(self):
        self.group.auto_membership_fields.append(
            {
                "guid": self.another_field.guid,
                "value": ["baz"],
            }
        )

        self.assertEqual(
            self.auto_members._get_changed_fields(), {self.another_field.guid: ["baz"]}
        )

    @mock.patch(
        "core.utils.auto_member_profile_field.AutoMemberProfileFieldAtGroup._get_changed_fields"
    )
    def test_get_changed_fields_called(self, mocked_get_changed_fields):
        mocked_get_changed_fields.return_value = {}
        self.auto_members.apply()

        self.assertTrue(mocked_get_changed_fields)

    @mock.patch(
        "core.utils.auto_member_profile_field.AutoMemberProfileFieldAtGroup._get_changed_fields"
    )
    def test_changed_to_foo(self, mocked_get_changed_fields):
        mocked_get_changed_fields.return_value = {
            self.profile_field.guid: ["option foo"]
        }

        user_without_profile = self.create_user(field_value=None)
        user_with_foo_profile = self.create_user(field_value="option foo")
        user_with_bar_profile = self.create_user(field_value="option bar")

        self.auto_members.apply()

        self.assertFalse(self.group.is_member(user_without_profile))
        self.assertTrue(self.group.is_member(user_with_foo_profile))
        self.assertFalse(self.group.is_member(user_with_bar_profile))

    @mock.patch(
        "core.utils.auto_member_profile_field.AutoMemberProfileFieldAtGroup._get_changed_fields"
    )
    def test_changed_to_bar(self, mocked_get_changed_fields):
        mocked_get_changed_fields.return_value = {
            self.profile_field.guid: ["option bar"]
        }

        user_without_profile = self.create_user(field_value=None)
        user_with_foo_profile = self.create_user(field_value="option foo")
        user_with_bar_profile = self.create_user(field_value="option bar")

        self.auto_members.apply()

        self.assertFalse(self.group.is_member(user_without_profile))
        self.assertFalse(self.group.is_member(user_with_foo_profile))
        self.assertTrue(self.group.is_member(user_with_bar_profile))


class TestIsMatchTestCase(PleioTenantTestCase):
    def test_field_type_select_field(self):
        options = ["option foo", "option bar"]
        field_type = "select_field"

        self.assertTrue(_is_match("option foo", options, field_type))
        self.assertTrue(_is_match("option bar", options, field_type))
        self.assertFalse(_is_match("option baz", options, field_type))
        self.assertFalse(_is_match("", options, field_type))
        self.assertFalse(_is_match(None, options, field_type))
        self.assertFalse(_is_match(None, "", field_type))
        self.assertFalse(_is_match(None, None, field_type))
        self.assertFalse(_is_match("", None, field_type))
        self.assertFalse(_is_match("", [], field_type))

    def test_field_type_multi_select_field(self):
        options = ["option foo", "option bar"]
        field_type = "multi_select_field"

        self.assertTrue(_is_match("option foo", options, field_type))
        self.assertTrue(_is_match("option foo, option bar", options, field_type))
        self.assertTrue(_is_match("option bar", options, field_type))
        self.assertTrue(_is_match("option bar, option baz", options, field_type))
        self.assertFalse(_is_match("option baz", options, field_type))
        self.assertFalse(_is_match("option baz, option qux", options, field_type))
        self.assertFalse(_is_match("option qux", options, field_type))
        self.assertFalse(_is_match("option foo", [], field_type))
        self.assertFalse(_is_match(None, None, field_type))
        self.assertFalse(_is_match(None, [], field_type))
        self.assertFalse(_is_match(None, options, field_type))
        self.assertFalse(_is_match("", options, field_type))

    def test_other_field_types(self):
        options = ["option foo", "option bar"]
        field_types = ["text_field", "html_field", "date_field"]
        for field_type in field_types:
            self.assertFalse(
                _is_match("option foo", options, field_type), msg=field_type
            )
            self.assertFalse(
                _is_match("option bar", options, field_type), msg=field_type
            )
            self.assertFalse(
                _is_match("option baz", options, field_type), msg=field_type
            )
