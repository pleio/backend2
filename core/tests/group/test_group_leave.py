from unittest import mock

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class GroupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.member = self.remember(UserFactory(email="member@example.com"))

        self.group = self.remember(GroupFactory(owner=self.owner))
        self.group.join(self.member)

        self.mutation = """
        mutation RemoveMemberToGroup($input: leaveGroupInput!) {
            leaveGroup(input: $input) {
                group {
                    guid
                }
            }
        }
        """
        self.variables = {"input": {"guid": self.group.guid}}

    @mock.patch("core.models.group.Group.leave")
    def test_leave_a_group_graphql_resolver(self, leave):
        self.graphql_client.force_login(self.member)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(leave.called)
        self.assertEqual(leave.call_args.args, (self.member,))
        self.assertEqual(leave.call_args.kwargs, {"recursive": True})

    def test_leave_a_group_graphql_resolver_by_owner(self):
        with self.assertGraphQlError("could_not_leave"):
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_leave_a_group_triggers_refresh_index(self):
        with mock.patch("core.elasticsearch.schedule_index_document") as index_document:
            # When.
            self.group.leave(self.member, recursive=True)

            # Then.
            assert index_document.called_with(self.member)

    def test_leave_group(self):
        # Given.
        subgroup = self.group.subgroups.create(name="sub")
        subgroup.members.add(self.member)

        # When.
        self.group.leave(self.member, recursive=True)

        # Then.
        self.assertFalse(self.group.members.filter(user=self.member).exists())
        self.assertEqual(0, subgroup.members.count())

    def test_leave_group_nonrecursive(self):
        # Given.
        subgroup = self.group.subgroups.create(name="sub")
        subgroup.members.add(self.member)

        # When.
        self.group.leave(self.member, recursive=False)

        # Then.
        self.assertFalse(self.group.members.filter(user=self.member).exists())
        self.assertEqual(1, subgroup.members.count())
