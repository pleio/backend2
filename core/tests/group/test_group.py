from unittest import mock

from django.core.files.base import ContentFile
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, GROUP_MEMBER_ROLES
from core.factories import GroupFactory
from core.models import Group, GroupInvitation
from core.tests.helpers import PleioTenantTestCase
from entities.file.models import FileFolder
from user.factories import AdminFactory, UserFactory
from user.models import User


class GroupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.authenticatedUser = mixer.blend(User, name="yy")
        self.user1 = mixer.blend(User)
        self.user2 = mixer.blend(User, name="aa")
        self.user3 = mixer.blend(User)
        self.user4 = mixer.blend(User, name="xx")
        self.user5 = mixer.blend(User, name="yyy")
        self.user6 = mixer.blend(User, name="zz")
        self.inactive_user = mixer.blend(User, name="inactive", is_active=False)
        self.userAdmin = mixer.blend(User, roles=["ADMIN"])
        self.group = mixer.blend(
            Group, owner=self.authenticatedUser, introduction="introductionMessage"
        )
        self.group.join(self.authenticatedUser, "owner")
        self.group.join(self.user2, "member")
        self.group.join(self.user3, "pending")
        self.group.join(self.user4, "admin")
        self.group.join(self.user6, "admin")
        self.group.join(self.inactive_user)
        self.hidden_group = mixer.blend(
            Group, owner=self.authenticatedUser, is_hidden=True
        )

        self.file = FileFolder.objects.create(
            owner=self.authenticatedUser,
            upload=None,
            type=FileFolder.Types.FILE,
            parent=None,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
        )
        self.invitation = GroupInvitation.objects.create(
            code="7d97cea90c83722c7262",
            acting_user=self.authenticatedUser,
            invited_user=self.user1,
            group=self.group,
        )
        self.invitation = GroupInvitation.objects.create(
            acting_user=self.authenticatedUser,
            code="7d97cea90c83722c7262",
            invited_user=self.user3,
            group=self.group,
        )

    def tearDown(self):
        super().tearDown()

    def test_entity_group_invited_list(self):
        query = """
            query InvitedList($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ... on Group {
                        invited (limit:1){
                            total
                            edges {
                                id
                                invited
                                timeCreated
                                email
                                user {
                                    guid
                                    username
                                    name
                                    __typename
                                }
                                __typename
                            }
                            __typename
                        }
                    __typename
                    }
                    __typename
                }
            }
        """
        variables = {"guid": self.group.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], self.group.guid)
        self.assertEqual(data["entity"]["invited"]["total"], 2)
        self.assertEqual(len(data["entity"]["invited"]["edges"]), 1)

    def test_entity_group_invite_list(self):
        query = """
            query InviteAutoCompleteList($guid: String!, $q: String) {
                entity(guid: $guid) {
                    guid
                    ... on Group {
                        invite(q: $q) {
                            total
                            edges {
                                invited
                                user {
                                    guid
                                    username
                                    name
                                    __typename
                                }
                                __typename
                            }
                            __typename
                        }
                        __typename
                    }
                    __typename
                }
            }
        """
        variables = {"guid": self.group.guid, "q": ""}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], self.group.guid)
        self.assertEqual(data["entity"]["__typename"], "Group")
        self.assertEqual(data["entity"]["invite"]["total"], 3)
        self.assertEqual(len(data["entity"]["invite"]["edges"]), 3)

    def test_entity_group_invite_list_empty(self):
        query = """
            query InviteAutoCompleteList($guid: String!, $q: String) {
                entity(guid: $guid) {
                    guid
                    ... on Group {
                        invite(q: $q) {
                            total
                            edges {
                                invited
                                user {
                                    guid
                                    username
                                    name
                                    __typename
                                }
                                __typename
                            }
                            __typename
                        }
                        __typename
                    }
                    __typename
                }
            }
        """
        variables = {
            "guid": self.group.guid,
            "q": "DFWETCCVSDFFSDGSER43254457453tqertq345",
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], self.group.guid)
        self.assertEqual(data["entity"]["__typename"], "Group")
        self.assertEqual(data["entity"]["invite"]["total"], 0)

    def test_entity_group_membership_request_list(self):
        query = """
            query MembershipRequestsList($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ... on Group {
                    membershipRequests {
                        total
                        edges {
                            guid
                            username
                            name
                            __typename
                        }
                        __typename
                    }
                    __typename
                    }
                    __typename
                }
            }
        """
        variables = {"guid": self.group.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], self.group.guid)
        self.assertEqual(data["entity"]["__typename"], "Group")
        self.assertEqual(data["entity"]["membershipRequests"]["total"], 1)
        self.assertEqual(
            data["entity"]["membershipRequests"]["edges"][0]["guid"], self.user3.guid
        )

    def test_entity_group_memberlist(self):
        query = """
            query MembersList($guid: String!, $q: String, $offset: Int) {
                entity(guid: $guid) {
                    ... on Group {
                        guid
                        memberCount
                        members(q: $q, offset: $offset, limit: 20) {
                            total
                            edges {
                                roles
                                isOwner
                                email
                                memberSince
                                user {
                                    guid
                                    username
                                    url
                                    name
                                }
                            }
                        }
                    }
                }
            }

        """
        variables = {"guid": self.group.guid}

        member_owner = self.group.members.get(user=self.authenticatedUser)
        member_admin = self.group.members.filter(
            roles__contains=[GROUP_MEMBER_ROLES.GROUP_ADMIN]
        ).first()

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], self.group.guid)
        self.assertEqual(data["entity"]["memberCount"], 4)
        self.assertEqual(data["entity"]["members"]["total"], 4)
        self.assertEqual(len(data["entity"]["members"]["edges"]), 4)

        self.assertEqual(data["entity"]["members"]["edges"][0]["isOwner"], True)
        self.assertEqual(
            data["entity"]["members"]["edges"][0]["memberSince"],
            member_owner.created_at.isoformat(),
        )
        self.assertEqual(
            data["entity"]["members"]["edges"][0]["email"], member_owner.user.email
        )

        self.assertIn("admin", data["entity"]["members"]["edges"][1]["roles"])
        self.assertEqual(
            data["entity"]["members"]["edges"][1]["memberSince"],
            member_admin.created_at.isoformat(),
        )
        self.assertEqual(
            data["entity"]["members"]["edges"][1]["email"], member_admin.user.email
        )

    def test_group_is_hidden_by_admin(self):
        query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        guid
                    }
                }
            }

        """
        variables = {"guid": self.hidden_group.guid}

        self.graphql_client.force_login(self.userAdmin)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], str(self.hidden_group.id))

    def test_group_hidden_introduction(self):
        query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        introduction
                    }
                }
            }

        """
        variables = {"guid": self.group.guid}

        self.graphql_client.force_login(self.user5)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["introduction"], "")

    def test_group_can_change_ownership_member_owner(self):
        query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        canChangeOwnership
                        isOwner
                    }
                }
            }

        """
        variables = {"guid": self.group.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["canChangeOwnership"], True)
        self.assertEqual(data["entity"]["isOwner"], True)

    def test_group_can_change_ownership_member_admin(self):
        query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        canChangeOwnership
                        isOwner
                    }
                }
            }

        """
        variables = {"guid": self.group.guid}

        self.graphql_client.force_login(self.user4)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["canChangeOwnership"], False)
        self.assertEqual(data["entity"]["isOwner"], False)

    def test_group_can_change_ownership_site_admin(self):
        query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        canChangeOwnership
                    }
                }
            }

        """
        variables = {"guid": self.group.guid}

        self.graphql_client.force_login(self.userAdmin)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["canChangeOwnership"], True)

    def test_group_cannot_change_ownership_anonymous(self):
        self.override_config(IS_CLOSED=False)
        query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        canChangeOwnership
                    }
                }
            }

        """
        variables = {"guid": self.group.guid}

        result = self.graphql_client.post(query, variables)
        data = result["data"]

        self.assertEqual(data["entity"]["canChangeOwnership"], False)

    inaccessible_field = ["members", "invite", "invited", "membershipRequests"]

    def test_unaccessible_data_for_unauthenticated_user(self):
        self.override_config(IS_CLOSED=False)
        for field in self.inaccessible_field:
            with self.subTest():
                query = """
                    query Group($guid: String!) {{
                        entity(guid: $guid) {{
                            ... on Group {{
                                {field} {{ total }}
                            }}
                        }}
                    }}
                """.format(field=field)

                variables = {"guid": self.group.guid}

                self.graphql_client.reset()
                result = self.graphql_client.post(query, variables)

                self.assertEqual(result["data"]["entity"][field]["total"], 0)


class TestGroupModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.member = self.remember(UserFactory(email="member@example.com"))
        self.nomember = self.remember(UserFactory(email="nomember@example.com"))

        self.group = self.remember(GroupFactory(owner=self.owner))
        self.group.join(self.member)

    def test_join_a_group_adds_a_member(self):
        # Given.
        self.assertFalse(self.group.members.filter(user=self.nomember).exists())

        # When.
        self.group.join(self.nomember)

        # Then.
        select_nomember = self.group.members.filter(user=self.nomember)
        self.assertTrue(select_nomember.exists())
        self.assertEqual(select_nomember.first().type, "member")

    def test_join_a_group_as_admin(self):
        # When.
        self.group.join(self.nomember, "admin")

        # Then.
        select_nomember = self.group.members.filter(user=self.nomember).first()
        self.assertTrue(self.group.is_admin(select_nomember.user))

    def test_join_a_group_triggers_refresh_index(self):
        with mock.patch("core.elasticsearch.schedule_index_document") as index_document:
            self.group.join(self.nomember)
            self.assertTrue(index_document.called_with(self.nomember))

    @mock.patch(
        "core.mail_builders.group_change_ownership.schedule_change_group_ownership_mail"
    )
    def test_change_ownership(self, schedule_mail):
        actor = self.remember(UserFactory(email="actor@example.com"))
        prev_owner = self.group.members.get(user=self.owner)
        new_owner = self.group.members.get(user=self.member)

        self.group.change_ownership(self.member, actor)
        self.group.refresh_from_db()
        prev_owner.refresh_from_db()
        new_owner.refresh_from_db()

        self.assertTrue(self.group.is_admin(prev_owner.user))
        self.assertTrue(self.group.is_owner(new_owner.user))
        self.assertEqual(self.group.owner, self.member)
        self.assertTrue(schedule_mail.called)
        self.assertTrue(
            schedule_mail.called_with(user=self.member, sender=actor, group=self.group)
        )


class TestGroupDefaultAccessIdTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory(name="Owner")
        self.open_group = GroupFactory(owner=self.owner, name="Open", is_closed=False)
        self.closed_group = GroupFactory(
            owner=self.owner, name="Closed", is_closed=True
        )

        self.query = """
        fragment GroupProperties on Group{
            defaultReadAccessId
            defaultWriteAccessId
            readAccessIds {
                id
                description
            }
            writeAccessIds {
                id
                description
            }
        }
        query EntityQuery($open: String!, $closed: String!) {
            open: entity(guid: $open) {
                ...GroupProperties
            }
            closed: entity(guid: $closed) {
                ...GroupProperties
            }
        }
        """
        self.variables = {
            "open": self.open_group.guid,
            "closed": self.closed_group.guid,
        }

    def test_group_access_ids_closed_site(self):
        self.override_config(IS_CLOSED=True)
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.query, self.variables)
        data = response["data"]
        self.assertDictEqual(
            data["open"],
            {
                "defaultReadAccessId": 1,
                "defaultWriteAccessId": 0,
                "readAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Open"},
                    {"id": 1, "description": "Ingelogde gebruikers"},
                ],
                "writeAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Open"},
                    {"id": 1, "description": "Ingelogde gebruikers"},
                ],
            },
        )
        self.assertDictEqual(
            data["closed"],
            {
                "defaultReadAccessId": 4,
                "defaultWriteAccessId": 0,
                "readAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Closed"},
                ],
                "writeAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Closed"},
                ],
            },
        )

    def test_group_access_ids_open_site(self):
        self.override_config(IS_CLOSED=False)
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.query, self.variables)
        data = response["data"]
        self.maxDiff = None
        self.assertDictEqual(
            data["open"],
            {
                "defaultReadAccessId": 1,
                "defaultWriteAccessId": 0,
                "readAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Open"},
                    {"id": 1, "description": "Ingelogde gebruikers"},
                    {"id": 2, "description": "Iedereen (publiek zichtbaar)"},
                ],
                "writeAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Open"},
                    {"id": 1, "description": "Ingelogde gebruikers"},
                ],
            },
        )
        self.assertDictEqual(
            data["closed"],
            {
                "defaultReadAccessId": 4,
                "defaultWriteAccessId": 0,
                "readAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Closed"},
                ],
                "writeAccessIds": [
                    {"id": 0, "description": "Alleen eigenaar"},
                    {"id": 4, "description": "Groep: Closed"},
                ],
            },
        )


class GroupStatsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        csv_bytes = (
            b"aap;row-1-2@example.com;row-1-3;row-1-4;row-1-5\n"
            b"noot;row-2-2@example.com;row-2-3;row-2-4;row-2-5\n"
            b"mies;row-3-2@example.com;row-3-3;row-3-4;row-3-5"
        )

        upload = ContentFile(csv_bytes, "test.csv")

        csv_bytes2 = (
            b"aap;row-1-2@example.com;row-1-3;row-1-4;row-1-5\n"
            b"noot;row-2-2@example.com;row-2-3;row-2-4;row-2-5\n"
            b"aap;row-1-2@example.com;row-1-3;row-1-4;row-1-5\n"
            b"noot;row-2-2@example.com;row-2-3;row-2-4;row-2-5\n"
            b"mies;row-3-2@example.com;row-3-3;row-3-4;row-3-5"
        )

        upload2 = ContentFile(csv_bytes2, "test.csv")

        self.admin = mixer.blend(User, roles=["ADMIN"], is_delete_requested=False)
        self.groupowner = self.remember(UserFactory(email="owner@example.com"))
        self.groupadmin = self.remember(UserFactory(email="member@example.com"))
        self.groupuser = self.remember(UserFactory(email="nomember@example.com"))

        self.group = self.remember(GroupFactory(owner=self.groupowner))
        self.group.join(
            self.groupowner,
        )
        self.group.join(self.groupadmin, "admin")
        self.group.join(self.groupuser)

        self.file = mixer.blend(
            FileFolder,
            type=FileFolder.Types.FILE,
            upload=upload,
            group=self.group,
        )
        self.file2 = mixer.blend(
            FileFolder,
            type=FileFolder.Types.FILE,
            upload=upload2,
            group=self.group,
        )
        self.file3 = mixer.blend(
            FileFolder, type=FileFolder.Types.FILE, upload=upload, size=1000
        )
        self.variables = {"guid": self.group.guid}
        self.query = """
            query Group($guid: String!) {
                entity(guid: $guid) {
                    ... on Group {
                        fileDiskUsage
                    }
                }
            }

        """

    def tearDown(self):
        super().tearDown()

    def test_get_group_stats_by_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, self.variables)
        data = result["data"]

        self.assertEqual(data["entity"]["fileDiskUsage"], 387.0)

    def test_get_group_stats_by_group_admin(self):
        self.graphql_client.force_login(self.groupadmin)
        result = self.graphql_client.post(self.query, self.variables)
        data = result["data"]

        self.assertEqual(data["entity"]["fileDiskUsage"], 387.0)

    def test_get_group_stats_by_group_owner(self):
        self.graphql_client.force_login(self.groupowner)
        result = self.graphql_client.post(self.query, self.variables)
        data = result["data"]

        self.assertEqual(data["entity"]["fileDiskUsage"], 387.0)

    def test_get_group_stats_by_group_member(self):
        self.graphql_client.force_login(self.groupuser)
        result = self.graphql_client.post(self.query, self.variables)
        data = result["data"]

        self.assertEqual(data["entity"]["fileDiskUsage"], 0.0)

    def test_get_group_stats_by_anonymous(self):
        self.override_config(IS_CLOSED=False)
        result = self.graphql_client.post(self.query, self.variables)
        data = result["data"]

        self.assertEqual(data["entity"]["fileDiskUsage"], 0.0)


class TestGroupAccess(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.group_owner = UserFactory(email="group_owner@example.com")

        self.open_group = GroupFactory(
            owner=self.group_owner, name="Open group", is_closed=False
        )
        self.closed_group = GroupFactory(
            owner=self.group_owner, name="Closed group", is_closed=True
        )
        self.hidden_group = GroupFactory(
            owner=self.group_owner, name="Hidden group", is_closed=True, is_hidden=True
        )

        self.query = """
        fragment GroupDetails on Group {
            name
            canEdit
            canArchiveAndDelete
        }
        query GetGroups($guidOpen: String!, $guidClosed: String!, $guidHidden: String!) {
            openGroup: entity(guid: $guidOpen) {
                ...GroupDetails
            }
            closedGroup: entity(guid: $guidClosed) {
                ...GroupDetails
            }
            hiddenGroup: entity(guid: $guidHidden) {
                ...GroupDetails
            }
            groups {
                total
                edges {
                    name
                }
            }
        }
        """

        self.variables = {
            "guidOpen": self.open_group.guid,
            "guidClosed": self.closed_group.guid,
            "guidHidden": self.hidden_group.guid,
        }

    def test_anonymous_access(self):
        result = self.graphql_client.post(self.query, self.variables)

        self.assertContainsGroups(result["data"]["groups"], set())
        self.assertIsNone(result["data"]["openGroup"])
        self.assertIsNone(result["data"]["closedGroup"])
        self.assertIsNone(result["data"]["hiddenGroup"])

    def test_anonymous_open_site_group_access(self):
        self.override_config(IS_CLOSED=False)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertNonMemberAccess(result)

    def test_authenticated_access(self):
        self.graphql_client.force_login(UserFactory())
        result = self.graphql_client.post(self.query, self.variables)

        self.assertNonMemberAccess(result)

    def make_member(self, user, role):
        self.open_group.join(user, role)
        self.closed_group.join(user, role)
        self.hidden_group.join(user, role)
        return user

    def test_member_access(self):
        self.graphql_client.force_login(self.make_member(UserFactory(), "member"))
        result = self.graphql_client.post(self.query, self.variables)

        self.assertMemberAccess(result)

    def test_group_admin_access(self):
        self.graphql_client.force_login(self.make_member(UserFactory(), "admin"))
        result = self.graphql_client.post(self.query, self.variables)

        self.assertAdministratorAccess(result)

    def test_group_owner_access(self):
        self.graphql_client.force_login(self.group_owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertAdministratorAccess(result)

    def test_site_admin_access(self):
        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(self.query, self.variables)

        self.assertAdministratorAccess(result)

    def assertNonMemberAccess(self, result):
        self.assertContainsGroups(
            result["data"]["groups"], {self.open_group.name, self.closed_group.name}
        )
        self.assertNotHasAccess(result["data"]["openGroup"])
        self.assertNotHasAccess(result["data"]["closedGroup"])
        self.assertIsNone(result["data"]["hiddenGroup"])

    def assertMemberAccess(self, result):
        self.assertContainsGroups(
            result["data"]["groups"],
            {self.open_group.name, self.closed_group.name, self.hidden_group.name},
        )
        self.assertNotHasAccess(result["data"]["openGroup"])
        self.assertNotHasAccess(result["data"]["closedGroup"])
        self.assertNotHasAccess(result["data"]["hiddenGroup"])

    def assertAdministratorAccess(self, result):
        self.assertContainsGroups(
            result["data"]["groups"],
            {self.open_group.name, self.closed_group.name, self.hidden_group.name},
        )
        self.assertHasAccess(result["data"]["openGroup"])
        self.assertHasAccess(result["data"]["closedGroup"])
        self.assertHasAccess(result["data"]["hiddenGroup"])

    def assertHasAccess(self, group):
        self.assertTrue(group["canEdit"])
        self.assertTrue(group["canArchiveAndDelete"])

    def assertNotHasAccess(self, group):
        self.assertFalse(group["canEdit"])
        self.assertFalse(group["canArchiveAndDelete"])

    def assertContainsGroups(self, groups, names):
        self.assertEqual(groups["total"], len(names))
        self.assertEqual({edge["name"] for edge in groups["edges"]}, names)
