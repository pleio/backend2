from unittest import mock

from mixer.backend.django import mixer

from core.models import Group, GroupInvitation
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class AcceptGroupInvitationTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory())
        self.aspirant = self.remember(UserFactory())
        self.member = self.remember(UserFactory())
        self.group = self.remember(mixer.blend(Group, owner=self.owner))
        self.invitation = self.remember(
            GroupInvitation.objects.create(
                code="7d97cea90c83722c7262",
                acting_user=self.owner,
                invited_user=self.aspirant,
                group=self.group,
            )
        )
        self.group.join(self.member, "member")

        self.mutation = """
            mutation Invitations($input: acceptGroupInvitationInput!) {
                acceptGroupInvitation(input: $input) {
                    group {
                        guid
                        members { total }
                    }
                }
            }
        """

        self.variables = {"input": {"code": self.invitation.code}}

    def test_accept_group_inivitation(self):
        self.graphql_client.force_login(self.aspirant)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.assertEqual(
            data["acceptGroupInvitation"]["group"]["guid"], self.group.guid
        )
        self.assertEqual(data["acceptGroupInvitation"]["group"]["members"]["total"], 2)

    def test_accept_group_inivitation_twice(self):
        self.graphql_client.force_login(self.aspirant)
        self.graphql_client.post(self.mutation, self.variables)

        with self.assertGraphQlError("invalid_code"):
            self.graphql_client.post(self.mutation, self.variables)

    @mock.patch("core.models.group.Group.join")
    def test_accept_group_invitation_for_admin_role(self, join):
        self.invitation.role = "admin"
        self.invitation.save()

        self.graphql_client.force_login(self.aspirant)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(join.called)
        self.assertTrue(join.called_with(self.aspirant, "admin"))

    @mock.patch("core.models.group.Group.change_ownership")
    def test_accept_group_invitation_for_owner_role(self, change_ownership):
        self.invitation.role = "owner"
        self.invitation.save()

        self.graphql_client.force_login(self.aspirant)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(change_ownership.called)
        self.assertTrue(change_ownership.called_with(self.aspirant, self.owner))
