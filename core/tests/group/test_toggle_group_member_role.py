from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class ToggleGroupMemberRoleTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.group_owner = self.remember(UserFactory(email="user1@example.com"))
        self.user2 = self.remember(UserFactory(email="user2@example.com"))
        self.user3 = self.remember(UserFactory(email="user3@example.com"))
        self.group_admin = self.remember(UserFactory(email="user4@example.com"))
        self.admin = self.remember(AdminFactory(email="admin@example.com"))
        self.group1 = self.remember(GroupFactory(name="Group1", owner=self.group_owner))
        self.group1.join(self.user2, "member")
        self.group1.join(self.group_admin, "admin")

        self.mutation = """
            mutation MemberItem($input: toggleGroupMemberRoleInput!) {
                toggleGroupMemberRole(input: $input) {
                    group {
                        ... on Group {
                            guid
                        }
                    }
                }
            }
        """

    def test_toggle_group_member_role_to_admin_by_group_owner(self):
        variables = {
            "input": {
                "guid": self.group1.guid,
                "userGuid": self.user2.guid,
                "role": "admin",
            }
        }
        self.assertFalse(self.group1.is_admin(self.user2))
        self.graphql_client.force_login(self.group_owner)

        # Toggle
        result = self.graphql_client.post(self.mutation, variables)
        self.assertTrue(self.group1.is_admin(self.user2))

        # Toggle again
        result = self.graphql_client.post(self.mutation, variables)
        self.assertFalse(self.group1.is_admin(self.user2))

        data = result["data"]["toggleGroupMemberRole"]

        self.assertEqual(data["group"]["guid"], self.group1.guid)

    def test_toggle_group_member_role_to_admin_by_group_admin(self):
        variables = {
            "input": {
                "guid": self.group1.guid,
                "userGuid": self.user2.guid,
                "role": "admin",
            }
        }
        self.assertFalse(self.group1.is_admin(self.user2))
        self.graphql_client.force_login(self.group_admin)

        # Toggle
        result = self.graphql_client.post(self.mutation, variables)
        self.assertTrue(self.group1.is_admin(self.user2))

        # Toggle again
        result = self.graphql_client.post(self.mutation, variables)
        self.assertFalse(self.group1.is_admin(self.user2))

        data = result["data"]["toggleGroupMemberRole"]

        self.assertEqual(data["group"]["guid"], self.group1.guid)

    def test_toggle_group_member_role_to_news_editor_by_group_owner(self):
        variables = {
            "input": {
                "guid": self.group1.guid,
                "userGuid": self.user2.guid,
                "role": "newsEditor",
            }
        }
        self.assertFalse(self.group1.is_admin(self.user2))
        self.graphql_client.force_login(self.group_owner)

        # Toggle
        result = self.graphql_client.post(self.mutation, variables)
        self.assertTrue(self.group1.is_news_editor(self.user2))

        # Toggle again
        result = self.graphql_client.post(self.mutation, variables)
        self.assertFalse(self.group1.is_news_editor(self.user2))

        data = result["data"]["toggleGroupMemberRole"]

        self.assertEqual(data["group"]["guid"], self.group1.guid)

    def test_toggle_group_member_role_to_admin_by_other_user(self):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.user3)
            self.graphql_client.post(self.mutation, variables)

    def test_toggle_group_member_role_to_admin_by_anonymous_user(self):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)
