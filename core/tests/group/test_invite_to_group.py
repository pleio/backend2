from unittest import mock

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class InviteToGroupTestCase(PleioTenantTestCase):
    EXTERNAL_MAIL = "external@example.com"

    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.aspirant = self.remember(UserFactory(email="aspirant@example.com"))
        self.admin = self.remember(AdminFactory(email="admin@example.com"))
        self.group = self.remember(GroupFactory(owner=self.owner))

        self.mutation = """
            mutation InviteItem($input: inviteToGroupInput!) {
                invite: inviteToGroup(input: $input) {
                    group {
                        ... on Group {
                            guid
                            members { edges { email } }
                            invited { edges { email } }
                        }
                    }
                }
            }
        """
        self.variables = {
            "input": {
                "directAdd": False,
                "guid": self.group.guid,
                "users": [{"guid": self.aspirant.guid}],
            }
        }

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.schedule_invite_to_group_mail"
    )
    def test_invite_to_group_by_guid_by_group_owner(self, mocked_mail):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        data = result["data"]

        self.assertEqual(data["invite"]["group"]["guid"], self.group.guid)
        self.assertEqual(mocked_mail.call_count, 1)

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.schedule_invite_to_group_mail"
    )
    def test_invite_to_group_by_email_by_group_owner(self, mocked_mail):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        data = result["data"]

        self.assertEqual(data["invite"]["group"]["guid"], self.group.guid)
        self.assertEqual(mocked_mail.call_count, 1)

    def test_direct_add_users_to_group_by_group_owner(self):
        self.variables["input"]["directAdd"] = True

        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_direct_add_users_to_group_by_admin(self):
        self.variables["input"]["directAdd"] = True

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.assertEqual(data["invite"]["group"]["guid"], self.group.guid)
        self.assertEqual(len(self.group.members.all()), 2)

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.schedule_invite_to_group_mail"
    )
    def test_invite_non_site_member_to_group_by_guid_by_group_owner(self, mocked_mail):
        self.variables["input"]["users"] = [{"email": self.EXTERNAL_MAIL}]

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        data = result["data"]

        self.assertEqual(data["invite"]["group"]["guid"], self.group.guid)
        self.assertEqual(mocked_mail.call_count, 1)

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.InviteToGroupMutation.get_or_create_remote_user"
    )
    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.schedule_invite_to_group_mail"
    )
    def test_invite_to_group_with_mail_with_new_user(self, mocked_mail, create_user):
        new_user = self.remember(UserFactory(email="new_user@example.com"))
        create_user.return_value = new_user
        self.variables["input"]["directAdd"] = True
        # Next is a different address then new_user.mail, for else the user is found to early.
        self.variables["input"]["users"] = [
            {"guid": self.aspirant.guid},
            {"email": self.EXTERNAL_MAIL},
        ]

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        self.assertEqual(
            {
                edge["email"]
                for edge in result["data"]["invite"]["group"]["members"]["edges"]
            },
            {self.owner.email, self.aspirant.email, new_user.email},
        )

    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.InviteToGroupMutation.get_or_create_remote_user"
    )
    @mock.patch(
        "core.resolvers.mutations.mutation_invite_to_group.schedule_invite_to_group_mail"
    )
    def test_invite_to_group_with_mail_without_new_user(self, mocked_mail, create_user):
        create_user.return_value = None
        self.variables["input"]["directAdd"] = True
        self.variables["input"]["users"] = [
            {"guid": self.aspirant.guid},
            {"email": "external@example.com"},
        ]

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        self.assertEqual(
            {
                edge["email"]
                for edge in result["data"]["invite"]["group"]["members"]["edges"]
            },
            {self.owner.email, self.aspirant.email},
        )
        self.assertEqual(
            [
                edge["email"]
                for edge in result["data"]["invite"]["group"]["invited"]["edges"]
            ],
            ["external@example.com"],
        )

    def test_invite_to_group_as_admin(self):
        self.variables["input"]["users"][0]["role"] = "admin"

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.mutation, self.variables)
        invitation = self.group.invitations.get(invited_user=self.aspirant)

        self.assertEqual(invitation.role, "admin")

    @mock.patch("core.models.group.Group.join")
    def test_add_to_group_as_admin(self, join):
        self.variables["input"]["directAdd"] = True
        self.variables["input"]["users"][0]["role"] = "admin"

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(join.called)
        self.assertTrue(join.called_with(self.aspirant, "admin"))
