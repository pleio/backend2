from unittest import mock

from django.core.files.base import ContentFile

from core import override_local_config
from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase, override_config
from entities.file.factories import FileFactory
from user.factories import AdminFactory, UserFactory


class AddGroupCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.auto_member_profile_apply = mock.patch(
            "core.utils.auto_member_profile_field.AutoMemberProfileFieldAtGroup.apply"
        ).start()
        self.user = UserFactory()
        self.admin = AdminFactory()
        self.profile_field = ProfileField.objects.create(
            key="profile_field",
            name="text_name",
            field_type="select_field",
            is_in_auto_group_membership=True,
            field_options=["foo", "bar", "baz"],
        )

        self.mutation = """
            mutation ($group: addGroupInput!) {
                addGroup(input: $group) {
                    group {
                        name
                        canEdit
                        canArchiveAndDelete
                        icon { embed }
                        richDescription
                        introduction
                        isIntroductionPublic
                        welcomeMessage
                        requiredProfileFieldsMessage
                        isClosed
                        isHidden
                        isMenuAlwaysVisible
                        isMembershipOnRequest
                        isFeatured
                        isChatEnabled
                        autoNotification
                        fileNotification
                        tags
                        isLeavingGroupDisabled
                        isAutoMembershipEnabled
                        requiredProfileFields {
                            guid
                        }
                        showMemberProfileFields {
                            guid
                            name
                        }
                        defaultTags
                        defaultTagCategories {
                            name
                            values
                        }
                        plugins
                        startPage { title }
                        menu {
                            ...GroupMenuFragment
                        }
                        autoMembershipFields {
                            field { guid }
                            value
                        }
                    }
                }
            }
            fragment GroupMenuFragment on GroupMenuItem {
                type
                ... on GroupMenuPluginItem {
                    plugin
                    label
                    id
                }
                ... on GroupMenuPageItem {
                    label
                    id
                }
                ... on GroupMenuLinkItem {
                    label
                    id
                }
                ... on GroupSubmenuItem {
                    label
                    id
                    submenu {
                        type
                        ... on GroupMenuPluginItem {
                            plugin
                            label
                            id
                        }
                        ... on GroupMenuPageItem {
                            label
                            id
                        }
                        ... on GroupMenuLinkItem {
                            label
                            id
                        }
                    }
                }
            }
        """

        self.icon = FileFactory(owner=self.user, upload=ContentFile(b"", "icon.png"))

        self.data = {
            "group": {
                "name": "Name",
                "iconGuid": self.icon.guid,
                "richDescription": self.tiptap_paragraph("richDescription"),
                "introduction": "introductionMessage",
                "isIntroductionPublic": False,
                "welcomeMessage": "welcomeMessage",
                "isClosed": True,
                "isMembershipOnRequest": True,
                "isFeatured": True,
                "isAutoMembershipEnabled": True,
                "isLeavingGroupDisabled": True,
                "autoNotification": True,
                "fileNotification": True,
                "isHidden": True,
                "isMenuAlwaysVisible": False,
                "requiredProfileFieldsMessage": "I'd expect this message for requiredProfileFieldsMessage",
                "tags": ["tag_one", "tag_two"],
                "requiredProfileFieldGuids": [self.profile_field.guid],
                "defaultTags": ["tag_one", "tag_three"],
                "defaultTagCategories": [{"name": "Test", "values": ["One", "Two"]}],
                "autoMembershipFields": [
                    {"guid": self.profile_field.guid, "value": ["foo"]}
                ],
            }
        }

    @mock.patch("core.resolvers.scalars.secure_rich_text.Tiptap")
    def test_add_group_anon(self, mock_tiptap):
        variables = self.data

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)

        mock_tiptap.assert_called_once_with(self.data["group"]["richDescription"])
        mock_tiptap.return_value.check_for_external_urls.assert_called_once_with()

    def test_add_group(self):
        self.override_config(LIMITED_GROUP_ADD=False)

        variables = self.data

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        group = result["data"]["addGroup"]["group"]
        self.assertEqual(group["name"], variables["group"]["name"])
        self.assertIn("/icon.png", group["icon"]["embed"])
        self.assertEqual(
            group["richDescription"],
            variables["group"]["richDescription"],
        )
        self.assertEqual(
            group["introduction"],
            variables["group"]["introduction"],
        )
        self.assertEqual(
            group["isIntroductionPublic"],
            variables["group"]["isIntroductionPublic"],
        )
        self.assertEqual(
            group["welcomeMessage"],
            variables["group"]["welcomeMessage"],
        )
        self.assertEqual(
            group["requiredProfileFieldsMessage"],
            variables["group"]["requiredProfileFieldsMessage"],
        )
        self.assertEqual(group["isClosed"], variables["group"]["isClosed"])
        self.assertEqual(group["isHidden"], variables["group"]["isHidden"])
        self.assertEqual(
            group["isMembershipOnRequest"],
            variables["group"]["isMembershipOnRequest"],
        )
        self.assertEqual(group["isFeatured"], False)
        self.assertEqual(group["isMenuAlwaysVisible"], False)
        self.assertEqual(group["isLeavingGroupDisabled"], False)
        self.assertEqual(group["isAutoMembershipEnabled"], False)
        self.assertEqual(group["isChatEnabled"], False)
        self.assertEqual(
            group["autoNotification"],
            variables["group"]["autoNotification"],
        )
        self.assertEqual(
            group["fileNotification"],
            variables["group"]["fileNotification"],
        )
        self.assertEqual(group["tags"], ["tag_one", "tag_two"])
        self.assertEqual(
            group["requiredProfileFields"],
            [{"guid": self.profile_field.guid}],
        )
        self.assertEqual(group["defaultTags"], variables["group"]["defaultTags"])
        self.assertEqual(
            group["defaultTagCategories"],
            variables["group"]["defaultTagCategories"],
        )
        self.assertEqual(
            group["autoMembershipFields"],
            [{"field": {"guid": self.profile_field.guid}, "value": ["foo"]}],
        )
        self.assertEqual(group["plugins"], ["members"])
        self.assertEqual(
            group["menu"],
            [
                {
                    "label": "Leden",
                    "type": "plugin",
                    "plugin": "members",
                    "id": "members",
                }
            ],
        )
        self.assertIsNotNone(group["startPage"])
        self.assertEqual(group["startPage"]["title"], "Tijdlijn")
        self.assertTrue(group["canEdit"])
        self.assertTrue(group["canArchiveAndDelete"])

        self.assertTrue(self.auto_member_profile_apply.called)

    @override_local_config(LIMITED_GROUP_ADD=True)
    def test_add_group_limited_group_add(self):
        variables = self.data
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, variables)

    def test_add_group_by_admin(self):
        variables = self.data

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        group = result["data"]["addGroup"]["group"]
        self.assertEqual(group["name"], variables["group"]["name"])
        self.assertIn("/icon.png", group["icon"]["embed"])
        self.assertEqual(
            group["richDescription"], variables["group"]["richDescription"]
        )
        self.assertEqual(group["introduction"], variables["group"]["introduction"])
        self.assertEqual(group["welcomeMessage"], variables["group"]["welcomeMessage"])
        self.assertEqual(group["isClosed"], variables["group"]["isClosed"])
        self.assertEqual(group["isFeatured"], True)
        self.assertEqual(
            group["isMembershipOnRequest"],
            variables["group"]["isMembershipOnRequest"],
        )
        self.assertEqual(group["isLeavingGroupDisabled"], True)
        self.assertEqual(group["isAutoMembershipEnabled"], True)
        self.assertEqual(
            group["autoNotification"],
            variables["group"]["autoNotification"],
        )
        self.assertEqual(group["tags"], ["tag_one", "tag_two"])

    def test_add_group_member_fields(self):
        profile_field1 = ProfileField.objects.create(
            key="text_key", name="text_name", field_type="text_field"
        )

        variables = {
            "group": {
                "name": "Test123",
                "showMemberProfileFieldGuids": [str(profile_field1.id)],
            }
        }

        with override_config(
            PROFILE_SECTIONS=[
                {
                    "name": "section_one",
                    "profileFieldGuids": [profile_field1.guid],
                }
            ]
        ):
            self.graphql_client.force_login(self.admin)
            result = self.graphql_client.post(self.mutation, variables)

        group = result["data"]["addGroup"]["group"]
        self.assertEqual(group["name"], variables["group"]["name"])
        self.assertEqual(len(group["showMemberProfileFields"]), 1)
        self.assertEqual(
            group["showMemberProfileFields"][0]["guid"],
            profile_field1.guid,
        )

    def test_add_prohibited_member_fields(self):
        profile_field1 = ProfileField.objects.create(
            key="text_key", name="text_name", field_type="html_field"
        )

        variables = {
            "group": {
                "name": "Test123",
                "showMemberProfileFieldGuids": [str(profile_field1.id)],
            }
        }
        with override_config(
            PROFILE_SECTIONS=[
                {
                    "name": "section_one",
                    "profileFieldGuids": [profile_field1.guid],
                }
            ]
        ):
            with self.assertGraphQlError(
                "Long text fields are not allowed to display on the member page."
            ):
                self.graphql_client.force_login(self.admin)
                self.graphql_client.post(self.mutation, variables)

    def test_add_group_multiple_plugins(self):
        self.data["group"]["plugins"] = ["blog", "files", "members"]
        self.data["group"]["menu"] = [
            {"type": "plugin", "id": "blog"},
            {"type": "plugin", "id": "files"},
        ]
        variables = self.data

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        self.assertEqual(
            result["data"]["addGroup"]["group"]["plugins"],
            ["blog", "files", "members"],
        )
        self.assertEqual(
            result["data"]["addGroup"]["group"]["menu"],
            [
                {
                    "label": "Blogs",
                    "type": "plugin",
                    "plugin": "blog",
                    "id": "blog",
                },
                {
                    "label": "Bestanden",
                    "type": "plugin",
                    "plugin": "files",
                    "id": "files",
                },
            ],
        )

    def test_add_minimum_group(self):
        self.variables = {
            "group": {
                "name": "Name",
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        self.maxDiff = None

        self.assertEqual(
            result["data"]["addGroup"]["group"],
            {
                "autoNotification": False,
                "defaultTagCategories": [],
                "defaultTags": [],
                "fileNotification": False,
                "icon": None,
                "introduction": "",
                "isAutoMembershipEnabled": False,
                "isChatEnabled": False,
                "isClosed": False,
                "isFeatured": False,
                "isHidden": False,
                "isIntroductionPublic": False,
                "isLeavingGroupDisabled": False,
                "isMenuAlwaysVisible": True,
                "isMembershipOnRequest": False,
                "menu": [
                    {
                        "id": "members",
                        "label": "Leden",
                        "plugin": "members",
                        "type": "plugin",
                    }
                ],
                "name": "Name",
                "plugins": ["members"],
                "requiredProfileFields": [],
                "requiredProfileFieldsMessage": "",
                "richDescription": "",
                "showMemberProfileFields": [],
                "startPage": {"title": "Tijdlijn"},
                "tags": [],
                "welcomeMessage": "",
                "autoMembershipFields": [
                    {
                        "field": {"guid": self.profile_field.guid},
                        "value": [],
                    }
                ],
                "canEdit": True,
                "canArchiveAndDelete": True,
            },
        )

    def test_add_group_submenu(self):
        self.data["group"]["plugins"] = ["blog", "files", "members"]
        self.data["group"]["menu"] = [
            {"type": "plugin", "id": "blog"},
            {"type": "plugin", "id": "files"},
            {"type": "link", "id": "/test", "label": "Test"},
            {
                "type": "submenu",
                "label": "Submenu",
                "submenu": [
                    {"type": "plugin", "id": "members", "label": "Leden"},
                    {"type": "plugin", "id": "blog", "label": "Blogs"},
                ],
            },
        ]
        variables = self.data

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        self.assertEqual(
            result["data"]["addGroup"]["group"]["plugins"],
            ["blog", "files", "members"],
        )
        self.assertEqual(
            result["data"]["addGroup"]["group"]["menu"],
            [
                {
                    "label": "Blogs",
                    "type": "plugin",
                    "plugin": "blog",
                    "id": "blog",
                },
                {
                    "label": "Bestanden",
                    "type": "plugin",
                    "plugin": "files",
                    "id": "files",
                },
                {"label": "Test", "type": "link", "id": "/test"},
                {
                    "id": "Submenu",
                    "label": "Submenu",
                    "type": "submenu",
                    "submenu": [
                        {
                            "label": "Leden",
                            "type": "plugin",
                            "plugin": "members",
                            "id": "members",
                        },
                        {
                            "label": "Blogs",
                            "type": "plugin",
                            "plugin": "blog",
                            "id": "blog",
                        },
                    ],
                },
            ],
        )
