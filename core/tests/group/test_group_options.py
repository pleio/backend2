from core.factories import GroupFactory
from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestGroupOptionHideJoinButton(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()

        self.query = """
        query GetGroup($guid: String!) {
            entity(guid: $guid) {
                ... on Group {
                    guid
                    isJoinButtonVisible
                }
            }
        }
        """
        self.mutation = """
        mutation editGroup($input: editGroupInput!) {
            editGroup(input: $input) {
                group {
                    guid
                    isJoinButtonVisible
                }
            }
        }
        """
        self.add_mutation = """
        mutation addGroup($input: addGroupInput!) {
            addGroup(input: $input) {
                group {
                    name
                    isJoinButtonVisible
                }
            }
        }
        """

    def test_query_group(self):
        """
        Test that the isJoinButtonVisible field is true by default.
        """
        self.group = GroupFactory(owner=self.owner)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"guid": self.group.guid})
        group_data = result["data"]["entity"]

        self.assertEqual(
            group_data, {"guid": self.group.guid, "isJoinButtonVisible": True}
        )

    def test_mutation_edit_group(self):
        """
        Test that the isJoinButtonVisible field can be set to false at edit time.
        """
        self.group = GroupFactory(owner=self.owner)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(
            self.mutation,
            {"input": {"guid": self.group.guid, "isJoinButtonVisible": False}},
        )
        group_data = result["data"]["editGroup"]["group"]
        self.assertEqual(
            group_data, {"guid": self.group.guid, "isJoinButtonVisible": False}
        )

    def test_mutation_add_group(self):
        """
        Test that the isJoinButtonVisible field can be set to false at creation time.
        """
        self.override_config(LIMITED_GROUP_ADD=False)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(
            self.add_mutation,
            {"input": {"name": "Test group", "isJoinButtonVisible": False}},
        )
        group_data = result["data"]["addGroup"]["group"]
        self.assertEqual(
            group_data, {"name": "Test group", "isJoinButtonVisible": False}
        )


class TestAutoMembershipFieldsProperty(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.profile_field = ProfileField.objects.create(
            key="profile_field",
            name="text_name",
            field_type="select_field",
            is_in_auto_group_membership=True,
        )
        self.group = GroupFactory(
            owner=self.owner,
            auto_membership_fields=[
                {"guid": self.profile_field.guid, "value": ["test"]}
            ],
        )

        self.query = """
        query GetGroup($guid: String!) {
            entity(guid: $guid) {
                ... on Group {
                    autoMembershipFields {
                        field {
                            guid
                            key
                        }
                        value
                    }
                }
            }
        }
        """
        self.variables = {"guid": self.group.guid}

    def test_owner_access_to_group(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(
            result["data"]["entity"]["autoMembershipFields"],
            [
                {
                    "field": {
                        "guid": self.profile_field.guid,
                        "key": self.profile_field.key,
                    },
                    "value": ["test"],
                }
            ],
        )
