from core.factories import GroupFactory
from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory


class TestSearchWithExcludedContentTypesTestCase(
    Template.TestSearchWithExcludedContentTypesTestCase
):
    EXCLUDE_TYPES = ["group", "page"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return GroupFactory(name=title, owner=self.owner)
