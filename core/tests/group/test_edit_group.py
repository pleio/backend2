from unittest import mock

from django.core.files.base import ContentFile

from core import constances
from core.factories import GroupFactory
from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase, override_config
from entities.cms.factories import CampagnePageFactory
from entities.file.factories import FileFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestEditGroupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.admin = AdminFactory()
        self.group = GroupFactory(owner=self.user)
        self.icon = FileFactory(
            owner=self.user,
            upload=self.build_contentfile(
                self.relative_path(__file__, ["..", "assets", "avatar.jpg"])
            ),
        )
        self.profile_field = ProfileField.objects.create(
            key="profile_field",
            name="text_name",
            field_type="select_field",
            is_in_auto_group_membership=True,
            field_options=["foo", "bar", "baz"],
        )
        self.auto_member_profile_apply = mock.patch(
            "core.utils.auto_member_profile_field.AutoMemberProfileFieldAtGroup.apply"
        ).start()

    def tearDown(self):
        super().tearDown()

    def test_edit_group_anon(self):
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        name
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "name": "test",
            }
        }

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(mutation, variables)

    def test_edit_group(self):
        new_start_page = CampagnePageFactory(owner=self.group.owner, group=self.group)

        file_mock = FileFactory(owner=self.user, upload=ContentFile(b"", "icon.png"))

        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        name
                        icon { embed }
                        excerpt
                        richDescription
                        introduction
                        isIntroductionPublic
                        welcomeMessage
                        isClosed
                        isHidden
                        isMenuAlwaysVisible
                        isMembershipOnRequest
                        isFeatured
                        isChatEnabled
                        autoNotification
                        fileNotification
                        tags
                        defaultTags
                        defaultTagCategories {
                            name
                            values
                        }
                        isLeavingGroupDisabled
                        isAutoMembershipEnabled
                        startPage { guid, title }
                        autoMembershipFields { field { key }, value }
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "name": "Name",
                "iconGuid": file_mock.guid,
                "richDescription": self.tiptap_paragraph("richDescription"),
                "introduction": "introdcution",
                "isIntroductionPublic": True,
                "welcomeMessage": "welcomeMessage",
                "isClosed": True,
                "isMembershipOnRequest": True,
                "isFeatured": True,
                "isMenuAlwaysVisible": False,
                "isChatEnabled": True,
                "autoNotification": True,
                "fileNotification": True,
                "isLeavingGroupDisabled": True,
                "isAutoMembershipEnabled": True,
                "defaultTags": ["tag_one", "tag_three"],
                "defaultTagCategories": [{"name": "Test", "values": ["One", "Two"]}],
                "startPageGuid": new_start_page.guid,
                "autoMembershipFields": [
                    {"guid": self.profile_field.guid, "value": ["foo"]}
                ],
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(mutation, variables)

        data = result["data"]
        self.assertEqual(data["editGroup"]["group"]["guid"], variables["group"]["guid"])
        self.assertEqual(data["editGroup"]["group"]["name"], variables["group"]["name"])
        self.assertIn("/icon.png", data["editGroup"]["group"]["icon"]["embed"])
        self.assertEqual(
            data["editGroup"]["group"]["richDescription"],
            variables["group"]["richDescription"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["introduction"],
            variables["group"]["introduction"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["isIntroductionPublic"],
            variables["group"]["isIntroductionPublic"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["welcomeMessage"],
            variables["group"]["welcomeMessage"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["isClosed"], variables["group"]["isClosed"]
        )
        self.assertEqual(
            data["editGroup"]["group"]["isMembershipOnRequest"],
            variables["group"]["isMembershipOnRequest"],
        )
        self.assertEqual(data["editGroup"]["group"]["isFeatured"], False)
        self.assertEqual(data["editGroup"]["group"]["isMenuAlwaysVisible"], False)
        self.assertEqual(data["editGroup"]["group"]["isLeavingGroupDisabled"], False)
        self.assertEqual(data["editGroup"]["group"]["isAutoMembershipEnabled"], False)
        self.assertEqual(
            data["editGroup"]["group"]["isChatEnabled"],
            variables["group"]["isChatEnabled"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["autoNotification"],
            variables["group"]["autoNotification"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["fileNotification"],
            variables["group"]["fileNotification"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["defaultTags"], variables["group"]["defaultTags"]
        )
        self.assertEqual(
            data["editGroup"]["group"]["defaultTagCategories"],
            variables["group"]["defaultTagCategories"],
        )
        self.assertEqual(
            data["editGroup"]["group"]["startPage"],
            {
                "guid": new_start_page.guid,
                "title": new_start_page.title,
            },
        )
        self.assertEqual(
            data["editGroup"]["group"]["autoMembershipFields"],
            [
                {"field": {"key": self.profile_field.key}, "value": ["foo"]},
            ],
        )
        self.assertTrue(self.auto_member_profile_apply.called)

    def test_edit_group_member_fields_invalid_id(self):
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        showMemberProfileFields {
                            guid
                            name
                        }
                    }
                }
            }
        """
        variables = {
            "group": {"guid": self.group.guid, "showMemberProfileFieldGuids": ["123"]}
        }

        with self.assertGraphQlError("‘123’ is geen geldige UUID."):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(mutation, variables)

    def _build_profile_fields(self):
        profile_field1 = ProfileField.objects.create(
            key="text_key", name="text_name", field_type="text_field"
        )
        profile_field2 = ProfileField.objects.create(
            key="text_key2", name="text_name2", field_type="text_field"
        )
        return [profile_field1, profile_field2]

    def test_edit_group_member_fields(self):
        profile_field1, profile_field2 = self._build_profile_fields()
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        showMemberProfileFields {
                            guid
                            name
                        }
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "showMemberProfileFieldGuids": [
                    profile_field1.guid,
                    profile_field2.guid,
                ],
            }
        }

        with override_config(
            PROFILE_SECTIONS=[
                {
                    "name": "section_one",
                    "profileFieldGuids": [profile_field1.guid, profile_field2.guid],
                }
            ]
        ):
            self.graphql_client.force_login(self.user)
            result = self.graphql_client.post(mutation, variables)

            data = result["data"]
            self.assertEqual(
                data["editGroup"]["group"]["guid"], variables["group"]["guid"]
            )
            self.assertEqual(
                len(data["editGroup"]["group"]["showMemberProfileFields"]), 2
            )
            self.assertEqual(
                data["editGroup"]["group"]["showMemberProfileFields"][0]["guid"],
                profile_field1.guid,
            )
            self.assertEqual(
                data["editGroup"]["group"]["showMemberProfileFields"][1]["guid"],
                profile_field2.guid,
            )

            variables = {
                "group": {
                    "guid": self.group.guid,
                    "showMemberProfileFieldGuids": [profile_field2.guid],
                }
            }

            result = self.graphql_client.post(mutation, variables)

            data = result["data"]
            self.assertEqual(
                data["editGroup"]["group"]["guid"], variables["group"]["guid"]
            )
            self.assertEqual(
                len(data["editGroup"]["group"]["showMemberProfileFields"]), 1
            )
            self.assertEqual(
                data["editGroup"]["group"]["showMemberProfileFields"][0]["guid"],
                profile_field2.guid,
            )

    def test_group_can_be_hidden_with_site_admin_perms(self):
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        isHidden
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "isHidden": True,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(mutation, variables)

        data = result["data"]
        # Expect is_hidden is set to True like requested
        self.assertEqual(
            data["editGroup"]["group"]["isHidden"], variables["group"]["isHidden"]
        )

    def test_group_cannot_be_hidden_without_site_admin_perms(self):
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        isHidden
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "isHidden": True,
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(mutation, variables)

        data = result["data"]
        # Expect is_hidden is not set to True like requested
        self.assertFalse(data["editGroup"]["group"]["isHidden"])

    def test_edit_required_profile_fields(self):
        profile_field1, profile_field2 = self._build_profile_fields()
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        requiredProfileFields {
                            guid
                            name
                        }
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "requiredProfileFieldGuids": [profile_field1.guid],
            }
        }

        with override_config(
            PROFILE_SECTIONS=[
                {
                    "name": "section_one",
                    "profileFieldGuids": [profile_field1.guid, profile_field2.guid],
                }
            ]
        ):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(mutation, variables)

            from core.models.group import GroupProfileFieldSetting

            required_fields = [
                obj.profile_field.guid
                for obj in GroupProfileFieldSetting.objects.filter(
                    is_required=True, group=self.group
                )
            ]
            self.assertEqual(
                len(required_fields),
                1,
                msg="We expected exactly one result as required GroupProfileFieldSetting",
            )
            self.assertEqual(
                required_fields,
                [profile_field1.guid],
                msg="We expected the first profile field as required GroupProfileFieldSetting",
            )

    def test_edit_required_profile_fields_help_message(self):
        EXPECTED_MESSAGE = "I'd expect it to look like this"
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        requiredProfileFieldsMessage
                    }
                }
            }
        """
        variables = {
            "group": {
                "guid": self.group.guid,
                "requiredProfileFieldsMessage": EXPECTED_MESSAGE,
            }
        }

        self.graphql_client.force_login(self.user)
        self.graphql_client.post(mutation, variables)
        self.group.refresh_from_db()

        self.assertEqual(
            self.group.required_fields_message,
            EXPECTED_MESSAGE,
            msg="De inhoud van required_fields_message wordt niet goed geupdate.",
        )

    def test_delete_icon(self):
        self.group.icon = self.icon
        self.group.save()
        mutation = """
            mutation ($group: editGroupInput!) {
                editGroup(input: $group) {
                    group {
                        guid
                        requiredProfileFieldsMessage
                    }
                }
            }
        """
        variables = {"group": {"guid": self.group.guid, "iconGuid": None}}
        self.graphql_client.force_login(self.user)
        self.graphql_client.post(mutation, variables)
        self.group.refresh_from_db()

        self.assertIsNone(self.group.icon)

    def test_update_menu_and_plugins(self):
        member_page = CampagnePageFactory(owner=self.group.owner, group=self.group)
        mutation = """
        mutation($input: editGroupInput!) {
            editGroup(input: $input) {
                group {
                    plugins
                    menu {
                        ...GroupMenuFragment
                    }
                }
            }
        }
        fragment GroupMenuFragment on GroupMenuItem {
            type
            ... on GroupMenuPluginItem {
                plugin
                label
            }
            ... on GroupMenuPageItem {
                page { guid }
                label
            }
        }
        """
        variables = {
            "input": {
                "guid": self.group.guid,
                "plugins": ["blog"],
                "menu": [
                    {"type": "plugin", "id": "blog", "label": "ignored"},
                    {"type": "plugin", "id": "members", "label": "ignored"},
                    {"type": "page", "id": member_page.guid, "label": "Foo"},
                ],
            }
        }

        self.graphql_client.force_login(self.group.owner)
        result = self.graphql_client.post(mutation, variables)

        self.assertEqual(
            result["data"]["editGroup"]["group"],
            {
                "plugins": ["blog"],
                "menu": [
                    {"type": "plugin", "plugin": "blog", "label": "Blogs"},
                    {"type": "plugin", "plugin": "members", "label": "Leden"},
                    {
                        "type": "page",
                        "page": {"guid": member_page.guid},
                        "label": "Foo",
                    },
                ],
            },
        )

    def test_set_invalid_start_page(self):
        """
        Test that the result contains an error if an invalid start page is given
        """
        invalid_start_page = CampagnePageFactory(owner=EditorFactory())

        mutation = """
        mutation($input: editGroupInput!) {
            editGroup(input: $input) {
                group {
                    startPage { guid, title }
                }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.group.guid,
                "startPageGuid": invalid_start_page.guid,
            }
        }

        with self.assertGraphQlError(constances.COULD_NOT_FIND_START_PAGE):
            self.graphql_client.force_login(self.group.owner)
            self.graphql_client.post(mutation, variables)

    def test_update_submenu(self):
        member_page = CampagnePageFactory(owner=self.group.owner, group=self.group)
        mutation = """
        mutation($input: editGroupInput!) {
            editGroup(input: $input) {
                group {
                    menu {
                        ...GroupMenuFragment
                    }
                }
            }
        }
        fragment GroupMenuFragment on GroupMenuItem {
            type
            ... on GroupMenuPluginItem {
                plugin
                label
            }
            ... on GroupMenuPageItem {
                page { guid }
                label
            }
            ... on GroupMenuLinkItem {
                link
                label
            }
            ... on GroupSubmenuItem {
                label
                submenu {
                    type
                    ... on GroupMenuPluginItem {
                        plugin
                        label
                    }
                    ... on GroupMenuPageItem {
                        page { guid }
                        label
                    }
                    ... on GroupMenuLinkItem {
                        link
                        label
                    }
                }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.group.guid,
                "menu": [
                    {"type": "plugin", "id": "blog", "label": "ignored"},
                    {"type": "plugin", "id": "members", "label": "ignored"},
                    {"type": "page", "id": member_page.guid, "label": "Foo"},
                    {"type": "link", "id": "https://example.com", "label": "Example"},
                    {
                        "type": "submenu",
                        "label": "Submenu",
                        "submenu": [
                            {"type": "plugin", "id": "blog", "label": "ignored"},
                            {"type": "plugin", "id": "members", "label": "ignored"},
                            {"type": "page", "id": member_page.guid, "label": "Foo"},
                            {
                                "type": "link",
                                "id": "https://example.com",
                                "label": "Example",
                            },
                        ],
                    },
                ],
            }
        }

        self.graphql_client.force_login(self.group.owner)
        result = self.graphql_client.post(mutation, variables)
        self.assertEqual(
            result["data"]["editGroup"]["group"],
            {
                "menu": [
                    {"type": "plugin", "plugin": "blog", "label": "Blogs"},
                    {"type": "plugin", "plugin": "members", "label": "Leden"},
                    {
                        "type": "page",
                        "page": {"guid": member_page.guid},
                        "label": "Foo",
                    },
                    {"type": "link", "link": "https://example.com", "label": "Example"},
                    {
                        "type": "submenu",
                        "submenu": [
                            {"type": "plugin", "plugin": "blog", "label": "Blogs"},
                            {"type": "plugin", "plugin": "members", "label": "Leden"},
                            {
                                "type": "page",
                                "page": {"guid": member_page.guid},
                                "label": "Foo",
                            },
                            {
                                "type": "link",
                                "link": "https://example.com",
                                "label": "Example",
                            },
                        ],
                        "label": "Submenu",
                    },
                ],
            },
        )

    def test_update_submenu_errors(self):
        member_page = CampagnePageFactory(owner=self.group.owner, group=self.group)
        global_page = CampagnePageFactory(owner=EditorFactory())
        mutation = """
        mutation($input: editGroupInput!) {
            editGroup(input: $input) {
                group {
                    menu {
                        ...GroupMenuFragment
                    }
                }
            }
        }
        fragment GroupMenuFragment on GroupMenuItem {
            type
            ... on GroupMenuPluginItem {
                plugin
                label
            }
            ... on GroupMenuPageItem {
                page { guid }
                label
            }
            ... on GroupMenuLinkItem {
                link
                label
            }
            ... on GroupSubmenuItem {
                label
                submenu {
                    type
                    ... on GroupMenuPluginItem {
                        plugin
                        label
                    }
                    ... on GroupMenuPageItem {
                        page { guid }
                        label
                    }
                    ... on GroupMenuLinkItem {
                        link
                        label
                    }
                }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.group.guid,
                "menu": [
                    {"type": "plugin", "id": "sdsd", "label": "ignored"},
                ],
            }
        }

        self.graphql_client.force_login(self.group.owner)
        with self.assertGraphQlError(constances.GROUP_MENU_INVALID_PLUGIN):
            self.graphql_client.post(mutation, variables)

        variables = {
            "input": {
                "guid": self.group.guid,
                "menu": [
                    {"type": "plugin", "id": ""},
                ],
            }
        }

        self.graphql_client.force_login(self.group.owner)
        with self.assertGraphQlError(constances.GROUP_MENU_MISSING_ID):
            self.graphql_client.post(mutation, variables)

        variables = {
            "input": {
                "guid": self.group.guid,
                "menu": [
                    {"type": "link", "id": "udisuds", "label": "linkje"},
                ],
            }
        }

        self.graphql_client.force_login(self.group.owner)
        with self.assertGraphQlError(constances.GROUP_MENU_INVALID_LINK):
            self.graphql_client.post(mutation, variables)

        variables = {
            "input": {
                "guid": self.group.guid,
                "plugins": ["blog"],
                "menu": [
                    {"type": "plugin", "id": "blog"},
                    {"type": "plugin", "id": "members"},
                    {"type": "page", "id": member_page.guid},
                    {
                        "type": "page",
                        "id": global_page.guid,
                    },  # invalid page
                ],
            }
        }

        # test with invalid page
        self.graphql_client.force_login(self.group.owner)
        with self.assertGraphQlError("group_menu_invalid_page"):
            self.graphql_client.post(mutation, variables)
