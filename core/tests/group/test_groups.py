from django.core.files.base import ContentFile
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.cms.factories import CampagnePageFactory
from entities.file.factories import FileFactory
from user.factories import AdminFactory, UserFactory
from user.models import User


class GroupsEmptyTestCase(PleioTenantTestCase):
    def test_groups_empty(self):
        query = """
            {
                groups {
                    total
                    edges {
                        guid
                        name
                    }
                }
            }
        """

        result = self.graphql_client.post(query, {})

        data = result["data"]

        self.assertEqual(data["groups"]["total"], 0)
        self.assertEqual(data["groups"]["edges"], [])


class GroupsNotEmptyTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=False)
        self.user = mixer.blend(User)
        self.group1 = mixer.blend(Group, name="Group 1")
        self.group1.join(self.user, "member")
        self.groups = mixer.cycle(5).blend(Group, is_closed=False)
        self.group2 = mixer.blend(Group, is_featured=True, name="Group 2")
        self.query = """
            query GroupsQuery(
                    $filter: GroupFilter
                    $offset: Int
                    $limit: Int
                    $q: String
                    $userGuid: String) {
                groups(
                        filter: $filter
                        offset: $offset
                        limit: $limit
                        q: $q
                        userGuid: $userGuid) {
                    total
                    edges {
                        guid
                        name
                    }
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_groups_default(self):
        result = self.graphql_client.post(self.query, {})

        data = result["data"]

        self.assertEqual(data["groups"]["total"], 7)
        self.assertEqual(data["groups"]["edges"][0]["guid"], self.group2.guid)

    def test_groups_limit(self):
        result = self.graphql_client.post(self.query, {"limit": 1})

        data = result["data"]

        self.assertEqual(data["groups"]["total"], 7)

    def test_groups_mine(self):
        variables = {"filter": "mine", "offset": 0, "limit": 20, "q": ""}
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]

        self.assertEqual(data["groups"]["total"], 1)
        self.assertEqual(data["groups"]["edges"][0]["guid"], self.group1.guid)

    def test_groups_user_filter(self):
        variables = {"offset": 0, "limit": 20, "q": "", "userGuid": self.user.guid}
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]

        self.assertEqual(data["groups"]["total"], 1)
        self.assertEqual(data["groups"]["edges"][0]["guid"], self.group1.guid)


class HiddenGroupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticated_user = mixer.blend(User, name="yy")
        self.group_member_user = mixer.blend(User, name="xx")
        self.non_member_user = mixer.blend(User, name="yyy")
        self.group = mixer.blend(
            Group,
            owner=self.authenticated_user,
            introduction="introductionMessage",
            is_hidden=True,
        )
        self.group.join(self.group_member_user, "member")
        self.admin = AdminFactory()

    def test_hidden_group_is_hidden_for_non_members(self):
        query = """
            query GroupsQuery {
                groups {
                    total
               }
            }
        """

        self.graphql_client.force_login(self.non_member_user)
        result = self.graphql_client.post(query, {})

        data = result["data"]
        self.assertEqual(data["groups"]["total"], 0)

    def test_hidden_group_is_visible_for_members(self):
        query = """
            query GroupsQuery {
                groups {
                    total
                    edges {
                        guid
                    }
               }
            }
        """

        self.graphql_client.force_login(self.group_member_user)
        result = self.graphql_client.post(query, {})

        data = result["data"]
        self.assertEqual(data["groups"]["total"], 1)
        self.assertEqual(data["groups"]["edges"][0]["guid"], self.group.guid)

    def test_hidden_group_is_hidden_for_anonymous_users(self):
        query = """
            query GroupsQuery {
                groups {
                    total
               }
            }
        """
        result = self.graphql_client.post(query, {})

        data = result["data"]
        self.assertEqual(data["groups"]["total"], 0)

    def test_hidden_group_is_visible_for_admin_users(self):
        query = """
            query GroupsQuery {
                groups {
                    total
                    edges {
                        guid
                    }
               }
            }
        """
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query, {})

        data = result["data"]
        self.assertEqual(data["groups"]["total"], 1)
        self.assertEqual(data["groups"]["edges"][0]["guid"], self.group.guid)


class TestGroupDiskSize(PleioTenantTestCase):
    ONE_CONTENT = b"1"
    TWO_CONTENT = b"22"
    THREE_CONTENT = b"333"
    FIVE_CONTENT = b"55555"
    SEVEN_CONTENT = b"7777777"
    ELEVEN_CONTENT = b"11111111111"

    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group1 = GroupFactory(owner=self.owner)

        self.file1 = FileFactory(
            owner=self.owner,
            upload=ContentFile(self.ONE_CONTENT, "Test1.txt"),
            group=self.group1,
        )
        self.file2 = FileFactory(
            owner=self.owner,
            upload=ContentFile(self.TWO_CONTENT, "Test2.txt"),
            group=self.group1,
        )
        self.file3 = FileFactory(
            owner=self.owner, upload=ContentFile(self.THREE_CONTENT, "Test3.txt")
        )

        self.blog1 = BlogFactory(
            owner=self.owner,
            group=self.group1,
            rich_description=self.tiptap_attachment(self.file3),
        )

        self.file4 = FileFactory(
            owner=self.owner, upload=ContentFile(self.ELEVEN_CONTENT, "Test6.txt")
        )

        self.group2 = GroupFactory(owner=self.owner, featured_image=self.file4)

        self.file5 = FileFactory(
            owner=self.owner,
            upload=ContentFile(self.FIVE_CONTENT, "Test4.txt"),
            group=self.group2,
        )
        self.file6 = FileFactory(
            owner=self.owner,
            upload=ContentFile(self.SEVEN_CONTENT, "Test5.txt"),
            group=self.group2,
        )

        self.group3 = GroupFactory(owner=self.owner)

    def tearDown(self):
        super().tearDown()

    def test_disk_size(self):
        self.assertEqual(self.group1.disk_size(), 6)
        self.assertEqual(self.group2.disk_size(), 23)
        self.assertEqual(self.group3.disk_size(), 0)


class TestGroupMenuAccess(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=False)

        self.group_owner = UserFactory()
        self.group = GroupFactory(owner=self.group_owner)

        self.public_page = CampagnePageFactory(
            owner=self.group_owner, group=self.group, read_access=[ACCESS_TYPE.public]
        )
        self.protected_page = CampagnePageFactory(
            owner=self.group_owner,
            group=self.group,
            read_access=[ACCESS_TYPE.logged_in],
        )
        self.group_only_page = CampagnePageFactory(
            owner=self.group_owner,
            group=self.group,
            read_access=[ACCESS_TYPE.group.format(self.group.guid)],
        )

        self.group_menu = [
            {
                "type": "plugin",
                "id": self.public_page.guid,
            },
            {
                "type": "plugin",
                "id": self.protected_page.guid,
                "access": "loggedIn",
            },
            {
                "type": "plugin",
                "id": self.group_only_page.guid,
                "access": "members",
            },
        ]

        self.group.menu = self.group_menu
        self.group.save()

        self.query = """
        query GetGroup($groupGuid: String!) {
            entity(guid: $groupGuid) {
                ... on Group {
                    guid
                    menu {
                        type
                        id
                        access
                    }
                }
            }
        }
        """

        self.variables = {
            "groupGuid": self.group.guid,
        }

    def test_access_menu_as_anonymous(self):
        """
        Anonymous users should only see public pages
        """
        response = self.graphql_client.post(self.query, self.variables)
        group_data = response["data"]["entity"]

        self.assertEqual(
            group_data["menu"],
            [
                {
                    "type": "plugin",
                    "id": self.public_page.guid,
                    "access": "public",
                }
            ],
        )

    def test_access_menu_as_logged_in(self):
        self.graphql_client.force_login(UserFactory())
        response = self.graphql_client.post(self.query, self.variables)
        group_data = response["data"]["entity"]

        self.assertEqual(
            group_data["menu"],
            [
                {
                    "type": "plugin",
                    "id": self.public_page.guid,
                    "access": "public",
                },
                {
                    "type": "plugin",
                    "id": self.protected_page.guid,
                    "access": "loggedIn",
                },
            ],
        )

    def assertFullAccessToMenu(self, user):
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.query, self.variables)
        group_data = response["data"]["entity"]

        self.assertEqual(
            group_data["menu"],
            [
                {
                    "type": "plugin",
                    "id": self.public_page.guid,
                    "access": "public",
                },
                {
                    "type": "plugin",
                    "id": self.protected_page.guid,
                    "access": "loggedIn",
                },
                {
                    "type": "plugin",
                    "id": self.group_only_page.guid,
                    "access": "members",
                },
            ],
        )

    def test_access_menu_as_member(self):
        member = UserFactory()
        self.group.join(member)

        self.assertFullAccessToMenu(member)

    def test_access_menu_as_administrator(self):
        self.assertFullAccessToMenu(AdminFactory())
