from unittest import mock

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class ChangeGroupRoleTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = self.remember(UserFactory(email="user1@example.com"))
        self.user2 = self.remember(UserFactory(email="user2@example.com"))
        self.user3 = self.remember(UserFactory(email="user3@example.com"))
        self.group_admin = self.remember(UserFactory(email="group_admin@example.com"))
        self.admin = self.remember(AdminFactory(email="admin@example.com"))
        self.group1 = self.remember(GroupFactory(name="Group1", owner=self.user1))
        self.group1.join(self.user2, "member")
        self.group1.join(self.group_admin, "admin")

        self.mutation = """
            mutation MemberItem($input: changeGroupOwnerInput!) {
                changeGroupOwner(input: $input) {
                    group {
                        ... on Group {
                            guid
                        }
                    }
                }
            }
        """

    @mock.patch("core.models.group.Group.change_ownership")
    def test_change_group_owner_by_group_owner(self, change_ownership):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]["changeGroupOwner"]

        self.assertEqual(data["group"]["guid"], self.group1.guid)
        self.assertEqual(change_ownership.call_count, 1)
        self.assertEqual(change_ownership.call_args.args, (self.user2, self.user1))

    @mock.patch("core.models.group.Group.change_ownership")
    def test_change_group_owner_by_admin(self, change_ownership):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]["changeGroupOwner"]

        self.assertEqual(data["group"]["guid"], self.group1.guid)
        self.assertEqual(change_ownership.call_count, 1)
        self.assertEqual(change_ownership.call_args.args, (self.user2, self.admin))

    @mock.patch("core.models.group.Group.change_ownership")
    def test_change_group_owner_by_group_admin(self, change_ownership):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        self.graphql_client.force_login(self.group_admin)
        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]["changeGroupOwner"]

        self.assertEqual(data["group"]["guid"], self.group1.guid)
        self.assertEqual(change_ownership.call_count, 1)
        self.assertEqual(
            change_ownership.call_args.args, (self.user2, self.group_admin)
        )

    def test_change_group_owner_by_other_user(self):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.user3)
            self.graphql_client.post(self.mutation, variables)

    def test_change_group_owner_by_anonymous_user(self):
        variables = {"input": {"guid": self.group1.guid, "userGuid": self.user2.guid}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)
