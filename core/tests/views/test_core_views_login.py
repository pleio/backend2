from core.tests.helpers import PleioTenantTestCase


class TestCoreViewsLoginTestCase(PleioTenantTestCase):
    def test_login(self):
        response = self.client.get("/login")

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/oidc/authenticate/?provider=pleio")

    def test_login_with_idp(self):
        self.override_config(
            IDP_ID="fnv",
            IDP_NAME="Mijn FNV",
        )
        response = self.client.get("/login")

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/oidc/authenticate/?idp=fnv")

    def test_login_plural_providers(self):
        self.override_config(OIDC_PROVIDERS=[0, 1])
        response = self.client.get("/login")

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")

    def test_login_with_parameters(self):
        response = self.client.get("/login?next=/test&method=register")

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            "/oidc/authenticate/?provider=pleio&next=%2Ftest&method=register",
        )
