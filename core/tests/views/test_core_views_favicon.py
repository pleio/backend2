import os
from typing import TYPE_CHECKING
from unittest import mock

from django.conf import settings
from django.http import HttpRequest

from core.tests.helpers import PleioTenantTestCase, override_config
from core.views import favicon

if TYPE_CHECKING:
    from entities.file.models import FileFolder


class TestCoreViewsFaviconTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.request = HttpRequest()
        path = os.path.join(os.path.dirname(__file__), "..", "assets", "favicon.ico")
        self.file: FileFolder = self.file_factory(path)

    def test_default_favicon(self):
        response = favicon(self.request)

        self.assertEqual(response.status_code, 200)
        file_path = os.path.join(settings.STATIC_ROOT, "favicon-192.png")
        self.assertTrue(response.streaming)
        # compare response dta with file data
        with open(file_path, "rb") as f:
            self.assertEqual(b"".join(response.streaming_content), f.read())

    @mock.patch("entities.file.models.FileFolder.objects.file_by_path")
    def test_custom_favicon(self, mock_file_by_path):
        with override_config(FAVICON=self.file.download_url):
            response = favicon(self.request)

            # test if mock_file_by_path was called
            mock_file_by_path.assert_called_once_with(self.file.download_url)

            self.assertEqual(response.status_code, 200)
