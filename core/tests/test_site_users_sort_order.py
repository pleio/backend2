from django.utils import timezone

from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestSiteUsersSortOrderTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.authenticatedUser = UserFactory(
            name="user", email="user@test.nl", created_at=timezone.now()
        )
        self.authenticatedUser.profile.last_online = (
            timezone.now() - timezone.timedelta(days=4)
        )
        self.authenticatedUser.profile.save()
        self.admin = AdminFactory(
            name="admin",
            email="admin@test.nl",
            created_at=timezone.now() - timezone.timedelta(days=1),
            schedule_ban_at=timezone.now() + timezone.timedelta(days=15),
        )
        self.admin.profile.last_online = timezone.now() - timezone.timedelta(days=7)
        self.admin.profile.save()

        self.user1 = UserFactory(
            name="user1",
            email="user1@test.nl",
            schedule_ban_at=timezone.now() + timezone.timedelta(days=10),
            created_at=timezone.now() - timezone.timedelta(days=100),
        )
        self.user1.profile.last_online = timezone.now() - timezone.timedelta(days=3)
        self.user1.profile.save()
        self.user2 = UserFactory(
            name="user2",
            email="user2@test.nl",
            schedule_ban_at=timezone.now() + timezone.timedelta(days=11),
            created_at=timezone.now() - timezone.timedelta(days=50),
        )
        self.user3 = UserFactory(
            name="user3",
            email="user3@test.nl",
            schedule_ban_at=timezone.now() + timezone.timedelta(days=12),
            created_at=timezone.now() - timezone.timedelta(days=75),
        )
        self.user3.profile.last_online = timezone.now() - timezone.timedelta(days=5)
        self.user3.profile.save()
        self.invisible_superadmin = UserFactory(is_active=True, is_superadmin=True)

        self.banned_user1 = UserFactory(
            name="banned_user1",
            email="banned_user1@test.nl",
            created_at=timezone.now() - timezone.timedelta(days=100),
            banned_at=timezone.now() - timezone.timedelta(days=10),
            ban_reason="C",
            is_active=False,
        )
        self.banned_user1.profile.last_online = timezone.now() - timezone.timedelta(
            days=3
        )
        self.banned_user1.profile.save()
        self.banned_user2 = UserFactory(
            name="banned_user2",
            email="banned_user2@test.nl",
            created_at=timezone.now() - timezone.timedelta(days=50),
            banned_at=timezone.now() - timezone.timedelta(days=15),
            ban_reason="A",
            is_active=False,
        )
        self.banned_user3 = UserFactory(
            name="banned_user3",
            email="banned_user3@test.nl",
            created_at=timezone.now() - timezone.timedelta(days=75),
            banned_at=timezone.now() - timezone.timedelta(days=2),
            ban_reason="B",
            is_active=False,
        )
        self.banned_user3.profile.last_online = timezone.now() - timezone.timedelta(
            days=5
        )
        self.banned_user3.profile.save()

        self.query = """
        query UsersInOrderOfCreatedAt($field: SiteUsersOrderBy, $dir: OrderDirection, $isBanned: Boolean) {
            siteUsers(orderBy: $field, orderDirection: $dir, isBanned: $isBanned) {
                edges {
                    name
                    memberSince
                }
            }
        }
        """

    def test_authenticated_user_has_access(self):
        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(
            self.query, {"field": "memberSince", "dir": "asc"}
        )

        users = [u["name"] for u in result["data"]["siteUsers"]["edges"]]
        self.assertEqual(users, ["user1", "user3", "user2", "admin", "user"])

    def test_query_site_users_by_created_at_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "memberSince", "dir": "asc"}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["user1", "user3", "user2", "admin", "user"],
        )

    def test_query_site_users_by_created_at_order_desc(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "memberSince", "dir": "desc"}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["user", "admin", "user2", "user3", "user1"],
        )

    def test_query_site_users_by_name_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"field": "name", "dir": "asc"})
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["admin", "user", "user1", "user2", "user3"],
        )

    def test_query_site_users_by_name_order_desc(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"field": "name", "dir": "desc"})
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["user3", "user2", "user1", "user", "admin"],
        )

    def test_query_site_users_by_last_online_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "lastOnline", "dir": "asc"}
        )

        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["admin", "user3", "user", "user1", "user2"],
        )

    def test_query_site_users_by_ban_scheduled_at_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "banScheduledAt", "dir": "asc"}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["user1", "user2", "user3", "admin", "user"],
        )

    def test_query_site_users_by_last_online_order_desc(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "lastOnline", "dir": "desc"}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["user2", "user1", "user", "user3", "admin"],
        )

    def test_query_site_users_by_email_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"field": "email", "dir": "asc"})
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["admin", "user1", "user2", "user3", "user"],
        )

    def test_query_site_users_by_email_order_desc(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"field": "email", "dir": "desc"})
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["user", "user3", "user2", "user1", "admin"],
        )

    def test_query_site_users_by_ban_reason_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "banReason", "dir": "asc", "isBanned": True}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["banned_user2", "banned_user3", "banned_user1"],
        )

    def test_query_site_users_by_ban_reason_order_desc(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "banReason", "dir": "desc", "isBanned": True}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["banned_user1", "banned_user3", "banned_user2"],
        )

    def test_query_site_users_by_banned_since_order(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "bannedSince", "dir": "asc", "isBanned": True}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["banned_user2", "banned_user1", "banned_user3"],
        )

    def test_query_site_users_by_banned_since_order_desc(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            self.query, {"field": "bannedSince", "dir": "desc", "isBanned": True}
        )
        self.assertEqual(
            [u["name"] for u in result["data"]["siteUsers"]["edges"]],
            ["banned_user3", "banned_user1", "banned_user2"],
        )
