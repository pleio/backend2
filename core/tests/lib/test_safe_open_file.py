import os

from core.lib import safe_open_file
from core.tests.helpers import PleioTenantTestCase


class TestSafeOpenFileTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.TEST_DIR = os.path.dirname(__file__)
        self.SUB_DIR = os.path.basename(self.TEST_DIR)
        self.TEST_FILE = os.path.basename(__file__)

    def test_improperly_located_file(self):
        try:
            with safe_open_file(self.TEST_DIR, "..", self.TEST_FILE, mode="r"):
                pass
            self.fail("Unexpectedly didn't raise an error")
        except ValueError as e:
            self.assertEqual(str(e), "Invalid file path")

    def test_properly_improperly_located_file(self):
        with safe_open_file(
            self.TEST_DIR, "..", self.SUB_DIR, self.TEST_FILE, mode="r"
        ) as f:
            assert f is not None

    def test_properly_located_file(self):
        with safe_open_file(self.TEST_DIR, self.TEST_FILE, mode="r") as f:
            assert f is not None
