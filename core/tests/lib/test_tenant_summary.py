import os
import uuid
from unittest.mock import patch

from core.lib import tenant_summary
from core.tests.helpers import PleioTenantTestCase


class TestTenantSummary(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_setting(ENV="unit-test")

    def test_without_favicon(self):
        self.override_config(
            TENANT_API_TOKEN="test",
            DESCRIPTION="test site description.",
            NAME="test site name",
        )
        self.assertDictEqual(
            tenant_summary(),
            {
                "api_token": "test",
                "description": "test site description.",
                "name": "test site name",
                "url": "https://%s" % self.tenant.primary_domain,
            },
        )

        self.assertDictEqual(
            tenant_summary(with_favicon=True),
            {
                "api_token": "test",
                "description": "test site description.",
                "name": "test site name",
                "url": "https://%s" % self.tenant.primary_domain,
            },
        )

    @patch("base64.encodebytes")
    def test_with_favicon(self, mocked_encode_string):
        mocked_encode_string.return_value = b"file contents"
        path = os.path.join(os.path.dirname(__file__), "..", "assets", "favicon.ico")
        file = self.file_factory(path)
        self.override_config(
            TENANT_API_TOKEN="test",
            DESCRIPTION="test site description.",
            NAME="test site name",
            FAVICON=file.download_url,
        )

        summary = tenant_summary()
        self.assertFalse(summary.get("favicon"))
        self.assertFalse(summary.get("favicon_data"))

        summary = tenant_summary(with_favicon=True)
        self.assertEqual("favicon.ico", summary.get("favicon"))
        self.assertEqual("file contents", summary.get("favicon_data"))

    def test_with_favicon_error(self):
        self.override_config(
            TENANT_API_TOKEN="test",
            DESCRIPTION="test site description.",
            NAME="test site name",
            FAVICON=uuid.uuid4(),
        )
        self.assertDictEqual(
            tenant_summary(with_favicon=True),
            {
                "api_token": "test",
                "description": "test site description.",
                "name": "test site name",
                "url": "https://%s" % self.tenant.primary_domain,
            },
        )
