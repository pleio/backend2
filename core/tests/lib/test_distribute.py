import uuid

from core.lib import Distribute
from core.tests.helpers import PleioTenantTestCase


class TestLibDistributeTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.distribution = []
        for _n in range(0, 100):
            self.distribution.append(uuid.uuid4())

        # Copy the distribution.
        self.expected_content = [*self.distribution]

    def test_distribute(self):
        mixer = Distribute(self.distribution, distance=10)
        actual_content = [*mixer.spread()]

        self.assertEqual({*actual_content}, {*self.expected_content})
        self.assertNotEqual(actual_content, self.expected_content)
