from core.lib import CspHeaderExceptionConfig
from core.tests.helpers import PleioTenantTestCase


class TestLibCspHeaderConfig(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            CSP_HEADER_EXCEPTIONS=[
                {"url": "https://img.example.com", "types": ["img-src"]},
                {"url": "https://frame.example.com", "types": ["frame-src"]},
                {
                    "url": "https://img-frame.example.com",
                    "types": ["frame-src", "img-src"],
                },
                {
                    "url": "https://img-invalid.example.com",
                    "types": ["img-src", "invalid-src"],
                },
                {"url": "http://img.insecure-example.com", "types": ["img-src"]},
                {"url": "https://invalid.example.com", "types": ["invalid-src"]},
            ]
        )

    def test_legacy_csp_header_config(self):
        self.override_config(CSP_HEADER_EXCEPTIONS=["https://api.example.com"])
        config = CspHeaderExceptionConfig()
        self.assertEqual(
            config.exceptions,
            [{"url": "https://api.example.com", "types": ["img-src", "frame-src"]}],
        )

    def test_as_csp_dict(self):
        config = CspHeaderExceptionConfig()
        self.assertEqual(
            config.as_csp_dict(),
            {
                "img-src": [
                    "https://img.example.com",
                    "https://img-frame.example.com",
                    "https://img-invalid.example.com",
                ],
                "frame-src": [
                    "https://frame.example.com",
                    "https://img-frame.example.com",
                ],
            },
        )

    def test_filter_img_src(self):
        config = CspHeaderExceptionConfig()
        self.assertEqual(
            config.filter("img-src"),
            [
                "https://img.example.com",
                "https://img-frame.example.com",
                "https://img-invalid.example.com",
            ],
        )

    def test_filter_frame_src(self):
        config = CspHeaderExceptionConfig()
        self.assertEqual(
            config.filter("frame-src"),
            [
                "https://frame.example.com",
                "https://img-frame.example.com",
            ],
        )

    def test_filter_invalid_src(self):
        config = CspHeaderExceptionConfig()
        self.assertEqual(config.filter("invalid-src"), [])
