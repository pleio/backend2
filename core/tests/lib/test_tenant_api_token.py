from unittest.mock import patch

from core.lib import tenant_api_token
from core.tests.helpers import PleioTenantTestCase


class TestTenantApiToken(PleioTenantTestCase):
    @patch("core.lib.uuid.uuid4")
    def test_tenant_api_token_if_exists(self, mocked_uuid4):
        self.override_config(TENANT_API_TOKEN="exists")
        mocked_uuid4.return_value = "created-by-uuid4"
        self.assertEqual("exists", tenant_api_token())

    @patch("core.lib.uuid.uuid4")
    def test_tenant_api_token_if_not_exists(self, mocked_uuid4):
        self.override_config(TENANT_API_TOKEN=None)
        mocked_uuid4.return_value = "created-by-uuid4"

        self.assertEqual("created-by-uuid4", tenant_api_token())
