from core.lib import get_file_name_with_original_extension
from core.tests.helpers import PleioTenantTestCase


class TestGetFileNameWithOriginalExtension(PleioTenantTestCase):
    def test_get_file_name_with_original_extension(self):
        result = get_file_name_with_original_extension("test.jpg", "image.png")
        self.assertEqual(result, "test.png")

        result = get_file_name_with_original_extension("test", "image.png")
        self.assertEqual(result, "test.png")

        result = get_file_name_with_original_extension("test", "image")
        self.assertEqual(result, "test")
