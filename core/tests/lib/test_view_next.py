from core.lib import view_next
from core.tests.helpers import PleioTenantTestCase


class TestViewNextTestCase(PleioTenantTestCase):
    def test_view_next(self):
        self.assertEqual(
            view_next("/test", "/next"),
            "/test?next=%2Fnext",
        )
        self.assertEqual(
            view_next("/test?foo=bar", "/next"),
            "/test?foo=bar&next=%2Fnext",
        )
        self.assertEqual(
            view_next("http://www.example.com/test?foo=bar&next=/next", "/next"),
            "http://www.example.com/test?foo=bar&next=%2Fnext",
        )
