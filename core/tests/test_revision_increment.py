from core.models.revision import Revision
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestRevisionIncrementTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory(email="user1@example.com")
        self.blog = BlogFactory(
            owner=self.user1,
            title="Only valid blog post",
            rich_description="json string1",
        )

        self.mutation = """
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    }
                }
            }
        """

        self.variables = {
            "input": {
                "guid": self.blog.guid,
                "title": "My first Event",
                "richDescription": "richDescription",
            }
        }

    def tearDown(self):
        super().tearDown()

    def test_edit_blog_by_user(self):
        self.assertEqual(Revision.objects.all().count(), 0)
        self.graphql_client.force_login(self.user1)
        self.graphql_client.post(self.mutation, self.variables)
        self.assertEqual(Revision.objects.all().count(), 1)
        # post with same data, so nothing will be changed
        self.graphql_client.post(self.mutation, self.variables)
        self.assertEqual(Revision.objects.all().count(), 1)
