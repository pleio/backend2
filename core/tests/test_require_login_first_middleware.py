from urllib.parse import urlencode

from core.lib import add_url_params
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestRequireLoginFirstMiddlewareTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(IS_CLOSED=False)

        self.owner = self.remember(UserFactory())
        self.blog = self.remember(BlogFactory(owner=self.owner))

    def test_unauthenticated_no_redirect(self):
        response = self.client.get(add_url_params(self.blog.url))

        self.assertEqual(response.status_code, 200)

    def test_unauthenticated_redirect(self):
        response = self.client.get(add_url_params(self.blog.url, __auth=True))

        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.startswith("/login?"))
        self.assertIn(urlencode({"next": self.blog.url}), response.url)

    def test_authenticated_redirect(self):
        self.client.force_login(self.owner)
        response = self.client.get(add_url_params(self.blog.url, __auth=True))

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, self.blog.url)
