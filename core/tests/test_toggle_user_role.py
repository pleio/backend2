from unittest import mock

from django.contrib.auth.models import AnonymousUser

from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestToggleUserRoleToAdminTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory()
        self.user2 = AdminFactory()
        self.user_admin = UserFactory(roles=[USER_ROLES.USER_ADMIN])
        self.admin = AdminFactory()

        self.mutation = """
            mutation toggleUserRole($input: toggleUserRoleInput!) {
                toggleUserRole(input: $input) {
                    success
                }
            }
        """
        self.variables = {"input": {"guid": self.user1.guid, "role": "admin"}}

        self.admin_mail_mock = mock.patch(
            "core.resolvers.mutations.mutation_toggle_user_role.schedule_assign_admin_for_admin_mail"
        ).start()

        self.user_mail_mock = mock.patch(
            "core.resolvers.mutations.mutation_toggle_user_role.schedule_assign_admin_for_user_mail"
        ).start()

    def run_query(self, user):
        self.admin_mail_mock.reset_mock()
        self.user_mail_mock.reset_mock()
        self.graphql_client.force_login(user)
        self.graphql_client.post(self.mutation, self.variables)
        self.user1.refresh_from_db()

    def test_toggle_user_role_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.run_query(AnonymousUser())

    def test_toggle_user_role_by_user(self):
        with self.assertGraphQlError("not_authorized"):
            self.run_query(self.user1)

    def test_toggle_user_role_switch_on(self):
        for user, msg in (
            (self.admin, "Test with Admin"),
            (self.user_admin, "Test with UserAdmin"),
        ):
            self.run_query(user)

            self.assertTrue(self.user_mail_mock.called, msg=msg)
            self.assertTrue(self.admin_mail_mock.called, msg=msg)
            self.assertEqual(self.user1.roles, [USER_ROLES.ADMIN])

            self.run_query(user)

            self.assertFalse(self.user_mail_mock.called, msg=msg)
            self.assertFalse(self.admin_mail_mock.called, msg=msg)
            self.assertEqual(self.user1.roles, [])


class TestToggleUserRoleToUseradminTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.admin = AdminFactory()

        self.mutation = """
        mutation ToggleUserRoleToUseradmin($input: toggleUserRoleInput!) {
            toggleUserRole(input: $input) {
                user {
                    roles
                }
            }
        }
        """
        self.variables = {
            "input": {"guid": self.user.guid, "role": USER_ROLES.USER_ADMIN}
        }

    def test_switch_it_on(self):
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.mutation, self.variables)

        self.assertEqual(
            response["data"]["toggleUserRole"]["user"]["roles"],
            [USER_ROLES.USER_ADMIN],
        )

    def test_switch_it_off(self):
        self.user.roles = ["USER_ADMIN"]
        self.user.save()

        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.mutation, self.variables)

        self.assertEqual(
            response["data"]["toggleUserRole"]["user"]["roles"],
            [],
        )
