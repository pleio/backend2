from datetime import timedelta

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, COULD_NOT_USE_EVENT_FILTER
from core.factories import GroupFactory
from core.models import Group
from core.resolvers.queries.shared_filters import is_combination_of
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from entities.cms.factories import CampagnePageFactory
from entities.cms.models import Page
from entities.event.models import Event
from entities.news.models import News
from user.factories import EditorFactory, UserFactory
from user.models import User


class EntitiesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user: User = mixer.blend(User)
        self.user2: User = mixer.blend(User)
        self.admin: User = mixer.blend(User, roles=["ADMIN"])
        self.group: Group = mixer.blend(Group, owner=self.user)

        self.blog1: Blog = Blog.objects.create(
            title="Blog1",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
        )

        self.blog2: Blog = Blog.objects.create(
            title="Blog2",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
        )

        self.blog3: Blog = Blog.objects.create(
            title="Blog3",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            group=self.group,
        )

        self.blog4: Blog = Blog.objects.create(
            title="Blog4",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            group=self.group,
            is_featured=True,
        )

        self.news1: News = News.objects.create(
            title="News1",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            is_featured=True,
        )
        self.page1: Page = mixer.blend(
            Page,
            title="Page1",
            owner=self.admin,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.admin.id)],
        )
        self.page2: Page = mixer.blend(
            Page,
            title="Page2",
            position=0,
            owner=self.admin,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.admin.id)],
            parent=self.page1,
        )
        self.page3: Page = mixer.blend(
            Page,
            title="Page3",
            is_pinned=True,
            position=1,
            parent=self.page1,
            owner=self.admin,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.admin.id)],
        )

        self.blog_draft1: Blog = Blog.objects.create(
            title="Blog draft1",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            group=self.group,
            is_featured=True,
            published=None,
        )

        self.blog_draft2: Blog = Blog.objects.create(
            title="Blog draft2",
            owner=self.user2,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            group=self.group,
            is_featured=True,
            published=timezone.now() + timedelta(days=5),
        )

        self.archived1: Blog = Blog.objects.create(
            title="Archived 1",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            group=self.group,
            is_archived=True,
            published=timezone.now() - timezone.timedelta(days=1),
        )
        self.archived2: Blog = Blog.objects.create(
            title="Archived 2",
            owner=self.user2,
            read_access=[ACCESS_TYPE.public],
            group=self.group,
            is_archived=True,
            published=timezone.now() - timezone.timedelta(days=1),
        )

        self.query = """
            query getEntities(
                    $q: String
                    $subtype: String
                    $containerGuid: String
                    $isFeatured: Boolean
                    $limit: Int
                    $offset: Int
                    $statusPublished: [StatusPublished]
                    $userGuid: String) {
                entities(
                        q: $q
                        subtype: $subtype
                        containerGuid: $containerGuid
                        isFeatured: $isFeatured
                        limit: $limit
                        offset: $offset
                        statusPublished: $statusPublished
                        userGuid: $userGuid) {
                    total
                    edges {
                        guid
                        ...BlogListFragment
                        ...PageListFragment
                        ...NewsListFragment
                        ...EventListFragment
                        __typename
                    }
                }
            }
            fragment BlogListFragment on Blog {
                title
            }
            fragment PageListFragment on Page {
                title
            }
            fragment NewsListFragment on News {
                title
            }
            fragment EventListFragment on Event {
                title
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_entities_all(self):
        variables = {"containerGuid": None}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]["entities"]
        self.assertEqual(data["total"], 8)

    def test_entities_site(self):
        variables = {"containerGuid": "1"}
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result.get("data")
        self.assertIsNotNone(data, msg=result)
        self.assertIsNotNone(data.get("entities"), msg=result)
        self.assertEqual(data["entities"]["total"], 6)

    def test_entities_group(self):
        variables = {"containerGuid": self.group.guid}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 2)

    def test_entities_all_pages_by_admin(self):
        variables = {"subtype": "page"}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 3)

    def test_entities_filtered_is_featured(self):
        variables = {"isFeatured": True}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 2)

    def test_entities_single_filtered_is_featured(self):
        variables = {"isFeatured": True, "subtype": "blog"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 1)

    def test_entities_multiple_filtered_is_featured(self):
        variables = {"isFeatured": True, "subtypes": ["blog", "news"]}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 2)

    def test_entities_limit(self):
        variables = {"limit": 5}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(len(data["entities"]["edges"]), 5)
        self.assertEqual(data["entities"]["total"], 8)

    def test_entities_show_pinned(self):
        variables = {"containerGuid": None, "showPinned": True}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["edges"][0]["guid"], self.page3.guid)

    def test_entities_all_draft(self):
        variables = {"containerGuid": None, "statusPublished": "draft"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 1)

    def test_entities_all_draft_admin(self):
        variables = {"containerGuid": None, "statusPublished": "draft"}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertIsNotNone(data, msg=result)
        self.assertIsNotNone(data.get("entities"), msg=result)

        self.assertEqual(data["entities"]["total"], 2)

    def test_entities_all_draft_anon(self):
        variables = {"containerGuid": None, "statusPublished": "draft"}

        result = self.graphql_client.post(self.query, variables)

        data = result.get("data")
        self.assertIsNotNone(data, msg=result)
        self.assertIsNotNone(data.get("entities"), msg=result)
        self.assertEqual(data["entities"]["total"], 0)

    def test_entities_all_draft_other(self):
        variables = {"containerGuid": None, "statusPublished": ["draft"]}

        self.graphql_client.force_login(self.user2)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertIsNotNone(data, msg=result)
        self.assertIsNotNone(data.get("entities"), msg=result)

        result_guids = {e["guid"] for e in data["entities"]["edges"]}
        result_msg = "Result is %s" % [e["title"] for e in data["entities"]["edges"]]
        self.assertEqual(data["entities"]["total"], 1, msg=result_msg)
        self.assertNotIn(self.blog_draft1.guid, result_guids, msg=result_msg)
        self.assertIn(self.blog_draft2.guid, result_guids, msg=result_msg)

    def test_entities_all_for_user(self):
        variables = {"containerGuid": None, "userGuid": self.user.guid}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        result_guids = {e["guid"] for e in data["entities"]["edges"]}
        result_msg = "Result is %s" % [e["title"] for e in data["entities"]["edges"]]
        self.assertEqual(data["entities"]["total"], 5)
        self.assertIn(self.blog1.guid, result_guids, msg=result_msg)
        self.assertIn(self.blog2.guid, result_guids, msg=result_msg)
        self.assertIn(self.blog3.guid, result_guids, msg=result_msg)
        self.assertIn(self.blog4.guid, result_guids, msg=result_msg)
        self.assertIn(self.news1.guid, result_guids, msg=result_msg)

    def test_entities_archived_filter(self):
        for while_testing_with_user, user in [
            ("while testing admin", self.admin),
            ("while testing anonymous user", AnonymousUser()),
            ("while testing authenticated user", self.user),
        ]:
            variables = {
                "containerGuid": None,
                "statusPublished": "archived",
            }

            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.query, variables)

            self.assertIsNotNone(result, msg="No result %s" % while_testing_with_user)
            self.assertIsNotNone(
                result.get("data"), msg="Data is empty %s" % while_testing_with_user
            )

            data = result.get("data")
            self.assertEqual(
                data["entities"]["total"],
                2,
                msg="Unexpected number of results %s" % while_testing_with_user,
            )
            result_guids = {e["guid"] for e in data["entities"]["edges"]}
            self.assertIn(self.archived1.guid, result_guids)
            self.assertIn(self.archived2.guid, result_guids)

    def test_enities_filter_my_draft_archived_articles(self):
        variables = {
            "containerGuid": None,
            "userGuid": self.user.guid,
            "statusPublished": ["archived", "draft"],
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 2)
        result_guids = {e["guid"] for e in data["entities"]["edges"]}
        self.assertIn(self.blog_draft1.guid, result_guids)
        self.assertIn(self.archived1.guid, result_guids)

    def test_entities_all_for_admin_by_user(self):
        variables = {"containerGuid": None, "userGuid": self.admin.guid}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 3)

    def test_entities_all_for_admin(self):
        variables = {"containerGuid": None, "userGuid": self.admin.guid}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["total"], 3)

    def test_blog_draft_owner(self):
        query = """
            fragment BlogParts on Blog {
                title
            }
            query GetBlog($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...BlogParts
                }
            }
        """

        variables = {"guid": self.blog_draft1.guid}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(query, variables)

        data = result.get("data")
        self.assertIsNotNone(data, msg=result)
        self.assertIsNotNone(data.get("entity"), msg=result)

        self.assertEqual(data["entity"]["guid"], self.blog_draft1.guid)
        self.assertEqual(data["entity"]["title"], self.blog_draft1.title)

    def test_blog_draft_other(self):
        query = """
            fragment BlogParts on Blog {
                title
            }
            query GetBlog($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...BlogParts
                }
            }
        """

        variables = {"guid": self.blog_draft1.guid}

        self.graphql_client.force_login(self.user2)
        result = self.graphql_client.post(query, variables)

        data = result["data"]
        self.assertIsNone(data["entity"])

    def test_entities_no_subevents(self):
        event = Event.objects.create(
            title="Event1",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
        )
        Event.objects.create(
            title="Subevent1",
            owner=self.user,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            parent=event,
        )

        variables = {"containerGuid": None}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["edges"][0]["guid"], event.guid)

    def test_entities_search_by_title(self):
        variables = {"containerGuid": None, "q": "Blog2"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["edges"][0]["guid"], self.blog2.guid)

        variables = {"containerGuid": None, "q": "blog2"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entities"]["edges"][0]["guid"], self.blog2.guid)

        variables = {"containerGuid": None, "q": "blog"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(len(data["entities"]["edges"]), 4)


class EntitiesEventsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.event_in_4_days = Event.objects.create(
            owner=self.user1,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user1.id)],
            start_date=timezone.now() + timezone.timedelta(days=4),
            end_date=timezone.now() + timezone.timedelta(days=4),
        )
        self.event_5_days_ago = Event.objects.create(
            owner=self.user1,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user1.id)],
            start_date=timezone.now() - timezone.timedelta(days=5),
            end_date=timezone.now() - timezone.timedelta(days=5),
        )
        self.event_ongoing = Event.objects.create(
            owner=self.user1,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user1.id)],
            start_date=timezone.now() - timezone.timedelta(days=5),
            end_date=timezone.now() + timezone.timedelta(days=5),
        )
        self.event_in_6_days = Event.objects.create(
            owner=self.user1,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user1.id)],
            start_date=timezone.now() + timezone.timedelta(days=6),
            end_date=timezone.now() + timezone.timedelta(days=6),
        )
        self.event_3_days_ago = Event.objects.create(
            owner=self.user1,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user1.id)],
            start_date=timezone.now() - timezone.timedelta(days=3),
            end_date=timezone.now() - timezone.timedelta(days=3),
        )

        self.query = """
            query getEntities(
                    $subtype: String
                    $containerGuid: String
                    $isFeatured: Boolean
                    $limit: Int
                    $offset: Int
                    $subtypes: [String!]
                    $eventFilter: EventFilter
                    $statusPublished: [StatusPublished]
                    $userGuid: String) {
                entities(
                        subtype: $subtype
                        containerGuid: $containerGuid
                        isFeatured: $isFeatured
                        limit: $limit
                        offset: $offset
                        subtypes: $subtypes,
                        eventFilter: $eventFilter
                        statusPublished: $statusPublished
                        userGuid: $userGuid) {
                    total
                    edges {
                        guid
                        ...EventListFragment
                        __typename
                    }
                }
            }
            fragment EventListFragment on Event {
                title
            }
        """

    def test_entities_filter_by_upcoming(self):
        variables = {
            "limit": 20,
            "offset": 0,
            "subtypes": ["event"],
            "eventFilter": "upcoming",
        }

        self.graphql_client.force_login(self.user2)
        result = self.graphql_client.post(self.query, variables)
        self.assertEqual(len(result["data"]["entities"]["edges"]), 3)
        self.assertEqual(
            result["data"]["entities"]["edges"][0]["guid"], self.event_in_6_days.guid
        )

    def test_entities_filter_by_previous(self):
        variables = {
            "limit": 20,
            "offset": 0,
            "subtypes": ["event"],
            "eventFilter": "previous",
        }

        self.graphql_client.force_login(self.user2)
        result = self.graphql_client.post(self.query, variables)
        self.assertEqual(len(result["data"]["entities"]["edges"]), 2)
        self.assertEqual(
            result["data"]["entities"]["edges"][0]["guid"], self.event_3_days_ago.guid
        )

    def test_entities_blog_event_filter_by_previous(self):
        variables = {
            "limit": 20,
            "offset": 0,
            "subtypes": ["event", "blog"],
            "eventFilter": "previous",
        }

        with self.assertGraphQlError(COULD_NOT_USE_EVENT_FILTER):
            self.graphql_client.force_login(self.user2)
            self.graphql_client.post(self.query, variables)


class TestImproperFormatEntitiesTestCase(PleioTenantTestCase):
    def test_invalid_guid_entity_query(self):
        query = """
        query GetEntity($guid: String!) {
            entity(guid: $guid) {
                guid
            }
        }
        """
        variables = {"guid": "improper-format-guid"}
        response = self.graphql_client.post(query, variables)

        self.assertIsNone(response["data"]["entity"])


class TestGroupFilterTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.group_owner = UserFactory()
        self.group = GroupFactory(owner=self.group_owner)
        self.other_group = GroupFactory(owner=self.group_owner)

        self.user = UserFactory()
        self.group.join(self.user)

        self.article = BlogFactory(
            owner=self.group_owner, group=self.group, title="Article"
        )
        self.other_article = BlogFactory(
            owner=self.group_owner, group=self.other_group, title="Other article"
        )
        self.global_article = BlogFactory(
            owner=self.group_owner, title="Global article"
        )

        self.query = """
            query Entities($groupFilter: [String]) {
                entities(groupFilter: $groupFilter) {
                    edges { ...BlogFragment }
                }
            }
            fragment BlogFragment on Blog {
                title
            }
        """

    def test_my_grouped_articles(self):
        """
        Test that filter 'mine' returns only articles from groups the user is a member of.
        """
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {"groupFilter": ["mine"]})

        self.assertEqual(
            response["data"]["entities"], {"edges": [{"title": self.article.title}]}
        )

    def test_all_grouped_articles(self):
        """
        Test that filter 'all' returns only articles from groups.
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {"groupFilter": ["all"]})

        self.assertEqual(
            response["data"]["entities"],
            {
                "edges": [
                    {"title": self.other_article.title},
                    {"title": self.article.title},
                ]
            },
        )

    def test_all_articles(self):
        """
        Test that filter '' returns all articles.
        """
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {"groupFilter": [""]})

        self.assertFullResult(response)

    def test_clear_filter(self):
        """
        Test that filter '' returns all articles.
        """
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})

        self.assertFullResult(response)

    def assertFullResult(self, response):
        """
        Test that without a filter we get all articles.
        """
        self.assertEqual(
            response["data"]["entities"],
            {
                "edges": [
                    {"title": self.global_article.title},
                    {"title": self.other_article.title},
                    {"title": self.article.title},
                ]
            },
        )


class TestEntityQueryWithLandingPages(PleioTenantTestCase):
    """
    Test entities query behaviour when there are landing-pages in the database.
    """

    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.group_campagne_page = CampagnePageFactory(
            group=self.group, owner=self.owner
        )
        self.group_landing_page = self.group.start_page

        self.query = """
        query Entities($subtypes: [String]) {
            entities(subtypes: $subtypes) {
                edges { guid }
            }
        }
        """

    def test_clean_query(self):
        """
        Test that when the query does not specify subtypes landingpages are not
        in the resultset.
        """
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {})

        ids = [edge["guid"] for edge in result["data"]["entities"]["edges"]]
        self.assertResultContainsPages(ids)
        self.assertNotResultContainsLandingpages(ids)

    def test_filter_pages(self):
        """
        Test that when the query specifies 'page' in subtypes landingpages are
        not in the resultset; but pages are.
        """
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"subtypes": ["page"]})

        ids = [edge["guid"] for edge in result["data"]["entities"]["edges"]]
        self.assertResultContainsPages(ids)
        self.assertNotResultContainsLandingpages(ids)

    def test_filter_landingpages(self):
        """
        Test that when the query specifies "landingpage" in subtypes
        landingpages are in the resultset. but pages are.
        """
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"subtypes": ["landingpage"]})

        ids = [edge["guid"] for edge in result["data"]["entities"]["edges"]]
        self.assertNotResultContainsPages(ids)
        self.assertResultContainsLandingpages(ids)

    def test_filter_all_pages(self):
        """
        Test that when the query specifies both "page" and "landingpage" both
        pages and landingpages are in the result.
        """
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(
            self.query, {"subtypes": ["landingpage", "page"]}
        )

        ids = [edge["guid"] for edge in result["data"]["entities"]["edges"]]
        self.assertResultContainsPages(ids)
        self.assertResultContainsLandingpages(ids)

    def assertResultContainsPages(self, ids):
        self.assertIn(self.group_campagne_page.guid, ids)

    def assertNotResultContainsPages(self, ids):
        self.assertNotIn(self.group_campagne_page.guid, ids)

    def assertResultContainsLandingpages(self, ids):
        self.assertIn(self.group_landing_page.guid, ids)

    def assertNotResultContainsLandingpages(self, ids):
        self.assertNotIn(self.group_landing_page.guid, ids)


class TestEntityQueryWithAllKindsOfPages(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.editor = EditorFactory()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.group_page = CampagnePageFactory(owner=self.owner, group=self.group)
        self.group_page2 = CampagnePageFactory(owner=self.owner, group=self.group)
        self.global_page = CampagnePageFactory(owner=self.editor)
        self.global_page2 = CampagnePageFactory(owner=self.editor)

        self.group_pages = {self.group_page.guid, self.group_page2.guid}
        self.global_pages = {self.global_page.guid, self.global_page2.guid}

        self.query = """
        query Entities($containerGuid: String) {
            entities(containerGuid: $containerGuid) {
                edges { guid }
            }
        }
        """

    """
    Tests
    """

    def test_query_all_pages(self):
        """
        Test that when the query does not specify containerGuid all pages are in the result set.
        """

        ids = self.post_query({})

        self.assertResultContainsGroupPages(ids)
        self.assertResultContainsGlobalPages(ids)

    def test_query_group_pages(self):
        """
        Test that when the group is given as containerGuid only group pages are in the result set.
        """

        ids = self.post_query({"containerGuid": self.group.guid})

        self.assertResultContainsGroupPages(ids)
        self.assertNotResultContainsGlobalPages(ids)

    def test_query_global_pages(self):
        """
        Test that when '1' is given as containerGuid group pages are excluded from the result set.
        """

        ids = self.post_query({"containerGuid": "1"})

        self.assertNotResultContainsGroupPages(ids)
        self.assertResultContainsGlobalPages(ids)

    """
    Helper methods
    """

    def post_query(self, variables):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)
        return {edge["guid"] for edge in result["data"]["entities"]["edges"]}

    def assertResultContainsGroupPages(self, ids):
        self.assertEqual(len({*ids} & self.group_pages), len(self.group_pages))

    def assertNotResultContainsGroupPages(self, ids):
        self.assertEqual(len({*ids} & self.group_pages), 0)

    def assertResultContainsGlobalPages(self, ids):
        self.assertEqual(len({*ids} & self.global_pages), len(self.global_pages))

    def assertNotResultContainsGlobalPages(self, ids):
        self.assertEqual(len({*ids} & self.global_pages), 0)


class TestIsCombinationOfHelperMethod(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.reference = ["left", "right"]

    def test_match_all(self):
        """True if all elements are in reference"""
        self.assertTrue(is_combination_of(["left", "right"], self.reference))

    def test_match_one(self):
        """True if one element is in reference"""
        self.assertTrue(is_combination_of(["left"], self.reference))
        self.assertTrue(is_combination_of(["right"], self.reference))

    def test_match_empty(self):
        """False if input is empty"""
        self.assertFalse(is_combination_of([], self.reference))

    def test_match_none(self):
        """Only true if all elements exclusively exist in reference"""
        self.assertFalse(is_combination_of(["foo", "bar"], self.reference))

    def test_match_mixed(self):
        """Only true if all elements exist in the reference"""
        self.assertFalse(is_combination_of(["left", "foo"], self.reference))
