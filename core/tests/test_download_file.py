from django.core.files.base import ContentFile

from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestDownloadHtmlFileTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=False)
        self.owner = UserFactory()
        self.file = FileFactory(
            owner=self.owner, upload=ContentFile(b"html content", "test.html")
        )

    def test_download_html_file(self):
        response = self.client.get(self.file.download_url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            response.headers["content-disposition"].startswith("attachment")
        )

    def test_attachment_html_file(self):
        response = self.client.get(self.file.attachment_url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            response.headers["content-disposition"].startswith("attachment")
        )
