from unittest import mock

from django.http import HttpRequest
from django.test import override_settings
from django.utils import timezone

from core.exceptions import FloodOverflowError
from core.models import FloodLog
from core.tests.helpers import PleioTenantTestCase


class TestFloodLog(PleioTenantTestCase):
    EXPECTED_IP = "127.0.0.1"
    EXPECTED_TARGET = "login"
    EXPECTED_USER_IDENTIFIER = "user@test.com"

    def setUp(self):
        super().setUp()
        for _n in range(0, 3):
            FloodLog.objects.create(
                ip=self.EXPECTED_IP,
                target=self.EXPECTED_TARGET,
                user_identifier=self.EXPECTED_USER_IDENTIFIER,
                expires=timezone.now() + timezone.timedelta(hours=1),
            )
        self.get_client_ip = mock.patch("core.models.flood_log.get_client_ip").start()
        self.get_client_ip.return_value = self.EXPECTED_IP

        self.get_user_identifier_from_request = mock.patch(
            "core.models.flood_log.get_user_identifier_from_request"
        ).start()
        self.get_user_identifier_from_request.return_value = (
            self.EXPECTED_USER_IDENTIFIER
        )

    def tearDown(self):
        for record in FloodLog.objects.all():
            record.delete()
        super().tearDown()

    @override_settings(FLOOD_THRESHOLD=3)
    def test_log_overflow(self):
        try:
            FloodLog.objects.assert_not_blocked(HttpRequest(), self.EXPECTED_TARGET)
            self.fail("unexpectedly did not break at overflow situation")
        except FloodOverflowError:
            pass

    @override_settings(FLOOD_THRESHOLD=4)
    def test_log_no_overflow(self):
        try:
            FloodLog.objects.assert_not_blocked(HttpRequest(), self.EXPECTED_TARGET)
        except FloodOverflowError:
            self.fail("unexpectedly did break at non-overflow situation")
