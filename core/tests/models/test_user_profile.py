from unittest.mock import MagicMock, patch

from django.core.files.base import ContentFile
from django.test import tag

from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import UserFactory


@tag("TestCasesWithWidgets")
@tag("UserWidgetSettings")
class TestUserWidgetSettingsTestCase(PleioTenantTestCase):
    """Test user widget settings."""

    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.profile = self.user.profile
        self.profile.set_widget_settings(
            "widget1",
            [
                {
                    "key": "widget1",
                    "value": "settings1",
                },
            ],
        )
        self.profile.set_widget_settings(
            "widget2",
            [
                {
                    "key": "widget2",
                    "value": "settings2",
                },
            ],
        )
        self.profile.save()

    def test_get_widget_settings(self):
        self.assertEqual(
            self.profile.get_widget_settings("widget1"),
            [
                {
                    "key": "widget1",
                    "value": "settings1",
                },
            ],
        )
        self.assertEqual(
            self.profile.get_widget_settings("widget2"),
            [
                {
                    "key": "widget2",
                    "value": "settings2",
                },
            ],
        )

    def test_set_widget_settings(self):
        self.profile.set_widget_settings(
            "widget1",
            [
                {
                    "key": "widget1",
                    "value": "Foo",
                },
            ],
        )
        self.assertEqual(
            self.profile.get_widget_settings("widget1"),
            [
                {
                    "key": "widget1",
                    "value": "Foo",
                },
            ],
        )

    def test_clear_widget_settings(self):
        self.profile.set_widget_settings("widget1", None)
        self.assertIsNone(self.profile.get_widget_settings("widget1"))


class TestAvatarFilePropertyTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.profile = self.user.profile

    def build_avatar(self):
        return FileFactory(owner=self.user, upload=ContentFile(b"123", "avatar.jpg"))

    def test_no_avatar(self):
        self.assertIsNone(self.profile.avatar_file)

    def test_avatar_from_concierge(self):
        avatar = self.build_avatar()
        self.profile.avatar_managed_file = avatar

        self.assertEqual(self.profile.avatar_file, avatar)

    def test_avatar_picture(self):
        avatar = self.build_avatar()
        self.profile.picture_file = avatar

        self.assertEqual(self.profile.avatar_file, avatar)


class TestUpdateAvatarTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.profile = self.user.profile
        self.avatar_file = FileFactory(
            owner=self.user, upload=ContentFile(b"123", "avatar.jpg")
        )

        self.file_fetcher = MagicMock()
        self.file_fetcher.process = MagicMock()
        self.file_fetcher.process.return_value = self.avatar_file

        self.file_fetcher_mock = patch("core.models.user.FetchAvatarFile").start()
        self.file_fetcher_mock.return_value = self.file_fetcher

        self.profile_save_mock = patch("core.models.user.UserProfile.save").start()

    def test_update_avatar(self):
        self.profile.update_avatar("https://example.com/avatar.jpg")

        self.file_fetcher_mock.assert_called_once_with(
            self.user, "https://example.com/avatar.jpg"
        )
        self.file_fetcher.process.assert_called_once_with()
        self.profile_save_mock.assert_called_once_with()
        self.assertEqual(self.profile.avatar_managed_file, self.avatar_file)
