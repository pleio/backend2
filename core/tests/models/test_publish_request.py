from django.contrib.auth.models import AnonymousUser
from django.core.files.base import ContentFile
from django.utils import timezone

from core.models import PublishRequest
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestPublishRequestModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.admin = AdminFactory()
        self.owner = UserFactory()
        self.blog = BlogFactory(owner=self.owner, published=None)

    def test_assign_publish_request(self):
        request = self.blog.publish_requests.create(time_published=timezone.now())

        request.assign_request(self.admin)

        self.assertEqual(request.assigned_to, self.admin)

    def test_reset_publish_request(self):
        request = self.blog.publish_requests.create(
            time_published=timezone.now(), assigned_to=self.admin
        )

        request.reset_request()

        self.assertEqual(request.assigned_to, None)

    def test_confirm_request(self):
        expected_publish_time = timezone.now()
        request = self.blog.publish_requests.create(
            time_published=expected_publish_time
        )

        request.confirm_request()
        self.blog.refresh_from_db()

        self.assertEqual(request.status, "published")
        self.assertEqual(self.blog.published, expected_publish_time)


class TestFileFilterTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()

        self.blog_request = PublishRequest.objects.create(
            entity=BlogFactory(owner=self.owner, published=None)
        )
        self.file_request = PublishRequest.objects.create(
            entity=FileFactory(
                owner=self.owner,
                published=None,
                upload=ContentFile(b"Test123!", "test.txt"),
            )
        )
        self.folder_request = PublishRequest.objects.create(
            entity=FolderFactory(owner=self.owner, published=None)
        )

    def test_filter_files(self):
        response = PublishRequest.objects.filter_files()

        self.assertEqual(response.count(), 1)
        self.assertEqual(
            response.values_list("id", flat=True).first(),
            self.file_request.id,
        )

    def test_exclude_files(self):
        response = PublishRequest.objects.exclude_files()

        self.assertEqual(response.count(), 2)
        self.assertEqual(
            {*response.values_list("id", flat=True)},
            {self.blog_request.id, self.folder_request.id},
        )


class Wrapper:
    class TestWithBlogsInModerationBaseTestCase(PleioTenantTestCase):
        def setUp(self):
            super().setUp()
            self.owner1 = UserFactory(email="owner1@example.com")
            self.blog1 = self._create_blog_in_moderation(self.owner1)
            self.blog2 = self._create_blog_in_moderation(self.owner1)

            self.owner2 = UserFactory(email="owner2@example.com")
            self.blog3 = self._create_blog_in_moderation(self.owner2)
            self.blog4 = self._create_blog_in_moderation(self.owner2)

        @staticmethod
        def _create_blog_in_moderation(owner):
            blog = BlogFactory(owner=owner, published=None)
            blog.publish_requests.create(time_published=timezone.now())
            return blog

        def assertMatchExpectedPublishRequests(self, response, expected_blogs):
            self.assertEqual(response.count(), len(expected_blogs))
            self.assertEqual(
                {*response.values_list("id", flat=True)},
                {b.publish_requests.first().id for b in expected_blogs},
            )


class TestOwnerFilterTestCase(Wrapper.TestWithBlogsInModerationBaseTestCase):
    def test_filter_owner(self):
        response = PublishRequest.objects.filter_owner(self.owner1)
        self.assertMatchExpectedPublishRequests(response, {self.blog1, self.blog2})

        response = PublishRequest.objects.filter_owner(self.owner2)
        self.assertMatchExpectedPublishRequests(response, {self.blog3, self.blog4})


class TestVisiblePublishRequests(Wrapper.TestWithBlogsInModerationBaseTestCase):
    def test_anonymous(self):
        # given
        user = AnonymousUser()

        # when
        result = PublishRequest.objects.visible(user)

        # then
        self.assertMatchExpectedPublishRequests(result, [])

    def test_logged_in_user(self):
        # given
        user = UserFactory()

        # when
        result = PublishRequest.objects.visible(user)

        # then
        self.assertMatchExpectedPublishRequests(result, [])

    def test_owner(self):
        # given
        user = self.owner1

        # when
        result = PublishRequest.objects.visible(user)

        # then
        self.assertMatchExpectedPublishRequests(result, {self.blog1, self.blog2})

    def test_editor_user(self):
        # given
        user = EditorFactory()

        # when
        result = PublishRequest.objects.visible(user)

        # then
        self.assertFullAccess(result)

    def test_site_admin_user(self):
        # given
        user = AdminFactory()

        # when
        result = PublishRequest.objects.visible(user)

        # then
        self.assertFullAccess(result)

    def assertFullAccess(self, result):
        self.assertMatchExpectedPublishRequests(
            result, {self.blog1, self.blog2, self.blog3, self.blog4}
        )
