from unittest.mock import patch

from core.models.featured import FeaturedCoverMixin
from core.tests.helpers import PleioTenantTestCase, override_config


class TestFeaturedCoverMixinTestCase(PleioTenantTestCase):
    def test_extract_video_details(self):
        self.assertIsNone(FeaturedCoverMixin.extract_video_details(None))
        self.assertIsNone(FeaturedCoverMixin.extract_video_details(""))

    def test_extract_video_details_youtube(self):
        self.assertEqual(
            FeaturedCoverMixin.extract_video_details(
                "https://www.youtube.com/watch?v=12345"
            ),
            {
                "type": "youtube",
                "id": "12345",
            },
        )

        self.assertEqual(
            FeaturedCoverMixin.extract_video_details(
                "https://www.youtube.com/embed/12345"
            ),
            {
                "type": "youtube",
                "id": "12345",
            },
        )

        self.assertEqual(
            FeaturedCoverMixin.extract_video_details("https://www.youtube.com/v/12345"),
            {
                "type": "youtube",
                "id": "12345",
            },
        )

        self.assertEqual(
            FeaturedCoverMixin.extract_video_details("https://youtu.be/12345"),
            {
                "type": "youtube",
                "id": "12345",
            },
        )

    def test_extract_video_details_vimeo(self):
        self.assertEqual(
            FeaturedCoverMixin.extract_video_details("https://vimeo.com/12345"),
            {
                "type": "vimeo",
                "id": "12345",
            },
        )

        self.assertEqual(
            FeaturedCoverMixin.extract_video_details(
                "https://player.vimeo.com/video/12345"
            ),
            {
                "type": "vimeo",
                "id": "12345",
            },
        )

        self.assertEqual(
            FeaturedCoverMixin.extract_video_details(
                "https://vimeo.com/channels/12345"
            ),
            {
                "type": "vimeo",
                "id": "12345",
            },
        )

        self.assertEqual(
            FeaturedCoverMixin.extract_video_details(
                "https://vimeo.com/groups/lala/videos/12345"
            ),
            {
                "type": "vimeo",
                "id": "12345",
            },
        )

    def test_extract_video_details_kaltura(self):
        self.assertEqual(
            FeaturedCoverMixin.extract_video_details(
                "https://videoleren.nvwa.nl/12345"
            ),
            {
                "type": "kaltura",
                "id": "12345",
            },
        )

    def test_get_video_thumbnail_url(self):
        self.assertIsNone(FeaturedCoverMixin.get_video_thumbnail_url(None))
        self.assertIsNone(FeaturedCoverMixin.get_video_thumbnail_url(""))

    def test_get_video_thumbnail_url_youtube(self):
        self.assertEqual(
            FeaturedCoverMixin.get_video_thumbnail_url(
                "https://www.youtube.com/watch?v=12345"
            ),
            "https://i.ytimg.com/vi/12345/maxresdefault.jpg",
        )

        self.assertEqual(
            FeaturedCoverMixin.get_video_thumbnail_url(
                "https://www.youtube.com/embed/12345"
            ),
            "https://i.ytimg.com/vi/12345/maxresdefault.jpg",
        )

        self.assertEqual(
            FeaturedCoverMixin.get_video_thumbnail_url(
                "https://www.youtube.com/v/12345"
            ),
            "https://i.ytimg.com/vi/12345/maxresdefault.jpg",
        )

        self.assertEqual(
            FeaturedCoverMixin.get_video_thumbnail_url("https://youtu.be/12345"),
            "https://i.ytimg.com/vi/12345/maxresdefault.jpg",
        )

    @patch("core.models.featured.requests.get")
    def test_get_video_thumbnail_url_vimeo(self, mock_get):
        mock_response = mock_get.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = [
            {"thumbnail_large": "https://i.vimeocdn.com/12345_1280.jpg"}
        ]

        self.assertEqual(
            FeaturedCoverMixin.get_video_thumbnail_url("https://vimeo.com/12345"),
            "https://i.vimeocdn.com/12345_1280.jpg",
        )

    @override_config(KALTURA_VIDEO_PARTNER_ID="54321")
    def test_get_video_thumbnail_url_kaltura(self):
        self.assertEqual(
            FeaturedCoverMixin.get_video_thumbnail_url(
                "https://videoleren.nvwa.nl/12345"
            ),
            "https://api.eu.kaltura.com/p/54321/thumbnail/entry_id/12345/width/0/height/0",
        )
