from django.contrib.auth.models import AnonymousUser
from django.utils import timezone
from django.utils.timezone import timedelta
from freezegun import freeze_time

from core.factories import CommentFactory
from core.models.comment import CommentModerationRequest
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestCommentModerationRequest(PleioTenantTestCase):
    """
    Test CommentModerationRequest attributes
    """

    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.admin = AdminFactory()
        self.article = BlogFactory(owner=self.owner)

    def test_set_assignee(self):
        moderation_request = CommentModerationRequest.objects.create(
            entity=self.article
        )
        now = timezone.now()

        with freeze_time(now):
            moderation_request.assign_to(self.admin)

        self.assertEqual(moderation_request.assigned_to, self.admin)
        self.assertEqual(moderation_request.assigned_until, now + timedelta(hours=1))

    def test_reset_assignee(self):
        moderation_request = CommentModerationRequest.objects.create(
            entity=self.article,
            assigned_to=self.admin,
            assigned_until=timezone.now() + timedelta(hours=1),
        )

        moderation_request.reset_assignee()

        self.assertIsNone(moderation_request.assigned_to)
        self.assertIsNone(moderation_request.assigned_until)

    def test_reset_if_no_comments(self):
        # Given
        moderation_request = CommentModerationRequest.objects.create(
            entity=self.article,
            assigned_to=self.admin,
            assigned_until=timezone.now() + timedelta(hours=1),
        )
        CommentFactory(
            owner=self.owner,
            container=self.article,
            comment_moderation_request=moderation_request,
        )

        # When
        moderation_request.reset_if_no_comments()
        moderation_request.refresh_from_db()

        # Then
        self.assertIsNotNone(moderation_request.assigned_to)

    def test_reset_if_no_comments_no_comments(self):
        # Given
        moderation_request = CommentModerationRequest.objects.create(
            entity=self.article,
            assigned_to=self.admin,
            assigned_until=timezone.now() + timedelta(hours=1),
        )

        # When
        moderation_request.reset_if_no_comments()
        moderation_request.refresh_from_db()

        # Then
        self.assertIsNone(moderation_request.assigned_to)
        self.assertIsNone(moderation_request.assigned_until)


class Wrapper:
    class VisibilityBaseTestCase(PleioTenantTestCase):
        def setUp(self):
            super().setUp()

            self.respondent1 = UserFactory()
            self.comment1 = self._create_comment(self.respondent1)
            self.comment2 = self._create_comment(self.respondent1)
            self.comment3 = self._create_comment(UserFactory())
            self.comment4 = self._create_comment(UserFactory())

        @staticmethod
        def _create_comment(respondent):
            blog = BlogFactory(owner=UserFactory())
            moderation_request = CommentModerationRequest.objects.create(entity=blog)
            return CommentFactory(
                owner=respondent,
                container=blog,
                comment_moderation_request=moderation_request,
            )


class TestVisibleCommentModerationRequests(Wrapper.VisibilityBaseTestCase):
    def test_visible_for_anonymous(self):
        requests = CommentModerationRequest.objects.visible(AnonymousUser())
        self.assertEqual(requests.count(), 0)

    def test_visible_for_visitor(self):
        requests = CommentModerationRequest.objects.visible(UserFactory())
        self.assertEqual(requests.count(), 0)

    def test_visible_for_owner(self):
        requests = CommentModerationRequest.objects.visible(self.respondent1)
        self.assertEqual(requests.count(), 2)
        self.assertIn(self.comment1.comment_moderation_request, requests)
        self.assertIn(self.comment2.comment_moderation_request, requests)

    def test_visible_for_editor(self):
        requests = CommentModerationRequest.objects.visible(EditorFactory())
        self.assertFullAccess(requests)

    def test_visible_for_admin(self):
        requests = CommentModerationRequest.objects.visible(AdminFactory())
        self.assertFullAccess(requests)

    def assertFullAccess(self, requests):
        self.assertEqual(requests.count(), 4)
        self.assertIn(self.comment1.comment_moderation_request, requests)
        self.assertIn(self.comment2.comment_moderation_request, requests)
        self.assertIn(self.comment3.comment_moderation_request, requests)
        self.assertIn(self.comment4.comment_moderation_request, requests)


class TestOwnedCommentModerationRequestTestCase(Wrapper.VisibilityBaseTestCase):
    def test_not_a_user(self):
        requests = CommentModerationRequest.objects.filter_owner(None)
        self.assertEqual(requests.count(), 0)

    def test_non_responding_user(self):
        requests = CommentModerationRequest.objects.filter_owner(UserFactory())
        self.assertEqual(requests.count(), 0)

    def test_responding_user(self):
        requests = CommentModerationRequest.objects.filter_owner(self.respondent1.guid)
        self.assertEqual(requests.count(), 2)
        self.assertIn(self.comment1.comment_moderation_request, requests)
        self.assertIn(self.comment2.comment_moderation_request, requests)


class TestFilterCommentModerationRequestsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.respondent = UserFactory()
        self.respondent1 = UserFactory()
        self.respondent2 = UserFactory()
        self.admin = AdminFactory()
        self.comment_set1 = self._create_comment([self.respondent], 3)
        self.comment_set2 = self._create_comment([self.respondent1], 2)
        self.comment_set3 = self._create_comment([self.respondent, self.respondent2], 1)

    @staticmethod
    def _create_comment(respondents, comment_count):
        blog = BlogFactory(owner=UserFactory())
        moderation_request = CommentModerationRequest.objects.create(entity=blog)
        for respondent in respondents:
            for _ in range(comment_count):
                CommentFactory(
                    owner=respondent,
                    container=blog,
                    comment_moderation_request=moderation_request,
                )
        return moderation_request

    def test_filter_respondent(self):
        respondent_requests = CommentModerationRequest.objects.filter_owner(
            self.respondent.guid
        )
        self.assertEqual(respondent_requests.count(), 2)
        self.assertIn(self.comment_set1, respondent_requests)
        self.assertNotIn(self.comment_set2, respondent_requests)
        self.assertIn(self.comment_set3, respondent_requests)

    def test_filter_respondent1(self):
        respondent_requests = CommentModerationRequest.objects.filter_owner(
            self.respondent1.guid
        )
        self.assertEqual(respondent_requests.count(), 1)
        self.assertNotIn(self.comment_set1, respondent_requests)
        self.assertIn(self.comment_set2, respondent_requests)
        self.assertNotIn(self.comment_set3, respondent_requests)

    def test_filter_respondent2(self):
        respondent_requests = CommentModerationRequest.objects.filter_owner(
            self.respondent2.guid
        )
        self.assertEqual(respondent_requests.count(), 1)
        self.assertNotIn(self.comment_set1, respondent_requests)
        self.assertNotIn(self.comment_set2, respondent_requests)
        self.assertIn(self.comment_set3, respondent_requests)
