from unittest import mock

from django.contrib.auth.models import AnonymousUser
from django.db import connection
from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.factories import CommentFactory
from core.models import Comment
from core.models.comment import CommentModerationRequest
from core.tests._shared.mixin import MixinTestCase
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from user.factories import AdminFactory, EditorFactory, UserFactory
from user.models import User


class TestCommentModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.container = BlogFactory(owner=UserFactory())
        self.owner = UserFactory()
        self.comment = CommentFactory(owner=self.owner, container=self.container)

    def test_can_edit(self):
        self.assert_common_update_access(lambda user: self.comment.can_write(user))

    def test_can_archive(self):
        self.assert_common_update_access(lambda user: self.comment.can_archive(user))

    def assert_common_update_access(self, callback):
        self.assertFalse(callback(AnonymousUser()))
        self.assertTrue(callback(self.owner))
        self.assertFalse(callback(UserFactory()))
        self.assertFalse(callback(EditorFactory()))
        self.assertTrue(callback(AdminFactory()))

    def test_can_edit_moderated_comment(self):
        self.override_config(
            COMMENT_MODERATION_ENABLED=True, REQUIRE_COMMENT_MODERATION_FOR=["blog"]
        )

        self.assertFalse(self.comment.can_write(AnonymousUser()))
        self.assertFalse(self.comment.can_write(self.owner))
        self.assertFalse(self.comment.can_write(UserFactory()))
        self.assertTrue(self.comment.can_write(EditorFactory()))
        self.assertTrue(self.comment.can_write(AdminFactory()))

    def test_can_archive_moderated_comment(self):
        self.override_config(
            COMMENT_MODERATION_ENABLED=True, REQUIRE_COMMENT_MODERATION_FOR=["blog"]
        )

        self.assertFalse(self.comment.can_archive(AnonymousUser()))
        self.assertTrue(self.comment.can_archive(self.owner))
        self.assertFalse(self.comment.can_archive(UserFactory()))
        self.assertTrue(self.comment.can_archive(EditorFactory()))
        self.assertTrue(self.comment.can_archive(AdminFactory()))


class CommentTestCase(MixinTestCase.AssertCanUploadMedia, PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.anonymousUser = AnonymousUser()
        self.authenticated_user = mixer.blend("user.User", name="authenticated_user")
        self.responding_user = mixer.blend("user.User", name="responding_user")

        self.blogPublic = Blog.objects.create(
            title="Test public blog",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticated_user.id)],
            owner=self.authenticated_user,
            is_recommended=True,
            group=None,
        )

        self.comments = mixer.cycle(5).blend(
            Comment, owner=self.authenticated_user, container=self.blogPublic
        )

        self.anonComment = Comment.objects.create(
            name="Anoniemptje",
            email="test@test.com",
            container=self.blogPublic,
            rich_description="Just testing 1",
        )

        self.lastComment = Comment.objects.create(
            owner=self.authenticated_user,
            container=self.blogPublic,
            rich_description="Just testing 2",
        )

        self.lastCommentSub = Comment.objects.create(
            owner=self.responding_user,
            container=self.lastComment,
            rich_description="reply to just testing 2",
        )

        self.mutation1 = """
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                        guid
                    }
                }
            }
        """

        self.mutation2 = """
            mutation ($input: deleteEntityInput!) {
                deleteEntity(input: $input) {
                    success
                }
            }
        """

        self.query = """
            fragment BlogParts on Blog {
                title
                commentCount
                comments {
                    guid
                    richDescription
                    ownerName
                    canComment
                    canEdit
                    canArchiveAndDelete
                    commentCount
                    comments {
                        guid
                        richDescription
                        ownerName
                        canComment
                        canEdit
                        canArchiveAndDelete
                    }
                }
            }
            query GetBlog($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...BlogParts
                }
            }
            """

    def tearDown(self):
        super().tearDown()

    def test_blog_anonymous(self):
        variables = {"guid": self.blogPublic.guid}

        result = self.graphql_client.post(self.query, variables)

        data = result["data"]

        self.assertEqual(data["entity"]["guid"], self.blogPublic.guid)
        self.assertEqual(data["entity"]["commentCount"], 8)

        # first should be last added comment
        self.assertEqual(data["entity"]["comments"][0]["guid"], self.lastComment.guid)
        self.assertEqual(
            data["entity"]["comments"][0]["ownerName"], self.lastComment.owner.name
        )
        self.assertEqual(
            data["entity"]["comments"][0]["richDescription"],
            self.lastComment.rich_description,
        )
        self.assertEqual(data["entity"]["comments"][0]["canComment"], False)
        self.assertFalse(data["entity"]["comments"][0]["canEdit"])
        self.assertFalse(data["entity"]["comments"][0]["canArchiveAndDelete"])

        # second should be anon comment
        self.assertEqual(
            data["entity"]["comments"][1]["ownerName"], self.anonComment.name
        )
        self.assertFalse(data["entity"]["comments"][1]["canEdit"])
        self.assertFalse(data["entity"]["comments"][1]["canArchiveAndDelete"])

    # test toevoegen voor het checken op media in question comments
    def test_comment_on_comment(self):
        variables = {"guid": self.blogPublic.guid}

        self.graphql_client.force_login(self.responding_user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]

        self.assertEqual(data["entity"]["guid"], self.blogPublic.guid)
        self.assertEqual(data["entity"]["commentCount"], 8)
        comments = data["entity"]["comments"]
        comments_comments = comments[0]["comments"]

        # First should be last added comment
        self.assertEqual(comments[0]["guid"], self.lastComment.guid)
        self.assertEqual(comments[0]["ownerName"], self.lastComment.owner.name)
        self.assertEqual(comments[0]["canComment"], True)
        self.assertEqual(
            comments[0]["richDescription"], self.lastComment.rich_description
        )
        self.assertEqual(comments[0]["commentCount"], 1)
        self.assertFalse(comments[0]["canEdit"])
        self.assertFalse(comments[0]["canArchiveAndDelete"])
        self.assertEqual(comments_comments[0]["guid"], self.lastCommentSub.guid)
        self.assertEqual(comments_comments[0]["ownerName"], "responding_user")
        self.assertEqual(comments_comments[0]["canComment"], False)
        self.assertEqual(comments_comments[0]["canEdit"], True)
        self.assertEqual(comments_comments[0]["canArchiveAndDelete"], True)

        # second should be anon comment
        self.assertEqual(comments[1]["ownerName"], self.anonComment.name)

    def test_edit_comment_not_logged_in(self):
        variables = {"input": {"guid": self.anonComment.guid}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation1, variables)

    def test_edit_comment_could_not_find(self):
        variables = {"input": {"guid": "43ee295a-5950-4330-8f0e-372f9f4caddf"}}

        with self.assertGraphQlError("could_not_find"):
            self.graphql_client.force_login(self.authenticated_user)
            self.graphql_client.post(self.mutation1, variables)

    def test_edit_comment_could_not_save(self):
        variables = {"input": {"guid": self.anonComment.guid}}

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.authenticated_user)
            self.graphql_client.post(self.mutation1, variables)

    def test_edit_comment(self):
        variables = {
            "input": {"guid": self.lastComment.guid, "richDescription": "test"}
        }

        self.graphql_client.force_login(self.authenticated_user)
        self.graphql_client.post(self.mutation1, variables)

        self.lastComment.refresh_from_db()

        self.assertEqual(
            self.lastComment.rich_description, variables["input"]["richDescription"]
        )
        self.assert_can_upload_media_called_once_with(
            user=self.authenticated_user,
            entity_valid=lambda e: (e == self.lastComment.container)
            or ("Given entity is not the expected container but %s" % e),
            input={"richDescription": "test"},
        )

    # test toevoegen voor het checken op media in question comments
    @tag("createEntity")
    def test_add_comment(self):
        blog = Blog.objects.create(
            title="Test public blog",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticated_user.id)],
            owner=self.authenticated_user,
            is_recommended=True,
            group=None,
        )

        mutation = """
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                        guid
                    }
                }
            }
        """
        variables = {
            "input": {
                "subtype": "comment",
                "containerGuid": blog.guid,
                "richDescription": "Comment one",
            }
        }

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(mutation, variables)

        data = result["data"]

        self.assertEqual(blog.comments.count(), 1)

        self.assert_can_upload_media_called_once_with(
            user=self.authenticated_user,
            entity_valid=lambda e: e == blog
            or ("Given entity is not the expected container but %s" % e),
            input={"richDescription": "Comment one"},
            _reset=True,
        )

        # sub

        variables = {
            "input": {
                "subtype": "comment",
                "containerGuid": data["addEntity"]["entity"]["guid"],
                "richDescription": "Comment two",
            }
        }

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(mutation, variables)

        data = result["data"]

        self.assertEqual(blog.comments.count(), 1)
        self.assertEqual(blog.comments.first().comments.count(), 1)
        self.assert_can_upload_media_called_once_with(
            user=self.authenticated_user,
            entity_valid=lambda e: e == blog
            or "Entity is not the expected blog but %s" % e,
            input={"richDescription": "Comment two"},
            _reset=True,
        )

        # sub sub comments

        variables = {
            "input": {
                "subtype": "comment",
                "containerGuid": data["addEntity"]["entity"]["guid"],
                "richDescription": "Comment three",
            }
        }

        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.authenticated_user)
            self.graphql_client.post(mutation, variables)

        self.assertEqual(blog.comments.count(), 1)
        self.assertEqual(blog.comments.first().comments.count(), 1)
        self.assertEqual(blog.comments.first().comments.first().comments.count(), 0)
        self.assert_can_upload_media_not_called()

    def test_flat_comment_list(self):
        owner = mixer.blend(User)
        # Blog, or any other comment containing entity.
        instance = Blog.objects.create(
            owner=owner,
            title="Title",
            rich_description="Bla",
            read_access=[ACCESS_TYPE.public],
            published=timezone.now() - timezone.timedelta(days=-1),
        )
        # Normal comment.
        c1 = Comment.objects.create(container=instance, owner=owner)
        # Nested comment.
        Comment.objects.create(container=c1, owner=owner)

        query = """
        query GetCommentCount($query: String!) {
          entity(guid: $query) {
            guid
            ... on Blog {
              commentCount
            }
            __typename
          }
        }
        """
        variables = {"query": instance.guid}

        self.graphql_client.force_login(owner)
        result = self.graphql_client.post(query, variables)

        self.assertEqual(result["data"]["entity"]["commentCount"], 2)

    def test_delete_comment_not_logged_in(self):
        variables = {"input": {"guid": self.anonComment.guid}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation2, variables)

    def test_delete_comment_could_not_find(self):
        variables = {"input": {"guid": "43ee295a-5950-4330-8f0e-372f9f4caddf"}}

        with self.assertGraphQlError("could_not_find"):
            self.graphql_client.force_login(self.authenticated_user)
            self.graphql_client.post(self.mutation2, variables)

    def test_delete_comment_could_not_save(self):
        variables = {"input": {"guid": self.anonComment.guid}}

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.authenticated_user)
            self.graphql_client.post(self.mutation2, variables)

    def test_delete_comment(self):
        variables = {"input": {"guid": self.lastComment.guid}}

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation2, variables)

        self.assertTrue(result["data"]["deleteEntity"]["success"])

    @mock.patch("core.tasks.create_notification.delay")
    def test_notification_send(self, mocked_create_notification):
        self.comment1 = Comment.objects.create(
            owner=self.authenticated_user, container=self.blogPublic
        )

        mocked_create_notification.assert_called_once_with(
            connection.schema_name,
            "commented",
            "blog.blog",
            self.blogPublic.id,
            self.comment1.owner.id,
        )


class SpecificCommentTestCases:
    @tag("TestCommentsInModeration")
    class TestCommentsInModeration(PleioTenantTestCase):
        QUERY_CONTENT_TYPE = None

        def get_query(self):
            return """
            query GetComments($guid: String!) {
                entity(guid: $guid) {
                    ... on %(content_type)s {
                        comments {
                            guid
                            pendingModeration
                            comments {
                                guid
                                pendingModeration
                            }
                        }
                    }
                }
            }
            """ % {
                "content_type": self.QUERY_CONTENT_TYPE,
            }

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def setUp(self):
            super().setUp()
            self.override_config(COMMENT_MODERATION_ENABLED=True)

            self.owner = UserFactory()
            self.respondent = UserFactory()
            self.admin = AdminFactory()
            self.entity = self.build_entity(owner=self.owner)
            self.comment_moderation_request = CommentModerationRequest.objects.create(
                entity=self.entity
            )
            self.published_comment = CommentFactory(
                container=self.entity, owner=self.respondent
            )
            self.unpublished_comment = CommentFactory(
                container=self.entity,
                owner=self.respondent,
                comment_moderation_request=self.comment_moderation_request,
            )
            self.published_subcomment = CommentFactory(
                container=self.published_comment, owner=self.respondent
            )
            self.unpublished_subcomment = CommentFactory(
                container=self.published_comment,
                owner=self.respondent,
                comment_moderation_request=self.comment_moderation_request,
            )
            self.query = self.get_query()
            self.variables = {"guid": self.entity.guid}

        def assertCompleteCommentResponse(self, result):
            self.assertEqual(
                result["data"]["entity"]["comments"],
                [
                    {
                        "guid": self.unpublished_comment.guid,
                        "pendingModeration": True,
                        "comments": [],
                    },
                    {
                        "guid": self.published_comment.guid,
                        "pendingModeration": False,
                        "comments": [
                            {
                                "guid": self.unpublished_subcomment.guid,
                                "pendingModeration": True,
                            },
                            {
                                "guid": self.published_subcomment.guid,
                                "pendingModeration": False,
                            },
                        ],
                    },
                ],
            )

        def assertLimitedCommentResponse(self, result):
            self.assertEqual(
                result["data"]["entity"]["comments"],
                [
                    {
                        "guid": self.published_comment.guid,
                        "pendingModeration": False,
                        "comments": [
                            {
                                "guid": self.published_subcomment.guid,
                                "pendingModeration": False,
                            }
                        ],
                    },
                ],
            )

        def test_comment_in_moderation_as_respondent(self):
            self.graphql_client.force_login(self.respondent)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertCompleteCommentResponse(result)

        def test_comment_in_moderation_as_administrator(self):
            self.graphql_client.force_login(self.admin)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertCompleteCommentResponse(result)

        def test_comment_in_moderation_as_visitor(self):
            self.graphql_client.force_login(UserFactory())
            result = self.graphql_client.post(self.query, self.variables)

            self.assertLimitedCommentResponse(result)
