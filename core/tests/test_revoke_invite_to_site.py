from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class RevokeInviteToSiteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.mutation = """
            mutation InviteItem($input: revokeInviteToSiteInput!) {
                revokeInviteToSite(input: $input) {
                    success
                }
            }
        """
        self.variables = {"input": {"emailAddresses": ["a@a.nl", "b@b.nl", "c@c.nl"]}}

    def test_revoke_invite_to_site_by_admin(self):
        self.graphql_client.force_login(UserFactory(roles=[USER_ROLES.ADMIN]))
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.assertEqual(data["revokeInviteToSite"]["success"], True)

    def test_revoke_invite_to_site_by_user_admin(self):
        self.graphql_client.force_login(UserFactory(roles=[USER_ROLES.USER_ADMIN]))
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.assertEqual(data["revokeInviteToSite"]["success"], True)

    def test_revoke_invite_to_site_by_user(self):
        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_revoke_invite_to_site_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)
