from datetime import date

from core.models import ProfileField, SiteAccessRequest
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestSiteAccessRequestsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.admin = AdminFactory()

        self.access_request1 = SiteAccessRequest.objects.create(
            name="Alpha", email="test@example.com", accepted=True, processed=True
        )
        self.access_request2 = SiteAccessRequest.objects.create(
            name="Bravo", email="test2@example.com", accepted=False, processed=False
        )
        self.access_request3 = SiteAccessRequest.objects.create(
            name="Charly",
            email="test3@example.com",
            accepted=False,
            processed=True,
            reason="Verboden toegang",
        )

        self.user_created = UserFactory()

        self.access_request4 = SiteAccessRequest.objects.create(
            name="Delta",
            email="test4@example.com",
            accepted=True,
            processed=True,
            user_created=self.user_created,
        )

        self.query = """
        fragment RequestData on SiteAccessRequest {
            name,
            email,
            status,
            reason,
        }
        query SiteAccessRequest($status: SiteAccessStatus) {
            siteSettings {
                siteAccessRequests(status: $status) {
                    edges { ... RequestData }
                }
            }
        }
        """

    def test_query_site_access_requests_anonymously(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query)

    def test_query_site_access_requests_authenticated(self):
        self.graphql_client.force_login(UserFactory())
        result = self.graphql_client.post(self.query)
        self.assertIsNone(result["data"]["siteSettings"]["siteAccessRequests"])

    def test_query_site_access_requests_as_admin_unprocessed(self):
        self.graphql_client.force_login(AdminFactory())
        variables = {"status": "unprocessed"}
        result = self.graphql_client.post(self.query, variables)
        edges = result["data"]["siteSettings"]["siteAccessRequests"]["edges"]
        self.assertEqual(
            edges,
            [
                {
                    "name": "Bravo",
                    "email": "test2@example.com",
                    "status": "unprocessed",
                    "reason": None,
                }
            ],
        )

    def test_query_site_access_requests_as_admin_pending(self):
        self.graphql_client.force_login(AdminFactory())
        variables = {"status": "pending"}
        result = self.graphql_client.post(self.query, variables)
        edges = result["data"]["siteSettings"]["siteAccessRequests"]["edges"]
        self.assertEqual(
            edges,
            [
                {
                    "name": "Alpha",
                    "email": "test@example.com",
                    "status": "pending",
                    "reason": None,
                }
            ],
        )

    def test_query_site_access_requests_as_admin_accepted(self):
        self.graphql_client.force_login(AdminFactory())
        variables = {"status": "accepted"}
        result = self.graphql_client.post(self.query, variables)
        edges = result["data"]["siteSettings"]["siteAccessRequests"]["edges"]
        self.assertEqual(
            edges,
            [
                {
                    "name": "Delta",
                    "email": "test4@example.com",
                    "status": "accepted",
                    "reason": None,
                }
            ],
        )

    def test_query_site_access_requests_as_admin_rejected(self):
        self.graphql_client.force_login(AdminFactory())
        variables = {"status": "rejected"}
        result = self.graphql_client.post(self.query, variables)
        edges = result["data"]["siteSettings"]["siteAccessRequests"]["edges"]
        self.assertEqual(
            edges,
            [
                {
                    "name": "Charly",
                    "email": "test3@example.com",
                    "status": "rejected",
                    "reason": "Verboden toegang",
                }
            ],
        )


def _of(field_type, name, key, **kwargs):
    """OnboardingField"""
    return {
        **kwargs,
        "name": name,
        "key": key,
        "field_type": field_type,
        "is_in_onboarding": True,
    }


class TestSiteAccessRequestsWithOnboarding(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.textField = ProfileField.objects.create(
            **_of("text_field", "Name", key="f1")
        )
        self.selectField = ProfileField.objects.create(
            **_of(
                "select_field",
                "Type",
                key="f2",
                field_options=["Alpha", "Bravo", "Delta", "Hotel"],
            )
        )
        self.multiSelectField = ProfileField.objects.create(
            **_of(
                "multi_select_field",
                "Preference",
                key="f3",
                field_options=["Golf", "India", "Julliet", "Kilo", "Lima"],
            )
        )
        self.htmlField = ProfileField.objects.create(
            **_of("html_field", "Bio", key="f4")
        )
        self.dateField = ProfileField.objects.create(
            **_of("date_field", "A Date", key="f5")
        )
        self.access_request = SiteAccessRequest.objects.create(
            email="test@example.com",
            name="John Doodle",
            profile={
                self.textField.guid: "John Doe",
                self.selectField.guid: "Alpha",
                self.multiSelectField.guid: ["India", "Lima"],
                self.htmlField.guid: None,
                self.dateField.guid: date(2010, 5, 2).isoformat(),
            },
        )
        self.query = """
        fragment RequestData on SiteAccessRequest {
            name,
            email,
            status
            profileFields {
                name
                value
            }
        }
        query SiteAccessRequest {
            siteSettings {
                requests: siteAccessRequests {
                    edges { ... RequestData }
                }
            }
        }
        """

    def _update_access_request(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self.access_request, key, value)
        self.access_request.save()

    def test_site_access_requests(self):
        self.graphql_client.force_login(AdminFactory())
        response = self.graphql_client.post(self.query)
        requests = response["data"]["siteSettings"]["requests"]["edges"]
        self.assertEqual(
            requests,
            [
                {
                    "name": "John Doodle",
                    "email": "test@example.com",
                    "status": "unprocessed",
                    "profileFields": [
                        {"name": "Name", "value": "John Doe"},
                        {"name": "Type", "value": "Alpha"},
                        {"name": "Preference", "value": "India,Lima"},
                        {"name": "Bio", "value": ""},
                        {"name": "A Date", "value": "2010-05-02"},
                    ],
                }
            ],
        )
