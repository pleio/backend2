from core import config
from core.models import ProfileField, UserProfile
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class SiteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=[])
        self.user = UserFactory()

        self.query = """
            query testSite {
                site {
                    guid
                    name
                    theme
                    menu {
                        label
                        link
                        access
                        children {
                            label
                            link
                            access
                        }
                    }
                    menuState
                    profile {
                        key
                        name
                        fieldType
                        isFilter
                        isFilterable
                    }
                    cancelMembershipEnabled
                    footer {
                        title
                        link
                    }
                    directLinks {
                        title
                        link
                    }
                    readAccessIds { id, description }
                    writeAccessIds { id, description }
                    defaultReadAccessId
                    defaultWriteAccessId
                    language
                    languageOptions { value, label }
                    contentTranslation
                    logo
                    logoAlt
                    icon
                    iconAlt
                    showIcon
                    startpage
                    showLeader
                    showLeaderButtons
                    subtitle
                    leaderImage
                    showInitiative
                    initiativeTitle
                    initiativeImage
                    initiativeImageAlt
                    initiativeDescription
                    initiatorLink
                    style {
                        fontHeading
                        fontBody
                        colorPrimary
                        colorSecondary
                        colorHeader
                    }
                    customTagsAllowed
                    tagCategories {
                        values
                    }
                    activityFilter {
                        contentTypes {
                            key
                            value
                        }
                    }
                    entityFilter {
                        contentTypes {
                            key
                            value
                        }
                    }
                    searchFilter {
                        contentTypes {
                            key
                            value
                        }
                    }
                    showExtraHomepageFilters
                    showTagsInFeed
                    showTagsInDetail
                    showCustomTagsInFeed
                    showCustomTagsInDetail
                    usersOnline
                    profileFields {
                        key
                    }
                    editUserNameEnabled
                    sitePlanName
                    sitePlanType
                    commentWithoutAccountEnabled
                    questionLockAfterActivity
                    questionLockAfterActivityLink
                    maxCharactersInAbstract
                    showSuggestedItems
                    collabEditingEnabled
                    preserveFileExif
                    scheduleAppointmentEnabled
                    videocallEnabled
                    videocallProfilepage
                    blockedUserIntroMessage
                    pageTagFilters(contentType: "blog") {
                        contentType
                        showTagFilter
                        showTagCategories
                    }
                    recurringEventsEnabled
                    chatEnabled
                    searchPublishedFilterEnabled
                    searchArchiveOption
                    showOriginalFileName
                    integratedVideocallEnabled
                    integratedVideocallUrl
                    integratedTokenOnlyVideocallUrl
                    autoMembershipProfileFields { key }
                    contentModerationEnabled
                    requireContentModerationFor
                    commentModerationEnabled
                    requireCommentModerationFor

                }
            }
        """

    def test_site(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})
        self.maxDiff = None

        data = result["data"]
        self.assertEqual(data["site"]["name"], config.NAME)
        self.assertEqual(data["site"]["language"], "nl")
        self.assertEqual(
            data["site"]["languageOptions"], [{"label": "Nederlands", "value": "nl"}]
        )
        self.assertEqual(data["site"]["contentTranslation"], False)
        self.assertEqual(data["site"]["guid"], "1")
        self.assertEqual(data["site"]["theme"], config.THEME)
        self.assertEqual(data["site"]["menu"], config.MENU)
        self.assertEqual(data["site"]["menuState"], config.MENU_STATE)
        self.assertEqual(data["site"]["style"]["fontHeading"], config.FONT)
        self.assertEqual(data["site"]["style"]["fontBody"], config.FONT)
        self.assertEqual(data["site"]["style"]["colorPrimary"], config.COLOR_PRIMARY)
        self.assertEqual(
            data["site"]["style"]["colorSecondary"], config.COLOR_SECONDARY
        )
        self.assertEqual(data["site"]["showTagsInFeed"], config.SHOW_TAGS_IN_FEED)
        self.assertEqual(data["site"]["showTagsInDetail"], config.SHOW_TAGS_IN_DETAIL)
        self.assertEqual(
            data["site"]["showCustomTagsInFeed"], config.SHOW_CUSTOM_TAGS_IN_FEED
        )
        self.assertEqual(
            data["site"]["showCustomTagsInDetail"], config.SHOW_CUSTOM_TAGS_IN_DETAIL
        )

        self.assertEqual(
            data["site"]["readAccessIds"],
            [
                {"id": 0, "description": "Alleen eigenaar"},
                {"id": 1, "description": "Ingelogde gebruikers"},
            ],
        )
        self.assertEqual(
            data["site"]["writeAccessIds"],
            [
                {"id": 0, "description": "Alleen eigenaar"},
                {"id": 1, "description": "Ingelogde gebruikers"},
            ],
        )
        self.assertEqual(data["site"]["defaultReadAccessId"], 1)
        self.assertEqual(data["site"]["defaultWriteAccessId"], 0)

        self.assertEqual(data["site"]["profileFields"], [])

        self.assertEqual(data["site"]["sitePlanName"], "")
        self.assertEqual(data["site"]["sitePlanType"], "")

        self.assertEqual(
            data["site"]["editUserNameEnabled"], config.EDIT_USER_NAME_ENABLED
        )
        self.assertEqual(
            data["site"]["commentWithoutAccountEnabled"],
            config.COMMENT_WITHOUT_ACCOUNT_ENABLED,
        )
        self.assertEqual(
            data["site"]["questionLockAfterActivity"],
            config.QUESTION_LOCK_AFTER_ACTIVITY,
        )
        self.assertEqual(
            data["site"]["questionLockAfterActivityLink"],
            config.QUESTION_LOCK_AFTER_ACTIVITY_LINK,
        )
        self.assertEqual(
            data["site"]["maxCharactersInAbstract"], config.MAX_CHARACTERS_IN_ABSTRACT
        )
        self.assertEqual(
            data["site"]["showSuggestedItems"], config.SHOW_SUGGESTED_ITEMS
        )
        self.assertEqual(
            data["site"]["collabEditingEnabled"], config.COLLAB_EDITING_ENABLED
        )
        self.assertEqual(data["site"]["preserveFileExif"], config.PRESERVE_FILE_EXIF)
        self.assertEqual(data["site"]["blockedUserIntroMessage"], "")

        self.assertDictEqual(
            data["site"]["activityFilter"],
            {
                "contentTypes": [
                    {"key": "event", "value": "Agenda-item"},
                    {"key": "blog", "value": "Blog"},
                    {"key": "discussion", "value": "Discussie"},
                    {"key": "magazine_issue", "value": "Magazine uitgave"},
                    {"key": "news", "value": "Nieuws"},
                    {"key": "page", "value": "Pagina"},
                    {"key": "episode", "value": "Podcast aflevering"},
                    {"key": "statusupdate", "value": "Status-update"},
                    {"key": "question", "value": "Vraag"},
                    {"key": "wiki", "value": "Wiki"},
                ]
            },
        )
        self.assertDictEqual(
            data["site"]["entityFilter"],
            {
                "contentTypes": [
                    {"key": "event", "value": "Agenda-item"},
                    {"key": "file", "value": "Bestand"},
                    {"key": "blog", "value": "Blog"},
                    {"key": "discussion", "value": "Discussie"},
                    {"key": "magazine", "value": "Magazine"},
                    {"key": "magazine_issue", "value": "Magazine uitgave"},
                    {"key": "folder", "value": "Map"},
                    {"key": "news", "value": "Nieuws"},
                    {"key": "pad", "value": "Pad"},
                    {"key": "page", "value": "Pagina"},
                    {"key": "podcast", "value": "Podcast"},
                    {"key": "episode", "value": "Podcast aflevering"},
                    {"key": "question", "value": "Vraag"},
                    {"key": "wiki", "value": "Wiki"},
                ]
            },
        )
        self.assertDictEqual(
            data["site"]["searchFilter"],
            {
                "contentTypes": [
                    {"key": "image", "value": "Afbeelding"},
                    {"key": "event", "value": "Agenda-item"},
                    {"key": "file", "value": "Bestand"},
                    {"key": "blog", "value": "Blog"},
                    {"key": "discussion", "value": "Discussie"},
                    {"key": "user", "value": "Gebruiker"},
                    {"key": "group", "value": "Groep"},
                    {"key": "magazine_issue", "value": "Magazine uitgave"},
                    {"key": "folder", "value": "Map"},
                    {"key": "news", "value": "Nieuws"},
                    {"key": "pad", "value": "Pad"},
                    {"key": "page", "value": "Pagina"},
                    {"key": "podcast", "value": "Podcast"},
                    {"key": "episode", "value": "Podcast aflevering"},
                    {"key": "question", "value": "Vraag"},
                    {"key": "wiki", "value": "Wiki"},
                ]
            },
        )
        self.assertEqual(
            data["site"]["pageTagFilters"],
            {"contentType": "blog", "showTagFilter": True, "showTagCategories": []},
        )
        self.assertEqual(data["site"]["recurringEventsEnabled"], False)
        self.assertEqual(data["site"]["chatEnabled"], False)
        self.assertEqual(data["site"]["integratedVideocallEnabled"], False)
        self.assertEqual(data["site"]["showOriginalFileName"], False)
        self.assertEqual(data["site"]["searchPublishedFilterEnabled"], True)
        self.assertEqual(data["site"]["searchArchiveOption"], "nobody")
        self.assertEqual(data["site"]["autoMembershipProfileFields"], [])

        self.assertEqual(data["site"]["contentModerationEnabled"], False)
        self.assertEqual(data["site"]["requireContentModerationFor"], [])
        self.assertEqual(data["site"]["commentModerationEnabled"], False)
        self.assertEqual(data["site"]["requireCommentModerationFor"], [])

    def test_page_tag_filters(self):
        self.override_config(
            PAGE_TAG_FILTERS=[
                {
                    "showTagFilter": False,
                    "showTagCategories": [],
                    "contentType": "blog",
                },
                {
                    "showTagFilter": False,
                    "showTagCategories": [],
                    "contentType": "news",
                },
            ]
        )

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})
        data = result["data"]
        self.assertEqual(
            data["site"]["pageTagFilters"],
            {"showTagFilter": False, "showTagCategories": [], "contentType": "blog"},
        )

    def test_site_open(self):
        self.override_config(IS_CLOSED=False)
        self.override_config(DEFAULT_ACCESS_ID=2)

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})

        data = result["data"]
        self.assertEqual(
            data["site"]["readAccessIds"],
            [
                {"id": 0, "description": "Alleen eigenaar"},
                {"id": 1, "description": "Ingelogde gebruikers"},
                {"id": 2, "description": "Iedereen (publiek zichtbaar)"},
            ],
        )
        self.assertEqual(
            data["site"]["writeAccessIds"],
            [
                {"id": 0, "description": "Alleen eigenaar"},
                {"id": 1, "description": "Ingelogde gebruikers"},
            ],
        )
        self.assertEqual(data["site"]["defaultReadAccessId"], 2)
        self.assertEqual(data["site"]["defaultWriteAccessId"], 0)

    def test_schedule_appointment_enabled(self):
        self.override_config(ONLINEAFSPRAKEN_ENABLED=True)

        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["site"]["scheduleAppointmentEnabled"], True)

    def test_schedule_appointment_disabled(self):
        self.override_config(ONLINEAFSPRAKEN_ENABLED=False)

        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["site"]["scheduleAppointmentEnabled"], False)

    def test_videocall_enabled(self):
        self.override_config(VIDEOCALL_ENABLED=True)

        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["site"]["videocallEnabled"], True)

    def test_videocall_disabled(self):
        self.override_config(VIDEOCALL_ENABLED=False)

        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["site"]["videocallEnabled"], False)

    def test_videocall_profilepage_enabled(self):
        self.override_config(VIDEOCALL_PROFILEPAGE=True)

        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["site"]["videocallProfilepage"], True)

    def test_videocall_profilepage_disabled(self):
        self.override_config(VIDEOCALL_PROFILEPAGE=False)

        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["site"]["videocallProfilepage"], False)

    def test_with_pad_enabled(self):
        self.override_config(COLLAB_EDITING_ENABLED=True)
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})

        self.maxDiff = None

        data = result["data"]
        self.assertDictEqual(
            data["site"]["entityFilter"],
            {
                "contentTypes": [
                    {"key": "event", "value": "Agenda-item"},
                    {"key": "file", "value": "Bestand"},
                    {"key": "blog", "value": "Blog"},
                    {"key": "discussion", "value": "Discussie"},
                    {"key": "magazine", "value": "Magazine"},
                    {"key": "magazine_issue", "value": "Magazine uitgave"},
                    {"key": "folder", "value": "Map"},
                    {"key": "news", "value": "Nieuws"},
                    {"key": "pad", "value": "Pad"},
                    {"key": "page", "value": "Pagina"},
                    {"key": "podcast", "value": "Podcast"},
                    {"key": "episode", "value": "Podcast aflevering"},
                    {"key": "question", "value": "Vraag"},
                    {"key": "wiki", "value": "Wiki"},
                ]
            },
        )
        self.assertDictEqual(
            data["site"]["searchFilter"],
            {
                "contentTypes": [
                    {"key": "image", "value": "Afbeelding"},
                    {"key": "event", "value": "Agenda-item"},
                    {"key": "file", "value": "Bestand"},
                    {"key": "blog", "value": "Blog"},
                    {"key": "discussion", "value": "Discussie"},
                    {"key": "user", "value": "Gebruiker"},
                    {"key": "group", "value": "Groep"},
                    {"key": "magazine_issue", "value": "Magazine uitgave"},
                    {"key": "folder", "value": "Map"},
                    {"key": "news", "value": "Nieuws"},
                    {"key": "pad", "value": "Pad"},
                    {"key": "page", "value": "Pagina"},
                    {"key": "podcast", "value": "Podcast"},
                    {"key": "episode", "value": "Podcast aflevering"},
                    {"key": "question", "value": "Vraag"},
                    {"key": "wiki", "value": "Wiki"},
                ]
            },
        )

    def test_with_some_content_types_excluded_from_search(self):
        self.override_config(
            SEARCH_EXCLUDED_CONTENT_TYPES=[
                "discussion",
                "episode",
                "event",
                "file",
                "folder",
                "group",
                "image",
                "magazine_issue",
                "news",
                "pad",
                "page",
                "podcast",
                "question",
                "user",
                "wiki",
            ]
        )
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})

        search_filter = result["data"]["site"]["searchFilter"]
        self.assertDictEqual(
            search_filter,
            {
                "contentTypes": [
                    {"key": "blog", "value": "Blog"},
                ]
            },
        )

    def test_with_blocked_intro_message(self):
        INTRO_MESSAGE = "another blockedUserIntroMessage"
        self.override_config(BLOCKED_USER_INTRO_MESSAGE=INTRO_MESSAGE)
        self.query = """
            query SiteGeneralSettings {
                site {
                    blockedUserIntroMessage
                }
            }
        """
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["blockedUserIntroMessage"], INTRO_MESSAGE)

    def test_recurring_events_enabled(self):
        self.override_config(RECURRING_EVENTS_ENABLED=True)
        self.query = """
            query SiteGeneralSettings {
                site {
                    recurringEventsEnabled
                }
            }
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["recurringEventsEnabled"], True)

    def test_chat_enabled(self):
        self.override_config(CHAT_ENABLED=True)
        self.query = """
            query SiteGeneralSettings {
                site {
                    chatEnabled
                }
            }
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["chatEnabled"], True)

    def test_integrated_videocall_enabled(self):
        self.override_config(INTEGRATED_VIDEOCALL_ENABLED=True)
        self.override_setting(INTEGRATED_VIDEO_CALL_URL="https://test.url")
        self.override_setting(
            INTEGRATED_TOKEN_ONLY_VIDEO_CALL_URL="https://test-token-only.url"
        )
        self.override_config()
        self.query = """
            query SiteGeneralSettings {
                site {
                    integratedVideocallEnabled
                    integratedVideocallUrl
                    integratedTokenOnlyVideocallUrl
                }
            }
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["integratedVideocallEnabled"], True)
        self.assertEqual(response["site"]["integratedVideocallUrl"], "https://test.url")
        self.assertEqual(
            response["site"]["integratedTokenOnlyVideocallUrl"],
            "https://test-token-only.url",
        )

    def test_search_published_filter_disabled(self):
        self.override_config(SEARCH_PUBLISHED_FILTER_ENABLED=False)
        self.query = """
            query SiteGeneralSettings {
                site {
                    searchPublishedFilterEnabled
                }
            }
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["searchPublishedFilterEnabled"], False)

    def test_other_search_archive_option(self):
        self.override_config(SEARCH_ARCHIVE_OPTION="everyone")
        self.query = """
            query SiteGeneralSettings {
                site {
                    searchArchiveOption
                }
            }
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["searchArchiveOption"], "everyone")

    def test_font_properties(self):
        self.override_config(FONT="fontHeading", FONT_BODY="fontBody")
        self.query = """
            query SiteGeneralSettings {
                site {
                    style {
                        fontHeading
                        fontBody
                    }
                }
            }
        """
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(
            response["site"]["style"],
            {"fontHeading": "fontHeading", "fontBody": "fontBody"},
        )

    def test_language_options(self):
        self.override_config(EXTRA_LANGUAGES=["en", "de", "fr", "nl"])
        result = self.graphql_client.post(self.query)

        self.assertEqual(
            result["data"]["site"]["languageOptions"],
            [
                {"value": "nl", "label": "Nederlands"},
                {"value": "en", "label": "English"},
                {"value": "de", "label": "Deutsch"},
                {"value": "fr", "label": "Français"},
            ],
        )


class TestProfileFieldSiteQueryTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()

        self.profileField1 = ProfileField.objects.create(
            key="text_key1", name="text_name", field_type="text_field"
        )
        self.profileField2 = ProfileField.objects.create(
            key="text_key2", name="text_name", field_type="date_field"
        )
        self.profileField3 = ProfileField.objects.create(
            key="text_key3",
            name="text_name",
            field_type="select_field",
            is_in_auto_group_membership=True,
        )

        self.override_config(
            PROFILE_SECTIONS=[
                {
                    "name": "section_one",
                    "profileFieldGuids": [
                        self.profileField1.guid,
                        self.profileField2.guid,
                    ],
                }
            ]
        )

        self.query = """
            fragment fieldFragment on ProfileItem {
                key
                name
                category
                fieldType
            }
            query testSite {
                site {
                    profileFields { ...fieldFragment }
                    autoMembershipProfileFields { ...fieldFragment }
                }
            }
        """

    def test_profile_fields(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})

        self.assertEqual(
            result["data"]["site"]["profileFields"],
            [
                {
                    "key": "text_key1",
                    "name": "text_name",
                    "category": "section_one",
                    "fieldType": "textField",
                },
                {
                    "key": "text_key2",
                    "name": "text_name",
                    "category": "section_one",
                    "fieldType": "dateField",
                },
            ],
        )

    def test_auto_membership_profile_fields(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})

        self.assertEqual(
            result["data"]["site"]["autoMembershipProfileFields"],
            [
                {
                    "key": "text_key3",
                    "name": "text_name",
                    "category": None,
                    "fieldType": "selectField",
                }
            ],
        )


class TestSiteOverridesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=False)
        self.query = """
        query Site {
            site {
                %s
            }
        }
        """

    def test_content_translation_enabled(self):
        self.override_config(CONTENT_TRANSLATION=True)
        response = self.graphql_client.post(self.query % "contentTranslation")
        self.assertEqual(response["data"]["site"]["contentTranslation"], True)


class TestSiteMenuTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            MENU=[
                {"label": "Home", "link": "/", "access": "public"},
                {
                    "label": "Blogs",
                    "link": "/blogs",
                    "access": "loggedIn",
                },
                {
                    "label": "Pages",
                    "link": "/pages",
                    "access": "administrator",
                },
                {
                    "label": "How to log in",
                    "access": "public",
                    "children": [
                        {
                            "label": "When you're not logged in yet",
                            "link": "/how-to-log-in",
                            "access": "publicOnly",
                        },
                        {
                            "label": "When you already have logged in",
                            "link": "/how-to-log-in",
                            "access": "loggedIn",
                        },
                        {
                            "label": "When your name is administrator",
                            "link": "/how-to-log-in",
                            "access": "administrator",
                        },
                    ],
                },
            ]
        )
        self.query = """
        query Site {
            site {
                menu {
                    label
                    link
                    access
                    children {
                        label
                        link
                        access
                        children {
                            label
                            link
                            access
                        }
                    }
                }
            }
        }
        """

    def test_menu_as_anonymous(self):
        result = self.graphql_client.post(self.query)
        self.assertEqual(
            result["data"]["site"]["menu"],
            [
                {"label": "Home", "link": "/", "access": "public", "children": None},
                {
                    "label": "How to log in",
                    "access": "public",
                    "link": None,
                    "children": [
                        {
                            "label": "When you're not logged in yet",
                            "link": "/how-to-log-in",
                            "access": "publicOnly",
                            "children": None,
                        },
                    ],
                },
            ],
        )

    def test_menu_as_logged_in(self):
        self.graphql_client.force_login(UserFactory())
        result = self.graphql_client.post(self.query)
        self.assertEqual(
            result["data"]["site"]["menu"],
            [
                {"label": "Home", "link": "/", "access": "public", "children": None},
                {
                    "label": "Blogs",
                    "link": "/blogs",
                    "access": "loggedIn",
                    "children": None,
                },
                {
                    "label": "How to log in",
                    "access": "public",
                    "link": None,
                    "children": [
                        {
                            "label": "When you already have logged in",
                            "link": "/how-to-log-in",
                            "access": "loggedIn",
                            "children": None,
                        },
                    ],
                },
            ],
        )

    def test_menu_as_administrator(self):
        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(self.query)
        self.assertEqual(
            result["data"]["site"]["menu"],
            [
                {"label": "Home", "link": "/", "access": "public", "children": None},
                {
                    "label": "Blogs",
                    "link": "/blogs",
                    "access": "loggedIn",
                    "children": None,
                },
                {
                    "label": "Pages",
                    "link": "/pages",
                    "access": "administrator",
                    "children": None,
                },
                {
                    "label": "How to log in",
                    "access": "public",
                    "link": None,
                    "children": [
                        {
                            "label": "When you already have logged in",
                            "link": "/how-to-log-in",
                            "access": "loggedIn",
                            "children": None,
                        },
                        {
                            "label": "When your name is administrator",
                            "link": "/how-to-log-in",
                            "access": "administrator",
                            "children": None,
                        },
                    ],
                },
            ],
        )


class TestMenuViaSiteQueryTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            CONTENT_TRANSLATION=True,
            LANGUAGE="nl",
            EXTRA_LANGUAGES=["en"],
            MENU=[
                {
                    "translations": [
                        {
                            "language": "nl",
                            "label": "Dit moet je lezen",
                        },
                        {
                            "language": "en",
                            "label": "Vertaling van 'Dit moet je lezen'",
                        },
                    ],
                    "access": "public",
                    "children": [
                        {
                            "translations": [
                                {
                                    "language": "en",
                                    "label": "Vertaling van 'Eerste blog'",
                                },
                                {
                                    "language": "nl",
                                    "label": "Eerste blog",
                                },
                            ],
                            "link": "/blog1",
                            "access": "public",
                        },
                        {"label": "Tweede blog", "link": "/blog2"},
                    ],
                },
            ],
        )

        self.query = """
        fragment MenuFragment on MenuItem {
            label
            localLabel
        }
        query Site {
            site {
                menu {
                    ...MenuFragment
                    children {
                        ...MenuFragment
                    }
                }
            }
        }
        """

    def assertUntranslatedResult(self, result):
        self.assertEqual(
            result["data"]["site"]["menu"],
            [
                {
                    "label": "Dit moet je lezen",
                    "localLabel": None,
                    "children": [
                        {
                            "label": "Eerste blog",
                            "localLabel": None,
                        },
                        {
                            "label": "Tweede blog",
                            "localLabel": None,
                        },
                    ],
                },
            ],
        )

    def assertTranslatedResult(self, result):
        self.assertEqual(
            result["data"]["site"]["menu"],
            [
                {
                    "label": "Dit moet je lezen",
                    "localLabel": "Vertaling van 'Dit moet je lezen'",
                    "children": [
                        {
                            "label": "Eerste blog",
                            "localLabel": "Vertaling van 'Eerste blog'",
                        },
                        {
                            "label": "Tweede blog",
                            "localLabel": None,
                        },
                    ],
                },
            ],
        )

    def create_user(self, language):
        user = UserFactory()
        UserProfile.objects.filter(user_id=user.id).update(language=language)
        user.refresh_from_db()
        return user

    def test_menu(self):
        self.graphql_client.force_login(self.create_user(None))
        result = self.graphql_client.post(self.query)
        self.assertUntranslatedResult(result)

    def test_menu_as_anonumous(self):
        result = self.graphql_client.post(self.query)
        self.assertUntranslatedResult(result)

    def test_menu_with_translation(self):
        self.graphql_client.force_login(self.create_user("en"))
        result = self.graphql_client.post(self.query)
        self.assertTranslatedResult(result)

    def test_menu_with_translation_disabled(self):
        self.override_config(CONTENT_TRANSLATION=False)
        self.graphql_client.force_login(self.create_user("en"))
        result = self.graphql_client.post(self.query)
        self.assertTranslatedResult(result)
