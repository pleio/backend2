from django.contrib.auth.models import AnonymousUser

from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import CampagnePageFactory
from entities.cms.models import Page
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    UserFactory,
    UserManagerFactory,
)


class TestFooterPageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(FOOTER_PAGE_ENABLED=True)

        self.query = """
        query FooterPage{
            siteSettings {
                footerPage {
                    guid
                    pageType
                }
            }
        }
        """

    def test_get_footer_page_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query)

    def test_get_footer_page_unprivileged(self):
        for user in [
            UserFactory(email="user"),
            EditorFactory(email="editor"),
            UserManagerFactory(email="user_manager"),
            NewsEditorFactory(email="news_editor"),
        ]:
            with self.subTest(msg=str(user)):
                self.graphql_client.force_login(user)
                response = self.graphql_client.post(self.query)

                site_settings = response["data"]["siteSettings"]
                self.assertIsNone(site_settings["footerPage"])
                self.assertFalse(Page.objects.exists())

    def test_get_footer_page_privileged(self):
        self.graphql_client.force_login(AdminFactory())
        response = self.graphql_client.post(self.query)

        page = Page.objects.first()
        self.assertIsNotNone(page)
        self.assertTrue(page.system_setting)

        site_settings = response["data"]["siteSettings"]
        self.assertIsNotNone(site_settings["footerPage"])
        self.assertEqual(site_settings["footerPage"]["guid"], page.guid)


class TestFooterRowsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.query = """
        query FooterRows{
            site {
                footerPageEnabled
                footerRows {
                    columns {
                        widgets { type }
                    }
                }
            }
        }
        """

        self.expected_rows = [
            {"columns": [{"widgets": [{"type": "text"}]}]},
        ]

        self.page = CampagnePageFactory(
            owner=AdminFactory(), row_repository=self.expected_rows
        )
        self.override_config(FOOTER_PAGE=self.page.guid)

    def get_footer_rows(self, user=None):
        if user:
            self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.query)
        return response["data"]["site"]

    def test_all_users_access_to_the_footer_rows(self):
        """
        All kinds of users should have access to the footerRows property.
        """
        for user_factory in [
            AnonymousUser,
            UserFactory,
            EditorFactory,
            UserManagerFactory,
            NewsEditorFactory,
            AdminFactory,
        ]:
            user = user_factory()
            with self.subTest(
                msg="Open site with %s and footerpage enabled" % str(user_factory)
            ):
                self.override_config(IS_CLOSED=False, FOOTER_PAGE_ENABLED=True)
                result = self.get_footer_rows(user)
                self.assertEqual(result["footerRows"], self.expected_rows)
                self.assertTrue(result["footerPageEnabled"])

            with self.subTest(
                msg="Open site with %s and footerpage disabled" % str(user_factory)
            ):
                self.override_config(IS_CLOSED=False, FOOTER_PAGE_ENABLED=False)
                result = self.get_footer_rows(user)
                self.assertEqual(result["footerRows"], self.expected_rows)
                self.assertFalse(result["footerPageEnabled"])

            with self.subTest(
                msg="Closed site with %s and footerpage enabled" % str(user_factory)
            ):
                self.override_config(IS_CLOSED=True, FOOTER_PAGE_ENABLED=True)
                result = self.get_footer_rows(user)
                self.assertEqual(result["footerRows"], self.expected_rows)
                self.assertTrue(result["footerPageEnabled"])

            with self.subTest(
                msg="Closed site with %s and footerpage disabled" % str(user_factory)
            ):
                self.override_config(IS_CLOSED=True, FOOTER_PAGE_ENABLED=False)
                result = self.get_footer_rows(user)
                self.assertEqual(result["footerRows"], self.expected_rows)
                self.assertFalse(result["footerPageEnabled"])
