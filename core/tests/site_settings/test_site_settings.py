import uuid
from unittest import mock

from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import USER_ROLES
from core.lib import get_language_options
from core.models import ProfileField, Setting, SiteInvitation
from core.tests.helpers import PleioTenantTestCase, override_config
from entities.cms.models import Page
from user.factories import AdminFactory, UserFactory


def time_factory(**kwargs):
    return timezone.localtime() + timezone.timedelta(**kwargs)


class TestSiteSettingsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.tenant.administrative_details["site_owner"] = "Demo"
        self.tenant.administrative_details["site_owner_email"] = "demo@example.com"
        self.tenant.save()

        self.user = UserFactory(is_delete_requested=False)
        self.admin = UserFactory(roles=["ADMIN"], is_delete_requested=False)
        self.delete_user = UserFactory(
            email="deleted@example.com", is_delete_requested=True
        )
        self.cmsPage1 = mixer.blend(Page, title="Z title")
        self.cmsPage2 = mixer.blend(Page, title="A title")
        self.profileField1 = ProfileField.objects.create(
            key="text_key1", name="text_name", field_type="text_field"
        )
        self.profileField2 = ProfileField.objects.create(
            key="text_key2", name="text_name", field_type="text_field"
        )
        self.siteInvitation = mixer.blend(SiteInvitation, email="a@a.nl")

        self.query = """
            query SiteGeneralSettings {
                siteSettings {
                    languageOptions {
                        value
                        label
                    }
                    language
                    extraLanguages
                    name
                    isClosed
                    allowRegistration
                    directRegistrationDomains
                    defaultReadAccessIdOptions {
                        value
                        label
                    }
                    defaultReadAccessId
                    searchEngineIndexingEnabled
                    piwikUrl
                    piwikId

                    themeOptions {
                        value
                        label
                    }
                    fontOptions {
                        value
                        label
                    }
                    fontHeading
                    fontBody
                    colorHeader
                    colorPrimary
                    colorSecondary
                    theme
                    logo
                    logoAlt
                    favicon
                    likeIcon

                    startPageOptions {
                        value
                        label
                    }
                    startPage
                    startPageCmsOptions {
                        value
                        label
                    }
                    startPageCms
                    anonymousStartPage
                    anonymousStartPageCms
                    showIcon
                    icon
                    menu {
                        label
                        link
                        access
                        children {
                            label
                            link
                            access
                        }
                    }

                    numberOfFeaturedItems
                    enableFeedSorting
                    showExtraHomepageFilters
                    showLeader
                    showLeaderButtons
                    subtitle
                    leaderImage
                    showInitiative
                    initiativeTitle
                    initiativeDescription
                    initiativeImage
                    initiativeImageAlt
                    initiativeLink
                    directLinks {
                        title
                        link
                    }
                    footer {
                        title
                        link
                    }
                    redirects {
                        source
                        destination
                    }
                    profile {
                        key
                        name
                        isFilterable
                        isFilter
                    }

                    profileFields {
                        key
                    }

                    showTagsInFeed
                    showTagsInDetail
                    showCustomTagsInFeed
                    showCustomTagsInDetail

                    defaultEmailOverviewFrequencyOptions {
                        value
                        label
                    }
                    defaultEmailOverviewFrequency
                    emailOverviewSubject
                    emailOverviewTitle
                    emailOverviewIntro
                    emailNotificationShowExcerpt

                    exportableUserFields {
                        field_type
                        field
                        label
                    }

                    exportableContentTypes {
                        value
                        label
                    }

                    showLoginRegister
                    customTagsAllowed
                    showUpDownVoting
                    enableSharing
                    showViewsCount
                    newsletter
                    cancelMembershipEnabled
                    showExcerptInNewsCard
                    commentsOnNews
                    eventExport
                    eventTiles
                    questionerCanChooseBestAnswer
                    statusUpdateGroups
                    subgroups
                    groupMemberExport
                    showSuggestedItems

                    onboardingEnabled
                    onboardingForceExistingUsers
                    onboardingIntro
                    siteInvites {
                        edges {
                            email
                        }
                    }
                    cookieConsent
                    roleOptions {
                        value
                        label
                    }
                    deleteAccountRequests {
                        edges {
                            guid
                        }
                    }

                    profileSyncEnabled
                    profileSyncToken

                    customCss
                    walledGardenByIpEnabled
                    whitelistedIpRanges
                    siteMembershipAcceptedSubject
                    siteMembershipAcceptedIntro
                    siteMembershipDeniedSubject
                    siteMembershipDeniedIntro
                    idpId
                    idpName
                    autoApproveSSO

                    flowEnabled
                    flowSubtypes
                    flowAppUrl
                    flowToken
                    flowCaseId
                    flowUserGuid

                    commentWithoutAccountEnabled

                    kalturaVideoEnabled
                    kalturaVideoPartnerId
                    kalturaVideoPlayerId

                    pdfCheckerEnabled
                    securityText
                    securityTextPGP
                    securityTextRedirectEnabled
                    securityTextRedirectUrl
                    collabEditingEnabled
                    sitePlanName
                    sitePlanType
                    siteCategory
                    siteNotes
                    supportContractEnabled
                    supportContractHoursRemaining
                    searchPublishedFilterEnabled
                    searchArchiveOption
                    searchExcludedContentTypes
                    blockedUserIntroMessage

                    appointmentTypeVideocall {
                        name
                        hasVideocall
                    }

                    pushNotificationsEnabled

                    pageTagFilters {
                        showTagFilter
                        showTagCategories
                        contentType
                    }
                    hideContentOwner
                    hideAccessLevelSelect
                    recommendedType
                    siteOwnerName
                    siteOwnerEmail
                    showOriginalFileName
                    openForCreateContentTypes
                    contentModerationEnabled
                    requireContentModerationFor
                    commentModerationEnabled
                    requireCommentModerationFor
                    searchContentTypes { contentTypes { key, value } }
                }
            }
        """

        self.ALL_SEARCH_CONTENT_TYPES = [
            {"key": "image", "value": "Afbeelding"},
            {"key": "event", "value": "Agenda-item"},
            {"key": "file", "value": "Bestand"},
            {"key": "blog", "value": "Blog"},
            {"key": "discussion", "value": "Discussie"},
            {"key": "user", "value": "Gebruiker"},
            {"key": "group", "value": "Groep"},
            {"key": "magazine_issue", "value": "Magazine uitgave"},
            {"key": "folder", "value": "Map"},
            {"key": "news", "value": "Nieuws"},
            {"key": "pad", "value": "Pad"},
            {"key": "page", "value": "Pagina"},
            {"key": "podcast", "value": "Podcast"},
            {"key": "episode", "value": "Podcast aflevering"},
            {"key": "question", "value": "Vraag"},
            {"key": "wiki", "value": "Wiki"},
        ]

    def tearDown(self):
        super().tearDown()

    def test_site_settings_by_admin(self):
        self.override_config(
            IS_CLOSED=False,
            ALLOW_REGISTRATION=True,
            ANONYMOUS_START_PAGE="cms",
            ANONYMOUS_START_PAGE_CMS=self.cmsPage2.guid,
            PAGE_TAG_FILTERS=[
                {"showTagFilter": False, "showTagCategories": [], "contentType": "blog"}
            ],
            SITE_PLAN="basic",
        )
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {})

        self.maxDiff = None

        data = result["data"]
        self.assertEqual(data["siteSettings"]["name"], "Pleio 2.0")
        self.assertEqual(data["siteSettings"]["language"], "nl")
        self.assertEqual(data["siteSettings"]["extraLanguages"], [])
        self.assertEqual(
            data["siteSettings"]["languageOptions"], get_language_options()
        )
        self.assertEqual(data["siteSettings"]["isClosed"], False)
        self.assertEqual(data["siteSettings"]["allowRegistration"], True)
        self.assertEqual(data["siteSettings"]["directRegistrationDomains"], [])
        self.assertEqual(
            data["siteSettings"]["defaultReadAccessIdOptions"],
            [
                {"value": 0, "label": "Alleen eigenaar"},
                {"value": 1, "label": "Ingelogde gebruikers"},
                {"value": 2, "label": "Iedereen (publiek zichtbaar)"},
            ],
        )
        self.assertEqual(data["siteSettings"]["defaultReadAccessId"], 1)
        self.assertEqual(data["siteSettings"]["searchEngineIndexingEnabled"], False)
        self.assertEqual(data["siteSettings"]["piwikUrl"], "https://stats.pleio.nl/")
        self.assertEqual(data["siteSettings"]["piwikId"], "")

        self.assertEqual(
            data["siteSettings"]["themeOptions"],
            [
                {"value": "leraar", "label": "Standaard"},
                {"value": "rijkshuisstijl", "label": "Rijkshuisstijl"},
            ],
        )
        self.assertEqual(
            data["siteSettings"]["fontOptions"],
            [
                {"value": "Arial", "label": "Arial"},
                {"value": "General Sans", "label": "General Sans"},
                {"value": "Montserrat", "label": "Montserrat"},
                {"value": "Open Sans", "label": "Open Sans"},
                {"value": "Palanquin", "label": "Palanquin"},
                {"value": "PT Sans", "label": "PT Sans"},
                {"value": "Rijksoverheid Sans", "label": "Rijksoverheid Sans"},
                {"value": "Roboto", "label": "Roboto"},
                {"value": "Source Sans Pro", "label": "Source Sans Pro"},
                {"value": "Source Serif Pro", "label": "Source Serif Pro"},
            ],
        )

        self.assertEqual(data["siteSettings"]["fontHeading"], "Rijksoverheid Sans")
        self.assertEqual(data["siteSettings"]["fontBody"], "Rijksoverheid Sans")
        self.assertEqual(data["siteSettings"]["colorHeader"], "#0e2f56")
        self.assertEqual(data["siteSettings"]["colorPrimary"], "#0e2f56")
        self.assertEqual(data["siteSettings"]["colorSecondary"], "#009ee3")
        self.assertEqual(data["siteSettings"]["theme"], "leraar")
        self.assertEqual(data["siteSettings"]["logo"], "")
        self.assertEqual(data["siteSettings"]["logoAlt"], "")
        self.assertEqual(data["siteSettings"]["favicon"], "")
        self.assertEqual(data["siteSettings"]["likeIcon"], "heart")

        self.assertEqual(data["siteSettings"]["anonymousStartPage"], "cms")
        self.assertEqual(
            data["siteSettings"]["anonymousStartPageCms"], self.cmsPage2.guid
        )
        self.assertEqual(
            data["siteSettings"]["startPageOptions"],
            [
                {"value": "activity", "label": "Activiteitenstroom"},
                {"value": "cms", "label": "Pagina"},
            ],
        )
        self.assertEqual(data["siteSettings"]["startPage"], "activity")
        self.assertEqual(
            data["siteSettings"]["startPageCmsOptions"],
            [
                {"value": self.cmsPage2.guid, "label": self.cmsPage2.title},
                {"value": self.cmsPage1.guid, "label": self.cmsPage1.title},
            ],
        )
        self.assertEqual(data["siteSettings"]["startPageCms"], "")
        self.assertEqual(data["siteSettings"]["showIcon"], False)
        self.assertIn("/static/icon", data["siteSettings"]["icon"])  # show default'':
        self.assertEqual(
            data["siteSettings"]["menu"],
            [
                {"link": "/blog", "label": "Blog", "children": [], "access": "public"},
                {
                    "link": "/news",
                    "label": "Nieuws",
                    "children": [],
                    "access": "public",
                },
                {
                    "link": "/groups",
                    "label": "Groepen",
                    "children": [],
                    "access": "public",
                },
                {
                    "link": "/questions",
                    "label": "Vragen",
                    "children": [],
                    "access": "public",
                },
                {"link": "/wiki", "label": "Wiki", "children": [], "access": "public"},
            ],
        )

        self.assertEqual(data["siteSettings"]["numberOfFeaturedItems"], 0)
        self.assertEqual(data["siteSettings"]["enableFeedSorting"], False)
        self.assertEqual(data["siteSettings"]["showExtraHomepageFilters"], True)
        self.assertEqual(data["siteSettings"]["showLeader"], False)
        self.assertEqual(data["siteSettings"]["showLeaderButtons"], False)
        self.assertEqual(data["siteSettings"]["subtitle"], "")
        self.assertEqual(data["siteSettings"]["leaderImage"], "")
        self.assertEqual(data["siteSettings"]["showInitiative"], False)
        self.assertEqual(data["siteSettings"]["initiativeTitle"], "")
        self.assertEqual(data["siteSettings"]["initiativeDescription"], "")
        self.assertEqual(data["siteSettings"]["initiativeImage"], "")
        self.assertEqual(data["siteSettings"]["initiativeImageAlt"], "")
        self.assertEqual(data["siteSettings"]["initiativeLink"], "")
        self.assertEqual(data["siteSettings"]["directLinks"], [])
        self.assertEqual(data["siteSettings"]["footer"], [])
        self.assertEqual(data["siteSettings"]["redirects"], [])

        self.assertEqual(data["siteSettings"]["profile"], [])
        self.assertEqual(
            data["siteSettings"]["profileFields"],
            [{"key": self.profileField1.key}, {"key": self.profileField2.key}],
        )

        self.assertEqual(data["siteSettings"]["showTagsInFeed"], False)
        self.assertEqual(data["siteSettings"]["showTagsInDetail"], False)
        self.assertEqual(data["siteSettings"]["showCustomTagsInFeed"], False)
        self.assertEqual(data["siteSettings"]["showCustomTagsInDetail"], False)

        self.assertEqual(
            data["siteSettings"]["defaultEmailOverviewFrequencyOptions"],
            [
                {"value": "daily", "label": "Dagelijks"},
                {"value": "weekly", "label": "Wekelijks"},
                {"value": "monthly", "label": "Maandelijks"},
                {"value": "never", "label": "Nooit"},
            ],
        )
        self.assertEqual(
            data["siteSettings"]["defaultEmailOverviewFrequency"], "weekly"
        )
        self.assertEqual(data["siteSettings"]["emailOverviewSubject"], "")
        self.assertEqual(data["siteSettings"]["emailOverviewTitle"], "Pleio 2.0")
        self.assertEqual(data["siteSettings"]["emailOverviewIntro"], "")
        self.assertEqual(data["siteSettings"]["emailNotificationShowExcerpt"], False)

        self.assertEqual(data["siteSettings"]["onboardingEnabled"], False)
        self.assertEqual(data["siteSettings"]["onboardingForceExistingUsers"], False)
        self.assertEqual(data["siteSettings"]["onboardingIntro"], "")

        self.assertEqual(
            data["siteSettings"]["exportableUserFields"][0]["field_type"], "userField"
        )
        self.assertEqual(
            data["siteSettings"]["exportableUserFields"][0]["field"], "guid"
        )
        self.assertEqual(
            data["siteSettings"]["exportableUserFields"][0]["label"], "guid"
        )
        self.assertEqual(
            data["siteSettings"]["exportableUserFields"][6]["field_type"], "userField"
        )
        self.assertEqual(
            data["siteSettings"]["exportableUserFields"][6]["field"],
            "group_memberships",
        )
        self.assertEqual(
            data["siteSettings"]["exportableUserFields"][6]["label"],
            "group_memberships",
        )

        self.assertEqual(
            data["siteSettings"]["exportableContentTypes"],
            [
                {"value": "statusupdate", "label": "Updates"},
                {"value": "blog", "label": "Blogs"},
                {"value": "page", "label": "Pagina's"},
                {"value": "discussion", "label": "Discussies"},
                {"value": "event", "label": "Agenda-items"},
                {"value": "file", "label": "Bestanden"},
                {"value": "magazine", "label": "Magazines"},
                {"value": "magazine_issue", "label": "Magazine uitgaven"},
                {"value": "news", "label": "Nieuws"},
                {"value": "poll", "label": "Polls"},
                {"value": "question", "label": "Vragen"},
                {"value": "task", "label": "Taken"},
                {"value": "wiki", "label": "Wiki pagina's"},
                {"value": "comment", "label": "Reacties"},
                {"value": "group", "label": "Groepen"},
                {"value": "podcast", "label": "Podcasts"},
                {"value": "episode", "label": "Podcast afleveringen"},
            ],
        )

        self.assertEqual(data["siteSettings"]["showLoginRegister"], True)
        self.assertEqual(data["siteSettings"]["customTagsAllowed"], True)
        self.assertEqual(data["siteSettings"]["showUpDownVoting"], True)
        self.assertEqual(data["siteSettings"]["enableSharing"], True)
        self.assertEqual(data["siteSettings"]["showViewsCount"], True)
        self.assertEqual(data["siteSettings"]["newsletter"], False)
        self.assertEqual(data["siteSettings"]["cancelMembershipEnabled"], True)
        self.assertEqual(data["siteSettings"]["showExcerptInNewsCard"], False)
        self.assertEqual(data["siteSettings"]["commentsOnNews"], False)
        self.assertEqual(data["siteSettings"]["eventExport"], False)
        self.assertEqual(data["siteSettings"]["eventTiles"], False)
        self.assertEqual(data["siteSettings"]["questionerCanChooseBestAnswer"], False)
        self.assertEqual(data["siteSettings"]["statusUpdateGroups"], True)
        self.assertEqual(data["siteSettings"]["subgroups"], False)
        self.assertEqual(data["siteSettings"]["groupMemberExport"], False)
        self.assertEqual(data["siteSettings"]["showSuggestedItems"], False)
        self.assertEqual(
            data["siteSettings"]["siteInvites"]["edges"][0]["email"], "a@a.nl"
        )
        self.assertEqual(data["siteSettings"]["cookieConsent"], False)
        self.assertEqual(
            data["siteSettings"]["roleOptions"],
            [
                {"value": "ADMIN", "label": "Beheerder"},
                {"value": "EDITOR", "label": "Redacteur"},
                {"value": "QUESTION_MANAGER", "label": "Vraagexpert"},
                {"value": "NEWS_EDITOR", "label": "Nieuwsredacteur"},
                {"value": "USER_ADMIN", "label": "Gebruikersbeheerder"},
                {"value": "REQUEST_MANAGER", "label": "Toegangsaanvraagbeheerder"},
            ],
        )
        self.assertEqual(
            data["siteSettings"]["deleteAccountRequests"]["edges"][0]["guid"],
            self.delete_user.guid,
        )
        self.assertEqual(data["siteSettings"]["profileSyncEnabled"], False)
        self.assertEqual(data["siteSettings"]["profileSyncToken"], "")
        self.assertEqual(data["siteSettings"]["customCss"], "")
        self.assertEqual(data["siteSettings"]["whitelistedIpRanges"], [])
        self.assertEqual(data["siteSettings"]["walledGardenByIpEnabled"], False)
        self.assertEqual(data["siteSettings"]["siteMembershipAcceptedSubject"], "")
        self.assertEqual(data["siteSettings"]["siteMembershipAcceptedIntro"], "")
        self.assertEqual(data["siteSettings"]["siteMembershipDeniedSubject"], "")
        self.assertEqual(data["siteSettings"]["siteMembershipDeniedIntro"], "")
        self.assertEqual(data["siteSettings"]["idpId"], "")
        self.assertEqual(data["siteSettings"]["idpName"], "")
        self.assertEqual(data["siteSettings"]["autoApproveSSO"], False)
        # TODO: remove after flow connects to general api
        self.assertEqual(data["siteSettings"]["flowEnabled"], False)
        self.assertEqual(data["siteSettings"]["flowSubtypes"], [])
        self.assertEqual(data["siteSettings"]["flowAppUrl"], "")
        self.assertEqual(data["siteSettings"]["flowToken"], "")
        self.assertEqual(data["siteSettings"]["flowCaseId"], None)
        self.assertEqual(data["siteSettings"]["flowUserGuid"], "")
        self.assertEqual(data["siteSettings"]["commentWithoutAccountEnabled"], False)
        self.assertEqual(data["siteSettings"]["kalturaVideoEnabled"], False)
        self.assertEqual(data["siteSettings"]["kalturaVideoPartnerId"], "")
        self.assertEqual(data["siteSettings"]["kalturaVideoPlayerId"], "")
        self.assertEqual(data["siteSettings"]["pdfCheckerEnabled"], True)
        self.assertEqual(data["siteSettings"]["collabEditingEnabled"], True)
        self.assertEqual(data["siteSettings"]["sitePlanName"], "Basis")
        self.assertEqual(data["siteSettings"]["sitePlanType"], "basic")
        self.assertEqual(data["siteSettings"]["siteCategory"], "")
        self.assertEqual(data["siteSettings"]["siteNotes"], "")
        self.assertEqual(data["siteSettings"]["supportContractEnabled"], False)
        self.assertEqual(data["siteSettings"]["supportContractHoursRemaining"], 0)
        self.assertEqual(data["siteSettings"]["searchPublishedFilterEnabled"], True)
        self.assertEqual(data["siteSettings"]["searchArchiveOption"], "nobody")
        self.assertEqual(data["siteSettings"]["recommendedType"], "recommended")
        self.assertEqual(data["siteSettings"]["blockedUserIntroMessage"], "")
        self.assertEqual(data["siteSettings"]["pushNotificationsEnabled"], False)
        self.assertEqual(
            data["siteSettings"]["pageTagFilters"],
            [
                {"contentType": "news", "showTagFilter": True, "showTagCategories": []},
                {
                    "contentType": "blog",
                    "showTagFilter": False,
                    "showTagCategories": [],
                },
                {
                    "contentType": "question",
                    "showTagFilter": True,
                    "showTagCategories": [],
                },
                {
                    "contentType": "discussion",
                    "showTagFilter": True,
                    "showTagCategories": [],
                },
                {
                    "contentType": "event",
                    "showTagFilter": False,
                    "showTagCategories": [],
                },
            ],
        )
        self.assertEqual({*data["siteSettings"]["hideContentOwner"]}, {"page", "wiki"})
        self.assertEqual(data["siteSettings"]["hideAccessLevelSelect"], False)
        self.assertEqual(data["siteSettings"]["showOriginalFileName"], False)
        self.assertEqual(data["siteSettings"]["siteOwnerName"], "Demo")
        self.assertEqual(data["siteSettings"]["siteOwnerEmail"], "demo@example.com")
        self.assertEqual(
            data["siteSettings"]["openForCreateContentTypes"],
            [
                "blog",
                "discussion",
                "event",
                "podcast",
                "episode",
                "poll",
                "question",
                "wiki",
            ],
        )
        self.assertEqual(data["siteSettings"]["contentModerationEnabled"], False)
        self.assertEqual(data["siteSettings"]["requireContentModerationFor"], [])
        self.assertEqual(data["siteSettings"]["commentModerationEnabled"], False)
        self.assertEqual(data["siteSettings"]["requireCommentModerationFor"], [])

        search_content_types = data["siteSettings"]["searchContentTypes"][
            "contentTypes"
        ]
        self.assertEqual(search_content_types, self.ALL_SEARCH_CONTENT_TYPES)

    def test_site_settings_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.query, {})

    def test_site_settings_by_user(self):
        self.override_config(
            SITE_PLAN="basic",
        )
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})

        self.maxDiff = None

        self.assertEqual(
            {k: v for k, v in result["data"]["siteSettings"].items() if v is not None},
            {
                "blockedUserIntroMessage": "",
                "cancelMembershipEnabled": True,
                "collabEditingEnabled": True,
                "commentWithoutAccountEnabled": False,
                "customTagsAllowed": True,
                "defaultReadAccessId": 1,
                "directLinks": [],
                "fontBody": "Rijksoverheid Sans",
                "fontHeading": "Rijksoverheid Sans",
                "footer": [],
                "icon": "/static/icon.c82b41a356fc.svg",
                "idpId": "",
                "initiativeDescription": "",
                "initiativeImage": "",
                "initiativeImageAlt": "",
                "initiativeTitle": "",
                "language": "nl",
                "leaderImage": "",
                "logo": "",
                "logoAlt": "",
                "menu": [
                    {
                        "access": "public",
                        "children": [],
                        "link": "/blog",
                        "label": "Blog",
                    },
                    {
                        "access": "public",
                        "children": [],
                        "link": "/news",
                        "label": "Nieuws",
                    },
                    {
                        "access": "public",
                        "children": [],
                        "link": "/groups",
                        "label": "Groepen",
                    },
                    {
                        "access": "public",
                        "children": [],
                        "link": "/questions",
                        "label": "Vragen",
                    },
                    {
                        "access": "public",
                        "children": [],
                        "link": "/wiki",
                        "label": "Wiki",
                    },
                ],
                "name": "Pleio 2.0",
                "onboardingEnabled": False,
                "onboardingIntro": "",
                "pdfCheckerEnabled": True,
                "securityText": "",
                "securityTextPGP": "",
                "securityTextRedirectEnabled": False,
                "securityTextRedirectUrl": "",
                "profile": [],
                "pushNotificationsEnabled": False,
                "sitePlanType": "basic",
                "sitePlanName": "Basis",
                "searchArchiveOption": "nobody",
                "recommendedType": "recommended",
                "searchPublishedFilterEnabled": True,
                "showExtraHomepageFilters": True,
                "showIcon": False,
                "showInitiative": False,
                "showLeader": False,
                "showLeaderButtons": False,
                "showSuggestedItems": False,
                "showTagsInDetail": False,
                "showTagsInFeed": False,
                "showCustomTagsInDetail": False,
                "showCustomTagsInFeed": False,
                "startPage": "activity",
                "subtitle": "",
                "theme": "leraar",
                "hideAccessLevelSelect": False,
                "showOriginalFileName": False,
                "contentModerationEnabled": False,
                "requireContentModerationFor": [],
                "commentModerationEnabled": False,
                "requireCommentModerationFor": [],
            },
        )

    def test_site_settings_as_user_admin(self):
        query = """
        query SiteSettings {
            siteSettings {
                profileFields { key }
                exportableUserFields { field }
                siteInvites { edges { email } }
                roleOptions { value }
                deleteAccountRequests { edges { email } }
                profileSets { field { guid } }
            }
        }
        """
        self.graphql_client.force_login(UserFactory(roles=[USER_ROLES.USER_ADMIN]))
        result = self.graphql_client.post(query, {})

        self.assertEqual(
            result["data"]["siteSettings"],
            {
                "profileFields": [
                    {"key": self.profileField1.key},
                    {"key": self.profileField2.key},
                ],
                "exportableUserFields": [
                    {"field": "guid"},
                    {"field": "name"},
                    {"field": "email"},
                    {"field": "created_at"},
                    {"field": "updated_at"},
                    {"field": "last_online"},
                    {"field": "group_memberships"},
                    {"field": "receive_newsletter"},
                    {"field": "created_at_unix"},
                    {"field": "updated_at_unix"},
                    {"field": "last_online_unix"},
                ],
                "siteInvites": {"edges": [{"email": "a@a.nl"}]},
                "roleOptions": [
                    {"value": "ADMIN"},
                    {"value": "EDITOR"},
                    {"value": "QUESTION_MANAGER"},
                    {"value": "NEWS_EDITOR"},
                    {"value": "USER_ADMIN"},
                    {"value": "REQUEST_MANAGER"},
                ],
                "deleteAccountRequests": {"edges": [{"email": "deleted@example.com"}]},
                "profileSets": [],
            },
        )

    def test_site_settings_font(self):
        self.override_config(FONT="headerFont", FONT_BODY="bodyFont")
        self.graphql_client.force_login(self.admin)
        query = """
        query SiteSettings {
            siteSettings {
                fontHeading
                fontBody
            }
        }
        """
        result = self.graphql_client.post(query, {})
        self.assertEqual(
            result["data"]["siteSettings"],
            {"fontHeading": "headerFont", "fontBody": "bodyFont"},
        )

    def test_site_settings_menu_state_normal(self):
        query = """
        mutation UpdateFileOptions($input: editSiteSettingInput!) {
            editSiteSetting(input: $input) {
                siteSettings {
                    menuState
                    __typename
                }
                __typename
            }
        }
        """
        variables = {
            "input": {
                "menuState": "normal",
            }
        }
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query, variables)

        self.assertEqual(
            result["data"]["editSiteSetting"]["siteSettings"]["menuState"],
            "normal",
            msg=result,
        )

    def test_site_settings_menu_state_compact(self):
        query = """
        mutation UpdateFileOptions($input: editSiteSettingInput!) {
            editSiteSetting(input: $input) {
                siteSettings {
                    menuState
                    __typename
                }
                __typename
            }
        }
        """
        variables = {
            "input": {
                "menuState": "compact",
            }
        }
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query, variables)

        self.assertEqual(
            result["data"]["editSiteSetting"]["siteSettings"]["menuState"],
            "compact",
            msg=result,
        )

    def test_site_settings_file_description_enabled(self):
        query = """
        mutation UpdateFileOptions($input: editSiteSettingInput!) {
            editSiteSetting(input: $input) {
                siteSettings {
                    fileDescriptionFieldEnabled
                    __typename
                }
                __typename
            }
        }
        """
        variables = {
            "input": {
                "fileDescriptionFieldEnabled": True,
            }
        }
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query, variables)

        self.assertTrue(
            result["data"]["editSiteSetting"]["siteSettings"][
                "fileDescriptionFieldEnabled"
            ],
            msg=result,
        )

    def test_site_settings_file_description_disabled(self):
        query = """
        mutation UpdateFileOptions($input: editSiteSettingInput!) {
            editSiteSetting(input: $input) {
                siteSettings {
                    fileDescriptionFieldEnabled
                    __typename
                }
                __typename
            }
        }
        """
        variables = {
            "input": {
                "fileDescriptionFieldEnabled": False,
            }
        }
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query, variables)

        self.assertFalse(
            result["data"]["editSiteSetting"]["siteSettings"][
                "fileDescriptionFieldEnabled"
            ],
            msg=result,
        )

    def test_site_settings_content_moderation_enabled(self):
        query = """
        query {
            siteSettings {
                contentModerationEnabled
                requireContentModerationFor
            }
        }
        """
        self.override_config(
            CONTENT_MODERATION_ENABLED=True,
            REQUIRE_CONTENT_MODERATION_FOR=["blog"],
            REQUIRE_COMMENT_MODERATION_FOR=["blog"],
        )

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query)
        self.assertEqual(
            result["data"]["siteSettings"],
            {
                "contentModerationEnabled": True,
                "requireContentModerationFor": ["blog"],
            },
        )

    def test_site_settings_comment_moderation_enabled(self):
        query = """
        query {
            siteSettings {
                commentModerationEnabled
                requireCommentModerationFor
            }
        }
        """
        self.override_config(
            COMMENT_MODERATION_ENABLED=True,
            REQUIRE_COMMENT_MODERATION_FOR=["blog"],
        )

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(query)
        self.assertEqual(
            result["data"]["siteSettings"],
            {
                "commentModerationEnabled": True,
                "requireCommentModerationFor": ["blog"],
            },
        )

    @mock.patch("core.resolvers.shared.MeetingsApi.get_appointment_types")
    def test_query_no_videocall_settings(self, get_appointment_types):
        get_appointment_types.return_value = [
            {"Id": "1000", "Name": "First"},
            {"Id": "1001", "Name": "Second"},
        ]

        with override_config(
            VIDEOCALL_APPOINTMENT_TYPE=None,
            ONLINEAFSPRAKEN_ENABLED=True,
        ):
            self.graphql_client.force_login(self.admin)
            response = self.graphql_client.post(self.query, {})

        self.assertEqual(
            response["data"]["siteSettings"]["appointmentTypeVideocall"],
            [
                {"name": "First", "hasVideocall": False},
                {"name": "Second", "hasVideocall": False},
            ],
        )

    @mock.patch("core.resolvers.shared.MeetingsApi.get_appointment_types")
    def test_query_videocall_settings(self, get_appointment_types):
        get_appointment_types.return_value = [
            {"Id": "1000", "Name": "First"},
            {"Id": "1001", "Name": "Second"},
        ]

        with override_config(
            VIDEOCALL_APPOINTMENT_TYPE=[{"id": "1000", "hasVideocall": True}],
            ONLINEAFSPRAKEN_ENABLED=True,
        ):
            self.graphql_client.force_login(self.admin)
            response = self.graphql_client.post(self.query, {})

        self.assertEqual(
            response["data"]["siteSettings"]["appointmentTypeVideocall"],
            [
                {"name": "First", "hasVideocall": True},
                {"name": "Second", "hasVideocall": False},
            ],
        )

    @override_config(ONLINEAFSPRAKEN_ENABLED=True)
    @mock.patch("core.resolvers.shared.MeetingsApi.get_appointment_types")
    def test_update_videocall_settings(self, get_appointment_types):
        get_appointment_types.return_value = [
            {"Id": "1000", "Name": "First"},
            {"Id": "1001", "Name": "Second"},
        ]

        mutation = """
        mutation UpdateSiteSettings($input: editSiteSettingInput!) {
            m: editSiteSetting(input: $input) {
                r: siteSettings {
                    appointmentTypeVideocall {
                        name
                        hasVideocall
                    }
                }
            }
        }
        """

        setting, _ = Setting.objects.get_or_create(key="VIDEOCALL_APPOINTMENT_TYPE")
        setting.value = [{"id": "1000", "hasVideocall": True}]
        setting.save()

        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(
            mutation,
            {
                "input": {
                    "appointmentTypeVideocall": [
                        {"id": "1000", "hasVideocall": False},
                        {"id": "1001", "hasVideocall": True},
                    ]
                }
            },
        )

        self.assertEqual(
            response["data"]["m"]["r"]["appointmentTypeVideocall"],
            [
                {"name": "First", "hasVideocall": False},
                {"name": "Second", "hasVideocall": True},
            ],
        )

        setting.refresh_from_db()

        self.assertEqual(
            setting.value,
            [
                {"id": "1000", "hasVideocall": False},
                {"id": "1001", "hasVideocall": True},
            ],
        )

        setting.delete()

    def test_hidden_content_types_for_search(self):
        self.maxDiff = None
        """
        Test that excluded content types are not returned in the search content types on 'site'
        But they are returned in the search content types on 'siteSettings'
        """
        self.override_config(
            SEARCH_EXCLUDED_CONTENT_TYPES=[
                "episode",
                "event",
                "file",
                "blog",
                "discussion",
                "user",
                "group",
                "magazine_issue",
                "folder",
                "news",
                "pad",
                "page",
                "podcast",
                "question",
                "wiki",
            ]
        )
        self.query = """
            query SiteGeneralSettings {
                siteSettings {
                    searchContentTypes { contentTypes { key, value } }
                }
                site {
                    searchFilter { contentTypes { key, value } }
                }
            }
        """

        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query)

        self.assertEqual(
            response["data"]["siteSettings"]["searchContentTypes"],
            {
                "contentTypes": self.ALL_SEARCH_CONTENT_TYPES,
            },
        )
        self.assertEqual(
            response["data"]["site"]["searchFilter"],
            {"contentTypes": [{"key": "image", "value": "Afbeelding"}]},
        )


class TestSiteSettingsIsClosedTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=True)

    def test_site_settings_is_closed_random(self):
        response = self.client.get("/981random3")
        self.assertTemplateUsed(response, "registration/login.html")

    def test_site_settings_is_closed_graphql(self):
        response = self.client.get("/graphql")
        self.assertTemplateUsed(response, "registration/login.html")

    def test_site_settings_is_closed_robots(self):
        response = self.client.get("/robots.txt")
        self.assertTemplateNotUsed(response, "registration/login.html")

    def test_site_settings_is_closed_sitemap(self):
        response = self.client.get("/sitemap.xml")
        self.assertTemplateNotUsed(response, "registration/login.html")

    def test_site_settings_is_closed_oidc(self):
        response = self.client.get("/oidc/test")
        self.assertTemplateNotUsed(response, "registration/login.html")

    def test_site_settings_is_closed_login(self):
        response = self.client.get("/login")
        self.assertTemplateNotUsed(response, "registration/login.html")
        self.assertEqual(response.status_code, 302)

    def test_site_settings_is_closed_login_multiple_choice(self):
        self.override_config(OIDC_PROVIDERS=["pleio", "custom-idp"])

        response = self.client.get("/login")
        self.assertTemplateUsed(response, "registration/login.html")

    def test_site_settings_is_closed_static(self):
        response = self.client.get("/static/favicon.ico")
        self.assertTemplateNotUsed(response, "registration/login.html")

    def test_site_settings_is_closed_featured_file(self):
        response = self.client.get("/file/featured/test.txt")
        self.assertTemplateNotUsed(response, "registration/login.html")


class TestSiteSettingsOpenSiteTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=False)

    def test_site_settings_is_walled_garden_by_ip_enabled_but_whitelisted(self):
        self.override_config(
            WALLED_GARDEN_BY_IP_ENABLED=True,
            WHITELISTED_IP_RANGES=["10.10.10.10"],
        )
        response = self.client.get("/981random3", REMOTE_ADDR="10.10.10.10")

        self.assertTemplateNotUsed(response, "registration/login.html")

    def test_site_settings_is_walled_garden_by_ip_enabled_but_whitelisted_different_ip(
        self,
    ):
        self.override_config(
            WALLED_GARDEN_BY_IP_ENABLED=True,
            WHITELISTED_IP_RANGES=["10.10.10.11/32"],
        )
        response = self.client.get("/981random3", REMOTE_ADDR="10.10.10.10")

        self.assertTemplateUsed(response, "registration/login.html")

    def test_site_settings_is_walled_garden_by_ip_enabled_but_whitelisted_large_network(
        self,
    ):
        self.override_config(
            WALLED_GARDEN_BY_IP_ENABLED=True,
            WHITELISTED_IP_RANGES=["10.10.10.0/24"],
        )

        response = self.client.get("/981random3", HTTP_X_FORWARDED_FOR="10.10.10.108")

        self.assertTemplateNotUsed(response, "registration/login.html")

    def test_site_settings_is_walled_garden_by_ip_enabled_but_whitelisted_large_network_different_range(
        self,
    ):
        self.override_config(
            WALLED_GARDEN_BY_IP_ENABLED=True,
            WHITELISTED_IP_RANGES=["10.10.11.0/24"],
        )

        response = self.client.get("/981random3", HTTP_X_FORWARDED_FOR="10.10.10.108")

        self.assertTemplateUsed(response, "registration/login.html")

    def test_site_settings_is_not_walled_garden_by_ip_enabled_but_whitelisted(self):
        self.override_config(
            WALLED_GARDEN_BY_IP_ENABLED=False,
            WHITELISTED_IP_RANGES=[],
        )

        response = self.client.get("/981random3", REMOTE_ADDR="10.10.10.10")

        self.assertTemplateNotUsed(response, "registration/login.html")


class TestSiteSettingsJavascriptSection(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.authenticated_user = UserFactory()
        self.AUTHENTICATED_UUID = str(uuid.uuid4())
        self.ANONYMOUS_UUID = str(uuid.uuid4())

    def test_anonymous(self):
        with override_config(
            IS_CLOSED=False,
            STARTPAGE="cms",
            STARTPAGE_CMS=self.AUTHENTICATED_UUID,
            ANONYMOUS_START_PAGE="cms",
            ANONYMOUS_START_PAGE_CMS=self.ANONYMOUS_UUID,
        ):
            response = self.client.get("")
            content = response.content.decode()

        self.assertNotIn(self.AUTHENTICATED_UUID, content)
        self.assertIn(self.ANONYMOUS_UUID, content)

    def test_authenticated_user(self):
        with override_config(
            IS_CLOSED=False,
            STARTPAGE="cms",
            STARTPAGE_CMS=self.AUTHENTICATED_UUID,
            ANONYMOUS_START_PAGE="cms",
            ANONYMOUS_START_PAGE_CMS=self.ANONYMOUS_UUID,
        ):
            self.client.force_login(self.authenticated_user)
            response = self.client.get("")
            content = response.content.decode()

        self.assertIn(self.AUTHENTICATED_UUID, content)
        self.assertNotIn(self.ANONYMOUS_UUID, content)


class TestSiteSettingsProperties(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.admin = AdminFactory()
        self.query = """
        query SiteGeneralSettings {
            siteSettings {
                %s
            }
        }
        """

    def test_site_plan_name(self):
        self.override_config(SITE_PLAN="basic")
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query % "sitePlanName", {})
        self.assertEqual(response["data"]["siteSettings"]["sitePlanName"], "Basis")

    def test_site_plan_type(self):
        self.override_config(SITE_PLAN="basic")
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query % "sitePlanType", {})
        self.assertEqual(response["data"]["siteSettings"]["sitePlanType"], "basic")

    def test_site_category(self):
        self.override_config(SITE_CATEGORY="ov")
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query % "siteCategory", {})
        self.assertEqual(response["data"]["siteSettings"]["siteCategory"], "Overig")

    def test_site_notes(self):
        self.override_config(SITE_NOTES="Notes of site")
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query % "siteNotes", {})
        self.assertEqual(response["data"]["siteSettings"]["siteNotes"], "Notes of site")

    def test_support_contract_enabled(self):
        self.override_config(SUPPORT_CONTRACT_ENABLED=True)
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query % "supportContractEnabled", {})
        self.assertEqual(
            response["data"]["siteSettings"]["supportContractEnabled"], True
        )

    def test_support_contract_hours(self):
        self.override_config(SUPPORT_CONTRACT_HOURS_REMAINING="9.2")
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(
            self.query % "supportContractHoursRemaining", {}
        )
        self.assertEqual(
            response["data"]["siteSettings"]["supportContractHoursRemaining"], 9.2
        )

    def test_search_published_filter_disabled(self):
        self.override_config(SEARCH_PUBLISHED_FILTER_ENABLED=False)
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(
            self.query % "searchPublishedFilterEnabled", {}
        )
        self.assertEqual(
            response["data"]["siteSettings"]["searchPublishedFilterEnabled"], False
        )

    def test_other_search_archive_option(self):
        self.override_config(SEARCH_ARCHIVE_OPTION="everyone")
        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query % "searchArchiveOption", {})
        self.assertEqual(
            response["data"]["siteSettings"]["searchArchiveOption"], "everyone"
        )
