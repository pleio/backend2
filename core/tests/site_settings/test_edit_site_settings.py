from unittest.mock import patch

from django.core.cache import cache
from mixer.backend.django import mixer

from core import config
from core.constances import ALL_CONTENT_TYPES
from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase, override_config
from user.factories import AdminFactory
from user.models import User


class EditSiteSettingTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = mixer.blend(User)
        self.admin = mixer.blend(User, roles=["ADMIN"])
        self.profileField1 = ProfileField.objects.create(
            key="key1", name="text_name", field_type="text_field"
        )
        self.profileField2 = ProfileField.objects.create(
            key="key2", name="text_name", field_type="text_field"
        )
        self.profileField3 = ProfileField.objects.create(
            key="key3",
            name="text_name",
            field_type="select_field",
            field_options=["baz", "qux"],
        )
        self.profileField4 = ProfileField.objects.create(
            key="text_key4", name="text_name", field_type="text_field"
        )
        self.PAGE_TAG_FILTERS = [
            {"showTagFilter": False, "showTagCategories": [], "contentType": "blog"}
        ]

    def tearDown(self):
        cache.clear()
        super().tearDown()

    def test_edit_site_setting_by_admin(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        language
                        extraLanguages
                        name
                        isClosed
                        allowRegistration
                        directRegistrationDomains
                        description
                        defaultReadAccessId
                        defaultReadAccessIdOptions {
                            value
                            label
                        }
                        footerPageEnabled
                        searchEngineIndexingEnabled
                        searchPublishedFilterEnabled
                        searchArchiveOption
                        searchExcludedContentTypes

                        piwikUrl
                        piwikId

                        theme
                        logoAlt
                        likeIcon
                        fontHeading
                        fontBody
                        colorPrimary
                        colorSecondary
                        colorHeader

                        startPage
                        startPageCms
                        anonymousStartPage
                        anonymousStartPageCms
                        showIcon
                        icon
                        iconAlt
                        menu {
                            label
                            link
                            children {
                                label
                                link
                                children {
                                    label
                                }
                                access
                            }
                            access
                        }

                        numberOfFeaturedItems
                        enableFeedSorting
                        showExtraHomepageFilters
                        showLeader
                        showLeaderButtons
                        subtitle
                        leaderImage
                        showInitiative
                        initiativeTitle
                        initiativeDescription
                        initiativeImage
                        initiativeImageAlt
                        initiativeLink
                        directLinks {
                            title
                            link
                        }
                        footer {
                            title
                            link
                        }
                        redirects {
                            source
                            destination
                        }

                        profile {
                            key
                            name
                            isFilter
                            isInOverview
                            isOnVcard
                            isInAutoGroupMembership
                        }

                        profileSections {
                            name
                            profileFieldGuids
                        }

                        showTagsInFeed
                        showTagsInDetail
                        showCustomTagsInFeed
                        showCustomTagsInDetail

                        defaultEmailOverviewFrequency
                        emailOverviewSubject
                        emailOverviewTitle
                        emailOverviewIntro
                        emailNotificationShowExcerpt

                        showLoginRegister
                        customTagsAllowed
                        showUpDownVoting
                        enableSharing
                        showViewsCount
                        newsletter
                        cancelMembershipEnabled
                        showExcerptInNewsCard
                        commentsOnNews
                        eventExport
                        eventTiles
                        questionerCanChooseBestAnswer
                        statusUpdateGroups
                        subgroups
                        groupMemberExport
                        showSuggestedItems

                        onboardingEnabled
                        onboardingForceExistingUsers
                        onboardingIntro
                        cookieConsent
                        loginIntro

                        profileSyncEnabled
                        profileSyncToken

                        customCss
                        walledGardenByIpEnabled
                        whitelistedIpRanges

                        siteMembershipAcceptedSubject
                        siteMembershipAcceptedIntro
                        siteMembershipDeniedSubject
                        siteMembershipDeniedIntro
                        idpId
                        idpName
                        autoApproveSSO

                        flowEnabled
                        flowSubtypes
                        flowAppUrl
                        flowToken
                        flowCaseId
                        flowUserGuid

                        commentWithoutAccountEnabled

                        questionLockAfterActivity
                        questionLockAfterActivityLink

                        kalturaVideoEnabled
                        kalturaVideoPartnerId
                        kalturaVideoPlayerId

                        pdfCheckerEnabled
                        securityText
                        securityTextPGP
                        securityTextRedirectEnabled
                        securityTextRedirectUrl
                        maxCharactersInAbstract
                        preserveFileExif
                        recommendedType
                        blockedUserIntroMessage
                        pushNotificationsEnabled

                        pageTagFilters {
                            contentType
                            showTagCategories
                            showTagFilter
                        }
                        hideContentOwner
                        collabEditingEnabled
                        hideAccessLevelSelect
                        showOriginalFileName
                        openForCreateContentTypes
                        requireContentModerationFor
                        requireCommentModerationFor
                    }
                }
            }
        """
        variables = {
            "input": {
                "language": "en",
                "extraLanguages": ["nl"],
                "name": "name2",
                "description": "description2",
                "isClosed": True,
                "allowRegistration": False,
                "directRegistrationDomains": ["a.nl", "b.nl"],
                "defaultReadAccessId": 0,
                "footerPageEnabled": True,
                "searchEngineIndexingEnabled": True,
                "piwikUrl": "url3",
                "piwikId": "id1",
                "theme": "rijkshuisstijl",
                "logoAlt": "alttext1",
                "likeIcon": "thumbs",
                "fontHeading": "Roboto",
                "fontBody": "Roboto",
                "colorPrimary": "#111111",
                "colorSecondary": "#222222",
                "colorHeader": "#333333",
                "startPage": "cms",
                "startPageCms": "123456",
                "anonymousStartPage": "",
                "anonymousStartPageCms": "",
                "showIcon": True,
                "iconAlt": "iconAlt",
                # TODO: "icon": "",
                "menu": [
                    {"id": 1, "link": "/news", "parentId": None, "label": "Nieuws"},
                    {"id": 2, "link": "/blog", "parentId": 1, "label": "Blog"},
                ],
                "numberOfFeaturedItems": 3,
                "enableFeedSorting": True,
                "showExtraHomepageFilters": False,
                "showLeader": True,
                "showLeaderButtons": True,
                "subtitle": "subtitle1",
                "leaderImage": "https://test1.nl",
                "showInitiative": True,
                "initiativeTitle": "intitle1",
                "initiativeDescription": "indescription1",
                "initiativeImage": "https://test2.nl",
                "initiativeImageAlt": "inimagealt1",
                "initiativeLink": "https://link.nl",
                "directLinks": [
                    {"title": "extern", "link": "https://nos.nl"},
                    {"title": "intern", "link": "/news"},
                    {
                        "title": "intern lang",
                        "link": "https://nieuw-template.pleio-test.nl/news",
                    },
                ],
                "footer": [
                    {"link": "https://www.nieuw.nl", "title": "Nieuwe link"},
                    {"link": "https://wnas.nl", "title": "wnas"},
                ],
                "redirects": [
                    {"source": "/path1", "destination": "/path2"},
                    {"source": "/path3", "destination": "/path4"},
                    {"source": "/path5", "destination": "/path6"},
                ],
                "profile": [
                    {
                        "isFilter": False,
                        "isInOverview": False,
                        "key": "key1",
                        "name": "name1",
                        "isOnVcard": True,
                    },
                    {
                        "isFilter": False,
                        "isInOverview": False,
                        "key": "key2",
                        "name": "name2",
                    },
                    {
                        "isFilter": True,
                        "isInOverview": True,
                        "key": "key3",
                        "name": "name3",
                        "isInAutoGroupMembership": True,
                    },
                ],
                "profileSections": [
                    {
                        "name": "section_one",
                        "profileFieldGuids": [
                            str(self.profileField1.id),
                            str(self.profileField3.id),
                        ],
                    },
                    {
                        "name": "section_two",
                        "profileFieldGuids": [str(self.profileField4.id)],
                    },
                    {"name": "section_three", "profileFieldGuids": ["notrealid"]},
                ],
                "showTagsInFeed": True,
                "showTagsInDetail": True,
                "showCustomTagsInFeed": True,
                "showCustomTagsInDetail": True,
                "defaultEmailOverviewFrequency": "monthly",
                "emailOverviewSubject": "emailOverviewSubject1",
                "emailOverviewTitle": "emailOverviewTitle1",
                "emailOverviewIntro": "emailOverviewIntro1",
                "emailNotificationShowExcerpt": True,
                "showLoginRegister": False,
                "customTagsAllowed": False,
                "showUpDownVoting": False,
                "enableSharing": False,
                "showViewsCount": False,
                "newsletter": True,
                "cancelMembershipEnabled": False,
                "showExcerptInNewsCard": True,
                "commentsOnNews": True,
                "eventExport": True,
                "eventTiles": True,
                "questionerCanChooseBestAnswer": True,
                "statusUpdateGroups": False,
                "subgroups": True,
                "groupMemberExport": True,
                "showSuggestedItems": True,
                "onboardingEnabled": True,
                "onboardingForceExistingUsers": True,
                "onboardingIntro": "Welcome onboarding",
                "cookieConsent": True,
                "loginIntro": "test",
                "profileSyncEnabled": True,
                "profileSyncToken": "124928374810983affdddffd",
                "customCss": "h1 {color: maroon;margin-left: 40px;}",
                "walledGardenByIpEnabled": False,
                "whitelistedIpRanges": ["192.168.0.1/32", "192.168.0.0/24"],
                "siteMembershipAcceptedSubject": "Membership request accepted",
                "siteMembershipAcceptedIntro": "You request is accepted",
                "siteMembershipDeniedSubject": "Membership request denied",
                "siteMembershipDeniedIntro": "Your request is not accepted",
                "idpId": "idp_id",
                "idpName": "idp_name",
                "autoApproveSSO": True,
                # TODO: remove after flow connects to general api
                "flowEnabled": True,
                "flowSubtypes": ["blog", "question"],
                "flowAppUrl": "https://flow.test",
                "flowToken": "1234567890qwertyuiop",
                "flowCaseId": 1,
                "flowUserGuid": self.admin.guid,
                "commentWithoutAccountEnabled": True,
                "questionLockAfterActivity": True,
                "questionLockAfterActivityLink": "https://test.link",
                "kalturaVideoEnabled": True,
                "kalturaVideoPartnerId": "123",
                "kalturaVideoPlayerId": "456",
                "pdfCheckerEnabled": False,
                "securityText": "very secure \n very secure",
                "securityTextPGP": "ae1f",
                "securityTextRedirectEnabled": True,
                "securityTextRedirectUrl": "https://url.url",
                "maxCharactersInAbstract": 500,
                "preserveFileExif": True,
                "searchArchiveOption": "authenticated",
                "searchExcludedContentTypes": ["blog"],
                "recommendedType": "certified",
                "searchPublishedFilterEnabled": False,
                "blockedUserIntroMessage": "blockedUserIntroMessage",
                "pushNotificationsEnabled": True,
                "pageTagFilters": [
                    {
                        "showTagFilter": False,
                        "showTagCategories": [],
                        "contentType": "blog",
                    },
                ],
                "hideContentOwner": [
                    "blog",
                    "discussion",
                    "news",
                    "event",
                    "question",
                    "task",
                ],
                "collabEditingEnabled": False,
                "hideAccessLevelSelect": True,
                "showOriginalFileName": True,
                "openForCreateContentTypes": ["blog", "news"],
                "requireContentModerationFor": ALL_CONTENT_TYPES,
                "requireCommentModerationFor": ALL_CONTENT_TYPES,
            }
        }

        with override_config(PAGE_TAG_FILTERS=self.PAGE_TAG_FILTERS):
            self.graphql_client.force_login(self.admin)
            result = self.graphql_client.post(mutation, variables)

        site_settings = result["data"]["editSiteSetting"]["siteSettings"]
        self.assertEqual(site_settings["language"], "en")
        self.assertEqual(site_settings["extraLanguages"], ["nl"])
        self.assertEqual(site_settings["name"], "name2")
        self.assertEqual(site_settings["description"], "description2")
        self.assertEqual(site_settings["isClosed"], True)
        self.assertEqual(site_settings["allowRegistration"], False)
        self.assertEqual(
            site_settings["directRegistrationDomains"],
            ["a.nl", "b.nl"],
        )
        self.assertEqual(site_settings["defaultReadAccessId"], 0)
        self.assertEqual(
            site_settings["defaultReadAccessIdOptions"],
            [
                {"value": 0, "label": "Alleen eigenaar"},
                {"value": 1, "label": "Ingelogde gebruikers"},
            ],
        )
        self.assertEqual(site_settings["footerPageEnabled"], True)
        self.assertEqual(site_settings["searchEngineIndexingEnabled"], True)
        self.assertEqual(
            site_settings["searchPublishedFilterEnabled"],
            False,
        )
        self.assertEqual(
            site_settings["searchArchiveOption"],
            "authenticated",
        )
        self.assertEqual(site_settings["searchExcludedContentTypes"], ["blog"])

        self.assertEqual(site_settings["piwikUrl"], "url3")
        self.assertEqual(site_settings["piwikId"], "id1")

        self.assertEqual(site_settings["theme"], "rijkshuisstijl")
        # TODO: self.assertEqual(site_settings["logo"], "id1")
        self.assertEqual(site_settings["logoAlt"], "alttext1")
        self.assertEqual(site_settings["likeIcon"], "thumbs")
        self.assertEqual(site_settings["fontHeading"], "Roboto")
        self.assertEqual(site_settings["fontBody"], "Roboto")
        self.assertEqual(site_settings["colorPrimary"], "#111111")
        self.assertEqual(site_settings["colorSecondary"], "#222222")
        self.assertEqual(site_settings["colorHeader"], "#333333")

        self.assertEqual(site_settings["startPage"], "cms")
        self.assertEqual(site_settings["startPageCms"], "123456")
        self.assertEqual(site_settings["anonymousStartPage"], "")
        self.assertEqual(site_settings["anonymousStartPageCms"], "")
        self.assertEqual(site_settings["showIcon"], True)
        self.assertEqual(site_settings["iconAlt"], "iconAlt")

        # TODO: self.assertEqual(site_settings["icon"], "heart")
        self.assertEqual(
            site_settings["menu"],
            [
                {
                    "label": "Nieuws",
                    "link": "/news",
                    "children": [
                        {
                            "label": "Blog",
                            "link": "/blog",
                            "children": [],
                            "access": "public",
                        },
                    ],
                    "access": "public",
                }
            ],
        )

        self.assertEqual(site_settings["numberOfFeaturedItems"], 3)
        self.assertEqual(site_settings["enableFeedSorting"], True)
        self.assertEqual(site_settings["showExtraHomepageFilters"], False)
        self.assertEqual(site_settings["showLeader"], True)
        self.assertEqual(site_settings["showLeaderButtons"], True)
        self.assertEqual(site_settings["subtitle"], "subtitle1")
        self.assertEqual(site_settings["leaderImage"], "https://test1.nl")
        self.assertEqual(site_settings["showInitiative"], True)
        self.assertEqual(site_settings["initiativeTitle"], "intitle1")
        self.assertEqual(
            site_settings["initiativeDescription"],
            "indescription1",
        )
        self.assertEqual(
            site_settings["initiativeImage"],
            "https://test2.nl",
        )
        self.assertEqual(site_settings["initiativeImageAlt"], "inimagealt1")
        self.assertEqual(site_settings["initiativeLink"], "https://link.nl")
        self.assertEqual(
            site_settings["directLinks"],
            [
                {"title": "extern", "link": "https://nos.nl"},
                {"title": "intern", "link": "/news"},
                {
                    "title": "intern lang",
                    "link": "https://nieuw-template.pleio-test.nl/news",
                },
            ],
        )
        self.assertEqual(
            site_settings["footer"],
            [
                {"title": "Nieuwe link", "link": "https://www.nieuw.nl"},
                {"title": "wnas", "link": "https://wnas.nl"},
            ],
        )
        self.assertEqual(
            site_settings["redirects"],
            [
                {"source": "/path1", "destination": "/path2"},
                {"source": "/path3", "destination": "/path4"},
                {"source": "/path5", "destination": "/path6"},
            ],
        )

        self.assertEqual(
            site_settings["profile"],
            [
                {
                    "isFilter": False,
                    "isInOverview": False,
                    "key": "key1",
                    "name": "name1",
                    "isOnVcard": True,
                    "isInAutoGroupMembership": False,
                },
                {
                    "isFilter": False,
                    "isInOverview": False,
                    "key": "key2",
                    "name": "name2",
                    "isOnVcard": False,
                    "isInAutoGroupMembership": False,
                },
                {
                    "isFilter": True,
                    "isInOverview": True,
                    "key": "key3",
                    "name": "name3",
                    "isOnVcard": False,
                    "isInAutoGroupMembership": True,
                },
            ],
        )

        self.assertEqual(
            site_settings["profileSections"],
            [
                {
                    "name": "section_one",
                    "profileFieldGuids": [
                        str(self.profileField1.id),
                        str(self.profileField3.id),
                    ],
                },
                {
                    "name": "section_two",
                    "profileFieldGuids": [str(self.profileField4.id)],
                },
                {"name": "section_three", "profileFieldGuids": []},
            ],
        )

        self.assertEqual(site_settings["showTagsInFeed"], True)
        self.assertEqual(site_settings["showTagsInDetail"], True)
        self.assertEqual(site_settings["showCustomTagsInFeed"], True)
        self.assertEqual(site_settings["showCustomTagsInDetail"], True)

        self.assertEqual(
            site_settings["defaultEmailOverviewFrequency"],
            "monthly",
        )
        self.assertEqual(
            site_settings["emailOverviewSubject"],
            "emailOverviewSubject1",
        )
        self.assertEqual(
            site_settings["emailOverviewTitle"],
            "emailOverviewTitle1",
        )
        self.assertEqual(
            site_settings["emailOverviewIntro"],
            "emailOverviewIntro1",
        )
        self.assertEqual(
            site_settings["emailNotificationShowExcerpt"],
            True,
        )

        self.assertEqual(site_settings["showLoginRegister"], False)
        self.assertEqual(site_settings["customTagsAllowed"], False)
        self.assertEqual(site_settings["showUpDownVoting"], False)
        self.assertEqual(site_settings["enableSharing"], False)
        self.assertEqual(site_settings["showViewsCount"], False)
        self.assertEqual(site_settings["newsletter"], True)
        self.assertEqual(site_settings["cancelMembershipEnabled"], False)
        self.assertEqual(site_settings["showExcerptInNewsCard"], True)
        self.assertEqual(site_settings["commentsOnNews"], True)
        self.assertEqual(site_settings["eventExport"], True)
        self.assertEqual(site_settings["eventTiles"], True)
        self.assertEqual(
            site_settings["questionerCanChooseBestAnswer"],
            True,
        )
        self.assertEqual(site_settings["statusUpdateGroups"], False)
        self.assertEqual(site_settings["subgroups"], True)
        self.assertEqual(site_settings["groupMemberExport"], True)
        self.assertEqual(site_settings["showSuggestedItems"], True)

        self.assertEqual(site_settings["onboardingEnabled"], True)
        self.assertEqual(
            site_settings["onboardingForceExistingUsers"],
            True,
        )
        self.assertEqual(
            site_settings["onboardingIntro"],
            "Welcome onboarding",
        )
        self.assertEqual(site_settings["cookieConsent"], True)

        self.assertEqual(site_settings["profileSyncEnabled"], True)
        self.assertEqual(
            site_settings["profileSyncToken"],
            "124928374810983affdddffd",
        )

        self.assertEqual(site_settings["loginIntro"], "test")
        self.assertEqual(
            site_settings["customCss"],
            "h1 {color: maroon;margin-left: 40px;}",
        )
        self.assertEqual(site_settings["walledGardenByIpEnabled"], False)
        self.assertEqual(
            site_settings["whitelistedIpRanges"],
            ["192.168.0.1/32", "192.168.0.0/24"],
        )
        self.assertEqual(
            site_settings["siteMembershipAcceptedSubject"],
            "Membership request accepted",
        )
        self.assertEqual(
            site_settings["siteMembershipAcceptedIntro"],
            "You request is accepted",
        )
        self.assertEqual(
            site_settings["siteMembershipDeniedSubject"],
            "Membership request denied",
        )
        self.assertEqual(
            site_settings["siteMembershipDeniedIntro"],
            "Your request is not accepted",
        )
        self.assertEqual(site_settings["idpId"], "idp_id")
        self.assertEqual(site_settings["idpName"], "idp_name")
        self.assertEqual(site_settings["autoApproveSSO"], True)

        # TODO: remove after flow connects to general api
        self.assertEqual(site_settings["flowEnabled"], True)
        self.assertEqual(
            site_settings["flowSubtypes"],
            ["blog", "question"],
        )
        self.assertEqual(site_settings["flowAppUrl"], "https://flow.test")
        self.assertEqual(site_settings["flowToken"], "1234567890qwertyuiop")
        self.assertEqual(site_settings["flowCaseId"], 1)
        self.assertEqual(site_settings["flowUserGuid"], self.admin.guid)

        self.assertEqual(
            site_settings["commentWithoutAccountEnabled"],
            True,
        )

        self.assertEqual(site_settings["questionLockAfterActivity"], True)
        self.assertEqual(
            site_settings["questionLockAfterActivityLink"],
            "https://test.link",
        )

        self.assertEqual(site_settings["kalturaVideoEnabled"], True)
        self.assertEqual(site_settings["kalturaVideoPartnerId"], "123")
        self.assertEqual(site_settings["kalturaVideoPlayerId"], "456")

        self.assertEqual(site_settings["pdfCheckerEnabled"], False)
        self.assertEqual(
            site_settings["securityText"],
            "very secure \n very secure",
        )
        self.assertEqual(site_settings["securityTextPGP"], "ae1f")
        self.assertEqual(site_settings["securityTextRedirectEnabled"], True)
        self.assertEqual(
            site_settings["securityTextRedirectUrl"],
            "https://url.url",
        )
        self.assertEqual(site_settings["maxCharactersInAbstract"], 500)
        self.assertEqual(site_settings["preserveFileExif"], True)
        self.assertEqual(
            site_settings["recommendedType"],
            "certified",
        )
        self.assertEqual(
            site_settings["blockedUserIntroMessage"],
            "blockedUserIntroMessage",
        )
        self.assertEqual(site_settings["pushNotificationsEnabled"], True)

        self.assertEqual(
            site_settings["pageTagFilters"],
            [
                {"showTagFilter": True, "showTagCategories": [], "contentType": "news"},
                {
                    "showTagFilter": False,
                    "showTagCategories": [],
                    "contentType": "blog",
                },
                {
                    "showTagFilter": True,
                    "showTagCategories": [],
                    "contentType": "question",
                },
                {
                    "showTagFilter": True,
                    "showTagCategories": [],
                    "contentType": "discussion",
                },
                {
                    "showTagFilter": False,
                    "showTagCategories": [],
                    "contentType": "event",
                },
            ],
        )
        self.assertEqual(
            site_settings["hideContentOwner"],
            ["blog", "discussion", "news", "event", "question", "task"],
        )
        self.assertEqual(site_settings["collabEditingEnabled"], False)
        self.assertEqual(site_settings["hideAccessLevelSelect"], True)
        self.assertEqual(site_settings["showOriginalFileName"], True)
        self.assertEqual(
            site_settings["openForCreateContentTypes"],
            ["blog"],
        )
        self.assertEqual(
            site_settings["requireContentModerationFor"],
            [
                "blog",
                "discussion",
                "event",
                "file",
                "news",
                "episode",
                "podcast",
                "question",
                "wiki",
            ],
        )
        self.assertEqual(
            site_settings["requireCommentModerationFor"],
            [
                "blog",
                "discussion",
                "event",
                "news",
                "question",
            ],
        )

    def test_edit_site_setting_logo_and_icon(self):
        file_mock = self.use_mock_file("logo.png")

        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        logo
                        icon
                    }
                }
            }
        """

        variables = {"input": {"logo": file_mock, "icon": file_mock}}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(mutation, variables)

        site_settings = result["data"]["editSiteSetting"]["siteSettings"]
        self.assertIsNotNone(site_settings["logo"])
        self.assertIsNotNone(site_settings["icon"])

    def test_edit_site_setting_by_anonymous(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        language
                    }
                }
            }
        """

        variables = {"input": {"language": "en"}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_by_user(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        language
                    }
                }
            }
        """

        variables = {"input": {"language": "en"}}

        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_invalid_domain(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        directRegistrationDomains
                    }
                }
            }
        """

        variables = {"input": {"directRegistrationDomains": ["invaliddomain"]}}

        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_invalid_security_text_redirect_url(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        securityTextRedirectUrl
                    }
                }
            }
        """

        variables = {"input": {"securityTextRedirectUrl": "invalidurl"}}

        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)

    @patch(
        "core.resolvers.mutations.mutation_edit_site_setting.schedule_site_access_changed_mail"
    )
    def test_edit_site_setting_is_closed_default_access(self, mocked_mail):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        isClosed
                        defaultReadAccessId
                    }
                }
            }
        """

        variables = {"input": {"isClosed": False, "defaultReadAccessId": 2}}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(mutation, variables)
        site_settings = result["data"]["editSiteSetting"]["siteSettings"]

        self.assertEqual(site_settings["isClosed"], False)
        self.assertEqual(site_settings["defaultReadAccessId"], 2)
        self.assertEqual(mocked_mail.call_count, 1)

        variables = {"input": {"isClosed": True}}
        result = self.graphql_client.post(mutation, variables)
        site_settings = result["data"]["editSiteSetting"]["siteSettings"]

        self.assertEqual(site_settings["isClosed"], True)
        self.assertEqual(site_settings["defaultReadAccessId"], 1)
        self.assertEqual(mocked_mail.call_count, 2)

    def test_edit_site_setting_wrong_whitelisted_ip_range(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        whitelistedIpRanges
                    }
                }
            }
        """

        variables = {"input": {"whitelistedIpRanges": ["10.10.266.3"]}}

        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_redirects_loop(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        redirects {
                            source
                            destination
                        }
                    }
                }
            }
        """

        variables = {
            "input": {
                "redirects": [
                    {"source": "/path1", "destination": "/path2"},
                    {"source": "/path2/", "destination": "/path3"},
                    {"source": "/path3", "destination": "/path6"},
                ]
            }
        }

        with self.assertGraphQlError("redirects_has_loop"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_redirects_invalid_url(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        redirects {
                            source
                            destination
                        }
                    }
                }
            }
        """

        variables = {
            "input": {
                "redirects": [
                    {"source": "/pat  /", "destination": "/path2"},
                    {"source": "/path3/", "destination": "/path4"},
                    {"source": "/path5", "destination": "http://test.nl/path6/"},
                ]
            }
        }

        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_walled_garden_by_ip(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        walledGardenByIpEnabled
                        searchEngineIndexingEnabled
                    }
                }
            }
        """

        variables = {"input": {"walledGardenByIpEnabled": True}}

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(mutation, variables)
        site_settings = result["data"]["editSiteSetting"]["siteSettings"]

        self.assertEqual(site_settings["walledGardenByIpEnabled"], True)
        self.assertEqual(
            site_settings["searchEngineIndexingEnabled"],
            False,
        )

    def test_move_language_to_extra_languages(self):
        self.override_config(LANGUAGE="en", EXTRA_LANGUAGES=["nl"])
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        language
                        extraLanguages
                    }
                }
            }
        """
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(
            mutation, {"input": {"language": "nl", "extraLanguages": ["en", "nl"]}}
        )
        site_settings = result["data"]["editSiteSetting"]["siteSettings"]

        self.assertEqual(site_settings["language"], "nl")
        self.assertEqual(site_settings["extraLanguages"], ["en"])

    def test_edit_site_setting_invalid_extra_language(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        extraLanguages
                    }
                }
            }
        """

        variables = {"input": {"extraLanguages": ["invalidlanguage"]}}

        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)

    def test_edit_site_setting_invalid_max_characters(self):
        mutation = """
            mutation EditSiteSetting($input: editSiteSettingInput!) {
                editSiteSetting(input: $input) {
                    siteSettings {
                        maxCharactersInAbstract
                    }
                }
            }
        """

        variables = {"input": {"maxCharactersInAbstract": 1001}}

        with self.assertGraphQlError("invalid_value"):
            self.graphql_client.force_login(self.admin)
            self.graphql_client.post(mutation, variables)


class EditMenuSettingsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            CONTENT_TRANSLATION=True,
            LANGUAGE="nl",
            EXTRA_LANGUAGES=["en"],
        )

        self.mutation = """
        fragment MenuFragment on MenuItem {
            label
            translations {
                language
                label
            }
        }
        mutation UpdateMenuSettings ($input: editSiteSettingInput!) {
            editSiteSetting(input: $input) {
                siteSettings {
                    menu {
                        ...MenuFragment
                        children {
                            ...MenuFragment
                        }
                    }
                }
            }
        }
        """

        self.actor = AdminFactory()

    def run_mutation(self, variables):
        self.graphql_client.force_login(self.actor)
        result = self.graphql_client.post(self.mutation, variables)
        return result["data"]["editSiteSetting"]["siteSettings"]

    def test_update_without_translations(self):
        variables = {
            "input": {
                "menu": [
                    {
                        "label": "Nieuws",
                        "link": "/news",
                        "id": 1,
                        "parentId": None,
                    },
                    {
                        "label": "Blog",
                        "link": "/blog",
                        "parentId": 1,
                        "id": 2,
                    },
                ]
            }
        }

        data = self.run_mutation(variables)
        self.assertEqual(
            data["menu"],
            [
                {
                    "label": "Nieuws",
                    "translations": [],
                    "children": [
                        {
                            "label": "Blog",
                            "translations": [],
                        }
                    ],
                }
            ],
        )

    def test_update_with_translations(self):
        variables = {
            "input": {
                "menu": [
                    {
                        "label": "Nieuws",
                        "link": "/news",
                        "translations": [{"language": "en", "label": "News"}],
                        "id": 1,
                        "parentId": None,
                    },
                    {
                        "label": "Blog",
                        "link": "/blog",
                        "parentId": 1,
                        "id": 2,
                    },
                ]
            }
        }

        data = self.run_mutation(variables=variables)
        self.assertEqual(
            data["menu"],
            [
                {
                    "label": "Nieuws",
                    "translations": [
                        {
                            "language": "en",
                            "label": "News",
                        }
                    ],
                    "children": [
                        {
                            "label": "Blog",
                            "translations": [],
                        }
                    ],
                }
            ],
        )
        # Version of the current default language should be stored in the Configuration separately.
        self.assertEqual(
            config.MENU,
            [
                {
                    "access": None,
                    "label": "Nieuws",
                    "link": "/news",
                    "translations": [
                        {"language": "nl", "label": "Nieuws"},
                        {"language": "en", "label": "News"},
                    ],
                    "children": [
                        {
                            "access": None,
                            "label": "Blog",
                            "link": "/blog",
                            "translations": [{"language": "nl", "label": "Blog"}],
                            "children": [],
                        },
                    ],
                },
            ],
        )
