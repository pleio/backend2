from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestResolveOverwriteSiteOwnerTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.tenant.administrative_details = {}
        self.tenant.save()

        self.mutation = """
        mutation ($input: OverwriteSiteOwnerInput!){
            overwriteSiteOwner(input: $input) {
                siteOwnerName
                siteOwnerEmail
            }
        }
        """
        self.variables = {
            "input": {
                "siteOwnerName": "New Owner",
                "siteOwnerEmail": "new.owner@example.com",
            }
        }

    def test_resolve_set_site_owner_as_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_resolve_set_site_owner_as_non_admin(self):
        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.mutation, self.variables)

    @mock.patch("core.resolvers.mutations.mutation_overwrite_site_owner.send_mail")
    def test_resolve_set_site_owner(self, send_mail):
        # Given
        self.graphql_client.force_login(AdminFactory())

        # When
        result = self.graphql_client.post(self.mutation, self.variables)

        # Then
        self.assertEqual(
            result["data"]["overwriteSiteOwner"],
            {"siteOwnerName": "New Owner", "siteOwnerEmail": "new.owner@example.com"},
        )
        send_mail.assert_not_called()

    @mock.patch("core.resolvers.mutations.mutation_overwrite_site_owner.send_mail")
    def test_resolve_overwrite_site_owner(self, send_mail):
        # Given
        self.tenant.administrative_details = {
            "site_owner": "Old Owner",
            "site_owner_email": "old.owner@example.com",
        }
        self.tenant.save()
        admin = AdminFactory()
        self.graphql_client.force_login(admin)

        # When
        self.graphql_client.post(self.mutation, self.variables)

        # Then
        send_mail.assert_called_with(
            sender=admin,
            current_owner="old.owner@example.com",
        )
