from datetime import timedelta

from django.utils import timezone
from mixer.backend.django import mixer

from core.factories import GroupFactory
from core.lib import access_id_to_acl
from core.models import ProfileField, UserProfileField
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class UsersByBirthDateTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory(name="User 1")
        self.user2 = UserFactory(name="User 2")
        self.user3 = UserFactory(name="User 3")
        self.user4 = UserFactory(name="Xx")
        self.user5 = UserFactory(name="Public birthday")
        self.user6 = UserFactory(name="Private birthday")
        self.admin1 = AdminFactory(name="Yy")

        self.birthday_field = ProfileField.objects.create(
            key="birthday", name="birthday", field_type="date_field"
        )

        today = timezone.now()
        tomorrow = today + timedelta(days=1)
        overtomorrow = today + timedelta(days=2)
        next_months = today + timedelta(weeks=5)

        mixer.blend(
            UserProfileField,
            user_profile=self.user1.profile,
            profile_field=self.birthday_field,
            value=tomorrow.strftime("%Y-%m-%d"),
            read_access=access_id_to_acl(self.user1, 1),
        )
        mixer.blend(
            UserProfileField,
            user_profile=self.user3.profile,
            profile_field=self.birthday_field,
            value=today.strftime("%Y-%m-%d"),
            read_access=access_id_to_acl(self.user3, 1),
        )
        mixer.blend(
            UserProfileField,
            user_profile=self.user4.profile,
            profile_field=self.birthday_field,
            value=next_months.strftime("%Y-%m-%d"),
            read_access=access_id_to_acl(self.user4, 1),
        )
        mixer.blend(
            UserProfileField,
            user_profile=self.user5.profile,
            profile_field=self.birthday_field,
            value=overtomorrow.strftime("%Y-%m-%d"),
            read_access=access_id_to_acl(self.user5, 2),
        )
        mixer.blend(
            UserProfileField,
            user_profile=self.user6.profile,
            profile_field=self.birthday_field,
            value=overtomorrow.strftime("%Y-%m-%d"),
            read_access=access_id_to_acl(self.user6, 0),
        )

        self.query = """
        query usersByBirthDate(
            $profileFieldGuid: String!
            $groupGuid: String
            $futureDays: Int
            $offset: Int
            $limit: Int
        ) {
            usersByBirthDate(
                profileFieldGuid: $profileFieldGuid
                groupGuid: $groupGuid
                futureDays: $futureDays
                offset: $offset
                limit: $limit
            ) {
                edges {
                    name
                }
                total
            }
        }
        """

        self.variables = {
            "profileFieldGuid": str(self.birthday_field.guid),
        }

    def tearDown(self):
        super().tearDown()

    def test_users_by_birth_date_by_user(self):
        self.graphql_client.force_login(self.user2)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["usersByBirthDate"]["total"], 3)
        self.assertEqual(data["usersByBirthDate"]["edges"][0]["name"], self.user3.name)
        self.assertEqual(len(data["usersByBirthDate"]["edges"]), 3)

    def test_users_by_birth_date_by_user_future(self):
        self.variables["futureDays"] = 60

        self.graphql_client.force_login(self.user2)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["usersByBirthDate"]["total"], 4)
        self.assertEqual(data["usersByBirthDate"]["edges"][0]["name"], self.user3.name)
        self.assertEqual(len(data["usersByBirthDate"]["edges"]), 4)

    def test_users_by_birth_date_by_anonymous(self):
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]

        self.assertEqual(data["usersByBirthDate"]["total"], 1)
        self.assertEqual(data["usersByBirthDate"]["edges"][0]["name"], self.user5.name)
        self.assertEqual(len(data["usersByBirthDate"]["edges"]), 1)

    def test_users_by_birth_date_by_admin(self):
        self.variables["futureDays"] = 60

        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["usersByBirthDate"]["total"], 5)
        self.assertEqual(data["usersByBirthDate"]["edges"][0]["name"], self.user3.name)
        self.assertEqual(len(data["usersByBirthDate"]["edges"]), 5)

    def test_users_within_group(self):
        owner = UserFactory()
        group = GroupFactory(owner=owner)
        group.join(self.user1)
        group.join(self.user2)
        group.join(self.user3)

        self.variables["futureDays"] = 60
        self.variables["groupGuid"] = group.guid

        self.graphql_client.force_login(self.admin1)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(
            result["data"]["usersByBirthDate"]["edges"],
            [
                {"name": self.user3.name},
                {"name": self.user1.name},
            ],
        )
