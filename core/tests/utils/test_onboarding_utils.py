from datetime import date

from core.models import ProfileField
from core.tests.helpers import PleioTenantTestCase
from core.utils.onboarding import DateFieldType, MultiSelectFieldType


class TestOnboardingUtilsTestCase(PleioTenantTestCase):
    def test_multi_select_field_type(self):
        assert MultiSelectFieldType.TYPE in dict(ProfileField.FIELD_TYPES)

        type = MultiSelectFieldType(["Alpha", "Bravo", "Charlie"])
        self.assertEqual(str(type), "Alpha,Bravo,Charlie")

        type = MultiSelectFieldType(None)
        self.assertEqual(str(type), "")

        type = MultiSelectFieldType.from_string("Delta,Echo,Foxtrot")
        self.assertEqual(type.value, ["Delta", "Echo", "Foxtrot"])

    def test_date_field_type(self):
        assert DateFieldType.TYPE in dict(ProfileField.FIELD_TYPES)

        type = DateFieldType(date(2024, 1, 31))
        self.assertEqual(str(type), "2024-01-31")

        type = DateFieldType(None)
        self.assertEqual(str(type), "")

        type = DateFieldType.from_string("1999-05-21")
        self.assertIsInstance(type.value, date)
        self.assertEqual(type.value.year, 1999)
        self.assertEqual(type.value.month, 5)
        self.assertEqual(type.value.day, 21)
