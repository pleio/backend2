import json

from core.tests.helpers import PleioTenantTestCase
from core.utils.convert import html_to_tiptap


class TestHtmlToTiptap(PleioTenantTestCase):
    def test_no_html_to_tiptap(self):
        self.assertEqual(html_to_tiptap(None), None)

    def test_empty_html_to_tiptap(self):
        self.assertEqual(json.loads(html_to_tiptap("")), {"type": "doc", "content": []})

    def test_html_paragraph_to_tiptap(self):
        self.assertEqual(
            json.loads(html_to_tiptap("<p>Foo</p>")),
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [{"type": "text", "text": "Foo"}],
                    }
                ],
            },
        )

    def test_bold_paragraph_to_tiptap(self):
        self.assertEqual(
            json.loads(html_to_tiptap('<p class="bold">Foo</p>')),
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph_bold",
                        "content": [{"type": "text", "text": "Foo"}],
                    }
                ],
            },
        )

    def test_bold_classy_paragraph_to_tiptap(self):
        self.assertEqual(
            json.loads(html_to_tiptap('<p class="bold cheese">Foo</p>')),
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph_bold",
                        "content": [{"type": "text", "text": "Foo"}],
                    }
                ],
            },
        )

    def test_html_link_to_tiptap(self):
        self.assertEqual(
            json.loads(html_to_tiptap('<p><a href="https://example.com">Foo</a></p>')),
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [
                            {
                                "type": "text",
                                "text": "Foo",
                                "marks": [
                                    {
                                        "type": "link",
                                        "attrs": {
                                            "href": "https://example.com",
                                            "title": None,
                                            "target": None,
                                        },
                                    }
                                ],
                            }
                        ],
                    }
                ],
            },
        )

    def test_html_with_lang_attr_to_tiptap(self):
        result = html_to_tiptap('<p><span lang="en">Foo</span></p>')
        self.assertEqual(
            json.loads(result),
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [
                            {
                                "type": "text",
                                "text": "Foo",
                                "marks": [{"type": "lang", "attrs": {"lang": "en"}}],
                            }
                        ],
                    }
                ],
            },
        )

    def test_html_with_class_attr_to_tiptap(self):
        self.assertEqual(
            json.loads(html_to_tiptap('<span class="foo">Foo</span>')),
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [
                            {
                                "type": "text",
                                "marks": [
                                    {"type": "cssclass", "attrs": {"class": "foo"}}
                                ],
                                "text": "Foo",
                            }
                        ],
                    }
                ],
            },
        )
