from core.tests.helpers import PleioTenantTestCase
from core.utils.text_from_hierarchy import HierarchyFromText, TextFromHierarchy


class TestTextFromJsonTestCase(PleioTenantTestCase):
    def test_simple_text(self):
        translator = TextFromHierarchy("hi")
        self.assertEqual([*translator.get_text()], [])

    def test_list(self):
        translator = TextFromHierarchy([{"text": "content"}])
        self.assertEqual([*translator.get_text()], [("0.text.", "content")])

    def test_dict(self):
        translator = TextFromHierarchy({"text": "content"})
        self.assertEqual([*translator.get_text()], [("text.", "content")])

    def test_nested(self):
        translator = TextFromHierarchy({"hi": {"attribute": "foo", "text": "content"}})
        self.assertEqual([*translator.get_text()], [("hi.text.", "content")])

    def test_marked_lang(self):
        translator = TextFromHierarchy(
            [
                {"marks": [{"type": "lang"}], "text": "lang content"},
            ]
        )
        self.assertEqual(
            [*translator.get_text()],
            [("0.text.", "lang content")],
        )


class TestHierarchyFromTextTestCase(PleioTenantTestCase):
    def test_dict(self):
        translator = HierarchyFromText([("hi.text.", "content")])
        self.assertEqual(
            translator.apply_to(
                {
                    "hi": {
                        "text": "old content",
                        "attribute": "foo",
                    },
                },
            ),
            {
                "hi": {
                    "text": "content",
                    "attribute": "foo",
                },
            },
        )

    def test_list(self):
        translator = HierarchyFromText([("0.text.", "content")])
        self.assertEqual(
            translator.apply_to([{"text": "old content"}, {"text": "untouched"}]),
            [{"text": "content"}, {"text": "untouched"}],
        )

    def test_nested_mixed(self):
        translator = HierarchyFromText(
            [
                ("0.text.", "content"),
                ("1.hi.text.", "content2"),
            ]
        )
        self.assertEqual(
            translator.apply_to(
                [
                    {"text": "old content"},
                    {"hi": {"text": "other old content"}},
                    {"text": "untouched"},
                ]
            ),
            [
                {"text": "content"},
                {"hi": {"text": "content2"}},
                {"text": "untouched"},
            ],
        )

    def test_with_lang_attribute(self):
        translator = HierarchyFromText(
            [
                ("0.text.", "El palabra "),
                ("1.text.", "impressionante"),
                ("2.text.", " no se traduce"),
            ]
        )
        self.assertEqual(
            translator.apply_to(
                [
                    {"text": "Het woord "},
                    {"text": "awesome", "marks": [{"type": "lang", "value": "en"}]},
                    {"text": " wordt niet vertaald"},
                ]
            ),
            [
                {"text": "El palabra "},
                {"text": "awesome", "marks": [{"type": "lang", "value": "en"}]},
                {"text": " no se traduce"},
            ],
        )

    def test_with_codeblock(self):
        translator = HierarchyFromText(
            [
                ("0.content.0.text.", "El palabra "),
                ("1.content.0.text.", "impressionante"),
                ("2.content.0.text.", " no se traduce"),
            ]
        )
        self.assertEqual(
            translator.apply_to(
                [
                    {"content": [{"text": "Het woord "}]},
                    {"content": [{"text": "awesome"}], "type": "codeBlock"},
                    {"content": [{"text": " wordt niet vertaald"}]},
                ]
            ),
            [
                {"content": [{"text": "El palabra "}]},
                {"content": [{"text": "awesome"}], "type": "codeBlock"},
                {"content": [{"text": " no se traduce"}]},
            ],
        )
        self.assertEqual(
            translator.apply_to(
                [
                    {"content": [{"text": "Het woord "}]},
                    {"content": [{"text": "awesome"}], "type": "code"},
                    {"content": [{"text": " wordt niet vertaald"}]},
                ]
            ),
            [
                {"content": [{"text": "El palabra "}]},
                {"content": [{"text": "awesome"}], "type": "code"},
                {"content": [{"text": " no se traduce"}]},
            ],
        )
