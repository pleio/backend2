from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from core.utils.color import hex_black_or_white, hex_color_tint


class TestUtilsColorTestCase(PleioTenantTestCase):
    def test_hex_color_tint(self):
        self.assertEqual(hex_color_tint("#010203"), "#808081")
        self.assertEqual(hex_color_tint("#010203", 0.75), "#bfc0c0")

        with mock.patch("core.utils.color.Color") as color:
            color.side_effect = AttributeError()
            self.assertEqual(hex_color_tint("#010203"), "#010203")

    def test_hex_black_or_white(self):
        self.assertEqual(hex_black_or_white("#ffffff"), "#000000")
        self.assertEqual(hex_black_or_white("#000000"), "#ffffff")
        self.assertEqual(hex_black_or_white("#EDCCFB"), "#000000")
        self.assertEqual(hex_black_or_white(""), "#000000")
