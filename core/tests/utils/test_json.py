from django.utils.timezone import now

from core.tests.helpers import PleioTenantTestCase
from core.utils.json import json_dumps, json_loads


class TestJsonTestCase(PleioTenantTestCase):
    def test_json_with_date(self):
        datetime = now()
        date = datetime.date()
        data = {
            "date": date,
            "datetime": datetime,
        }

        json_stream = json_dumps(data)
        with self.subTest("json_dumps"):
            self.assertEqual(
                json_stream,
                '{"date": "%s", "datetime": "%s"}'
                % (date.isoformat(), datetime.isoformat()),
            )

        recovered_data = json_loads(json_stream)
        with self.subTest("json_loads"):
            self.assertEqual(data, recovered_data)
