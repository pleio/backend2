from unittest import mock

from django.utils import timezone

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from core.utils.content_moderation import ContentModerationTrackTimePublished
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestTimePublishedTrackerTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(CONTENT_MODERATION_ENABLED=True)
        self.entity = BlogFactory(
            owner=UserFactory(email="entity-owner@example.com"), published=None
        )
        self.tracker = ContentModerationTrackTimePublished(
            self.entity, self.entity.owner
        )

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished.can_update"
    )
    def test_maybe_revert_time_published_when_not_changed(self, mocked_can_update):
        self.tracker.maybe_revert_time_published()
        self.assertFalse(mocked_can_update.called)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished.can_update"
    )
    def test_maybe_revert_time_published_when_changed(self, mocked_can_update):
        mocked_can_update.return_value = True
        self.entity.published = timezone.now()
        self.tracker.maybe_revert_time_published()

        self.assertIsNotNone(self.entity.published)
        self.assertFalse(self.tracker._should_create_request)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished.can_update"
    )
    def test_maybe_revert_time_published_when_changed_and_can_not_update(
        self, mocked_can_update
    ):
        mocked_can_update.return_value = False
        self.entity.published = timezone.now()
        self.tracker.maybe_revert_time_published()

        self.assertIsNone(self.entity.published)
        self.assertTrue(self.tracker._should_create_request)

    @mock.patch("core.models.entity.PublishRequestManager.create")
    def test_can_update_not_creates_request(self, mocked_create_request):
        self.tracker._should_create_request = False
        self.tracker.maybe_create_publish_request()

        self.assertFalse(mocked_create_request.called)

    @mock.patch("core.models.entity.PublishRequestManager.create")
    def test_can_not_update_not_creates_request(self, mocked_create_request):
        self.tracker._should_create_request = True
        self.tracker.maybe_create_publish_request()

        self.assertTrue(mocked_create_request.called)

    def test_is_staff(self):
        for acting_user, expect_staff in (
            (UserFactory(email="user@example.com"), False),
            (EditorFactory(email="editor@example.com"), True),
            (AdminFactory(email="admin@example.com"), True),
        ):
            with self.subTest("Testing staff as %s" % acting_user.email):
                self.tracker.acting_user = acting_user
                self.assertEqual(self.tracker.is_staff(), expect_staff)

    def test_is_group_staff(self):
        # Given.
        group = GroupFactory(owner=UserFactory(email="group-owner@example.com"))
        admin = UserFactory(email="group-admin@example.com")
        group.join(admin, "admin")
        member = UserFactory(email="group-member@example.com")
        group.join(member)
        self.entity.group = group
        self.entity.save()

        for acting_user, expect_staff in (
            (group.owner, True),
            (admin, True),
            (member, False),
            (self.entity.owner, False),
        ):
            with self.subTest("Testing group staff as %s" % acting_user.email):
                # When.
                self.tracker.acting_user = acting_user

                # Then.
                self.assertEqual(self.tracker.is_group_staff(), expect_staff)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished.is_staff"
    )
    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished."
        "is_group_staff"
    )
    def test_can_update_uses_is_group_staff(self, is_group_staff, is_staff):
        is_staff.return_value = False
        is_group_staff.return_value = False
        self.override_config(REQUIRE_CONTENT_MODERATION_FOR=["blog"])
        self.tracker.published = timezone.now()

        result = self.tracker.can_update()

        self.assertFalse(result)
        self.assertTrue(is_staff.called)
        self.assertTrue(is_group_staff.called)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished.is_staff"
    )
    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished."
        "is_group_staff"
    )
    def test_can_update_uses_is_staff(self, is_group_staff, is_staff):
        is_staff.return_value = True
        is_group_staff.return_value = False
        self.override_config(REQUIRE_CONTENT_MODERATION_FOR=["blog"])
        self.tracker.published = timezone.now()

        result = self.tracker.can_update()

        self.assertTrue(result)
        self.assertTrue(is_staff.called)
        self.assertFalse(is_group_staff.called)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished."
        "_get_type_and_moderation_types"
    )
    def test_can_update(self, mocked_get_type):
        self.tracker.can_update()

        self.assertFalse(mocked_get_type.called)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished."
        "_get_type_and_moderation_types"
    )
    def test_get_can_update_when_changed(self, mocked_get_type):
        mocked_get_type.return_value = ("blog", [])
        self.tracker.published = timezone.now()
        self.tracker.can_update()

        self.assertTrue(mocked_get_type.called)

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished."
        "_get_type_and_moderation_types"
    )
    def test_get_can_update_when_changed_and_required_admin(self, mocked_get_type):
        for role in ["ADMIN", "EDITOR"]:
            mocked_get_type.return_value = ("blog", ["blog"])
            self.tracker.published = timezone.now()

            self.entity.owner.roles = []
            self.assertFalse(self.tracker.can_update())
            self.entity.owner.roles = [role]
            self.assertTrue(self.tracker.can_update())

    @mock.patch(
        "core.utils.content_moderation.ContentModerationTrackTimePublished."
        "_get_type_and_moderation_types"
    )
    def test_get_can_update_when_changed_and_not_required(self, mocked_get_type):
        mocked_get_type.return_value = ("blog", [])
        self.tracker.published = timezone.now()
        self.tracker.can_update()

        self.assertTrue(self.tracker.can_update())
