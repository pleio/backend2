from core.tests.helpers import PleioTenantTestCase
from core.utils import nested_form_input


class TestNestedFormInputDataParser(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.data = {
            "field_one[20][name]": "Charlie",
            "field_one[20][description]": "Charlie description",
            "field_one[20][duplicate]": "Delta",
            "field_one[10][name]": "Alpha",
            "field_one[10][description]": "Alpha description",
            "field_one[50][name]": "Bravo",
            "field_one[50][description]": "Bravo description",
            "field_two[10][1][foo][0]": "Echo description",
        }

    def test_parse(self):
        parsed = nested_form_input.parse(self.data)
        self.assertEqual(
            parsed,
            {
                "field_one": [
                    {"name": "Alpha", "description": "Alpha description"},
                    {
                        "name": "Charlie",
                        "description": "Charlie description",
                        "duplicate": "Delta",
                    },
                    {"name": "Bravo", "description": "Bravo description"},
                ],
                "field_two": [[{"foo": ["Echo description"]}]],
            },
        )

    def test_parse_keep_index(self):
        parsed = nested_form_input.parse(self.data, keep_index=True)
        self.assertEqual(
            parsed,
            {
                "field_one": {
                    "10": {"name": "Alpha", "description": "Alpha description"},
                    "50": {"name": "Bravo", "description": "Bravo description"},
                    "20": {
                        "name": "Charlie",
                        "description": "Charlie description",
                        "duplicate": "Delta",
                    },
                },
                "field_two": {"10": {"1": {"foo": {"0": "Echo description"}}}},
            },
        )
