from core.lib import get_full_url
from core.tests.helpers import PleioTenantTestCase
from core.utils.convert import filter_html_mail_input


class TestFilterMailInputTestCase(PleioTenantTestCase):
    def test_html_with_explicit_link(self):
        """A mail with explicit link should be converted to text"""

        # Given
        html = """<a href="https://www.example.com">Click here</a>"""

        # When
        safe_html = filter_html_mail_input(html)

        # Then
        self.assertEqual(safe_html, """Click here""")

    def test_html_with_implicit_link(self):
        """A mail with an implicit link should convert the implicit link to a clickable link"""

        # Given
        html = "Go to https://www.example.com"

        # When
        safe_html = filter_html_mail_input(html)

        # Then
        self.assertEqual(
            safe_html,
            """Go to <a href="https://www.example.com">https://www.example.com</a>""",
        )

    def test_html_with_local_link(self):
        """A mail with an explicit link to a local source should be accepted"""

        # Given
        home_page = get_full_url("/")
        html = f"""<a href="{home_page}">Visit our homepage</a>"""

        # When
        safe_html = filter_html_mail_input(html)

        # Then
        self.assertEqual(safe_html, html)
