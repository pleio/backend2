from unittest.mock import MagicMock, patch

from django.core.files.base import ContentFile

from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from entities.file.models import FileFolder
from user.factories import UserFactory


class Wrapper:
    class BaseTestCase(PleioTenantTestCase):
        def setUp(self):
            super().setUp()

            self.user = UserFactory()
            self.profile = self.user.profile
            self.avatar_content = ContentFile(b"123", "avatar.jpg")

        def get_fetcher(self, url):
            from core.utils.avatar import FetchAvatarFile

            return FetchAvatarFile(self.user, url)


class TestFetchAvatarFileTestCase(Wrapper.BaseTestCase):
    def setUp(self):
        super().setUp()

        self.client = MagicMock()
        self.client.response = MagicMock()
        self.client.response.ok = True
        self.client.get_file.return_value = self.avatar_content
        self.client_builder = patch("core.utils.avatar.ConciergeClient").start()
        self.client_builder.return_value = self.client

    def test_fetch_file(self):
        url = "https://example.com/avatar.jpg"
        fetcher = self.get_fetcher(url)
        fetcher._fetch_file()

        self.client_builder.assert_called_once_with("fetch avatar from url")
        self.client.get_file.assert_called_once_with(url + "?size=600")

        self.assertIsInstance(fetcher.file, FileFolder)
        self.assertEqual(fetcher.file.owner, self.user)
        self.assertEqual(fetcher.file.upload.read(), b"123")
        self.assertEqual(fetcher.file.title, "avatar.jpg")

    def test_fetch_file_error_response(self):
        self.client.response.ok = False
        url = "https://example.com/avatar.jpg"
        fetcher = self.get_fetcher(url)
        fetcher._fetch_file()

        self.assertIsNone(fetcher.file)

    def test_fetch_file_invalid_url(self):
        fetcher = self.get_fetcher(None)
        fetcher._fetch_file()

        self.assertEqual(self.client_builder.call_count, 0)
        self.assertIsNone(fetcher.file)


class TestFetchAvatarScanFileTestCase(Wrapper.BaseTestCase):
    def setUp(self):
        super().setUp()

        self.file = FileFactory(owner=self.user, upload=self.avatar_content)

        self.scan_mock = patch("core.utils.avatar.FileFolder.scan").start()
        self.scan_mock.return_value = True

        self.delete_mock = patch("core.utils.avatar.FileFolder.delete").start()

    def test_valid_file(self):
        fetcher = self.get_fetcher("https://example.com/avatar.jpg")
        fetcher.file = self.file

        fetcher._scan_file()

        self.scan_mock.assert_called_once_with()
        self.delete_mock.assert_not_called()
        self.assertEqual(fetcher.file, self.file)

    def test_invalid_file(self):
        fetcher = self.get_fetcher("https://example.com/avatar.jpg")
        fetcher.file = self.file
        self.scan_mock.return_value = False

        fetcher._scan_file()

        self.scan_mock.assert_called_once_with()
        self.delete_mock.assert_called_once_with()
        self.assertIsNone(fetcher.file)


class TestFetchAvatarPreserveFileTestCase(Wrapper.BaseTestCase):
    def setUp(self):
        super().setUp()

        self.file = FileFactory(owner=self.user, upload=self.avatar_content)

        self.update_mock = patch(
            "core.utils.avatar.FileReference.objects.update_avatar_file"
        ).start()

    def test_valid_file(self):
        fetcher = self.get_fetcher("https://example.com/avatar.jpg")
        fetcher.file = self.file

        fetcher._preserve_file()

        self.update_mock.assert_called_once_with(self.user, self.file)

    def test_invalid_file(self):
        fetcher = self.get_fetcher("https://example.com/avatar.jpg")
        fetcher.file = None

        fetcher._preserve_file()

        self.update_mock.assert_called_once_with(self.user, None)
