from unittest import mock

from mixer.backend.django import mixer

from core.constances import USER_ROLES
from core.models import SiteAccessRequest
from core.tests.helpers import PleioTenantTestCase
from user.models import User


class HandleSiteAccessRequestTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = mixer.blend(User)
        self.admin = mixer.blend(User, roles=[USER_ROLES.ADMIN])
        self.user_manager = mixer.blend(User, roles=[USER_ROLES.USER_ADMIN])
        self.request_manager = mixer.blend(User, roles=[USER_ROLES.REQUEST_MANAGER])
        self.request1 = mixer.blend(
            SiteAccessRequest,
            email="test1@pleio.nl",
            claims={"email": "test1@pleio.nl", "name": "Test 123", "sub": 1},
        )

        self.mutation = """
            mutation SiteAccessRequest($input: handleSiteAccessRequestInput!) {
                handleSiteAccessRequest(input: $input) {
                    success
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    @mock.patch(
        "core.resolvers.mutations.mutation_handle_site_access_request.schedule_site_access_request_accepted_mail"
    )
    def test_handle_access_request_by_admin(self, mocked_mail):
        variables = {
            "input": {
                "email": "test1@pleio.nl",
                "accept": True,
                "message": "<p>accepted !</p>",
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["handleSiteAccessRequest"]["success"], True)
        self.assertTrue(
            SiteAccessRequest.objects.filter(
                email=self.request1.email, accepted=True
            ).exists()
        )
        self.assertFalse(User.objects.filter(email=self.request1.email).exists())
        self.assertEqual(mocked_mail.call_count, 1)
        args, kwargs = mocked_mail.call_args
        self.assertEqual(kwargs["message"], "<p>accepted !</p>")

    @mock.patch(
        "core.resolvers.mutations.mutation_handle_site_access_request.schedule_site_access_request_denied_mail"
    )
    def test_handle_access_request_deny_by_admin(self, mocked_mail):
        variables = {
            "input": {
                "email": "test1@pleio.nl",
                "accept": False,
                "message": "<p>rejected !</p>",
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["handleSiteAccessRequest"]["success"], True)
        self.assertFalse(User.objects.filter(email=self.request1.email).exists())
        self.assertEqual(mocked_mail.call_count, 1)
        args, kwargs = mocked_mail.call_args
        self.assertEqual(kwargs["message"], "<p>rejected !</p>")

    @mock.patch(
        "core.resolvers.mutations.mutation_handle_site_access_request.schedule_site_access_request_denied_mail"
    )
    def test_handle_access_request_deny_silent_by_admin(self, mocked_mail):
        variables = {
            "input": {"email": "test1@pleio.nl", "accept": False, "silent": True}
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["handleSiteAccessRequest"]["success"], True)
        self.assertFalse(User.objects.filter(email=self.request1.email).exists())
        self.assertFalse(mocked_mail.called)

    def test_handle_access_request_by_user(self):
        variables = {"input": {"email": "test1@pleio.nl", "accept": True}}

        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, variables)

    def test_handle_access_request_by_anonymous(self):
        variables = {"input": {"email": "test1@pleio.nl", "accept": True}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)

    def test_handle_access_request_by_access_request_manager(self):
        variables = {
            "input": {"email": "test1@pleio.nl", "accept": True, "silent": True}
        }

        self.graphql_client.force_login(self.request_manager)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["handleSiteAccessRequest"]["success"], True)

    def test_handle_access_request_by_user_manager(self):
        variables = {
            "input": {"email": "test1@pleio.nl", "accept": True, "silent": True}
        }

        self.graphql_client.force_login(self.user_manager)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(data["handleSiteAccessRequest"]["success"], True)
