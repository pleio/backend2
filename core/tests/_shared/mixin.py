from unittest.mock import patch

from django.test import TestCase


def _assert(self, callback, *args, **kwargs):
    bool_true_or_str_error = callback(*args, **kwargs)
    if bool_true_or_str_error is not True:
        self.fail(bool_true_or_str_error)


class MixinTestCase:
    class AssertCanUploadMedia(TestCase):
        def setUp(self):
            super().setUp()
            self.assert_can_upload_media = patch(
                "core.resolvers.shared.assert_can_upload_media"
            ).start()

        def assert_can_upload_media_called_once_with(
            self, user, entity_valid, input, _reset=False
        ):
            self.assertEqual(1, len(self.assert_can_upload_media.mock_calls))
            self.assertEqual(3, len(self.assert_can_upload_media.mock_calls[0].args))
            call_args = self.assert_can_upload_media.mock_calls[0].args

            # compare first argument
            self.assertEqual(call_args[0], user)

            # compare second argument
            _assert(self, entity_valid, call_args[1])

            # compare relevant part of third argument
            self.assertIn(
                ("richDescription", input["richDescription"]),
                call_args[2].items(),
            )
            if _reset:
                self.assert_can_upload_media.reset_mock()

        def assert_can_upload_media_not_called(self):
            self.assertEqual(
                0,
                len(self.assert_can_upload_media.mock_calls),
                msg="Unexpectedly counting %s call(s) for %s"
                % (
                    len(self.assert_can_upload_media.mock_calls),
                    self.assert_can_upload_media,
                ),
            )
