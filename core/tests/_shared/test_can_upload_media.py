from django.core.files.base import ContentFile
from graphql import GraphQLError

from core.resolvers.shared import assert_can_upload_media
from core.tests.helpers import PleioTenantTestCase, tiptap
from entities.file.factories import FileFactory
from entities.question.factories import QuestionFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestCanUploadMediaTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(PREVENT_INSERT_MEDIA_AT_QUESTIONS=True)

    def build_media_types(self, user):
        file = FileFactory(
            owner=user, upload=ContentFile(b"Not a real jpg", "real.jpg")
        )
        return [
            tiptap.File(file),
            tiptap.Video("FooGuid", "BarPlatform"),
            tiptap.Picture(file.download_url),
        ]

    def assertCanSaveData(self, user, entity, clean_input):
        try:
            assert_can_upload_media(user, entity, clean_input)
        except Exception as e:
            self.fail(f"Unexpectedly could not save data because of {e}")

    def assertCanNotSaveData(self, user, entity, clean_input):
        try:
            assert_can_upload_media(user, entity, clean_input)
            self.fail("Unexpectedly could save data")
        except GraphQLError as e:
            if "could_not_save" in e.message:
                return
            self.fail("Unexpectedly got %s stating %s" % (type(e), e))

    def test_can_upload_media_without_setting(self):
        self.override_config(PREVENT_INSERT_MEDIA_AT_QUESTIONS=False)
        for user in (
            UserFactory(email="user@example.com"),
            EditorFactory(email="editor@example.com"),
            AdminFactory(email="admin@example.com"),
        ):
            entity = QuestionFactory(owner=user)
            with self.subTest(msg=" At %s" % user.email):
                clean_input = {
                    "richDescription": tiptap.RichDescription(
                        tiptap.Text("Text"), *self.build_media_types(user)
                    ).as_json()
                }
                self.assertCanSaveData(user, entity, clean_input)

    def test_assert_can_upload_non_media_with_setting(self):
        for user in (
            UserFactory(email="user@example.com"),
            EditorFactory(email="editor@example.com"),
            AdminFactory(email="admin@example.com"),
        ):
            entity = QuestionFactory(owner=user)
            with self.subTest(msg=" At %s" % user.email):
                clean_input = {
                    "richDescription": tiptap.RichDescription(
                        tiptap.Text("Text")
                    ).as_json()
                }
                self.assertCanSaveData(user, entity, clean_input)

    def test_assert_can_upload_media_with_setting(self):
        for user, can_save in (
            (UserFactory(email="user@example.com"), False),
            (EditorFactory(email="editor@example.com"), False),
            (AdminFactory(email="admin@example.com"), True),
        ):
            entity = QuestionFactory(owner=user)
            for tiptap_object, msg in (
                (mt, type(mt)) for mt in self.build_media_types(user)
            ):
                with self.subTest(msg=" At %s saving a %s" % (user.email, msg)):
                    clean_input = {
                        "richDescription": tiptap.RichDescription(
                            tiptap.Text("Text"), tiptap_object
                        ).as_json()
                    }
                    if can_save:
                        self.assertCanSaveData(user, entity, clean_input)
                    else:
                        self.assertCanNotSaveData(user, entity, clean_input)
