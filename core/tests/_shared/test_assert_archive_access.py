from unittest.mock import Mock, call

from graphql import GraphQLError

from core.resolvers.shared import assert_archive_access
from core.tests.helpers import PleioTenantTestCase


class TestAssertArchiveAccessTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.entity = Mock()
        self.user = Mock()

    def test_positive_feedback(self):
        self.entity.can_archive.return_value = True

        try:
            assert_archive_access(self.entity, self.user)
        except GraphQLError as e:
            self.fail(
                f"assert_archive_access() raised {e} unexpectedly"
            )  # pragma: no cover

        self.assertTrue(self.entity.can_archive.called)
        self.assertEqual(self.entity.can_archive.mock_calls, [call(self.user)])

    def test_negative_feedback(self):
        self.entity.can_archive.return_value = False

        try:
            assert_archive_access(self.entity, self.user)
            self.fail(
                "assert_archive_access() unexpectedly did not raise GraphQLError"
            )  # pragma: no cover
        except GraphQLError as e:
            self.assertEqual(e.message, "could_not_save")

        self.assertTrue(self.entity.can_archive.called)
        self.assertEqual(self.entity.can_archive.mock_calls, [call(self.user)])
