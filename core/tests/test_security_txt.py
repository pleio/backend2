from django.urls import reverse
from django_tenants.utils import schema_context

from control.models import Configuration
from core.tests.helpers import PleioTenantTestCase, override_config
from user.factories import UserFactory


class Wrapper:
    class TestSecurityTxtBase(PleioTenantTestCase):
        def setUp(self):
            super().setUp()
            self.user = self.build_user()

        def tearDown(self):
            with schema_context("public"):
                Configuration.objects.all().delete()

        def build_user(self):
            return None

        def acquire_page(self):
            if self.user:
                self.client.force_login(self.user)
            return self.client.get(reverse("security_txt"))

        def acquire_page_pgp_key(self):
            if self.user:
                self.client.force_login(self.user)
            return self.client.get(reverse("security_txt_pgp"))

        def test_access_page_when_on_no_configuration(self):
            response = self.acquire_page()

            self.assertEqual(response.status_code, 404)

        @override_config(SECURITY_TEXT="")
        def test_access_page_with_proper_configuration(self):
            VALUE = "blah!"
            with schema_context("public"):
                Configuration.objects.create(id="security_txt", value=VALUE)

            response = self.acquire_page()
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.headers["content-type"], "text/plain")
            self.assertEqual(response.getvalue().decode(), VALUE)

        @override_config(
            SECURITY_TEXT="Laalaah! \n new line", SECURITY_TEXT_ENABLED=True
        )
        def test_access_page_with_custom_configuration(self):
            VALUE = "blah!"
            with schema_context("public"):
                Configuration.objects.create(id="security_txt", value=VALUE)

            response = self.acquire_page()
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.headers["content-type"], "text/plain")
            self.assertEqual(response.getvalue().decode(), "Laalaah! \n new line")

        @override_config(SECURITY_TEXT="Laalaah!", SECURITY_TEXT_ENABLED=False)
        def test_access_page_with_custom_configuration_disabled(self):
            VALUE = "blah!"
            with schema_context("public"):
                Configuration.objects.create(id="security_txt", value=VALUE)

            response = self.acquire_page()
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.headers["content-type"], "text/plain")
            self.assertEqual(response.getvalue().decode(), VALUE)

        @override_config(
            SECURITY_TEXT="Laalaah!",
            SECURITY_TEXT_ENABLED=False,
            SECURITY_TEXT_PGP="-----BEGIN PGP PUBLIC KEY BLOCK-----\n\nmQINBFMPxPfYZVsgN\nehFL8UgqiP58X\nZ5VTY/V44H7zznm\n....\n-----END PGP PUBLIC KEY BLOCK-----",
        )
        def test_custom_scurity_text_pgp_key(self):
            response = self.acquire_page_pgp_key()
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.headers["content-type"], "text/plain")
            self.assertEqual(
                response.getvalue().decode(),
                "-----BEGIN PGP PUBLIC KEY BLOCK-----\n\nmQINBFMPxPfYZVsgN\nehFL8UgqiP58X\nZ5VTY/V44H7zznm\n....\n-----END PGP PUBLIC KEY BLOCK-----",
            )

        @override_config(
            SECURITY_TEXT_REDIRECT_ENABLED=True,
            SECURITY_TEXT_REDIRECT_URL="https://test.url",
        )
        def test_security_text_redirect(self):
            response = self.acquire_page()
            self.assertEqual(response.url, "https://test.url")
            self.assertEqual(response.status_code, 302)


class TestSecurityTxtPublicSite(Wrapper.TestSecurityTxtBase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=False)


class TestSecurityTxtProtectedSite(Wrapper.TestSecurityTxtBase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=True)


class TestSecurityTxtLoggedIn(Wrapper.TestSecurityTxtBase):
    def setUp(self):
        super().setUp()
        self.override_config(IS_CLOSED=True)

    def build_user(self):
        return UserFactory()
