from django.core.files.base import ContentFile

from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestAlterFeaturedImageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.old_image = FileFactory(
            owner=self.owner, upload=ContentFile(b"", "test.jpg")
        )
        self.new_image = FileFactory(
            owner=self.owner, upload=ContentFile(b"", "test-new.jpg")
        )
        self.article = BlogFactory(owner=self.owner, featured_image=self.old_image)

        self.mutation = """
        mutation ModifyBlog($input: editEntityInput!) {
            editEntity(input: $input) {
                entity { guid }
            }
        }
        """

        self.variables = {
            "input": {
                "guid": self.article.guid,
                "featured": {"imageGuid": self.new_image.guid},
            }
        }

    def test_valid_featured_image_file(self):
        with self.assertNotRaisesException():
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_non_existing_featured_image_file(self):
        self.new_image.delete()

        with self.assertGraphQlError("could_not_find"):
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_invalid_featured_image_file(self):
        new_image = FileFactory(
            owner=self.owner, upload=ContentFile(b"", "test-new.csv")
        )
        self.variables["input"]["featured"]["imageGuid"] = new_image.guid

        with self.assertGraphQlError("not_an_image"):
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)
