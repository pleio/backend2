from unittest import mock

from mixer.backend.django import mixer

from core.tests.helpers import PleioTenantTestCase
from entities.event.factories import EventFactory
from user.factories import UserFactory


class VideoCallTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.user = UserFactory()
        self.moderator_user = UserFactory()
        self.event = EventFactory(
            owner=self.owner,
            title="eventtitle",
            rich_description="content",
            abstract="abstract",
        )

        self.video_call = mixer.blend("core.EntityVideoCall", entity=self.event)
        self.moderator = mixer.blend(
            "core.EntityVideoCallModerator",
            video_call=self.video_call,
            user=self.moderator_user,
        )

        self.variables = {"guid": self.video_call.guid}
        self.query = """
            query videoCall($guid: String!) {
                videoCall(guid: $guid) {
                    guid
                    jwtToken
                    isModerator
                    title
                }
            }
        """

    def test_video_call_anonymous(self):
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]

        self.assertEqual(data["videoCall"]["guid"], self.video_call.guid)
        self.assertEqual(data["videoCall"]["jwtToken"], None)
        self.assertEqual(data["videoCall"]["isModerator"], False)
        self.assertEqual(data["videoCall"]["title"], "eventtitle")

    def test_video_call_user(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]

        self.assertEqual(data["videoCall"]["guid"], self.video_call.guid)
        self.assertEqual(data["videoCall"]["jwtToken"], None)
        self.assertEqual(data["videoCall"]["isModerator"], False)
        self.assertEqual(data["videoCall"]["title"], "eventtitle")

    @mock.patch("core.models.videocall.EntityVideoCall.get_jwt_token")
    def test_video_call_moderator(self, mocked_get_token):
        mocked_get_token.return_value = "mockedToken1234"
        self.graphql_client.force_login(self.moderator_user)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["videoCall"]["guid"], self.video_call.guid)
        self.assertEqual(data["videoCall"]["isModerator"], True)
        self.assertEqual(data["videoCall"]["jwtToken"], "mockedToken1234")
        self.assertEqual(data["videoCall"]["title"], "eventtitle")

    @mock.patch("core.models.videocall.EntityVideoCall.get_jwt_token")
    def test_video_call_entity_owner(self, mocked_get_token):
        mocked_get_token.return_value = "mockedToken1234"
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["videoCall"]["guid"], self.video_call.guid)
        self.assertEqual(data["videoCall"]["isModerator"], True)
        self.assertEqual(data["videoCall"]["jwtToken"], "mockedToken1234")
        self.assertEqual(data["videoCall"]["title"], "eventtitle")
