from unittest.mock import MagicMock, patch

from django.contrib.auth.models import AnonymousUser
from django.core.files.base import ContentFile

from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, UserFactory


class TestUserAvatar(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        # Testing with anonymous user, so we test with the site gates open.
        self.override_config(IS_CLOSED=False)

        self.query = """
            fragment UserProperties on Blog {
                owner {
                    icon
                }
            }
            query UserViaEntity($guid: String!) {
                entity(guid: $guid) {
                    ... UserProperties
                }
            }
        """

    def build_user_blog(self, access_id=None, with_avatar_file=False):
        avatar = None
        user = UserFactory(email="owner@example.com")
        if with_avatar_file:
            with patch("concierge.api.ConciergeClient") as client_builder:
                client = MagicMock()
                client.get_file.return_value = ContentFile(b"test123", "avatar.jpg")
                client.response = MagicMock()
                client.response.status_code = 200
                client.response.ok = True
                client_builder.return_value = client

                user.profile.update_avatar_access(access_id)
                user.profile.update_avatar("https://example.com/avatar.jpg")
                avatar = user.profile.avatar_file
        return BlogFactory(owner=user), user, avatar

    def post_query(self, user, via_content_guid):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(
            self.query,
            {
                "guid": via_content_guid,
            },
        )
        return result["data"]["entity"]["owner"]

    def assert_no_access(self, user_data, avatar, expected_access):
        self.assertEqual(user_data, {"icon": None})

    def assert_access(self, user_data, avatar, expected_access):
        self.assertIn(avatar.download_url, user_data["icon"])

    def test_user_private_icon(self):
        blog, user, avatar = self.build_user_blog(with_avatar_file=True, access_id=0)

        for actor, assert_fn in [
            (user, self.assert_no_access),
            (AnonymousUser(), self.assert_no_access),
            (UserFactory(email="another_user@example.com"), self.assert_no_access),
            (AdminFactory(email="admin@example.com"), self.assert_no_access),
        ]:
            with self.subTest(msg="Actor: %s" % actor):
                user_data = self.post_query(actor, blog.guid)
                assert_fn(user_data, avatar, "private")

    def test_user_logged_in_icon(self):
        blog, user, avatar = self.build_user_blog(with_avatar_file=True, access_id=1)

        for actor, assert_fn in [
            (user, self.assert_access),
            (AnonymousUser(), self.assert_no_access),
            (UserFactory(email="another_user@example.com"), self.assert_access),
            (AdminFactory(email="admin@example.com"), self.assert_access),
        ]:
            with self.subTest(msg="Actor: %s" % actor):
                user_data = self.post_query(actor, blog.guid)
                assert_fn(user_data, avatar, "loggedIn")

    def test_user_public_icon(self):
        blog, user, avatar = self.build_user_blog(with_avatar_file=True, access_id=2)

        for actor, assert_fn in [
            (user, self.assert_access),
            (AnonymousUser(), self.assert_access),
            (UserFactory(email="another_user@example.com"), self.assert_access),
            (AdminFactory(email="admin@example.com"), self.assert_access),
        ]:
            with self.subTest(msg="Actor: %s" % actor):
                user_data = self.post_query(actor, blog.guid)
                assert_fn(user_data, avatar, "public")
