from unittest.mock import patch

from django.contrib.auth.models import AnonymousUser

from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class TestQueryFetchVideoMetadataTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.mutation = """
        query FetchVideoMetadata($url: String!) {
            fetchVideoMetadata(input: {url: $url}) {
                metadata {
                    thumbnailUrl
                }
            }
        }
        """
        self.variables = {"url": "https://www.youtube.com/watch?v=aboZctrHfK8"}

        self.mocked_get_data = patch(
            "core.models.featured.FeaturedCoverMixin.get_video_thumbnail_url"
        ).start()
        self.mocked_get_data.return_value = "https://www.example.com/thumbnail.jpg"

    def get_metadata(self, acting_user):
        self.graphql_client.force_login(acting_user)
        response = self.graphql_client.post(self.mutation, self.variables)
        return response["data"]["fetchVideoMetadata"]

    def test_as_anonymous(self):
        """
        Test that anonymous users have no access to the service.
        """
        with self.assertGraphQlError("not_logged_in"):
            self.get_metadata(AnonymousUser())

    def test_as_authenticated(self):
        """
        Test that the metadata is fetched using the appropriate channel.
        """
        result = self.get_metadata(UserFactory())
        self.assertTrue(self.mocked_get_data.called)
        self.assertEqual(self.mocked_get_data.call_args[0][0], self.variables["url"])
        self.assertEqual(
            result["metadata"]["thumbnailUrl"], "https://www.example.com/thumbnail.jpg"
        )
