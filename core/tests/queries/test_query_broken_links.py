from mixer.backend.django import mixer

from core.models import BrokenLink
from core.tests.helpers import PleioTenantTestCase
from user.models import User


class BrokenLinksTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = mixer.blend(User, roles=[], is_delete_requested=False)
        self.admin = mixer.blend(User, roles=["ADMIN"], is_delete_requested=False)

        self.links = mixer.cycle(11).blend(
            BrokenLink,
            reason=BrokenLink.Reasons.DOES_NOT_EXIST,
            source=mixer.sequence(lambda c: "https://source/%s" % c),
            target=mixer.sequence(lambda c: "https://target/%s" % c),
        )

        self.query = """
            query BrokenLinks($limit: Int, $offset: Int) {
                brokenLinks(limit: $limit, offset: $offset) {
                    total
                    edges {
                        id
                        source
                        target
                        reason
                    }
                }
            }
        """

    def test_by_user(self):
        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.query, {})

    def test_by_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"limit": 10, "offset": 0})

        data = result["data"]["brokenLinks"]
        self.assertEqual(data["total"], 11)
        self.assertEqual(len(data["edges"]), 10)
