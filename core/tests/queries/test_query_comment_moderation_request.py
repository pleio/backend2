from unittest.mock import call, patch

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone
from django.utils.timezone import timedelta

from core.factories import CommentFactory
from core.models.comment import CommentModerationRequest
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestQueryCommentModerationRequestTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.admin = AdminFactory()
        self.editor = EditorFactory()
        self.participant = UserFactory()

        self.open_request = self.create_moderation_request(
            number_of_comments=3,
            _entity=BlogFactory(owner=self.owner, title="Open request"),
        )
        self.assigned_request = self.create_moderation_request(
            number_of_comments=3,
            _entity=BlogFactory(owner=self.owner, title="Assigned request"),
            assigned_to=self.admin,
            assigned_until=timezone.now() + timedelta(hours=1),
        )
        self.assigned_due_request = self.create_moderation_request(
            number_of_comments=3,
            _entity=BlogFactory(owner=self.owner, title="Assigned due request"),
            assigned_to=self.admin,
            assigned_until=timezone.now() - timedelta(hours=1),
        )
        self.closed_request = self.create_moderation_request(
            number_of_comments=0,
            _entity=BlogFactory(owner=self.owner, title="Closed request"),
        )
        self.closed_assigned_request = self.create_moderation_request(
            number_of_comments=0,
            _entity=BlogFactory(owner=self.owner, title="Closed assigned request"),
            assigned_to=self.admin,
            assigned_until=timezone.now() + timedelta(hours=1),
        )

        self.query = """
            query CommentModerationRequests(
                $limit: Int
                $offset: Int
                $status: CommentModerationStatusEnum
                $userGuid: String
            ) {
                commentModerationRequests(
                    limit: $limit
                    offset: $offset
                    status: $status
                    userGuid: $userGuid
                ) {
                    total
                    edges {
                        guid
                        assignedTo { guid }
                        entity { ... on Blog { guid, title } }
                        comments { guid }
                        status
                    }
                }
            }
        """

    def create_moderation_request(self, number_of_comments, **kwargs):
        moderation_request = CommentModerationRequest.objects.create(**kwargs)
        for _ in range(number_of_comments):
            moderation_request.comments.add(
                CommentFactory(
                    container=moderation_request.entity, owner=self.participant
                )
            )
        return moderation_request

    @patch("core.models.comment.CommentModerationRequestQuerySet.visible")
    def test_moderation_requests(self, mocked_visible):
        mocked_visible.return_value = CommentModerationRequest.objects.none()
        self.graphql_client.post(self.query, {})

        self.assertEqual(mocked_visible.call_count, 1)
        self.assertEqual(mocked_visible.mock_calls, [call(AnonymousUser())])

    @patch("core.models.comment.CommentModerationRequestQuerySet.visible")
    def test_moderation_requests_as_authenticated_visitor(self, mocked_visible):
        user = UserFactory()
        mocked_visible.return_value = CommentModerationRequest.objects.none()
        self.graphql_client.force_login(user)
        self.graphql_client.post(self.query, {})

        self.assertEqual(mocked_visible.call_count, 1)
        self.assertEqual(mocked_visible.mock_calls, [call(user)])

    @patch("core.models.comment.CommentModerationRequestQuerySet.filter_owner")
    def test_moderation_request_without_owner_filter(self, mocked_filter_owner):
        mocked_filter_owner.return_value = CommentModerationRequest.objects.none()
        self.graphql_client.post(self.query, {})

        self.assertEqual(mocked_filter_owner.call_count, 0)

    @patch("core.models.comment.CommentModerationRequestQuerySet.filter_owner")
    def test_moderation_request_with_owner_filter(self, mocked_filter_owner):
        mocked_filter_owner.return_value = CommentModerationRequest.objects.none()
        self.graphql_client.post(self.query, {"userGuid": "123"})

        self.assertEqual(mocked_filter_owner.call_count, 1)
        self.assertEqual(mocked_filter_owner.mock_calls, [call("123")])

    def test_moderation_requests_by_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {})
        data = result["data"]["commentModerationRequests"]

        self.assertEqual(data["total"], 5)
        self.assertEqual(len(data["edges"]), 5)
        self.assertEqual(
            [e["entity"]["title"] for e in data["edges"]],
            [
                "Closed assigned request",
                "Closed request",
                "Assigned due request",
                "Assigned request",
                "Open request",
            ],
        )

    def test_moderation_requests_by_editor(self):
        self.graphql_client.force_login(self.editor)
        result = self.graphql_client.post(self.query, {})
        data = result["data"]["commentModerationRequests"]

        self.assertEqual(data["total"], 5)
        self.assertEqual(len(data["edges"]), 5)
        self.assertEqual(
            [e["entity"]["title"] for e in data["edges"]],
            [
                "Closed assigned request",
                "Closed request",
                "Assigned due request",
                "Assigned request",
                "Open request",
            ],
        )

    def test_moderation_requests_limit(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"limit": 2})
        data = result["data"]["commentModerationRequests"]

        self.assertEqual(data["total"], 5)
        self.assertEqual(len(data["edges"]), 2)
        self.assertEqual(
            [e["entity"]["title"] for e in data["edges"]],
            [
                "Closed assigned request",
                "Closed request",
            ],
        )

    def test_moderation_requests_offset(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"offset": 2})
        data = result["data"]["commentModerationRequests"]

        self.assertEqual(data["total"], 5)
        self.assertEqual(len(data["edges"]), 3)
        self.assertEqual(
            [e["entity"]["title"] for e in data["edges"]],
            [
                "Assigned due request",
                "Assigned request",
                "Open request",
            ],
        )

    def test_status_filter(self):
        self.graphql_client.force_login(self.admin)

        for status, expected_titles in (
            ("open", ["Assigned due request", "Open request"]),
            ("assigned", ["Assigned request"]),
            ("closed", ["Closed assigned request", "Closed request"]),
        ):
            with self.subTest(message=status):
                result = self.graphql_client.post(self.query, {"status": status})
                data = result["data"]["commentModerationRequests"]
                self.assertEqual(
                    [e["entity"]["title"] for e in data["edges"]],
                    expected_titles,
                    msg="Failed at status: {}".format(status),
                )

    def test_moderation_requests_offset_limit(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"offset": 2, "limit": 2})
        data = result["data"]["commentModerationRequests"]

        self.assertEqual(data["total"], 5)
        self.assertEqual(len(data["edges"]), 2)
        self.assertEqual(
            [e["entity"]["title"] for e in data["edges"]],
            [
                "Assigned due request",
                "Assigned request",
            ],
        )

    def test_moderation_requests_content(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {})
        data = result["data"]["commentModerationRequests"]

        requests = {e["guid"]: e for e in data["edges"]}
        self.assertEqual(
            requests[self.open_request.guid],
            {
                "guid": self.open_request.guid,
                "entity": {
                    "guid": self.open_request.entity.guid,
                    "title": "Open request",
                },
                "assignedTo": None,
                "comments": [
                    {"guid": c.guid} for c in self.open_request.comments.all()
                ],
                "status": "open",
            },
        )
        self.assertEqual(
            requests[self.assigned_request.guid],
            {
                "guid": self.assigned_request.guid,
                "entity": {
                    "guid": self.assigned_request.entity.guid,
                    "title": "Assigned request",
                },
                "assignedTo": {"guid": self.admin.guid},
                "comments": [
                    {"guid": c.guid} for c in self.assigned_request.comments.all()
                ],
                "status": "assigned",
            },
        )
        self.assertEqual(
            requests[self.assigned_due_request.guid],
            {
                "guid": self.assigned_due_request.guid,
                "entity": {
                    "guid": self.assigned_due_request.entity.guid,
                    "title": "Assigned due request",
                },
                "assignedTo": None,
                "comments": [
                    {"guid": c.guid} for c in self.assigned_due_request.comments.all()
                ],
                "status": "open",
            },
        )
        self.assertEqual(
            requests[self.closed_request.guid],
            {
                "guid": self.closed_request.guid,
                "entity": {
                    "guid": self.closed_request.entity.guid,
                    "title": "Closed request",
                },
                "assignedTo": None,
                "comments": [],
                "status": "closed",
            },
        )
        self.assertEqual(
            requests[self.closed_assigned_request.guid],
            {
                "guid": self.closed_assigned_request.guid,
                "entity": {
                    "guid": self.closed_assigned_request.entity.guid,
                    "title": "Closed assigned request",
                },
                "assignedTo": None,
                "comments": [],
                "status": "closed",
            },
        )


class TestCommentCount(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            COMMENT_MODERATION_ENABLED=True, REQUIRE_COMMENT_MODERATION_FOR=["blog"]
        )
        self.owner = UserFactory()
        self.article = BlogFactory(owner=self.owner)
        self.moderation_request = CommentModerationRequest.objects.create(
            entity=self.article
        )
        self.comment = CommentFactory(
            container=self.article,
            owner=self.owner,
            comment_moderation_request=self.moderation_request,
        )

        self.query = """
            query GetBlog($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ... on Blog {
                        commentCount
                    }
                }
            }
        """
        self.variables = {"guid": str(self.article.guid)}

    def test_comment_counter_with_comments_in_moderation(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["commentCount"], 0)

    def test_comment_counter_without_comments_in_moderation(self):
        self.comment.comment_moderation_request = None
        self.comment.save()
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["commentCount"], 1)
