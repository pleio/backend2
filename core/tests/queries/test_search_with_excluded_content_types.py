from django.test import tag

from core.tests.helpers import ElasticsearchTestCase
from user.factories import AdminFactory, UserFactory


class Template:
    @tag("search_query")
    class TestSearchWithExcludedContentTypesTestCase(ElasticsearchTestCase):
        EXCLUDE_TYPES = []

        def build_included_article(self, title):
            raise NotImplementedError()

        def build_excluded_article(self, title):
            raise NotImplementedError()

        def get_exclude_types(self):
            return self.EXCLUDE_TYPES

        def setUp(self):
            super().setUp()

            self.owner = UserFactory(email="oscar@example.com")
            self.acting_user = AdminFactory()

            self.override_config(
                SEARCH_EXCLUDED_CONTENT_TYPES=["user", *self.get_exclude_types()]
            )

            self.included = {
                "alpha": self.build_included_article("alpha"),
                "bravo": self.build_included_article("bravo"),
                "charlie": self.build_included_article("charlie"),
            }
            self.excluded = {
                "delta": self.build_excluded_article("delta"),
                "echo": self.build_excluded_article("echo"),
                "foxtrot": self.build_excluded_article("foxtrot"),
            }

            self.query = """
            fragment SearchGroup on Group { title: name }
            fragment SearchUser on User { title: name }
            fragment SearchBlog on Blog { title }
            fragment SearchPage on Page { title }
            fragment SearchEpisode on Episode { title }
            fragment SearchDiscussion on Discussion { title }
            fragment SearchEvent on Event { title }
            fragment SearchExternalContent on ExternalContent { title }
            fragment SearchFile on File { title }
            fragment SearchFolder on Folder { title }
            fragment SearchPad on Pad { title }
            fragment SearchPodcast on Podcast { title }
            fragment SearchMagazineIssue on MagazineIssue { title }
            fragment SearchNews on News { title }
            fragment SearchQuestion on Question { title }
            fragment SearchWiki on Wiki { title }
            fragment SearchStatusUpdate on StatusUpdate { title }
            query Search {
                search(q: "") {
                    edges {
                        ...SearchGroup
                        ...SearchUser
                        ...SearchBlog
                        ...SearchPage
                        ...SearchEpisode
                        ...SearchDiscussion
                        ...SearchEvent
                        ...SearchExternalContent
                        ...SearchFile
                        ...SearchFolder
                        ...SearchPad
                        ...SearchPodcast
                        ...SearchMagazineIssue
                        ...SearchNews
                        ...SearchQuestion
                        ...SearchWiki
                        ...SearchStatusUpdate
                    }
                }
            }
            """
            self.populate_index()

        def run_query(self):
            self.graphql_client.force_login(self.acting_user)
            response = self.graphql_client.post(self.query)
            return response["data"]["search"]["edges"]

        def test_search(self):
            edges = self.run_query()

            self.assertEqual(len(edges), len(self.included))
            self.assertEqual({n["title"] for n in edges}, set(self.included.keys()))
