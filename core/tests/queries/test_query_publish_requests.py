from unittest.mock import call, patch

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone

from core.models import PublishRequest
from core.tests.helpers import PleioTenantTestCase, override_config
from entities.blog.factories import BlogFactory
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    UserFactory,
    UserManagerFactory,
)


class TestQueryPublishRequests(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.query = """
        query FetchPublishRequests(
                $offset: Int,
                $limit: Int,
                $status: PublishRequestStatusEnum,
                $files: Boolean
                $userGuid: String
            ) {
            publishRequests(
                    offset: $offset,
                    limit: $limit,
                    status: $status,
                    files: $files
                    userGuid: $userGuid
                ) {
                total
                edges {
                    guid
                    author { guid }
                    assignedTo { guid }
                    entity {
                        ... on Blog {
                            title
                        }
                    }
                    publishedAt,
                    status
                }
            }
        }
        """

        self.owner = UserFactory()
        self.admin = AdminFactory()
        self.editor = EditorFactory()
        self.open_request = PublishRequest.objects.create(
            entity=BlogFactory(owner=self.owner, published=None, title="Draft blog"),
        )
        self.assigned_request = PublishRequest.objects.create(
            entity=BlogFactory(owner=self.owner, published=None, title="Assigned blog"),
            assigned_to=self.admin,
        )
        """ Blog published still being assigned to someone.
            Has no particular meaning, but it is a valid state.
            It should behave as "published".
        """
        self.closed_request = PublishRequest.objects.create(
            entity=BlogFactory(
                owner=self.owner, published=timezone.now(), title="Published blog"
            ),
            assigned_to=self.admin,
        )
        self.closed_unassigned = PublishRequest.objects.create(
            entity=BlogFactory(
                owner=self.owner,
                published=timezone.now(),
                title="Published unassigned blog",
            ),
            assigned_to=None,
        )

    def test_anonymous_user_has_no_access(self):
        for user, user_description in (
            (AnonymousUser(), "anonymous"),
            (UserFactory(), "authenticated"),
            (NewsEditorFactory(), "news_editor"),
            (UserManagerFactory(), "user_manager"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.query, {})
            publish_requests = result["data"]["publishRequests"]

            self.assertEqual(
                publish_requests["total"], 0, msg="as %s" % user_description
            )
            self.assertEqual(
                publish_requests["edges"], [], msg="as %s" % user_description
            )

    def test_get_publish_requests(self):
        for user, msg in (
            (self.owner, "as owner"),
            (self.admin, "as admin"),
            (self.editor, "as editor"),
        ):
            with self.subTest(message=msg):
                self.graphql_client.force_login(user)
                response = self.graphql_client.post(self.query, {})

                # Test expected number of items
                self.assertEqual(response["data"]["publishRequests"]["total"], 2)

                # Test expected sort order
                self.assertEqual(
                    [
                        edge["entity"]["title"]
                        for edge in response["data"]["publishRequests"]["edges"]
                    ],
                    [
                        "Assigned blog",
                        "Draft blog",
                    ],
                )

                # Test expected content.
                mapped_edges = {
                    edge["guid"]: edge
                    for edge in response["data"]["publishRequests"]["edges"]
                }
                self.assertEqual(
                    mapped_edges[self.open_request.guid],
                    {
                        "guid": self.open_request.guid,
                        "author": {"guid": self.owner.guid},
                        "assignedTo": None,
                        "entity": {"title": "Draft blog"},
                        "publishedAt": self.open_request.time_published.isoformat(),
                        "status": "open",
                    },
                )
                self.assertEqual(
                    mapped_edges[self.assigned_request.guid],
                    {
                        "guid": self.assigned_request.guid,
                        "author": {"guid": self.owner.guid},
                        "assignedTo": {"guid": self.admin.guid},
                        "entity": {"title": "Assigned blog"},
                        "publishedAt": self.assigned_request.time_published.isoformat(),
                        "status": "assigned",
                    },
                )

    def test_get_publish_requests_with_status_filter(self):
        self.graphql_client.force_login(self.admin)
        for status, expected_blogs in (
            ("open", ["Draft blog"]),
            ("assigned", ["Assigned blog"]),
            ("published", ["Published unassigned blog", "Published blog"]),
        ):
            error_message = "At {} status filter".format(status)
            with self.subTest(message=error_message):
                response = self.graphql_client.post(self.query, {"status": status})
                self.assertEqual(
                    response["data"]["publishRequests"]["total"],
                    len(expected_blogs),
                    msg=error_message,
                )
                self.assertEqual(
                    [
                        edge["entity"]["title"]
                        for edge in response["data"]["publishRequests"]["edges"]
                    ],
                    expected_blogs,
                    msg=error_message,
                )

    def create_open_request(self, title):
        return PublishRequest.objects.create(
            entity=BlogFactory(owner=self.owner, published=None, title=title),
        )

    def test_limit(self):
        self.create_open_request("Draft blog 2")
        self.create_open_request("Draft blog 3")
        self.create_open_request("Draft blog 4")

        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query, {"limit": 2})

        self.assertEqual(response["data"]["publishRequests"]["total"], 5)
        self.assertEqual(
            [
                edge["entity"]["title"]
                for edge in response["data"]["publishRequests"]["edges"]
            ],
            ["Draft blog 4", "Draft blog 3"],
        )

    def test_offset(self):
        self.create_open_request("Draft blog 2")
        self.create_open_request("Draft blog 3")
        self.create_open_request("Draft blog 4")

        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query, {"offset": 2})
        self.assertEqual(response["data"]["publishRequests"]["total"], 5)
        self.assertEqual(
            [
                edge["entity"]["title"]
                for edge in response["data"]["publishRequests"]["edges"]
            ],
            ["Draft blog 2", "Assigned blog", "Draft blog"],
        )

    def test_offset_limit(self):
        self.create_open_request("Draft blog 2")
        self.create_open_request("Draft blog 3")
        self.create_open_request("Draft blog 4")

        self.graphql_client.force_login(self.admin)
        response = self.graphql_client.post(self.query, {"offset": 1, "limit": 2})

        self.assertEqual(response["data"]["publishRequests"]["total"], 5)
        self.assertEqual(
            [
                edge["entity"]["title"]
                for edge in response["data"]["publishRequests"]["edges"]
            ],
            ["Draft blog 3", "Draft blog 2"],
        )

    @override_config(CONTENT_MODERATION_ENABLED=True)
    def test_entity_blog_with_publication_request_by_editor(self):
        blog = BlogFactory(
            owner=UserFactory(), published=None, rich_description="jsoneee"
        )
        editor = EditorFactory()
        PublishRequest.objects.create(
            entity=blog,
        )
        query = """
            fragment BlogParts on Blog {
                title
                richDescription
            }
            query GetBlog($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...BlogParts
                }
            }
        """
        variables = {"guid": blog.guid}

        self.graphql_client.force_login(editor)
        result = self.graphql_client.post(query, variables)

        data = result["data"]

        self.assertEqual(data["entity"]["guid"], blog.guid)
        self.assertEqual(data["entity"]["title"], blog.title)
        self.assertEqual(data["entity"]["richDescription"], blog.rich_description)

    @patch("core.models.entity.PublishRequestQuerySet.filter_files")
    @patch("core.models.entity.PublishRequestQuerySet.exclude_files")
    def test_with_files(self, exclude_files, filter_files):
        filter_files.return_value = PublishRequest.objects.none()

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.query, {"files": True})

        filter_files.assert_called_once()
        exclude_files.assert_not_called()

    @patch("core.models.entity.PublishRequestQuerySet.filter_files")
    @patch("core.models.entity.PublishRequestQuerySet.exclude_files")
    def test_without_files(self, exclude_files, filter_files):
        exclude_files.return_value = PublishRequest.objects.none()

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.query)

        filter_files.assert_not_called()
        exclude_files.assert_called_once()

    @patch("core.models.entity.PublishRequestQuerySet.filter_owner")
    def test_without_owner_filter(self, mocked_filter_owner):
        mocked_filter_owner.return_value = PublishRequest.objects.none()

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.query, {})

        self.assertEqual(mocked_filter_owner.call_count, 0)

    @patch("core.models.entity.PublishRequestQuerySet.filter_owner")
    def test_with_owner_filter(self, mocked_filter_owner):
        mocked_filter_owner.return_value = PublishRequest.objects.none()

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.query, {"userGuid": self.owner.guid})

        self.assertEqual(mocked_filter_owner.call_count, 1)
        self.assertEqual(mocked_filter_owner.mock_calls, [call(self.owner.guid)])
