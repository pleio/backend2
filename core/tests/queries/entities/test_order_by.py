from django.core.files.base import ContentFile
from django.utils import timezone
from django.utils.timezone import timedelta
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, ORDER_BY
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.magazine.factories import MagazineFactory
from user.factories import EditorFactory, UserFactory


def _days_ago(days):
    return timezone.now() - timedelta(days=days)


class OrderByBaseTestCases:
    class OrderByTestCaseBase(PleioTenantTestCase):
        ORDER_BY_VALUE = None
        SUBTYPES = None

        def setUp(self):
            super().setUp()
            self.owner = self.build_owner()

        def build_owner(self):
            return UserFactory(email="owner@example.com")

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def query(self):
            return """
                fragment AnyEntity on Entity {
                    guid
                    ... on StatusUpdate    { title }
                    ... on Blog            { title }
                    ... on Page            { title }
                    ... on Discussion      { title }
                    ... on ExternalContent { title }
                    ... on Event           { title }
                    ... on FileFolder      { title }
                    ... on File            { size  }
                    ... on Magazine        { title }
                    ... on MagazineIssue   { title }
                    ... on News            { title }
                    ... on Podcast         { title }
                    ... on Episode         { title }
                    ... on Poll            { title }
                    ... on Question        { title }
                    ... on Task            { title }
                    ... on Wiki            { title }
                }
                query entities($orderBy: OrderBy, $subTypes: [String]) {
                    asc_entities: entities(orderBy: $orderBy, orderDirection: asc, subtypes: $subTypes) {
                        edges {
                            ...AnyEntity
                        }
                    }
                    desc_entities: entities(orderBy: $orderBy, orderDirection: desc, subtypes: $subTypes) {
                        edges {
                            ...AnyEntity
                        }
                    }
                }
            """

        def variables(self):
            return {
                "orderBy": self.ORDER_BY_VALUE,
                **({"subTypes": self.SUBTYPES} if self.SUBTYPES else {}),
            }

        def execute(self, user, variables=None):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(
                self.query(), variables or self.variables()
            )
            return result["data"]

    class CommonOrderByTestCase(OrderByTestCaseBase):
        def test_order_by(self):
            result = self.execute(self.owner)
            with self.subTest(msg="Ascending order"):
                self.assertEqual(
                    [edge["title"] for edge in result["asc_entities"]["edges"]],
                    self.EXPECTED_ASC,
                    msg="Actual order differs from expected ascending order",
                )
            with self.subTest(msg="Descending order"):
                self.assertEqual(
                    [edge["title"] for edge in result["desc_entities"]["edges"]],
                    self.EXPECTED_DESC,
                    msg="Actual order differs from expected descending order",
                )

        EXPECTED_ASC = []
        EXPECTED_DESC = []

    class UnavailableOrderByTestCase(OrderByTestCaseBase):
        EXPECTED_ERROR_MESSAGE = None

        def test_order_by(self):
            with self.assertGraphQlError(self.EXPECTED_ERROR_MESSAGE):
                self.execute(self.owner)

    class TestOrderByTimeCreated(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.timeCreated

        def setUp(self):
            super().setUp()

            self.entity_one = self.build_entity(
                owner=self.owner, title="Alpha", created_at=_days_ago(1)
            )
            self.entity_two = self.build_entity(
                owner=self.owner, title="Bravo", created_at=_days_ago(10)
            )
            self.entity_three = self.build_entity(
                owner=self.owner, title="Charlie", created_at=_days_ago(5)
            )
            self.entity_four = self.build_entity(
                owner=self.owner, title="Delta", created_at=_days_ago(2)
            )

        EXPECTED_ASC = ["Bravo", "Charlie", "Delta", "Alpha"]
        EXPECTED_DESC = ["Alpha", "Delta", "Charlie", "Bravo"]

    class TestOrderByTimeUpdated(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.timeUpdated

        def setUp(self):
            super().setUp()

            self.entity_one = self.build_entity(
                owner=self.owner,
                title="Alpha",
                updated_at=_days_ago(2),
            )
            self.entity_two = self.build_entity(
                owner=self.owner, title="Bravo", updated_at=_days_ago(1)
            )
            self.entity_three = self.build_entity(
                owner=self.owner, title="Charlie", updated_at=_days_ago(4)
            )
            self.entity_four = self.build_entity(
                owner=self.owner, title="Delta", updated_at=_days_ago(3)
            )

        EXPECTED_ASC = ["Charlie", "Delta", "Alpha", "Bravo"]
        EXPECTED_DESC = ["Bravo", "Alpha", "Delta", "Charlie"]

    class TestOrderByTimePublished(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.timePublished

        def setUp(self):
            super().setUp()

            self.build_entity(owner=self.owner, title="Alpha", published=_days_ago(10))
            self.build_entity(owner=self.owner, title="Bravo", published=_days_ago(1))
            self.build_entity(owner=self.owner, title="Charlie", published=_days_ago(4))
            self.build_entity(owner=self.owner, title="Delta", published=_days_ago(3))

        EXPECTED_ASC = ["Alpha", "Charlie", "Delta", "Bravo"]
        EXPECTED_DESC = ["Bravo", "Delta", "Charlie", "Alpha"]

    class TestOrderByLastAction(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.lastAction

        def setUp(self):
            super().setUp()

            self.build_entity(
                owner=self.owner, title="Alpha", last_action=_days_ago(12)
            )
            self.build_entity(
                owner=self.owner, title="Bravo", last_action=_days_ago(11)
            )
            self.build_entity(
                owner=self.owner, title="Charlie", last_action=_days_ago(4)
            )
            self.build_entity(
                owner=self.owner,
                title="Delta",
                last_action=_days_ago(3),
            )

        EXPECTED_ASC = ["Alpha", "Bravo", "Charlie", "Delta"]
        EXPECTED_DESC = ["Delta", "Charlie", "Bravo", "Alpha"]

    class TestOrderByLastSeen(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.lastSeen

        def setUp(self):
            super().setUp()

            self.entity_one = self.build_entity(owner=self.owner, title="Alpha")
            self.entity_one.views.create(viewer=self.owner, created_at=_days_ago(10))
            self.entity_two = self.build_entity(owner=self.owner, title="Bravo")
            self.entity_two.views.create(viewer=self.owner, created_at=_days_ago(1))
            self.entity_three = self.build_entity(owner=self.owner, title="Charlie")
            self.entity_three.views.create(viewer=self.owner, created_at=_days_ago(4))
            self.entity_four = self.build_entity(owner=self.owner, title="Delta")
            self.entity_four.views.create(viewer=self.owner, created_at=_days_ago(3))

        EXPECTED_ASC = ["Alpha", "Charlie", "Delta", "Bravo"]
        EXPECTED_DESC = ["Bravo", "Delta", "Charlie", "Alpha"]

    class TestOrderByTitle(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.title

        def setUp(self):
            super().setUp()

            self.entity_one = self.build_entity(owner=self.owner, title="Alpha")
            self.entity_two = self.build_entity(owner=self.owner, title="Bravo")
            self.entity_three = self.build_entity(owner=self.owner, title="Charlie")
            self.entity_four = self.build_entity(owner=self.owner, title="Delta")

        EXPECTED_ASC = ["Alpha", "Bravo", "Charlie", "Delta"]
        EXPECTED_DESC = ["Delta", "Charlie", "Bravo", "Alpha"]

    class TestOrderByGroupName(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.groupName

        def add_group_to_entity(self, entity, group_name):
            group = GroupFactory(owner=UserFactory(), name=group_name)
            entity.group = group
            entity.save()
            return entity

        def setUp(self):
            super().setUp()

            self.add_group_to_entity(
                self.build_entity(owner=self.owner, title="Alpha"),
                group_name="Zulu",
            )
            self.add_group_to_entity(
                self.build_entity(owner=self.owner, title="Bravo"),
                group_name="Echo",
            )
            self.add_group_to_entity(
                self.build_entity(owner=self.owner, title="Charlie"),
                group_name="Yankee",
            )
            self.add_group_to_entity(
                self.build_entity(owner=self.owner, title="Delta"),
                group_name="November",
            )

        EXPECTED_ASC = ["Bravo", "Delta", "Charlie", "Alpha"]
        EXPECTED_DESC = ["Alpha", "Charlie", "Delta", "Bravo"]

    class TestOrderByOwnerName(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.ownerName

        def entity_with_owner(self, entity_title, owner_name):
            owner = UserFactory(name=owner_name, email=f"{owner_name}@example.com")
            return self.build_entity(owner=owner, title=entity_title)

        def setUp(self):
            super().setUp()

            self.entity_with_owner("Alpha", owner_name="carol")
            self.entity_with_owner("Bravo", owner_name="bart")
            self.entity_with_owner("Charlie", owner_name="albertus")
            self.entity_with_owner("Delta", owner_name="david")

        EXPECTED_ASC = ["Charlie", "Bravo", "Alpha", "Delta"]
        EXPECTED_DESC = ["Delta", "Alpha", "Bravo", "Charlie"]

    class TestOrderByReadAccess(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.readAccess

        def build_group_entity(self, **kwargs):
            return self.build_entity(owner=self.owner, **kwargs)

        def setUp(self):
            super().setUp()
            group = GroupFactory(
                owner=UserFactory(),
                name="Group for read_access tests",
            )
            group.join(self.owner)

            user_access = ACCESS_TYPE.user.format(self.owner.id)
            group_access = ACCESS_TYPE.group.format(group.id)
            public_access = ACCESS_TYPE.public
            member_access = ACCESS_TYPE.logged_in

            self.build_group_entity(
                title="Alpha", group=group, read_access=[user_access]
            )
            self.build_group_entity(
                title="Bravo", group=group, read_access=[group_access]
            )
            self.build_group_entity(
                title="Charlie", group=group, read_access=[public_access]
            )
            self.build_group_entity(
                title="Delta", group=group, read_access=[member_access]
            )

        EXPECTED_ASC = ["Charlie", "Delta", "Bravo", "Alpha"]
        EXPECTED_DESC = ["Alpha", "Bravo", "Delta", "Charlie"]

    class TestOrderByStartDate(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.startDate

        def variables(self):
            return {**super().variables(), "subTypes": ["event"]}

        def setUp(self):
            super().setUp()

            self.build_entity(
                owner=self.owner,
                title="Alpha",
                start_date=_days_ago(10),
            )
            self.build_entity(
                owner=self.owner,
                title="Bravo",
                start_date=_days_ago(1),
            )
            self.build_entity(
                owner=self.owner,
                title="Charlie",
                start_date=_days_ago(30),
            )
            self.build_entity(
                owner=self.owner,
                title="Delta",
                start_date=_days_ago(3),
            )

        EXPECTED_ASC = ["Charlie", "Alpha", "Delta", "Bravo"]
        EXPECTED_DESC = ["Bravo", "Delta", "Alpha", "Charlie"]

    class TestOrderByStartDateNotAvailable(UnavailableOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.startDate
        EXPECTED_ERROR_MESSAGE = "order_by_start_date_is_only_for_events"

    class TestOrderByFileSize(CommonOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.fileSize

        def setUp(self):
            super().setUp()

            self.build_entity(
                owner=self.owner,
                title="Alpha",
                upload=ContentFile(b"1234567890", "Alpha.txt"),
            )
            self.build_entity(
                owner=self.owner,
                title="Bravo",
                upload=ContentFile(b"12345", "Bravo.txt"),
            )
            self.build_entity(
                owner=self.owner,
                title="Charlie",
                upload=ContentFile(b"1234567", "Charlie.txt"),
            )
            self.build_entity(
                owner=self.owner,
                title="Delta",
                upload=ContentFile(b"123", "Delta.txt"),
            )

        EXPECTED_ASC = ["Delta", "Bravo", "Charlie", "Alpha"]
        EXPECTED_DESC = ["Alpha", "Charlie", "Bravo", "Delta"]

    class TestOrderByFileSizeNotAvailable(UnavailableOrderByTestCase):
        ORDER_BY_VALUE = ORDER_BY.fileSize
        EXPECTED_ERROR_MESSAGE = "order_by_size_is_only_for_files"


class TestOrderByContentType(OrderByBaseTestCases.CommonOrderByTestCase):
    ORDER_BY_VALUE = ORDER_BY.contentType

    def create(self, _model, title=None, **kwargs):
        return mixer.blend(
            _model,
            owner=self.owner,
            title=title,
            read_access=[ACCESS_TYPE.public],
            **kwargs,
        )

    def setUp(self):
        super().setUp()

        self.create("activity.StatusUpdate", "Alpha")
        self.create("blog.Blog", "Bravo")
        self.create("cms.Page", "Charlie", page_type="text")
        self.create("cms.Page", "Delta", page_type="campagne")
        self.create("discussion.Discussion", "Echo")
        ec_source = self.create("external_content.ExternalContentSource", name="Yankee")
        self.create("external_content.ExternalContent", "Foxtrot", source=ec_source)
        ec_source = self.create("external_content.ExternalContentSource", name="Zulu")
        self.create("external_content.ExternalContent", "Golf", source=ec_source)
        self.create("event.Event", "Hotel", start_date=_days_ago(1))
        self.create("file.FileFolder", "India", type="File")
        magazine = MagazineFactory(owner=EditorFactory(), title="X-Ray")
        self.create("magazine.MagazineIssue", "Juliet", container=magazine)
        self.create("news.News", "Kilo")
        podcast = self.create("podcast.Podcast", "Whiskey")
        self.create("podcast.Episode", "Lima", _podcast=podcast)
        self.create("poll.Poll", "Mike")
        self.create("question.Question", "November")
        self.create("task.Task", "Oscar")
        self.create("wiki.Wiki", "Papa")

    EXPECTED_ASC = [
        "India",  # Bestand
        "Bravo",  # Blog
        "Echo",  # Discussie
        "Hotel",  # Evenement
        "X-Ray",  # Magazine
        "Juliet",  # Magazine uitgave
        "Kilo",  # Nieuws
        "Charlie",  # Pagina (text)
        "Delta",  # Pagina (widget)
        "Mike",  # Poll
        "Lima",  # Podcast episode
        "Alpha",  # StatusUpdate
        "Oscar",  # Taak
        "November",  # Vraag
        "Papa",  # Wiki
        "Foxtrot",  # ExternalContent: Yankee
        "Golf",  # ExternalContent: Zulu
    ]

    EXPECTED_DESC = [
        "Golf",  # ExternalContent: Zulu
        "Foxtrot",  # ExternalContent: Yankee
        "Papa",  # Wiki
        "November",  # Vraag
        "Oscar",  # Taak
        "Alpha",  # StatusUpdate
        "Lima",  # Podcast Episode
        "Mike",  # Poll
        "Delta",  # Pagina (widget)
        "Charlie",  # Pagina (text)
        "Kilo",  # Nieuws
        "Juliet",  # Magazine uitgave
        "X-Ray",  # Magazine
        "Hotel",  # Evenement
        "Echo",  # Discussie
        "Bravo",  # Blog
        "India",  # Bestand
    ]
