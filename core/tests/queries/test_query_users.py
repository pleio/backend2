from django.core.files.base import ContentFile

from core.factories import GroupFactory
from core.tests.helpers import ElasticsearchTestCase, PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    QuestionManagerFactory,
    UserFactory,
    UserManagerFactory,
)

USER_FIELDS = """
"""


class TestQueryUsersFilterGroup(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.other_group = GroupFactory(owner=self.owner)

        self.member = UserFactory()
        self.pending_member = UserFactory()
        self.non_member_1 = UserFactory()
        self.non_member_2 = UserFactory()

        self.group.join(self.member)
        self.group.join(self.pending_member, "pending")

        self.other_group.join(self.pending_member)

        self.query = """
        query SiteUsers($q: String!, $groupGuid: String) {
            users(q: $q, groupGuid: $groupGuid) {
                edges { guid }
            }
        }
        """

        self.variables = {"q": ""}

    def test_query_all_users(self):
        """
        Test that all known users are available on a clean user query.
        """
        self.populate_index()
        self.wait_for_records(index="user", count=5, timeout=1)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)
        users = {e["guid"] for e in result["data"]["users"]["edges"]}

        self.assertEqual(
            users,
            {
                self.owner.guid,
                self.member.guid,
                self.pending_member.guid,
                self.non_member_1.guid,
                self.non_member_2.guid,
            },
        )

    def test_query_group_users(self):
        """
        Test that only members of the given group are returned in the query, not pending members.
        """
        self.variables["groupGuid"] = self.group.guid

        self.populate_index()
        self.wait_for_records(index="user", count=5, timeout=1)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)
        users = {e["guid"] for e in result["data"]["users"]["edges"]}

        self.assertEqual(users, {self.owner.guid, self.member.guid})


class TestUserQueryResult(PleioTenantTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()

        self.query = """
        fragment UserFields on User {
            guid
            status
            username
            name
            email
            emailNotifications
            emailNotificationsFrequency
            isMentionNotificationsEnabled
            isMentionNotificationPushEnabled
            isMentionNotificationDirectMailEnabled
            isCommentNotificationsEnabled
            isCommentNotificationPushEnabled
            isCommentNotificationDirectMailEnabled
            isChatGroupNotificationPushEnabled
            isChatUsersNotificationPushEnabled
            isChatOnline
            isChatOnlineOverride
            getsNewsletter
            emailOverview {
                frequency
                tags
                tagCategories { name, values }
            }
            profile { guid, fieldType, value }
            vcard { guid, fieldType, value }
            stats { key, name, value }
            groupNotifications {
                guid
                name
                isNotificationsEnabled
                isNotificationPushEnabled
                isNotificationDirectMailEnabled
            }
            icon
            url
            canEdit
            canDelete
            canBan
            requestDelete
            roles
            language
            lastOnline
            memberSince
            isSuperadmin
            profileSetManager { id, name, field { guid } }
            isBanned
            banReason
            bannedSince
            banScheduledAt
            banScheduledReason
        }
        query SiteUser($query: String) {
            siteUsers(q: $query) {
                edges {
                    ...UserFields
                }
            }
        }
        """
        self.variables = {"query": "Alpha"}

        self.user = UserFactory(
            name="Alpha",
            email="alpha@example.com",
        )
        self.profile = self.user.profile
        self.profile.avatar_managed_file = FileFactory(
            owner=self.user, upload=ContentFile(b"", "avatar.png")
        )
        self.profile.save()

    def commonly_readable_fields(self):
        return {
            "guid": self.user.guid,
            "name": "Alpha",
            "banReason": "",
            "banScheduledReason": None,
            "banScheduledAt": None,
            "bannedSince": None,
            "email": "alpha@example.com",
            "emailNotifications": True,
            "emailNotificationsFrequency": 4,
            "emailOverview": {
                "frequency": "weekly",
                "tags": [],
                "tagCategories": [],
            },
            "getsNewsletter": False,
            "groupNotifications": [],
            "icon": self.user.icon,
            "isBanned": False,
            "isChatGroupNotificationPushEnabled": False,
            "isChatOnline": False,
            "isChatOnlineOverride": False,
            "isChatUsersNotificationPushEnabled": True,
            "isCommentNotificationDirectMailEnabled": False,
            "isCommentNotificationPushEnabled": False,
            "isCommentNotificationsEnabled": True,
            "isMentionNotificationDirectMailEnabled": False,
            "isMentionNotificationPushEnabled": False,
            "isMentionNotificationsEnabled": True,
            "isSuperadmin": False,
            "language": "nl",
            "lastOnline": None,
            "memberSince": self.user.created_at.isoformat(),
            "profile": [],
            "profileSetManager": [],
            "requestDelete": False,
            "roles": [],
            "stats": [],
            "status": 200,
            "url": f"/user/{self.user.guid}/profile",
            "username": self.user.guid,
            "vcard": [],
        }

    def get_user(self, acting_user):
        self.graphql_client.force_login(acting_user)
        result = self.graphql_client.post(self.query, self.variables)
        return result["data"]["siteUsers"]["edges"][0]

    def test_query_user_as_self(self):
        user_data = self.get_user(self.user)

        self.assertEqual(
            user_data,
            {
                **self.commonly_readable_fields(),
                "canBan": False,
                "canDelete": False,
                "canEdit": True,
            },
        )

    def commonly_protected_fields(self):
        return {
            "canBan": None,
            "canDelete": None,
            "canEdit": False,
            "emailNotifications": None,
            "emailNotificationsFrequency": None,
            "emailOverview": None,
            "getsNewsletter": None,
            "isChatGroupNotificationPushEnabled": None,
            "isChatOnlineOverride": None,
            "isChatUsersNotificationPushEnabled": None,
            "isCommentNotificationDirectMailEnabled": None,
            "isCommentNotificationPushEnabled": None,
            "isCommentNotificationsEnabled": None,
            "isMentionNotificationDirectMailEnabled": None,
            "isMentionNotificationPushEnabled": None,
            "isMentionNotificationsEnabled": None,
        }

    def test_query_user_as_administrator(self):
        user_data = self.get_user(AdminFactory())
        self.assertEqual(
            user_data,
            {
                **self.commonly_readable_fields(),
                "canBan": True,
                "canDelete": True,
                "canEdit": True,
            },
        )

    def test_query_user_as_user_manager(self):
        user_data = self.get_user(UserManagerFactory())

        self.assertEqual(
            user_data,
            {
                **self.commonly_readable_fields(),
                **self.commonly_protected_fields(),
                "canBan": True,
                "canDelete": True,
            },
        )

    def test_query_user_as_non_privileged_user(self):
        for factory in [
            UserFactory,
            EditorFactory,
            QuestionManagerFactory,
            NewsEditorFactory,
        ]:
            with self.subTest(str(factory)):
                user_data = self.get_user(acting_user=factory())
                self.assertEqual(
                    user_data,
                    {
                        **self.commonly_readable_fields(),
                        **self.commonly_protected_fields(),
                        "banReason": None,
                        "email": None,
                        "isBanned": None,
                        "isSuperadmin": None,
                        "language": None,
                        "requestDelete": None,
                        "roles": None,
                    },
                )


class TestQueryUnicodeUsers(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.other_group = GroupFactory(owner=self.owner)

        self.user1 = UserFactory(name="Celine")
        self.user2 = UserFactory(name="Cëline")
        self.user3 = UserFactory(name="Celíne")
        self.user4 = UserFactory(name="Kéline")
        self.user5 = UserFactory(name="Kooline")
        self.user5 = UserFactory(name="Mandoline")

        self.query = """
        query SiteUsers($q: String!) {
            users(q: $q) {
                edges { guid }
            }
        }
        """

    def test_query_all_users(self):
        """
        Test that all known users are available on a clean user query.
        """
        self.populate_index()

        self.wait_for_records(index="user", count=7, timeout=1)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"q": "celine"})
        users = {e["guid"] for e in result["data"]["users"]["edges"]}

        self.assertEqual(len(users), 3)
        self.assertIn(self.user1.guid, users)
        self.assertIn(self.user2.guid, users)
        self.assertIn(self.user3.guid, users)
