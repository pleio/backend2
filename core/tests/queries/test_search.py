import time

from django.core.files.base import ContentFile
from django.test import override_settings, tag
from django.utils import timezone
from django_tenants.utils import schema_context
from mixer.backend.django import mixer

from control.tasks import add_site
from core import config
from core.constances import ACCESS_TYPE, ConfigurationChoices
from core.factories import GroupFactory
from core.models import Group
from core.tests.helpers import ElasticsearchTestCase, suppress_stdout
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from entities.file.models import FileFolder, FileReference
from entities.news.models import News
from entities.wiki.models import Wiki
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    QuestionManagerFactory,
    UserFactory,
    UserManagerFactory,
)


@tag("search_query")
class SearchTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(COLLAB_EDITING_ENABLED=True)
        self.override_setting(ENV="unit-test")

        self.common_tag1 = "Alles moet weg"
        self.common_tag2 = "Niets blijft staan"
        self.q = "Alles"

        self.group = mixer.blend(Group)
        self.user = UserFactory()
        self.user2 = UserFactory()

        self.query = """
            query Search(
                        $q: String!
                        $subtype: String
                        $subtypes: [String]
                        $filterArchived: Boolean) {
                search(
                        q: $q,
                        subtype: $subtype
                        subtypes: $subtypes
                        filterArchived: $filterArchived) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                    total
                    totals {
                        subtype
                        total
                    }
                }
            }
        """

        permission = {
            "owner": self.user,
            "read_access": [ACCESS_TYPE.public],
            "write_access": [ACCESS_TYPE.user.format(self.user.guid)],
        }

        self.pad = FileFolder.objects.create(
            type=FileFolder.Types.PAD,
            title="Test group pad",
            rich_description="padtest",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user.id)],
            owner=self.user,
            group=self.group,
        )

        permission2 = {
            "owner": self.user2,
            "read_access": [ACCESS_TYPE.public],
            "write_access": [ACCESS_TYPE.user.format(self.user2.guid)],
        }

        self.blog1 = mixer.blend(Blog, title=self.common_tag1, **permission)
        self.blog2 = mixer.blend(
            Blog, title=self.common_tag2, is_recommended_in_search=True, **permission
        )
        self.wiki1 = mixer.blend(Wiki, title=self.common_tag1, **permission)
        self.wiki2 = mixer.blend(Wiki, title=self.common_tag2, **permission)
        self.news1 = mixer.blend(News, title=self.common_tag1, **permission)
        self.news2 = mixer.blend(News, title=self.common_tag2, **permission)
        self.news3 = mixer.blend(News, title=self.common_tag2, **permission2)

        self.attachment = FileFolder.objects.create(
            title="Demo.txt",
            owner=self.user,
            upload=ContentFile(b"Content...", "attachment_file_name.txt"),
        )
        self.blocked_attachment = FileFolder.objects.create(
            title="Demo.jpg",
            owner=self.user,
            upload=ContentFile(b"Content...", "attachment_file_name.jpg"),
            blocked=True,
        )
        self.blog1.attachments.create(file=self.attachment)
        self.blog1.attachments.create(file=self.blocked_attachment)

        self.favicon = FileFolder.objects.create(
            title="Favicon.jpg",
            owner=self.user,
            upload=ContentFile(b"Content...", "attachment_file_name.jpg"),
        )
        FileReference.objects.update_configuration(
            ConfigurationChoices.FAVICON.name, self.favicon
        )

        self.populate_index()

    def tearDown(self):
        super().tearDown()

    def test_invalid_subtype(self):
        with self.assertGraphQlError("invalid_subtype"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.query, {"q": "", "subtype": "test"})

        with self.assertGraphQlError("invalid_subtype"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.query, {"q": "", "subtypes": ["test"]})

    def test_multiple_subtypes(self):
        self.override_config(COLLAB_EDITING_ENABLED=True)
        variables = {
            "q": self.q,
            "subtypes": ["blog", "wiki", "pad"],
            "subtype": "blog",
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]["search"]
        items = [i["guid"] for i in data["edges"]]

        self.assertEqual(2, data["total"])
        self.assertEqual(1, len(items))
        self.assertIn(self.blog1.guid, items)
        self.assertNotIn(self.wiki1.guid, items)
        self.assertNotIn(self.news1.guid, items)

    def test_invalid_date(self):
        query = """
            query Search(
                        $q: String!,
                        $dateFrom: String,
                        $dateTo: String) {
                search(
                        q: $q,
                        dateFrom: $dateFrom,
                        dateTo: $dateTo) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                }
            }
        """

        variables = {
            "q": "",
            "dateFrom": "2016-33-03T19:00:00",
            "dateTo": "2016-44-03T19:00:00",
        }

        with self.assertGraphQlError("invalid_date"):
            self.graphql_client.post(query, variables)

    def test_pad_search(self):
        self.override_config(COLLAB_EDITING_ENABLED=True)

        variables = {"q": "padtest", "subtype": "pad"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]["search"]
        self.assertEqual(1, data["total"])

        self.assertEqual(self.pad.guid, data["edges"][0]["guid"])

    def test_owner_guids(self):
        query = """
            query Search(
                        $ownerGuids: [String]
                        ) {
                search(
                        ownerGuids: $ownerGuids
                        ) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                    total
                    totals {
                        subtype
                        total
                    }
                }
            }
        """

        variables = {"ownerGuids": [str(self.user2.id)]}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(query, variables)

        data = result["data"]["search"]
        items = [i["guid"] for i in data["edges"]]

        self.assertEqual(1, data["total"])
        self.assertEqual(1, len(items))
        self.assertIn(self.news3.guid, items)
        self.assertNotIn(self.news1.guid, items)
        self.assertNotIn(self.blog1.guid, items)

    def test_attachment_search(self):
        variables = {"q": "Demo", "subtype": "file"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]["search"]
        self.assertEqual(1, data["total"])

        self.assertEqual(self.attachment.guid, data["edges"][0]["guid"])

    def test_favicon_not_indexed(self):
        variables = {"q": "Favicon", "subtype": "file"}

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]["search"]
        self.assertEqual(0, data["total"])

    def test_is_recommended_in_search(self):
        query = """
            query Search(
                        $q: String!,
                        $isRecommendedInSearch: Boolean) {
                search(
                        q: $q,
                        isRecommendedInSearch: $isRecommendedInSearch) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                    total
                }
            }
        """

        variables = {
            "q": "",
            "isRecommendedInSearch": True,
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(query, variables)

        data = result["data"]["search"]
        items = [i["guid"] for i in data["edges"]]

        self.assertEqual(1, data["total"])
        self.assertEqual(1, len(items))
        self.assertIn(self.blog2.guid, items)
        self.assertNotIn(self.news1.guid, items)
        self.assertNotIn(self.blog1.guid, items)


@tag("search_query")
class TestSearchArchivedTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.archived_blog = BlogFactory(
            owner=self.user, is_archived=True, published=timezone.localtime()
        )
        self.not_archived_blog = BlogFactory(
            owner=self.user, is_archived=False, published=timezone.localtime()
        )

        self.query = """
        query Search($q: String! $enabled: Boolean, $disabled: Boolean) {
                onlyArchived: search(q: $q, filterArchived: $enabled) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                }
                allContent: search(q: $q, filterArchived: $disabled) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                }
            }
        """
        self.variables = {"q": "", "enabled": True, "disabled": False}

        self.populate_index()

    def test_filter_archived(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, self.variables)

        only_archived = [e["guid"] for e in result["data"]["onlyArchived"]["edges"]]
        all_content = [e["guid"] for e in result["data"]["allContent"]["edges"]]

        self.assertIn(self.archived_blog.guid, only_archived)
        self.assertNotIn(self.archived_blog.guid, all_content)

        self.assertNotIn(self.not_archived_blog.guid, only_archived)
        self.assertIn(self.not_archived_blog.guid, all_content)


@tag("search_query")
class TestCaseSensitivityTitleSortingSearchTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.lowercase = BlogFactory(owner=self.owner, title="the quick brown fox")
        self.uppercase = BlogFactory(owner=self.owner, title="The Quick Fox is brown")
        self.lowercase2 = BlogFactory(owner=self.owner, title="i'm quick but that's ok")
        self.uppercase2 = BlogFactory(
            owner=self.owner, title="I'm Quick if that's OK with you"
        )
        self.uppercase3 = BlogFactory(owner=self.owner, title="That's Quick!")

        self.expected_order = sorted(
            [
                self.lowercase,
                self.lowercase2,
                self.uppercase,
                self.uppercase2,
                self.uppercase3,
            ],
            key=lambda b: b.title.lower(),
        )

        self.populate_index()

    def tearDown(self):
        super().tearDown()

    def test_search_query(self):
        query = """
            query Search($query: String!, $orderBy: SearchOrderBy) {
                search(q: $query, orderBy: $orderBy) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                        ... on Blog {
                            title
                        }
                    }
                }
            }
        """
        variables = {"query": "", "orderBy": "title"}

        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(query, variables)

        expected_titles = [b.title for b in self.expected_order]
        actual_titles = [edge["title"] for edge in response["data"]["search"]["edges"]]

        self.assertEqual(expected_titles, actual_titles)

    def test_entities_query(self):
        query = """
            query Search($orderBy: OrderBy, $orderDirection: OrderDirection) {
                entities(orderBy: $orderBy, orderDirection: $orderDirection) {
                    edges {
                        guid
                        ... on Blog {
                            title
                        }
                    }
                }
            }
        """
        variables = {
            "orderBy": "title",
            "orderDirection": "asc",
        }
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(query, variables)

        expected_titles = [b.title for b in self.expected_order]
        actual_titles = [
            edge["title"] for edge in response["data"]["entities"]["edges"]
        ]

        self.assertEqual(expected_titles, actual_titles)


@tag("search_query")
class TestSearchRelatedSiteTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()
        self.override_setting(ENV="unit-test")

        self.user = UserFactory(email="user@example.com")
        self.user_both_sites = UserFactory(
            email="user_both_sides@example.com", external_id="123"
        )
        self.blog = BlogFactory(
            owner=self.user,
            is_archived=False,
            published=timezone.localtime(),
            read_access=[ACCESS_TYPE.logged_in],
        )

        self.query = """
        query Search($q: String!, $searchRelatedSites: Boolean!) {
                search(q: $q, searchRelatedSites: $searchRelatedSites) {
                    edges {
                        ... on Entity {
                            guid
                        }
                        ... on RelatedSiteSearchResult {
                            guid
                        }
                    }
                }
            }
        """
        self.variables = {"q": "", "searchRelatedSites": True}

        self.populate_index()
        self.setup_related_site("demo1")

    @suppress_stdout()
    def setup_related_site(self, schema_name):
        """
        Setup a related site with a user and a blog with custom elasticsearch indexing
        """
        with schema_context("public"):
            add_site(schema_name, "%s.local" % schema_name)

        with schema_context(schema_name):
            from core.tasks.elasticsearch_tasks import (
                elasticsearch_delete_data_for_tenant,
            )

            # elasticsearch needs time to settle changes before some delete?
            time.sleep(0.100)
            elasticsearch_delete_data_for_tenant("demo1", None)
            time.sleep(0.100)

            self.external_user = UserFactory(email="external_user@example.com")
            self.external_user_same = UserFactory(
                email="external_user_same@example.com",
                external_id=self.user_both_sites.external_id,
            )
            self.external_group_closed = GroupFactory(
                owner=self.external_user, name="Hello", is_closed=True
            )
            self.external_group_closed.join(self.external_user_same, "member")
            self.external_group_closed.join(self.external_user, "member")
            self.external_blog1 = BlogFactory(
                owner=self.external_user,
                is_archived=False,
                published=timezone.localtime(),
                read_access=[ACCESS_TYPE.public],
            )
            self.external_blog2 = BlogFactory(
                owner=self.external_user,
                is_archived=False,
                published=timezone.localtime(),
                read_access=[ACCESS_TYPE.group.format(self.external_group_closed.id)],
            )
            config.NAME = schema_name + "_name"
            from core.tasks.elasticsearch_tasks import (
                elasticsearch_index_data_for_tenant,
            )

            elasticsearch_index_data_for_tenant(schema_name, None)
            time.sleep(0.100)

    def test_search_user_not_on_related_site(self):
        self.override_config(SEARCH_RELATED_SCHEMAS=["demo1"])
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, self.variables)

        content = [e["guid"] for e in result["data"]["search"]["edges"]]

        self.assertIn(self.blog.guid, content)
        self.assertIn(self.external_blog1.guid, content)
        self.assertNotIn(self.external_blog2.guid, content, "Should not be visible")

    @override_settings(ENV="not_local")
    def test_search_related_site_names(self):
        self.override_config(SEARCH_RELATED_SCHEMAS=["demo1"])
        self.query = """
            query SiteGeneralSettings {
                site {
                    searchRelatedSitesEnabled
                    searchRelatedSites {
                        name
                        url
                    }
                }
            }
        """
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query, {})
        response = response["data"]
        self.assertEqual(response["site"]["searchRelatedSitesEnabled"], True)
        self.assertEqual(
            response["site"]["searchRelatedSites"][0]["name"], "demo1_name"
        )
        self.assertEqual(
            response["site"]["searchRelatedSites"][0]["url"], "https://demo1.local"
        )


class TestSearchQueryEmailAddresses(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.EMAIL = "matching-email@address.com"
        self.email_as_email = UserFactory(email=self.EMAIL)
        self.email_as_name = UserFactory(name=self.EMAIL)
        self.email_as_title = BlogFactory(title=self.EMAIL, owner=UserFactory())

        self.populate_index()

    def send_query(self, acting_user):
        self.graphql_client.force_login(acting_user)
        result = self.graphql_client.post(
            """
            fragment ResultProperties on SearchResult {
                ... on Blog {
                    guid
                }
                ... on User {
                    guid
                }
            }
            query Search($query: String!) {
                search(q: $query) {
                    edges {
                        ...ResultProperties
                    }
                }
            }
        """,
            {"query": self.EMAIL},
        )
        return result["data"]["search"]["edges"]

    def test_query_as_authenticated_user(self):
        """
        Test that non-privileged users are not able to search based on the actual email address of a user.
        """
        for user, description in [
            (self.email_as_email, "the user with the email as email address"),
            (self.email_as_name, "The user with the email as name"),
            (QuestionManagerFactory(), "A question manager"),
            (EditorFactory(), "An editor"),
            (NewsEditorFactory(), "A news editor"),
        ]:
            with self.subTest(description):
                results = self.send_query(user)
                self.assertEqual(len(results), 2)
                self.assertEqual(
                    {r["guid"] for r in results},
                    {self.email_as_name.guid, self.email_as_title.guid},
                )

    def test_query_as_privileged_user(self):
        """
        Test that privileged users are able to search based on the actual email address of a user.
          - Admins
          - User managers
        """
        for user, description in [
            (AdminFactory(), "Run as administrator"),
            (UserManagerFactory(), "Run as user manager"),
        ]:
            with self.subTest(description):
                results = self.send_query(user)
                self.assertEqual(len(results), 3)
                self.assertEqual(
                    {r["guid"] for r in results},
                    {
                        self.email_as_email.guid,
                        self.email_as_name.guid,
                        self.email_as_title.guid,
                    },
                )
