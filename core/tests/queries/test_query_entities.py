from django.core.files.base import ContentFile
from django.utils.timezone import now

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestQueryEntities(PleioTenantTestCase):
    """
    Demonstrate that Query.entities can be used for the 'activity' page of a user.
    """

    def setUp(self):
        super().setUp()

        self.override_config(CONTENT_MODERATION_ENABLED=True)

        self.user = UserFactory(email="author@example.com")
        self.group_owner = UserFactory(email="group-owner@example.com")

        self.group = GroupFactory(owner=self.group_owner)

        # Author has a
        # - Published blog
        self.published_blog = BlogFactory(
            owner=self.user,
            title="Published blog",
        )
        # - Draft blog with publish request
        self.draft_blog = BlogFactory(
            owner=self.user,
            title="Draft blog",
            published=None,
        )
        self.draft_blog.publish_requests.create(time_published=now())
        # - An archived blog
        self.archived_blog = BlogFactory(
            owner=self.user,
            title="Archived blog",
            is_archived=True,
        )

        # - A published file
        self.published_file = FileFactory(
            owner=self.user,
            title="Published file",
            upload=ContentFile(b"1234567", "test1.txt"),
            group=self.group,
        )
        # - A file with publish request
        self.draft_file = FileFactory(
            owner=self.user,
            title="Draft file",
            published=None,
            upload=ContentFile(b"2345678", "test2.txt"),
            group=self.group,
        )
        self.draft_file.publish_requests.create(time_published=now())
        # - An archived file
        self.archived_file = FileFactory(
            owner=self.user,
            title="Archived file",
            upload=ContentFile(b"3456789", "test3.txt"),
            group=self.group,
            is_archived=True,
        )

    def test_activity_page_summary(self):
        """
        It should be possible to load a summary of published, draft-draft-submitted and archived content.
        """

        query = """
        fragment BlogSummary on Blog { title }
        fragment FileSummary on File { title }
        fragment Edges on EntityList {
            total
            edges {
                guid
                ... BlogSummary,
                ... FileSummary
            }
        }
        query ($userGuid: String!){
            publishedContent: entities(
                    userGuid: $userGuid
                    statusPublished: published,
                    orderDirection: asc
                    orderBy: timeCreated
                ) { ... Edges }
            draftContent: entities(
                    userGuid: $userGuid
                    statusPublished: draft
                    orderDirection: asc
                    orderBy: timeCreated
                ) { ... Edges }
            archivedContent: entities(
                    userGuid: $userGuid
                    statusPublished: archived
                    orderDirection: asc
                    orderBy: timeCreated
                ) { ... Edges }
        }
        """

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(
            query, variables={"userGuid": self.user.guid}
        )

        self.assertEqual(
            response["data"],
            {
                "publishedContent": {
                    "total": 2,
                    "edges": [
                        {"guid": self.published_blog.guid, "title": "Published blog"},
                        {"guid": self.published_file.guid, "title": "Published file"},
                    ],
                },
                "draftContent": {
                    "total": 2,
                    "edges": [
                        {"guid": self.draft_blog.guid, "title": "Draft blog"},
                        {"guid": self.draft_file.guid, "title": "Draft file"},
                    ],
                },
                "archivedContent": {
                    "total": 2,
                    "edges": [
                        {"guid": self.archived_blog.guid, "title": "Archived blog"},
                        {"guid": self.archived_file.guid, "title": "Archived file"},
                    ],
                },
            },
            msg=response,
        )
