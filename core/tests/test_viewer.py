from typing import TYPE_CHECKING

from django.contrib.auth.models import AnonymousUser

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase, override_config
from entities.wiki.factories import WikiFactory
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    RequestManagerFactory,
    UserFactory,
    UserManagerFactory,
)

if TYPE_CHECKING:
    from core.models import Group


class ViewerTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.query = """
            query Viewer {
                viewer {
                    guid
                    loggedIn
                    isSubEditor
                    isNewsEditor
                    isAdmin
                    isBanned
                    has2faEnabled
                    user {
                        guid
                        email
                        name
                    }
                }
            }
        """

    def post_query(self, user):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, {})
        return result["data"]["viewer"]

    def test_viewer_anonymous(self):
        self.assertEqual(
            self.post_query(AnonymousUser()),
            {
                "guid": "viewer:0",
                "has2faEnabled": False,
                "isAdmin": False,
                "isBanned": False,
                "isSubEditor": False,
                "isNewsEditor": False,
                "loggedIn": False,
                "user": None,
            },
        )

    def assertIsAuthenticated(self, result, user):
        self.assertEqual(result["guid"], "viewer:%s" % user.guid)
        self.assertTrue(result["loggedIn"])
        self.assertEqual(
            result["user"],
            {
                "guid": user.guid,
                "email": user.email,
                "name": user.name,
            },
        )

    def test_viewer_authenticated(self):
        user = UserFactory()
        result = self.post_query(user)

        self.assertIsAuthenticated(result, user)
        self.assertFalse(result["has2faEnabled"])
        self.assertFalse(result["isAdmin"])
        self.assertFalse(result["isBanned"])
        self.assertFalse(result["isSubEditor"])
        self.assertFalse(result["isNewsEditor"])

    def test_viewer_authenticated_2fa(self):
        user = UserFactory(has_2fa_enabled=True)
        result = self.post_query(user)

        self.assertIsAuthenticated(result, user)
        self.assertTrue(result["has2faEnabled"])

    def test_viewer_editor(self):
        user = EditorFactory()
        result = self.post_query(user)

        self.assertIsAuthenticated(result, user)
        self.assertTrue(result["isNewsEditor"])
        self.assertTrue(result["isSubEditor"])
        self.assertFalse(result["isAdmin"])

    def test_viewer_admin(self):
        user = AdminFactory()
        result = self.post_query(user)

        self.assertIsAuthenticated(result, user)
        self.assertTrue(result["isNewsEditor"])
        self.assertTrue(result["isSubEditor"])
        self.assertTrue(result["isAdmin"])

    def test_viewer_news_editor(self):
        user = NewsEditorFactory()
        result = self.post_query(user)

        self.assertIsAuthenticated(result, user)
        self.assertFalse(result["isSubEditor"])
        self.assertFalse(result["isAdmin"])
        self.assertTrue(result["isNewsEditor"])


class ViewerCanWriteSubtypeToContainerTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory(email="user@example.com")
        self.admin = AdminFactory(email="admin@example.com")
        self.editor = EditorFactory(email="editor@example.com")
        self.groupOwner = UserFactory(email="group_owner@example.com")
        self.memberAdmin = UserFactory(email="member_admin@example.com")
        self.memberUser = UserFactory(email="member_user@example.com")
        self.memberEditor = EditorFactory(email="member_editor@example.com")
        self.memberWikiAuthor = UserFactory(email="wiki_author@example.com")

        self.group: Group = GroupFactory(owner=self.groupOwner)
        self.group.join(self.memberAdmin, "admin")
        self.group.join(self.memberUser)
        self.group.join(self.memberEditor)
        self.group.join(self.memberWikiAuthor)

        self.wiki = WikiFactory(owner=self.memberWikiAuthor, group=self.group)

        self.query = """
            query viewer($container: String, $wikiGuid: String){
                page: viewer {
                    canWriteToContainer(subtype: "page")
                }
                groupPage: viewer {
                    canWriteToContainer(subtype: "page", containerGuid: $container)
                }
                blog: viewer {
                    canWriteToContainer(subtype: "blog")
                }
                groupBlog: viewer {
                    canWriteToContainer(subtype: "blog", containerGuid: $container)
                }
                news: viewer {
                    canWriteToContainer(subtype: "news")
                }
                groupNews: viewer {
                    canWriteToContainer(subtype: "news", containerGuid: $container)
                }
                specificGroupWiki: viewer {
                    canWriteToContainer(subtype: "wiki", containerGuid: $wikiGuid)
                }
            }
        """
        self.variables = {
            "container": self.group.guid,
            "wikiGuid": self.wiki.guid,
        }

    def test_viewer_anonymous(self):
        data = self.post_query(AnonymousUser())

        self.assertEqual(data["blog"]["canWriteToContainer"], False)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], False)
        self.assertEqual(data["news"]["canWriteToContainer"], False)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], False)
        self.assertEqual(data["page"]["canWriteToContainer"], False)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], False)
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], False)

    def test_viewer_user(self):
        data = self.post_query(self.user)

        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], False)
        self.assertEqual(data["news"]["canWriteToContainer"], False)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], False)
        self.assertEqual(data["page"]["canWriteToContainer"], False)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], False)
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], False)

    def test_viewer_admin(self):
        data = self.post_query(self.admin)

        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], True)
        self.assertEqual(data["news"]["canWriteToContainer"], True)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], True)
        self.assertEqual(data["page"]["canWriteToContainer"], True)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], True)
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], True)

    def test_viewer_editor(self):
        data = self.post_query(self.editor)

        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], False)
        self.assertEqual(data["news"]["canWriteToContainer"], True)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], False)
        self.assertEqual(data["page"]["canWriteToContainer"], True)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], False)
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], False)

    def test_viewer_group_owner(self):
        data = self.post_query(self.groupOwner)
        self.assertGroupAdminAccess(data)

    def test_viewer_group_admin(self):
        data = self.post_query(self.memberAdmin)
        self.assertGroupAdminAccess(data)

    def test_viewer_member_user(self):
        data = self.post_query(self.memberUser)

        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], True)
        self.assertEqual(data["news"]["canWriteToContainer"], False)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], False)
        self.assertEqual(data["page"]["canWriteToContainer"], False)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], False)

        # TODO: ik verwacht dat dit eigenlijk wel mag
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], False)

    def test_viewer_member_editor(self):
        data = self.post_query(self.memberEditor)

        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], True)
        self.assertEqual(data["news"]["canWriteToContainer"], True)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], True)
        self.assertEqual(data["page"]["canWriteToContainer"], True)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], True)

        # TODO: ik verwacht dat dit eigenlijk wel mag
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], False)

    def test_viewer_wiki_author(self):
        data = self.post_query(self.memberWikiAuthor)

        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], True)
        self.assertEqual(data["news"]["canWriteToContainer"], False)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], False)
        self.assertEqual(data["page"]["canWriteToContainer"], False)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], False)
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], True)

    """
    Helper functions
    """

    def post_query(self, user):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, self.variables)
        return result["data"]

    def assertGroupAdminAccess(self, data):
        self.assertEqual(data["blog"]["canWriteToContainer"], True)
        self.assertEqual(data["groupBlog"]["canWriteToContainer"], True)
        self.assertEqual(data["news"]["canWriteToContainer"], False)
        self.assertEqual(data["groupNews"]["canWriteToContainer"], True)
        self.assertEqual(data["page"]["canWriteToContainer"], False)
        self.assertEqual(data["groupPage"]["canWriteToContainer"], True)
        self.assertEqual(data["specificGroupWiki"]["canWriteToContainer"], True)


class TestViewerAccessToWriteFilesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.query = """
            query canWriteToContainer($guid: String, $subtype: String){
                viewer {
                    canWriteToContainer(
                        containerGuid: $guid
                        subtype: $subtype
                    )
                }
            }
        """

    def post_query(self, user, variables):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, variables)
        return result["data"]

    def test_viewer_can_write_to_container_user_self(self):
        user = UserFactory()
        data = self.post_query(
            user,
            {
                "guid": user.guid,
                "subtype": "file",
            },
        )

        self.assertEqual(data["viewer"]["canWriteToContainer"], True)

    def test_viewer_can_not_write_to_container_user_other(self):
        data = self.post_query(
            UserFactory(),
            {
                "guid": UserFactory().guid,
                "subtype": "file",
            },
        )

        self.assertEqual(data["viewer"]["canWriteToContainer"], False)

    def test_admin_viewer_can_write_to_user_container(self):
        data = self.post_query(
            AdminFactory(),
            variables={
                "guid": UserFactory().guid,
                "subtype": "file",
            },
        )

        self.assertEqual(data["viewer"]["canWriteToContainer"], True)


class TestViewerCanAccessAdmin(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.query = """
            query {
                viewer {
                    canAccessAdmin: canAccessAdmin(permission: accessAdmin)
                    canAccessAdminRequestManagement: canAccessAdmin(permission: accessRequestManagement)
                    canAccessAdminFullControl: canAccessAdmin(permission: fullControl)
                    canAccessAdminUserManagement: canAccessAdmin(permission: userManagement)
                    canAccessAdminPublicationRequestManagement: canAccessAdmin(permission: publicationRequestManagement)
                    canAccessAdminContentManagement: canAccessAdmin(permission: contentManagement)
                }
            }
        """

    def test_visitors_without_access(self):
        for visitor, message in (
            (AnonymousUser(), "Anonymous visitor"),
            (UserFactory(), "Authenticated visitor"),
        ):
            self.graphql_client.force_login(visitor)
            result = self.graphql_client.post(self.query)

            self.assertEqual(
                result["data"]["viewer"],
                {
                    "canAccessAdmin": False,
                    "canAccessAdminRequestManagement": False,
                    "canAccessAdminFullControl": False,
                    "canAccessAdminUserManagement": False,
                    "canAccessAdminPublicationRequestManagement": False,
                    "canAccessAdminContentManagement": False,
                },
                msg=message,
            )

    def test_user_admin_access(self):
        self.graphql_client.force_login(UserManagerFactory())
        result = self.graphql_client.post(self.query)

        self.assertEqual(
            result["data"]["viewer"],
            {
                "canAccessAdmin": True,
                "canAccessAdminRequestManagement": True,
                "canAccessAdminFullControl": False,
                "canAccessAdminUserManagement": True,
                "canAccessAdminPublicationRequestManagement": False,
                "canAccessAdminContentManagement": False,
            },
        )

    def test_editor_admin_access(self):
        self.graphql_client.force_login(EditorFactory())
        result = self.graphql_client.post(self.query)

        self.assertEqual(
            result["data"]["viewer"],
            {
                "canAccessAdmin": True,
                "canAccessAdminRequestManagement": False,
                "canAccessAdminFullControl": False,
                "canAccessAdminUserManagement": False,
                "canAccessAdminPublicationRequestManagement": True,
                "canAccessAdminContentManagement": True,
            },
        )

    @override_config(CONTENT_MODERATION_ENABLED=True)
    def test_publication_request_admin_access_with_content_moderation(self):
        self.graphql_client.force_login(EditorFactory())
        result = self.graphql_client.post(self.query)

        self.assertEqual(
            result["data"]["viewer"],
            {
                "canAccessAdmin": True,
                "canAccessAdminRequestManagement": False,
                "canAccessAdminFullControl": False,
                "canAccessAdminUserManagement": False,
                "canAccessAdminPublicationRequestManagement": True,
                "canAccessAdminContentManagement": True,
            },
        )

    def test_site_admin_access(self):
        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(self.query)

        self.assertEqual(
            result["data"]["viewer"],
            {
                "canAccessAdmin": True,
                "canAccessAdminRequestManagement": True,
                "canAccessAdminFullControl": True,
                "canAccessAdminUserManagement": True,
                "canAccessAdminPublicationRequestManagement": True,
                "canAccessAdminContentManagement": True,
            },
        )

    def test_site_admin_access_request_manager(self):
        self.graphql_client.force_login(RequestManagerFactory())
        result = self.graphql_client.post(self.query)

        self.assertEqual(
            result["data"]["viewer"],
            {
                "canAccessAdmin": True,
                "canAccessAdminRequestManagement": True,
                "canAccessAdminFullControl": False,
                "canAccessAdminUserManagement": False,
                "canAccessAdminPublicationRequestManagement": False,
                "canAccessAdminContentManagement": False,
            },
        )


class TestViewerCanUpdateAccessLevel(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.query = """
            query Viewer($guid: String) {
                groupViewer: viewer {
                    canUpdateAccessLevel(groupGuid: $guid)
                }
                viewer {
                    canUpdateAccessLevel
                }
            }
        """
        self.variables = {"guid": self.group.guid}

    def post_query(self, user):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, self.variables)
        return result["data"]

    def assertFullPositive(self, data):
        self.assertEqual(
            data,
            {
                "groupViewer": {"canUpdateAccessLevel": True},
                "viewer": {"canUpdateAccessLevel": True},
            },
        )

    def assertFullNegative(self, data):
        self.assertEqual(
            data,
            {
                "groupViewer": {"canUpdateAccessLevel": False},
                "viewer": {"canUpdateAccessLevel": False},
            },
        )

    def test_viewer_anonymous(self):
        data = self.post_query(AnonymousUser())

        self.assertFullNegative(data)

    def test_viewer_anonymous_with_feature(self):
        self.override_config(HIDE_ACCESS_LEVEL_SELECT=True)
        data = self.post_query(AnonymousUser())

        self.assertFullNegative(data)

    def test_viewer_user(self):
        user = UserFactory()
        self.group.join(user)

        data = self.post_query(user)

        self.assertFullPositive(data)

    def test_viewer_user_with_feature(self):
        self.override_config(HIDE_ACCESS_LEVEL_SELECT=True)
        user = UserFactory()
        self.group.join(user)

        data = self.post_query(user)

        self.assertFullNegative(data)

    def test_viewer_group_admin(self):
        user = UserFactory()
        self.group.join(user, "admin")

        data = self.post_query(user)

        self.assertFullPositive(data)

    def test_viewer_group_admin_with_feature(self):
        self.override_config(HIDE_ACCESS_LEVEL_SELECT=True)
        user = UserFactory()
        self.group.join(user, "admin")

        data = self.post_query(user)

        self.assertEqual(
            data,
            {
                "groupViewer": {"canUpdateAccessLevel": True},
                "viewer": {"canUpdateAccessLevel": False},
            },
        )

    def test_viewer_site_admin(self):
        data = self.post_query(AdminFactory())

        self.assertFullPositive(data)

    def test_viewer_site_admin_with_feature(self):
        self.override_config(HIDE_ACCESS_LEVEL_SELECT=True)

        data = self.post_query(AdminFactory())

        self.assertFullPositive(data)


class TestViewerRichTextEditorFeature(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.query = """
            query Viewer ($feature: RichTextEditorFeatureEnum!){
                viewer {
                    richTextEditorFeature(feature: $feature)
                }
            }
        """

    def test_allow_admin_add_class_as_admin(self):
        self.override_config(RICH_TEXT_EDITOR_FEATURES=["allowTextCssClasses"])

        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(
            self.query, {"feature": "allowTextCssClasses"}
        )
        viewer = result["data"]["viewer"]

        self.assertEqual(viewer["richTextEditorFeature"], True)

    def test_allow_admin_add_class_as_non_admin(self):
        self.override_config(RICH_TEXT_EDITOR_FEATURES=["allowTextCssClasses"])

        self.graphql_client.force_login(UserFactory())
        result = self.graphql_client.post(
            self.query, {"feature": "allowTextCssClasses"}
        )
        viewer = result["data"]["viewer"]

        self.assertEqual(viewer["richTextEditorFeature"], False)


class TestContentModerationProperties(PleioTenantTestCase):
    SUBTYPES = [
        "blog",
        "discussion",
        "event",
        "news",
        "file",
        "podcast",
        "episode",
        "question",
        "wiki",
    ]

    def setUp(self):
        super().setUp()

        self.override_config(CONTENT_MODERATION_ENABLED=True)
        self.query = """
            query ModerationParameters($subtype: String!, $groupGuid: String){
                viewer {
                    requiresContentModeration(subtype: $subtype, groupGuid: $groupGuid)
                }
            }
        """

    def run_query(self, user, msg, subtype, expect_content, groupGuid=None):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(
            self.query,
            {"subtype": subtype, "groupGuid": groupGuid},
        )
        viewer = result["data"]["viewer"]

        self.assertEqual(viewer["requiresContentModeration"], expect_content, msg=msg)

    def test_not_requires_moderation(self):
        self.override_config(REQUIRE_CONTENT_MODERATION_FOR=[])
        for user_msg, user, expect_content in (
            ("Anonymous user", AnonymousUser(), False),
            ("Authenticated user", UserFactory(), False),
            ("Editor user", EditorFactory(), False),
            ("Site admin user", AdminFactory(), False),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                self.run_query(user, msg, subtype, expect_content)

    def test_requires_content_moderation(self):
        for user_msg, user, expect_content in (
            ("Anonymous user", AnonymousUser(), True),
            ("Authenticated user", UserFactory(), True),
            ("Editor user", EditorFactory(), False),
            ("Site admin user", AdminFactory(), False),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.override_config(REQUIRE_CONTENT_MODERATION_FOR=[subtype])
                    self.run_query(user, msg, subtype, expect_content)

    def test_requires_content_moderation_for_group_members(self):
        group = GroupFactory(owner=UserFactory())
        for user_msg, user, expect_content in (
            ("Group member", make_group_member(group, UserFactory()), True),
            ("Group admin", make_group_admin(group, UserFactory()), False),
            ("Group owner", group.owner, False),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.override_config(REQUIRE_CONTENT_MODERATION_FOR=[subtype])
                    self.run_query(user, msg, subtype, expect_content, group.guid)

    def test_requires_content_moderation_if_disabled(self):
        self.override_config(CONTENT_MODERATION_ENABLED=False)
        for user_msg, user in (
            ("Anonymous user", AnonymousUser()),
            ("Authenticated user", UserFactory()),
            ("Editor user", EditorFactory()),
            ("Site admin user", AdminFactory()),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.override_config(REQUIRE_CONTENT_MODERATION_FOR=[subtype])
                    self.run_query(user, msg, subtype, False)


class TestCommentModerationProperties(PleioTenantTestCase):
    SUBTYPES = [
        "blog",
        "discussion",
        "event",
        "news",
        "question",
    ]

    def setUp(self):
        super().setUp()

        self.override_config(COMMENT_MODERATION_ENABLED=True)
        self.query = """
            query ModerationParameters($subtype: String!, $groupGuid: String){
                viewer {
                    requiresCommentModeration(subtype: $subtype, groupGuid: $groupGuid)
                }
            }
        """

    def run_query(self, user, msg, subtype, expect_content, group_guid=None):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(
            self.query,
            {
                "subtype": subtype,
                "groupGuid": group_guid,
            },
        )
        viewer = result["data"]["viewer"]

        self.assertEqual(viewer["requiresCommentModeration"], expect_content, msg=msg)

    def test_not_requires_moderation(self):
        self.override_config(REQUIRE_COMMENT_MODERATION_FOR=[])
        for user_msg, user, expect_content in (
            ("Anonymous user", AnonymousUser(), False),
            ("Authenticated user", UserFactory(), False),
            ("Editor user", EditorFactory(), False),
            ("Site admin user", AdminFactory(), False),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.run_query(user, msg, subtype, expect_content)

    def test_requires_moderation(self):
        for user_msg, user, expect_content in (
            ("Anonymous user", AnonymousUser(), True),
            ("Authenticated user", UserFactory(), True),
            ("Editor user", EditorFactory(), False),
            ("Site admin user", AdminFactory(), False),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.override_config(REQUIRE_COMMENT_MODERATION_FOR=[subtype])
                    self.run_query(user, msg, subtype, expect_content)

    def test_requires_moderation_for_group_members(self):
        group = GroupFactory(owner=UserFactory())
        for user_msg, user, expect_content in (
            ("Group member", make_group_member(group, UserFactory()), True),
            ("Group admin", make_group_admin(group, UserFactory()), False),
            ("Group owner", group.owner, False),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.override_config(REQUIRE_COMMENT_MODERATION_FOR=[subtype])
                    self.run_query(user, msg, subtype, expect_content, group.guid)

    def test_requires_moderation_if_disabled(self):
        self.override_config(COMMENT_MODERATION_ENABLED=False)
        for user_msg, user in (
            ("Anonymous user", AnonymousUser()),
            ("Authenticated user", UserFactory()),
            ("Editor user", EditorFactory()),
            ("Site admin user", AdminFactory()),
        ):
            for subtype in self.SUBTYPES:
                msg = f"{user_msg} - {subtype}"
                with self.subTest(msg):
                    self.override_config(REQUIRE_COMMENT_MODERATION_FOR=[subtype])
                    self.run_query(user, msg, subtype, False)


def make_group_member(group, user):
    group.join(user)
    return user


def make_group_admin(group, user):
    group.join(user, "admin")
    return user
