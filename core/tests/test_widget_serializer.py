from unittest import mock

from django.test import tag

from core.exceptions import AttachmentVirusScanError
from core.tests.helpers import PleioTenantTestCase
from core.utils.clamav import FILE_SCAN, FileScanError
from core.widget_resolver import WidgetSerializer, WidgetSettingSerializer
from entities.file.models import FileFolder
from user.factories import UserFactory


@tag("TestCasesWithWidgets")
class TestWidgetSerializerTestCase(PleioTenantTestCase):
    """Test specifics on the core.widget_resolver.WidgetSerializer"""

    def setUp(self):
        super().setUp()

        self.acting_user = UserFactory()

        self.file_mock = self.use_mock_file("test.gif")
        self.file_mock.name = "test.gif"
        self.file_mock.content_type = "image/gif"

        self.widget_spec = {
            "guid": "1234",
            "type": "demo",
            "settings": [{"key": "attachment", "attachment": self.file_mock}],
        }

        self.scan = mock.patch("core.utils.clamav.scan").start()

    def test_contains_attachment(self):
        widget = WidgetSerializer(
            self.widget_spec, self.acting_user, writing=True
        ).serialize()
        attachment = FileFolder.objects.first()

        self.assertEqual(attachment.title, self.file_mock.name)
        self.assertEqual(
            {**widget},
            {
                **WidgetSerializer.CLEAN_WIDGET,
                "guid": "1234",
                "type": "demo",
                "settings": [
                    {
                        **WidgetSettingSerializer.CLEAN_SETTING,
                        "key": "attachment",
                        "attachmentId": attachment.guid,
                    }
                ],
            },
        )

    def test_contains_attachment_with_virus(self):
        # Given.
        self.scan.side_effect = FileScanError(FILE_SCAN.VIRUS, "NL.SARS-PLEIO.Z665+")

        try:
            # When.
            WidgetSerializer(
                self.widget_spec, self.acting_user, writing=True
            ).serialize()
            self.fail(  # pragma: no cover
                "Unexpectedly did not respond correctly to the virus found behaviour"
            )
        except AttachmentVirusScanError as e:
            # Then.
            self.assertEqual(str(e), self.file_mock.name)


@tag("TestCasesWithWidgets")
@tag("UserWidgetSettings")
class TestHasUserSettings(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.profile = self.user.profile
        self.profile.set_widget_settings("1234", [{"key": "foo", "value": "bar"}])
        self.profile.save()

        self.widget = {
            "guid": "1234",
            "type": "demo",
            "settings": [
                {
                    "key": "foo",
                    "value": "baz",
                },
            ],
        }

    def test_fetch_user_settings(self):
        widget = WidgetSerializer(self.widget, self.user)

        self.assertEqual(
            widget.serialize(),
            {
                **WidgetSerializer.CLEAN_WIDGET,
                "guid": "1234",
                "type": "demo",
                "settings": [
                    {
                        **WidgetSettingSerializer.CLEAN_SETTING,
                        "key": "foo",
                        "value": "baz",
                    },
                ],
                "userSettings": [
                    {
                        **WidgetSettingSerializer.CLEAN_SETTING,
                        "key": "foo",
                        "value": "bar",
                    }
                ],
            },
        )
