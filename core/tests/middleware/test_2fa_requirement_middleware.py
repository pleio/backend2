from core.tests.helpers import PleioTenantTestCase
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    QuestionManagerFactory,
    UserFactory,
    UserManagerFactory,
)


class Template:
    class Base2FARequirementMiddlewareTestCase(PleioTenantTestCase):
        REQUIRE_2FA = None

        def setUp(self):
            super().setUp()
            self.override_config(REQUIRE_2FA=self.REQUIRE_2FA)

        def get_frontpage(self, user):
            self.client.force_login(user)
            return self.client.get("/")

        def test_resolved_by_proper_user_settings(self):
            with_2fa_enabled = {"has_2fa_enabled": True}
            for create_user in [
                AdminFactory,
                UserFactory,
                EditorFactory,
                QuestionManagerFactory,
                NewsEditorFactory,
                UserManagerFactory,
            ]:
                with self.subTest(str(create_user)):
                    self.assertTemplateNotUsed(
                        self.get_frontpage(user=create_user(**with_2fa_enabled)),
                        "registration/2fa_required.html",
                    )

    class AppliesToAnyUser(Base2FARequirementMiddlewareTestCase):
        """
        Tests that the middleware applies to any user.
        """

        def test_applies_to_any_user(self):
            for create_user in [
                AdminFactory,
                UserFactory,
                EditorFactory,
                QuestionManagerFactory,
                NewsEditorFactory,
                UserManagerFactory,
            ]:
                with self.subTest(str(create_user)):
                    self.assertTemplateUsed(
                        self.get_frontpage(user=create_user()),
                        "registration/2fa_required.html",
                    )

    class AppliesToAdminUsers(Base2FARequirementMiddlewareTestCase):
        """
        Tests that the middleware applies to any user.
        """

        def test_not_applies_to_ordinary_users(self):
            for create_user in [
                UserFactory,
                EditorFactory,
                QuestionManagerFactory,
                NewsEditorFactory,
                UserManagerFactory,
            ]:
                with self.subTest(str(create_user)):
                    self.assertTemplateNotUsed(
                        self.get_frontpage(user=create_user()),
                        "registration/2fa_required.html",
                    )

        def test_applies_to_admins(self):
            self.assertTemplateUsed(
                self.get_frontpage(user=AdminFactory()),
                "registration/2fa_required.html",
            )

    class AppliesToNoUser(Base2FARequirementMiddlewareTestCase):
        def test_does_not_apply_to_any_user(self):
            for create_user in [
                AdminFactory,
                UserFactory,
                EditorFactory,
                QuestionManagerFactory,
                NewsEditorFactory,
                UserManagerFactory,
            ]:
                with self.subTest(str(create_user)):
                    self.assertTemplateNotUsed(
                        self.get_frontpage(user=create_user()),
                        "registration/2fa_required.html",
                    )


class Test2FaRequirementMiddlewareStrNone(Template.AppliesToNoUser):
    """
    When the system setting is "none" the requirement should not apply to any user.
    """

    REQUIRE_2FA = "none"


class Test2FaRequirementMiddlewareFalse(Template.AppliesToNoUser):
    """
    When the system setting is (str) "none" the requirement should not apply to any user.
    """

    REQUIRE_2FA = False


class Test2FaRequirementMiddlewareNone(Template.AppliesToNoUser):
    """
    When the system setting is None the requirement should not apply to any user.
    """

    REQUIRE_2FA = None


class Test2FaRequirementMiddlewareTrue(Template.AppliesToAnyUser):
    """
    When the system setting is (bool) True the requirement should apply to any user.
    """

    REQUIRE_2FA = True


class Test2FaRequirementMiddlewareStrAll(Template.AppliesToAnyUser):
    """
    When the system setting is "all" the requirement should apply to any user.
    """

    REQUIRE_2FA = "all"


class Test2FaRequirementMiddlewareStrAdmins(Template.AppliesToAdminUsers):
    """
    When the system setting is "admin" the requirement should apply to admins only.
    """

    REQUIRE_2FA = "admin"
