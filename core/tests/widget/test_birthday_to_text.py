from core.tests.helpers import PleioTenantTestCase
from core.widget import BirthDaysWidgetToText


class BirthDayToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "birthdays",
            "settings": [
                {
                    "key": "title",
                    "value": "De lievelingsdag Sierra",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "description",
                    "value": "Sierra danst de tango",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "profileFieldGuid",
                    "value": "21c632dc-d7e3-4ff2-8e75-2a809fea90c1",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "showAge",
                    "value": "true",
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(BirthDaysWidgetToText.type, self.widget["type"])
        self.assertEqual(
            BirthDaysWidgetToText.to_text(self.widget),
            "De lievelingsdag Sierra\nSierra danst de tango",
        )
