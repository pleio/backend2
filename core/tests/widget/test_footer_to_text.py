from core.tests.helpers import PleioTenantTestCase
from core.widget import FooterWidgetToText


class FooterToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "footer",
            "settings": [
                {
                    "key": "title",
                    "value": "",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "links",
                    "value": '[{"url":"https://youarrentgonnaneedit.com","label":"Yagni"},{"url":"/news/view/dd47cf4f-d882-4bb4-b277-022f36874261/golf","label":"Golf"}]',
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(FooterWidgetToText.type, self.widget["type"])
        self.assertEqual(FooterWidgetToText.to_text(self.widget), "Yagni\nGolf")
