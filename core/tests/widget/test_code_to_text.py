from core.tests.helpers import PleioTenantTestCase
from core.widget import CodeWidgetToText


class CodeToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "html",
            "settings": [{"key": "description", "value": "<p>hello</p>"}],
        }

        self.widget_javascript = {
            "type": "html",
            "settings": [{"key": "description", "value": "<script>lalala</script>"}],
        }

        self.widget_none = {
            "type": "html",
            "settings": [{"key": "description", "value": None}],
        }

    def test_widget_to_text(self):
        self.assertEqual(CodeWidgetToText.type, self.widget["type"])
        self.assertEqual(CodeWidgetToText.to_text(self.widget), "hello")

    def test_widget_to_text_javascript(self):
        self.assertEqual(CodeWidgetToText.to_text(self.widget_javascript), "")

    def test_widget_to_text_none_value(self):
        self.assertEqual(CodeWidgetToText.to_text(self.widget_none), "")
