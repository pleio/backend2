from mixer.backend.django import mixer

from core.tests.helpers import PleioTenantTestCase
from core.widget import PollWidgetToText
from entities.poll.models import Poll


class PollToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.poll = mixer.blend(Poll, title="Bravo Charlie")

        self.widget = {
            "type": "poll",
            "settings": [
                {
                    "key": "pollGuid",
                    "value": self.poll.guid,
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(PollWidgetToText.type, self.widget["type"])
        self.assertEqual(PollWidgetToText.to_text(self.widget), "Bravo Charlie")
