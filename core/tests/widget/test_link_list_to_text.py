from core.tests.helpers import PleioTenantTestCase
from core.widget import LinkListWidgetToText


class LinkListToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "linkList",
            "settings": [
                {
                    "key": "title",
                    "value": "Mike November",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "links",
                    "links": [
                        {
                            "id": "1",
                            "label": "Foxtrot",
                            "url": "/blog/view/2dc868cf-3f1b-4df7-a8d0-ca4de22ca9f0/foxtrot",
                        },
                        {
                            "id": "2",
                            "label": "Golf",
                            "url": "/news/view/dd47cf4f-d882-4bb4-b277-022f36874261/golf",
                        },
                    ],
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(LinkListWidgetToText.type, self.widget["type"])
        self.assertEqual(
            LinkListWidgetToText.to_text(self.widget),
            "Mike November\nFoxtrot\n/blog/view/2dc868cf-3f1b-4df7-a8d0-ca4de22ca9f0/foxtrot\nGolf\n/news/view/dd47cf4f-d882-4bb4-b277-022f36874261/golf",
        )
