from core.tests.helpers import PleioTenantTestCase
from core.widget import FeaturedWidgetToText


class FeaturedToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "featured",
            "settings": [
                {
                    "key": "title",
                    "value": "Romeo",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "subtypes",
                    "value": "blog",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "limit",
                    "value": "6",
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(FeaturedWidgetToText.type, self.widget["type"])
        self.assertEqual(FeaturedWidgetToText.to_text(self.widget), "Romeo")
