from core.tests.helpers import PleioTenantTestCase
from core.widget import LeadWidgetToText


class LeadToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "lead",
            "settings": [
                {
                    "key": "image",
                    "value": "http://test1.pleio.local:8000/file/download/6e1e330b-abfc-4d18-9a12-8546b77b7cf7/salamander-13-coloring-page.gif",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "imageAlt",
                    "value": "Hotel India",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "title",
                    "value": "Julliet",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "link",
                    "value": "",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "poshorizontal",
                    "value": "1",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "posvertical",
                    "value": "1",
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(LeadWidgetToText.type, self.widget["type"])
        self.assertEqual(
            LeadWidgetToText.to_text(self.widget), "Julliet\n\nHotel India"
        )
