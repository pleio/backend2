from core.tests.helpers import PleioTenantTestCase
from core.widget import CallToActionWidgetToText


class CallToActionToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "callToAction",
            "settings": [
                {
                    "key": "title",
                    "value": "Alpha",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "description",
                    "value": "Bravo",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "link",
                    "value": "https://erlendtermaat.nl",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "buttonText",
                    "value": "Charlie",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "image",
                    "value": None,
                    "attachmentId": "799aba56-e4a9-48f0-a337-133ded482696",
                    "richDescription": None,
                },
                {
                    "key": "imageAlt",
                    "value": "Uniform",
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(CallToActionWidgetToText.type, self.widget["type"])
        self.assertEqual(
            CallToActionWidgetToText.to_text(self.widget),
            "Alpha\nBravo\nCharlie\nUniform",
        )
