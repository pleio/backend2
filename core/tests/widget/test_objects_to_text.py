from core.tests.helpers import PleioTenantTestCase
from core.widget import ObjectsWidgetToText


class ObjectsToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "objects",
            "settings": [
                {
                    "key": "title",
                    "value": "Oscar",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "subtypes",
                    "value": "group",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "categoryTags",
                    "value": "[]",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "tags",
                    "value": "",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "numberOfItems",
                    "value": "5",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "limit",
                    "value": "20",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "allowDisableLimit",
                    "value": "true",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "showDate",
                    "value": "false",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "sortBy",
                    "value": "timePublished-desc",
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(ObjectsWidgetToText.type, self.widget["type"])
        self.assertEqual(ObjectsWidgetToText.to_text(self.widget), "Oscar")
