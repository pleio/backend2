from core.tests.helpers import PleioTenantTestCase
from core.widget import TextWidgetToText


class TextToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "text",
            "settings": [
                {
                    "key": "richDescription",
                    "value": None,
                    "attachmentId": None,
                    "richDescription": '{"type":"doc","content":[{"type":"paragraph","attrs":{"intro":false},"content":[{"type":"text","text":"Quebec"}]},{"type":"image","attrs":{"src":"/attachment/entity/11040908-a1f7-41c9-badc-775092678c97","size":"large","alt":null,"caption":null}}]}',
                },
                {
                    "key": "textSize",
                    "value": "normal",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "backgroundColor",
                    "value": "white",
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(TextWidgetToText.type, self.widget["type"])
        self.assertEqual(TextWidgetToText.to_text(self.widget), "Quebec")
