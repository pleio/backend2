from core.tests.helpers import PleioTenantTestCase
from core.widget import EventsWidgetToText


class EventsToTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.widget = {
            "type": "events",
            "settings": [
                {
                    "key": "itemCount",
                    "value": "5",
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

    def test_widget_to_text(self):
        self.assertEqual(EventsWidgetToText.type, self.widget["type"])
        self.assertEqual(EventsWidgetToText.to_text(self.widget), "Agenda")
