from unittest.mock import patch

from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory
from entities.file.models import FileFolder
from user.factories import UserFactory


class AddAttachmentTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.authenticatedUser = UserFactory()
        self.authenticatedUser2 = UserFactory()

        self.mutation = """
            mutation addAttachment($input: addAttachmentInput!) {
                addAttachment(input: $input) {
                    attachment {
                        id
                        url
                        downloadUrl
                        mimeType
                        name
                    }
                }
            }
        """

    def test_add_attachment_anonymous(self):
        file_mock = self.use_mock_file("test.gif")
        file_mock.name = "test.gif"
        file_mock.content_type = "image/gif"

        variables = {
            "input": {
                "file": file_mock,
            }
        }

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)

    @patch("entities.file.models.FileFolder.scan")
    @patch("core.resolvers.shared.strip_exif")
    def test_add_attachment(self, mocked_strip_exif, mocked_scan):
        file_mock = self.use_mock_file("test.gif")
        file_mock.name = "test.gif"
        file_mock.content_type = "image/gif"

        variables = {
            "input": {
                "file": file_mock,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        attachment = FileFolder.objects.get(
            id=data["addAttachment"]["attachment"]["id"]
        )
        self.assertEqual(data["addAttachment"]["attachment"]["id"], str(attachment.id))
        self.assertEqual(
            data["addAttachment"]["attachment"]["url"], attachment.attachment_url
        )
        self.assertEqual(
            data["addAttachment"]["attachment"]["downloadUrl"], attachment.download_url
        )
        self.assertEqual(
            data["addAttachment"]["attachment"]["mimeType"], attachment.mime_type
        )
        self.assertEqual(data["addAttachment"]["attachment"]["name"], file_mock.name)
        self.assertTrue(mocked_strip_exif.called)
        self.assertTrue(mocked_scan.called)


class TestAddFolderAttachment(PleioTenantTestCase):
    """
    Test that add a folder as attachment also links files in the folder as attachment.
    """

    def setUp(self):
        super().setUp()
        self.owner = self.remember(UserFactory())
        self.folder = self.remember(FolderFactory(owner=self.owner))
        self.file = self.remember(FileFactory(owner=self.owner, parent=self.folder))

    def test_referencing_to_both_folder_and_content(self):
        self.assertFalse(self.folder.referenced_by.exists())
        self.assertFalse(self.file.referenced_by.exists())

        BlogFactory(
            owner=self.owner, rich_description=self.tiptap_attachment(self.folder)
        )

        self.assertEqual(self.folder.referenced_by.count(), 1)
        self.assertEqual(self.file.referenced_by.count(), 1)
