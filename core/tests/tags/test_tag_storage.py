from core.tests.helpers import PleioTenantTestCase


class TestTagStorageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        from core.utils.tags import TagCategoryStorage

        self.expected_settings = [
            {
                "name": "category",
                "values": [
                    {"value": "alpha", "synonyms": ["India", "Juliet", "Kilo", "Lima"]},
                    {"value": "bravo", "synonyms": ["Oskar", "Papa", "Quebec"]},
                    {"value": "charlie", "synonyms": []},
                    {"value": "foxtrot", "synonyms": []},
                ],
            },
            {"name": "category2", "values": []},
        ]

        self.override_config(TAG_CATEGORIES=self.expected_settings)
        self.storage = TagCategoryStorage()

    def test_get_all_settings(self):
        self.assertEqual(self.storage.all_settings(), self.expected_settings)

    def test_get_specific_settings(self):
        self.assertEqual(self.storage.get_settings("not_existing"), None)
        self.assertEqual(
            self.storage.get_settings("category"), self.expected_settings[0]
        )

    def test_get_all_tag_categories(self):
        self.assertEqual(
            self.storage.all_tag_categories(),
            [
                {
                    "name": "category",
                    "values": ["alpha", "bravo", "charlie", "foxtrot"],
                },
                {"name": "category2", "values": []},
            ],
        )

    def test_get_specific_tag_category(self):
        self.assertEqual(self.storage.get_tag_category("not_existing"), None)
        self.assertEqual(
            self.storage.get_tag_category("category"),
            {"name": "category", "values": ["alpha", "bravo", "charlie", "foxtrot"]},
        )

    def test_move_tag_category_up(self):
        self.storage.move_tag_category("category", 1)
        categories = [t["name"] for t in self.storage.all_settings()]
        self.assertEqual(categories, ["category2", "category"])

    def test_move_tag_category_down(self):
        self.storage.move_tag_category("category2", 0)
        categories = [t["name"] for t in self.storage.all_settings()]
        self.assertEqual(categories, ["category2", "category"])

    def test_store_new_tag_category(self):
        self.storage.store_tag_category("new_category", {"name": "new_category"})

        self.assertEqual(
            self.storage.all_settings(),
            [
                {
                    "name": "category",
                    "values": [
                        {
                            "synonyms": ["India", "Juliet", "Kilo", "Lima"],
                            "value": "alpha",
                        },
                        {"synonyms": ["Oskar", "Papa", "Quebec"], "value": "bravo"},
                        {"synonyms": [], "value": "charlie"},
                        {"synonyms": [], "value": "foxtrot"},
                    ],
                },
                {"name": "category2", "values": []},
                {"name": "new_category", "values": []},
            ],
        )

    def test_store_existing_tag_category(self):
        self.storage.store_tag_category("category", {"name": "new_category"})

        self.assertEqual(
            self.storage.all_settings(),
            [
                {"name": "new_category", "values": []},
                {"name": "category2", "values": []},
            ],
        )

    def test_store_tag_category_with_values(self):
        self.storage.store_tag_category_values(
            "category", ["foxtrot", "echo", "delta", "charlie", "bravo", "alpha"]
        )

        self.assertEqual(
            self.storage.get_settings("category"),
            {
                "name": "category",
                "values": [
                    {"value": "foxtrot", "synonyms": []},
                    {"value": "echo", "synonyms": []},
                    {"value": "delta", "synonyms": []},
                    {"value": "charlie", "synonyms": []},
                    {"value": "bravo", "synonyms": ["Oskar", "Papa", "Quebec"]},
                    {"value": "alpha", "synonyms": ["India", "Juliet", "Kilo", "Lima"]},
                ],
            },
        )

    def test_flat_category_tags(self):
        def _flat(category):
            return [*self.storage.flat_category_tags(category, add_synonyms=False)]

        self.assertEqual(
            _flat({"name": "category", "values": ["alpha"]}),
            ["alpha (category)"],
        )
        self.assertEqual(
            _flat({"name": "category", "values": ["bravo"]}),
            ["bravo (category)"],
        )
        self.assertEqual(
            _flat({"name": "category", "values": ["charlie"]}),
            ["charlie (category)"],
        )
        self.assertEqual(
            _flat({"name": "category", "values": ["delta"]}),
            [],
        )
        self.assertEqual(
            _flat({"name": "category", "values": ["foxtrot"]}),
            ["foxtrot (category)"],
        )

    def test_flat_category_tags_with_synonyms(self):
        def _flat(category):
            return [*self.storage.flat_category_tags(category, add_synonyms=True)]

        self.assertEqual(
            _flat({"name": "category", "values": ["alpha"]}),
            [
                "alpha (category)",
                "india (category)",
                "juliet (category)",
                "kilo (category)",
                "lima (category)",
            ],
        )
        self.assertEqual(
            _flat({"name": "category", "values": ["bravo"]}),
            [
                "bravo (category)",
                "oskar (category)",
                "papa (category)",
                "quebec (category)",
            ],
        )
        self.assertEqual(
            _flat({"name": "category", "values": ["charlie"]}),
            ["charlie (category)"],
        )
