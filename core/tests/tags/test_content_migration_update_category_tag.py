from mixer.backend.django import mixer

from core import config
from core.factories import GroupFactory
from core.models import UserProfile
from core.tests.helpers import PleioTenantTestCase
from core.utils.tags import (
    WEIGHT_NOT_SPECIFIED,
    DuplicateValueError,
    UpdateTagCategoryTag,
    ValueNotFoundError,
)
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestContentMigrationRenameCategoryTag(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "category",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                    ],
                }
            ]
        )
        self.owner = UserFactory()
        self.migration = UpdateTagCategoryTag(
            "category", "alpha", "next_to_alpha", [], WEIGHT_NOT_SPECIFIED
        )

    def test_migrate_config(self):
        # When.
        self.migration.process()

        # Then.
        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "next_to_alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                    ],
                }
            ],
        )

    def test_update_weight(self):
        self.migration.weight = 1
        self.migration.process()

        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "bravo", "synonyms": []},
                        {"value": "next_to_alpha", "synonyms": []},
                    ],
                }
            ],
        )

    def test_update_weight_only(self):
        self.migration.weight = 1
        self.migration.new_value = self.migration.value

        self.migration.process()

        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "bravo", "synonyms": []},
                        {"value": "alpha", "synonyms": []},
                    ],
                }
            ],
        )

    def test_update_duplicate_value(self):
        self.migration.new_value = "bravo"

        with self.assertRaises(DuplicateValueError):
            self.migration.process()

    def test_update_non_existing_name(self):
        self.migration.category_settings = None

        with self.assertRaises(ValueNotFoundError):
            self.migration.process()

    def test_update_non_existing_value(self):
        self.migration.value = "non-existing"

        with self.assertRaises(ValueNotFoundError):
            self.migration.process()

    def test_migrate_entities(self):
        blog = BlogFactory(
            owner=self.owner,
            category_tags=[{"name": "category", "values": ["alpha", "bravo"]}],
        )

        self.migration.process()
        blog.refresh_from_db()

        self.assertEqual(
            blog.category_tags,
            [{"name": "category", "values": ["next_to_alpha", "bravo"]}],
        )

    def test_migrate_groups(self):
        group = GroupFactory(
            owner=self.owner,
            category_tags=[{"name": "category", "values": ["alpha", "bravo"]}],
        )

        self.migration.process()
        group.refresh_from_db()

        self.assertEqual(
            group.category_tags,
            [{"name": "category", "values": ["next_to_alpha", "bravo"]}],
        )

    def test_migration_profile(self):
        profile, created = UserProfile.objects.get_or_create(user=self.owner)
        profile.overview_email_categories = [
            {"name": "category", "values": ["alpha", "bravo"]}
        ]
        profile.save()

        self.migration.process()
        profile.refresh_from_db()

        self.assertEqual(
            profile.overview_email_categories,
            [{"name": "category", "values": ["next_to_alpha", "bravo"]}],
        )

    def test_migrate_objects_widget(self):
        page = mixer.blend("cms.Page", page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "objects",
                                "settings": [
                                    {
                                        "key": "categoryTags",
                                        "value": '[{"name":"category","values":["alpha","bravo"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name":"category","values":["next_to_alpha","bravo"]}]',
        )

    def test_migrate_activity_widget(self):
        page = mixer.blend("cms.Page", page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "activity",
                                "settings": [
                                    {
                                        "key": "tagFilter",
                                        "value": '[{"name":"category","values":["alpha","bravo"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name":"category","values":["next_to_alpha","bravo"]}]',
        )


class TestContentMigrationRenameCategoryTagHasChanged(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "category",
                    "values": [
                        {"value": "alpha", "synonyms": ["first", "eerste"]},
                        {"value": "bravo", "synonyms": []},
                    ],
                }
            ]
        )

    def test_has_changed(self):
        migration = UpdateTagCategoryTag(
            "category", "bravo", "next_to_bravo", [], WEIGHT_NOT_SPECIFIED
        )
        migration.process()
        self.assertTrue(migration.has_changed())

    def test_has_not_changed(self):
        migration = UpdateTagCategoryTag(
            "category", "bravo", "bravo", [], WEIGHT_NOT_SPECIFIED
        )
        migration.process()
        self.assertFalse(migration.has_changed())

    def test_has_changed_synonyms(self):
        migration = UpdateTagCategoryTag(
            "category", "alpha", "alpha", ["first"], WEIGHT_NOT_SPECIFIED
        )
        migration.process()
        self.assertTrue(migration.has_changed())

    def test_has_not_changed_synonyms(self):
        migration = UpdateTagCategoryTag(
            "category", "alpha", "alpha", ["first", "eerste"], WEIGHT_NOT_SPECIFIED
        )
        migration.process()
        self.assertFalse(migration.has_changed())
