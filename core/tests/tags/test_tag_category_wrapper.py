from unittest.mock import patch

from core.tests.helpers import PleioTenantTestCase


class TestTagCategoryWrapper(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "Foo Category",
                    "values": [
                        {"value": "Foo", "synonyms": ["Alpha", "Bravo"]},
                        {"value": "Bar", "synonyms": []},
                    ],
                },
            ]
        )

    @staticmethod
    def wrapper_class():
        from core.utils.tags import TagCategoryWrapper

        return TagCategoryWrapper

    def test_load_from_config(self):
        tag_category_wrapper = self.wrapper_class().load_from_config("Foo Category")
        self.assertEqual(tag_category_wrapper.tags, ["Foo", "Bar"])

    def test_load_non_existing_from_config(self):
        tag_category_wrapper = self.wrapper_class().load_from_config("non_existing")
        self.assertEqual(tag_category_wrapper.tags, [])

    def test_add(self):
        tag_category_wrapper = self.wrapper_class().load_from_config("Foo Category")
        tag_category_wrapper.add("Baz")
        self.assertEqual(tag_category_wrapper.tags, ["Foo", "Bar", "Baz"])

    @patch("core.utils.tags.TagCategoryStorage.store_tag_category_values")
    def test_save_unsorted(self, store_tag_category_values):
        tag_category_wrapper = self.wrapper_class()("Alpha")
        tag_category_wrapper.save("Foo Category", sort=False)

        store_tag_category_values.assert_called_once_with(
            "Foo Category", ["Foo", "Bar", "Alpha"]
        )

    @patch("core.utils.tags.TagCategoryStorage.store_tag_category_values")
    def test_save_sorted(self, store_tag_category_values):
        tag_category_wrapper = self.wrapper_class()("Alpha")
        tag_category_wrapper.save("Foo Category", sort=True)

        store_tag_category_values.assert_called_once_with(
            "Foo Category", ["Alpha", "Bar", "Foo"]
        )

    def test_as_dict(self):
        tag_category_wrapper = self.wrapper_class()("Alpha")

        self.assertEqual(
            tag_category_wrapper.as_dict("Foo Category"),
            {"name": "Foo Category", "values": ["Alpha"]},
        )
