from core import config
from core.tests.helpers import PleioTenantTestCase
from core.utils.tags import (
    WEIGHT_NOT_SPECIFIED,
    AddTagCategory,
    DuplicateValueError,
    ExtendTagCategory,
    ValueNotFoundError,
)


class TestAddTagCategoryTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "category",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                        {"value": "charlie", "synonyms": []},
                        {"value": "foxtrot", "synonyms": []},
                    ],
                }
            ]
        )

    def test_add_category(self):
        mutation = AddTagCategory("new_category")
        mutation.process()

        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                        {"value": "charlie", "synonyms": []},
                        {"value": "foxtrot", "synonyms": []},
                    ],
                },
                {
                    "name": "new_category",
                    "values": [],
                },
            ],
        )

    def test_add_duplicate_category(self):
        with self.assertRaises(DuplicateValueError):
            mutation = AddTagCategory("category")
            mutation.process()

    def test_extend_tag_category(self):
        mutation = ExtendTagCategory("category", "cero", [], WEIGHT_NOT_SPECIFIED)
        mutation.process()

        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                        {"value": "charlie", "synonyms": []},
                        {"value": "foxtrot", "synonyms": []},
                        {"value": "cero", "synonyms": []},
                    ],
                }
            ],
        )

    def test_append_tag_category_tag(self):
        mutation = ExtendTagCategory("category", "delta", [], 3)
        mutation.process()
        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                        {"value": "charlie", "synonyms": []},
                        {"value": "delta", "synonyms": []},
                        {"value": "foxtrot", "synonyms": []},
                    ],
                }
            ],
        )

    def test_append_tag_category_tag_duplicate(self):
        with self.assertRaises(DuplicateValueError):
            mutation = ExtendTagCategory("category", "alpha", [], WEIGHT_NOT_SPECIFIED)
            mutation.process()

    def test_append_to_non_existing_category(self):
        with self.assertRaises(ValueNotFoundError):
            mutation = ExtendTagCategory(
                "non-existing", "alpha", [], WEIGHT_NOT_SPECIFIED
            )
            mutation.process()
