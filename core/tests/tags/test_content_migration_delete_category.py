from mixer.backend.django import mixer

from core import config
from core.factories import GroupFactory
from core.models import UserProfile
from core.tests.helpers import PleioTenantTestCase
from core.utils.tags import DeleteTagCategory, ValueNotFoundError
from entities.blog.factories import BlogFactory
from entities.cms.models import Page
from user.factories import UserFactory


class TestContentMigrationDeleteCategory(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            # Deze moeten nog worden verbeterd
            TAG_CATEGORIES=[
                {
                    "name": "to_remove",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                        {"value": "charlie", "synonyms": []},
                    ],
                },
                {
                    "name": "to_keep",
                    "values": [
                        {"value": "delta", "synonyms": []},
                        {"value": "echo", "synonyms": []},
                        {"value": "foxtrot", "synonyms": []},
                    ],
                },
            ]
        )
        self.owner = UserFactory()
        self.migration = DeleteTagCategory("to_remove")
        self.INITIAL_TAGS = [
            {
                "name": "to_remove",
                "values": ["bravo"],
            },
            {
                "name": "to_keep",
                "values": ["delta", "foxtrot"],
            },
        ]

    def test_remove_non_existing_category(self):
        self.migration = DeleteTagCategory("non_existing")

        with self.assertRaises(ValueNotFoundError):
            self.migration.process()

    def test_migrate_config(self):
        # When.
        self.migration.process()

        # Then.
        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "to_keep",
                    "values": [
                        {"value": "delta", "synonyms": []},
                        {"value": "echo", "synonyms": []},
                        {"value": "foxtrot", "synonyms": []},
                    ],
                }
            ],
        )

    def test_migrate_entities(self):
        blog = BlogFactory(owner=self.owner, category_tags=self.INITIAL_TAGS)

        self.migration.process()
        blog.refresh_from_db()

        self.assertEqual(
            blog.category_tags,
            [{"name": "to_keep", "values": ["delta", "foxtrot"]}],
        )

    def test_migrate_groups(self):
        group = GroupFactory(owner=self.owner, category_tags=self.INITIAL_TAGS)

        self.migration.process()
        group.refresh_from_db()

        self.assertEqual(
            group.category_tags,
            [{"name": "to_keep", "values": ["delta", "foxtrot"]}],
        )

    def test_migration_profile(self):
        profile, created = UserProfile.objects.get_or_create(user=self.owner)
        profile.overview_email_categories = self.INITIAL_TAGS
        profile.save()

        self.migration.process()
        profile.refresh_from_db()

        self.assertEqual(
            profile.overview_email_categories,
            [{"name": "to_keep", "values": ["delta", "foxtrot"]}],
        )

    def test_keep_tags_switch(self):
        """
        Test that if the keep-tags switch is enabled the removed tags are stored in the custom tags where this applies.
        """
        blog = BlogFactory(
            owner=self.owner, category_tags=self.INITIAL_TAGS, tags=["First of many"]
        )
        self.migration.keep_tags = True

        self.migration.process()
        blog.refresh_from_db()

        self.assertEqual(blog.tags, ["First of many", "bravo"])

    def test_migrate_objects_widget(self):
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "objects",
                                "settings": [
                                    {
                                        "key": "categoryTags",
                                        "value": '[{"name":"to_remove","values":["bravo"]},{"name":"to_keep","values":["delta","foxtrot"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name":"to_keep","values":["delta","foxtrot"]}]',
        )

    def test_migrate_activity_widget(self):
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "activity",
                                "settings": [
                                    {
                                        "key": "tagFilter",
                                        "value": '[{"name":"to_remove","values":["bravo"]},{"name":"to_keep","values":["delta","foxtrot"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name":"to_keep","values":["delta","foxtrot"]}]',
        )

    def test_keep_tags_switch_objects_widget(self):
        """
        Test that if the keep-tags switch is enabled the removed tags are stored in a widget setting with key tags.
        """
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "objects",
                                "settings": [
                                    {
                                        "key": "categoryTags",
                                        "value": '[{"name":"to_remove","values":["bravo"]},{"name":"to_keep","values":["delta","foxtrot"]}]',
                                    },
                                    {"key": "tags", "value": "tag4,tag5"},
                                ],
                            },
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.keep_tags = True
        self.migration.process()
        page.refresh_from_db()

        self.assertIn(
            "bravo",
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][1][
                "value"
            ].split(","),
        )
        self.assertIn(
            "tag4",
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][1][
                "value"
            ].split(","),
        )

    def test_keep_tags_switch_activity_widget(self):
        """
        Test that if the keep-tags switch is enabled the removed tags are stored in a widget setting with key tags.
        """
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "activity",
                                "settings": [
                                    {
                                        "key": "categoryTags",
                                        "value": '[{"name":"to_remove","values":["bravo"]},{"name":"to_keep","values":["delta","foxtrot"]}]',
                                    }
                                ],
                            },
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.keep_tags = True
        self.migration.process()
        page.refresh_from_db()

        self.assertIn(
            "bravo",
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][1][
                "value"
            ].split(","),
        )
