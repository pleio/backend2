from mixer.backend.django import mixer

from core import config
from core.factories import GroupFactory
from core.models import UserProfile
from core.tests.helpers import PleioTenantTestCase
from core.utils.tags import (
    WEIGHT_NOT_SPECIFIED,
    DuplicateValueError,
    UpdateTagCategory,
    ValueNotFoundError,
)
from entities.blog.factories import BlogFactory
from entities.cms.models import Page
from user.factories import UserFactory


class TestContentMigrationUpdateCategory(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "original",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                    ],
                },
                {
                    "name": "existing_category",
                    "values": [
                        {"value": "charlie", "synonyms": []},
                        {"value": "delta", "synonyms": []},
                        {"value": "echo", "synonyms": []},
                    ],
                },
            ]
        )
        self.owner = UserFactory()
        self.migration = UpdateTagCategory(
            "original", "replacement", WEIGHT_NOT_SPECIFIED
        )

    def test_migrate_to_duplicate_name(self):
        self.migration.name = "existing_category"

        with self.assertRaises(DuplicateValueError):
            self.migration.process()

    def test_migrate_non_existing_name(self):
        self.migration.old_name = "non-existing"

        with self.assertRaises(ValueNotFoundError):
            self.migration.process()

    def test_migrate_config(self):
        # Given.
        self.migration.process()

        # Then.
        self.assertEqual(
            config.TAG_CATEGORIES[0],
            {
                "name": "replacement",
                "values": [
                    {"value": "alpha", "synonyms": []},
                    {"value": "bravo", "synonyms": []},
                ],
            },
        )

    def test_migrate_config_with_weight(self):
        self.migration.weight = 1
        self.migration.process()

        self.maxDiff = None

        # Then.
        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "existing_category",
                    "values": [
                        {"value": "charlie", "synonyms": []},
                        {"value": "delta", "synonyms": []},
                        {"value": "echo", "synonyms": []},
                    ],
                },
                {
                    "name": "replacement",
                    "values": [
                        {"value": "alpha", "synonyms": []},
                        {"value": "bravo", "synonyms": []},
                    ],
                },
            ],
        )

    def test_migrate_entities(self):
        blog = BlogFactory(
            owner=self.owner,
            category_tags=[{"name": "original", "values": ["alpha", "bravo"]}],
        )

        self.migration.process()
        blog.refresh_from_db()

        self.assertEqual(
            blog.category_tags,
            [{"name": "replacement", "values": ["alpha", "bravo"]}],
        )

    def test_migrate_groups(self):
        group = GroupFactory(
            owner=self.owner,
            category_tags=[{"name": "original", "values": ["alpha", "bravo"]}],
        )

        self.migration.process()
        group.refresh_from_db()

        self.assertEqual(
            group.category_tags, [{"name": "replacement", "values": ["alpha", "bravo"]}]
        )

    def test_migration_profile(self):
        profile, created = UserProfile.objects.get_or_create(user=self.owner)
        profile.overview_email_categories = [
            {"name": "original", "values": ["alpha", "bravo"]}
        ]
        profile.save()

        self.migration.process()
        profile.refresh_from_db()

        self.assertEqual(
            profile.overview_email_categories,
            [{"name": "replacement", "values": ["alpha", "bravo"]}],
        )

    def test_migrate_objects_widget(self):
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "objects",
                                "settings": [
                                    {
                                        "key": "categoryTags",
                                        "value": '[{"name":"original","values":["alpha","bravo"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name":"replacement","values":["alpha","bravo"]}]',
        )

    def test_migrate_search_widget(self):
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "search",
                                "settings": [
                                    {
                                        "key": "tagFilter",
                                        "value": '[{"name":"original","values":["alpha","bravo"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name":"replacement","values":["alpha","bravo"]}]',
        )
