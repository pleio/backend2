from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory


class TestFetchTagCategories(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = AdminFactory()

        self.query = """
            query GetTags {
                site {
                    tagCategories {
                        name
                        values
                    }
                }
                siteSettings {
                    tagCategorySettings {
                        name
                        values {
                            value
                            synonyms
                        }
                    }
                }
            }
        """

    def test_fetch_filled_tag_categories(self):
        self.override_config(TAG_CATEGORIES=[{"name": "category1"}])
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query)

        self.assertEqual(
            response["data"],
            {
                "site": {
                    "tagCategories": [{"name": "category1", "values": []}],
                },
                "siteSettings": {
                    "tagCategorySettings": [{"name": "category1", "values": []}],
                },
            },
        )

    def test_fetch_empty_tag_categories(self):
        self.override_config(TAG_CATEGORIES=[None])
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query)

        self.assertEqual(
            response["data"],
            {
                "site": {"tagCategories": []},
                "siteSettings": {"tagCategorySettings": []},
            },
        )

    def test_fetch_filled_tag_settings(self):
        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "setting1",
                    "values": [{"value": "value1", "synonyms": ["Alpha", "Bravo"]}],
                }
            ]
        )
        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.query)

        self.assertEqual(
            response["data"],
            {
                "site": {"tagCategories": [{"name": "setting1", "values": ["value1"]}]},
                "siteSettings": {
                    "tagCategorySettings": [
                        {
                            "name": "setting1",
                            "values": [
                                {"value": "value1", "synonyms": ["Alpha", "Bravo"]}
                            ],
                        }
                    ]
                },
            },
        )
