import json

from mixer.backend.django import mixer

from core import config
from core.factories import GroupFactory
from core.models import UserProfile
from core.tests.helpers import PleioTenantTestCase
from core.utils.tags import DeleteTagCategoryTag, ValueNotFoundError
from entities.blog.factories import BlogFactory
from entities.cms.models import Page
from user.factories import UserFactory


class TestContentMigrationDeleteCategoryTag(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "category",
                    "values": [
                        {"value": "to_remove", "synonyms": []},
                        {"value": "to_keep", "synonyms": []},
                        {"value": "also_keep", "synonyms": []},
                    ],
                },
            ]
        )
        self.owner = UserFactory()
        self.migration = DeleteTagCategoryTag("category", "to_remove")
        self.INITIAL_TAGS = [
            {
                "name": "category",
                "values": [
                    "to_remove",
                    "to_keep",
                ],
            },
        ]

    def test_remove_non_existing_category(self):
        self.migration = DeleteTagCategoryTag("non_existing", "to_remove")

        with self.assertRaises(ValueNotFoundError):
            self.migration.process()

    def test_remove_non_existing_tag(self):
        self.migration = DeleteTagCategoryTag("category", "non_existing")

        with self.assertRaises(ValueNotFoundError):
            self.migration.process()

    def test_migrate_config(self):
        # When.
        self.migration.process()

        # Then.
        self.assertEqual(
            config.TAG_CATEGORIES,
            [
                {
                    "name": "category",
                    "values": [
                        {"value": "to_keep", "synonyms": []},
                        {"value": "also_keep", "synonyms": []},
                    ],
                },
            ],
        )

    def test_migrate_entities(self):
        blog = BlogFactory(owner=self.owner, category_tags=self.INITIAL_TAGS)

        self.migration.process()
        blog.refresh_from_db()

        self.assertEqual(
            blog.category_tags,
            [{"name": "category", "values": ["to_keep"]}],
        )

    def test_migrate_groups(self):
        group = GroupFactory(owner=self.owner, category_tags=self.INITIAL_TAGS)

        self.migration.process()
        group.refresh_from_db()

        self.assertEqual(
            group.category_tags,
            [{"name": "category", "values": ["to_keep"]}],
        )

    def test_migration_profile(self):
        profile, created = UserProfile.objects.get_or_create(user=self.owner)
        profile.overview_email_categories = self.INITIAL_TAGS
        profile.save()

        self.migration.process()
        profile.refresh_from_db()

        self.assertEqual(
            profile.overview_email_categories,
            [{"name": "category", "values": ["to_keep"]}],
        )

    def test_keep_tag_switch(self):
        """
        Test that if the keep-tag switch is enabled the removed tag is stored in the custom tags where this applies.
        """
        blog = BlogFactory(
            owner=self.owner,
            category_tags=self.INITIAL_TAGS,
            tags=["First of many"],
        )
        self.migration.keep_tag = True

        self.migration.process()
        blog.refresh_from_db()

        self.assertEqual(blog.tags, ["First of many", "to_remove"])

    def test_migrate_objects_widget(self):
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "objects",
                                "settings": [
                                    {
                                        "key": "categoryTags",
                                        "value": '[{"name":"category","values":["to_remove","to_keep"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name": "category", "values": ["to_keep"]}]',
        )

    def test_migrate_activity_widget(self):
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "activity",
                                "settings": [
                                    {
                                        "key": "tagFilter",
                                        "value": '[{"name": "category","values":["to_remove","to_keep"]}]',
                                    }
                                ],
                            }
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name": "category", "values": ["to_keep"]}]',
        )

    def test_keep_tags_switch_objects_widget(self):
        """
        Test that if the keep-tags switch is enabled the removed tags are stored in a widget setting with key tags.
        """
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "objects",
                                "settings": [
                                    {
                                        "key": "tagFilter",
                                        "value": '[{"name":"category","values":["to_remove","to_keep"]}]',
                                    },
                                    {"key": "tags", "value": "tag4,tag5"},
                                ],
                            },
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.keep_tag = True
        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name": "category", "values": ["to_keep"]}]',
        )
        self.assertEqual(
            {"to_remove", "tag4", "tag5"},
            {
                *page.row_repository[0]["columns"][0]["widgets"][0]["settings"][1][
                    "value"
                ].split(",")
            },
        )

    def test_keep_tags_switch_activity_widget(self):
        """
        Test that if the keep-tags switch is enabled the removed tags are stored in a widget setting with key tags.
        """
        page = mixer.blend(Page, page_type="campagne")
        page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "activity",
                                "settings": [
                                    {
                                        "key": "tagFilter",
                                        "value": '[{"name":"category","values":["to_remove","to_keep"]}]',
                                    }
                                ],
                            },
                        ]
                    }
                ],
            }
        ]
        page.save()

        self.migration.keep_tag = True
        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][0]["value"],
            '[{"name": "category", "values": ["to_keep"]}]',
        )
        self.assertEqual(
            ["to_remove"],
            page.row_repository[0]["columns"][0]["widgets"][0]["settings"][1][
                "value"
            ].split(","),
        )

    def test_remove_from_widget_without_tags(self):
        """
        Test that a widget without tags is unaltered.
        """
        page = mixer.blend(Page, page_type="campagne")
        row_repository = json.dumps(
            [
                {
                    "columns": [
                        {
                            "widgets": [
                                {
                                    "type": "activity",
                                    "settings": [
                                        {
                                            "key": "tagFilter",
                                            "value": "[]",
                                        },
                                        {
                                            "key": "tags",
                                            "value": "",
                                        },
                                    ],
                                },
                            ]
                        }
                    ],
                }
            ]
        )

        page.row_repository = json.loads(row_repository)
        page.save()

        self.migration.keep_tag = True
        self.migration.process()
        page.refresh_from_db()

        self.assertEqual(page.row_repository, json.loads(row_repository))
