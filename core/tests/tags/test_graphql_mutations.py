from unittest import mock

from django.contrib.auth.models import AnonymousUser

from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class Generic:
    class BaseCategoryApiTestCase(PleioTenantTestCase):
        EXPECTED_MUTATION_METHOD = None

        def setUp(self):
            super().setUp()
            self.mutation = mock.patch(self.EXPECTED_MUTATION_METHOD).start()

        def execute_mutation(self, user):
            raise NotImplementedError()

        def test_execute_as_anonymous(self):
            with self.assertGraphQlError("not_logged_in"):
                self.execute_mutation(AnonymousUser())
            self.assertFalse(self.mutation.called)

        def test_execute_as_user(self):
            with self.assertGraphQlError("user_not_site_admin"):
                self.execute_mutation(UserFactory())
            self.assertFalse(self.mutation.called)

        def test_execute_as_admin(self):
            self.execute_mutation(AdminFactory())
            self.assertTrue(self.mutation.called)


class TestAddTagCategoryTestCase(Generic.BaseCategoryApiTestCase):
    EXPECTED_MUTATION_METHOD = "core.utils.tags.AddTagCategory.process"

    def execute_mutation(self, user):
        mutation = """
        mutation Mutation($input: MutateTagCategoryInput!) {
            addTagCategory(input: $input) {
                siteSettings {
                    name
                }
            }
        }
        """
        variables = {
            "input": {
                "name": "Foo",
            },
        }

        self.graphql_client.force_login(user)
        self.graphql_client.post(mutation, variables)


class TestUpdateTagCategoryTestCase(Generic.BaseCategoryApiTestCase):
    EXPECTED_MUTATION_METHOD = "core.utils.tags.UpdateTagCategory.process"

    def execute_mutation(self, user):
        mutation = """
        mutation Mutation($name: String!, $input: MutateTagCategoryInput!) {
            updateTagCategory(name: $name, input: $input) {
                siteSettings {
                    name
                }
            }
        }
        """
        variables = {
            "name": "Existing category",
            "input": {
                "name": "New category name",
            },
        }

        self.graphql_client.force_login(user)
        self.graphql_client.post(mutation, variables)


class TestExtendTagCategoryTestCase(Generic.BaseCategoryApiTestCase):
    EXPECTED_MUTATION_METHOD = "core.utils.tags.ExtendTagCategory.process"

    def execute_mutation(self, user):
        mutation = """
        mutation Mutation($name: String!, $input: MutateTagCategoryTagInput!) {
            extendTagCategory(name: $name, input: $input) {
                siteSettings {
                    name
                }
            }
        }
        """
        variables = {
            "name": "Existing category",
            "input": {
                "value": "New category name",
            },
        }

        self.graphql_client.force_login(user)
        self.graphql_client.post(mutation, variables)


class TestDeleteTagCategoryTestCase(Generic.BaseCategoryApiTestCase):
    EXPECTED_MUTATION_METHOD = "core.utils.tags.DeleteTagCategory.process"

    def execute_mutation(self, user):
        mutation = """
        mutation Mutation($name: String!) {
            deleteTagCategory(name: $name) {
                siteSettings {
                    name
                }
            }
        }
        """
        variables = {
            "name": "Existing category",
        }

        self.graphql_client.force_login(user)
        self.graphql_client.post(mutation, variables)


class TestUpdateTagCategoryTagTestCase(Generic.BaseCategoryApiTestCase):
    EXPECTED_MUTATION_METHOD = "core.utils.tags.UpdateTagCategoryTag.process"

    def execute_mutation(self, user):
        mutation = """
        mutation Mutation($name: String!, $value: String!, $input: MutateTagCategoryTagInput!) {
            updateTagCategoryTag(name: $name, value: $value, input: $input) {
                siteSettings {
                    name
                }
            }
        }
        """
        variables = {
            "name": "Existing category",
            "value": "Existing tag",
            "input": {
                "value": "New tag value",
            },
        }

        self.graphql_client.force_login(user)
        self.graphql_client.post(mutation, variables)


class TestDeleteTagCategoryTagTestCase(Generic.BaseCategoryApiTestCase):
    EXPECTED_MUTATION_METHOD = "core.utils.tags.DeleteTagCategoryTag.process"

    def execute_mutation(self, user):
        mutation = """
        mutation Mutation($name: String!, $value: String!) {
            deleteTagCategoryTag(name: $name, value: $value) {
                siteSettings {
                    name
                }
            }
        }
        """
        variables = {
            "name": "Existing category",
            "value": "Existing tag",
        }

        self.graphql_client.force_login(user)
        self.graphql_client.post(mutation, variables)
