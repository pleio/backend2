from unittest import mock

from graphql import GraphQLError

from core.factories import GroupFactory
from core.resolvers import shared
from core.resolvers.shared import assert_valid_abstract
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory
from user.factories import UserFactory


class SharedTests(PleioTenantTestCase):
    def test_clean_abstract(self):
        text = "x" * 320
        assert_valid_abstract(text)

    def test_clean_abstract_too_long(self):
        text = "x" * 321
        with self.assertRaises(GraphQLError):
            assert_valid_abstract(text)

    def test_clean_abstract_with_html(self):
        text = f"<p>{'x' * 320}</p>"
        assert_valid_abstract(text)

    def test_clean_abstract_too_long_with_html(self):
        text = f"<p>{'x' * 321}</p>"
        with self.assertRaises(GraphQLError):
            assert_valid_abstract(text)

    def test_clean_abstract_with_link(self):
        text = f'{"x" * 159}<a href="https://gibberish.nl"></a>{"x" * 160}'
        assert_valid_abstract(text)


class TestCopyHardReferencesTestCase(PleioTenantTestCase):
    """
    Testing 3 things of the core.resolvers.shared.copy_hard_references method.

    1. Test with group and without group: Shouldn't or should add personal reference
    2. Test with a parent that is or is not a personal folder: Shouldn't or should add a personal reference
    3. Test with an unreferenced or a referenced folder. Shouldn't or should update references of myself and the parent.
    """

    def setUp(self):
        super().setUp()
        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.file = self.remember(FileFactory(owner=self.owner))

        self.update_attachment_links = mock.patch(
            "core.models.rich_fields.AttachmentMixin.update_attachments_links"
        )
        self.persist_file = mock.patch("entities.file.models.FileFolder.persist_file")

    def test_with_group(self):
        # Given.
        group = self.remember(GroupFactory(owner=self.owner))

        # When
        persist_file = self.persist_file.start()
        shared.copy_attachment_references(self.file, group=group)

        # Then.
        self.assertFalse(persist_file.called)

    def test_without_group(self):
        # When
        persist_file = self.persist_file.start()
        shared.copy_attachment_references(self.file)

        # Then.
        self.assertTrue(persist_file.called)

    def test_with_personal_parent(self):
        # Given.
        parent = FolderFactory(owner=self.owner)
        parent.persist_file()

        # When
        persist_file = self.persist_file.start()
        shared.copy_attachment_references(self.file, parent=parent)

        # Then.
        self.assertTrue(persist_file.called)

    def test_with_hanging_parent(self):
        # Given.
        parent = FolderFactory(owner=self.owner)

        # When
        persist_file = self.persist_file.start()
        shared.copy_attachment_references(self.file, parent=parent)

        # Then.
        self.assertFalse(persist_file.called)

    def test_referenced_parent(self):
        # Given.
        parent = self.remember(FolderFactory(owner=self.owner))
        self.remember(
            BlogFactory(
                owner=self.owner, rich_description=self.tiptap_attachment(parent)
            )
        )

        # When
        update_attachment_links = self.update_attachment_links.start()
        shared.copy_attachment_references(self.file, parent=parent)

        # Then.
        self.assertTrue(update_attachment_links.called)

    def test_unreferenced_parent(self):
        # Given.
        parent = self.remember(FolderFactory(owner=self.owner))

        # When
        update_attachment_links = self.update_attachment_links.start()
        shared.copy_attachment_references(self.file, parent=parent)

        # Then.
        self.assertFalse(update_attachment_links.called)

    def test_changing_references(self):
        # Given.
        folder1 = self.remember(FolderFactory(owner=self.owner))
        file1 = self.remember(FileFactory(owner=self.owner, parent=folder1))
        blog1 = self.remember(
            BlogFactory(
                owner=self.owner, rich_description=self.tiptap_attachment(folder1)
            )
        )
        folder2 = self.remember(FolderFactory(owner=self.owner))
        blog2 = self.remember(
            BlogFactory(
                owner=self.owner, rich_description=self.tiptap_attachment(folder2)
            )
        )

        self.assertTrue(file1.referenced_by.filter(container_fk=blog1.pk).exists())
        self.assertFalse(file1.referenced_by.filter(container_fk=blog2.pk).exists())
        self.assertTrue(folder1.referenced_by.filter(container_fk=blog1.pk).exists())
        self.assertTrue(folder2.referenced_by.filter(container_fk=blog2.pk).exists())

        # When
        file1.parent = folder2
        file1.save()
        shared.copy_attachment_references(file1, parent=folder2)

        # Then.
        # References have switched
        self.assertFalse(file1.referenced_by.filter(container_fk=blog1.pk).exists())
        self.assertTrue(file1.referenced_by.filter(container_fk=blog2.pk).exists())
