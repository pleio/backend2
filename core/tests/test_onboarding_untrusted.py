from http import HTTPStatus
from unittest.mock import patch
from urllib.parse import quote

from django.test import override_settings

from core.models import (
    ProfileField,
    SiteAccessRequest,
    SiteInvitation,
    UserProfileField,
)
from core.tests.helpers import PleioTenantTestCase
from user.models import User


class OnboardingTestCase(PleioTenantTestCase):
    VALIDATE_BUTTON = '<a class="elgg-button button" href="/login" rel="nofollow">Toegangsaanvraag verifiëren</a>'
    SUBMIT_BUTTON = (
        '<input type="submit" value="Toegang aanvragen" class="elgg-button button">'
    )

    def setUp(self):
        super().setUp()

        # Prepare fields
        self.field = ProfileField.objects.create(
            key="profile_field1",
            name="Enter your least important detail",
            is_in_onboarding=True,
            is_mandatory=True,
        )

        self.override_config(
            PROFILE_SECTIONS=[{"name": "", "profileFieldGuids": [str(self.field.id)]}],
            IS_CLOSED=True,
            DIRECT_REGISTRATION_DOMAINS=["pleio.nl"],
            ONBOARDING_ENABLED=True,
            ONBOARDING_INTRO="There is an intro",
            ONBOARDING_FORCE_EXISTING_USERS=False,
        )

        self.EXPECTED_REDIRECT = "/events"
        self.URL = "/onboarding?next=" + quote(self.EXPECTED_REDIRECT)

        self.CLAIMS = {
            "email": "v.test@example.nl",
            "name": "Visitor Test",
            "picture": None,
            "is_government": False,
            "has_2fa_enabled": True,
            "sub": "1234",
            "is_superadmin": False,
        }

        self.EXPECTED_PROFILE_VALUE = "N.A."
        self.FORM_DATA = {self.field.guid: self.EXPECTED_PROFILE_VALUE}

        self.update_session(onboarding_claims={**self.CLAIMS})

        self.send_notifications = patch(
            "core.models.site.SiteAccessRequest.send_notifications"
        ).start()

    def test_onboarding_page_without_access_request(self):
        """Enter the page without an access request should show the onboarding form"""

        # When
        response = self.client.get(self.URL)

        # Then
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "registration/onboarding.html")
        self.assertContains(response, self.SUBMIT_BUTTON, html=True)
        self.assertNotContains(response, self.VALIDATE_BUTTON, html=True)

    def test_onboarding_page_with_pending_access_request(self):
        """Enter the page with an access request should show a read-only onboarding form"""

        # Given
        self._create_access_request()

        # When
        response = self.client.get(self.URL)

        # Then
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "registration/onboarding.html")
        self.assertContains(response, self.VALIDATE_BUTTON, html=True)
        self.assertNotContains(response, self.SUBMIT_BUTTON, html=True)

    def test_onboarding_page_with_rejected_access_request(self):
        """
        Enter the page with a rejected access request should show a normal onboarding form with the
        reason why it got rejected.
        """

        # Given
        self._create_access_request(processed=True, reason="Not in for reason at all")

        # When
        response = self.client.get(self.URL)

        # Then
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, "registration/onboarding.html")
        self.assertContains(response, "Not in for reason at all")
        self.assertContains(response, self.SUBMIT_BUTTON, html=True)
        self.assertNotContains(response, self.VALIDATE_BUTTON, html=True)

    @override_settings(
        AUTHENTICATION_BACKENDS=["django.contrib.auth.backends.ModelBackend"]
    )
    def test_onboarding_page_with_approved_access_request(self):
        """
        Enter the page with an approved access request should finalize the onboarding process and
        move to the last known destination page.
        """

        # Given
        self._create_access_request(accepted=True, processed=True)

        # When
        response = self.client.get(self.URL)

        # Then
        self.assertValidOnboardingCompletion(response)

    def test_onboarding_submit_without_account_with_invitation(self):
        """
        Submit the form without account but with an invitation should result in a new user being created
        """

        # Given
        SiteInvitation.objects.create(email=self.CLAIMS["email"])

        # When
        response = self.client.post(self.URL, self.FORM_DATA)

        # Then
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.url, self.EXPECTED_REDIRECT)
        self.assertTrue(User.objects.filter(email=self.CLAIMS["email"]).exists())
        self.assertEqual(response.wsgi_request.user.email, self.CLAIMS["email"])

    def test_onboarding_submit_without_access_request(self):
        """
        Submit the form without an existing access request should start the site access request procedure
        """

        # When
        response = self.client.post(self.URL, self.FORM_DATA)

        # Then
        self.assertValidAccessRequestProcedure(response)

    def test_onboarding_submit_with_access_request(self):
        """Submit the form with an existing in-progress access request should not change anything."""

        # Given
        self._create_access_request(profile={self.field.guid: "Original value"})
        data = {self.field.guid: "Overwrite attempt"}

        # When
        response = self.client.post(self.URL, data)

        # Then
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, "/onboarding/requested")
        self.assertTrue(
            SiteAccessRequest.objects.filter(email=self.CLAIMS["email"]).exists()
        )

        access_request = SiteAccessRequest.objects.get(email=self.CLAIMS["email"])
        self.assertEqual(access_request.profile[self.field.guid], "Original value")
        self.send_notifications.assert_not_called()

    def test_onboarding_submit_with_rejected_access_request(self):
        """
        Submit the form with an existing rejected access request should restart the site access request procedure
        """

        # Given
        self._create_access_request(processed=True, reason="Rejected access request")

        # When
        response = self.client.post(self.URL, self.FORM_DATA)

        # Then
        self.assertValidAccessRequestProcedure(response)

    def assertValidOnboardingCompletion(self, response):
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.url, self.EXPECTED_REDIRECT)
        self.assertTrue(User.objects.filter(email=self.CLAIMS["email"]).exists())

        user = User.objects.get(email=self.CLAIMS["email"])
        self.assertEqual(user.name, self.CLAIMS["name"])
        self.assertTrue(
            UserProfileField.objects.filter(
                profile_field=self.field,
                user_profile__user=user,
                value=self.EXPECTED_PROFILE_VALUE,
            ).exists()
        )
        self.assertTrue(
            SiteAccessRequest.objects.filter_status("accepted")
            .filter(email=user.email)
            .exists()
        )
        self.assertFalse(SiteInvitation.objects.filter(email=user.email).exists())

    def assertValidAccessRequestProcedure(self, response):
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, "/onboarding/requested")
        self.assertTrue(
            SiteAccessRequest.objects.filter(email=self.CLAIMS["email"]).exists()
        )

        access_request = SiteAccessRequest.objects.get(email=self.CLAIMS["email"])
        self.assertEqual(access_request.processed, False)
        self.assertEqual(access_request.reason, None)
        self.assertEqual(access_request.profile, self.FORM_DATA)
        self.send_notifications.assert_called()

    def _create_access_request(self, **kwargs):
        kwargs.setdefault("profile", self.FORM_DATA)
        return SiteAccessRequest.objects.create(
            name=self.CLAIMS["name"],
            email=self.CLAIMS["email"],
            claims=self.CLAIMS,
            next=self.EXPECTED_REDIRECT,
            **kwargs,
        )

    def update_session(self, **kwargs):
        session = self.client.session
        for key, value in kwargs.items():
            session[key] = value
        session.save()
