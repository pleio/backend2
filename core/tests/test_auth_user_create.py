from unittest import mock
from unittest.mock import call

import faker
from django.test import tag
from django.utils.crypto import get_random_string

from core.auth import RequestAccessException
from core.models import SiteInvitation
from core.tests.helpers_oidc import BaseOIDCAuthBackendTestCase
from user.models import User


@tag("OIDCAuthBackend")
class TestAuthUserCreateTestCase(BaseOIDCAuthBackendTestCase):
    def setUp(self):
        # Given.
        super().setUp()

        self.backend.request = self.request
        self.claims = {
            "name": faker.Faker().name(),
            "email": faker.Faker().email(),
        }

    def assertValidHandleCreateUser(
        self, result, mocked_create_user, mocked_apply_claims
    ):
        mocked_user = mocked_create_user.return_value
        self.assertEqual(result, mocked_create_user.return_value)
        self.assertTrue(mocked_create_user.called)
        self.assertDictEqual(
            mocked_create_user.call_args.kwargs,
            {
                "email": self.claims["email"],
                "name": self.claims["name"],
            },
        )
        self.assertTrue(mocked_apply_claims.called)
        self.assertEqual(
            mocked_apply_claims.mock_calls, [call(mocked_user, self.claims)]
        )

    @mock.patch("user.utils.claims.apply_claims")
    @mock.patch("user.models.UserManager.create_user")
    @mock.patch("core.auth.OIDCAuthBackend.requires_approval")
    def test_create_user(
        self, mocked_requires_approval, mocked_create_user, mocked_apply_claims
    ):
        mocked_requires_approval.return_value = False
        mocked_create_user.return_value = mock.MagicMock(spec=User)

        # When.
        result = self.backend.create_user(self.claims)

        # Then.
        self.assertValidHandleCreateUser(
            result, mocked_create_user, mocked_apply_claims
        )

    @mock.patch("user.utils.claims.apply_claims")
    @mock.patch("user.models.UserManager.create_user")
    @mock.patch("core.auth.OIDCAuthBackend.requires_approval")
    def test_without_approval_with_code(
        self, mocked_requires_approval, mocked_create_user, mocked_apply_claims
    ):
        mocked_requires_approval.return_value = False
        mocked_create_user.return_value = mock.MagicMock(spec=User)
        self.request.session["invitecode"] = get_random_string(12)

        # When.
        result = self.backend.create_user(self.claims)

        # Then.
        self.assertValidHandleCreateUser(
            result, mocked_create_user, mocked_apply_claims
        )

    @mock.patch("user.utils.claims.apply_claims")
    @mock.patch("user.models.UserManager.create_user")
    @mock.patch("core.auth.OIDCAuthBackend.requires_approval")
    def test_with_approval(
        self, mocked_requires_approval, mocked_create_user, mocked_apply_claims
    ):
        mocked_requires_approval.return_value = True
        invitation = SiteInvitation.objects.create(code=get_random_string(12))
        mocked_create_user.return_value = mock.MagicMock(spec=User)
        self.request.session["invitecode"] = invitation.code

        result = self.backend.create_user(self.claims)
        self.assertFalse(SiteInvitation.objects.filter(id=invitation.id).exists())

        self.assertValidHandleCreateUser(
            result, mocked_create_user, mocked_apply_claims
        )

    @mock.patch("core.auth.OIDCAuthBackend.requires_approval")
    def test_with_nonexistent_invitecode(self, mocked_requires_approval):
        mocked_requires_approval.return_value = True
        self.request.session["invitecode"] = get_random_string(12)

        with self.assertRaises(RequestAccessException):
            self.backend.create_user(self.claims)

    @mock.patch("core.auth.OIDCAuthBackend.requires_approval")
    def test_without_invitecode(self, mocked_requires_approval):
        mocked_requires_approval.return_value = True

        with self.assertRaises(RequestAccessException):
            self.backend.create_user(self.claims)

        self.assertDictEqual(self.request.session["onboarding_claims"], self.claims)
