from core.models import Entity
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class Wrapper:
    class TestDeleteEntities(PleioTenantTestCase):
        def build_owner(self, **kwargs):
            return UserFactory(**kwargs)

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def prepare_test(self):
            self.entities = [
                self.build_entity(owner=self.user),
                self.build_entity(owner=self.user),
                self.build_entity(owner=self.user),
                self.build_entity(owner=self.user),
            ]
            self.all_entity_ids = [entity.id for entity in self.entities]
            self.delete_entity_ids = [self.entities[0].id, self.entities[1].id]
            self.expected_entity_ids = [
                id for id in self.all_entity_ids if id not in self.delete_entity_ids
            ]

        def ids_in_database(self):
            return Entity.objects.filter(id__in=self.all_entity_ids).values_list(
                "id", flat=True
            )

        def setUp(self):
            super().setUp()

            self.user = self.build_owner()
            self.all_entity_ids = []
            self.delete_entity_ids = []
            self.expected_entity_ids = []

            self.prepare_test()

            self.mutation = """
                mutation deleteEntities($input: deleteEntitiesInput!) {
                    deleteEntities(input: $input) {
                        success
                    }
                }
            """
            self.variables = {
                "input": {
                    "guids": [str(id) for id in self.delete_entity_ids],
                }
            }

        def test_delete_entities(self):
            self.graphql_client.force_login(self.user)
            self.assertEqual({*self.ids_in_database()}, {*self.all_entity_ids})

            self.graphql_client.post(self.mutation, self.variables)

            self.assertEqual({*self.ids_in_database()}, {*self.expected_entity_ids})
