from django.utils import timezone
from django.utils.timezone import timedelta

from core.models import Entity
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class Wrapper:
    class TestArchiveNestedEntities(PleioTenantTestCase):
        def create_owner(self, **kwargs):
            return UserFactory(**kwargs)

        def create_entity(self, **kwargs):
            raise NotImplementedError()

        def setUp(self):
            super().setUp()
            self.owner = self.create_owner(email="owner@example.com")

            self.grandparent = self.create_entity(owner=self.owner)
            self.parent = self.create_entity(owner=self.owner, parent=self.grandparent)
            self.brother = self.create_entity(owner=self.owner, parent=self.parent)
            self.sister = self.create_entity(owner=self.owner, parent=self.parent)

        def send_toggle_archive(self, guid):
            mutation = """
            mutation ToggleArchive($guid: String!) {
                toggleEntityArchived(guid: $guid) { success }
            }
            """
            return self.graphql_client.post(mutation, variables={"guid": guid})

        def send_archive_entities(self, guids):
            query = """
                mutation ArchiveEntities($input: archiveEntitiesInput!) {
                    archiveEntities(input: $input) { edges { guid } }
                }
            """
            variables = {"input": {"guids": guids}}
            return self.graphql_client.post(query, variables=variables)

        def send_update_entity(self, input_data):
            query = """
                mutation UpdateEntity($input: editEntityInput!) {
                    editEntity(input: $input) {
                        entity { guid }
                    }
                }
            """
            return self.graphql_client.post(query, variables={"input": input_data})

        def assertIsArchived(self, entity):
            entity.refresh_from_db()
            self.assertTrue(entity.is_archived)

        def assertIsNotArchived(self, entity):
            entity.refresh_from_db()
            self.assertFalse(entity.is_archived)

        def test_archive_pages(self):
            self.graphql_client.force_login(self.owner)
            self.send_archive_entities([self.grandparent.guid])

            self.assertIsArchived(self.grandparent)
            self.assertIsArchived(self.parent)
            self.assertIsArchived(self.brother)
            self.assertIsArchived(self.sister)

        def test_toggle_archive_page(self):
            """
            Toggling a page will archive it and all its children
            """

            self.graphql_client.force_login(self.owner)
            self.send_toggle_archive(self.grandparent.guid)

            self.assertIsArchived(self.grandparent)
            self.assertIsArchived(self.parent)
            self.assertIsArchived(self.brother)
            self.assertIsArchived(self.sister)

        def test_detoggle_archive_page(self):
            """
            De-toggle must bring back the page and all its children
            """

            self.graphql_client.force_login(self.owner)
            self.send_toggle_archive(self.grandparent.guid)
            self.send_toggle_archive(self.grandparent.guid)

            self.assertIsNotArchived(self.grandparent)
            self.assertIsNotArchived(self.parent)
            self.assertIsNotArchived(self.brother)
            self.assertIsNotArchived(self.sister)

        def test_toggle_subtree(self):
            """
            Toggle a child will not toggle parents or siblings
            """
            self.graphql_client.force_login(self.owner)
            self.send_toggle_archive(self.brother.guid)

            self.assertIsNotArchived(self.grandparent)
            self.assertIsNotArchived(self.parent)
            self.assertIsArchived(self.brother)
            self.assertIsNotArchived(self.sister)

        def test_toggle_subtree_with_siblings(self):
            """
            Test that de-toggling the grandparent will not de-toggle the earlier toggled brother
            """
            self.graphql_client.force_login(self.owner)
            self.send_toggle_archive(self.brother.guid)
            self.send_toggle_archive(self.grandparent.guid)
            self.send_toggle_archive(self.grandparent.guid)

            self.assertIsNotArchived(self.grandparent)
            self.assertIsNotArchived(self.parent)
            self.assertIsArchived(self.brother)
            self.assertIsNotArchived(self.sister)

        def test_not_set_archive_date_on_children(self):
            """
            Test that setting an archive date on a parent will not set it on the children
            """
            self.graphql_client.force_login(self.owner)
            self.send_update_entity(
                {
                    "guid": self.grandparent.guid,
                    "scheduleArchiveEntity": (
                        timezone.now() + timedelta(days=1)
                    ).isoformat(),
                }
            )
            self.grandparent.refresh_from_db()

            self.assertIsNotNone(self.grandparent.schedule_archive_after)
            self.assertTrue(self.grandparent.schedule_archive_after > timezone.now())

            self.assertIsNone(self.parent.schedule_archive_after)
            self.assertIsNone(self.brother.schedule_archive_after)
            self.assertIsNone(self.sister.schedule_archive_after)

        def test_archive_children_directly_too_when_date_is_due(self):
            """
            Test that when setting an archive date in the (near) past on a parent
              will archive the children as well.
            """
            self.graphql_client.force_login(self.owner)
            self.send_update_entity(
                {
                    "guid": self.grandparent.guid,
                    "scheduleArchiveEntity": (
                        timezone.now() - timedelta(days=1)
                    ).isoformat(),
                }
            )
            self.grandparent.refresh_from_db()

            self.assertIsNotNone(self.grandparent.schedule_archive_after)
            self.assertTrue(self.grandparent.is_archived)
            self.assertTrue(self.grandparent.schedule_archive_after < timezone.now())

            self.assertIsArchived(self.parent)
            self.assertEqual(
                self.parent.schedule_archive_after,
                self.grandparent.schedule_archive_after,
            )
            self.assertIsArchived(self.brother)
            self.assertEqual(
                self.brother.schedule_archive_after,
                self.grandparent.schedule_archive_after,
            )
            self.assertIsArchived(self.sister)
            self.assertEqual(
                self.sister.schedule_archive_after,
                self.grandparent.schedule_archive_after,
            )

    class TestArchiveEntities(PleioTenantTestCase):
        def create_owner(self, **kwargs):
            return UserFactory(**kwargs)

        def create_entity(self, **kwargs):
            raise NotImplementedError()

        def setUp(self):
            super().setUp()
            self.owner = self.create_owner(email="owner@example.com")

            self.archive_guids = []
            self.non_archived_guids = []

            self.prepare_content()

            self.mutation = """
            mutation ($input: archiveEntitiesInput!) {
                archiveEntities(input: $input) {
                    edges { guid }
                }
            }
            """

            self.variables = {"input": {"guids": self.archive_guids}}

        def prepare_content(self):
            self.entities = [
                self.create_entity(owner=self.owner),
                self.create_entity(owner=self.owner),
                self.create_entity(owner=self.owner),
                self.create_entity(owner=self.owner),
            ]

            entity_guids = [entity.guid for entity in self.entities]
            self.archive_guids = entity_guids[:2]
            self.non_archived_guids = entity_guids[2:]

        def test_archive_entities(self):
            self.graphql_client.force_login(self.owner)
            response = self.graphql_client.post(self.mutation, variables=self.variables)

            self.assertEqual(
                self.archive_guids,
                [
                    entity["guid"]
                    for entity in response["data"]["archiveEntities"]["edges"]
                ],
            )
            non_archived = Entity.objects.filter(
                id__in=self.non_archived_guids, is_archived=False
            )
            self.assertEqual(non_archived.count(), len(self.non_archived_guids))
