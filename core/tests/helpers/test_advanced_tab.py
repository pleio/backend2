from django.test import tag

from core.factories import GroupFactory
from user.factories import AdminFactory, UserFactory

from . import PleioTenantTestCase


class TestAdvancedTab:
    @tag("TestAdvancedTabTestCase")
    class TestCase(PleioTenantTestCase):
        TYPE = None

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def build_owner(self, **kwargs):
            return UserFactory(**kwargs)

        def setUp(self):
            super().setUp()

            self.admin = AdminFactory(email="administrator@example.com")
            self.group_owner = UserFactory(email="group_owner@example.com")
            self.entity_owner = self.build_owner(email="entity_owner@example.com")
            self.another_user = UserFactory(email="another_user@example.com")
            self.group_admin = UserFactory(email="group_admin@example.com")

            self.group = GroupFactory(owner=self.group_owner, name="Test Group")
            self.group.join(self.entity_owner)
            self.group.join(self.another_user)
            self.group.join(self.group_admin, "admin")

            self.another_group = GroupFactory(owner=self.group_owner)

            self.entity = self.build_entity(
                owner=self.entity_owner, group=self.group, title="An entity"
            )

            self.ORIGINAL_CREATED_TIME = self.entity.created_at.isoformat()
            self.UPDATED_CREATED_TIME = "2018-12-10T22:00:00+00:00"
            self.assertNotEquals(self.ORIGINAL_CREATED_TIME, self.UPDATED_CREATED_TIME)

            self.mutation = """
            mutation EditEntity($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                        ... on %(type)s {
                            title
                            timeCreated
                            group { guid }
                            owner { guid }
                            canEdit
                            canEditAdvanced
                            canEditGroup
                            canArchiveAndDelete
                        }
                    }
                }
            }
            """ % {"type": self.TYPE}

            self.variables = {
                "input": {
                    "guid": self.entity.guid,
                    "title": "My first update",
                    "timeCreated": self.UPDATED_CREATED_TIME,
                    "groupGuid": self.another_group.guid,
                    "ownerGuid": self.another_user.guid,
                }
            }

        def test_update_entity_not_logged_in(self):
            with self.assertGraphQlError("not_logged_in"):
                self.graphql_client.post(self.mutation, self.variables)

        def test_update_entity_as_owner(self):
            self.graphql_client.force_login(self.entity_owner)
            result = self.graphql_client.post(self.mutation, self.variables)
            entity = result["data"]["editEntity"]["entity"]

            self.assertEqual(
                entity,
                {
                    "canEdit": True,
                    "canEditAdvanced": False,
                    "canEditGroup": False,
                    "canArchiveAndDelete": True,
                    "timeCreated": self.ORIGINAL_CREATED_TIME,
                    "group": {"guid": self.group.guid},
                    "owner": {"guid": self.entity_owner.guid},
                    "title": "My first update",
                },
            )

        def test_update_entity_as_group_admin(self):
            self.graphql_client.force_login(self.group_admin)
            result = self.graphql_client.post(self.mutation, self.variables)
            entity = result["data"]["editEntity"]["entity"]

            self.assertEqual(
                entity,
                {
                    "canEdit": True,
                    "canEditAdvanced": True,
                    "canEditGroup": False,
                    "canArchiveAndDelete": True,
                    "timeCreated": self.UPDATED_CREATED_TIME,
                    "group": {"guid": self.group.guid},
                    "owner": {"guid": self.another_user.guid},
                    "title": "My first update",
                },
            )

        def test_update_entity_as_admin(self):
            self.graphql_client.force_login(self.admin)
            result = self.graphql_client.post(self.mutation, self.variables)
            entity = result["data"]["editEntity"]["entity"]

            self.assertEqual(
                entity,
                {
                    "canEdit": True,
                    "canEditAdvanced": True,
                    "canEditGroup": True,
                    "canArchiveAndDelete": True,
                    "timeCreated": self.UPDATED_CREATED_TIME,
                    "group": {"guid": self.another_group.guid},
                    "owner": {"guid": self.another_user.guid},
                    "title": "My first update",
                },
            )
