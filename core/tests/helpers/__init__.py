import json
import os
import time
from contextlib import contextmanager
from datetime import datetime
from time import sleep
from unittest import mock

from ariadne import graphql_sync
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.db import connection
from django.http import HttpRequest
from django.test import override_settings
from django.utils import timezone, translation
from django.utils.crypto import get_random_string
from elasticsearch_dsl import Search
from mixer.backend.django import mixer
from PIL import Image, UnidentifiedImageError

from backend2.schema import schema
from core.base_config import DEFAULT_SITE_CONFIG
from tenants.helpers import FastTenantTestCase


class PleioTenantTestCase(FastTenantTestCase):
    def setUp(self):
        super().setUp()
        self.use_postgres_unaccent()

        self.graphql_client = GraphQLClient()
        self._cleanup = []
        self._settings_cache = {}
        self._restore_language = None
        self._mock_used = False

        self.mocked_log_warning = mock.patch("logging.Logger.warning").start()
        self.mocked_warn = mock.patch("warnings.warn").start()
        self.mocked_deepl_ctl = mock.patch(
            "core.services.translate_service.DeeplClient.translate"
        )
        self.mocked_deepl = self.mocked_deepl_ctl.start()
        self.mocked_deepl.return_value = "translated"

    def upgrade_user(self, user, roles):
        user.roles = roles
        user.save()
        return user

    def use_mock_file(self, filename):
        return ContentFile(b"file", filename)

    def _cleanup_storage(self, fullpath):
        from django.core.files.storage import default_storage

        dirs, files = default_storage.listdir(fullpath)
        for file in files:
            filepath = f"{fullpath}{file}"
            default_storage.delete(filepath)
        for dir in dirs:
            new_dir = f"{fullpath}{dir}/"
            self._cleanup_storage(new_dir)

    def tearDown(self):
        self._cleanup_storage("")

        for entity in reversed(self._cleanup):
            try:
                entity.delete()
            except (AttributeError, ValueError):
                pass

        for key, value in self._settings_cache.items():
            setattr(settings, key, value)
        cache.clear()

        if self._restore_language:
            translation.activate(self._restore_language)

        super().tearDown()

    def remember(self, *args):
        """Allow cleanup add-hoc objects"""
        if args:
            self._cleanup.append(*args)
            return args if len(args) != 1 else args[0]

    def switch_language(self, language_code):
        assert language_code in ["en", "nl", "de", "fr"], "Language code is restricted."
        if not self._restore_language:
            self._restore_language = translation.get_language()
        self.override_setting(LANGUAGE_CODE=language_code)
        self.override_config(LANGUAGE=language_code)
        translation.activate(language_code)

    def diskfile_factory(self, filename, content="", binary=False):
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w%s" % ("b" if binary else "")) as fh:
            fh.write(content)

    def file_factory(self, filepath, **kwargs):
        from entities.file.models import FileFolder

        filename = os.path.basename(filepath)
        upload = None
        if os.path.exists(filepath):
            with open(filepath, "rb") as fh:
                upload = ContentFile(fh.read(), filename)
        file = mixer.blend(
            FileFolder, type=FileFolder.Types.FILE, upload=upload, **kwargs
        )

        return file

    @staticmethod
    def use_postgres_unaccent():
        with connection.cursor() as cursor:
            cursor.execute("CREATE EXTENSION IF NOT EXISTS unaccent;")

    def override_config(self, **kwargs):
        for key, value in kwargs.items():
            assert key in DEFAULT_SITE_CONFIG, "%s is not a valid key" % key
            cache.set("%s%s" % (self.tenant.schema_name, key), value)

    def override_setting(self, **kwargs):
        for key, value in kwargs.items():
            if key not in self._settings_cache:
                self._settings_cache[key] = getattr(settings, key, None)
            setattr(settings, key, value)

    @staticmethod
    def relative_path(root, path):
        return os.path.join(os.path.dirname(root), *path)

    @staticmethod
    def build_contentfile(path):
        content = open(path, "rb").read()
        return ContentFile(content, os.path.basename(path))

    def update_session(self, **kwargs):
        session = self.client.session
        for key, value in kwargs.items():
            session[key] = value
        session.save()

    @contextmanager
    def assertGraphQlError(self, expected=None, msg=None):
        fail_reason = False
        try:
            yield
            fail_reason = (
                "%s -- Unexpectedly didn't find any errors in graphql result" % msg
            )  # pragma: no cover
        except GraphQlError as e:
            if not e.has_message(expected):  # pragma: no cover
                error = f"Didn't find [{expected}] in {e.messages}"
                fail_reason = "{} -- {}".format(msg, error) if msg else error

        if fail_reason:  # pragma: no cover
            self.fail(fail_reason)

    @contextmanager
    def assertNotRaisesException(self, msg=None):
        try:
            yield
        except Exception:  # pragma: no cover
            if msg:
                self.fail(msg)
            raise

    def _preprocess_date(self, date):
        try:
            date.isoformat()
            return date
        except AttributeError:
            pass
        assert isinstance(date, str), f"{date} should be a string. Is now {type(date)}."
        return datetime.fromisoformat(date)

    def assertDateEqual(self, lhs, rhs, *args, **kwargs):
        lhs = self._preprocess_date(lhs)
        rhs = self._preprocess_date(rhs)
        self.assertEqual(lhs, rhs, *args, **kwargs)

    def assertDateNotEqual(self, lhs, rhs, *args, **kwargs):
        lhs = self._preprocess_date(lhs)
        rhs = self._preprocess_date(rhs)
        self.assertNotEqual(lhs, rhs, *args, **kwargs)

    def assertDictEqual(self, d1, d2, msg=None):
        if isinstance(d1, dict) and isinstance(d2, dict):
            super().assertDictEqual(d1, d2, msg)
        super().assertDictEqual({"data": d1}, {"data": d2}, msg)

    def assertImageWidth(self, fh, width, msg=None):
        image = Image.open(fh)
        self.assertEqual(image.width, width, msg)

    def assertImageHeight(self, fh, height, msg=None):
        image = Image.open(fh)
        self.assertEqual(image.height, height, msg)

    @staticmethod
    def tiptap_paragraph(*paragraphs):
        return json.dumps(
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [
                            {
                                "type": "text",
                                "text": p,
                            }
                        ],
                    }
                    for p in paragraphs
                ],
            }
        )

    def tiptap_attachment(self, *files):
        return json.dumps(
            {
                "type": "doc",
                "content": [
                    {
                        "type": "file",
                        "attrs": {
                            "guid": None,
                            "url": file.attachment_url,
                            "name": file.title,
                            "mimeType": file.mime_type,
                            "size": file.size,
                        },
                    }
                    for file in files
                ],
            }
        )

    def assertExif(self, fp, msg=None):  # pragma: no cover
        image = None
        try:
            image = Image.open(fp)
        except UnidentifiedImageError:
            pass
        assert image and image.getexif(), msg or "Unexpectedly no exif data found."

    def assertNotExif(self, fp, msg=None):
        try:
            image = Image.open(fp)
            assert not image.getexif(), msg or "Unexpectedly found exif data."
        except UnidentifiedImageError:
            pass

    def clone_tenant(self, schema_name, domain):
        from tenants.models import Client, Domain

        pass


class GraphQlError(Exception):
    def __init__(self, data):
        self.data = data

    def has_message(self, expected=None):
        if expected:
            return expected in self.messages
        return True

    @property
    def messages(self):
        return [e["message"] for e in self.data["errors"]]


class GraphQLClient:
    result = None
    request = None

    def __init__(self):
        self.reset()

    def reset(self):
        self.force_login(AnonymousUser())

    def force_login(self, user):
        self.request = HttpRequest()
        self.request.user = user
        self.request.COOKIES["sessionid"] = get_random_string(32)
        self.request.META["REMOTE_ADDR"] = "127.0.0.1"

    def post(self, query, variables=None):
        success, self.result = graphql_sync(
            schema,
            {"query": query, "variables": variables or {}},
            context_value={"request": self.request},
        )
        if self.result.get("errors"):
            raise GraphQlError(self.result)

        return self.result


class ElasticsearchTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.initialize_index()
        # use self.populate_index() in tests to populate the index after creating data

    @override_settings(ENV="unit-test")
    def initialize_index(self):
        from core.tasks.elasticsearch_tasks import elasticsearch_delete_data_for_tenant

        # elasticsearch needs time to settle changes before some delete?
        time.sleep(0.100)
        elasticsearch_delete_data_for_tenant(self.tenant.schema_name, None)
        time.sleep(0.100)

    @override_settings(ENV="unit-test")
    def populate_index(self):
        from core.tasks.elasticsearch_tasks import elasticsearch_index_data_for_tenant

        elasticsearch_index_data_for_tenant(self.tenant.schema_name, None)
        time.sleep(0.100)

    def wait_for_records(self, index, count, timeout):
        start = timezone.now()
        while (timezone.now() - start).total_seconds() < timeout:
            query = Search(index=index).query()
            query = query.filter("term", tenant_name=self.tenant.schema_name)
            if query.count() >= count:
                return
            sleep(timeout / 10)
        msg = f"Did not find {count} records in {index}"
        raise TimeoutError(msg)


@contextmanager
def suppress_stdout():
    from contextlib import redirect_stderr, redirect_stdout
    from os import devnull

    with mock.patch("warnings.warn"):
        with open(devnull, "w") as fnull:
            with redirect_stderr(fnull) as err, redirect_stdout(fnull) as out:
                yield (err, out)


def get_system_root():
    return os.path.abspath(os.path.sep)


@contextmanager
def override_config(**kwargs):
    for config in kwargs.keys():
        assert config in DEFAULT_SITE_CONFIG, "%s is not a valid key" % config

    with mock.patch("core.base_config.cache.get") as mocked_cache:

        def mock_cache_get(key, default=None):
            for config, value in kwargs.items():
                if key.endswith(config):
                    return value
            return default

        mocked_cache.side_effect = mock_cache_get

        yield
