from unittest.mock import patch

from django.test import tag

from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class Wrapper:
    @tag("translation")
    class BaseTestCase(PleioTenantTestCase):
        ENTITY_TYPE = None

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def entity_properties(self, field):
            properties = {
                "owner": self.owner,
                "input_language": "nl",
            }
            fields = {
                "title": "title",
                "rich_description": self.tiptap_paragraph("Rich description"),
                "abstract": "<h2>Abstract</h2>",
                "sub_title": "Subtitle",
            }
            properties[field] = fields[field]
            return properties

        def submit_query(self, field_name):
            self.query = self.query % {
                "type": self.ENTITY_TYPE,
                "field_name": field_name,
            }
            self.graphql_client.force_login(self.owner)
            return self.graphql_client.post(self.query, {"guid": self.entity.guid})

        def build_owner(self, **kwargs):
            return UserFactory(**kwargs)

        def setUp(self):
            super().setUp()

            self.owner = self.build_owner()
            self.override_config(
                EXTRA_LANGUAGES=["nl", "fr", "de", "en"],
                CONTENT_TRANSLATION=True,
            )
            self.translate_string = patch(
                "core.utils.translation.translator.Translator.translate"
            ).start()
            self.translate_string.return_value = "translated"
            self.should_use_foreign_language = patch(
                "core.resolvers.shared.should_use_foreign_language"
            ).start()
            self.should_use_foreign_language.return_value = True

            self.query = """
            query Query($guid: String) {
                entity(guid: $guid) {
                    guid
                    ... on %(type)s {
                        field: %(field_name)s
                    }
                }
            }
            """

    class TestTitleFieldTranslationTestCase(BaseTestCase):
        def test_local_title(self):
            self.entity = self.build_entity(**self.entity_properties("title"))
            response = self.submit_query("localTitle")

            self.assertTrue(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], "translated")

        def test_unchanged_title(self):
            self.entity = self.build_entity(**self.entity_properties("title"))
            self.should_use_foreign_language.return_value = False
            response = self.submit_query("localTitle")

            self.assertFalse(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], None)

    class TestSubtitleFieldTranslationTestCase(BaseTestCase):
        def test_local_subtitle(self):
            self.entity = self.build_entity(**self.entity_properties("sub_title"))
            response = self.submit_query("localSubtitle")

            self.assertTrue(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], "translated")

        def test_unchanged_subtitle(self):
            self.entity = self.build_entity(**self.entity_properties("sub_title"))
            self.should_use_foreign_language.return_value = False
            response = self.submit_query("localSubtitle")

            self.assertFalse(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], None)

    class TestRichDescriptionFieldTranslationTestCase(BaseTestCase):
        @patch("core.utils.translation.translator.XMLTranslator.translate")
        def test_local_rich_description(self, xml_translate):
            self.entity = self.build_entity(
                **self.entity_properties("rich_description")
            )
            xml_translate.return_value = """<root><doc><par name="content.0.content.0.text.">translated</par></doc></root>"""
            response = self.submit_query("localRichDescription")

            self.assertTrue(xml_translate.called)
            self.assertEqual(
                response["data"]["entity"]["field"], self.tiptap_paragraph("translated")
            )

        def test_unchanged_rich_description(self):
            self.entity = self.build_entity(
                **self.entity_properties("rich_description")
            )
            self.should_use_foreign_language.return_value = False
            response = self.submit_query("localRichDescription")

            self.assertFalse(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], None)

    class TestAbastractFieldTranslationTestCase(BaseTestCase):
        def test_local_abstract(self):
            self.entity = self.build_entity(**self.entity_properties("abstract"))
            response = self.submit_query("localAbstract")

            self.assertTrue(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], "translated")

        def test_unchanged_abstract(self):
            self.entity = self.build_entity(**self.entity_properties("abstract"))
            self.should_use_foreign_language.return_value = False
            response = self.submit_query("localAbstract")

            self.assertFalse(self.translate_string.called)
            self.assertEqual(response["data"]["entity"]["field"], None)

    class TestDescriptionFieldTranslationTestCase(BaseTestCase):
        EXPECTED_ORIGINAL_DESCRIPTION = "Rich description\n\n"

        @patch("core.utils.translation.translatables.DescriptionTranslatable.translate")
        @patch("core.resolvers.shared.resolve_entity_description")
        def test_local_description(self, resolve_description, translate):
            self.entity = self.build_entity(
                **self.entity_properties("rich_description")
            )
            resolve_description.return_value = "description"
            translate.return_value = "translated"
            response = self.submit_query("localDescription, description")

            self.assertTrue(translate.called)
            self.assertEqual(response["data"]["entity"]["field"], "translated")
            self.assertEqual(
                response["data"]["entity"]["description"],
                self.EXPECTED_ORIGINAL_DESCRIPTION,
            )
