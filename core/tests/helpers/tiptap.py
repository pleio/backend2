import json


class TiptapWrapperBase:
    def as_dict(self):
        raise NotImplementedError()

    def as_json(self):
        return json.dumps(self.as_dict())


class RichDescription(TiptapWrapperBase):
    def __init__(self, *args):
        self.args = args

    def as_dict(self):
        return {"type": "doc", "content": [arg.as_dict() for arg in self.args]}


class Text(TiptapWrapperBase):
    def __init__(self, text, **kwargs):
        self.text = text
        self.kwargs = kwargs

    def as_dict(self):
        return {
            "type": "text",
            "text": self.text,
            "attrs": self.kwargs,
        }


class File(TiptapWrapperBase):
    def __init__(self, file, **kwargs):
        self.file = file
        self.kwargs = kwargs

    def as_dict(self):
        return {
            "type": "file",
            "attrs": {
                "guid": None,
                "url": self.file.attachment_url,
                "name": self.file.title,
                "mimeType": self.file.mime_type,
                "size": self.file.size,
                **self.kwargs,
            },
        }


class Video(TiptapWrapperBase):
    def __init__(self, guid, platform, title=None, **kwargs):
        self.guid = guid
        self.platform = platform
        self.title = title
        self.kwargs = kwargs

    def as_dict(self):
        return {
            "type": "video",
            "attrs": {
                "guid": self.guid,
                "platform": self.platform,
                "title": self.title,
                **self.kwargs,
            },
        }


class Picture(TiptapWrapperBase):
    def __init__(
        self, src, size="large", align="center", alt=None, caption=None, **kwargs
    ):
        self.src = src
        self.size = size
        self.align = align
        self.alt = alt
        self.caption = caption
        self.kwargs = kwargs

    def as_dict(self):
        return {
            "type": "image",
            "attrs": {
                "src": self.src,
                "size": self.size,
                "align": self.align,
                "alt": self.alt,
                "caption": self.caption,
                **self.kwargs,
            },
        }
