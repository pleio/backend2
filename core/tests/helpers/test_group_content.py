from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class Template:
    class TestGroupContentTestCase(PleioTenantTestCase):
        def build_owner(self):
            raise NotImplementedError()

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def setUp(self):
            super().setUp()

            self.group_owner = UserFactory()
            self.group = GroupFactory(owner=self.group_owner)

            self.owner = self.build_owner()
            self.group.join(self.owner)

            self.entity = self.build_entity(owner=self.owner, group=self.group)
            self.global_entity = self.build_entity(owner=self.owner)

        def test_query_entities_without_group_filter(self):
            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(
                """
            query QueryContent {
                entities {
                    edges { guid }
                }
            }
            """,
                {},
            )

            guids = {e["guid"] for e in result["data"]["entities"]["edges"]}

            self.assertEqual(
                guids,
                {
                    self.entity.guid,
                    self.global_entity.guid,
                },
            )

        def test_query_entities_with_group_filter(self):
            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(
                """
            query QueryContent($groupGuid: String) {
                entities(containerGuid: $groupGuid) {
                    edges { guid }
                }
            }
            """,
                {"groupGuid": self.group.guid},
            )

            guids = {e["guid"] for e in result["data"]["entities"]["edges"]}

            self.assertEqual(
                guids,
                {
                    self.entity.guid,
                },
            )

        def test_query_activities_without_group(self):
            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(
                """
            query QueryContent {
                activities {
                    edges { entity { guid } }
                }
            }
            """,
                {},
            )

            guids = {e["entity"]["guid"] for e in result["data"]["activities"]["edges"]}

            self.assertEqual(
                guids,
                {
                    self.entity.guid,
                    self.global_entity.guid,
                },
            )

        def test_query_activities_with_group_filter(self):
            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(
                """
            query QueryContent($groupGuid: String) {
                activities(containerGuid: $groupGuid) {
                    edges { entity { guid } }
                }
            }
            """,
                {"groupGuid": self.group.guid},
            )

            guids = {e["entity"]["guid"] for e in result["data"]["activities"]["edges"]}

            self.assertEqual(
                guids,
                {
                    self.entity.guid,
                },
            )
