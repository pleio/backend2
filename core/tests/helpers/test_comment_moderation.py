from core.models import Comment
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class CommentModerationTestCases:
    class TestAddCommentForModerationTestCase(PleioTenantTestCase):
        type_to_string = None

        def entity_factory(self, **kwargs):
            raise NotImplementedError()

        def setUp(self):
            super().setUp()
            self.override_config(
                REQUIRE_COMMENT_MODERATION_FOR=[self.type_to_string],
                COMMENT_MODERATION_ENABLED=True,
            )
            self.respondent = UserFactory()
            self.container = self.entity_factory(owner=UserFactory())

        def test_create_comment(self):
            self.override_config(REQUIRE_COMMENT_MODERATION_FOR=[])
            comment = Comment.objects.create(
                container=self.container,
                owner=self.respondent,
            )
            self.container.refresh_from_db()

            self.assertIsNone(comment.comment_moderation_request)
            self.assertFalse(hasattr(comment.container, "comment_moderation_request"))

        def test_create_comment_for_moderation(self):
            comment = Comment.objects.create(
                container=self.container,
                owner=self.respondent,
            )
            self.container.refresh_from_db()

            self.assertIsNotNone(comment.comment_moderation_request)
            self.assertTrue(hasattr(comment.container, "comment_moderation_request"))

        def test_create_comment_for_moderation_by_admin(self):
            comment = Comment.objects.create(
                container=self.container, owner=AdminFactory()
            )
            self.container.refresh_from_db()

            self.assertIsNone(comment.comment_moderation_request)
            self.assertFalse(hasattr(comment.container, "comment_moderation_request"))

        def test_create_two_comments_in_one_container(self):
            c1 = Comment.objects.create(container=self.container, owner=self.respondent)
            c2 = Comment.objects.create(container=self.container, owner=self.respondent)
            self.container.refresh_from_db()

            self.assertIsNotNone(c1.comment_moderation_request)
            self.assertIsNotNone(c2.comment_moderation_request)
            self.assertTrue(hasattr(self.container, "comment_moderation_request"))
            self.assertEqual(
                c1.comment_moderation_request.guid,
                c2.comment_moderation_request.guid,
            )
