from django.test import tag
from django.utils import timezone

from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, EditorFactory, UserFactory


class PublishRequestTestCases:
    @tag("TestEditEntityWithPublishRequest")
    class TestEditEntityWithPublishRequest(PleioTenantTestCase):
        TYPE_TO_STRING = None
        EDITORS_CAN_ALWAYS_EDIT = False

        def setUp(self):
            super().setUp()
            self.override_config(CONTENT_MODERATION_ENABLED=True)
            self.override_config(REQUIRE_CONTENT_MODERATION_FOR=[self.TYPE_TO_STRING])

            self.owner = UserFactory()
            self.admin = AdminFactory()
            self.another_user = UserFactory()
            self.editor = EditorFactory()
            self.entity = self.entity_factory(owner=self.owner, published=None)

        def entity_factory(self, **kwargs):
            raise NotImplementedError()

        def assertDraftArchiveBehaviour(self, entity):
            self.assertTrue(entity.can_archive(self.owner))
            self.assertTrue(entity.can_archive(self.admin))
            self.assertFalse(entity.can_archive(self.another_user))

        def test_edit_draft_entity_without_publish_request(self):
            """Editing a draft entity without a publish_request is possible"""
            self.assertTrue(self.entity.can_write(self.owner))
            self.assertTrue(self.entity.can_write(self.admin))
            self.assertFalse(self.entity.can_write(self.another_user))
            self.assertDraftArchiveBehaviour(self.entity)

            # If editors can always edit, they should be able to edit the entity
            if self.EDITORS_CAN_ALWAYS_EDIT:
                self.assertTrue(self.entity.can_write(self.editor))
            else:
                self.assertFalse(self.entity.can_write(self.editor))

        def test_edit_draft_entity_with_publish_request(self):
            """Editing a draft entity with a publish_request is not possible"""
            self.entity.publish_requests.create(time_published=timezone.now())

            self.assertFalse(self.entity.can_write(self.owner))
            self.assertTrue(self.entity.can_write(self.admin))
            self.assertTrue(self.entity.can_write(self.editor))
            self.assertFalse(self.entity.can_write(self.another_user))

            self.assertDraftArchiveBehaviour(self.entity)

        def test_edit_published_entity_with_publish_request(self):
            """Editing a published entity with a publish_request is possible"""
            pr = self.entity.publish_requests.create(time_published=timezone.now())
            pr.confirm_request()

            self.assertFalse(self.entity.can_write(self.owner))
            self.assertTrue(self.entity.can_write(self.admin))
            self.assertTrue(self.entity.can_write(self.editor))
            self.assertFalse(self.entity.can_write(self.another_user))

            self.assertDraftArchiveBehaviour(self.entity)
