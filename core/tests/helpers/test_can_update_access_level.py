from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class Wrapper:
    class TestCanUpdateAccessLevelBaseClass(PleioTenantTestCase):
        output_type = None
        input_type = None
        update_call = None
        content_key = "entity"

        def build_entity(self, **kwargs):
            raise NotImplementedError()

        def build_owner(self, **kwargs):
            return UserFactory(**kwargs)

        def acting_user(self):
            return self.owner

        def setUp(self):
            super().setUp()
            # Full range access tests.
            self.override_config(
                IS_CLOSED=False,
                HIDE_ACCESS_LEVEL_SELECT=True,
            )

            self.owner = self.build_owner(
                name="Acting user", email="acting-user@example.com"
            )
            self.entity = self.build_entity(owner=self.owner)

            self.mutation = """
            mutation UpdateAccessId($input: %(input_type)s!) {
                updateResult: %(update_call)s(input: $input) {
                    entity: %(content_key)s {
                        ... on %(output_type)s {
                            accessId
                            canEdit
                        }
                    }
                }
            }
            """ % {
                "input_type": self.input_type,
                "update_call": self.update_call,
                "output_type": self.output_type,
                "content_key": self.content_key,
            }
            self.query_variables = {"guid": self.entity.guid}
            self.mutation_variables = {
                "input": {"guid": self.entity.guid, "accessId": 0}
            }

        expected_access_id = None

        def test_mutation(self):
            self.graphql_client.force_login(self.acting_user())
            result = self.graphql_client.post(self.mutation, self.mutation_variables)
            entity = result["data"]["updateResult"]["entity"]

            self.assertEqual(entity["canEdit"], True)
            self.assertEqual(entity["accessId"], self.expected_access_id)
