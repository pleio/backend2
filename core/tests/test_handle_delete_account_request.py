from unittest import mock

from mixer.backend.django import mixer

from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory
from user.models import User


class HandleDeleteAccountRequestTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = mixer.blend(User)
        self.admin = mixer.blend(User, roles=[USER_ROLES.ADMIN])
        self.delete_user = mixer.blend(User, is_delete_requested=True)

        self.mocked_mail = mock.patch(
            "core.resolvers.mutations.mutation_handle_delete_account_request.schedule_user_delete_complete_mail"
        ).start()

        self.mutation = """
            mutation handleDeleteAccountRequest($input: handleDeleteAccountRequestInput!) {
                handleDeleteAccountRequest(input: $input) {
                    success
                }
            }
        """
        self.variables = {"input": {"guid": self.delete_user.guid, "accept": True}}

    def assertDeleteAccepted(self, user):
        user.refresh_from_db()
        self.assertEqual(user.name, "Verwijderde gebruiker")
        self.assertEqual(user.is_delete_requested, False)
        self.assertEqual(user.is_active, False)

    def assertDeleteDenied(self, user):
        user.refresh_from_db()
        self.assertEqual(user.is_delete_requested, False)
        self.assertEqual(user.is_active, True)

    def test_handle_delete_account_request_by_admin(self):
        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertDeleteAccepted(self.delete_user)
        self.assertEqual(self.mocked_mail.call_count, 1)

    def test_handle_delete_account_request_deny_by_admin(self):
        self.variables["input"]["accept"] = False

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertDeleteDenied(self.delete_user)
        self.assertEqual(self.mocked_mail.call_count, 0)

    def test_handle_delete_account_request_by_user_manager(self):
        user_admin = UserFactory(roles=[USER_ROLES.USER_ADMIN])

        self.graphql_client.force_login(user_admin)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertDeleteAccepted(self.delete_user)
        self.assertEqual(self.mocked_mail.call_count, 1)

    def test_handle_delete_account_request_deny_by_user_manager(self):
        self.variables["input"]["accept"] = False
        user_admin = UserFactory(roles=[USER_ROLES.USER_ADMIN])

        self.graphql_client.force_login(user_admin)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertDeleteDenied(self.delete_user)
        self.assertEqual(self.mocked_mail.call_count, 0)

    def test_handle_delete_account_request_by_user(self):
        variables = {"input": {"guid": self.delete_user.guid, "accept": True}}

        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, variables)

    def test_handle_delete_account_request_by_anonymous(self):
        variables = {"input": {"guid": self.delete_user.guid, "accept": True}}

        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, variables)
