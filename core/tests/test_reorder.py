from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import TextPageFactory
from entities.cms.models import Page
from entities.wiki.factories import WikiFactory
from entities.wiki.models import Wiki
from user.factories import AdminFactory, EditorFactory, UserFactory


class ReorderTextPagesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = EditorFactory(email="user@example.com")
        self.admin = AdminFactory(email="admin@example.com")

        self.page1 = TextPageFactory(owner=self.user)
        self.page2 = TextPageFactory(owner=self.user, position=0, parent=self.page1)
        self.page3 = TextPageFactory(owner=self.user, position=1, parent=self.page1)
        self.page4 = TextPageFactory(owner=self.user, position=2, parent=self.page1)
        self.page5 = TextPageFactory(owner=self.user, position=3, parent=self.page1)

        self.page6 = TextPageFactory(owner=self.user)
        # when no position is set order is created_at (OLD -> NEW)
        self.page7 = TextPageFactory(owner=self.user, parent=self.page6)
        self.page8 = TextPageFactory(owner=self.user, parent=self.page6)
        self.page9 = TextPageFactory(owner=self.user, parent=self.page6)

        self.mutation = """
            mutation SubNavReorder($input: reorderInput!) {
                reorder(input: $input) {
                    container {
                        guid
                        ...PageFragment
                    }
                }
            }
            fragment PageFragment on Page {
                canEdit
                children {
                    guid
                    title
                }
            }
        """

    def test_reorder_page_move_up_position_by_admin(self):
        variables = {
            "input": {
                "guid": self.page2.guid,
                "sourcePosition": 0,
                "destinationPosition": 2,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Page.objects.get(id=self.page2.id).position, 2)
        self.assertEqual(Page.objects.get(id=self.page3.id).position, 0)
        self.assertEqual(Page.objects.get(id=self.page4.id).position, 1)
        self.assertEqual(Page.objects.get(id=self.page5.id).position, 3)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.page3.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.page4.guid
        )

    def test_reorder_page_move_up_position_by_owner(self):
        variables = {
            "input": {
                "guid": self.page2.guid,
                "sourcePosition": 0,
                "destinationPosition": 2,
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        self.assertEqual(result["data"]["reorder"]["container"]["canEdit"], True)

    def test_reorder_page_move_down_position_by_admin(self):
        variables = {
            "input": {
                "guid": self.page4.guid,
                "sourcePosition": 2,
                "destinationPosition": 0,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Page.objects.get(id=self.page4.id).position, 0)
        self.assertEqual(Page.objects.get(id=self.page2.id).position, 1)
        self.assertEqual(Page.objects.get(id=self.page3.id).position, 2)
        self.assertEqual(Page.objects.get(id=self.page5.id).position, 3)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.page4.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.page2.guid
        )

    def test_reorder_page_move_up_position_unordered_children_by_admin(self):
        # default order is OLD -> NEW when no position is set
        variables = {
            "input": {
                "guid": self.page8.guid,
                "sourcePosition": 1,
                "destinationPosition": 2,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Page.objects.get(id=self.page7.id).position, 0)
        self.assertEqual(Page.objects.get(id=self.page8.id).position, 2)
        self.assertEqual(Page.objects.get(id=self.page9.id).position, 1)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.page7.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.page9.guid
        )

    def test_reorder_page_move_down_position_unordered_children_by_admin(self):
        # default order is OLD -> NEW when no position is set

        variables = {
            "input": {
                "guid": self.page8.guid,
                "sourcePosition": 1,
                "destinationPosition": 0,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Page.objects.get(id=self.page7.id).position, 1)
        self.assertEqual(Page.objects.get(id=self.page8.id).position, 0)
        self.assertEqual(Page.objects.get(id=self.page9.id).position, 2)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.page8.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.page7.guid
        )


class ReorderWikiPagesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory(email="user@example.com")
        self.admin = AdminFactory(email="admin@example.com")

        self.wiki1 = WikiFactory(owner=self.user)
        self.wiki2 = WikiFactory(owner=self.user, position=0, parent=self.wiki1)
        self.wiki3 = WikiFactory(owner=self.user, position=1, parent=self.wiki1)
        self.wiki4 = WikiFactory(owner=self.user, position=2, parent=self.wiki1)
        self.wiki5 = WikiFactory(owner=self.user, position=3, parent=self.wiki1)

        self.wiki6 = WikiFactory(owner=self.user)
        # when no position is set order is created_at (OLD -> NEW)
        self.wiki7 = WikiFactory(owner=self.user, parent=self.wiki6)
        self.wiki8 = WikiFactory(owner=self.user, parent=self.wiki6)
        self.wiki9 = WikiFactory(owner=self.user, parent=self.wiki6)

        self.mutation = """
            mutation SubNavReorder($input: reorderInput!) {
                reorder(input: $input) {
                    container {
                        guid
                        ...WikiFragment
                    }
                }
            }
            fragment WikiFragment on Wiki {
                canEdit
                children {
                    guid
                }
            }
        """

    def test_reorder_wiki_move_up_position_by_admin(self):
        variables = {
            "input": {
                "guid": self.wiki2.guid,
                "sourcePosition": 0,
                "destinationPosition": 2,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Wiki.objects.get(id=self.wiki2.id).position, 2)
        self.assertEqual(Wiki.objects.get(id=self.wiki3.id).position, 0)
        self.assertEqual(Wiki.objects.get(id=self.wiki4.id).position, 1)
        self.assertEqual(Wiki.objects.get(id=self.wiki5.id).position, 3)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.wiki3.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.wiki4.guid
        )

    def test_reorder_wiki_move_down_position_by_admin(self):
        variables = {
            "input": {
                "guid": self.wiki4.guid,
                "sourcePosition": 2,
                "destinationPosition": 0,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Wiki.objects.get(id=self.wiki4.id).position, 0)
        self.assertEqual(Wiki.objects.get(id=self.wiki2.id).position, 1)
        self.assertEqual(Wiki.objects.get(id=self.wiki3.id).position, 2)
        self.assertEqual(Wiki.objects.get(id=self.wiki5.id).position, 3)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.wiki4.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.wiki2.guid
        )

    def test_reorder_wiki_move_up_position_unordered_children_by_admin(self):
        # default order is OLD -> NEW when no position is set

        variables = {
            "input": {
                "guid": self.wiki8.guid,
                "sourcePosition": 1,
                "destinationPosition": 2,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Wiki.objects.get(id=self.wiki7.id).position, 0)
        self.assertEqual(Wiki.objects.get(id=self.wiki8.id).position, 2)
        self.assertEqual(Wiki.objects.get(id=self.wiki9.id).position, 1)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.wiki7.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.wiki9.guid
        )

    def test_reorder_wiki_move_down_position_unordered_children_by_admin(self):
        # default order is OLD -> NEW when no position is set

        variables = {
            "input": {
                "guid": self.wiki8.guid,
                "sourcePosition": 1,
                "destinationPosition": 0,
            }
        }

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(Wiki.objects.get(id=self.wiki7.id).position, 1)
        self.assertEqual(Wiki.objects.get(id=self.wiki8.id).position, 0)
        self.assertEqual(Wiki.objects.get(id=self.wiki9.id).position, 2)
        self.assertEqual(
            data["reorder"]["container"]["children"][0]["guid"], self.wiki8.guid
        )
        self.assertEqual(
            data["reorder"]["container"]["children"][1]["guid"], self.wiki7.guid
        )

    def test_reorder_wiki_move_up_position_by_owner(self):
        variables = {
            "input": {
                "guid": self.wiki2.guid,
                "sourcePosition": 0,
                "destinationPosition": 2,
            }
        }

        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, variables)

        self.assertEqual(result["data"]["reorder"]["container"]["canEdit"], True)
