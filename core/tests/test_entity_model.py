from unittest import mock

from core.factories import GroupFactory
from core.lib import datetime_utciso
from core.models import Entity
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class EntityModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.entity = Entity.objects.create(owner=self.owner)

    def test_serialize_entity(self):
        self.assertEqual(
            self.entity.serialize(),
            {
                "accessId": 0,
                "writeAccessId": 0,
                "inputLanguage": "nl",
                "groupGuid": "",
                "isFeatured": False,
                "isPinned": False,
                "isRecommended": False,
                "isRecommendedInSearch": False,
                "ownerGuid": self.owner.guid,
                "scheduleArchiveEntity": "",
                "scheduleDeleteEntity": "",
                "statusPublished": "published",
                "suggestedItems": [],
                "tagCategories": [],
                "tags": [],
                "timeCreated": datetime_utciso(self.entity.created_at),
                "timePublished": datetime_utciso(self.entity.published),
            },
        )

    def test_can_create_globally(self):
        visitor = UserFactory(email="visitor@example.com")
        site_admin = AdminFactory(email="admin@example.com")

        with mock.patch("core.models.entity.allow_create", return_value=True):
            self.assertTrue(self.entity.can_add(site_admin))
            self.assertTrue(self.entity.can_add(visitor))

        with mock.patch("core.models.entity.allow_create", return_value=False):
            self.assertTrue(self.entity.can_add(site_admin))
            self.assertFalse(self.entity.can_add(visitor))

    def test_can_create_in_group(self):
        group_admin = UserFactory(email="groupadmin@example.com")
        group_member = UserFactory(email="member@example.com")
        visitor = UserFactory(email="visitor@example.com")
        site_admin = AdminFactory(email="admin@example.com")
        group = GroupFactory(owner=self.owner)
        group.join(group_admin, "admin")
        group.join(group_member, "member")

        self.assertTrue(self.entity.can_add(site_admin, group))
        self.assertTrue(self.entity.can_add(group_admin, group))
        self.assertTrue(self.entity.can_add(group_member, group))
        self.assertTrue(self.entity.can_add(self.owner, group))
        self.assertFalse(self.entity.can_add(visitor, group))
