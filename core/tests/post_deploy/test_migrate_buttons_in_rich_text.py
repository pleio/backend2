import json

from core.post_deploy.migrate_buttons_in_rich_text import MigrateButtonsInRichText
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestMigrateButtonsInRichTextTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.BEFORE = {
            "type": "button",
            "attrs": {"url": "www.google.com"},
            "content": [{"type": "text", "text": "Dit is een button"}],
        }
        self.AFTER = {
            "type": "paragraph",
            "attrs": {"intro": False},
            "content": [
                {
                    "type": "button",
                    "attrs": {"url": "www.google.com"},
                    "content": [{"type": "text", "text": "Dit is een button"}],
                }
            ],
        }

        self.processor = MigrateButtonsInRichText(None)

    def test_migrate_original_situation(self):
        translated = self.processor.move_button({**self.BEFORE})
        self.assertEqual(
            translated,
            self.AFTER,
        )

    def test_migrate_new_situation(self):
        translated = self.processor.move_button({**self.AFTER})
        self.assertEqual(
            translated,
            self.AFTER,
        )


class TestMigrateButtonsInRichTextFullExampleTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.TIPTAP = json.dumps(
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [{"type": "text", "text": "Dit is tekst"}],
                    },
                    {
                        "type": "button",
                        "attrs": {"url": "www.google.com"},
                        "content": [{"type": "text", "text": "Dit is een button"}],
                    },
                ],
            }
        )
        self.EXPECTED_TRANSLATION = json.dumps(
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [{"type": "text", "text": "Dit is tekst"}],
                    },
                    {
                        "type": "paragraph",
                        "attrs": {"intro": False},
                        "content": [
                            {
                                "type": "button",
                                "attrs": {"url": "www.google.com"},
                                "content": [
                                    {"type": "text", "text": "Dit is een button"}
                                ],
                            },
                        ],
                    },
                ],
            }
        )

        self.owner = UserFactory()
        self.blog = BlogFactory(
            owner=self.owner,
            rich_description=self.TIPTAP,
        )
        self.processor = MigrateButtonsInRichText([self.blog])

    def test_migrate_full_example(self):
        self.processor.migrate()
        self.assertDictEqual(
            json.loads(self.blog.rich_description),
            json.loads(self.EXPECTED_TRANSLATION),
        )
