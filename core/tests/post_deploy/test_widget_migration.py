from core.post_deploy.showtagfilter_on_search_widgets import widget_migration
from core.tests.helpers import PleioTenantTestCase


class TestWidgetMigrationTestCase(PleioTenantTestCase):
    """
    Test the migration of search widgets that have enableFilters=True
    """

    def test_migrate_other_widget_then_search(self):
        """
        Test that other widgets then search are not migrated
        """
        widget = {
            "type": "objects",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "enableFilters", "value": "true"},
            ],
        }
        self.assertEqual(widget_migration(widget), widget)

    def test_migrate_search_widget_without_enable_filters(self):
        """
        Test that search widgets without enableFilters are not migrated
        """
        widget = {
            "type": "search",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "content", "value": "test"},
            ],
        }
        self.assertEqual(widget_migration(widget), widget)

    def test_migrate_search_widget_with_enable_filters(self):
        """
        Test that search widgets with enableFilters are migrated
        """
        widget = {
            "type": "search",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "content", "value": "test"},
                {"key": "enableFilters", "value": "true"},
            ],
        }
        self.assertEqual(
            widget_migration(widget),
            {
                "type": "search",
                "settings": [
                    {"key": "title", "value": "test"},
                    {"key": "content", "value": "test"},
                    {"key": "showTagFilter", "value": "true"},
                    {"key": "showDateFilter", "value": "true"},
                ],
            },
        )

    def test_migrate_widget_with_enable_filters_is_false(self):
        """
        Test that search widgets with enableFilters=False will have the enableFilters setting removed
        """
        widget = {
            "type": "search",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "content", "value": "test"},
                {"key": "enableFilters", "value": "false"},
            ],
        }
        self.assertEqual(
            widget_migration(widget),
            {
                "type": "search",
                "settings": [
                    {"key": "title", "value": "test"},
                    {"key": "content", "value": "test"},
                ],
            },
        )
