from core.post_deploy.migrate_menu_title_to_label import recursive_menu_conversion
from core.tests.helpers import PleioTenantTestCase


class TestMigrateMenuItemTitleToLabelTestCase(PleioTenantTestCase):
    def test_migrate_menu_item_title_to_label(self):
        menu = [
            {"link": "/blog", "title": "Blog", "children": [], "access": "public"},
            {"link": "/news", "title": "Nieuws", "children": [], "access": "public"},
            {"link": "/groups", "title": "Groepen", "children": [], "access": "public"},
            {
                "link": "/questions",
                "title": "Vragen",
                "children": [],
                "access": "public",
            },
            {"link": "/wiki", "title": "Wiki", "children": [], "access": "public"},
        ]

        self.assertEqual(
            [*recursive_menu_conversion(menu)],
            [
                {"link": "/blog", "label": "Blog", "children": [], "access": "public"},
                {
                    "link": "/news",
                    "label": "Nieuws",
                    "children": [],
                    "access": "public",
                },
                {
                    "link": "/groups",
                    "label": "Groepen",
                    "children": [],
                    "access": "public",
                },
                {
                    "link": "/questions",
                    "label": "Vragen",
                    "children": [],
                    "access": "public",
                },
                {"link": "/wiki", "label": "Wiki", "children": [], "access": "public"},
            ],
        )
