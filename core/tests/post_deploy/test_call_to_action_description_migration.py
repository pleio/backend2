import json

from core.post_deploy.migrate_call_to_action_description import (
    _description_value_to_rich_description,
)
from core.tests.helpers import PleioTenantTestCase


class TestCallToActionDescriptionMigration(PleioTenantTestCase):
    def test_migrate_call_to_action_description(self):
        # Given
        widget = json.loads(
            """{
            "type": "callToAction",
            "settings": [
              {
                "key": "description",
                "value": "Dacko\\n\\nBla",
                "attachmentId": null,
                "richDescription": null
              }
            ]
          }"""
        )

        # When
        widget = _description_value_to_rich_description(widget)

        self.assertEqual(
            widget,
            {
                "type": "callToAction",
                "settings": [
                    {
                        "key": "description",
                        "value": None,
                        "attachmentId": None,
                        "richDescription": '{"type": "doc", "content": [{"type": "paragraph", "content": [{"type": "text", "text": "Dacko"}]}, {"type": "paragraph", "content": [{"type": "text", "text": "Bla"}]}]}',
                    }
                ],
            },
        )
