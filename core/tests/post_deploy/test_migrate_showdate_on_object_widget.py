from core.post_deploy.migrate_showdate_on_object_widget import template_migration
from core.tests.helpers import PleioTenantTestCase


class TestMigrateShowdateOnObjectWidgetTestCase(PleioTenantTestCase):
    """
    Test the migration of search widgets that have enableFilters=True
    """

    def test_migrate_other_widget_then_objects(self):
        """
        Test that other widgets then search are not migrated
        """
        widget = {
            "type": "search",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "showDate", "value": "true"},
            ],
        }
        self.assertEqual(template_migration(widget), widget)

    def test_migrate_objects_widget_without_show_date(self):
        """
        Test that search widgets without enableFilters are not migrated
        """
        widget = {
            "type": "objects",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "content", "value": "test"},
            ],
        }
        self.assertEqual(template_migration(widget), widget)

    def test_migrate_objects_widget_with_show_date(self):
        """
        Test that search widgets with enableFilters are migrated
        """
        widget = {
            "type": "objects",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "content", "value": "test"},
                {"key": "showDate", "value": "true"},
            ],
        }
        self.assertEqual(
            template_migration(widget),
            {
                "type": "objects",
                "settings": [
                    {"key": "title", "value": "test"},
                    {"key": "content", "value": "test"},
                    {"key": "template", "value": "datePublished"},
                ],
            },
        )

    def test_migrate_widget_with_show_date_is_false(self):
        """
        Test that search widgets with enableFilters=False will have the enableFilters setting removed
        """
        widget = {
            "type": "objects",
            "settings": [
                {"key": "title", "value": "test"},
                {"key": "content", "value": "test"},
                {"key": "showDate", "value": "false"},
            ],
        }
        self.assertEqual(
            template_migration(widget),
            {
                "type": "objects",
                "settings": [
                    {"key": "title", "value": "test"},
                    {"key": "content", "value": "test"},
                ],
            },
        )
