from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


class ToggleIsChatOnlineOverrideTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user1 = UserFactory()
        self.user2 = UserFactory()
        self.admin = UserFactory(roles=["ADMIN"])

        self.mutation = """
            mutation toggleIsChatOnlineOverride {
                toggleIsChatOnlineOverride {
                    isChatOnlineOverride
                }
            }
        """

        self.query = """
            query UserQuery($guid: String!) {
                entity(guid: $guid) {
                     ... on User {
                        isChatOnline
                        isChatOnlineOverride
                    }
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_toggle_not_logged_in(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation)

    def test_toggle_override(self):
        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.mutation)

        data = result["data"]

        self.assertTrue(data["toggleIsChatOnlineOverride"]["isChatOnlineOverride"])

    def test_toggle_override_twice(self):
        self.graphql_client.force_login(self.user1)
        self.graphql_client.post(self.mutation)
        result = self.graphql_client.post(self.mutation)

        data = result["data"]

        self.assertFalse(data["toggleIsChatOnlineOverride"]["isChatOnlineOverride"])

    def test_override_read_other_user(self):
        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, {"guid": self.user2.guid})
        data = result["data"]

        self.assertEqual(data["entity"]["isChatOnlineOverride"], None)

    def test_override_read_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, {"guid": self.user2.guid})
        data = result["data"]

        self.assertEqual(data["entity"]["isChatOnlineOverride"], False)

    def test_is_chat_online(self):
        self.user1.profile.is_chat_online = True
        self.user1.profile.save()

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, {"guid": self.user1.guid})
        data = result["data"]

        self.assertEqual(data["entity"]["isChatOnline"], True)
        self.assertEqual(data["entity"]["isChatOnlineOverride"], False)

    def test_is_chat_online_override(self):
        self.user1.profile.is_chat_online = True
        self.user1.profile.is_chat_online_override = True
        self.user1.profile.save()

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, {"guid": self.user1.guid})
        data = result["data"]

        self.assertEqual(data["entity"]["isChatOnline"], False)
        self.assertEqual(data["entity"]["isChatOnlineOverride"], True)
