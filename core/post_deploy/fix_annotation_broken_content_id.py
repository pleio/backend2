from celery.utils.log import get_task_logger
from django.contrib.contenttypes.models import ContentType
from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import Annotation, BookmarkMixin, Entity, FollowMixin, VoteMixin

logger = get_task_logger(__name__)


@post_deploy_action
def task():
    if is_schema_public():
        return

    # get possible content types for BookmarkMixin, FollowMixin, VoteMixin subclasses
    content_type_ids = []
    possible_subclasses = (
        BookmarkMixin.__subclasses__()
        + FollowMixin.__subclasses__()
        + VoteMixin.__subclasses__()
    )
    for instance in possible_subclasses:
        content_type = ContentType.objects.get_for_model(instance)
        content_type_ids.append(content_type.id)

    fixed, deleted = 0, 0

    # select all annotations that have an invalid container_ct_id
    for broken_annotation in Annotation.objects.exclude(
        content_type__in=content_type_ids
    ):
        # try to find the correct container by searching the object_id in entities
        entity = (
            Entity.objects.filter(id=broken_annotation.object_id)
            .select_subclasses()
            .first()
        )
        if entity:
            try:
                broken_annotation.content_object = entity
                broken_annotation.save()
                fixed += 1
            except (
                Exception
            ):  # this can happen when annotations already exists then just delete it
                broken_annotation.delete()
                deleted += 1
        else:
            broken_annotation.delete()
            deleted += 1

    logger.info("Fixed %i annotations and deleted %i annotations.", fixed, deleted)
