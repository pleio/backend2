from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.cms.models import Page


@post_deploy_action
def task():
    """
    Update all search widgets that have the enableFilters setting
    - remove enableFilters
    - add showTagFilter=True
    - add showDateFilter=True
    """
    if is_schema_public():
        return

    for page in Page.objects.filter_campagne():
        page.update_widgets(widget_migration)
        page.save()


def widget_migration(widget):
    if widget["type"] != "search":
        return widget

    enable_filters = False
    new_settings = []
    for setting in widget.get("settings", []):
        if setting["key"] == "enableFilters":
            enable_filters = setting["value"] == "true"
            continue
        if setting["key"] in ["showTagFilter", "showDateFilter"]:
            return widget
        new_settings.append(setting)

    if enable_filters:
        new_settings.append({"key": "showTagFilter", "value": "true"})
        new_settings.append({"key": "showDateFilter", "value": "true"})

    widget["settings"] = new_settings
    return widget
