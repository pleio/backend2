import logging

from django.core.exceptions import ObjectDoesNotExist
from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.file.models import FileFolder, FileReference

logger = logging.getLogger(__name__)


@post_deploy_action
def task():
    if is_schema_public():
        return

    nr_changes_read_access = 0
    for ref in FileReference.objects.filter(configuration=None):
        try:
            f = FileFolder.objects.get(id=ref.file_id)
            if f.refresh_read_access():
                nr_changes_read_access += 1
                f.save()
        except ObjectDoesNotExist:
            logger.info("File with id %s does not exist.", str(ref.file_id))

    logger.info("Read access of %i files are changed.", nr_changes_read_access)
