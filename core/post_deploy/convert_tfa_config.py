from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public


@post_deploy_action
def convert_tfa_config_task():
    if is_schema_public():
        return

    if config.REQUIRE_2FA is True:
        config.REQUIRE_2FA = "all"
    elif config.REQUIRE_2FA is False:
        config.REQUIRE_2FA = "none"
