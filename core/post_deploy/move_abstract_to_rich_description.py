import json

from celery.utils.log import get_task_logger
from django.db.models.functions import Coalesce
from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import Entity

logger = get_task_logger(__name__)


@post_deploy_action
def task():
    if is_schema_public():
        return

    entities = Entity.objects.all()
    # Haal het veld "abstract" op van verschillende subclasses zoals blog, discussion, wiki.
    entities = entities.annotate(
        abstract=Coalesce(
            "news__abstract",
            "blog__abstract",
            "wiki__abstract",
            "question__abstract",
            "discussion__abstract",
            "event__abstract",
            "podcast__abstract",
            "episode__abstract",
        )
    ).filter(abstract__isnull=False)

    for entity in entities.select_subclasses():
        MigrateEntity(entity).migrate()


class MigrateEntity:
    def __init__(self, entity):
        self.entity = entity

    def get_abstract(self):
        try:
            return json.loads(self.entity.abstract)
        except json.JSONDecodeError:
            return None

    def get_rich_description(self):
        try:
            return json.loads(self.entity.rich_description)
        except json.JSONDecodeError:
            return None

    def encode(self, data):
        return json.dumps(data)

    def migrate(self):
        try:
            self.entity.rich_description = self.encode(
                self.join(self.get_abstract(), self.get_rich_description())
            )
            self.entity.save()
        except Exception:
            logger.error(
                "Failed to migrate entity schema=%s pk=%s",
            )
            pass

    def join(self, abstract, rich_description):
        assert abstract, "Abstract is empty"
        abstract["content"][0]["type"] = "paragraph_bold"

        if not rich_description:
            return abstract

        assert rich_description["content"][0]["type"] != "paragraph_bold", (
            "Rich description already contains abstract"
        )

        rich_description["content"] = [
            *abstract["content"],
            *rich_description["content"],
        ]

        return rich_description
