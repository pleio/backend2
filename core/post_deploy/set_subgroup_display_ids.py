from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import Subgroup


@post_deploy_action
def task():
    if is_schema_public():
        return

    for subgroup in Subgroup.objects.all():
        subgroup.save()
