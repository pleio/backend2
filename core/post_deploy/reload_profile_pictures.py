import logging

from post_deploy import post_deploy_action

from control.models import UserSyncController
from core.lib import is_schema_public

logger = logging.getLogger(__name__)


@post_deploy_action
def reload_profile_pictures_task():
    # Run task once
    if not is_schema_public():
        return

    UserSyncController.objects.get_or_create(active=True)


@post_deploy_action
def reload_profile_pictures_task_again():
    # Run task once
    if not is_schema_public():
        return

    UserSyncController.objects.get_or_create(active=True)
