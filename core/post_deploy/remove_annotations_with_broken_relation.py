from celery.utils.log import get_task_logger
from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import Annotation

logger = get_task_logger(__name__)


@post_deploy_action
def task():
    if is_schema_public():
        return

    count = 0

    queryset = Annotation.objects.all()

    for annotation in queryset.iterator(
        chunk_size=10000
    ):  # using iterator for large datasets memory consumption
        try:
            if not annotation.content_object:
                annotation.delete()
                count += 1
        except AttributeError:
            annotation.delete()
            count += 1

    logger.info("Deleted %s broken annotations", count)
