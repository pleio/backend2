from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import SiteAccessRequest


@post_deploy_action
def task():
    if is_schema_public():
        return

    for request in SiteAccessRequest.objects.filter(processed=False):
        if request.accepted or request.reason:
            request.processed = True
            request.save()
