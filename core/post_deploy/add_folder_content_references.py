from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.file.models import FileFolder, FileReference


@post_deploy_action
def task():
    if is_schema_public():
        return

    for folder_reference in FileReference.objects.filter(
        file__type=FileFolder.Types.FOLDER
    ):
        try:
            folder_reference.container.update_attachments_links()
        except AttributeError:
            pass
