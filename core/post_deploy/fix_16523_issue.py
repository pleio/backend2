from django.core.exceptions import ValidationError
from post_deploy import post_deploy_action

from core import config
from core.constances import ConfigurationChoices
from core.lib import is_schema_public
from entities.file.models import FileFolder, FileReference


@post_deploy_action
def task():
    if is_schema_public():
        return

    configuration_items = [
        (config.LOGO, ConfigurationChoices.LOGO.name),
        (config.ICON, ConfigurationChoices.ICON.name),
        (config.FAVICON, ConfigurationChoices.FAVICON.name),
    ]
    for path, name in configuration_items:
        if path:
            parts = path.split("/")
            for part in parts:
                try:
                    file = FileFolder.objects.filter(id=part).first()
                    FileReference.objects.update_configuration(name, file)
                except (FileFolder.DoesNotExist, ValidationError):
                    pass
