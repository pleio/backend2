from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public
from core.utils.tags import TagCategoryStorage


@post_deploy_action
def update_tag_categories_to_support_synonyms():
    if is_schema_public():
        return

    storage = TagCategoryStorage()

    for category_data in config.TAG_CATEGORIES:
        storage.store_tag_category(
            category_data["name"], _add_synonym_placeholder(category_data)
        )


def _add_synonym_placeholder(category_data):
    result = {
        "name": category_data["name"],
        "values": [],
    }
    for value in category_data["values"]:
        if isinstance(value, dict):
            result["values"].append(value)
        else:
            result["values"].append({"value": value, "synonyms": []})
    return result
