from core.lib import is_schema_public
from core.models import Group
from core.post_deploy import post_deploy_action


@post_deploy_action
def create_landingpages_for_existing_groups():
    if is_schema_public():
        return

    for group in Group.objects.all():
        if group.start_page:
            continue

        group._start_page = group.create_group_start_page(
            widgets=group.widget_repository,
            is_submit_updates_enabled=group.is_submit_updates_enabled,
        )
        group.menu = [{"type": "plugin", "id": plugin} for plugin in group.plugins]
        group.save()
