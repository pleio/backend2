import json

from core.lib import is_schema_public
from core.post_deploy import post_deploy_action
from entities.cms.models import Page


@post_deploy_action
def task():
    if is_schema_public():
        return

    for entity in Page.objects.all():
        before = entity.serialize()
        entity.update_widgets(_description_value_to_rich_description)
        if entity.serialize() != before:
            entity.save()


def _description_value_to_rich_description(widget):
    if widget["type"] == "callToAction":
        new_settings = []
        for setting in widget.get("settings", []):
            if setting["key"] == "description":
                setting["richDescription"] = _to_tiptap(setting["value"])
                setting["value"] = None
            new_settings.append(setting)
        widget["settings"] = new_settings
    return widget


def _to_tiptap(value):
    if not value:
        return "{}"
    paragraphs = value.split("\n\n")
    return json.dumps(
        {
            "type": "doc",
            "content": [
                {
                    "type": "paragraph",
                    "content": [
                        {
                            "type": "text",
                            "text": p,
                        }
                    ],
                }
                for p in paragraphs
            ],
        }
    )
