import logging

from django.contrib.contenttypes.models import ContentType
from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import AttachmentMixin, Entity
from entities.file.models import FileReference

logger = logging.getLogger(__name__)


@post_deploy_action
def task():
    if is_schema_public():
        return

    # get possible content types for AttachementMixin subclasses
    content_type_ids = []
    for instance in AttachmentMixin.__subclasses__():
        content_type = ContentType.objects.get_for_model(instance)
        content_type_ids.append(content_type.id)

    fixed, deleted = 0, 0

    # select all file references that an invalid container_ct_id
    for broken_reference in FileReference.objects.exclude(container_ct_id=None).exclude(
        container_ct_id__in=content_type_ids
    ):
        # try to find the correct container
        entity = (
            Entity.objects.filter(id=broken_reference.container_fk)
            .select_subclasses()
            .first()
        )
        if entity:
            broken_reference.container = entity
            broken_reference.save()
            fixed += 1
        else:
            broken_reference.delete()
            deleted += 1

    logger.info("Fixed %i file references and deleted %i references.", fixed, deleted)
