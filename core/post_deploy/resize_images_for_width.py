import logging

from PIL import Image
from post_deploy import post_deploy_action

from core.lib import is_schema_public, tenant_schema
from core.models import ResizedImage

logger = logging.getLogger(__name__)


@post_deploy_action(auto=True, description="Resize images for width")
def task():
    if is_schema_public():
        return

    for image in ResizedImage.objects.filter(status=ResizedImage.OK).iterator(
        chunk_size=1000
    ):
        retry_resize_image(image)


def retry_resize_image(container):
    from core.tasks import image_resize

    try:
        image = Image.open(container.upload.open())
        if image.width > container.size:
            ri = ResizedImage.objects.get(id=container.id)
            ri.upload.delete()
            ri.status = ResizedImage.PENDING
            ri.save()
            image_resize.delay(tenant_schema(), container.id)
    except Exception as e:
        logger.info(
            "Error processing ResizedImage.id %s with error: %s", str(container.id), e
        )
