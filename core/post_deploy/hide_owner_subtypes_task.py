from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public


@post_deploy_action
def task():
    if is_schema_public():
        return

    hide_owner_for_content = ["wiki", "page"]
    if not config.SHOW_NEWS_OWNER:
        hide_owner_for_content.append("news")

    config.HIDE_CONTENT_OWNER = hide_owner_for_content
