from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.cms.models import Page


@post_deploy_action
def task():
    """
    Update all objects widgets showDate == True to template = datePublished
    """
    if is_schema_public():
        return

    for page in Page.objects.filter_campagne():
        page.update_widgets(template_migration)
        page.save()


def template_migration(widget):
    if widget["type"] != "objects":
        return widget

    new_settings = []
    for setting in widget.get("settings", []):
        if setting["key"] == "showDate":
            if setting["value"] == "true":
                new_settings.append({"key": "template", "value": "datePublished"})
            continue

        new_settings.append(setting)

    widget["settings"] = new_settings
    return widget
