import json

from core.lib import is_schema_public
from core.models import Comment, Entity, Group
from core.post_deploy import post_deploy_action


@post_deploy_action
def task():
    if is_schema_public():
        return

    MigrateButtonsInRichText(Entity.objects.iterator(chunk_size=1000)).migrate()
    MigrateButtonsInRichText(Group.objects.all().iterator(chunk_size=1000)).migrate()
    MigrateButtonsInRichText(Comment.objects.all().iterator(chunk_size=1000)).migrate()


class MigrateButtonsInRichText:
    def __init__(self, qs):
        self.qs = qs
        self.changed = False

    def migrate(self):
        for entity in self.qs:
            if entity.__class__.__name__ == "Entity":
                entity = Entity.objects.get_subclass(id=entity.id)
            self._migrate_one(entity)

    def move_button(self, tiptap):
        try:
            if tiptap["type"] == "button":
                self.changed = True
                return {
                    "type": "paragraph",
                    "attrs": {"intro": False},
                    "content": [tiptap],
                }
        except Exception:
            pass
        return tiptap

    def _migrate_one(self, entity):
        self.changed = False

        def _move_buttons(maybe_json):
            try:
                tiptap = json.loads(maybe_json)
                if tiptap["content"]:
                    new_content = []
                    for item in tiptap["content"]:
                        new_content.append(self.move_button(item))
                    tiptap["content"] = new_content
                return json.dumps(tiptap)
            except (json.JSONDecodeError, TypeError):
                return maybe_json

        if "map_rich_text_fields" in dir(entity):
            entity.map_rich_text_fields(_move_buttons)

            if self.changed:
                entity.save()
