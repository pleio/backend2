from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public


@post_deploy_action
def task():
    if is_schema_public():
        return

    config.MENU = [*recursive_menu_conversion(config.MENU)]


def recursive_menu_conversion(menu):
    for item in menu or []:
        if "title" in item:
            item["label"] = item["title"]
            del item["title"]
        item["children"] = [*recursive_menu_conversion(item.get("children"))]
        yield item
