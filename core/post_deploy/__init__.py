import csv
import datetime
import os

import pandas as pd
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.files.storage import default_storage
from django.db.models import Prefetch
from notifications.models import Notification
from post_deploy import post_deploy_action

from core.lib import (
    access_id_to_acl,
    get_full_url,
    is_schema_public,
    safe_file_path,
    tenant_schema,
)

from .add_folder_content_references import task as add_folder_content_references_task
from .add_initial_group_widgets import task as add_initial_group_widgets_task
from .add_members_page_plugin_to_all_groups import (
    task as add_members_page_plugin_to_all_groups_task,
)
from .convert_abstract_to_tiptap import convert_abstract_to_tiptap_task
from .convert_tfa_config import convert_tfa_config_task
from .correct_access_request_processed_value import task
from .enable_collab_editing import task as enable_collab_editing_task
from .enable_push_notifications import task as enable_push_notifications_task
from .fill_config_site_notes import task as fill_config_site_notes_task
from .fix_16523_issue import task as fix_16523_issue_task
from .fix_annotation_broken_content_id import task as fix_annotation_broken_content_id
from .fix_event_add_email_attendee import task as fix_event_add_email_attendee_task
from .fix_file_reference_broken_content_id import (
    task as fix_file_reference_broken_content_id_task,
)
from .group_landingpages import create_landingpages_for_existing_groups
from .hide_owner_subtypes_task import task as hide_owner_subtypes_task
from .initial_site_owner import task as initial_site_owner_task
from .migrate_buttons_in_rich_text import task as migrate_buttons_in_rich_text_task
from .migrate_call_to_action_description import (
    task as migrate_call_to_action_description_task,
)
from .migrate_featured_video_thumbnail import (
    task as migrate_featured_video_thumbnail_task,
)
from .migrate_menu_access import task as migrate_menu_access_task
from .migrate_menu_title_to_label import task as migrate_menu_title_to_label_task
from .migrate_showdate_on_object_widget import (
    task as migrate_showdate_on_object_widget_task,
)
from .migrate_text_widgets_background_color import (
    task as migrate_text_widgets_background_color_task,
)
from .move_abstract_to_rich_description import (
    task as move_abstract_to_rich_description_task,
)
from .refresh_all_attachments_read_access import (
    task as refresh_all_attachments_read_access_task,
)
from .reload_profile_pictures import (
    reload_profile_pictures_task,
    reload_profile_pictures_task_again,
)
from .remove_annotations_with_broken_relation import (
    task as remove_annotations_with_broken_relation,
)
from .resize_images_for_width import task as resize_images_for_width_task
from .set_subgroup_display_ids import task as set_subgroup_display_ids_task
from .showtagfilter_on_search_widgets import (
    task as showtagfilter_on_search_widgets_task,
)
from .update_tag_categories import update_tag_categories_to_support_synonyms

logger = get_task_logger(__name__)


@post_deploy_action(auto=False)
def remove_notifications_with_broken_relation():
    if is_schema_public():
        return

    count = 0

    queryset = Notification.objects.prefetch_related(Prefetch("action_object"))

    for notification in queryset.iterator(
        chunk_size=10000
    ):  # using iterator for large datasets memory consumption
        if not notification.action_object:
            notification.delete()
            count += 1

    logger.info("Deleted %s broken notifications", count)


@post_deploy_action
def write_missing_file_report():
    if is_schema_public():
        return

    from entities.file.models import FileFolder

    filename = "missing_file_report_%s.csv" % tenant_schema()
    report_file = safe_file_path(settings.BACKUP_PATH, filename)
    total = 0
    files_ok = 0
    files_err = 0
    with open(report_file, "w") as fh:
        writer = csv.writer(fh, delimiter=";")
        writer.writerow(
            [
                "id",
                "path",
                "url",
                "name",
                "owner",
                "group",
                "groupowner",
                "groupownermail",
            ]
        )
        for file in FileFolder.objects.filter(
            type=FileFolder.Types.FILE, upload__isnull=False
        ):
            try:
                if not default_storage.exists(file.upload.name):
                    writer.writerow(
                        [
                            file.guid,
                            file.upload.name,
                            get_full_url(file.url),
                            file.title,
                            file.owner.name if file.owner else "-",
                            file.group.name if file.group else "-",
                            file.group.owner.name if file.group else "-",
                            file.group.owner.email if file.group else "-",
                        ]
                    )
                    total += 1
                else:
                    files_ok += 1
            except Exception as e:
                writer.writerow(
                    [
                        file.guid,
                        f"Error: {e.__class__} {e}",
                        file.owner.name if file.owner else "",
                        file.group.name if file.group else "",
                        file.group.owner.name if file.group else "",
                        file.group.owner.email if file.group else "",
                    ]
                )
                total += 1
                files_err += 1
        writer.writerow([f"{total} files can't be found on the disk."])
        writer.writerow([f"{files_ok} files were just fine."])
        writer.writerow([f"{files_err} files had errors while checking."])

    if total == 0:
        os.unlink(report_file)


@post_deploy_action
def entity_updated_at_from_report():
    """
    Fix updated_at date for entities involved in the 27/10/2022 updated_at incident
    Entity CSV format: id;updated_at
    """
    if is_schema_public():
        return

    report_file_path = (
        safe_file_path(settings.BACKUP_PATH, "entity_updated_at_" + tenant_schema())
        + ".csv"
    )

    if not os.path.isfile(report_file_path):
        logger.error(
            "%s does not exist for tenant %s", report_file_path, tenant_schema()
        )
        return

    df = pd.read_csv(report_file_path, delimiter=";")
    df["updated_at"] = pd.to_datetime(df["updated_at"])

    from core.models import Entity

    total = 0
    queryset = Entity.objects.filter(updated_at__date=datetime.date(2022, 10, 27))
    for entity in queryset.iterator(chunk_size=10000):
        result = df[df["id"].str.match(str(entity.id))]
        if len(result.index) > 0:
            match = result.iloc[0]
            entity.updated_at = match["updated_at"]
            entity.save()
            total += 1

    logger.error(
        "Updated updated_at for %i entities in schema %s ", total, tenant_schema()
    )


@post_deploy_action
def fix_user_profile_field_acl():
    """
    Fixes issue where onboarding set empty read_access on profile fields
    """
    if is_schema_public():
        return

    from core.models import UserProfileField

    count = 0
    for field in UserProfileField.objects.filter(read_access__len=0):
        field.read_access = access_id_to_acl(field.user_profile.user, 1)
        field.write_access = access_id_to_acl(field.user_profile.user, 0)
        field.save()
        count += 1

    logger.info("Fixed ACL of %i profile fields in schema %s", count, tenant_schema())
