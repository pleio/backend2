from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public, tenant_instance


@post_deploy_action
def task():
    if is_schema_public():
        return

    tenant = tenant_instance()
    config.SITE_NOTES = tenant.administrative_details.get("notes")
