from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public
from core.resolvers.shared import default_menu_access


def translate_access(accessId):
    if accessId is None:
        return default_menu_access()
    if accessId == 2:
        return "public"
    if accessId == 1:
        return "loggedIn"
    elif accessId == 0:
        return "administrator"


def transform_menu(items):
    for item in items or []:
        if "access" not in item:
            item["access"] = translate_access(item.get("accessId"))
        if "accessId" in item:
            del item["accessId"]
        item["children"] = [*transform_menu(item.get("children"))]
        yield item


@post_deploy_action
def task():
    if is_schema_public():
        return

    config.MENU = [*transform_menu(config.MENU)]
