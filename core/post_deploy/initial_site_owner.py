from post_deploy import post_deploy_action

from core.constances import USER_ROLES
from core.lib import is_schema_public, tenant_instance
from user.models import User


@post_deploy_action
def task():
    if is_schema_public():
        return

    tenant = tenant_instance()
    if tenant.administrative_details.get("site_owner"):
        return

    if (
        initial_admin := User.objects.filter(
            roles__contains=[USER_ROLES.ADMIN], is_active=True
        )
        .exclude(email__endswith="@pleio.nl")
        .order_by("created_at")
        .first()
    ):
        tenant.administrative_details = {
            **tenant.administrative_details,
            "site_owner": initial_admin.name,
            "site_owner_email": initial_admin.email,
        }
        tenant.save()
