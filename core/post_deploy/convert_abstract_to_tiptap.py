from django.apps import apps
from post_deploy import post_deploy_action

from core.lib import is_schema_public
from core.models import TranslationCache
from core.utils.convert import html_to_tiptap, is_tiptap


@post_deploy_action
def convert_abstract_to_tiptap_task():
    if is_schema_public():
        return

    # Remove ~:excerpt texts from translation cache.
    TranslationCache.objects.filter(reference__endswith=":excerpt").delete()

    # Convert all abstracts to tiptap.
    for instance in get_documents_with_abstract():
        instance.abstract = html_to_tiptap(instance.abstract)
        instance.save()


def get_documents_with_abstract():
    models = [
        ("blog", "Blog"),
        ("discussion", "Discussion"),
        ("event", "Event"),
        ("news", "News"),
        ("podcast", "Episode"),
        ("podcast", "Podcast"),
        ("question", "Question"),
        ("wiki", "Wiki"),
    ]
    for app_name, model_name in models:
        Model = apps.get_model(app_name, model_name)
        for instance in Model.objects.filter(abstract__isnull=False):
            if not is_tiptap(instance.abstract):
                yield instance
