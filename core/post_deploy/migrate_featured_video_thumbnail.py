import logging

from django.db.models import Q

from core.lib import is_schema_public
from core.models.featured import FeaturedCoverMixin
from core.post_deploy import post_deploy_action

logger = logging.getLogger(__name__)


@post_deploy_action
def task():
    if is_schema_public():
        return

    for subclass in FeaturedCoverMixin.__subclasses__():
        MigrateFeaturedVideoThumbnail(
            subclass.objects.exclude(
                Q(featured_video__isnull=True) | Q(featured_video__exact="")
            ).iterator(chunk_size=1000),
            subclass.__name__,
        )


class MigrateFeaturedVideoThumbnail:
    def __init__(self, qs, entity_name=""):
        self.entity_name = entity_name
        self.qs = qs
        self.migrate()

    def migrate(self):
        updated = 0
        for entity in self.qs:
            entity.featured_video_thumbnail_url = entity.get_video_thumbnail_url(
                entity.featured_video
            )
            entity.save()
            updated += 1

        logger.info(
            "Updated %i featured video thumbnail urls on %s",
            updated,
            self.entity_name,
        )
