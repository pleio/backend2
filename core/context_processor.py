from django.conf import settings

from core import config
from core.lib import is_schema_public, tenant_schema


def config_processor(request):
    """
    Add config to request context
    """
    tenant_context = (
        {
            "config": config,
            "FONTS_IN_USE": [*filter(bool, [config.FONT, config.FONT_BODY])],
            "HEADING_FONT": config.FONT,
            "BODY_FONT": config.FONT_BODY or config.FONT,
        }
        if not is_schema_public()
        else {}
    )
    return {
        "vapid_public_key": settings.WEBPUSH_SETTINGS["VAPID_PUBLIC_KEY"],
        "webpack_dev_server": settings.WEBPACK_DEV_SERVER,
        **tenant_context,
    }


def mail_context_processor(request):
    if is_schema_public():
        return {}
    return {
        "SCHEMA": tenant_schema(),
        "SHOW_OWNER_AT_BLOG": "blog" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_DISCUSSION": "discussion" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_EPISODE": "episode" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_EVENT": "event" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_FILE": "file" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_NEWS": "news" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_PAGE": "page" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_PODCAST": "podcast" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_QUESTION": "question" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_TASK": "task" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_UPDATE": "statusupdate" not in config.HIDE_CONTENT_OWNER,
        "SHOW_OWNER_AT_WIKI": "wiki" not in config.HIDE_CONTENT_OWNER,
    }
