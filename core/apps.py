from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "core"

    def ready(self):
        import core.signals  # noqa: F401

        from .models.tags import register_model_for_tags

        register_model_for_tags(self.get_model("Entity"))
        register_model_for_tags(self.get_model("Group"))

        from django.conf import settings

        from core.lib import webpack_dev_server_is_available

        settings.WEBPACK_DEV_SERVER = webpack_dev_server_is_available()
