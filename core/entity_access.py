from core import config
from core.lib import get_acl


class EntityAccessResult(Exception):
    def __init__(self, granted):
        self.granted = granted

    def __bool__(self):
        return bool(self.granted)


class AccessTestBase:
    def __init__(self, user, entity):
        self.entity = entity
        self.user = user

    def test(self):
        raise NotImplementedError()


class DenyAnonymousVisitors(AccessTestBase):
    """
    Deny access to anonymous visitors.
    """

    def test(self):
        if not self.user.is_authenticated:
            raise EntityAccessResult(False)


class GrantAdministrators(AccessTestBase):
    """
    Grant access if the user is a site-admin.
    """

    def test(self):
        if self.user.is_authenticated and self.user.is_site_admin:
            raise EntityAccessResult(True)


class ContentModeratorAccessRuleBase(AccessTestBase):
    def is_moderation_enabled(self):
        return (
            config.CONTENT_MODERATION_ENABLED
            and getattr(self.entity, "type_to_string", "")
            in config.REQUIRE_CONTENT_MODERATION_FOR
            and self.entity.publish_requests.exists()
        )

    def is_group_staff(self):
        return self.entity.group and self.entity.group.is_staff(self.user)


class GrantContentModerators(ContentModeratorAccessRuleBase):
    """
    Allow entity access if the user should have moderator access.
    """

    def test(self):
        if (
            self.is_moderation_enabled()
            and self.user.is_authenticated
            and (self.user.is_editor or self.is_group_staff())
        ):
            raise EntityAccessResult(True)


class DenyNonContentModerators(ContentModeratorAccessRuleBase):
    """
    Deny access if the user should not have moderation access.
    """

    def test(self):
        if self.is_moderation_enabled() and (
            not self.user.is_authenticated
            or not (self.user.is_editor or self.is_group_staff())
        ):
            raise EntityAccessResult(False)


class GrantGroupOwnersAndAdmins(AccessTestBase):
    """
    Allow access if the user is a group owner or admin, if applicable.
    """

    def test(self):
        group = self.entity.group
        if (
            self.user.is_authenticated
            and group
            and (group.is_admin(self.user) or group.is_owner(self.user))
        ):
            raise EntityAccessResult(True)


class GrantAuthorizedUsers(AccessTestBase):
    """
    Allow access if the user has write access according to the access control list.
    """

    def test(self):
        can_write = len(get_acl(self.user) & set(self.entity.write_access)) > 0
        if can_write:
            raise EntityAccessResult(True)


class GrantUsersThatCreate(AccessTestBase):
    """
    Allow access if the user as can_add privileges.
    """

    def test(self):
        if self.entity.can_add(self.user, self.entity.group):
            raise EntityAccessResult(True)
