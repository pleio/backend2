import os
import uuid

from django.core.exceptions import ValidationError

from core.exceptions import AttachmentVirusScanError


class WidgetSerializerBase:
    def __init__(self, data, acting_user=None, writing=False):
        self.data = data
        self.acting_user = acting_user
        self.writing = writing

    def serialize(self):
        raise NotImplementedError()

    def attachments(self):
        raise NotImplementedError()

    def rich_fields(self):
        raise NotImplementedError()


class WidgetSerializer(WidgetSerializerBase):
    CLEAN_WIDGET = {
        "guid": None,
        "type": None,
        "settings": [],
        "userSettings": [],
    }

    @property
    def guid(self):
        if not self.data.get("guid") and self.writing:
            return str(uuid.uuid4())
        return self.data.get("guid")

    @property
    def type(self):
        return self.data.get("type")

    @property
    def settings(self):
        return [
            WidgetSettingSerializer(s, self.acting_user, self.writing)
            for s in self.data.get("settings", []) or []
        ]

    @property
    def userSettings(self):
        if getattr(self.acting_user, "is_authenticated", False) and self.guid:
            for setting in (
                self.acting_user.profile.get_widget_settings(self.guid) or []
            ):
                yield WidgetSettingSerializer(setting, self.acting_user, self.writing)

    def serialize(self):
        result = {
            "guid": self.guid,
            "type": self.type,
            "settings": [],
            "userSettings": [s.serialize() for s in self.userSettings],
        }
        for setting in self.settings:
            result["settings"].append(setting.serialize())
        return result

    def attachments(self):
        for setting in self.settings:
            yield from setting.attachments()

    def rich_fields(self):
        for setting in self.settings:
            yield from setting.rich_fields()

    def map_rich_fields(self, callback):
        new_settings = []
        for setting in self.settings:
            setting.transform_rich_field(callback)
            new_settings.append(setting.serialize())
        self.data["settings"] = new_settings


class WidgetSettingSerializer(WidgetSerializerBase):
    CLEAN_SETTING = {
        "key": None,
        "richDescription": None,
        "value": None,
        "attachmentId": None,
        "links": [],
    }

    @property
    def key(self):
        return self.data.get("key")

    @property
    def value(self):
        return self.data.get("value")

    @property
    def richDescription(self):
        if self.data.get("richDescription"):
            return self.data.get("richDescription")
        if self.data.get("key") == "richDescription" and self.data.get("value"):
            return self.data.get("value")

    @property
    def attachmentId(self):
        if (
            self.writing
            and not self.data.get("attachmentId")
            and self.data.get("attachment")
            and self.acting_user
        ):
            self.data["attachmentId"] = serialize_attachment(
                self.data.get("attachment"), self.acting_user
            )
        return self.data.get("attachmentId")

    @property
    def attachment(self):
        from entities.file.models import FileFolder

        if self.attachmentId:
            return FileFolder.objects.filter(id=self.attachmentId).first()

    @property
    def links(self):
        return [
            serialize_link(link, self.acting_user)
            for link in self.data.get("links", [])
        ]

    def serialize(self):
        return {
            "key": self.key,
            "value": self.value,
            "richDescription": self.richDescription,
            "attachmentId": self.attachmentId,
            "links": self.links,
        }

    def attachments(self):
        if self.attachmentId:
            yield self.attachmentId
        for link in self.links:
            if link.get("imageId"):
                yield link.get("imageId")

    def rich_fields(self):
        if self.richDescription:
            yield self.richDescription

    def transform_rich_field(self, callback):
        if self.data.get("key") == "richDescription" and self.data.get("value"):
            self.data["value"] = callback(self.data["value"])
        if self.data.get("richDescription"):
            self.data["richDescription"] = callback(self.data["richDescription"])


def serialize_attachment(attachment_input, acting_user):
    from core.resolvers import shared
    from entities.file.models import FileFolder

    try:
        attachment = FileFolder.objects.get(id=attachment_input.get("id"))
    except (FileFolder.DoesNotExist, ValidationError, AttributeError):
        attachment = FileFolder.objects.create(
            upload=attachment_input, owner=acting_user
        )
        if not attachment.scan():
            attachment.delete()
            raise AttachmentVirusScanError(os.path.basename(attachment.upload.name))
        shared.post_upload_file(attachment)
    return str(attachment.id)


def serialize_link(link_input, acting_user):
    if not link_input.get("imageId") and link_input.get("image") and acting_user:
        link_input["imageId"] = serialize_attachment(
            link_input.get("image"), acting_user
        )
    # Return a copy of link_input without the image key.
    result = {k: v for k, v in link_input.items() if k != "image"}
    if "id" not in result:
        result["id"] = str(uuid.uuid4())

    return result
