import bleach
from django.contrib.auth.models import AnonymousUser
from django.contrib.syndication.views import Feed, FeedDoesNotExist
from django.db.models import Q

from core import config
from core.lib import early_this_morning
from core.models import Entity
from core.utils.convert import truncate_rich_description
from entities.activity.resolvers.query import (
    QUERY_SUBTYPES,
    conditional_subtypes_filter,
)


def is_valid_subtype(subtype):
    return any(subtype == filter.key for filter in QUERY_SUBTYPES)


class RssFeed(Feed):
    link = "/rss"

    @property
    def title(self):
        return config.NAME

    @property
    def description(self):
        return config.DESCRIPTION

    def get_object(self, request, entity_type=None):
        if entity_type and not is_valid_subtype(entity_type):
            raise FeedDoesNotExist
        return entity_type

    def items(self, obj):
        subtypes = []
        if obj:
            subtypes = [obj]

        qs = Entity.objects.visible(AnonymousUser()).filter(
            conditional_subtypes_filter(subtypes)
        )

        if obj == "event":
            qs = qs.filter(Q(event__end_date__gte=early_this_morning())).order_by(
                "event__start_date"
            )
        else:
            qs = qs.order_by("-published")

        return qs.select_subclasses()[:50]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        try:
            if getattr(item, "excerpt", None):
                # Feed does not allow any html.
                return bleach.clean(item.excerpt, tags=[], strip=True)
            elif getattr(item, "rich_description", None):
                return truncate_rich_description(item.rich_description)
        except ValueError:
            pass
        return ""

    def item_link(self, item):
        return item.url

    def item_pubdate(self, item):
        if hasattr(item, "start_date"):
            return item.start_date
        return item.published
