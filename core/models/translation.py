from django.db import models


class TranslationCache(models.Model):
    reference = models.CharField(max_length=128)
    hash = models.CharField(max_length=32)
    language_code = models.CharField(max_length=16)
    value = models.TextField()
