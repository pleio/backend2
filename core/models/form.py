from datetime import datetime

from django.db import models


class EntityUserFormFieldQuerySet(models.QuerySet):
    def filter_respondent(self, user, email):
        if user:
            return self.filter(user=user)
        return self.filter(email=email)


class EntityUserFormField(models.Model):
    class Meta:
        ordering = ["created_at"]
        constraints = [
            models.UniqueConstraint(
                fields=["entity_id", "form_field", "email"],
                name="unique_enitity_user_form_field_email",
            ),
            models.UniqueConstraint(
                fields=["entity_id", "form_field", "user"],
                name="unique_enitity_user_form_field_user",
            ),
        ]

    objects = EntityUserFormFieldQuerySet.as_manager()

    form_field = models.CharField(max_length=128)
    entity = models.ForeignKey(
        "core.Entity",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="entity_form_user_answers",
    )
    user = models.ForeignKey(
        "user.User", on_delete=models.CASCADE, null=True, blank=True
    )

    email = models.EmailField(max_length=256, null=True, blank=True)

    value = models.TextField(blank=True, null=True)
    value_date = models.DateField(default=None, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if self.user:
            self.email = self.user.email
        self.set_date_field_value()
        self.full_clean()
        super().save(*args, **kwargs)

    @property
    def field_type(self):
        from core.utils.entity_form import EntityFormWrapper

        form = EntityFormWrapper(self.entity.form_specs)
        field = form.get_field(self.form_field)
        return field.field_type

    def set_date_field_value(self):
        if self.field_type == "dateField":
            try:
                self.value_date = datetime.strptime(self.value, "%Y-%m-%d")
            except Exception:
                self.date_value = None

    def serialize(self):
        return {
            "form_field": self.form_field,
            "entity_id": self.entity_id,
            "user_id": self.user_id,
            "email": self.email,
            "value": self.value,
            "value_date": self.value_date,
            "created_at": self.created_at,
        }
