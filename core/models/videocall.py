import uuid

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.template.loader import render_to_string
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import get_video_call_jwt_token, tenant_schema
from core.models.entity import Entity
from user.models import User


class VideoCall(models.Model):
    class Meta:
        ordering = ["created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey("user.User", on_delete=models.CASCADE)
    host_url = models.URLField(null=False)
    guest_url = models.URLField(null=False)

    def recipients(self):
        return [self.user]

    def custom_message(self):
        guest_list, guest = self.guests.guest_list()
        if guest:
            return render_to_string(
                "notification/videocall_host_notification.html",
                {
                    "name": self.user.name,
                    "guest_list": ", ".join([g.name for g in guest_list]),
                    "guest": guest.name,
                    "url": self.host_url,
                },
            )
        return ""

    @property
    def guid(self):
        return str(self.id)

    def send_notification(self):
        from core.tasks import create_notification

        create_notification.delay(
            schema_name=tenant_schema(),
            verb="custom",
            model_name=self._meta.label,
            entity_id=self.guid,
            sender_id=self.user.guid,
        )


class VideoCallGuestQueryset(models.QuerySet):
    def guest_list(self):
        guest_list = []
        for vc_guest in self.all():
            guest_list.append(vc_guest.user)
        size = len(guest_list)
        if size > 1:
            return guest_list[:-1], guest_list[size - 1]
        return [], guest_list[0]


class VideoCallGuest(models.Model):
    class Meta:
        ordering = ["created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    created_at = models.DateTimeField(auto_now_add=True)
    video_call = models.ForeignKey(
        "core.VideoCall", on_delete=models.CASCADE, related_name="guests"
    )
    user = models.ForeignKey(
        "user.User", on_delete=models.CASCADE, related_name="guests"
    )

    objects = VideoCallGuestQueryset.as_manager()

    def recipients(self):
        return [self.user]

    def custom_message(self):
        return render_to_string(
            "notification/videocall_guest_notification.html",
            {
                "name": self.video_call.user.name,
                "url": self.video_call.guest_url,
            },
        )

    @property
    def host(self):
        return self.video_call.user

    @property
    def guid(self):
        return str(self.id)

    def send_notification(self):
        from core.tasks import create_notification

        create_notification.delay(
            schema_name=tenant_schema(),
            verb="custom",
            model_name=self._meta.label,
            entity_id=self.guid,
            sender_id=self.video_call.user.guid,
        )


class EntityVideoCall(models.Model):
    class Meta:
        ordering = ["created_at"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    created_at = models.DateTimeField(auto_now_add=True)
    entity = models.ForeignKey(
        "core.Entity", on_delete=models.CASCADE, related_name="video_calls"
    )

    @property
    def guid(self):
        return str(self.id)

    @property
    def url(self):
        from core.lib import get_full_url

        return get_full_url("/videocall/%s" % self.guid)

    def is_moderator(self, user):
        if not user.is_authenticated:
            return False
        if self.entity and self.entity.owner == user:
            return True
        if EntityVideoCallModerator.objects.filter(user=user, video_call=self).first():
            return True
        return False

    def get_moderators(self):
        moderators = []
        for moderator in EntityVideoCallModerator.objects.filter(
            video_call=self
        ).order_by("user__name"):
            moderators.append(moderator.user)
        return moderators

    def update_moderators(self, user_guids):
        self.moderators.all().exclude(user_id__in=user_guids).delete()

        valid_users = []
        for guid in user_guids:
            try:
                user = User.objects.get(id=guid)
                valid_users.append(user)
            except ObjectDoesNotExist:
                raise GraphQLError(COULD_NOT_FIND)

        for user in valid_users:
            EntityVideoCallModerator.objects.get_or_create(user=user, video_call=self)

    def get_jwt_token(self, user):
        if not user.is_authenticated:
            return None
        if self.is_moderator(user):
            return get_video_call_jwt_token(self.id, token_only=False)
        return None

    def get_title(self):
        if self.entity:
            return Entity.objects.get_subclass(id=self.entity.id).title
        return ""


class EntityVideoCallModerator(models.Model):
    user = models.ForeignKey("user.User", on_delete=models.CASCADE)
    video_call = models.ForeignKey(
        "core.EntityVideoCall", on_delete=models.CASCADE, related_name="moderators"
    )
