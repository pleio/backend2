import logging
import re

import requests
from django.db import models
from django.urls import reverse

from core import config

logger = logging.getLogger(__name__)

YOUTUBE_REGEX = re.compile(
    r"^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)",
    re.IGNORECASE,
)

VIMEO_REGEX = re.compile(
    r"(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_-]+)?",
    re.IGNORECASE,
)

KALTURA_REGEX = re.compile(
    r'(https?:\/\/videoleren.nvwa.nl(.*?)(?="|$))', re.IGNORECASE
)


class FeaturedCoverMixin(models.Model):
    """
    FeaturedCoverMixin add to model to implement featured cover fields
    """

    class Meta:
        abstract = True

    featured_image = models.ForeignKey(
        "file.FileFolder", on_delete=models.PROTECT, blank=True, null=True
    )
    featured_video = models.TextField(null=True, blank=True)
    featured_video_thumbnail_url = models.TextField(null=True, blank=True)
    featured_video_title = models.CharField(
        max_length=256, null=True, blank=True, default=""
    )
    featured_position_y = models.IntegerField(default=0, null=False)
    featured_alt = models.CharField(max_length=256, null=True, blank=True, default="")
    featured_caption = models.TextField(null=True, blank=True)

    def is_featured_changed(self, original):
        return self.serialize_featured() != original

    def serialize_featured(self):
        return {
            "video": self.featured_video,
            "videoTitle": self.featured_video_title,
            "videoThumbnailUrl": self.featured_video_thumbnail_url,
            "image": (
                {"guid": self.featured_image.guid} if self.featured_image else None
            ),
            "positionY": self.featured_position_y,
            "alt": self.featured_alt,
            "caption": self.featured_caption,
        }

    def is_featured_image_changed(self, original):
        content = self.serialize_featured()
        original_image = original["image"]
        new_image = content["image"]
        return original_image != new_image

    def unserialize_featured(self, data):
        from entities.file.models import FileFolder

        self.featured_video = data.get("video", "")
        self.featured_video_title = data.get("videoTitle", "")
        self.featured_video_thumbnail_url = data.get("videoThumbnailUrl", "")
        self.featured_position_y = data.get("positionY", 0)
        self.featured_alt = data.get("alt", "")
        self.featured_caption = data.get("caption", "")
        self.featured_image = (
            FileFolder.objects.get(id=data["image"]["guid"])
            if data.get("image", {}).get("guid")
            else None
        )

    @property
    def featured_image_url(self):
        if self.featured_image:
            timestamp = self.featured_image.updated_at.timestamp()
            try:
                latest = self.featured_image.resized_images.latest("updated_at")
                timestamp = latest.updated_at.timestamp()
            except Exception:
                pass

            return "%s?cache=%i" % (reverse("featured", args=[self.id]), int(timestamp))
        return None

    @property
    def featured_image_guid(self):
        if self.featured_image:
            return self.featured_image.guid
        return None

    @staticmethod
    def extract_video_details(video_url):
        if not video_url:
            return None

        def extract_video_id(regex, video_url, group_index):
            match = regex.search(video_url)
            return match.group(group_index) if match else None

        youtube_id = extract_video_id(YOUTUBE_REGEX, video_url, 7)
        if youtube_id:
            return {"type": "youtube", "id": youtube_id}

        vimeo_id = extract_video_id(VIMEO_REGEX, video_url, 1)
        if vimeo_id:
            return {"type": "vimeo", "id": vimeo_id}

        kaltura_id = extract_video_id(KALTURA_REGEX, video_url, 1)
        if kaltura_id:
            id_match = re.search(r"\/([^/]+)\/?$", video_url)
            return {"type": "kaltura", "id": id_match.group(1)} if id_match else None

        return None

    @staticmethod
    def get_video_thumbnail_url(video_url):
        video = FeaturedCoverMixin.extract_video_details(video_url)
        if not video:
            return None

        if video.get("type") == "youtube":
            return f"https://i.ytimg.com/vi/{video.get('id')}/maxresdefault.jpg"
        elif video.get("type") == "vimeo":
            response = requests.get(
                f"https://vimeo.com/api/v2/video/{video.get('id')}.json"
            )
            if response.status_code == 200:
                data = response.json()
                return data[0].get("thumbnail_large")
            else:
                return None
        elif video.get("type") == "kaltura" and config.KALTURA_VIDEO_PARTNER_ID:
            return f"https://api.eu.kaltura.com/p/{config.KALTURA_VIDEO_PARTNER_ID}/thumbnail/entry_id/{video.get('id')}/width/0/height/0"
        else:
            return None
