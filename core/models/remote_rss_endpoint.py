import uuid

import feedparser
import requests
from django.db import models
from django.utils import timezone


class RemoteRssEndpoint(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    url = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)
    last_fetched_at = models.DateTimeField(null=True, blank=True)
    last_content = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.url

    def is_valid(self):
        try:
            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.1234.567 Safari/537.36"
            }

            response = requests.get(self.url, headers=headers, timeout=10)
            assert response.ok

            feed_result = feedparser.parse(response.text)
            assert feed_result.get("bozo_exception") is None

            return True
        except Exception:
            return False

    def latest_last_content(self):
        try:
            result = requests.get(self.url, timeout=10)
            if result.ok:
                return result.text
        except Exception:
            pass
        return self.last_content

    def fetch(self):
        threshold = timezone.now() - timezone.timedelta(minutes=60)
        if not self.last_fetched_at or self.last_fetched_at < threshold:
            self.last_content = self.latest_last_content()
            self.last_fetched_at = timezone.now()
            self.save()
        if self.last_content:
            """
            NOTE: Dates can be in 'published' and/or 'updated' fields.
            """
            return feedparser.parse(self.last_content)["entries"]
        return []
