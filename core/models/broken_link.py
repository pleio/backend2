from django.db import models


class BrokenLink(models.Model):
    class Meta:
        ordering = ("created_at",)

    class Reasons(models.TextChoices):
        DOES_NOT_EXIST = "DOES_NOT_EXIST"
        ARCHIVED = "ARCHIVED"
        DRAFT = "DRAFT"
        SOURCE_MORE_PRIVILEGES = "SOURCE_MORE_PRIVILEGES"

    class LinkTypes(models.TextChoices):
        INTERNAL_RICH_DESCRIPTION = "INTERNAL_RICH_DESCRIPTION"

    link_type = models.CharField(
        max_length=32,
        choices=LinkTypes.choices,
        default=LinkTypes.INTERNAL_RICH_DESCRIPTION,
    )
    source = models.TextField()
    target = models.TextField()
    reason = models.CharField(
        max_length=32,
        choices=Reasons.choices,
        default=Reasons.DOES_NOT_EXIST,
    )
    scan_id = models.UUIDField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
