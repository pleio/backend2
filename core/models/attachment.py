import os

from django.utils.text import slugify


def attachment_path(instance, filename):
    ext = filename.split(".")[-1]
    name = filename.split(".")[0]
    filename = "%s.%s" % (slugify(name), ext)
    return os.path.join("attachments", str(instance.id), filename)
