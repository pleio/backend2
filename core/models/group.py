import logging
import uuid
from copy import deepcopy

from auditlog.registry import auditlog
from django.apps import apps
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext

from core.constances import GROUP_MEMBER_ROLES
from core.lib import ACCESS_TYPE, access_id_to_acl, get_access_ids
from core.models.featured import FeaturedCoverMixin
from core.utils.convert import tiptap_to_text

from .. import config
from ..utils.entity import load_entity_by_id
from .rich_fields import AttachmentMixin, ReplaceAttachments
from .tags import TagsModel

logger = logging.getLogger(__name__)


class GroupManager(models.Manager):
    def is_group_staff(self, id, user):
        try:
            return self.get(id=id).is_staff(user)
        except Exception:
            return False

    def visible(self, user):
        if not user.is_authenticated:
            if config.IS_CLOSED:
                return self.none()
            return self.get_queryset().exclude(is_hidden=True)

        if user.is_authenticated and user.is_site_admin:
            return self.get_queryset()

        hidden_groups_where_users_isnt_a_member = Q()
        hidden_groups_where_users_isnt_a_member.add(Q(is_hidden=True), Q.AND)
        hidden_groups_where_users_isnt_a_member.add(~Q(members__user=user), Q.AND)

        return self.get_queryset().exclude(hidden_groups_where_users_isnt_a_member)


def _default_widget_repository():
    return [
        {
            "type": "groupMembers",
            "settings": [],
        }
    ]


def _default_plugins():
    return ["members"]


def _default_menu():
    return [
        {
            "type": "plugin",
            "id": "members",
        }
    ]


def default_start_page_row_repository(widgets=None, is_submit_updates_enabled=True):
    return [
        {
            "backgroundColor": "",
            "isFullWidth": False,
            "columns": [
                {
                    # column with activity stream
                    "width": [8],
                    "widgets": [
                        {
                            "type": "groupIntroduction",
                            "settings": [],
                        },
                        {
                            "type": "activity",
                            "settings": [
                                {
                                    "key": "allowCreateStatusUpdate",
                                    "value": is_submit_updates_enabled,
                                }
                            ],
                        },
                    ],
                },
                {
                    # column with member-counter widget
                    "width": [4],
                    "widgets": widgets or _default_widget_repository(),
                },
            ],
        }
    ]


class Group(TagsModel, FeaturedCoverMixin, AttachmentMixin):
    class Meta:
        ordering = ["name"]

    objects = GroupManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    owner = models.ForeignKey("user.User", on_delete=models.PROTECT)

    name = models.CharField(max_length=200)

    rich_description = models.JSONField(null=True, blank=True)

    introduction = models.TextField(default="")
    is_introduction_public = models.BooleanField(default=False)
    welcome_message = models.TextField(default="")
    required_fields_message = models.TextField(default="")

    icon = models.ForeignKey(
        "file.FileFolder",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="group_icon",
    )

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    is_featured = models.BooleanField(default=False)
    featured_image = models.ForeignKey(
        "file.FileFolder",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="group_featured_image",
    )

    is_closed = models.BooleanField(default=False)
    is_membership_on_request = models.BooleanField(default=False)
    is_menu_always_visible = models.BooleanField(default=True)
    is_leaving_group_disabled = models.BooleanField(default=False)
    is_auto_membership_enabled = models.BooleanField(default=False)
    is_submit_updates_enabled = models.BooleanField(default=True)
    is_chat_enabled = models.BooleanField(default=False)

    is_hidden = models.BooleanField(default=False)
    is_join_button_enabled = models.BooleanField(default=True)

    auto_notification = models.BooleanField(default=False)
    file_notifications = models.BooleanField(default=False)

    plugins = ArrayField(
        models.CharField(max_length=256), blank=True, default=_default_plugins
    )
    menu = models.JSONField(null=True, default=_default_menu)

    # Automatic membership based on profile fields.
    auto_membership_fields = models.JSONField(default=list)

    content_presets = models.JSONField(default=dict)

    # `widget_repository` is replaced by startpage.row_repository.
    widget_repository = models.JSONField(null=True, default=_default_widget_repository)

    _start_page = models.ForeignKey(
        "core.Entity",
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        db_column="start_page_id",
        related_name="start_page_for",
    )

    @property
    def start_page(self):
        return load_entity_by_id(
            self._start_page_id, ["core.Entity"], fail_if_not_found=False
        )

    @start_page.setter
    def set_start_page(self, start_page):
        self._start_page = start_page

    @property
    def slug(self):
        return slugify(self.name)

    def __str__(self):
        return f"Group[{self.name}]"

    def is_member(self, user):
        if not user.is_authenticated:
            return False
        try:
            return self.members.filter(user=user).exists()
        except ObjectDoesNotExist:
            return False

    def is_owner(self, user):
        return user.is_authenticated and self.owner_id == user.pk

    def is_admin(self, user):
        return self.has_group_role(user, GROUP_MEMBER_ROLES.GROUP_ADMIN) or (
            user.is_authenticated and user.is_site_admin
        )

    def is_news_editor(self, user):
        if self.has_group_role(user, GROUP_MEMBER_ROLES.GROUP_NEWS_EDITOR):
            return True
        return False

    def is_full_member(self, user):
        if not user.is_authenticated:
            return False

        try:
            return self.members.filter(user=user, type="member").exists()
        except ObjectDoesNotExist:
            return False

    def is_pending_member(self, user):
        if not user.is_authenticated:
            return False

        return self.members.filter(user=user, type="pending").exists()

    def is_staff(self, user):
        return self.is_admin(user) or self.is_owner(user)

    def has_group_role(self, user, role):
        if not user.is_authenticated:
            return False

        if user.is_staff:
            return True

        try:
            roles = self.members.get(user=user).roles
        except ObjectDoesNotExist:
            return False

        return role in list(roles)

    def can_write(self, user):
        if not user.is_authenticated:
            return False

        return self.is_owner(user) or self.is_admin(user)

    def can_archive(self, user):
        return self.can_write(user)

    def can_delete(self, user):
        return self.can_archive(user)

    @classmethod
    def can_add(cls, user, group=None):
        if config.LIMITED_GROUP_ADD:
            return user.is_site_admin

        return user.is_authenticated

    def change_ownership(self, changing_user, acting_user):
        # make previous owner admin
        previous_user_membership = self.members.filter(user=self.owner).first()

        changing_user_membership = self.join(changing_user, "member")
        changing_user_membership.save()
        self.owner = changing_user
        self.save()

        if previous_user_membership:
            previous_user_membership.roles = [GROUP_MEMBER_ROLES.GROUP_ADMIN]
            previous_user_membership.save()
        # extra save, to trigger admin_weight
        changing_user_membership.save()

        from core.mail_builders.group_change_ownership import (
            schedule_change_group_ownership_mail,
        )

        schedule_change_group_ownership_mail(
            user=changing_user, sender=acting_user, group=self
        )

    def can_join(self, user):
        if self.can_write(user):
            return True
        if not self.is_closed and not self.is_membership_on_request:
            return True
        if self.invitations.filter_user(user).exists():
            return True
        return False

    def join(self, user, member_type="member"):
        # pylint: disable=unused-variable
        already_member = self.is_full_member(user)
        roles = []

        if member_type == "admin":
            member_type = "member"
            roles = [GROUP_MEMBER_ROLES.GROUP_ADMIN]
        elif member_type == "owner":
            member_type = "member"

        obj, created = self.members.update_or_create(
            user=user,
            defaults={
                "type": member_type,
                "is_notifications_enabled": self.auto_notification,
                "roles": roles,
            },
        )

        # send welcome message for new members
        if obj.type == "member" and not already_member and self.welcome_message:
            from core.mail_builders.group_welcome import schedule_group_welcome_mail

            schedule_group_welcome_mail(group=self, user=user)

        self.invitations.filter_user(user).delete()

        return obj

    def leave(self, user, recursive):
        if recursive and self.subgroups:
            for subgroup in self.subgroups.all():
                if user in subgroup.members.all():
                    subgroup.members.remove(user)
        try:
            return self.members.get(user=user).delete()
        except ObjectDoesNotExist:
            return False

    def set_member_is_notifications_enabled(self, user, is_notifications_enabled):
        member = self.members.filter(user=user).first()
        if member:
            member.is_notifications_enabled = is_notifications_enabled
            member.save()
            return True
        return False

    def set_member_is_notification_direct_mail_enabled(
        self, user, is_notification_direct_mail_enabled
    ):
        member = self.members.filter(user=user).first()
        if member:
            member.is_notification_direct_mail_enabled = (
                is_notification_direct_mail_enabled
            )
            member.save()
            return True
        return False

    def set_member_is_notification_push_enabled(
        self, user, is_notification_push_enabled
    ):
        member = self.members.filter(user=user).first()
        if member:
            member.is_notification_push_enabled = is_notification_push_enabled
            member.save()
            return True
        return False

    def save(self, *args, **kwargs):
        self.update_entity_access()
        super(Group, self).save(*args, **kwargs)

    def update_entity_access(self):
        """
        Update Entity read_access when group is set to 'Closed'
        """
        if self.is_closed and self.id:
            # to prevent cyclic import
            Entity = apps.get_model("core", "Entity")

            filters = Q()
            filters.add(Q(group__id=self.id), Q.AND)
            filters.add(
                Q(read_access__overlap=[ACCESS_TYPE.public, ACCESS_TYPE.logged_in]),
                Q.AND,
            )

            entities = Entity.objects.filter(filters)
            for entity in entities:
                if ACCESS_TYPE.public in entity.read_access:
                    entity.read_access.remove(ACCESS_TYPE.public)
                if ACCESS_TYPE.logged_in in entity.read_access:
                    entity.read_access.remove(ACCESS_TYPE.logged_in)
                if ACCESS_TYPE.group.format(self.id) not in entity.read_access:
                    entity.read_access.append(ACCESS_TYPE.group.format(self.id))
                entity.save()

    @property
    def guid(self):
        return str(self.id)

    @property
    def url(self):
        return "/groups/view/{}/{}".format(self.guid, self.slug)

    @property
    def type_to_string(self):
        return "group"

    @property
    def rich_fields(self):
        return list(self.attachments_from_rich_fields())

    def attachments_from_rich_fields(self):
        if self.rich_description:
            yield self.rich_description
        if self.introduction:
            yield self.introduction

    def lookup_attachments(self):
        yield from super().lookup_attachments()
        if self.icon:
            yield self.icon.guid

    def replace_attachments(self, attachment_map: ReplaceAttachments):
        super().replace_attachments(attachment_map)

        if self.introduction:
            self.introduction = attachment_map.replace(self.introduction)

    def map_rich_text_fields(self, callback):
        # Welcome message is for email, should be treated differently
        # self.welcome_message = callback(self.welcome_message)
        self.rich_description = callback(self.rich_description)
        self.introduction = callback(self.introduction)

    def search_read_access(self):
        return [ACCESS_TYPE.public]

    @property
    def description(self):
        return tiptap_to_text(self.rich_description)

    def disk_size(self):
        from core.models import Entity
        from entities.file.models import FileFolder, FileReference

        file_folder_size = 0
        attachment_size = 0

        ids = (
            FileFolder.objects.filter_files()
            .filter(group=self.id)
            .values_list("id", flat=True)
        )
        f = FileFolder.objects.filter(id__in=ids).aggregate(
            total_size=models.Sum("size")
        )
        if f.get("total_size", 0):
            file_folder_size = f.get("total_size", 0)

        entities = Entity.objects.filter(group=self).values_list("id", flat=True)
        e = (
            FileReference.objects.filter(container_fk__in=[*entities, self.id])
            .exclude(file__id__in=ids)
            .aggregate(total_size=models.Sum("file__size"))
        )
        if e.get("total_size", 0):
            attachment_size = e.get("total_size", 0)

        return file_folder_size + attachment_size

    def serialize(self):
        return {
            "name": self.name,
            "ownerGuid": self.owner.guid if self.owner else "",
            "richDescription": self.rich_description or "",
            "intro": self.introduction or "",
            "isIntroductionPublic": self.is_introduction_public,
            "welcomeMessage": self.welcome_message or "",
            "requiredProfileFieldsMessage": self.required_fields_message or "",
            "icon": self.icon_id,
            "isFeatured": self.is_featured,
            "featured": self.serialize_featured(),
            "isClosed": self.is_closed,
            "isMembershipOnRequest": self.is_membership_on_request,
            "isLeavingGroupDisabled": self.is_leaving_group_disabled,
            "isMenuAlwaysVisible": self.is_menu_always_visible,
            "isAutoMembershipEnabled": self.is_auto_membership_enabled,
            "isChatEnabled": self.is_chat_enabled,
            "isHidden": self.is_hidden,
            "autoNotification": self.auto_notification,
            "plugins": self.plugins,
            "menu": self.menu,
            "startPageGuid": self.start_page.guid if self.start_page else "",
            "defaultTags": sorted(self.content_presets.get("defaultTags") or []),
            "defaultTagCategories": deepcopy(
                self.content_presets.get("defaultTagCategories") or []
            ),
            "tags": sorted(self.tags),
            "tagCategories": self.category_tags,
            "autoMembershipFields": self.auto_membership_fields,
        }

    def get_read_access(self):
        result = set()
        for acid in get_access_ids(self):
            result.update(access_id_to_acl(self, acid["id"]))
        return [*result]

    def create_group_start_page(self, **kwargs):
        from entities.cms.models import Page

        page = Page()
        page.title = gettext("Timeline")
        page.page_type = "campagne"
        page.owner = self.owner
        page.group = self
        page.published = timezone.now()
        page.row_repository = default_start_page_row_repository(**kwargs)
        page.read_access = self.get_read_access()
        page.write_access = [ACCESS_TYPE.user.format(self.owner.guid)]
        page.save()
        return page


class GroupMembership(models.Model):
    class Meta:
        unique_together = ("user", "group")
        ordering = ["group"]

    MEMBER_TYPES = (
        ("member", "Member"),
        ("pending", "Pending"),
    )

    user = models.ForeignKey(
        "user.User", related_name="memberships", on_delete=models.PROTECT
    )
    type = models.CharField(max_length=10, choices=MEMBER_TYPES, default="member")
    group = models.ForeignKey(
        "core.Group", related_name="members", on_delete=models.CASCADE
    )

    is_notifications_enabled = models.BooleanField(default=True)
    is_notification_direct_mail_enabled = models.BooleanField(default=False)
    is_notification_push_enabled = models.BooleanField(default=False)

    roles = ArrayField(models.CharField(max_length=256), blank=True, default=list)

    admin_weight = models.IntegerField(
        default=100,
        null=False,
    )

    def __str__(self):
        return "GroupMembership[{}:{}:{}]".format(
            self.user.email, self.type, self.group.name
        )

    def save(self, *args, **kwargs):
        self.admin_weight = self.get_admin_weight()
        super().save(*args, **kwargs)

    def get_admin_weight(self):
        """
        Weight of the membership on administration pages. Owner on top.
        """
        if self.group.owner == self.user:
            return 1
        elif GROUP_MEMBER_ROLES.GROUP_ADMIN in self.roles:
            return 2
        elif self.type == "member":
            return 3
        return 100

    def index_instance(self):
        return self.user

    created_at = models.DateTimeField(default=timezone.now)


class GroupInvitationQuerySet(models.QuerySet):
    def filter_user(self, user):
        return self.filter(Q(email=user.email) | Q(invited_user=user))


class GroupInvitation(models.Model):
    class Meta:
        unique_together = ("invited_user", "group")

    objects = GroupInvitationQuerySet.as_manager()

    group = models.ForeignKey(
        "core.Group", related_name="invitations", on_delete=models.CASCADE
    )
    invited_user = models.ForeignKey(
        "user.User",
        related_name="invitation",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    email = models.EmailField(max_length=255, blank=True, null=True)

    code = models.CharField(max_length=36)

    role = models.CharField(max_length=32, blank=True, null=True)
    acting_user = models.ForeignKey(
        "user.User",
        related_name="invited_by_me",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "GroupInvitation[]"


class Subgroup(models.Model):
    """
    A group of users within a group
    """

    NOT_SET_ACCESS_ID = "not-set"

    name = models.CharField(max_length=512)
    members = models.ManyToManyField("user.User", related_name="subgroups")
    group = models.ForeignKey(
        "core.Group", related_name="subgroups", on_delete=models.CASCADE
    )
    access_id = models.CharField(max_length=64, default=NOT_SET_ACCESS_ID)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.access_id == self.NOT_SET_ACCESS_ID:
            Subgroup.objects.filter(id=self.id).update(access_id=str(10000 + self.id))
            self.refresh_from_db()

    def __str__(self):
        return f"Subgroup[{self.name}]"


class GroupProfileFieldSetting(models.Model):
    """
    Group settings for ProfileField
    """

    group = models.ForeignKey(
        "core.Group", related_name="profile_field_settings", on_delete=models.CASCADE
    )
    profile_field = models.ForeignKey(
        "core.ProfileField", related_name="group_settings", on_delete=models.CASCADE
    )
    show_field = models.BooleanField(default=False)
    is_required = models.BooleanField(default=False)

    def clean(self):
        if self.profile_field.field_type == "html_field" and self.show_field:
            msg = "Long text fields are not allowed to display on the member page."
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(GroupProfileFieldSetting, self).save(*args, **kwargs)


auditlog.register(Group)
auditlog.register(GroupMembership)
auditlog.register(Subgroup)
