from django.db import models
from django.utils import timezone

from core.utils.json import JSONDecoder, JSONEncoder


class SiteInvitation(models.Model):
    email = models.EmailField(max_length=255, unique=True)
    code = models.CharField(max_length=36)

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)


class SiteAccessRequestQuerySet(models.QuerySet):
    def filter_unprocessed(self):
        return self.filter(processed=False)

    def filter_pending(self):
        return self.filter(processed=True, accepted=True, user_created__isnull=True)

    def filter_accepted(self):
        return self.filter(processed=True, accepted=True, user_created__isnull=False)

    def filter_rejected(self):
        return self.filter(processed=True, accepted=False)

    def last_modified_on_top(self):
        return self.order_by("-updated_at")


class SiteAccessRequestManager(models.Manager):
    def get_queryset(self):
        return SiteAccessRequestQuerySet(self.model, using=self._db)

    def filter_status(self, status=None):
        match status:
            case "unprocessed":
                return self.get_queryset().filter_unprocessed()
            case "pending":
                return self.get_queryset().filter_pending()
            case "accepted":
                return self.get_queryset().filter_accepted()
            case "rejected":
                return self.get_queryset().filter_rejected()
            case None:
                return self.get_queryset()

        return self.get_queryset().filter_rejected()

    def last_modified_on_top(self):
        return self.get_queryset().last_modified_on_top()


class SiteAccessRequest(models.Model):
    class Meta:
        ordering = ("created_at",)

    objects = SiteAccessRequestManager()

    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=100)

    claims = models.JSONField(null=True, blank=True)

    accepted = models.BooleanField(default=False)
    processed = models.BooleanField(default=False)

    processed_by = models.ForeignKey(
        "user.User",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="processed_site_access_requests",
    )

    user_created = models.ForeignKey(
        "user.User",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="site_access_requests",
    )

    reason = models.TextField(null=True, blank=True)

    next = models.CharField(max_length=1024, null=True, blank=True)
    profile = models.JSONField(
        null=True, blank=True, encoder=JSONEncoder, decoder=JSONDecoder
    )

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    @property
    def status(self):
        if self.accepted and not self.user_created:
            return "pending"
        if self.accepted and self.user_created:
            return "accepted"
        if not self.processed:
            return "unprocessed"
        if not self.accepted:
            return "rejected"

    def send_notifications(self):
        from core.constances import USER_ROLES
        from core.mail_builders.site_access_request import (
            schedule_site_access_request_mail,
        )
        from user.models import User

        for admin in User.objects.filter(roles__contains=[USER_ROLES.ADMIN]):
            schedule_site_access_request_mail(name=self.name, admin=admin)


class SiteStat(models.Model):
    class Meta:
        ordering = ("created_at",)

    STAT_TYPES = (
        ("DISK_SIZE", "DISK_SIZE"),
        ("DB_SIZE", "DB_SIZE"),
    )

    stat_type = models.CharField(max_length=16, choices=STAT_TYPES, default="DISK_SIZE")

    value = models.BigIntegerField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
