import uuid

from auditlog.registry import auditlog
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q
from django.utils import timezone

from core.lib import datetime_utciso, get_model_name, tenant_schema

from .. import config
from ..entity_access import (
    AccessTestBase,
    DenyAnonymousVisitors,
    EntityAccessResult,
    GrantAdministrators,
)
from ..utils.convert import truncate_rich_description
from ..utils.entity import load_entity_by_id
from .mixin import CanWriteMixin, CommentMixin, VoteMixin
from .rich_fields import AttachmentMixin, MentionMixin


class CommentQuerySet(models.QuerySet):
    def visible(self, user=None):
        if (
            not config.COMMENT_MODERATION_ENABLED
            or not user
            or (user.is_authenticated and user.is_editor)
        ):
            # System (crontask or alike) or administrator.
            return self.all()

        # Website visitors; known or anonymous.
        approved_comments = Q(comment_moderation_request__isnull=True)
        user_is_owner = Q(owner=user)
        if not user.is_authenticated:
            return self.filter(approved_comments)
        return self.filter(approved_comments | user_is_owner)

    def filter_moderated(self):
        if config.COMMENT_MODERATION_ENABLED:
            return self.filter(comment_moderation_request__isnull=True)
        return self.all()


class CommentManager(models.Manager):
    def get_queryset(self):
        return CommentQuerySet(self.model, using=self._db)

    def visible(self, user=None):
        return self.get_queryset().visible(user)

    def filter_moderated(self):
        return self.get_queryset().filter_moderated()

    def create(self, *args, **kwargs):
        container = kwargs.get("container")
        owner = kwargs.get("owner")
        if isinstance(container, Comment):
            container = container.get_root_container()

        if (
            container
            and (container.type_to_string in config.REQUIRE_COMMENT_MODERATION_FOR)
            and config.COMMENT_MODERATION_ENABLED
            and owner
            and not owner.is_editor
            and not (container.group and container.group.is_staff(owner))
        ):
            request, created = CommentModerationRequest.objects.get_or_create(
                entity=container
            )
            kwargs["comment_moderation_request"] = request

        return super().create(*args, **kwargs)


class Comment(CanWriteMixin, VoteMixin, MentionMixin, AttachmentMixin, CommentMixin):
    class Meta:
        ordering = ["-created_at"]

    objects = CommentManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    owner = models.ForeignKey(
        "user.User", on_delete=models.PROTECT, null=True, blank=True
    )

    name = models.CharField(max_length=256, null=True, blank=True)
    email = models.CharField(max_length=256, null=True, blank=True)
    comment_moderation_request = models.ForeignKey(
        "core.CommentModerationRequest",
        on_delete=models.SET_NULL,
        related_name="comments",
        null=True,
        blank=True,
    )

    rich_description = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.UUIDField(default=uuid.uuid4)
    container = GenericForeignKey("content_type", "object_id")

    @property
    def guid(self):
        return str(self.id)

    @property
    def url(self):
        if self.container and hasattr(self.container, "url"):
            return self.container.url
        return False

    @property
    def rich_fields(self):
        return [self.rich_description]

    @property
    def excerpt(self):
        return truncate_rich_description(self.rich_description)

    @property
    def group(self):
        if self.container and hasattr(self.container, "group"):
            return self.container.group

        return None

    @property
    def type_to_string(self):
        return "comment"

    @property
    def title(self):
        if self.container and hasattr(self.container, "title"):
            return self.container.title

        return ""

    def __str__(self):
        return f"Comment[{self.guid}]"

    def save(self, *args, **kwargs):
        created = self._state.adding
        super(Comment, self).save(*args, **kwargs)
        if created:
            self.create_notifications()

    def create_notifications(self):
        """if comment is added to content, create a notification for all users following the content"""
        from core.tasks import create_notification

        if self.owner:
            sender = self.owner.id
        else:
            return

        container = self.get_root_container()
        if container.update_last_action(self.created_at):
            container.save()

        if hasattr(container, "add_follow"):
            container.add_follow(self.owner)

        create_notification.delay(
            tenant_schema(),
            "commented",
            get_model_name(container),
            container.id,
            sender,
        )

    def _get_write_validators(self, user):
        return [
            DenyAnonymousVisitors(user, entity=self),
            GrantAdministrators(user, entity=self),
            DenyNonCommentModerator(user, entity=self),
            GrantContentOwner(user, entity=self),
        ]

    def is_moderation_enabled(self):
        return (
            config.COMMENT_MODERATION_ENABLED
            and getattr(self.get_root_container(), "type_to_string", "")
            in config.REQUIRE_COMMENT_MODERATION_FOR
        )

    def can_archive(self, user):
        if self.is_moderation_enabled():
            return user.is_authenticated and (user.is_editor or user == self.owner)
        return self.can_write(user)

    def can_read(self, user):
        if (
            config.COMMENT_MODERATION_ENABLED
            and self.container.type_to_string in config.REQUIRE_COMMENT_MODERATION_FOR
            and self.comment_moderation_request_id
            and not (user == self.owner or user.is_editor)
        ):
            return False
        if self.container and hasattr(self.container, "can_read"):
            return self.container.can_read(user)
        return False

    def get_root_container(self, parent=None):
        if not parent:
            parent = self.container
        if isinstance(parent, Comment):
            return self.get_root_container(parent.container)
        return parent

    def index_instance(self):
        return self.get_root_container()

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)

    def serialize(self):
        return {
            "ownerGuid": self.owner.guid if self.owner else "",
            "name": self.owner.name if self.owner and not self.name else self.name,
            "email": self.owner.email if self.owner and not self.email else self.email,
            "richDescription": self.rich_description,
            "timeCreated": datetime_utciso(self.created_at),
            "containerGuid": str(self.container.id) if self.container else "",
        }

    def get_read_access(self):
        if hasattr(self.container, "read_access"):
            return self.container.read_access
        if hasattr(self.container, "get_read_access"):
            return self.container.get_read_access()
        return []


class CommentRequest(models.Model):
    code = models.CharField(max_length=36)

    name = models.CharField(max_length=256, null=True, blank=True)
    email = models.CharField(max_length=256, null=True, blank=True)

    rich_description = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(default=timezone.now)

    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.UUIDField(default=uuid.uuid4)
    container = GenericForeignKey("content_type", "object_id")

    def __str__(self):
        return f"CommentRequest[{self.name} commented on {self.container}]"


class CommentModerationRequestQuerySet(models.QuerySet):
    @staticmethod
    def _is_assigned():
        return Q(assigned_to__isnull=False, assigned_until__gt=timezone.now())

    @staticmethod
    def _has_comments():
        return Q(
            id__in=CommentModerationRequest.objects.filter(
                comments__isnull=False
            ).values_list("id")
        )

    def filter_open_requests(self):
        return self.filter(self._has_comments()).exclude(self._is_assigned())

    def filter_assigned_requests(self):
        return self.filter(self._is_assigned() & self._has_comments())

    def filter_closed_requests(self):
        return self.exclude(self._has_comments())

    def visible(self, user):
        if not user.is_authenticated:
            return self.none()
        if user.is_editor:
            return self.all()
        return self.filter_owner(user.id)

    def filter_owner(self, user_id):
        items = self.filter(comments__owner_id=user_id).values_list("id", flat=True)
        return self.filter(id__in=items)


class CommentModerationManager(models.Manager):
    def get_queryset(self):
        return CommentModerationRequestQuerySet(self.model, using=self._db)

    def is_pending_moderation(self, node):
        return self.get_queryset().filter(comments=node).exists()

    def get_or_create(self, defaults=None, **kwargs):
        if kwargs.get("entity"):
            kwargs["_entity_id"] = kwargs["entity"].id
            del kwargs["entity"]
        return super().get_or_create(defaults=defaults, **kwargs)

    def filter_open_requests(self):
        return self.get_queryset().filter_open_requests()

    def filter_assigned_requests(self):
        return self.get_queryset().filter_assigned_requests()

    def filter_closed_requests(self):
        return self.get_queryset().filter_closed_requests()

    def visible(self, user):
        return self.get_queryset().visible(user)

    def filter_owner(self, user_id):
        return self.get_queryset().filter_owner(user_id)


class CommentModerationRequest(models.Model):
    objects = CommentModerationManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(default=timezone.now)
    assigned_to = models.ForeignKey(
        "user.User",
        on_delete=models.PROTECT,
        related_name="comment_moderation_requests",
        null=True,
        blank=True,
    )
    assigned_until = models.DateTimeField(null=True, blank=True)
    _entity = models.OneToOneField(
        "core.Entity",
        on_delete=models.PROTECT,
        related_name="comment_moderation_request",
    )

    @property
    def entity(self):
        return load_entity_by_id(self._entity_id, ["core.Entity"])

    @entity.setter
    def entity(self, value):
        self._entity = value

    @property
    def guid(self):
        return str(self.id)

    @property
    def is_closed(self):
        return self.comments.count() == 0

    @property
    def is_open(self):
        return not self.is_assigned and not self.is_closed

    @property
    def is_assigned(self):
        return (
            self.assigned_to
            and (self.assigned_until is None or self.assigned_until > timezone.now())
            and not self.is_closed
        )

    def assign_to(self, user):
        self.assigned_to = user
        self.assigned_until = timezone.now() + timezone.timedelta(hours=1)

    def reset_assignee(self):
        self.assigned_to = None
        self.assigned_until = None

    def reset_if_no_comments(self):
        if not self.comments.exists():
            self.reset_assignee()
            self.save()


class GrantContentOwner(AccessTestBase):
    def test(self):
        if self.user == self.entity.owner:
            raise EntityAccessResult(True)


class DenyNonCommentModerator(AccessTestBase):
    def test(self):
        if self.entity.is_moderation_enabled() and self.user.is_authenticated:
            raise EntityAccessResult(self.user.is_editor)


auditlog.register(Comment)
