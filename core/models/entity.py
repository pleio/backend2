import logging
import uuid

from django.contrib.auth.models import AnonymousUser
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q
from django.utils import timezone
from model_utils.managers import InheritanceManager
from notifications.models import Notification

from core import config
from core.constances import ENTITY_STATUS, USER_ROLES
from core.entity_access import (
    DenyAnonymousVisitors,
    DenyNonContentModerators,
    GrantAdministrators,
    GrantAuthorizedUsers,
    GrantContentModerators,
    GrantGroupOwnersAndAdmins,
)
from core.lib import (
    acl_overlap,
    allow_create,
    datetime_isoformat,
    datetime_utciso,
    get_access_id,
    get_acl,
    get_model_name,
    tenant_schema,
)
from core.models.mixin import CanWriteMixin
from core.models.shared import read_access_default, write_access_default
from core.models.tags import TagsModel
from core.utils.convert import tiptap_to_text

logger = logging.getLogger(__name__)


class EntityManager(InheritanceManager):
    def status_published(self, filter_status, user=None):
        user = user or AnonymousUser()
        public_possible = False
        acl = get_acl(user)
        query = Q()

        if user.is_authenticated and user.is_site_admin:
            access = Q()
        else:
            access = Q(read_access__overlap=list(acl))

        if ENTITY_STATUS.PUBLISHED in filter_status:
            not_archived = Q(is_archived__isnull=True) | Q(is_archived=False)
            query.add(Q(published__lte=timezone.now()) & access & not_archived, Q.OR)
            public_possible = True

        if ENTITY_STATUS.ARCHIVED in filter_status:
            query.add(Q(is_archived=True) & access, Q.OR)
            public_possible = True

        if not user.is_authenticated and not public_possible:
            return self.get_queryset().none()

        if ENTITY_STATUS.DRAFT in filter_status:
            draft_query = Q(Q(published__gt=timezone.now()) | Q(published__isnull=True))
            if not user.has_role(USER_ROLES.ADMIN):
                draft_query.add(Q(owner=user), Q.AND)

            query.add(draft_query, Q.OR)

        return self.get_queryset().filter(query)

    def draft(self, user):
        qs = self.get_queryset()
        if not user.is_authenticated:
            return qs.none()

        qs = qs.filter(Q(published__gt=timezone.now()) | Q(published__isnull=True))

        if user.is_site_admin:
            return qs

        if config.CONTENT_MODERATION_ENABLED and user.is_editor:
            return qs

        return qs.filter(owner=user)

    def archived(self, user):
        qs = self.get_queryset().filter(is_archived=True)

        if user.is_authenticated and user.is_site_admin:
            return qs

        return qs.filter(read_access__overlap=list(get_acl(user)))

    def published(self):
        qs = self.get_queryset()
        return qs.filter(published__lte=timezone.now(), is_archived=False)

    def visible(self, user):
        qs = self.published()
        if user.is_authenticated and user.is_site_admin:
            return qs

        return qs.filter(read_access__overlap=list(get_acl(user)))

    def visible_all(self, user):
        qs = self.get_queryset()
        if user.is_authenticated and user.is_site_admin:
            return qs

        return qs.filter(read_access__overlap=list(get_acl(user)))


class Entity(CanWriteMixin, TagsModel):
    class Meta:
        ordering = ["published"]

    objects = EntityManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    owner = models.ForeignKey("user.User", on_delete=models.PROTECT)
    group = models.ForeignKey(
        "core.Group", on_delete=models.CASCADE, blank=True, null=True
    )
    read_access = ArrayField(
        models.CharField(max_length=64), blank=True, default=read_access_default
    )
    write_access = ArrayField(
        models.CharField(max_length=64), blank=True, default=write_access_default
    )
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    last_action = models.DateTimeField(default=timezone.now)

    published = models.DateTimeField(default=timezone.now, blank=True, null=True)
    is_archived = models.BooleanField(default=False)
    schedule_archive_after = models.DateTimeField(null=True, blank=True)
    schedule_delete_after = models.DateTimeField(null=True, blank=True)

    input_language = models.CharField(max_length=8, blank=True, null=True)
    is_translation_enabled = models.BooleanField(default=True)

    _tag_summary = ArrayField(
        models.CharField(max_length=256), blank=True, default=list, db_column="tags"
    )

    suggested_items = ArrayField(
        models.UUIDField(default=uuid.uuid4), blank=True, null=True
    )
    notifications_created = models.BooleanField(default=False)

    is_pinned = models.BooleanField(default=False)
    is_featured = models.BooleanField(default=False)
    is_recommended = models.BooleanField(default=False)
    is_recommended_in_search = models.BooleanField(default=False)

    background_color = models.CharField(max_length=32, blank=True, null=True)

    is_form_enabled = models.BooleanField(default=False)
    form_specs = models.JSONField(default=dict, blank=True)

    def get_form_fields(self):
        from core.utils.entity_form import EntityFormWrapper

        form = EntityFormWrapper(self.form_specs)
        return form.fields

    @property
    def hide_owner(self):
        return getattr(self, "type_to_string", "") in config.HIDE_CONTENT_OWNER

    @property
    def guid(self):
        return str(self.id)

    @property
    def slug(self):
        raise NotImplementedError()

    @property
    def status_published(self):
        if self.is_archived:
            return ENTITY_STATUS.ARCHIVED

        if self.published and self.published < timezone.now():
            return ENTITY_STATUS.PUBLISHED

        return ENTITY_STATUS.DRAFT

    def status_published_at(self, date):
        if self.schedule_archive_after and self.schedule_archive_after <= date:
            return ENTITY_STATUS.ARCHIVED
        if self.published and self.published <= date:
            return ENTITY_STATUS.PUBLISHED
        return ENTITY_STATUS.DRAFT

    @property
    def last_seen(self):
        try:
            view_count = EntityViewCount.objects.get(entity_id=self.guid)
            return view_count.updated_at
        except ObjectDoesNotExist:
            return None

    def save(self, *args, **kwargs):
        created = self._state.adding
        self.input_language = self.input_language or config.LANGUAGE
        if not created:
            self.cleanup_notifications()
        super(Entity, self).save(*args, **kwargs)
        if created:
            self.send_notifications_on_create()

    def cleanup_notifications(self):
        """Delete notifications when read_access changed and user can not read entity"""
        if not self.group or not self.id:
            return

        entity = Entity.objects.filter(id=self.id).first()
        if entity and entity.read_access != self.read_access:
            for notification in Notification.objects.filter(
                action_object_object_id=self.id
            ):
                if not self.can_read(notification.recipient):
                    notification.delete()

    def send_notifications_on_create(self):
        """Adds notification for group members if entity in group is created

        If an entity is created in a group, a notification is added for all group
        member in this group.

        """
        from core.models.mixin import NotificationMixin
        from core.tasks import create_notification

        if (
            not self.is_archived
            and issubclass(type(self), NotificationMixin)
            and self.group
        ):
            if (not self.published) or (
                datetime_isoformat(self.published) > datetime_isoformat(timezone.now())
            ):
                return

            create_notification.delay(
                tenant_schema(), "created", get_model_name(self), self.id, self.owner.id
            )
        else:
            self.notifications_created = True
            self.save()

    def has_revisions(self):
        return False

    def can_read(self, user):
        if user.is_authenticated and user.is_site_admin:
            return True

        if self.group and self.group.is_closed and not self.group.is_full_member(user):
            return False

        return len(get_acl(user) & set(self.read_access)) > 0

    def _get_write_validators(self, user):
        return [
            DenyAnonymousVisitors(user, entity=self),
            GrantAdministrators(user, entity=self),
            GrantContentModerators(user, entity=self),
            DenyNonContentModerators(user, entity=self),
            GrantGroupOwnersAndAdmins(user, entity=self),
            GrantAuthorizedUsers(user, entity=self),
        ]

    @classmethod
    def can_add(cls, user, group=None):
        if group:
            return group.is_full_member(user) or group.is_admin(user)

        return user.is_authenticated and (allow_create(cls) or user.is_site_admin)

    def is_content_moderation_enabled(self):
        return (
            config.CONTENT_MODERATION_ENABLED
            and getattr(self, "type_to_string", "")
            in config.REQUIRE_CONTENT_MODERATION_FOR
        )

    def can_archive(self, user):
        if self.is_content_moderation_enabled():
            return user.is_authenticated and (
                user.is_editor or acl_overlap(user, self.write_access)
            )
        return self.can_write(user)

    def can_delete(self, user):
        return self.can_archive(user)

    def get_read_access(self):
        return self.read_access

    def update_last_action(self, new_value):
        if new_value and new_value > self.last_action:
            self.last_action = new_value
            return True
        return False

    def description_index_value(self):
        try:
            tt_description = getattr(self, "rich_description", None)
            assert tt_description, "no description found"
            txt_description = tiptap_to_text(tt_description, with_links=True)
            assert txt_description, "no text description found"

            return (
                txt_description + "\n\n" + super().description_index_value()
            ).strip()
        except (AttributeError, AssertionError):
            pass

        return super().description_index_value().strip()

    def serialize(self):
        return {
            "ownerGuid": self.owner.guid if self.owner else "",
            "inputLanguage": self.input_language,
            "groupGuid": self.group.guid if self.group else "",
            "statusPublished": self.status_published.value,
            "timePublished": datetime_utciso(self.published),
            "timeCreated": datetime_utciso(self.created_at),
            "scheduleArchiveEntity": datetime_utciso(self.schedule_archive_after),
            "scheduleDeleteEntity": datetime_utciso(self.schedule_delete_after),
            "accessId": get_access_id(self.read_access),
            "writeAccessId": get_access_id(self.write_access),
            "suggestedItems": [str(i) for i in self.suggested_items or []],
            "tags": sorted(self.tags),
            "tagCategories": self.category_tags,
            "isPinned": self.is_pinned,
            "isFeatured": self.is_featured,
            "isRecommended": self.is_recommended,
            "isRecommendedInSearch": self.is_recommended_in_search,
        }


class EntityView(models.Model):
    entity = models.ForeignKey(
        "core.Entity", on_delete=models.CASCADE, related_name="views"
    )
    viewer = models.ForeignKey(
        "user.User",
        on_delete=models.CASCADE,
        related_name="viewed_entities",
        null=True,
        blank=True,
        default=None,
    )
    session = models.TextField(max_length=128, null=True, blank=True, default=None)
    created_at = models.DateTimeField(default=timezone.now)


class EntityViewCount(models.Model):
    entity = models.OneToOneField(
        "core.Entity", on_delete=models.CASCADE, related_name="view_count"
    )
    views = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)


def _publish_request_file_filter():
    from entities.file.models import FileFolder

    return Q(
        _entity__filefolder__isnull=False,
        _entity__filefolder__type=FileFolder.Types.FILE,
    )


class PublishRequestQuerySet(models.QuerySet):
    PUBLISHED = Q(_entity__published__isnull=False)
    ASSIGNED = Q(assigned_to__isnull=False)

    def exclude_status(self, status):
        if status == "published":
            return self.exclude(self.PUBLISHED)
        if status == "assigned":
            return self.exclude(self.ASSIGNED & ~self.PUBLISHED)
        if status == "open":
            return self.exclude(~self.PUBLISHED & ~self.ASSIGNED)
        return self.all()

    def filter_status(self, status):
        if status == "published":
            return self.filter(self.PUBLISHED)
        if status == "assigned":
            return self.filter(self.ASSIGNED & ~self.PUBLISHED)
        if status == "open":
            return self.filter(~self.PUBLISHED & ~self.ASSIGNED)
        return self.none()

    def filter_files(self):
        return self.filter(_publish_request_file_filter())

    def exclude_files(self):
        return self.exclude(_publish_request_file_filter())

    def visible(self, user):
        if user.is_authenticated and user.is_editor:
            return self
        return self.filter_owner(user.id)

    def filter_owner(self, user_id):
        return self.filter(_entity__owner_id=user_id)


class PublishRequestManager(models.Manager):
    """
    Separate manager for testing purposes.
    """

    def get_queryset(self):
        return PublishRequestQuerySet(self.model, using=self._db)

    def exclude_status(self, status):
        return self.get_queryset().exclude_status(status)

    def filter_status(self, status):
        return self.get_queryset().filter_status(status)

    def filter_files(self):
        return self.get_queryset().filter_files()

    def exclude_files(self):
        return self.get_queryset().exclude_files()

    def visible(self, user):
        return self.get_queryset().visible(user)

    def filter_owner(self, user_id):
        return self.get_queryset().filter_owner(user_id)


class PublishRequest(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    _entity = models.ForeignKey(
        "core.Entity",
        on_delete=models.CASCADE,
        related_name="publish_requests",
        null=True,
        blank=True,
    )
    assigned_to = models.ForeignKey(
        "user.User", on_delete=models.CASCADE, null=True, blank=True
    )
    time_published = models.DateTimeField(default=timezone.now)

    objects = PublishRequestManager()

    @property
    def entity(self):
        return Entity.objects.filter(id=self._entity_id).select_subclasses().first()

    @entity.setter
    def entity(self, entity):
        self._entity = entity

    @property
    def status(self):
        if not self.entity:
            return None
        if self.entity.published:
            return "published"
        if self.assigned_to_id:
            return "assigned"
        return "open"

    @property
    def author(self):
        return self.entity.owner

    def assign_request(self, user):
        self.assigned_to = user
        self.save()

    def reset_request(self):
        self.assigned_to = None
        self.save()

    def confirm_request(self):
        entity = self.entity
        entity.published = self.time_published
        entity.save()

    def reject_request(self):
        self.delete()

    @property
    def guid(self):
        return str(self.id)

    def __repr__(self):
        return f"<PublishRequest [{self.entity}]>"
