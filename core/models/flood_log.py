import logging
import math

from django.conf import settings
from django.db import models
from django.utils import timezone

from core.exceptions import FloodOverflowError

logger = logging.getLogger(__name__)


def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip


def get_user_identifier_from_request(request):
    if hasattr(request, "user") and request.user.is_authenticated:
        return request.user.email
    elif hasattr(request, "session") and request.session.get("onboarding_claims"):
        return request.session.get("onboarding_claims").get("email")
    return None


def flood_token(request, user):
    return "%s|%s" % (get_client_ip(request), user.id)


class FloodLogManager(models.Manager):
    def cleanup_expired_records(self):
        overflow = self.get_queryset().filter(
            expires__lt=timezone.now() - timezone.timedelta(days=60)
        )
        overflow.delete()

    def reset_target(self, request, target):
        overflow = self.get_queryset().filter(
            target=target,
            ip=get_client_ip(request),
            user_identifier=get_user_identifier_from_request(request),
        )
        overflow.delete()

    def assert_not_blocked(self, request, target):
        records = self.get_queryset().filter(
            target=target,
            ip=get_client_ip(request),
            expires__gte=timezone.now(),
            user_identifier=get_user_identifier_from_request(request),
        )
        if records.count() >= settings.FLOOD_THRESHOLD:
            expire_in_minutes = math.ceil(
                (records.last().expires - timezone.now()).seconds / 60
            )
            msg = "Flood overflow"
            raise FloodOverflowError(msg, expire_in_minutes=expire_in_minutes)

        self.assert_ip_not_blocked(request)

    def assert_ip_not_blocked(self, request):
        ip = get_client_ip(request)
        records = self.get_queryset().filter(ip=ip, expires__gte=timezone.now())
        if records.count() >= settings.FLOOD_IP_THRESHOLD:
            expire_in_minutes = math.ceil(
                (records.last().expires - timezone.now()).seconds / 60
            )
            msg = "IP Flood overflow"
            raise FloodOverflowError(msg, expire_in_minutes=expire_in_minutes)

    def add_record(self, request, target):
        records = self.get_queryset().filter(
            target=target,
            ip=get_client_ip(request),
            user_identifier=get_user_identifier_from_request(request),
        )
        next = records.count() + 1

        if next > settings.FLOOD_THRESHOLD:
            idx = next - settings.FLOOD_THRESHOLD
            if idx < len(settings.FLOOD_EXPIRE):
                flood_expire = settings.FLOOD_EXPIRE[idx]
            else:
                flood_expire = settings.FLOOD_EXPIRE[-1]
        else:
            flood_expire = settings.FLOOD_EXPIRE[0]

        expire_time = timezone.now() + timezone.timedelta(minutes=flood_expire)

        # update existing records expire time
        records.update(expires=expire_time)

        self.create(
            target=target,
            ip=get_client_ip(request),
            expires=expire_time,
            user_identifier=get_user_identifier_from_request(request),
        )


class FloodLog(models.Model):
    objects = FloodLogManager()

    ip = models.GenericIPAddressField()
    user_identifier = models.EmailField(max_length=255, blank=True, null=True)
    target = models.CharField(max_length=128)
    expires = models.DateTimeField()

    def __str__(self):
        return "%s@%s" % (self.target, self.ip)
