from django import forms


def create_select_field(profile_field, attributes):
    return forms.ChoiceField(
        label=profile_field.name,
        choices=profile_field.field_choices,
        required=profile_field.is_mandatory,
        widget=forms.Select(
            attrs={
                "class": "form__input",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def create_multi_select_field(profile_field, attributes):
    return forms.MultipleChoiceField(
        label=profile_field.name,
        choices=profile_field.field_choices,
        required=profile_field.is_mandatory,
        widget=forms.SelectMultiple(
            attrs={
                "class": "form__input",
                "size": "5",
                "style": "height:100px",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def create_date_field(profile_field, attributes):
    return forms.DateField(
        label=profile_field.name,
        required=profile_field.is_mandatory,
        input_formats=("%d-%m-%Y", "%Y-%m-%d"),
        widget=forms.DateInput(
            attrs={
                "type": "date",
                "class": "form__input",
                "placeholder": "dd-mm-jjjj",
                **maybe_autocomplete(profile_field),
                **attributes,
            },
            format="%d-%m-%Y",
        ),
    )


def create_html_field(profile_field, attributes):
    return forms.CharField(
        label=profile_field.name,
        required=profile_field.is_mandatory,
        widget=forms.Textarea(
            attrs={
                "class": "form__input",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def create_text_field(profile_field, attributes):
    return forms.CharField(
        label=profile_field.name,
        required=profile_field.is_mandatory,
        widget=forms.TextInput(
            attrs={
                "class": "form__input",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def create_email_field(profile_field, attributes):
    return forms.EmailField(
        label=profile_field.name,
        required=profile_field.is_mandatory,
        widget=forms.EmailInput(
            attrs={
                "class": "form__input",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def create_url_field(profile_field, attributes):
    return forms.URLField(
        label=profile_field.name,
        required=profile_field.is_mandatory,
        widget=forms.URLInput(
            attrs={
                "class": "form__input",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def create_tel_field(profile_field, attributes):
    return forms.IntegerField(
        label=profile_field.name,
        required=profile_field.is_mandatory,
        widget=forms.NumberInput(
            attrs={
                "class": "form__input",
                **maybe_autocomplete(profile_field),
                **attributes,
            }
        ),
    )


def maybe_autocomplete(profile_field):
    if _value := profile_field.autocomplete:
        return {"autocomplete": _value}
    else:
        return {}
