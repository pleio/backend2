from __future__ import unicode_literals

import logging

from django import forms
from django.utils.translation import gettext_lazy as _

from core import config
from core.exceptions import FloodOverflowError
from core.forms.utils.profile_fields import (
    create_date_field,
    create_email_field,
    create_html_field,
    create_multi_select_field,
    create_select_field,
    create_tel_field,
    create_text_field,
    create_url_field,
)
from core.models import ProfileField
from core.utils.datetime import force_date_format

logger = logging.getLogger(__name__)


class OnboardingForm(forms.Form):
    def __init__(self, *args, request=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._request = request
        self.profile_fields = self.get_profile_fields()

        for profile_field in self.profile_fields:
            self.add_profile_field(profile_field)

    def clean(self):
        cleaned_data = super().clean()
        for field in self.profile_fields:
            value = cleaned_data.get(field.guid)
            try:
                if not field.validate(value, self._request):
                    self.add_error(field.guid, _("Provide a valid value."))
            except FloodOverflowError as e:
                self.add_error(
                    field.guid,
                    _("To many failed attempts, try again in %(minutes)s minutes")
                    % {"minutes": e.expire_in_minutes},
                )

    @staticmethod
    def get_profile_fields():
        if config.ONBOARDING_ENABLED:
            return list(ProfileField.objects.filter_onboarding_fields())
        return []

    def add_field(self, profile_field, factory):
        self.fields[profile_field.guid] = factory(
            profile_field, self.widget_attributes()
        )

    def widget_attributes(self):
        return {}

    def add_profile_field(self, profile_field):
        if profile_field.field_type == "select_field":
            return self.add_select_field(profile_field)
        elif profile_field.field_type == "multi_select_field":
            return self.add_multi_select_field(profile_field)
        elif profile_field.field_type == "date_field":
            return self.add_date_field(profile_field)
        elif profile_field.field_type == "html_field":
            return self.add_html_field(profile_field)
        elif profile_field.field_type == "email_field":
            return self.add_email_field(profile_field)
        elif profile_field.field_type == "url_field":
            return self.add_url_field(profile_field)
        elif profile_field.field_type == "tel_field":
            return self.add_tel_field(profile_field)
        return self.add_text_field(profile_field)

    def add_select_field(self, profile_field):
        self.add_field(profile_field, create_select_field)

    def add_multi_select_field(self, profile_field):
        self.add_field(profile_field, create_multi_select_field)

    def add_date_field(self, profile_field):
        if profile_field.guid in self.initial:
            self.initial[profile_field.guid] = force_date_format(
                self.initial[profile_field.guid], "%Y-%m-%d"
            )
        self.add_field(profile_field, create_date_field)

    def add_html_field(self, profile_field):
        self.add_field(profile_field, create_html_field)

    def add_text_field(self, profile_field):
        self.add_field(profile_field, create_text_field)

    def add_email_field(self, profile_field):
        self.add_field(profile_field, create_email_field)

    def add_url_field(self, profile_field):
        self.add_field(profile_field, create_url_field)

    def add_tel_field(self, profile_field):
        self.add_field(profile_field, create_tel_field)


class OnboardingReadonlyForm(OnboardingForm):
    def widget_attributes(self):
        return {"readonly": "readonly"}

    def add_select_field(self, profile_field):
        self.add_field(profile_field, create_text_field)

    def add_multi_select_field(self, profile_field):
        if profile_field.guid in self.initial:
            value = self.initial[profile_field.guid]
            if isinstance(value, list):
                self.initial[profile_field.guid] = "\n".join(value)
        self.add_field(profile_field, create_html_field)
