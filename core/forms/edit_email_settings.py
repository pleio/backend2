# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django import forms
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class EditEmailSettingsForm(forms.Form):
    INTERVALS = (
        ("never", _("Never")),
        ("daily", _("Daily")),
        ("weekly", _("Weekly")),
        ("monthly", _("Monthly")),
    )

    notifications_email_enabled = forms.BooleanField(
        required=False, label=_("Receive notification emails")
    )
    overview_email_enabled = forms.ChoiceField(
        choices=INTERVALS, label=_("Receive overview emails")
    )
