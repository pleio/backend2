from django.apps import apps
from django.core.cache import cache
from django.db import connection
from django.utils.translation import gettext_lazy as _

"""
Default site configuration

Valid JSONFields types:
- boolean
- string
- integer
- float
- dict
- list
"""
DEFAULT_SITE_CONFIG = {
    "BACKEND_VERSION": ("2.0", _("Backend version")),
    "NAME": ("Pleio 2.0", _("Site name")),
    "DESCRIPTION": ("Omschrijving site", _("Site description")),
    "LANGUAGE": ("nl", _("Language")),
    "EXTRA_LANGUAGES": ([], _("Extra languages")),
    "CONTENT_TRANSLATION": (False, _("Use content translation")),
    "IS_CLOSED": (True, _("Restrict site access")),
    "ALLOW_REGISTRATION": (False, _("Allow registration without approval")),
    "DIRECT_REGISTRATION_DOMAINS": (
        [],
        _("Allow registration without approval from these domains only"),
    ),
    "DEFAULT_ACCESS_ID": (1, _("Default access level for new content")),
    "PIWIK_URL": ("https://stats.pleio.nl/", _("Matomo/Piwik url")),
    "PIWIK_ID": ("", _("Matomo/Piwik ID")),
    "OIDC_PROVIDERS": (["pleio"], _("OIDC Providers")),
    "REQUIRE_2FA": ("none", _("Require two-factor authentication")),
    "FILE_OPTIONS": ([], _("File options")),
    "AUTO_APPROVE_SSO": (
        False,
        _("Automatically approve users that use one of configured SSO options"),
    ),
    "CUSTOM_JAVASCRIPT": ("", _("Custom Javascript")),
    "THEME_OPTIONS": (
        [
            {"value": "leraar", "label": "Standaard"},
            {"value": "rijkshuisstijl", "label": "Rijkshuisstijl"},
        ],
        _("Default theme options"),
    ),
    "FONT": (
        "Rijksoverheid Sans",
        _("Font for site headers; SiteSettings.fontHeading"),
    ),
    "FONT_BODY": (None, _("Font for all other text; SiteSettings.fontBody")),
    "COLOR_PRIMARY": ("#0e2f56", _("Primary color")),
    "COLOR_SECONDARY": ("#009ee3", _("Secondary color")),
    "COLOR_HEADER": ("", _("Header color")),
    "THEME": ("leraar", _("Theme")),
    "LOGO": ("", _("Header (rijkshuisstijl)")),
    "LOGO_ALT": ("", _("Alt text")),
    "FAVICON": ("", _("Favicon")),
    "LIKE_ICON": ("heart", _("Icon for the like button")),
    "ICON": ("", _("Home icon")),
    "ICON_ALT": ("", _("Home icon alt text")),
    "ICON_ENABLED": (False, _("Use custom home icon")),
    "STARTPAGE": ("activity", _("Homepage")),
    "STARTPAGE_CMS": ("", _("Home cms page")),
    "ANONYMOUS_START_PAGE": ("", _("Homepage type for anonymous visitors")),
    "ANONYMOUS_START_PAGE_CMS": (
        "",
        _("Home cms page for anonymous visitors"),
    ),
    "NUMBER_OF_FEATURED_ITEMS": (0, _("Number of featured items")),
    "ENABLE_FEED_SORTING": (False, _("Show sorting button")),
    "ACTIVITY_FEED_FILTERS_ENABLED": (True, _("Show extra filters")),
    "SUBTITLE": ("", _("Subtitle")),
    "LEADER_ENABLED": (False, _("Show leader widget")),
    "LEADER_BUTTONS_ENABLED": (False, _("Show buttons")),
    "LEADER_IMAGE": ("", _("Image")),
    "INITIATIVE_ENABLED": (False, _("Show initiative widget")),
    "INITIATIVE_TITLE": ("", _("Title")),
    "INITIATIVE_IMAGE": ("", _("Image")),
    "INITIATIVE_IMAGE_ALT": ("", _("Image alt text")),
    "INITIATIVE_DESCRIPTION": ("", _("Description")),
    "INITIATIVE_LINK": ("", _("Link")),
    "DIRECT_LINKS": ([], _("Show direct links")),
    "FOOTER": ([], _("Show footer links")),
    "FOOTER_PAGE": (None, _("Advanced footer content")),
    "FOOTER_PAGE_ENABLED": (False, _("Use advanced footer")),
    "REDIRECTS": ({}, _("Redirects")),
    "MENU": (
        [
            {"link": "/blog", "label": "Blog", "children": [], "access": "public"},
            {"link": "/news", "label": "Nieuws", "children": [], "access": "public"},
            {"link": "/groups", "label": "Groepen", "children": [], "access": "public"},
            {
                "link": "/questions",
                "label": "Vragen",
                "children": [],
                "access": "public",
            },
            {"link": "/wiki", "label": "Wiki", "children": [], "access": "public"},
        ],
        _("Main menu"),
    ),
    "MENU_STATE": ("normal", _("Menu appearance")),
    "PROFILE": ([], _("Profile")),
    "PROFILE_SECTIONS": ([], _("Profile sections")),
    "TAG_CATEGORIES": ([], _("Tag categories")),
    "SHOW_TAGS_IN_FEED": (False, _("Show tags in feed items")),
    "SHOW_TAGS_IN_DETAIL": (False, _("Show tags in detail pages")),
    "SHOW_CUSTOM_TAGS_IN_FEED": (False, _("Show custom tags in feed items")),
    "SHOW_CUSTOM_TAGS_IN_DETAIL": (False, _("Show custom tags in detail pages")),
    "EMAIL_OVERVIEW_DEFAULT_FREQUENCY": (
        "weekly",
        _("Default frequency of the overview email"),
    ),
    "EMAIL_OVERVIEW_SUBJECT": ("", _("Subject of the overview e-mail")),
    "EMAIL_OVERVIEW_TITLE": ("Pleio 2.0", _("Title of the overview e-mail body")),
    "EMAIL_OVERVIEW_INTRO": ("", _("Introduction text of the overview e-mail")),
    "EMAIL_OVERVIEW_ENABLE_FEATURED": (
        False,
        _("Highlight featured items in overview e-mail"),
    ),
    "EMAIL_OVERVIEW_FEATURED_TITLE": (
        "",
        _("Title for the featured items section in the overview e-mail"),
    ),
    "EMAIL_NOTIFICATION_SHOW_EXCERPT": (
        False,
        _("Show excerpt of the content in the notification message"),
    ),
    "SHOW_LOGIN_REGISTER": (True, _("Show login and register buttons")),
    "CUSTOM_TAGS_ENABLED": (True, _("Use custom tags")),
    "SHOW_UP_DOWN_VOTING": (True, _("Use up and down voting")),
    "ENABLE_SHARING": (True, _("Use sharing")),
    "SHOW_VIEW_COUNT": (True, _("Show view count")),
    "NEWSLETTER": (False, _("Use newsletter subscription")),
    "CANCEL_MEMBERSHIP_ENABLED": (
        True,
        _("Allow sending a request for removing your account from this site"),
    ),
    "SHOW_EXCERPT_IN_NEWS_CARD": (False, _("Show excerpt in activity stream")),
    "COMMENT_ON_NEWS": (False, _("Allow add comments on news items")),
    "HIDE_CONTENT_OWNER": (
        ["wiki", "page"],
        _("Hide content owner name on for these content types"),
    ),
    "EVENT_EXPORT": (False, _("Allow download attendee list by administrators")),
    "EVENT_TILES": (False, _("Show events as tiles")),
    "EVENT_ADD_EMAIL_ATTENDEE": (
        "admin",
        _("Clearance for add attendees to event by any email address"),
    ),
    "QUESTIONER_CAN_CHOOSE_BEST_ANSWER": (
        False,
        _("Questioner can choose best answer"),
    ),
    "STATUS_UPDATE_GROUPS": (
        True,
        _("Group administrators can configure adding status updates"),
    ),
    "SUBGROUPS": (False, _("Use subgroups")),
    "GROUP_MEMBER_EXPORT": (False, _("Group administrators can export group members")),
    "GET_CREATE_GROUP_MEMBERS": (
        False,
        _("Allow get-or-create not-site-members at group invite functionality"),
    ),
    "LIMITED_GROUP_ADD": (True, _("Adding of groups limited to admins")),
    "SHOW_SUGGESTED_ITEMS": (False, _("Show suggested items")),
    "MAIL_REPLY_TO": ("noreply@pleio.nl", _("default reply-to mail address")),
    "ENABLE_SEARCH_ENGINE_INDEXING": (False, _("Enable indexing by search engines")),
    "ONBOARDING_ENABLED": (False, _("Use onboarding form")),
    "ONBOARDING_FORCE_EXISTING_USERS": (
        False,
        _("Force existing users to complete the onboarding form"),
    ),
    "ONBOARDING_INTRO": ("", _("Introduction text")),
    "COOKIE_CONSENT": (False, _("Show cookie consent popup")),
    "LOGIN_INTRO": ("", _("Message if access is restricted")),
    "PROFILE_SYNC_ENABLED": (False, _("Use the profile sync API")),
    "PROFILE_SYNC_TOKEN": ("", _("API token")),
    "TENANT_API_TOKEN": (None, _("Tenant API token")),
    "CUSTOM_CSS": ("", _("Custom Css")),
    "CUSTOM_CSS_TIMESTAMP": ("", _("Last updated at")),
    "WHITELISTED_IP_RANGES": ([], _("Whitelisted ip ranges")),
    "WALLED_GARDEN_BY_IP_ENABLED": (
        False,
        _("Use whitelist of IP's for site access without login"),
    ),
    "SITE_MEMBERSHIP_ACCEPTED_SUBJECT": (
        "",
        _(
            "Subject for the membership accepted email;"
            " use [site_name] as a placeholder for the site name"
        ),
    ),
    "SITE_MEMBERSHIP_ACCEPTED_INTRO": (
        "",
        _("Introduction text for the membership accepted email"),
    ),
    "SITE_MEMBERSHIP_DENIED_SUBJECT": (
        "",
        _(
            "Subject for the membership denied email;"
            " use [site_name] as a placeholder for the site name"
        ),
    ),
    "SITE_MEMBERSHIP_DENIED_INTRO": (
        "",
        _("Introduction text for the membership denied email"),
    ),
    "IDP_ID": ("", _("SSO provider ID")),
    "IDP_NAME": ("", _("SSO provider name")),
    "FLOW_ENABLED": (False, _("Use Flow")),
    "FLOW_SUBTYPES": ([], _("Follow these subtypes")),
    "FLOW_APP_URL": ("", _("App url")),
    "FLOW_TOKEN": ("", _("App token")),
    "FLOW_CASE_ID": (None, _("Case id")),
    "FLOW_USER_GUID": ("", _("User id")),
    "EDIT_USER_NAME_ENABLED": (False, _("Allow users to change name")),
    "COMMENT_WITHOUT_ACCOUNT_ENABLED": (
        False,
        _("Allow anonymous users to add comments"),
    ),
    "QUESTION_LOCK_AFTER_ACTIVITY": (
        False,
        _("Lock questions after comments are given"),
    ),
    "QUESTION_LOCK_AFTER_ACTIVITY_LINK": ("", _("Link for locked questions")),
    "LAST_RECEIVED_BOUNCING_EMAIL": ("2021-01-01", _("Last received bouncing email")),
    "LAST_RECEIVED_DELETED_USER": ("2021-01-01", _("Last received deleted user")),
    "CSP_HEADER_EXCEPTIONS": ([], _("CSP header exceptions")),
    "KALTURA_VIDEO_ENABLED": (False, _("Use adding Kaltura videos")),
    "KALTURA_VIDEO_PARTNER_ID": ("", _("Partner ID")),
    "KALTURA_VIDEO_PLAYER_ID": ("", _("Player ID")),
    "PDF_CHECKER_ENABLED": (True, _("PDFChecker enabled")),
    "SECURITY_TEXT": ("", _("Security.txt text")),
    "SECURITY_TEXT_ENABLED": (False, _("Use a custom Security.txt")),
    "SECURITY_TEXT_PGP": ("", _("Security.txt PGP key")),
    "SECURITY_TEXT_REDIRECT_ENABLED": (
        False,
        _("Use a redirect url for security.txt"),
    ),
    "SECURITY_TEXT_REDIRECT_URL": ("", _("Security.txt redirect url")),
    "MAX_CHARACTERS_IN_ABSTRACT": (
        320,
        _("Maximum number of characters in abstract text input"),
    ),
    "COLLAB_EDITING_ENABLED": (True, _("Use collaborative editing")),
    "PRESERVE_FILE_EXIF": (False, _("Preserve the metadata of uploaded files")),
    "ONLINEAFSPRAKEN_ENABLED": (None, _("Use Onlineafspraken.nl")),
    "ONLINEAFSPRAKEN_KEY": (None, _("Onlineafspraken.nl api key")),
    "ONLINEAFSPRAKEN_SECRET": (None, _("Onlineafspraken.nl api secret")),
    "ONLINEAFSPRAKEN_URL": (None, _("Override onlineafspraken.nl api url")),
    "VIDEOCALL_ENABLED": (None, _("Use videocall api")),
    "VIDEOCALL_API_URL": (None, _("Override videocall api url")),
    "VIDEOCALL_PROFILEPAGE": (None, _("Allow initiate videocall from profile page")),
    "VIDEOCALL_THROTTLE": (10, _("Maximum number of room reservations per hour")),
    "VIDEOCALL_APPOINTMENT_TYPE": (
        [],
        _("Setup what appointment-types trigger create a videocall link"),
    ),
    "SITE_PLAN": ("", _("Site plan")),
    "SITE_CATEGORY": ("", _("Site category")),
    "SITE_NOTES": ("", _("Site notes")),
    "SUPPORT_CONTRACT_ENABLED": (False, _("Support contract enabled for site")),
    "SUPPORT_CONTRACT_HOURS_REMAINING": (
        0,
        _("Support contract hours remaining for site"),
    ),
    "SEARCH_PUBLISHED_FILTER_ENABLED": (
        True,
        _("Allow filter by published date on search"),
    ),
    "SEARCH_ARCHIVE_OPTION": ("nobody", _("Allow filter archived articles on search")),
    "SEARCH_RELATED_SCHEMAS": ([], _("Search related schemas")),
    "SEARCH_EXCLUDED_CONTENT_TYPES": (
        [],
        _("Search result without the given content types"),
    ),
    "RECOMMENDED_TYPE": (
        "recommended",
        _("Field to store which recommended type label is preferred"),
    ),
    "BLOCKED_USER_INTRO_MESSAGE": (
        "",
        _("Custom message to show blocked users at login attempts."),
    ),
    "PUSH_NOTIFICATIONS_ENABLED": (
        False,
        _("Push notifications enabled for this site"),
    ),
    "DATAHUB_EXTERNAL_CONTENT_ENABLED": (
        False,
        _("Allow to add a datahub external content processor"),
    ),
    "RECURRING_EVENTS_ENABLED": (False, _("Enable recurring events (Béta)")),
    "PAGE_TAG_FILTERS": (
        [],
        _("Specify what tags should be available per content type."),
    ),
    "CHAT_ENABLED": (False, _("Enable chat for site")),
    "SKIP_CLAMAV": (False, _("Temporally disable clamav")),
    "EMAIL_DISABLED": (False, _("Temporally disable all emails")),
    "HIDE_ACCESS_LEVEL_SELECT": (
        False,
        _("Hide the access level switch for non-privileged visitors."),
    ),
    "SHOW_ORIGINAL_FILE_NAME": (
        False,
        _("Show original filename"),
    ),
    "RICH_TEXT_EDITOR_FEATURES": ([], _("Extra rich text editor features")),
    "INTEGRATED_VIDEOCALL_ENABLED": (False, _("Enable integrated videocall")),
    "OPEN_FOR_CREATE_CONTENT_TYPES": (
        [
            "blog",
            "discussion",
            "event",
            "podcast",
            "episode",
            "poll",
            "question",
            "wiki",
        ],
        _("Authenticated users can create content"),
    ),
    "CONTENT_MODERATION_ENABLED": (False, _("Use content moderation")),
    "REQUIRE_CONTENT_MODERATION_FOR": (
        [],
        _("Content types that require moderation before publishing"),
    ),
    "COMMENT_MODERATION_ENABLED": (False, _("Use comment moderation")),
    "REQUIRE_COMMENT_MODERATION_FOR": (
        [],
        _("Content types where adding comments to requires moderation"),
    ),
    "DAILY_SNAPSHOT_ENABLED": (True, _("Enable daily snapshot")),
    "PREVENT_INSERT_MEDIA_AT_QUESTIONS": (
        False,
        _("Disable insert media at questions"),
    ),
}


class ConfigBackend:
    def __init__(self):
        self._model = apps.get_model("core.Setting")
        self.init()

    def get(self, key):
        value = cache.get("%s%s" % (connection.schema_name, key))

        if value is None:
            try:
                value = self._model.objects.get(key=key).value
            except self._model.DoesNotExist:
                pass
            else:
                cache.set("%s%s" % (connection.schema_name, key), value)

        return value

    def set(self, key, value):
        setting, created = self._model.objects.get_or_create(key=key)
        if created or setting.value != value:
            setting.value = value
            setting.save()

        cache.set("%s%s" % (connection.schema_name, key), value)

    def init(self):
        # fill cache on init
        if connection.schema_name != "public":
            for setting in self._model.objects.all():
                if setting.key in DEFAULT_SITE_CONFIG:
                    cache.set(
                        "%s%s" % (connection.schema_name, setting.key), setting.value
                    )


class Config:
    def __init__(self):
        super(Config, self).__setattr__("_backend", ConfigBackend())

    def __getattr__(self, key):
        try:
            if len(DEFAULT_SITE_CONFIG[key]) not in (2, 3):
                raise AttributeError(key)
            default = DEFAULT_SITE_CONFIG[key][0]
        except KeyError:
            raise AttributeError(key)
        result = self._backend.get(key)

        if result is None:
            result = default
            setattr(self, key, default)
            return result
        return result

    def __setattr__(self, key, value):
        if key not in DEFAULT_SITE_CONFIG:
            raise AttributeError(key)
        self._backend.set(key, value)

    def __dir__(self):
        return DEFAULT_SITE_CONFIG.keys()

    def reset(self):
        self._backend.init()
