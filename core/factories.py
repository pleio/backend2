from faker import Faker
from mixer.backend.django import mixer

from core.models import Group
from user.models import User


def SearchQueryJournalFactory(**kwargs):
    from core.models import SearchQueryJournal

    kwargs.setdefault("query", Faker().name())
    journal = mixer.blend(SearchQueryJournal, **kwargs)

    if "created_at" in kwargs:
        journal.created_at = kwargs["created_at"]
        journal.save()
    return journal


def GroupFactory(**attributes) -> Group:
    assert isinstance(attributes.get("owner"), User), "Groups should have an owner"

    group = mixer.blend(Group, **attributes)
    group.join(attributes["owner"], "member")

    if "start_page" not in attributes:
        group._start_page = group.create_group_start_page()
        group.save()

    return group


class BuildGroupOnceFactoryMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._group = None

    def get_group(self, **kwargs):
        if not self._group:
            self._group = GroupFactory(**kwargs)
        return self._group

    def add_group_to_entity(self, **kwargs):
        if "group" not in kwargs:
            kwargs["group"] = self.get_group(owner=kwargs["owner"])
        return kwargs


def RemoteRssEndpointFactory(**kwargs):
    from core.models import RemoteRssEndpoint

    if "url" not in kwargs:
        kwargs.setdefault("url", Faker().url())
    return mixer.blend(RemoteRssEndpoint, **kwargs)


def CommentFactory(**kwargs):
    assert "owner" in kwargs, "Comments should have an owner"
    assert "container" in kwargs, "Comments should have a container"

    return mixer.blend("core.Comment", **kwargs)


def SiteAccessRequestFactory(**kwargs):
    return mixer.blend("core.SiteAccessRequest", **kwargs)
