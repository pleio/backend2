import logging
import uuid

from django.conf import settings
from django.core.exceptions import SuspiciousOperation
from django.db.models import Q
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.http import urlencode
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.utils import absolutify
from mozilla_django_oidc.views import (
    OIDCAuthenticationCallbackView,
    OIDCAuthenticationRequestView,
)

from core import config
from core.constances import OIDC_PROVIDER_OPTIONS
from core.lib import get_full_url, tenant_summary, view_next
from core.models import SiteAccessRequest, SiteInvitation
from user.models import User
from user.utils.claims import apply_claims

LOGGER = logging.getLogger(__name__)


class RequestAccessException(Exception):
    pass


class OnboardingException(Exception):
    pass


class OIDCAuthCallbackView(OIDCAuthenticationCallbackView):
    def view_with_success_url(self, viewname, *args, **kwargs):
        """
        Pick up the flow where the request originally came from.
        """
        view = reverse(viewname, args=args, kwargs=kwargs)
        return view_next(view, self.success_url)

    def get(self, request):
        try:
            return super(OIDCAuthCallbackView, self).get(request)
        except (RequestAccessException, OnboardingException):
            return redirect(self.view_with_success_url("onboarding"))


class OIDCAuthenticateView(OIDCAuthenticationRequestView):
    def get_extra_params(self, request):
        idp = self.request.GET.get("idp")
        extra_params = self.get_settings("OIDC_AUTH_REQUEST_EXTRA_PARAMS", {})
        if idp:
            extra_params.update({"idp": idp})

        method = self.request.GET.get("method")
        if method == "register":
            extra_params.update({"register": "1"})

        provider = self.request.GET.get("provider", None)
        providerOption = next(
            filter(lambda option: option["value"] == provider, OIDC_PROVIDER_OPTIONS),
            None,
        )
        if providerOption and not providerOption.get("isDefault", False):
            extra_params.update({"provider": provider})

        if settings.ENV == "local":
            # When running local the port number matters; That is ignored by the OIDC provider implementation.
            reverse_url = self.get_settings(
                "OIDC_AUTHENTICATION_CALLBACK_URL", "oidc_authentication_callback"
            )
            extra_params["redirect_uri"] = get_full_url(reverse(reverse_url))

        return extra_params


class OIDCAuthBackend(OIDCAuthenticationBackend):
    class AuthorizationOK(Exception):
        pass

    def filter_users_by_claims(self, claims):
        external_id = claims.get("sub")
        email = claims.get("email")

        if not external_id or not email:
            return User.objects.none()

        return User.objects.filter(
            Q(external_id__iexact=external_id) | Q(email__iexact=email)
        )

    def create_user(self, claims):
        try:
            self.maybe_is_administrator(claims)
            self.maybe_invitation_applies(claims)
            self.maybe_onboarding_applies(claims)
            self.maybe_approval_applies(claims)
        except self.AuthorizationOK:
            pass

        return User.objects.get_or_create_claims(claims)

    def maybe_is_administrator(self, claims):
        if claims.get("is_admin"):
            raise self.AuthorizationOK

    def maybe_invitation_applies(self, claims):
        invite_code = self.request.session.get("invitecode")
        if not invite_code:
            return

        invitations = SiteInvitation.objects.filter(
            Q(code=invite_code) | Q(email=claims.get("email"))
        )

        # There may be multiple invitations in theory.
        if invitations.exists():
            invitations.delete()
            del self.request.session["invitecode"]

            raise self.AuthorizationOK()

    def maybe_approval_applies(self, claims):
        if self.requires_approval(claims):
            self.request.session["onboarding_claims"] = claims
            raise RequestAccessException()

    def maybe_onboarding_applies(self, claims):
        if config.ONBOARDING_ENABLED:
            self.request.session["onboarding_claims"] = claims
            raise OnboardingException()

    def requires_approval(self, claims):
        """Check whether a new user needs approval of an admin before they can be given access based on their claims and the site's configuration"""

        return (
            not config.ALLOW_REGISTRATION  # a site that allows registration, automatically accepts new users
            and not claims.get(
                "is_admin", False
            )  # Pleio admins (not site admins) do not need approval
            and claims.get("email").split("@")[1]
            not in config.DIRECT_REGISTRATION_DOMAINS  # Approval can be skipped for configured email domains
            and not SiteAccessRequest.objects.filter_status(status="pending")
            .filter(email=claims.get("email"))
            .first()  # Users that are already approved, don't require it
            and not self.approve_by_sso(
                claims
            )  # Users can be approved based on the SSO they use
        )

    def approve_by_sso(self, claims):
        """Check if a user can be approved based on the sso they use (i.e. is one of the configured saml/oidc providers)"""
        if not config.AUTO_APPROVE_SSO:
            return False

        return set(claims.get("sso", [])) & set(config.OIDC_PROVIDERS) or set(
            claims.get("sso", [])
        ) & {config.IDP_ID}

    def update_user(self, user, claims):
        apply_claims(user, claims)
        SiteInvitation.objects.filter(email=user.email).delete()
        return user

    def authenticate(self, request, **kwargs):
        """Authenticates a user based on the OIDC code flow."""

        self.request = request
        if not self.request:
            return None

        state = self.request.GET.get("state")
        code = self.request.GET.get("code")
        nonce = kwargs.pop("nonce", None)

        if not code or not state:
            return None

        reverse_url = self.get_settings(
            "OIDC_AUTHENTICATION_CALLBACK_URL", "oidc_authentication_callback"
        )

        token_payload = {
            "client_id": self.OIDC_RP_CLIENT_ID,
            "client_secret": self.OIDC_RP_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": get_full_url(reverse(reverse_url)),
        }

        if settings.ACCOUNT_SYNC_ENABLED and settings.ACCOUNT_API_URL:
            # Fresh origin token.
            origin_token = uuid.uuid4()

            token_payload.update(
                {f"origin_{key}": value for key, value in tenant_summary().items()}
            )
            token_payload["origin_token"] = origin_token
        else:
            origin_token = None

        # Get the token
        token_info = self.get_token(token_payload)
        id_token = token_info.get("id_token")
        access_token = token_info.get("access_token")

        # Validate the token
        payload = self.verify_token(id_token, nonce=nonce)

        if payload:
            self.store_tokens(access_token, id_token)
            try:
                user = self.get_or_create_user(access_token, id_token, payload)
                is_active = getattr(user, "is_active", None)
                if not is_active:
                    request.session["pleio_user_is_banned"] = True
                elif "pleio_user_is_banned" in request.session:
                    del request.session["pleio_user_is_banned"]

                if origin_token:
                    user.profile.update_origin_token(origin_token)

                return user
            except SuspiciousOperation as e:
                LOGGER.warning("failed to get or create user: %s", e)
                return None

        return None

    def get_user(self, user_id):
        try:
            return User.objects.prefetch_related(
                "memberships", "memberships__group", "subgroups"
            ).get(
                pk=user_id
            )  # prefetch related to prevent extra queries in async context
        except User.DoesNotExist:
            return None


def oidc_provider_logout_url(request):
    return_url = absolutify(request, "/")
    query_params = urlencode(
        {
            "post_logout_redirect_uri": return_url,
            "client_id": settings.OIDC_RP_CLIENT_ID,
        }
    )
    return f"{settings.OIDC_OP_LOGOUT_ENDPOINT}?{query_params}"
