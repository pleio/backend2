from urllib.parse import urlparse

from django.shortcuts import redirect

from core.models import SiteAccessRequest, SiteInvitation


def access_request_exists(email) -> SiteAccessRequest:
    return SiteAccessRequest.objects.filter(email=email).first()


def invitation_exists(email):
    return SiteInvitation.objects.filter(email=email).first()


def redirect_or_next(request, url):
    if next := request.GET.get("next"):
        if urlparse(next).netloc == "":
            return redirect(urlparse(next).path)
    return redirect(url)


class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value
