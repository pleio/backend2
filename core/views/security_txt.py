import logging

from django.http import HttpResponse, HttpResponseRedirect
from django_tenants.utils import schema_context

from core import config
from core.http import NotFoundReact

logger = logging.getLogger(__name__)


def security_txt_view(request):
    if config.SECURITY_TEXT_REDIRECT_ENABLED and config.SECURITY_TEXT_REDIRECT_URL:
        return HttpResponseRedirect(config.SECURITY_TEXT_REDIRECT_URL)

    with schema_context("public"):
        from control.models import Configuration

        security_txt_content = Configuration.objects.filter(id="security_txt").first()

    if not security_txt_content and not config.SECURITY_TEXT:
        msg = "File not found"
        raise NotFoundReact(msg)

    if config.SECURITY_TEXT_ENABLED:
        text = config.SECURITY_TEXT
    elif security_txt_content:
        text = security_txt_content.value
    else:
        text = ""

    return HttpResponse(text.encode(), headers={"content-type": "text/plain"})


def security_txt_pgp(request):
    if not config.SECURITY_TEXT_PGP:
        msg = "File not found"
        raise NotFoundReact(msg)

    return HttpResponse(
        config.SECURITY_TEXT_PGP.encode(), headers={"content-type": "text/plain"}
    )
