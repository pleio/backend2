import logging

from django.contrib.auth.views import LogoutView
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.http import urlencode

from core import config
from core.auth import oidc_provider_logout_url
from core.constances import OIDC_PROVIDER_OPTIONS
from core.lib import registration_url

logger = logging.getLogger(__name__)


def register(request):
    if config.OIDC_PROVIDERS == ["pleio"]:
        return redirect(registration_url())
    return redirect("/login?login_credentials=true")


def logout(request):
    # should find out how we can make this better. OIDC logout only allows POST
    LogoutView.as_view()(request)
    return redirect(oidc_provider_logout_url(request))


def login(request):
    if request.GET.get("invitecode", None):
        request.session["invitecode"] = request.GET.get("invitecode")

    # redirect directly to authenticator if there is only one OIDC provider or if IDP is set
    if not request.GET.get("login_credentials") and (
        config.IDP_ID or len(config.OIDC_PROVIDERS) == 1
    ):
        query_args = {}
        if config.IDP_ID:
            query_args["idp"] = config.IDP_ID
        else:
            query_args["provider"] = config.OIDC_PROVIDERS[0]

        if request.GET.get("next"):
            query_args["next"] = request.GET.get("next")
        if request.GET.get("method") == "register":
            query_args["method"] = "register"

        redirect_url = reverse("oidc_authentication_init") + "?" + urlencode(query_args)
        return redirect(redirect_url)

    # render login page
    context = {
        "next": request.GET.get("next", ""),
        "banned": request.session.get("pleio_user_is_banned", False),
        "constants": {
            "OIDC_PROVIDER_OPTIONS": OIDC_PROVIDER_OPTIONS,
        },
    }
    return render(request, "registration/login.html", context)
