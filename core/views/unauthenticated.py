import json
import logging
from http import HTTPStatus

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.template import loader
from django.views.generic import TemplateView

from core import config
from core.http import HttpErrorReactPage, NotFoundReact
from core.lib import is_schema_public
from core.models import Comment, CommentRequest, Entity
from core.resolvers.queries.query_site import get_settings

logger = logging.getLogger(__name__)


def default(request, exception=None):
    if is_schema_public():
        return render(request, "domain_placeholder.html", status=404)

    metadata = {
        "description": config.DESCRIPTION,
    }

    og_metadata = {
        "og:title": config.NAME,
        "og:description": config.DESCRIPTION,
    }

    context = {
        "webpack_dev_server": settings.WEBPACK_DEV_SERVER,
        "json_settings": json.dumps(dict(get_settings(request.user))),
        "metadata": metadata,
        "og_metadata": og_metadata,
    }

    status = HTTPStatus.OK
    if isinstance(exception, HttpErrorReactPage):
        status = exception.status_code
    elif isinstance(exception, Http404):
        status = HTTPStatus.NOT_FOUND

    if exception:
        logger.error("Default view loaded with exception: %s", exception)

    content = loader.render_to_string("react.html", context, request)
    return HttpResponse(content, status=status)


def comment_confirm(request, entity_id):
    try:
        entity = Entity.objects.select_subclasses().get(id=entity_id)
    except ObjectDoesNotExist:
        msg = "Entity not found"
        raise NotFoundReact(msg)

    comment_request = CommentRequest.objects.filter(
        object_id=entity.id,
        code=request.GET.get("code", None),
        email=request.GET.get("email", None),
    ).first()

    if comment_request:
        Comment.objects.create(
            container=entity,
            rich_description=comment_request.rich_description,
            email=comment_request.email,
            name=comment_request.name,
        )

        comment_request.delete()

    return redirect(entity.url)


def unsupported_browser(request):
    return render(request, "unsupported_browser.html")


class ServiceWorkerView(TemplateView):
    """
    Service Worker need to be loaded from same domain.
    Therefore, use TemplateView in order to server the service_worker.js
    """

    template_name = "service_worker.js"
    content_type = "application/javascript"


def manifest(request):
    return render(request, "manifest.json", content_type="application/json")
