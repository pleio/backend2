import logging
from urllib.parse import urlparse

from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import gettext as _
from django.views.generic import FormView, TemplateView

from core import config
from core.forms import OnboardingForm, OnboardingReadonlyForm
from core.lib import is_access_request_required, is_onboarding_required
from core.models.site import SiteAccessRequest
from core.utils import onboarding as onboarding_utils
from core.views.shared import access_request_exists, invitation_exists, redirect_or_next

logger = logging.getLogger(__name__)


class OnboardingView(FormView):
    template_name = "registration/onboarding.html"
    success_url = "/"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_handler = None

    def load_data_handler(self, request):
        if is_access_request_required(request):
            create_handler = onboarding_utils.ProcessSiteAccessRequestFormdata
        else:
            create_handler = onboarding_utils.ProcessUserProfileFormdata
        self.data_handler = create_handler(request)

    def dispatch(self, request, *args, **kwargs):
        self.load_data_handler(request)

        try:
            if is_onboarding_required(request) or is_access_request_required(request):
                self.maybe_invitation_exists(request)
                self.maybe_complete_access_request_exists(request)
                self.maybe_readonly_message(request)
                self.maybe_show_access_denied_message(request)
                return super().dispatch(request, *args, **kwargs)
        except Continue as e:
            return e.redirect(self.success_url)

        return redirect_or_next(request, "/")

    def get_form_class(self):
        if self.is_read_only():
            return OnboardingReadonlyForm
        return OnboardingForm

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "is_access_request": self.is_access_request(),
            "read_only": self.is_read_only(),
            "onboarding_intro": (
                config.ONBOARDING_INTRO if config.ONBOARDING_ENABLED else ""
            ),
        }

    def form_valid(self, form):
        return self.data_handler.handle_form_valid(form)

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            "initial": self.data_handler.initial_values(),
            "request": self.request,
        }

    def maybe_invitation_exists(self, request):
        if request.user.is_authenticated:
            return
        if invitation := self.get_invitation():
            self.data_handler.get_or_create_user()
            invitation.delete()
            raise Continue(self.request.GET.get("next"))

    def maybe_complete_access_request_exists(self, request):
        if request.user.is_authenticated:
            return
        access_request = self.get_access_request()
        if access_request and access_request.status == "pending":
            self.data_handler.complete_user_onboarding(access_request.profile)
            raise Continue(access_request.next)

    def maybe_readonly_message(self, request):
        if self.is_read_only():
            messages.warning(request, _("Your access request is being processed."))

    def maybe_show_access_denied_message(self, request):
        access_request = self.get_access_request()
        if access_request and access_request.status == "rejected":
            if access_request.reason:
                messages.error(request, access_request.reason)
            else:
                messages.error(request, _("Your access request has been denied."))

    def is_access_request(self):
        return is_access_request_required(self.request) and not self.get_invitation()

    def get_access_request(self) -> SiteAccessRequest:
        return access_request_exists(self.get_email_address())

    def is_read_only(self):
        expect_access_request = self.is_access_request()
        access_request = self.get_access_request()
        return (
            expect_access_request
            and access_request
            and access_request.status == "unprocessed"
        )

    def get_invitation(self):
        return invitation_exists(self.get_email_address())

    def get_email_address(self):
        if self.request.session.get("onboarding_claims", None):
            return self.request.session["onboarding_claims"].get("email")
        return None


class OnboardingRequestedView(TemplateView):
    template_name = "registration/requested.html"


class Continue(Exception):
    def __init__(self, next):
        super().__init__()
        self.next = next

    def redirect(self, url):
        if self.next and urlparse(self.next).netloc == "":
            return redirect(self.next)
        return redirect(url)
