import logging

from django.core.exceptions import ObjectDoesNotExist

from core.constances import USER_ROLES
from core.http import ForbiddenReact, NotFoundReact, UnauthorizedReact
from core.lib import get_file_name_with_original_extension
from core.models.agreement import CustomAgreement
from core.utils.streaming_response import PleioStreamingHttpResponse

logger = logging.getLogger(__name__)


def site_custom_agreement(request, custom_agreement_id):
    user = request.user

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    if not user.has_role(USER_ROLES.ADMIN):
        msg = "Not Admin"
        raise ForbiddenReact(msg)

    try:
        return_file = CustomAgreement.objects.get(id=custom_agreement_id)
        response = PleioStreamingHttpResponse(
            file_name=get_file_name_with_original_extension(
                return_file.name, return_file.document.name
            ),
            streaming_content=return_file.document.open(),
            content_type="application/pdf",
        )
        response["Content-Length"] = return_file.document.size
        return response

    except ObjectDoesNotExist:
        msg = "File not found"
        raise NotFoundReact(msg)
