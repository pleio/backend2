import csv
import logging
import zipfile

from django.core.exceptions import ObjectDoesNotExist
from django.http import FileResponse, StreamingHttpResponse
from django.utils.translation import gettext as _

from core import config
from core.constances import USER_ROLES
from core.http import ForbiddenReact, NotFoundReact, UnauthorizedReact
from core.lib import (
    get_exportable_content_types,
    get_human_datasize,
    get_model_by_subtype,
    get_tmp_file_path,
)
from core.models import Group
from core.utils.export import stream
from core.views.shared import Echo

logger = logging.getLogger(__name__)


def export_content(request, content_type):
    user = request.user

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    if not user.has_role(USER_ROLES.ADMIN):
        msg = "Not logged in"
        raise ForbiddenReact(msg)

    exportable_content_types = [d["value"] for d in get_exportable_content_types()]

    if content_type not in exportable_content_types:
        raise NotFoundReact("Content type " + content_type + " can not be exported")

    Model = get_model_by_subtype(content_type)
    entities = Model.objects.all().order_by("-created_at")

    domain = request.tenant.get_primary_domain().domain

    response = StreamingHttpResponse(
        streaming_content=(stream(entities, Echo(), domain, Model)),
        content_type="text/csv",
    )

    filename = content_type + "-export.csv"
    response["Content-Disposition"] = "attachment;filename=" + filename

    return response


def export_group_members(request, group_id=None):
    user = request.user

    if not config.GROUP_MEMBER_EXPORT:
        msg = "Export could not be performed"
        raise NotFoundReact(msg)

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    try:
        group = Group.objects.get(id=group_id)
    except ObjectDoesNotExist:
        msg = "Group not found"
        raise NotFoundReact(msg)

    if not group.can_write(user):
        msg = "Visitor has no access"
        raise ForbiddenReact(msg)

    headers = ["guid", "name", "email", "member since", "last login"]

    subgroups = group.subgroups.all()
    subgroup_names = subgroups.values_list("name", flat=True)
    headers.extend(subgroup_names)

    rows = [headers]

    for membership in group.members.filter(type="member"):
        member = membership.user
        if not member.is_active:
            continue

        email = member.email

        member_subgroups = member.subgroups.all()
        subgroup_memberships = []

        for subgroup in subgroups:
            if subgroup in member_subgroups:
                subgroup_memberships.append(True)
            else:
                subgroup_memberships.append(False)
        row = [
            str(member.id),
            member.name,
            email,
            membership.created_at,
            member.last_login,
        ]
        row.extend(subgroup_memberships)
        rows.append(row)

    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer, delimiter=";", quotechar='"')
    writer.writerow(headers)
    response = StreamingHttpResponse(
        (writer.writerow(row) for row in rows), content_type="text/csv"
    )
    response["Content-Disposition"] = 'attachment; filename="' + group.name + '.csv"'

    return response


def export_groupowners(request):
    user = request.user

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    if not user.has_role(USER_ROLES.ADMIN):
        msg = "Not Admin"
        raise ForbiddenReact(msg)

    def stream(groups: [Group], pseudo_buffer):
        writer = csv.writer(pseudo_buffer, delimiter=";", quotechar='"')
        yield writer.writerow([_("Name"), _("E-mail"), _("Group"), _("Disk size")])

        for g in groups:
            yield writer.writerow(
                [
                    g.owner.name,
                    g.owner.email,
                    g.name,
                    get_human_datasize(g.disk_size()),
                ]
            )

    response = StreamingHttpResponse(
        streaming_content=(stream(Group.objects.all(), Echo())),
        content_type="text/csv",
    )

    filename = "group-owners.csv"
    response["Content-Disposition"] = "attachment;filename=" + filename

    return response


def export_selected_content(request):
    user = request.user

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    if not user.has_role(USER_ROLES.ADMIN):
        msg = "Not Admin"
        raise ForbiddenReact(msg)

    content_ids = request.GET.getlist("content_guids[]")

    exportable_content_types = [d["value"] for d in get_exportable_content_types()]

    domain = request.tenant.get_primary_domain().domain

    temp_file_path = get_tmp_file_path(user, ".zip")
    zipf = zipfile.ZipFile(temp_file_path, "w", zipfile.ZIP_DEFLATED)

    for content_type in exportable_content_types:
        Model = get_model_by_subtype(content_type)
        assert Model, (
            "Add the correct model to get_model_by_subtype for " + content_type
        )
        entities = Model.objects.filter(id__in=content_ids)
        if entities:
            csv = stream(entities, Echo(), domain, Model)
            zipf.writestr("{}-export.csv".format(content_type), "".join(csv))

    zipf.close()

    response = FileResponse(open(temp_file_path, "rb"))
    response["Content-Disposition"] = "attachment; filename=content_export.zip"

    return response
