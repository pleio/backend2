import logging

from django.contrib import messages
from django.core import signing
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import gettext as _

from core import config
from core.forms import EditEmailSettingsForm
from core.http import NotFoundReact
from core.lib import get_base_url, tenant_schema
from core.utils.mail import EmailSettingsTokenizer, UnsubscribeTokenizer

logger = logging.getLogger(__name__)


def edit_email_settings(request, token):
    try:
        tokenizer = EmailSettingsTokenizer()
        user = tokenizer.unpack(token)
    except (signing.BadSignature, ObjectDoesNotExist):
        return HttpResponseRedirect("/")

    if request.method == "POST":
        form = EditEmailSettingsForm(request.POST)

        if form.is_valid():
            user.profile.receive_notification_email = form.cleaned_data[
                "notifications_email_enabled"
            ]
            user.profile.overview_email_interval = form.cleaned_data[
                "overview_email_enabled"
            ]
            user.profile.save()
            messages.success(request, _("Your changes are saved"))
            return HttpResponseRedirect(request.path)

    initial_dict = {
        "notifications_email_enabled": user.profile.receive_notification_email,
        "overview_email_enabled": user.profile.overview_email_interval,
    }

    form = EditEmailSettingsForm(initial=initial_dict)

    context = {
        "user_name": user.name,
        "site_url": get_base_url(),
        "site_name": config.NAME,
        "form": form,
    }

    return render(request, "edit_email_settings.html", context)


def unsubscribe(request, token):
    try:
        user, mail_id, is_expired = UnsubscribeTokenizer().unpack(token)
        list_name = None
        if mail_id == UnsubscribeTokenizer.TYPE_OVERVIEW:
            user.profile.overview_email_interval = "never"
            list_name = _("Periodic overview")
        elif mail_id == UnsubscribeTokenizer.TYPE_NOTIFICATIONS:
            user.profile.receive_notification_email = False
            list_name = _("Notification overview")
        user.profile.save()

        msg = _("Successfully unsubscribed %(email)s from %(list_name)s") % {
            "email": user.email,
            "list_name": list_name,
        }

        if not is_expired:
            messages.success(request, msg)
            return HttpResponseRedirect(EmailSettingsTokenizer().create_url(user))

        return render(request, "unsubscribe.html", {"msg": msg})
    except Exception as e:
        logger.error(
            "unsubscribe_error: schema=%s, error=%s, type=%s, token=%s",
            tenant_schema(),
            str(e),
            e.__class__,
            token,
        )
        raise NotFoundReact()
