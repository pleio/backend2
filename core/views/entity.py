import json
import logging
from http import HTTPStatus

import pypandoc
import qrcode
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import FileResponse, HttpResponse
from django.shortcuts import render
from django.utils.text import Truncator, slugify

from core import config
from core.http import NotFoundReact, UnauthorizedReact
from core.lib import get_tmp_file_path, replace_html_img_src
from core.models import Entity, Group
from core.resolvers.queries.query_site import get_settings
from core.utils.convert import tiptap_to_html
from entities.event.lib import get_url

logger = logging.getLogger(__name__)


def download_rich_description_as(request, entity_id=None, file_type=None):
    user = request.user
    entity = None

    if file_type not in ["odt", "html"]:
        msg = "File type not supported, choose from odt or html"
        raise NotFoundReact(msg)

    if entity_id:
        try:
            entity = Entity.objects.visible(user).select_subclasses().get(id=entity_id)
        except ObjectDoesNotExist:
            pass

    if not entity:
        msg = "Entity not found"
        raise NotFoundReact(msg)

    if not entity.rich_description:
        msg = "Entity rich description not found"
        raise NotFoundReact(msg)

    filename = "file_contents"
    if entity.title:
        filename = entity.title

    temp_file_path = get_tmp_file_path(user, ".{}".format(file_type))
    html = tiptap_to_html(entity.rich_description)
    html = replace_html_img_src(html, user, file_type)

    pypandoc.convert_text(html, file_type, format="html", outputfile=temp_file_path)
    response = FileResponse(open(temp_file_path, "rb"))
    response["Content-Disposition"] = "attachment; filename=" + "{}.{}".format(
        filename, file_type
    )
    return response


def entity_view(request, entity_id=None, entity_title=None):
    user = request.user

    entity = None

    metadata = {
        "description": config.DESCRIPTION,
    }

    og_metadata = {
        "og:title": config.NAME,
        "og:description": config.DESCRIPTION,
    }

    if entity_id:
        try:
            entity = Entity.objects.visible(user).select_subclasses().get(id=entity_id)
        except ObjectDoesNotExist:
            pass

    if not entity:
        try:
            entity = Entity.objects.draft(user).select_subclasses().get(id=entity_id)
        except ObjectDoesNotExist:
            pass

    if entity:
        status_code = HTTPStatus.OK
        if hasattr(entity, "description") and entity.description:
            metadata["description"] = (
                Truncator(entity.description).words(26).replace('"', "")
            )
            og_metadata["og:description"] = metadata["description"]
        og_metadata["og:title"] = entity.title
        og_metadata["og:type"] = "article"
        if hasattr(entity, "featured_image_url") and entity.featured_image_url:
            og_metadata["og:image"] = request.build_absolute_uri(
                entity.featured_image_url
            )
        if hasattr(entity, "featured_video") and entity.featured_video:
            og_metadata["og:video"] = entity.featured_video
        og_metadata["og:url"] = request.build_absolute_uri(request.path)
        og_metadata["og:site_name"] = config.NAME
        metadata["article:published_time"] = entity.created_at.strftime(
            "%Y-%m-%d %H:%M"
        )
        metadata["article:modified_time"] = entity.updated_at.strftime("%Y-%m-%d %H:%M")
    else:
        try:
            Group.objects.visible(user).get(id=entity_id)
            status_code = HTTPStatus.OK
        except ObjectDoesNotExist:
            status_code = HTTPStatus.NOT_FOUND

    context = {
        "webpack_dev_server": settings.WEBPACK_DEV_SERVER,
        "json_settings": json.dumps(get_settings(request.user)),
        "metadata": metadata,
        "og_metadata": og_metadata,
    }

    return render(request, "react.html", context, status=status_code)


def get_url_qr(request, entity_id=None):
    # Only implemented for Events. Can be adjusted to be used for other entities
    user = request.user

    if not user.is_authenticated:
        msg = "Not logged in"
        raise UnauthorizedReact(msg)

    try:
        entity = Entity.objects.visible(user).get_subclass(id=entity_id)
    except ObjectDoesNotExist:
        msg = "Entity not found"
        raise NotFoundReact(msg)

    url = get_url(entity)
    filename = slugify(entity.title)[:248].removesuffix("-")

    code = qrcode.make(url)

    response = HttpResponse(content_type="image/png")
    code.save(response, "PNG")
    response["Content-Disposition"] = f'attachment; filename="qr_{filename}.png"'

    return response
