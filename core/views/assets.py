import logging
import os

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import FileResponse, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.decorators.cache import cache_control
from django.views.decorators.http import require_GET

from core import config
from core.http import NotFoundReact, file_blocked_response
from core.lib import get_file_name_with_original_extension, safe_file_path
from core.utils.streaming_response import PleioStreamingHttpResponse
from entities.file.models import FileFolder

logger = logging.getLogger(__name__)


def attachment(request, attachment_id, attachment_type=None):
    user = request.user

    size = request.GET.get("size", None)

    try:
        file = FileFolder.objects.get(id=attachment_id)

        if not file.can_read(user):
            msg = "File not found"
            raise NotFoundReact(msg)

        if file.blocked:
            return file_blocked_response(request, file.upload.name, file.block_reason)

        return_file = file

        if file.is_image() and size:
            resized_image = file.get_resized_image(size)

            if resized_image:
                return_file = resized_image
            else:
                return redirect(file.attachment_url)

        response = PleioStreamingHttpResponse(
            file_name=get_file_name_with_original_extension(
                file.title, return_file.upload.name
            ),
            streaming_content=return_file.upload.open(),
            content_type=return_file.mime_type,
        )
        response["Content-Length"] = return_file.upload.size
        return response

    except ObjectDoesNotExist:
        msg = "File not found"
        raise NotFoundReact(msg)


@cache_control(public=True, max_age=15724800)
def custom_css(request):
    return HttpResponse(config.CUSTOM_CSS, content_type="text/css")


@cache_control(public=True)
def favicon(request, size=192):
    allowed_sizes = [16, 32, 180, 192, 512]
    if size not in allowed_sizes:
        return HttpResponse(status=404)

    if config.FAVICON:
        # get FileFolder by url
        file = FileFolder.objects.file_by_path(config.FAVICON)

        if file:
            # return original until we have resized favicons
            resized_or_original = (
                file.get_resized_image(size, valid_sizes=allowed_sizes) or file
            )
            if resized_or_original:
                response = PleioStreamingHttpResponse(
                    file_name=get_file_name_with_original_extension(
                        file.title, resized_or_original.upload.name
                    ),
                    streaming_content=resized_or_original.upload.open(),
                    content_type=resized_or_original.mime_type,
                )
                response["Content-Length"] = resized_or_original.upload.size
                return response

    file_path = safe_file_path(settings.STATIC_ROOT, f"favicon-{size}.png")
    if os.path.exists(file_path):
        return FileResponse(open(file_path, "rb"))

    return HttpResponse(status=404)


@require_GET
def robots_txt(request):
    if config.ENABLE_SEARCH_ENGINE_INDEXING:
        lines = [
            "User-Agent: *",
            f"Sitemap: {request.build_absolute_uri(reverse('sitemap'))}",
            "Disallow: /user",
            "Disallow: /search",
            "Disallow: /search/",
            "Disallow: /tags",
            "Disallow: /tags/",
        ]
    else:
        lines = [
            "User-Agent: *",
            "Disallow: /",
        ]

    return HttpResponse("\n".join(lines), content_type="text/plain")
