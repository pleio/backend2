import base64
import collections
import ipaddress
import json
import logging
import mimetypes
import os
import re
import secrets
import tempfile
import uuid
from enum import Enum
from math import ceil
from urllib.parse import parse_qs, urlencode, urljoin, urlparse, urlunparse

import html2text
from bs4 import BeautifulSoup
from datasize import DataSize
from dateutil.parser import parse as dateutil_parse
from django.apps import apps
from django.conf import settings
from django.core.cache import cache
from django.core.files.storage import default_storage
from django.core.validators import URLValidator
from django.db import connection
from django.urls import reverse
from django.utils import timezone as django_timezone
from django.utils.module_loading import import_string
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy, pgettext_lazy
from django_tenants.utils import get_tenant_model
from pytz import timezone

from core import config
from core.constances import ACCESS_TYPE, PROTECTED_CONTENT_TYPES
from core.exceptions import IgnoreIndexError, UnableToTestIndex

logger = logging.getLogger(__name__)


class TypeModels(Enum):
    """Can be used to convert GraphQL types to Django models"""

    magazine = "magazine.Magazine"
    magazine_issue = "magazine.MagazineIssue"
    news = "news.News"
    poll = "poll.Poll"
    discussion = "discussion.Discussion"
    event = "event.Event"
    wiki = "wiki.Wiki"
    question = "question.Question"
    page = "cms.Page"
    blog = "blog.Blog"
    group = "core.Group"
    user = "user.User"
    statusupdate = "activity.StatusUpdate"
    task = "task.Task"
    comment = "core.Comment"
    file = "file.FileFolder"
    folder = "file.FileFolder"
    pad = "file.FileFolder"
    externalcontent = "external_content.ExternalContent"
    externalcontentsource = "external_content.ExternalContentSource"
    podcast = "podcast.Podcast"
    episode = "podcast.Episode"


def get_model_by_subtype(subtype):
    """
    Get Django model by subtype name
    """

    try:
        model_name = TypeModels[subtype].value
        return apps.get_model(model_name)
    except (AttributeError, KeyError):  # pragma: no cover
        return None


def exclude_root_acl(obj, acl):
    exclude = include_root_acl(obj, [])
    return [x for x in acl if x not in exclude]


def include_root_acl(obj, acl):
    """
    Helper method to include the owner of the object
    in the list of acl items.
    """
    if getattr(obj, "type_to_string", None) == "user":
        return [ACCESS_TYPE.user.format(obj.id), *acl]
    else:
        return [ACCESS_TYPE.user.format(obj.owner.id), *acl]


def access_id_to_acl(obj, access_id):
    """
    @tag: acl_methods

    @see also
      * access_id_to_acl(obj: *, access_id: int)
      - get_acl(user: User)
      - get_access_id(acl: [str])
      - get_access_ids(obj: *)

    What are the access id's?
    0: private
    1: logged in
    2: public
    4: Group
    10000+: Subgroup
    """
    from core.models import Group

    acl = include_root_acl(obj, [])

    if isinstance(access_id, str):
        access_id = int(access_id)

    in_closed_group = False
    if hasattr(obj, "group") and obj.group:
        in_closed_group = obj.group.is_closed

    # object is in close group, convert public to group access
    if in_closed_group and access_id in (1, 2):
        access_id = 4

    if access_id == 1 and not in_closed_group:
        acl.append(ACCESS_TYPE.logged_in)
    elif access_id == 2 and not in_closed_group:
        acl.append(ACCESS_TYPE.public)
    elif access_id == 4 and getattr(obj, "group", None):
        acl.append(ACCESS_TYPE.group.format(obj.group.id))
    elif access_id == 4 and isinstance(obj, (Group,)):
        acl.append(ACCESS_TYPE.group.format(obj.id))
    elif access_id and access_id >= 10000 and getattr(obj, "group", None):
        acl.append(ACCESS_TYPE.subgroup.format(access_id))
    return acl


def acl_overlap(user, acl):
    return len({*get_acl(user)} & {*acl}) > 0


def get_acl(user):
    """
    @tag: acl_methods

    @see also
      - access_id_to_acl(obj: *, access_id: int)
      * get_acl(user: User)
      - get_access_id(acl: [str])
      - get_access_ids(obj: *)
    """

    acl = {ACCESS_TYPE.public}

    if user.is_authenticated:
        acl.add(ACCESS_TYPE.logged_in)
        acl.add(ACCESS_TYPE.user.format(user.id))

        if user.memberships:
            groups = {
                ACCESS_TYPE.group.format(membership.group.id)
                for membership in user.memberships.all()
                if membership.type
                in ["admin", "member", "owner"]  # prevent extra query in async context
            }
            acl = acl.union(groups)
        if user.subgroups:
            subgroups = {
                ACCESS_TYPE.subgroup.format(subgroup.access_id)
                for subgroup in user.subgroups.all()
            }
            acl = acl.union(subgroups)
    return acl


def clean_graphql_input(values, always_include=None):
    """Cleanup resolver input"""
    allow_none = [
        "timePublished",
        "scheduleArchiveEntity",
        "scheduleDeleteEntity",
        "groupGuid",
    ] + (always_include or [])
    # TODO: what are we going to do with values which kan be omitted or can be NULL

    # Remove items with None values from dict except for timePublished data
    return {k: v for k, v in values.items() if (v is not None) or (k in allow_none)}


def webpack_dev_server_is_available():  # pragma: no cover
    """Return true when webpack developer server is available"""

    if settings.ENV != "local":
        return False

    docker_host = os.environ.get("DOCKER_LOCAL_MACHINE", None)

    if docker_host:
        import socket

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                s.settimeout(1.0)
                return s.connect_ex((docker_host, 9001)) == 0
            except Exception:
                return False
    return False


def get_access_id(acl):
    """
    @tag: acl_methods

    @see also
      - access_id_to_acl(obj: *, access_id: int)
      - get_acl(user: User)
      * get_access_id(acl: [str])
      - get_access_ids(obj: *)
    """
    if ACCESS_TYPE.public in acl:
        return 2
    if ACCESS_TYPE.logged_in in acl:
        return 1
    for x in acl:
        if x.startswith("subgroup:"):
            return int(x.replace("subgroup:", ""))
        if x.startswith("group:"):
            return 4
    return 0


def get_access_ids(obj=None, exclude=None):
    """
    @tag: acl_methods

    @see also
      - access_id_to_acl(obj: *, access_id: int)
      - get_acl(user: User)
      - get_access_id(acl: [str])
      * get_access_ids(obj: *)
    """
    accessIds = []
    accessIds.append({"id": 0, "description": str(gettext_lazy("Just me"))})

    if isinstance(obj, apps.get_model("core.Group")):
        accessIds.append(
            {
                "id": 4,
                "description": str(
                    gettext_lazy("Group: %(group_name)s") % {"group_name": obj.name}
                ),
            }
        )
        if obj.subgroups:
            for subgroup in obj.subgroups.all():
                accessIds.append(
                    {
                        "id": subgroup.access_id,
                        "description": str(
                            gettext_lazy("Subgroup: %(subgroup_name)s")
                            % {"subgroup_name": subgroup.name}
                        ),
                    }
                )

    if isinstance(obj, apps.get_model("core.Group")) and obj.is_closed:
        pass
    else:
        accessIds.append({"id": 1, "description": str(gettext_lazy("Logged in users"))})
        if not config.IS_CLOSED:
            accessIds.append({"id": 2, "description": str(gettext_lazy("Public"))})

    return [*filter(lambda item: not exclude or item["id"] not in exclude, accessIds)]


def get_core_hook(hook_name):
    key = "CORE_HOOK_REPOSITORY:%s" % hook_name
    result = cache.get(key)
    if not result:
        result = []
        for app_config in apps.get_app_configs():
            try:
                hook_path = "{}.core_hooks.{}".format(app_config.name, hook_name)
                assert callable(import_string(hook_path))
                result.append(hook_path)
            except ImportError:
                pass
        cache.set(key, result)
    return result


def test_elasticsearch_index(index_name):
    for path in get_core_hook("test_elasticsearch_index"):
        try:
            test_function = import_string(path)
            test_function(index_name)
            return
        except IgnoreIndexError:
            pass

    raise UnableToTestIndex()


def get_hourly_cron_jobs():  # pragma: no cover
    for task_name in get_core_hook("get_hourly_cron_jobs"):
        yield from import_string(task_name)()


def get_activity_filters():
    for hook in get_core_hook("get_activity_filters"):
        hook_function = import_string(hook)
        yield from hook_function()


def get_entity_filters():
    for hook in get_core_hook("get_entity_filters"):
        hook_function = import_string(hook)
        yield from hook_function()


def get_search_filters():
    for hook in get_core_hook("get_search_filters"):
        hook_function = import_string(hook)
        yield from hook_function()


def generate_object_filename(obj, filename):
    ext = filename.split(".")[-1]
    name = filename.rsplit(".", 1)[0]
    filename = "%s.%s" % (slugify(name), ext)
    return os.path.join(str(obj.id), filename)


def delete_attached_file(filefield):
    if not filefield:
        return

    if default_storage.exists(filefield.name):
        default_storage.delete(filefield.name)


class FieldTypes(Enum):
    text_field = "textField"
    date_field = "dateField"
    date_time_field = "dateTimeField"
    html_field = "htmlField"
    select_field = "selectField"
    multi_select_field = "multiSelectField"
    radio_field = "radioField"
    checkbox_field = "checkboxField"
    email_field = "emailField"
    url_field = "urlField"
    tel_field = "charField"


def get_field_type(field_type):
    try:
        return FieldTypes[field_type].value
    except KeyError:
        return FieldTypes.text_field.value


def get_model_field_type(field_type):
    for ft in FieldTypes:
        if ft.value == field_type:
            return ft.name
    return FieldTypes.text_field.name


def is_valid_json(string):
    try:
        string = json.loads(string)
    except Exception:
        return False
    return True


def get_base_url():
    try:
        url_schema = "http" if settings.ENV == "local" else "https"
        url_port = ":8000" if settings.ENV == "local" else ""
        tenant = get_tenant_model().objects.get(schema_name=connection.schema_name)
        return f"{url_schema}://{tenant.get_primary_domain().domain}{url_port}"
    except Exception:
        return ""


def get_full_url(relative_path, __auth=False, **kwargs):
    relative_path = add_url_params(relative_path, __auth=__auth, **kwargs)
    if not re.match(r"^https?:\/\/", relative_path):
        return f"{get_base_url()}{relative_path}"
    return relative_path


def add_url_params(path, __auth=False, **kwargs):
    if __auth:
        kwargs[settings.REQUIRE_AUTH_PARAM] = "1"
    if kwargs:
        kwargs = collections.OrderedDict(sorted(kwargs.items()))
        return path + "?" + urlencode(kwargs)
    return path


def get_account_url(relative_path):
    prefix = str(settings.ACCOUNT_API_URL).rstrip("/")
    return urljoin(prefix, relative_path)


def get_control_url(relative_path, site=None):
    if site:
        relative_path = "site/{}/{}".format(str(site.pk), relative_path)
    return urljoin(str(settings.CONTROL3_URL), relative_path)


def tenant_api_token():
    if not config.TENANT_API_TOKEN:
        config.TENANT_API_TOKEN = str(uuid.uuid4())
    return config.TENANT_API_TOKEN


def tenant_summary(with_favicon=False):
    summary = {
        "url": get_base_url(),
        "name": config.NAME,
        "description": config.DESCRIPTION,
        "api_token": tenant_api_token(),
    }

    if with_favicon and config.FAVICON:
        try:
            from entities.file.models import FileFolder

            file = FileFolder.objects.file_by_path(config.FAVICON)
            summary["favicon"] = os.path.basename(file.upload.name)
            summary["favicon_data"] = file.get_content(
                wrap=lambda content: base64.encodebytes(content).decode()
            )
        except AttributeError:
            pass

    return summary


def obfuscate_email(email):
    # alter email: example@domain.com -> e******@domain.com
    try:
        email_splitted = email.split("@")
        nr_char = len(email_splitted[0]) - 1
        return email_splitted[0][0] + "*" * nr_char + "@" + email_splitted[1]
    except Exception:
        return ""


def generate_code():
    return secrets.token_hex(10)


def get_exportable_user_fields():
    from user.exporting import ExportUsers

    return [
        {
            "field_type": "userField",
            "field": r.field,
            "label": r.label,
        }
        for r in ExportUsers.AVAILABLE_SERIALIZERS
        if r.field not in ["ban_reason", "banned_at"]
    ]


def get_exportable_content_types():
    return [
        {"value": "statusupdate", "label": pgettext_lazy("plural", "Updates")},
        {"value": "blog", "label": pgettext_lazy("plural", "Blogs")},
        {"value": "page", "label": pgettext_lazy("plural", "Pages")},
        {"value": "discussion", "label": pgettext_lazy("plural", "Discussions")},
        {"value": "event", "label": pgettext_lazy("plural", "Events")},
        {"value": "file", "label": pgettext_lazy("plural", "Files")},
        {"value": "magazine", "label": pgettext_lazy("plural", "Magazines")},
        {
            "value": "magazine_issue",
            "label": pgettext_lazy("plural", "Magazine issues"),
        },
        {"value": "news", "label": pgettext_lazy("plural", "News")},
        {"value": "poll", "label": pgettext_lazy("plural", "Polls")},
        {"value": "question", "label": pgettext_lazy("plural", "Questions")},
        {"value": "task", "label": pgettext_lazy("plural", "Tasks")},
        {"value": "wiki", "label": pgettext_lazy("plural", "Wiki pages")},
        {"value": "comment", "label": pgettext_lazy("plural", "Comments")},
        {"value": "group", "label": pgettext_lazy("plural", "Groups")},
        {"value": "podcast", "label": pgettext_lazy("plural", "Podcasts")},
        {"value": "episode", "label": pgettext_lazy("plural", "Podcast episodes")},
    ]


def get_language_options():
    return [{"value": item[0], "label": item[1]} for item in settings.LANGUAGES]


def is_schema_public():
    return connection.schema_name == "public"


def tenant_schema():
    return connection.schema_name


def tenant_instance():
    Model = get_tenant_model()
    return Model.objects.get(schema_name=connection.schema_name)


def html_to_text(html):
    h = html2text.HTML2Text()
    h.ignore_links = True
    h.ignore_tables = True
    h.ignore_images = True
    return h.handle(html)


def replace_html_img_src(html, user, file_type):
    from entities.file.models import FileFolder

    def get_img_src(path, user):
        if file_type in ["odt"]:
            try:
                attachment_id = path.split("/")[2]
                attachment = FileFolder.objects.get(id=attachment_id)
                if attachment.can_read(user):
                    path = attachment.upload.name
            except Exception:
                pass
        elif file_type == "html":
            path = get_base_url() + path
        return path

    soup = BeautifulSoup(html, "html.parser")
    for img in soup.findAll("img"):
        img["src"] = get_img_src(img["src"], user)
    return str(soup)


def get_tmp_file_path(user, suffix=""):
    folder = safe_file_path(tempfile.gettempdir(), tenant_schema(), str(user.id))
    try:
        os.makedirs(folder)
    except FileExistsError:
        pass
    _, temp_file_path = tempfile.mkstemp(dir=folder, suffix=suffix)

    return temp_file_path


def is_valid_domain(domain):
    pattern = re.compile(
        r"^(?:[a-zA-Z0-9]"  # First character of the domain
        r"(?:[a-zA-Z0-9-_]{0,61}[A-Za-z0-9])?\.)"  # Sub domain + hostname
        r"+[A-Za-z0-9][A-Za-z0-9-_]{0,61}"  # First 61 characters of the gTLD
        r"[A-Za-z]$"  # Last character of the gTLD
    )
    try:
        return pattern.match(domain)
    except (UnicodeError, AttributeError):
        return None


def datetime_isoformat(obj):
    if obj:
        return obj.astimezone(timezone(settings.TIME_ZONE)).isoformat(
            timespec="seconds"
        )
    return None


def datetime_format(obj, seconds=False):
    if isinstance(obj, django_timezone.datetime):
        obj = obj.astimezone(django_timezone.get_current_timezone())
        if seconds:
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        return obj.strftime("%Y-%m-%d %H:%M")
    return ""


def datetime_utciso(value):
    if isinstance(value, (django_timezone.datetime,)):
        return value.astimezone(django_timezone.utc).isoformat()
    return ""


def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")

    try:
        ipv4_version = ipaddress.IPv6Address(x_forwarded_for).ipv4_mapped

        if ipv4_version:  # pragma: no cover
            x_forwarded_for = str(ipv4_version)
    except Exception:
        pass

    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip


def is_valid_url_or_path(url):
    validate = URLValidator()
    if not url.startswith("http"):
        url = "https://test.nl" + url
    try:
        validate(url)
        return True
    except Exception:
        return False


def is_full_url(url):
    try:
        result = urlparse(url)
        return all([result.scheme == "https", result.netloc])
    except ValueError:
        return False


def get_mimetype(filepath):
    mimetypes.init()
    mime_type, _ = mimetypes.guess_type(filepath)
    if not mime_type:
        return None
    return mime_type


def get_file_name_with_original_extension(filename, path):
    _, original_ext = os.path.splitext(path)
    if not get_mimetype(filename):
        return filename + original_ext

    result_filename, _ = os.path.splitext(filename)
    return result_filename + original_ext


def get_basename(filepath):
    return os.path.basename(filepath)


def get_file_extension(filepath):
    base_name, file_extension = os.path.splitext(filepath)
    return file_extension


def get_human_datasize(bytes):
    return "{:.1A}".format(DataSize(bytes))


def get_model_name(instance):
    return instance._meta.app_label + "." + instance._meta.model_name


def get_file_checksum(fh):
    try:
        from hashlib import md5

        return md5(fh.read()).hexdigest()
    except Exception:
        pass


def is_valid_uuid(val):
    try:
        uuid.UUID(str(val))
        return True
    except ValueError:
        return False


class NumberIncrement:
    def __init__(self, n=0):
        self.n = n

    def next(self):
        try:
            return self.n
        finally:
            self.n = self.n + 1


def early_this_morning(reference=None):
    reference = reference or django_timezone.localtime()
    return reference - django_timezone.timedelta(
        hours=reference.hour,
        minutes=reference.minute,
        seconds=reference.second,
        microseconds=reference.microsecond,
    )


def next_round_hour(reference=None):
    reference = (reference or django_timezone.localtime()) + django_timezone.timedelta(
        hours=1
    )
    return reference.replace(minute=0, second=0, microsecond=0)


def str_to_datetime(datetime_str):
    if not datetime_str:
        return None
    result = dateutil_parse(datetime_str)

    if not django_timezone.is_aware(result):
        return django_timezone.make_aware(result)

    return result.astimezone(django_timezone.get_current_timezone())


def validate_token(request, token):
    if not token:
        return False

    try:
        if str(request.META["HTTP_AUTHORIZATION"]) == str("Bearer " + token):
            return True
    except Exception:
        pass

    try:
        if str(request.META["headers"]["Authorization"]) == str("Bearer " + token):
            return True
    except Exception:
        pass
    return False


def get_page_tag_filters():
    defaults = [
        {"contentType": ct, "showTagFilter": tf, "showTagCategories": []}
        for ct, tf in [
            ("news", True),
            ("blog", True),
            ("question", True),
            ("discussion", True),
            ("event", False),
        ]
    ]
    stored_settings = {
        f["contentType"]: f for f in config.PAGE_TAG_FILTERS if "contentType" in f
    }

    # Ensure consistent number of settings for the applicable content types.
    for setting in defaults:
        if setting["contentType"] in stored_settings:
            yield stored_settings[setting["contentType"]]
        else:
            yield setting


# get token for one of jitsi videocall servers.
# token_only: True for INTEGRATED_TOKEN_ONLY_VIDEO_CALL_URL
# token_only: False for INTEGRATED_VIDEO_CALL_URL
def get_video_call_jwt_token(id, token_only=True):
    import jwt

    try:
        if token_only:
            sub = settings.INTEGRATED_TOKEN_ONLY_VIDEO_CALL_URL
            secret = settings.INTEGRATED_TOKEN_ONLY_VIDEO_CALL_SECRET
        else:
            sub = settings.INTEGRATED_VIDEO_CALL_URL
            secret = settings.INTEGRATED_VIDEO_CALL_SECRET

        payload = {
            "aud": "jitsi",
            "iss": "pleio",
            "sub": sub,
            "room": str(id),
        }
        return jwt.encode(payload, secret, algorithm="HS256")
    except Exception:
        return ""


def task_status(task_id):
    from celery.result import AsyncResult

    result = AsyncResult(task_id)
    print(result.status)


def registration_url():
    return reverse("oidc_authentication_init") + "?provider=pleio&method=register"


def is_max_parent_depth_reached(entity, count=0, max_depth=20):
    if entity.parent:
        count += 1
        is_max_parent_depth_reached(entity.parent, count, max_depth)
    if count >= max_depth:
        return True
    return False


def entity_exists_in_parent_tree(entity, parent, max_depth=99):
    depth = 1
    next = parent
    while next or depth > max_depth:
        if next == entity:
            return True
        next = next.parent
        depth += 1
    return False


class Distribute:
    """
    Given a list of items.

    Mix the left side of the list with the right side of the list so that neighbours end with a
    distance of approximately 'distance', and are mixed with the left side.
    """

    def __init__(self, content, distance):
        self.content = content or []
        self.distance = distance

    def spread(self):
        # Continue until all is done.
        if len(self.content) == 0:
            return

        # Step one: collect a linear-distributed subset from the full list.
        step = ceil(len(self.content) / self.distance)
        sub_set = []
        for n in reversed(range(0, self.distance)):
            try:
                sub_set.insert(0, self.content.pop(step * n))
            except IndexError:
                continue

        # Step 2: Mix them utter right, utter left, etc.
        while len(sub_set) > 0:
            try:
                yield sub_set.pop(0)
                yield sub_set.pop(len(sub_set) - 1)
            except IndexError:
                break

        # Recursive operation.
        yield from self.spread()


class TrackForChanges:
    def __init__(self, initial_values, track=None):
        self.initial_values = initial_values
        self.track = track or [*initial_values.keys()]

    def is_changed(self, updated_values):
        for key in self.track:
            if self.initial_values[key] != updated_values[key]:
                return True
        return False


def is_onboarding_required(request):
    """
    This subroutine is the single source of truth on to show or to hide the onboarding page.
    """

    user = request.user

    # Onboarding is not enabled, or the user is a superadmin.
    if not config.ONBOARDING_ENABLED or (user.is_authenticated and user.is_superadmin):
        return False

    # User comes from authorization process but has no account on this subsite yet.
    if not user.is_authenticated:
        return bool(request.session.get("onboarding_claims", None))

    # User has never been at the onboarding page.
    if not user.onboarding_complete:
        return True

    # User has been at the onbaording page, but some mandatory fields are still missing.
    if config.ONBOARDING_FORCE_EXISTING_USERS and not user.is_profile_complete:
        return True

    return False


def is_access_request_required(request):
    user = request.user
    claims = request.session.get("onboarding_claims", None)

    if user.is_authenticated or not claims:
        return False

    return (
        not config.ALLOW_REGISTRATION
        and not claims.get("is_admin", False)
        and claims.get("email").split("@")[1]
        not in config.DIRECT_REGISTRATION_DOMAINS  # Approval can be skipped for configured email domains
    )


def lcs(X, Y):
    """
    Longest common subsequence of two strings
    """

    m = len(X)
    n = len(Y)

    # declaring the array for storing the dp values
    L = [[None] * (n + 1) for i in range(m + 1)]

    # Following steps build L[m + 1][n + 1] in bottom up fashion
    # Note: L[i][j] contains length of LCS of X[0..i-1]
    # and Y[0..j-1]
    for i in range(m + 1):
        for j in range(n + 1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif X[i - 1] == Y[j - 1]:
                L[i][j] = L[i - 1][j - 1] + 1
            else:
                L[i][j] = max(L[i - 1][j], L[i][j - 1])

                # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1]

    return L[m][n]


def lcs_similarity(left, right):
    title_lcs_value = lcs(left, right)
    length = max(len(left), len(right))
    return title_lcs_value / length


def format_list(possible_list, default="", exclusive=False):
    if isinstance(possible_list, str):
        return possible_list
    if len(possible_list) == 0:
        return default
    if len(possible_list) == 1:
        return possible_list[0]

    left = possible_list[:-1]
    right = possible_list[-1:]
    if exclusive:
        return _("%(left)s or %(right)s") % {
            "left": ", ".join(left),
            "right": right[0],
        }
    return _("%(left)s and %(right)s") % {
        "left": ", ".join(left),
        "right": right[0],
    }


class CspHeaderExceptionConfig:
    SOURCES = [
        "connect-src",
        "default-src",
        "font-src",
        "frame-src",
        "frame-ancestors",
        "img-src",
        "media-src",
        "object-src",
        "script-src",
        "style-src",
        "form-action",
    ]

    def __len__(self):
        return len(self.exceptions)

    def __init__(self):
        self.exceptions = []
        for exception in config.CSP_HEADER_EXCEPTIONS:
            if isinstance(exception, str):
                self.exceptions.append(
                    {
                        "url": exception,
                        "types": ["img-src", "frame-src"],
                    }
                )
            else:
                self.exceptions.append(exception)

    @staticmethod
    def is_valid_url(url):
        if url == "data:":
            return True

        if url == "https://":
            return False

        return url and url.startswith("https://")

    def as_csp_dict(self):
        result = {}
        for source in self.SOURCES:
            if exceptions := self.filter(source):
                result[source] = exceptions
        return result

    def filter(self, type):
        if type not in self.SOURCES:
            return []
        return [
            exception["url"]
            for exception in self.exceptions
            if type in exception["types"] and self.is_valid_url(exception["url"])
        ]

    def save(self):
        config.CSP_HEADER_EXCEPTIONS = self.exceptions


def allow_create(model):
    try:
        type = model.content_type_from_class()
    except Exception:
        type = model.__name__.lower()
    return (
        type in config.OPEN_FOR_CREATE_CONTENT_TYPES or type in PROTECTED_CONTENT_TYPES
    )


def view_next(url, next_url):
    url_parts = list(urlparse(url))

    query = parse_qs(url_parts[4])
    url_parts[4] = urlencode({**query, "next": next_url}, doseq=True)

    return urlunparse(url_parts)


def safe_file_path(base_path, *args):
    # Resolve the absolute path of the user-supplied filename
    base_path = os.path.abspath(base_path)
    filename = os.path.join(base_path, *args)

    absolute_path = os.path.abspath(filename)

    # Check that the absolute path starts with the base path
    if not absolute_path.startswith(base_path):
        msg = "Invalid file path"
        raise ValueError(msg)

    return absolute_path


def safe_open_file(base_path, *args, mode="r"):
    return open(safe_file_path(base_path, *args), mode=mode)


def maybe_prop(tree, prop, wrapper=None):
    def wrap(value):
        return wrapper(value) if (wrapper and value) else value

    return {prop: wrap(tree.get(prop))} if tree.get(prop) else {}
