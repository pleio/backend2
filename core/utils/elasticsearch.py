from django.contrib.auth.models import AnonymousUser
from django.utils import dateparse, timezone
from django.utils.text import Truncator
from django_elasticsearch_dsl.registries import registry
from django_tenants.utils import schema_context
from elasticsearch_dsl import A, Q, Search
from graphql import GraphQLError

from core import config
from core.constances import INVALID_DATE, SEARCH_ORDER_BY
from core.lib import add_url_params, get_acl, get_base_url, tenant_schema
from core.models import Tag
from core.models.tags import flat_category_tags
from core.utils.convert import tiptap_to_text, truncate_rich_description
from user.models import User


class SchemaAcl:
    def __init__(self, acl, is_admin):
        self.acl = [*acl] if acl else []
        self.is_admin = is_admin


def _or(queries):
    result = None
    for item in queries:
        if result:
            result = result | item
        else:
            result = item
    return result


class QueryBuilder:
    def __init__(
        self,
        q,
        user,
        search_related_sites=False,
    ):
        self.q = q
        self.user = user
        self.search_related_sites = search_related_sites
        self.s = (
            Search(index="_all")
            .query(_or(self._query_sites()) & self._query_published())
            .exclude("term", is_active=False)
        )

    def _query_sites(self):
        for schema, access in self._available_sites():
            filter_content = self._content_filter()

            # only filter profile fields for the current schema
            if schema == tenant_schema():
                filter_content = filter_content | self._profile_fields_filter(access)

            if not access.is_admin:
                filter_content = filter_content & self._content_access(access)

            with schema_context(schema):
                if excluded_types := config.SEARCH_EXCLUDED_CONTENT_TYPES:
                    filter_content = filter_content & ~Q("terms", type=excluded_types)

            yield Q("bool", must=[Q("term", tenant_name=schema), filter_content])

    def _available_sites(self):
        is_admin = self.user.is_authenticated and self.user.is_site_admin
        yield tenant_schema(), SchemaAcl(get_acl(self.user), is_admin)
        if not self.search_related_sites or self.user.is_anonymous:
            return
        for schema in config.SEARCH_RELATED_SCHEMAS:
            with schema_context(schema):
                other_user = (
                    User.objects.exclude(external_id=None)
                    .filter(external_id=self.user.external_id)
                    .filter(is_active=True)
                    .first()
                ) or AnonymousUser()
                is_admin = other_user.is_authenticated and other_user.is_site_admin
                yield schema, SchemaAcl(get_acl(other_user), is_admin)

    def _content_filter(self):
        from core.resolvers.shared import get_user_match_fields

        # Hier moet het e-mail veld weggelaten worden als HasUserManagementPermission niet valideert.
        return Q(
            "simple_query_string",
            query=self.q,
            fields=[
                "title^3",
                "name^3",
                "description",
                "tags_matches^3",
                "file_contents",
                "introduction",
                "comments.description",
                "owner.name",
                *get_user_match_fields(self.user),
            ],
        )

    @staticmethod
    def _content_access(access):
        return Q("bool", should=[Q("match", read_access=acl) for acl in access.acl])

    def _profile_fields_filter(self, access):
        query_filter = [
            Q(
                "simple_query_string",
                query=self.q,
                fields=["user_profile_fields.value"],
            )
        ]
        if not access.is_admin:
            shoulds = [
                Q("match", user_profile_fields__read_access=acl) for acl in access.acl
            ]
            access_filter = [Q("bool", should=shoulds)]
        else:
            access_filter = []

        return Q(
            "nested",
            path="user_profile_fields",
            ignore_unmapped=True,
            query=Q("bool", must=query_filter + access_filter),
        )

    @staticmethod
    def _query_published():
        """
        Filter
          1. published content
          2. groups
          3. users; from the current schema only
        """
        return (
            Q("range", published={"gt": None, "lte": timezone.now()})
            | Q("term", type="group")
            | (Q("term", type="user") & Q("term", tenant_name=tenant_schema()))
        )

    def filter_from_to_dates(self, date_from, date_to):
        try:
            self.s = self.s.filter(
                "range",
                created_at={
                    "gte": dateparse.parse_datetime(date_from) if date_from else None,
                    "lte": dateparse.parse_datetime(date_to) if date_to else None,
                },
            )
        except ValueError:
            raise GraphQLError(INVALID_DATE)

    def maybe_filter_recommended_in_search(self, is_recommended):
        if is_recommended:
            self.s = self.s.filter("term", is_recommended_in_search=True)

    def maybe_filter_owners(self, owner_guids):
        if owner_guids:
            self.s = self.s.filter("terms", owner_guid=owner_guids)

    def maybe_filter_subtypes(self, subtypes):
        if subtypes:
            self.s = self.s.query("terms", type=subtypes)

    def maybe_filter_container(self, container_guid):
        # Filter on container_guid (group.guid)
        if container_guid:
            self.s = self.s.filter("term", container_guid=container_guid)

    def maybe_filter_tags(self, tags, strategy):
        if tags:
            tags_matches = list(Tag.translate_tags(tags))
            if strategy == "any":
                self.s = self.s.filter("terms", tags_matches=tags_matches)
            else:
                for tag in tags_matches:
                    self.s = self.s.filter("terms", tags_matches=[tag])

    def maybe_filter_categories(self, categories, strategy):
        if categories:
            for category in categories:
                matches = [*flat_category_tags(category)]
                if matches:
                    if strategy != "all":
                        # Categories: match-any
                        self.s = self.s.filter("terms", category_tags=matches)
                    else:
                        # match-all
                        for single_match in matches:
                            self.s = self.s.filter(
                                "terms", category_tags=[single_match]
                            )

    def filter_archived(self, filter_archived):
        self.s = self.s.filter("term", is_archived=bool(filter_archived))

    def order_by(self, sort_order, direction):
        if sort_order == SEARCH_ORDER_BY.title:
            self.s = self.s.sort({"title.raw": {"order": direction}})
        elif sort_order == SEARCH_ORDER_BY.timeCreated:
            self.s = self.s.sort({"created_at": {"order": direction}})
        elif sort_order == SEARCH_ORDER_BY.timePublished:
            self.s = self.s.sort({"published": {"order": direction}})
        else:
            self.s = self.s.sort({"_score": {"order": "desc"}})

    def add_aggregation(self):
        a = A("terms", field="type")
        self.s.aggs.bucket("type_terms", a)

    def execute(self, offset, limit):
        search_query = self.s[offset : offset + limit]
        return search_query, search_query.execute()


class RelatedSiteSearchResult:
    """
    This class is used to represent an entity from another site.
    """

    default_data = {
        "guid": "",
        "tenant_schema": "",
        "site_name": "",
        "site_url": "",
        "title": "",
        "subtype": "",
        "relative_url": "",
        "abstract": "",
        "rich_description": "",
        "description": "",
        "published": None,
        "start_date": None,
        "end_date": None,
        "mime_type": None,
    }

    def __init__(self, **kwargs):
        for key, value in self.default_data.items():
            setattr(self, key, kwargs.get(key, value))

    @staticmethod
    def from_model(model):
        """
        Create an RelatedSiteSearchResult from a model. This should be executed in correct tenant context.
        """
        from core.models import Entity, Group
        from user.models import User

        data = {
            "guid": str(model.guid),
            "tenant_schema": tenant_schema(),
            "site_name": config.NAME,
            "site_url": get_base_url(),
            "relative_url": model.url,
        }

        if isinstance(model, Entity):
            data.update({"subtype": model.type_to_string})
            for field in [
                "title",
                "published",
                "abstract",
                "rich_description",
                "mime_type",
                "download_url",
                "start_date",
                "end_date",
                "desc",
            ]:
                if hasattr(model, field):
                    data.update({field: getattr(model, field)})
        if isinstance(model, User):
            data.update({"title": model.name, "subtype": "user"})
        if isinstance(model, Group):
            data.update({"title": model.name, "subtype": "group"})

        return RelatedSiteSearchResult(**data)

    @property
    def url(self):
        relative_path = add_url_params(self.relative_url, __auth=True)
        return f"{self.site_url}{relative_path}"

    @property
    def excerpt(self):
        if self.abstract:
            return tiptap_to_text(self.abstract)

        if self.rich_description:
            return truncate_rich_description(self.rich_description)

        return Truncator(self.description).words(26)


def list_indices(models=None):
    indices = {i._name: i for i in registry.get_indices()}
    for document in registry.get_documents():
        if not models or document.Django.model in models:
            yield indices[document.Index.name]


def list_documents(models=None):
    for doc in registry.get_documents():
        if not models or doc.Django.model in models:
            yield doc
