from django.utils.translation import gettext_lazy as _

from core import config


class BaseRichTextEditorFeature:
    key = None
    label = None

    def __init__(self, user):
        self.user = user

    def is_enabled(self):
        raise NotImplementedError()


class AllowTextCssClassesFeature(BaseRichTextEditorFeature):
    """
    Allow admin to add classes to elements in the rich text editor.
    """

    key = "allowTextCssClasses"
    label = _("Site administrators can add custom classes to elements")

    def is_enabled(self):
        is_admin = getattr(self.user, "is_site_admin", False)
        return is_admin and self.key in config.RICH_TEXT_EDITOR_FEATURES


FEATURES_AVAILABLE = (AllowTextCssClassesFeature,)
