from django.utils.timezone import datetime


def maybe_date(value):
    try:
        return datetime.strptime(value, "%Y-%m-%d").date()
    except (ValueError, TypeError):
        return value


def maybe_datetime(value):
    try:
        value = maybe_date(value)
        return datetime.fromisoformat(value)
    except (ValueError, TypeError):
        return value


def force_date_format(value, fmt):
    if value:
        try:
            datetime = maybe_datetime(value)
            return datetime.strftime(fmt)
        except Exception:
            pass
    return value
