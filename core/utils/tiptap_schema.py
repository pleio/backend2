from prosemirror.model import Schema

from core.lib import maybe_prop

P_DOM = ["p", 0]
BLOCKQUOTE_DOM = ["blockquote", 0]
BR_DOM = ["br"]
OL_DOM = ["ol", 0]
UL_DOM = ["ul", 0]
LI_DOM = ["li", 0]
TABLE_DOM = ["table", 0]
TABLE_ROW_DOM = ["tr", 0]


def parse_table_cell(tag):
    return {
        "tag": tag,
        "getAttrs": lambda node: {
            **maybe_prop(node, "colspan", int),
            **maybe_prop(node, "rowspan", int),
        },
        "priority": 0,
    }


def table_cell_to_dom(tag):
    return lambda node: [
        tag,
        {
            "colspan": str(node.attrs.get("colspan", 1)),
            "rowspan": str(node.attrs.get("rowspan", 1)),
        },
        0,
    ]


TABLE_CELL_ATTRS = {
    "colspan": {"default": 1},
    "rowspan": {"default": 1},
}

nodes = {
    "doc": {"content": "block+"},
    "paragraph": {
        "content": "inline*",
        "group": "block",
        "parseDOM": [{"tag": "p", "priority": 10}],
        "toDOM": lambda _: P_DOM,
    },
    "paragraph_bold": {
        "content": "inline*",
        "group": "block",
        "parseDOM": [
            {
                "tag": "p",
                "getAttrs": lambda node: (
                    {"class": "bold"}
                    if ("bold" in node.get("class", "").split())
                    else False
                ),
                "priority": 20,
            }
        ],
        "toDOM": lambda node: ["p", {"class": "bold"}, 0],
    },
    "blockquote": {
        "content": "block+",
        "group": "block",
        "defining": True,
        "parseDOM": [{"tag": "blockquote"}],
        "toDOM": lambda _: BLOCKQUOTE_DOM,
    },
    "heading": {
        "attrs": {"level": {"default": 2}},
        "content": "inline*",
        "group": "block",
        "defining": True,
        "parseDOM": [
            {"tag": "h1", "attrs": {"level": 2}},
            {"tag": "h2", "attrs": {"level": 2}},
            {"tag": "h3", "attrs": {"level": 3}},
            {"tag": "h4", "attrs": {"level": 4}},
            {"tag": "h5", "attrs": {"level": 5}},
            {"tag": "h6", "attrs": {"level": 5}},
        ],
        "toDOM": lambda node: [f"h{node.attrs['level']}", 0],
    },
    "text": {"group": "inline"},
    "mention": {
        "inline": True,
        "attrs": {
            "id": {"default": None},
            "label": {"default": None},
            "href": {"default": None},
        },
        "group": "inline",
        "parseDOM": [{"tag": "a[href]:not([data-type='file'])"}],
        "toDOM": lambda node: [
            "a",
            {
                "href": node.attrs["href"],
                "alt": node.attrs["label"],
                "title": node.attrs["label"],
                "target": "_blank",
            },
            "@" + node.attrs["label"],
        ],
    },
    "image": {
        "inline": True,
        "attrs": {
            "src": {},
            "alt": {"default": None},
            "title": {"default": None},
            "size": {"default": None},
            "caption": {"default": None},
        },
        "group": "inline",
        "draggable": True,
        "parseDOM": [
            {
                "tag": "img[src]",
                "getAttrs": lambda e: {
                    "src": e.get("src"),
                    "alt": e.get("alt"),
                    "title": e.get("title"),
                    "size": e.get("size"),
                    "caption": e.get("caption"),
                },
            }
        ],
        "toDOM": lambda node: [
            "img",
            {
                "src": node.attrs["src"],
                "alt": node.attrs["alt"],
                "title": node.attrs["title"],
                "size": node.attrs["size"],
                "caption": node.attrs["caption"],
            },
        ],
    },
    "figure": {
        "inline": False,
        "attrs": {
            "src": {},
            "alt": {"default": None},
            "title": {"default": None},
            "size": {"default": None},
            "caption": {"default": None},
        },
        "group": "block",
        "draggable": False,
        "parseDOM": [{"tag": "figure"}],
        "toDOM": lambda node: [
            "img",
            {
                "src": node.attrs["src"],
                "alt": node.attrs["alt"],
                "title": node.attrs["title"],
                "size": node.attrs["size"],
                "caption": node.attrs["caption"],
            },
        ],
    },
    "file": {
        "inline": False,
        "attrs": {
            "url": {"default": None},
            "mimeType": {"default": None},
            "name": {"default": None},
            "size": {"default": None},
        },
        "group": "block",
        "draggable": False,
        "parseDOM": [
            {
                "priority": 60,
                "tag": "a[data-type='file']",
                "getAttrs": lambda e: {
                    "url": e.get("href"),
                    "name": e.get("data-name"),
                    "mimeType": e.get("data-mime-type"),
                    "size": e.get("data-size"),
                },
            }
        ],
        "toDOM": lambda node: [
            "a",
            {
                "href": node.attrs["url"],
                "data-type": "file",
                "data-mime-type": node.attrs["mimeType"],
                "data-size": str(node.attrs["size"]),
            },
            node.attrs["name"] or "",
        ],
    },
    "video": {
        "inline": False,
        "attrs": {
            "guid": {"default": None},
            "platform": {"default": None},
            "title": {"default": None},
        },
        "group": "block",
        "draggable": False,
        "parseDOM": [{"tag": "iframe"}],
        "toDOM": lambda node: [
            # TODO: build video view
            "iframe",
            {
                "src": node.attrs["platform"],
            },
        ],
    },
    "hardBreak": {
        "inline": True,
        "group": "inline",
        "selectable": False,
        "parseDOM": [{"tag": "br"}],
        "toDOM": lambda _: BR_DOM,
    },
    "bulletList": {
        "group": "block",
        "content": "listItem+",
        "parseDOM": [{"tag": "ul"}],
        "toDOM": lambda _: UL_DOM,
    },
    "orderedList": {
        "attrs": {"order": {"default": 1}},
        "group": "block",
        "content": "listItem+",
        "parseDOM": [{"tag": "ol"}],
        "toDOM": lambda node: (
            OL_DOM
            if node.attrs.get("order") == 1
            else ["ol", {"start": node.attrs["order"]}, 0]
        ),
    },
    "listItem": {
        "parseDOM": [{"tag": "li"}],
        "defining": True,
        "content": "paragraph block*",
        "toDOM": lambda _: LI_DOM,
    },
    "table": {
        "parseDOM": [{"tag": "table"}],
        "group": "block",
        "content": "tableRow+",
        "toDOM": lambda _: TABLE_DOM,
    },
    "tableRow": {
        "parseDOM": [{"tag": "tr"}],
        "content": "block+",
        "toDOM": lambda _: TABLE_ROW_DOM,
    },
    "tableHeader": {
        "attrs": TABLE_CELL_ATTRS,
        "group": "block",
        "parseDOM": [parse_table_cell("th")],
        "content": "paragraph block*",
        "toDOM": table_cell_to_dom("th"),
    },
    "tableCell": {
        "attrs": TABLE_CELL_ATTRS,
        "group": "block",
        "parseDOM": [parse_table_cell("td")],
        "content": "paragraph block*",
        "toDOM": table_cell_to_dom("td"),
    },
}

ITALIC_DOM = ["em", 0]
BOLD_DOM = ["strong", 0]
UNDERLINE_DOM = ["u", 0]
STRIKETHROUGH_DOM = ["s", 0]

PARSE_LANG_ATTR = {
    "getAttrs": lambda node: {"lang": node.get("lang")} if node.get("lang") else False
}
PARSE_ClASS_ATTR = {
    "getAttrs": lambda node: (
        {"class": node.get("class")} if node.get("class") else False
    )
}

marks = {
    "lang": {
        "attrs": {"lang": {}},
        "toDOM": lambda node, _: ["span", {"lang": node.attrs["lang"]}, 0],
        "parseDOM": [
            {"tag": "span", **PARSE_LANG_ATTR},
        ],
    },
    "link": {
        "attrs": {"href": {}, "title": {"default": None}, "target": {"default": None}},
        "inclusive": False,
        "parseDOM": [
            {
                "tag": "a[href]",
                "getAttrs": lambda e: {
                    "href": e.get("href"),
                    "title": e.get("title"),
                    "target": e.get("target"),
                },
            }
        ],
        "toDOM": lambda node, _: [
            "a",
            {
                "href": node.attrs["href"],
                "title": node.attrs["title"],
                "target": node.attrs["target"],
            },
            0,
        ],
    },
    "italic": {
        "parseDOM": [{"tag": "i"}, {"tag": "em"}],
        "toDOM": lambda _, __: ITALIC_DOM,
    },
    "bold": {
        "parseDOM": [{"tag": "strong"}, {"tag": "b"}],
        "toDOM": lambda _, __: BOLD_DOM,
    },
    "underline": {"parseDOM": [{"tag": "u"}], "toDOM": lambda _, __: UNDERLINE_DOM},
    "strike": {
        "parseDOM": [{"tag": "s"}, {"tag": "del"}, {"tag": "strike"}],
        "toDOM": lambda _, __: STRIKETHROUGH_DOM,
    },
    "cssclass": {
        "attrs": {"class": {}},
        "parseDOM": [
            {"tag": "span", **PARSE_ClASS_ATTR},
        ],
        "toDOM": lambda node, _: ["span", {"class": node.attrs["class"]}, 0],
    },
    "anchor": {
        "attrs": {"id": {}},
        "parseDOM": [{"tag": "[id]"}],
        "toDOM": lambda node, _: ["span", {"id": node.attrs["id"]}, 0],
    },
}

tiptap_schema = Schema({"nodes": nodes, "marks": marks})
