from django.db.models import Q

from core.models import Comment, PublishRequest, SiteAccessRequest
from user.admin_permissions import (
    HasPublicationRequestManagementPermission,
    HasUserManagementPermission,
)
from user.models import User


class AdminTaskCountersBase:
    key = None
    validator = None

    def __init__(self, user):
        self.user = user

    def count(self):
        if self._has_permission():
            return self._count()
        return 0

    def _has_permission(self):
        validator = self.validator(self.user)
        return validator.has_permission()

    def _count(self):
        raise NotImplementedError


class DeleteAccountRequestsCount(AdminTaskCountersBase):
    key = "deleteAccountRequests"
    validator = HasUserManagementPermission

    def _count(self):
        return User.objects.filter(is_delete_requested=True).count()


class SiteAccessRequestsCount(AdminTaskCountersBase):
    key = "siteAccessRequests"
    validator = HasUserManagementPermission

    def _count(self):
        return SiteAccessRequest.objects.filter_status(status="unprocessed").count()


class ContentPublicationRequestsCount(AdminTaskCountersBase):
    key = "contentPublicationRequests"
    validator = HasPublicationRequestManagementPermission

    def _count(self):
        constraint = Q(_entity__filefolder__isnull=True)
        content_publish_requests = PublishRequest.objects.filter(constraint)
        return content_publish_requests.filter_status("open").count()


class FilePublicationRequestsCount(AdminTaskCountersBase):
    key = "filePublicationRequests"
    validator = HasPublicationRequestManagementPermission

    def _count(self):
        constraint = Q(_entity__filefolder__isnull=False)
        content_publish_requests = PublishRequest.objects.filter(constraint)
        return content_publish_requests.filter_status("open").count()


class CommentPublicationRequestsCount(AdminTaskCountersBase):
    key = "commentPublicationRequests"
    validator = HasPublicationRequestManagementPermission

    def _count(self):
        return Comment.objects.filter(comment_moderation_request__isnull=False).count()
