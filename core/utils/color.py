from colour import Color


def hex_black_or_white(hex_background_color):
    """
    Calculates the luminance of a color and returns black or white depending on the luminance.

    https://stackoverflow.com/a/3943023
    """
    try:
        color = hex_background_color[1:]

        hex_red = int(color[0:2], base=16)
        hex_green = int(color[2:4], base=16)
        hex_blue = int(color[4:6], base=16)
    except Exception:
        return "#000000"

    luminance = hex_red * 0.299 + hex_green * 0.587 + hex_blue * 0.114

    if luminance > 150:
        return "#000000"
    else:
        return "#ffffff"


def hex_color_tint(hex_color, weight=0.5):
    try:
        color = Color(hex_color)
    except AttributeError:
        return hex_color

    newR = color.rgb[0] + (1 - color.rgb[0]) * weight
    newG = color.rgb[1] + (1 - color.rgb[1]) * weight
    newB = color.rgb[2] + (1 - color.rgb[2]) * weight
    new = Color(rgb=(newR, newG, newB))
    return new.hex
