from concierge.api.client import ConciergeClient
from entities.file.models import FileFolder, FileReference


class FetchAvatarFile:
    def __init__(self, user, url):
        self.user = user
        self.url = url
        self.file = None

    def process(self):
        self._fetch_file()
        self._scan_file()
        self._preserve_file()
        return self.file

    def _fetch_file(self):
        try:
            if not self.url or not isinstance(self.url, str):
                return

            client = ConciergeClient("fetch avatar from url")
            downloaded_file = client.get_file(self.url + "?size=600")
            assert client.response.ok, f"Failed to fetch avatar from {self.url}"
            self.file = FileFolder.objects.create(
                owner=self.user, upload=downloaded_file
            )
        except Exception:
            pass

    def _scan_file(self):
        if self.file and not self.file.scan():
            self.file.delete()
            self.file = None

    def _preserve_file(self):
        FileReference.objects.update_avatar_file(self.user, self.file)
