from collections import defaultdict

from core.models import Group, GroupMembership
from user.models import User


def _is_match(actual_value, expected_value, field_type):
    if field_type == "select_field":
        return expected_value and ((actual_value or "") in expected_value)
    elif field_type == "multi_select_field":
        actual_value_list = [v.strip() for v in (actual_value or "").split(",")]
        return len({*actual_value_list} & {*(expected_value or [])}) > 0
    return False


class AutoMemberProfileField:
    def __init__(self, changing_user):
        self.changing_user = changing_user
        self.groups = self._collect_groups()
        self.original_values = {}

    @staticmethod
    def _collect_groups():
        result = defaultdict(list)
        for group in Group.objects.all():
            for field in group.auto_membership_fields:
                result[field["guid"]].append(
                    {
                        "group": group,
                        "match_value": field["value"],
                    }
                )
        return result

    def log_previous_value(self, field):
        self.original_values[field.profile_field.guid] = field.serialize_value()

    def apply(self, field):
        if field.serialize_value() == self.original_values.get(
            field.profile_field.guid
        ):
            # If the value hasn't changed, don't do anything
            return
        for group_value in self.groups.get(field.profile_field.guid, []):
            if _is_match(
                field.value, group_value["match_value"], field.profile_field.field_type
            ):
                group_value["group"].join(self.changing_user, "member")


class AutoMemberProfileFieldAtGroup:
    def __init__(self, group):
        self.group = group
        self.current_fields = {
            field["guid"]: field["value"] for field in self.group.auto_membership_fields
        }

    def _get_changed_fields(self):
        new_fields = {
            field["guid"]: field["value"] for field in self.group.auto_membership_fields
        }
        changed_fields = []

        for guid, value in self.current_fields.items():
            if guid in new_fields:
                if value != new_fields[guid]:
                    changed_fields.append(guid)
        for guid in new_fields.keys():
            if guid not in self.current_fields:
                changed_fields.append(guid)

        return {
            changed_field: new_fields[changed_field] for changed_field in changed_fields
        }

    def apply(self):
        changed_fields = self._get_changed_fields()
        if not changed_fields:
            return

        changed_field_keys = list(changed_fields.keys())
        new_members = set()
        members = GroupMembership.objects.filter(group=self.group)
        if members.exists():
            member_ids = members.values_list("user_id", flat=True)
            non_members = User.objects.exclude(id__in=member_ids)
        else:
            non_members = User.objects.all()
        non_members_with_relevant_profile = non_members.filter(
            _profile__user_profile_fields__profile_field__id__in=changed_field_keys
        )
        for user in non_members_with_relevant_profile:
            for guid, expected_value in changed_fields.items():
                field = user.profile.user_profile_fields.filter(
                    profile_field__id=guid
                ).first()
                if field and _is_match(
                    field.value, expected_value, field.profile_field.field_type
                ):
                    new_members.add(user)
        for user in new_members:
            self.group.join(user, "member")
