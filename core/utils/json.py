import json
import logging

from django.core.serializers.json import DjangoJSONEncoder
from django.utils.timezone import datetime

from core.utils.datetime import maybe_datetime

logger = logging.getLogger(__name__)


def _object_hook(value):
    if isinstance(value, dict):
        return {k: _object_hook(v) for k, v in value.items()}
    elif isinstance(value, list):
        return [_object_hook(v) for v in value]
    return maybe_datetime(value)


class JSONDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("object_hook", _object_hook)
        super().__init__(*args, **kwargs)


class JSONEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return super().default(obj)


def json_dumps(data):
    """Time-Aware JSON encoder."""
    return json.dumps(data, cls=JSONEncoder)


def json_loads(data):
    """Time-Aware JSON decoder."""
    return json.loads(data, cls=JSONDecoder)
