from core import config
from core.models.entity import PublishRequest


class ContentModerationTrackTimePublished:
    """
    This class is used to create a PublishRequest when and content moderation is enabled and the published
    field is changed.

    1. Remember the current value of the published field using the constructor.
    2. Call maybe_revert_time_published() before the entity has been saved.
    3. Call maybe_create_publish_request() after the entity has been saved.
    """

    def __init__(self, entity, acting_user, is_new=False):
        self.entity = entity
        self.previously_published = None if is_new else entity.published
        self.acting_user = acting_user
        self.published = None
        self._should_create_request = False

    def maybe_revert_time_published(self):
        # Remember new value of is-published.
        self.published = self.entity.published
        if self.previously_published != self.published:
            if not self.can_update():
                self.entity.published = self.previously_published
                self._should_create_request = True

    def can_update(self):
        # Action "clear publish date" is fine.
        if not self.published or not config.CONTENT_MODERATION_ENABLED:
            return True

        type, moderation_types = self._get_type_and_moderation_types()
        if type not in moderation_types:
            return True

        if self.is_staff():
            return True

        if self.is_group_staff():
            return True

        return False

    def is_staff(self):
        return self.acting_user.is_editor

    def is_group_staff(self):
        group = self.entity.group
        return group and (group.is_staff(self.acting_user))

    def maybe_create_publish_request(self):
        if self._should_create_request:
            PublishRequest.objects.create(
                entity=self.entity, time_published=self.published
            )

    def _get_type_and_moderation_types(self):
        if self.entity.type_to_string == "comment":
            return (
                self.entity.container.type_to_string,
                config.REQUIRE_COMMENT_MODERATION_FOR,
            )
        return self.entity.type_to_string, config.REQUIRE_CONTENT_MODERATION_FOR
