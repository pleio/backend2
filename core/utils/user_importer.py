import logging
import traceback

from concierge.api.queries import get_or_create_user
from core.lib import access_id_to_acl
from core.models import ProfileField, UserProfileField
from user.models import User

STATUS_CREATED = "Created"
STATUS_UPDATED = "Updated"
STATUS_INCOMPLETE_INPUT = "Incomplete input"
IDP_STATUS_CREATED = "Created login for user"
IDP_STATUS_ALREADY_EXISTS = "Login already exists"

logger = logging.getLogger(__name__)


class UserImporter:
    def __init__(self, record, fields):
        self.record = record
        self.fields = fields
        self.status = STATUS_UPDATED
        self.idp_status = IDP_STATUS_ALREADY_EXISTS
        self.user = None

    def apply(self):
        data = self.get_import_users_data()
        self.user: User = self._get_or_create_user(data)

        if self.user:
            self._apply_profile_fields(data)
            self._ensure_account_user_exists()

        return self.status

    def get_import_users_data(self):
        data = {}
        for field in self.fields:
            field["value"] = self.clean_value(self.record[field["csvColumn"]])
            data[field["userField"]] = field
        return data

    @staticmethod
    def clean_value(value):
        return str(value).strip() if value else value

    @staticmethod
    def get_import_users_user(data):
        if "id" in data:
            try:
                return User.objects.get(id=data["id"]["value"])
            except Exception:
                pass
        if "email" in data:
            try:
                return User.objects.get(email__iexact=data["email"]["value"])
            except Exception:
                pass

    def _get_or_create_user(self, data):
        user: User = self.get_import_users_user(data)

        if not user:
            if (
                "name" in data
                and "email" in data
                and data["name"]["value"]
                and data["email"]["value"]
            ):
                try:
                    user = User.objects.create(
                        email=data["email"]["value"].lower(), name=data["name"]["value"]
                    )
                    self.status = STATUS_CREATED
                except Exception as e:
                    self.status.msg = "Error: %s" % e
            else:
                self.status = STATUS_INCOMPLETE_INPUT
        else:
            self.status = STATUS_UPDATED

        return user

    def _apply_profile_fields(self, data):
        # create profile fields
        for field, values in {
            d: data[d] for d in data if d not in ["id", "email", "name"]
        }.items():
            profile_field = ProfileField.objects.get(id=field)

            if profile_field:
                (
                    user_profile_field,
                    created,
                ) = UserProfileField.objects.get_or_create(
                    profile_field=profile_field, user_profile=self.user.profile
                )

                user_profile_field.value = values["value"]

                if created:
                    user_profile_field.read_access = access_id_to_acl(
                        self.user, values["accessId"]
                    )
                elif values["forceAccess"]:
                    user_profile_field.read_access = access_id_to_acl(
                        self.user, values["accessId"]
                    )

                user_profile_field.save()

    def _ensure_account_user_exists(self):
        try:
            if self.user.external_id:
                return

            response = get_or_create_user(self.user.email, self.user.name)

            if response.get("error"):
                self.idp_status = "error: %s" % response.get("error")
            else:
                self.idp_status = (
                    IDP_STATUS_CREATED
                    if response.get("_is_new")
                    else IDP_STATUS_ALREADY_EXISTS
                )
                self.user.external_id = response["guid"]
                self.user.save()

        except Exception as e:
            self.idp_status = "%s: %s" % (e.__class__.__name__, e)
            logger.error("Error at user %s", self.user.email)
            logger.error(traceback.format_exc())
