from django.core.exceptions import ValidationError
from django.db.models import Q
from django.utils.module_loading import import_string

from core.constances import ACCESS_TYPE
from core.lib import include_root_acl
from core.models import Group, Subgroup
from user.models import User


class GqlToAcl:
    def __init__(self, obj):
        self.read_access, self.write_access = [], []
        self.obj = obj

    def add_spec(self, spec):
        acl = None
        if spec["type"] == "public":
            acl = "public"
        elif spec["type"] == "loggedIn":
            acl = "logged_in"
        elif spec["type"] == "group":
            acl = self._try_access_item_from_gql(
                spec.get("guid"), "core.models.Group", ACCESS_TYPE.group
            )
        elif spec["type"] == "subGroup":
            acl = self._try_access_item_from_gql(
                int(spec["guid"]),
                "core.models.Subgroup",
                ACCESS_TYPE.subgroup,
                pk_property="access_id",
            )
        elif spec["type"] == "user":
            acl = self._try_access_item_from_gql(
                spec["guid"], "user.models.User", ACCESS_TYPE.user
            )

        if acl:
            self.read_access.append(acl)
            if spec["grant"] == "readWrite":
                self.write_access.append(acl)

    @staticmethod
    def _try_access_item_from_gql(pk, model_name, access_type, pk_property="id"):
        Model = import_string(model_name)
        try:
            if obj := Model.objects.get(pk=pk):
                return access_type.format(getattr(obj, pk_property, None))
        except (Model.DoesNotExist, ValidationError):
            pass

    def get_read_access(self):
        return include_root_acl(self.obj, self.read_access)

    def get_write_access(self):
        return include_root_acl(self.obj, self.write_access)


class AclToGql:
    WEIGHT = {
        "public": (0, "public"),
        "logged_in": (1, "loggedIn"),
        "group": (2, "group"),
        "subgroup": (3, "subGroup"),
        "user": (4, "user"),
    }

    class IgnoreInvalidGuid(Exception):
        pass

    def __init__(self):
        self.read_acl = []
        self.write_acl = []

    def add_read_acl(self, acl):
        self.read_acl = {*acl}

    def add_write_acl(self, acl):
        self.write_acl = {*acl}

    def as_array(self):
        acl = []
        for spec in {*self.read_acl, *self.write_acl}:
            try:
                access_type, guid = (
                    str(spec).split(":") if ":" in spec else (spec, None)
                )
                item_weight, item_type = self.WEIGHT[access_type]

                item = {
                    "_weight": item_weight,
                    "type": item_type,
                    "grant": "readWrite" if spec in self.write_acl else "read",
                }
                if guid:
                    self.assert_valid_group(access_type, guid)
                    self.assert_valid_user(access_type, guid)
                    self.assert_valid_subgroup(access_type, guid)
                    item["guid"] = guid

                acl.append(item)
            except self.IgnoreInvalidGuid:
                pass

        return sorted(acl, key=lambda item: item["_weight"])

    def assert_valid_group(self, access_type, guid):
        try:
            if access_type != "group":
                return
            if Group.objects.filter(id=guid).exists():
                return
        except Exception:
            pass
        raise self.IgnoreInvalidGuid()

    def assert_valid_user(self, access_type, guid):
        try:
            if access_type != "user":
                return
            if User.objects.filter(id=guid).exists():
                return
        except Exception:
            pass
        raise self.IgnoreInvalidGuid()

    def assert_valid_subgroup(self, access_type, guid):
        try:
            if access_type != "subgroup":
                return
            if Subgroup.objects.filter(Q(id=guid) | Q(access_id=guid)).exists():
                return
        except Exception:
            pass
        raise self.IgnoreInvalidGuid
