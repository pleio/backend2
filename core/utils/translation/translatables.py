import json
import logging

from core import config
from core.utils.convert import is_tiptap
from core.utils.text_from_hierarchy import HierarchyFromText, TextFromHierarchy
from core.utils.translation.lib import get_language_code
from core.utils.translation.translation_vehicle import XmlTranslationVehicle
from core.utils.translation.translator import Translator, XMLTranslator

logger = logging.getLogger(__name__)


class TranslatableBase:
    TRANSLATOR_CLASS = Translator

    def __init__(self, entity):
        self.entity = entity
        self.translator = self.TRANSLATOR_CLASS(
            source_language_code=self.entity.input_language or config.LANGUAGE,
            target_language_code=get_language_code(),
        )

    def get_reference(self):
        raise NotImplementedError()

    def get_translatable_value(self):
        raise NotImplementedError()

    def as_translated_value(self, value):
        return value

    def translate(self):
        translation = self.translator.get_translation(
            value=self.get_translatable_value(),
            reference=self.get_reference(),
        )
        return self.as_translated_value(translation)


class TitleTranslatable(TranslatableBase):
    def get_reference(self):
        return "%s:%s" % (self.entity.guid, "title")

    def get_translatable_value(self):
        return self.entity.title


class SubTitleTranslatable(TranslatableBase):
    def get_reference(self):
        return "%s:%s" % (self.entity.guid, "sub_title")

    def get_translatable_value(self):
        return self.entity.sub_title


class RichDescriptionTranslatable(TranslatableBase):
    TRANSLATOR_CLASS = XMLTranslator

    def get_reference(self):
        return "%s:%s" % (self.entity.guid, "rich_description")

    def _get_value(self):
        return self.entity.rich_description

    def get_translatable_value(self):
        translatable_value = self._get_value()
        try:
            if not is_tiptap(translatable_value):
                return translatable_value
            hierarchy = json.loads(translatable_value)
            text = TextFromHierarchy(hierarchy)
            xml = XmlTranslationVehicle()
            for key, value in text.get_text():
                xml.add_text(key, value)
            return xml.as_text()
        except (json.JSONDecodeError, TypeError):
            return ""

    def as_translated_value(self, value):
        if not is_tiptap(self._get_value()):
            return super().as_translated_value(value)
        return self._as_tiptap_translated_value(value)

    def _as_tiptap_translated_value(self, value):
        try:
            hierarchy = json.loads(self._get_value())
            xml = XmlTranslationVehicle.from_text(value)
            text = HierarchyFromText(xml.translations.items())
            return json.dumps(text.apply_to(hierarchy))
        except (json.JSONDecodeError, TypeError):
            return self._get_value()


class AbstractTranslatable(RichDescriptionTranslatable):
    def get_reference(self):
        return "%s:%s" % (self.entity.guid, "abstract")

    def _get_value(self):
        return getattr(self.entity, "abstract", None) or ""


class DescriptionTranslatable(TranslatableBase):
    def __init__(self, entity, value):
        super().__init__(entity)
        self.value = value

    def get_translatable_value(self):
        return self.value

    def get_reference(self):
        return "%s:%s" % (self.entity.guid, "description")
