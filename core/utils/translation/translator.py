from hashlib import md5

from core.models import TranslationCache
from core.services.translate_service import DeeplClient


class Translator:
    def __init__(self, target_language_code, source_language_code):
        self.target_language_code = target_language_code
        self.source_language_code = source_language_code

    def get_translation(self, value, reference):
        if not value:
            return value

        hash = md5(value.encode()).hexdigest()
        qs = TranslationCache.objects.filter(
            reference=reference,
            language_code=self.target_language_code,
        )

        cache = qs.first()
        if not cache or hash != cache.hash:
            qs.delete()
            translated = self.translate(value)

            if not translated:
                return value

            cache = TranslationCache.objects.create(
                reference=reference,
                hash=hash,
                language_code=self.target_language_code,
                value=translated,
            )

        return cache.value

    def get_parameters(self):
        return {
            "target_lang": self.target_language_code.upper(),
            "source_lang": self.source_language_code.upper(),
        }

    def translate(self, text):
        client = DeeplClient()
        return client.translate(
            text=text,
            **self.get_parameters(),
        )


class XMLTranslator(Translator):
    def get_parameters(self):
        return {
            **super().get_parameters(),
            "tag_handling": "xml",
            "splitting_tags": "root,doc,par",
        }
