import logging
import xml.etree.cElementTree as ET
from io import BytesIO

logger = logging.getLogger(__name__)


class TranslationVehicleBase:
    def __init__(self):
        self.translations = {}

    def add_text(self, key, text):
        self.translations[key] = text

    def as_text(self):
        raise NotImplementedError()

    @classmethod
    def from_text(cls, text):
        raise NotImplementedError()


class XmlTranslationVehicle(TranslationVehicleBase):
    def as_text(self):
        root = ET.Element("root")
        doc = ET.SubElement(root, "doc")

        for key, text in self.translations.items():
            ET.SubElement(doc, "par", name=key).text = text

        tree = ET.ElementTree(root)
        file = BytesIO()
        tree.write(file)
        return file.getvalue().decode("utf-8")

    @classmethod
    def from_text(cls, xml):
        vehicle = cls()

        try:
            root = ET.XML(xml)
            for par in root.findall("./doc/par"):
                vehicle.add_text(par.attrib["name"], par.text)
        except ET.ParseError:
            pass
        return vehicle
