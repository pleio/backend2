from django.utils.translation import get_language

from core import config


def get_language_code():
    return get_language()[:2]


def should_use_foreign_language(content_language=None):
    content_language = content_language or config.LANGUAGE
    if not config.CONTENT_TRANSLATION:
        return False

    selected_language = get_language_code()
    return not selected_language.startswith(content_language)
