from enum import Enum as EnumBase


class Enum(EnumBase):
    @classmethod
    def exists(cls, value):
        return value in cls._member_names_
