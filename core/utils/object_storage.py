import os
from pathlib import Path
from shutil import rmtree

import boto3
import botocore.client
from botocore.exceptions import ClientError
from django.conf import settings

from core.lib import safe_file_path


class ObjectStorageClient:
    def __init__(self, bucket_name):
        self.bucket_name = bucket_name
        # Due to the ODC-implementation of S3, we need two differently configured clients.
        self._l_client = None
        self._rw_client = None

    @classmethod
    def has_settings(cls):
        return all(
            [
                getattr(settings, "AWS_ACCESS_KEY_ID", False),
                getattr(settings, "AWS_SECRET_ACCESS_KEY", False),
                getattr(settings, "AWS_S3_ENDPOINT_URL", False),
            ]
        )

    @property
    def l_client(self):
        """
        The 'list' client is configured v4, but will be used to send with a v2 list_objects request.
        """
        if self._l_client is None:
            self._l_client = boto3.client(
                "s3",
                aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                endpoint_url=settings.AWS_S3_ENDPOINT_URL,
            )
        return self._l_client

    @property
    def rw_client(self):
        """
        For file-write actions we need to use a client that is configured with a v2 signature.
        """
        if self._rw_client is None:
            self._rw_client = boto3.client(
                "s3",
                aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                endpoint_url=settings.AWS_S3_ENDPOINT_URL,
                config=botocore.client.Config(signature_version="s3"),
            )
        return self._rw_client

    def is_folder(self, location):
        return self._folder_exists(location)

    def list_objects(self, subfolder=""):
        remote_path = f"{subfolder}/" if subfolder else ""
        response = self.l_client.list_objects_v2(
            Bucket=self.bucket_name, Prefix=remote_path
        )
        if "Contents" in response:
            return [obj["Key"] for obj in response["Contents"]]

    def exists(self, path):
        return self._folder_exists(path) or self._file_exists(path)

    def _folder_exists(self, subfolder):
        response = self.l_client.list_objects_v2(
            Bucket=self.bucket_name, Prefix=f"{subfolder}/", MaxKeys=1
        )
        return "Contents" in response

    def _file_exists(self, key):
        try:
            self.rw_client.head_object(Bucket=self.bucket_name, Key=key)
            return True
        except ClientError as e:
            if e.response["Error"]["Code"] == "404":
                return False
            raise

    def remove_folder(self, subfolder):
        for key in self.list_objects(subfolder):
            self.rw_client.delete_object(Bucket=self.bucket_name, Key=key)

    def upload(self, workdir, filename):
        self.rw_client.upload_file(
            safe_file_path(workdir, filename), self.bucket_name, filename
        )

    def download(self, workdir, filename):
        self.rw_client.download_file(
            self.bucket_name, filename, safe_file_path(workdir, filename)
        )

    def upload_sync(self, workdir, source_folder=""):
        directory_path = safe_file_path(workdir, source_folder)

        self.rw_client.remove_folder(source_folder)
        for root, _dirs, files in os.walk(directory_path):
            for file in files:
                file_path = Path(root) / file
                s3_key = str(
                    Path(source_folder) / file_path.relative_to(directory_path)
                )
                self.rw_client.upload_file(str(file_path), self.bucket_name, s3_key)

    def download_sync(self, workdir, source_folder=""):
        target_path = safe_file_path(workdir, source_folder)
        rmtree(target_path, ignore_errors=True)
        assert not os.path.exists(target_path), (
            f"Folder {source_folder} could not be removed"
        )
        for key in self.list_objects(source_folder):
            file_path = safe_file_path(workdir, key)
            file_dir = os.path.dirname(file_path)
            os.makedirs(file_dir)
            self.rw_client.download_file(self.bucket_name, key, str(file_path))
