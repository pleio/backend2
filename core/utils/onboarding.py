import json
import logging
from datetime import date, datetime

from django.conf import settings
from django.contrib.auth import login as auth_login
from django.shortcuts import redirect
from django.utils.html import linebreaks

from core.lib import access_id_to_acl
from core.models import ProfileField, SiteAccessRequest, UserProfileField
from core.utils.auto_member_profile_field import AutoMemberProfileField
from core.utils.convert import html_to_tiptap
from core.views.shared import access_request_exists, redirect_or_next
from user.models import User

logger = logging.getLogger(__name__)


class OnboardingProfile:
    def __init__(self, data):
        self.data = data or {}

    def value_from_field(self, field):
        field_class = get_field_class(field.field_type)
        return field_class(self.data.get(field.guid))

    def get_fields(self):
        for field in ProfileField.objects.filter_onboarding_fields():
            field.value = str(self.value_from_field(field))
            yield field


def get_field_class(field_type):
    field_classes = [DateFieldType, MultiSelectFieldType, HtmlFieldType]
    for field_class in field_classes:
        if field_type == field_class.TYPE:
            return field_class
    return FieldType


class FieldType:
    TYPE = None

    def __init__(self, value):
        self.value = value

    @classmethod
    def from_string(cls, string_value):
        return cls(string_value)

    def __str__(self):
        return str(self.value) if self.value is not None else ""


class MultiSelectFieldType(FieldType):
    TYPE = "multi_select_field"

    @classmethod
    def from_string(cls, string_value):
        return cls([v.strip() for v in string_value.split(",")])

    def __str__(self):
        return ",".join(self.value) if self.value else ""


class DateFieldType(FieldType):
    TYPE = "date_field"

    @classmethod
    def from_string(cls, string_value):
        return cls(date.fromisoformat(string_value))

    def __str__(self):
        try:
            return self.value.isoformat()
        except AttributeError:
            return ""


class HtmlFieldType(FieldType):
    TYPE = "html_field"

    @classmethod
    def plain_text_to_tiptap(cls, plain_text):
        return html_to_tiptap(linebreaks(plain_text))

    def __init__(self, value):
        try:
            if value:
                json.loads(value)
            super().__init__(value)
        except json.JSONDecodeError:
            # not tiptap yet.
            tiptap = self.plain_text_to_tiptap(value)
            super().__init__(tiptap)


class ProcessFormdataBase:
    def __init__(self, request):
        self.request = request

    def initial_values(self):
        raise NotImplementedError()

    def handle_form_valid(self, form):
        raise NotImplementedError()

    @property
    def claims(self):
        return self.request.session.get("onboarding_claims", None) or {}

    @claims.setter
    def claims(self, value):
        if not value and "onboarding_claims" in self.request.session:
            del self.request.session["onboarding_claims"]

    def get_or_create_user(self):
        user: User = self.request.user

        if not user.is_authenticated and self.claims:
            user = User.objects.get_or_create_claims(self.claims)

            logger.info("Onboarding is valid, new user %s created", user.email)

            auth_login(self.request, user, backend=settings.AUTHENTICATION_BACKENDS[0])
            self.claims = None

        return user

    def complete_user_onboarding(self, data):
        user = self.get_or_create_user()

        auto_member = AutoMemberProfileField(changing_user=user)
        onboarding_profile = OnboardingProfile(data)
        for profile_field in ProfileField.objects.filter_onboarding_fields():
            if profile_field.guid not in data:
                continue

            user_profile_field, created = UserProfileField.objects.get_or_create(
                user_profile=user.profile, profile_field=profile_field
            )
            user_profile_field.value = str(
                onboarding_profile.value_from_field(profile_field)
            )
            if created:
                user_profile_field.read_access = access_id_to_acl(user, 1)
                user_profile_field.write_access = access_id_to_acl(user, 0)
            user_profile_field.save()

            auto_member.apply(user_profile_field)

        if not user.onboarding_complete:
            user.onboarding_complete = True
            user.save()


class ProcessSiteAccessRequestFormdata(ProcessFormdataBase):
    def handle_form_valid(self, form):
        access_request = self._get_access_request()
        if not access_request:
            access_request = SiteAccessRequest.objects.create(
                email=self.claims.get("email"),
                name=self.claims.get("name"),
                claims=self.claims,
                next=self.request.GET.get("next", None),
                profile=form.cleaned_data,
            )
            access_request.send_notifications()
        elif access_request.processed:
            access_request.profile = form.cleaned_data
            access_request.processed = False
            access_request.accepted = False
            access_request.reason = None
            access_request.user_created = None
            access_request.save()
            access_request.send_notifications()
        return redirect("access_requested")

    def initial_values(self):
        if access_request := self._get_access_request():
            return access_request.profile
        return {}

    def _get_access_request(self) -> SiteAccessRequest:
        return access_request_exists(self.claims.get("email"))


class ProcessUserProfileFormdata(ProcessFormdataBase):
    def handle_form_valid(self, form):
        self.complete_user_onboarding(form.cleaned_data)
        return redirect_or_next(self.request, "/")

    def initial_values(self):
        user: User = self.request.user
        if not user.is_authenticated:
            return {}

        initial_values: dict = {}

        # get initial values from user
        user_profile_fields = UserProfileField.objects.filter(
            user_profile=user.profile
        ).all()

        for user_profile_field in user_profile_fields:
            # only set initial value if it is not null
            if user_profile_field.value:
                if user_profile_field.profile_field.field_type == "multi_select_field":
                    value = user_profile_field.value.split(",")
                elif user_profile_field.profile_field.field_type == "date_field":
                    # date is stored in format YYYY-mm-dd
                    try:
                        date = datetime.strptime(user_profile_field.value, "%Y-%m-%d")
                        value = date.strftime("%d-%m-%Y")
                    except ValueError as e:
                        logger.error("Unable to parse profile field date: %s", e)
                        value = ""
                else:
                    value = user_profile_field.value

                initial_values[user_profile_field.profile_field.guid] = value
        return initial_values
