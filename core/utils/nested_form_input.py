def parse(data, keep_index=False):
    """
    This method is used to convert a nested POST form input into a nested dictionary.

    When the following input is inserted:
    {"csrftoken": ...,
     "user[name]: "john",
     "user[phone][0][number]": "0192837465",
     "user[phone][0][tag]": "home",
     "user[phone][1][number]": "0645789123",
     "user[phone][1][tag]": "work",
     "user[custom_field][0][name]": "favorite food",
     "user[custom_field][0][value][0]": "water melon",
     "user[custom_field][0][value][1]": "cucumber",
     }

    The following output is expected:
    {"csrftoken": ...,
     "user": {
         "name": "john",
         "phone": [
             {"number": "0192837465", "tag": "home"},
             {"number": "0645789123", "tag": "work"},
         ],
         "custom_field": [{"name": "favorite food", "value": ["water melon", "cucumber"]}],
     }
    }
    """
    result = {}

    # Step 1: Parse the input into a nested dictionary
    for key, value in data.items():
        key = key.replace("]", "")
        _recursive_dict_parser(key, value, result)

    if keep_index:
        return result

    # Step 2: Convert the nested dictionary into a nested list or dictionary
    return _recursive_dict_may_be_list(result)


def _recursive_dict_parser(key, value, extend_dict):
    if "[" not in key:
        extend_dict[key] = value
        return
    left, right = key.split("[", 1)
    extend_dict.setdefault(left, {})
    _recursive_dict_parser(right, value, extend_dict[left])


def _recursive_dict_may_be_list(data):
    is_list = True
    top_index = 0

    if not isinstance(data, dict):
        return data

    # Explore if the data is a list or a dictionary.
    for key in data.keys():
        if not _is_index(key):
            is_list = False
        else:
            top_index = max(int(key), top_index)

    # Recursive call to handle nested dicts/lists.
    if is_list:
        result = []
        for i in range(0, top_index + 1):
            if str(i) in data:
                result.append(_recursive_dict_may_be_list(data[str(i)]))
    else:
        result = {}
        for key, value in data.items():
            result[key] = _recursive_dict_may_be_list(value)
    return result


def _is_index(key):
    """Test if a string may be seen as a list-index."""
    try:
        return "%s" % int(key) == key
    except (TypeError, ValueError):
        return False


__all__ = ["parse"]
