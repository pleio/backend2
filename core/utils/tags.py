import json
import logging

from core import config
from core.models import Entity, Group, UserProfile
from entities.cms.models import Page

logger = logging.getLogger(__name__)

WEIGHT_NOT_SPECIFIED = -1


class DuplicateValueError(Exception):
    pass


class ValueNotFoundError(Exception):
    pass


class MutateTagCategoryBase:
    def __init__(self):
        self.storage = TagCategoryStorage()

    def process(self):
        raise NotImplementedError()


class MigrateTagCategoryContentBase(MutateTagCategoryBase):
    def process(self):
        self.assert_valid()

        # Configuration is used as a reference for the other process_NNN_instances().
        self.process_configuration()

        self.process_entity_instances()
        self.process_group_instances()
        self.process_profile_instances()
        self.process_widget_instances()

    def assert_valid(self):
        pass

    def process_configuration(self):
        raise NotImplementedError()

    def process_entity_instances(self):
        raise NotImplementedError()

    def process_group_instances(self):
        raise NotImplementedError()

    def process_profile_instances(self):
        raise NotImplementedError()

    def process_widget_instances(self):
        raise NotImplementedError()


class AddTagCategory(MutateTagCategoryBase):
    """
    Add a new tag category to the site: A set of tags can be grouped by this name.
    """

    def __init__(self, name):
        super().__init__()
        self.name = name

    def process(self):
        if self.name in (x["name"] for x in self.storage.all_settings()):
            raise DuplicateValueError()

        self.storage.store_tag_category(self.name, {"name": self.name, "values": []})


class ExtendTagCategory(MutateTagCategoryBase):
    """
    Add a new tag to an existing tag category.
    """

    def __init__(self, name, value, synonyms, weight):
        super().__init__()
        self.category = self.storage.get_settings(name)
        self.name = name
        self.weight = weight
        self.value = value
        self.synonyms = synonyms

    def process(self):
        self.assert_valid()

        self.category["values"] = self.insert_value(self.category["values"])
        self.storage.store_tag_category(self.name, self.category)

    def assert_valid(self):
        if not self.category:
            raise ValueNotFoundError()
        if self.value in [c["value"] for c in self.category["values"]]:
            raise DuplicateValueError()

    def _value_to_store(self):
        return {"value": self.value, "synonyms": self.synonyms}

    def insert_value(self, values):
        if self.weight == WEIGHT_NOT_SPECIFIED or self.weight >= len(values):
            values.append(self._value_to_store())
        else:
            values.insert(self.weight, self._value_to_store())
        return values


class UpdateTagCategory(MigrateTagCategoryContentBase):
    """
    Update the name or weight of a tag category: the groupname and -order of a set of tags.
    """

    def __init__(self, old_name, name, weight):
        super().__init__()
        self.name = name or old_name
        self.old_name = old_name
        self.weight = weight

    def assert_valid(self):
        if self.name == self.old_name:
            # Assume the old name was valid already.
            return
        category_names = [c["name"] for c in self.storage.all_settings()]
        if self.old_name not in category_names:
            raise ValueNotFoundError()
        if self.name in category_names:
            raise DuplicateValueError()

    def process_configuration(self):
        category_tags = self.storage.get_settings(self.old_name)
        category_tags["name"] = self.name
        self.storage.store_tag_category(self.old_name, category_tags)

        if self.weight != WEIGHT_NOT_SPECIFIED:
            self.storage.move_tag_category(self.name, self.weight)

    def process_entity_instances(self):
        for entity in Entity.objects.filter(
            category_tags__contains=[{"name": self.old_name}]
        ).select_subclasses():
            entity.category_tags = [
                *self._translate_category_name(entity.category_tags)
            ]
            entity.save()

    def process_group_instances(self):
        for group in Group.objects.filter(
            category_tags__contains=[{"name": self.old_name}]
        ):
            group.category_tags = [*self._translate_category_name(group.category_tags)]
            group.save()

    def process_profile_instances(self):
        for profile in UserProfile.objects.filter(
            overview_email_categories__contains=[{"name": self.old_name}]
        ):
            profile.overview_email_categories = [
                *self._translate_category_name(profile.overview_email_categories)
            ]
            profile.save()

    def process_widget_instances(self):
        def update_tags(widget):
            new_settings = []
            if widget["type"] in ["objects", "activity", "search", "featured"]:
                for setting in widget.get("settings", []):
                    if setting.get("key", None) in ["categoryTags", "tagFilter"]:
                        try:
                            setting["value"] = json.dumps(
                                [
                                    *self._translate_category_name(
                                        json.loads(setting["value"])
                                    )
                                ],
                                separators=(",", ":"),
                            )
                        except Exception as e:
                            logger.warning(
                                "Failed to alter widget setting, error: %s", e
                            )
                    new_settings.append(setting)
                widget["settings"] = new_settings
            return widget

        for page in Page.objects.filter_campagne():
            page.update_widgets(callback=update_tags)
            page.save()

    def _translate_category_name(self, tags):
        for tag_group in tags:
            if tag_group["name"] == self.old_name:
                tag_group["name"] = self.name
            yield tag_group


class UpdateTagCategoryTag(MigrateTagCategoryContentBase):
    """
    Update the name and weight of one tag in a tag category.
    """

    def __init__(self, name, value, new_value, synonyms, weight):
        super().__init__()
        self.category_settings = self.storage.get_settings(name)
        self.name = name
        self.value = value
        self.new_value = new_value or value
        self.weight = weight
        self.synonyms = synonyms

        self.serialized = self.serialize()

    def has_changed(self):
        return self.serialized != self.serialize()

    def serialize(self):
        return [
            *self.storage.flat_category_tags(
                self.storage.get_tag_category(self.name), add_synonyms=True
            )
        ]

    def assert_valid(self):
        if not self.category_settings:
            raise ValueNotFoundError()

        all_tags = [t["value"] for t in self.category_settings["values"]]
        if self.value not in all_tags:
            raise ValueNotFoundError()
        if self.value == self.new_value:
            return
        if self.new_value in all_tags:
            raise DuplicateValueError()

    def process_configuration(self):
        new_values = self.category_settings["values"]

        new_values = self._update_tag(new_values)
        new_values = self._move_tag(new_values)

        self.category_settings["values"] = new_values
        self.storage.store_tag_category(self.name, self.category_settings)

    def _update_tag(self, values):
        new_values = []
        for tag in values:
            if tag["value"] == self.value:
                tag["value"] = self.new_value
                tag["synonyms"] = self.synonyms
            new_values.append(tag)
        return new_values

    def _move_tag(self, values):
        new_values = values
        if self.weight != WEIGHT_NOT_SPECIFIED:
            new_values = []
            found = None
            for tag in values:
                if tag["value"] == self.new_value:
                    found = tag
                else:
                    new_values.append(tag)
            if found:
                new_values.insert(self.weight, found)
        return new_values

    def insert_value(self, values, weight):
        if weight >= len(values):
            values.append(self.new_value)
        else:
            values.insert(weight, self.new_value)
        return values

    def process_entity_instances(self):
        if not self.has_changed():
            return

        for entity in Entity.objects.filter(
            category_tags__contains=[{"name": self.name, "values": [self.value]}]
        ).select_subclasses():
            entity.category_tags = [
                *self._translate_category_tags(entity.category_tags)
            ]
            entity.save()

    def process_group_instances(self):
        if not self.has_changed():
            return

        for group in Group.objects.filter(
            category_tags__contains=[{"name": self.name, "values": [self.value]}]
        ):
            group.category_tags = [*self._translate_category_tags(group.category_tags)]
            group.save()

    def process_profile_instances(self):
        if not self.has_changed():
            return

        for profile in UserProfile.objects.filter(
            overview_email_categories__contains=[
                {"name": self.name, "values": [self.value]}
            ]
        ):
            profile.overview_email_categories = [
                *self._translate_category_tags(profile.overview_email_categories)
            ]
            profile.save()

    def process_widget_instances(self):
        if not self.has_changed():
            return

        def update_tags(widget):
            new_settings = []
            if widget["type"] in ["objects", "activity", "search", "featured"]:
                for setting in widget.get("settings", []):
                    if setting.get("key", None) in ["categoryTags", "tagFilter"]:
                        try:
                            setting["value"] = json.dumps(
                                [
                                    *self._translate_category_tags(
                                        json.loads(setting["value"])
                                    )
                                ],
                                separators=(",", ":"),
                            )
                        except Exception as e:
                            logger.warning(
                                "Failed to alter widget setting, error: %s", e
                            )
                    new_settings.append(setting)
                widget["settings"] = new_settings
            return widget

        for page in Page.objects.filter_campagne():
            page.update_widgets(callback=update_tags)
            page.save()

    def _translate_category_tags(self, category_tags):
        for category in category_tags:
            if category["name"] == self.name and self.value in category["values"]:
                category["values"] = [*self._translate_tags(category["values"])]
            yield category

    def _translate_tags(self, values):
        for value in values:
            if value == self.value:
                yield self.new_value
            else:
                yield value


class DeleteTagCategory(MigrateTagCategoryContentBase):
    """
    Delete a tag-category with its member tags. Optionally keep the members as custom tags.
    """

    def __init__(self, name, keep_tags=False):
        super().__init__()
        self.name = name
        self.keep_tags = keep_tags

    def assert_valid(self):
        if self.name not in [x["name"] for x in self.storage.all_settings()]:
            raise ValueNotFoundError()

    def process_configuration(self):
        self.storage.store_tag_category(self.name, None)

    def process_entity_instances(self):
        for entity in Entity.objects.filter(
            category_tags__contains=[{"name": self.name}]
        ).select_subclasses():
            # Keep selected tags if required.
            entity.tags = self._extend_tags(entity.tags, entity.category_tags)
            # Then remove the tags from category_tags.
            entity.category_tags = [*self._delete_category(entity.category_tags)]
            entity.save()

    def process_group_instances(self):
        for group in Group.objects.filter(
            category_tags__contains=[{"name": self.name}]
        ):
            # Keep selected tags if required.
            group.tags = self._extend_tags(group.tags, group.category_tags)
            # Then remove the tags from category_tags.
            group.category_tags = [*self._delete_category(group.category_tags)]
            group.save()

    def process_profile_instances(self):
        for profile in UserProfile.objects.filter(
            overview_email_categories__contains=[{"name": self.name}]
        ):
            # First, if required, store existing category tags in the tags.
            profile.overview_email_tags = self._extend_tags(
                profile.overview_email_tags,
                profile.overview_email_categories,
            )
            # Then remove the tags from overview_email_categories.
            profile.overview_email_categories = [
                *self._delete_category(profile.overview_email_categories)
            ]
            profile.save()

    def process_widget_instances(self):
        def update_tags(widget):
            new_settings = []
            new_tag_setting = None
            if widget["type"] in ["objects", "activity", "search", "featured"]:
                for setting in widget.get("settings", []):
                    if setting.get("key", None) in ["categoryTags", "tagFilter"]:
                        if self.keep_tags:
                            try:
                                new_tag_setting = self._get_extended_widget_tags(
                                    widget.get("settings", []),
                                    json.loads(setting["value"]),
                                )
                            except Exception as e:
                                logger.warning(
                                    "Failed to create custom tags for widget setting, error: %s",
                                    e,
                                )
                        try:
                            setting["value"] = json.dumps(
                                [*self._delete_category(json.loads(setting["value"]))],
                                separators=(",", ":"),
                            )
                        except Exception as e:
                            logger.warning(
                                "Failed to alter widget setting, error: %s", e
                            )
                    new_settings.append(setting)
                if new_tag_setting:
                    new_settings = [
                        setting
                        for setting in new_settings
                        if setting.get("key") != "tags"
                    ]
                    new_settings.append(new_tag_setting)
                widget["settings"] = new_settings
            return widget

        for page in Page.objects.filter_campagne():
            page.update_widgets(callback=update_tags)
            page.save()

    def _delete_category(self, category_tags):
        for category in category_tags:
            if category["name"] != self.name:
                yield category

    def _extend_tags(self, tags, category_tags):
        if not self.keep_tags:
            return tags
        for category in category_tags:
            if category["name"] == self.name:
                return [*tags, *category["values"]]
        return tags

    def _get_extended_widget_tags(self, settings, cat_tags):
        def extend(value, extra_tags):
            tags = value.split(",")
            tags.extend(extra_tags)
            return ",".join(list(set(tags)))

        category_tags = []
        for category in cat_tags:
            if category["name"] == self.name:
                category_tags = category["values"]

        for setting in settings:
            if setting.get("key", None) == "tags":
                setting["value"] = extend(setting["value"], category_tags)
                return setting
        return {"key": "tags", "value": ",".join(list(set(category_tags)))}


class DeleteTagCategoryTag(MigrateTagCategoryContentBase):
    """
    Delete a tag from a tag category. Optionally keep the tag as custom tag.
    """

    def __init__(self, name, value, keep_tag=False):
        super().__init__()
        self.category_tags = self.storage.get_settings(name)
        self.name = name
        self.value = value
        self.keep_tag = keep_tag

    def assert_valid(self):
        if not self.category_tags:
            raise ValueNotFoundError()
        if self.value not in [t["value"] for t in self.category_tags["values"]]:
            raise ValueNotFoundError()

    def process_configuration(self):
        new_values = []
        for tag in self.category_tags["values"]:
            if tag["value"] != self.value:
                new_values.append(tag)
        self.category_tags["values"] = new_values
        self.storage.store_tag_category(self.name, self.category_tags)

    def process_entity_instances(self):
        for entity in Entity.objects.filter(
            category_tags__contains=[{"name": self.name, "values": [self.value]}]
        ).select_subclasses():
            entity.category_tags = [*self._delete_category_tag(entity.category_tags)]
            entity.tags = self._extend_tags(entity.tags)
            entity.save()

    def process_group_instances(self):
        for group in Group.objects.filter(
            category_tags__contains=[{"name": self.name, "values": [self.value]}]
        ):
            group.category_tags = [*self._delete_category_tag(group.category_tags)]
            group.tags = self._extend_tags(group.tags)
            group.save()

    def process_profile_instances(self):
        for profile in UserProfile.objects.filter(
            overview_email_categories__contains=[
                {"name": self.name, "values": [self.value]}
            ]
        ):
            profile.overview_email_categories = [
                *self._delete_category_tag(profile.overview_email_categories)
            ]
            profile.overview_email_tags = self._extend_tags(profile.overview_email_tags)
            profile.save()

    def process_widget_instances(self):
        def update_tags(widget):
            if widget["type"] in ["objects", "activity", "search", "featured"]:
                return self._remove_tag_from_widget(widget)
            return widget

        for page in Page.objects.filter_campagne():
            page.update_widgets(callback=update_tags)
            page.save()

    def _remove_tag_from_widget(self, widget):
        try:
            setting_dict = {
                setting["key"]: setting for setting in widget.get("settings", [])
            }
            if "categoryTags" in setting_dict:
                tag_key = "categoryTags"
            elif "tagFilter" in setting_dict:
                tag_key = "tagFilter"
            else:
                return widget

            category_tags = json.loads(setting_dict[tag_key]["value"])
            tags_str = setting_dict.get("tags", {"value": ""}).get("value") or ""
            tags = tags_str.split(",")

            for category in category_tags:
                if self.name == category["name"] and self.value in category["values"]:
                    if self.value not in tags and self.keep_tag:
                        tags.append(self.value)
                    setting_dict[tag_key]["value"] = json.dumps(
                        [*self._delete_category_tag(category_tags)]
                    )
                    setting_dict["tags"] = {
                        "key": "tags",
                        "value": ",".join(filter(bool, tags)),
                    }
                    widget["settings"] = [*setting_dict.values()]
                    break
        except json.JSONDecodeError as e:
            logger.warning("Failed to alter widget setting, error: %s", e)
        return widget

    def _delete_category_tag(self, category_tags):
        for category in category_tags:
            if category["name"] == self.name:
                category["values"] = [v for v in category["values"] if v != self.value]
            yield category

    def _extend_tags(self, tags):
        if not self.keep_tag or self.value in tags:
            return tags
        return [*tags, self.value]

    def _get_extended_widget_tags(self, settings):
        def extend(value, extra_tag):
            tags = value.split(",")
            tags.append(extra_tag)
            return ",".join(list(set(tags)))

        for setting in settings:
            if setting.get("key", None) == "tags":
                setting["value"] = extend(setting["value"], self.value)
                return setting
        return {"key": "tags", "value": self.value}


class TagCategoryStorage:
    def all_settings(self):
        return [
            {
                "name": tag_category["name"],
                "values": self._values_with_synonyms(tag_category.get("values")),
            }
            for tag_category in config.TAG_CATEGORIES
            if tag_category and "name" in tag_category
        ]

    def get_settings(self, name):
        try:
            for tag_category in config.TAG_CATEGORIES:
                if tag_category["name"] == name:
                    return {
                        "name": tag_category["name"],
                        "values": self._values_with_synonyms(
                            tag_category.get("values")
                        ),
                    }
        except KeyError:
            pass

    def get_or_create_settings(self, name):
        return self.get_settings(name) or {"name": name, "values": []}

    @staticmethod
    def _values_without_synonyms(tags):
        return [tag.get("value") for tag in (tags or []) if tag and "value" in tag]

    @staticmethod
    def _values_with_synonyms(tags):
        return [
            {
                "value": tag.get("value"),
                "synonyms": tag.get("synonyms", []) or [],
            }
            for tag in (tags or [])
            if tag and "value" in tag
        ]

    def all_tag_categories(self):
        return [
            {
                "name": tag_category.get("name", ""),
                "values": self._values_without_synonyms(tag_category.get("values", [])),
            }
            for tag_category in config.TAG_CATEGORIES
            if bool(tag_category)
        ]

    def get_tag_category(self, name):
        try:
            category = self.get_settings(name)
            category["values"] = self._values_without_synonyms(category["values"])
            return category
        except (KeyError, TypeError):
            return None

    def store_tag_category(self, name, category_tags):
        new_category_tags = []
        exists = False
        for category in config.TAG_CATEGORIES:
            if category["name"] == name:
                exists = True
                if category_tags:
                    new_category_tags.append(category_tags)
            else:
                new_category_tags.append(category)
        if not exists and category_tags:
            new_category_tags.append(category_tags)
        config.TAG_CATEGORIES = new_category_tags

    def store_tag_category_values(self, name, tag_values):
        tag_category = self.get_settings(name)
        if not tag_category:
            tag_category = {"name": name, "values": []}

        synonyms = {
            t["value"]: (t.get("synonyms") or [])
            for t in tag_category.get("values") or []
            if t and "value" in t
        }

        tag_category["values"] = [
            {"value": value, "synonyms": synonyms.get(value, [])}
            for value in tag_values
        ]
        self.store_tag_category(name, tag_category)

    def move_tag_category(self, name, position):
        new_category_tags = []
        moving_category = None
        for category in config.TAG_CATEGORIES:
            if category["name"] == name:
                moving_category = category
            else:
                new_category_tags.append(category)

        if moving_category:
            new_category_tags.insert(position, moving_category)
            config.TAG_CATEGORIES = new_category_tags

    def flat_category_tags(self, category, add_synonyms=False):
        if not category:
            return
        stored_category = self.get_settings(category["name"]) or {}
        values = {
            v["value"]: [v["value"], *(v["synonyms"] if add_synonyms else [])]
            for v in stored_category.get("values", [])
        }

        for value in category.get("values") or []:
            if value not in values:
                continue
            if not values[value]:
                continue
            for tag in values[value]:
                if tag:
                    yield ("%s (%s)" % (tag, category["name"])).lower()

    def tags_and_synonyms(self, category):
        settings = self.get_settings(category["name"])
        for value in settings.get("values") or []:
            if value["value"] in category["values"]:
                yield value["value"]
                yield from (value.get("synonyms") or [])


class TagCategoryWrapper:
    @classmethod
    def load_from_config(cls, name):
        if tag_category := TagCategoryStorage().get_tag_category(name):
            return cls(*tag_category["values"])
        return cls()

    def __init__(self, *args):
        self.tags = [*args] if args else []
        self.storage = TagCategoryStorage()

    def add(self, *tags):
        for tag in tags:
            if tag not in self.tags:
                self.tags.append(tag)

    def as_dict(self, name, sort=False):
        if sort:
            tags = sorted(self.tags, key=lambda x: (str(x).lower(), str(x)))
        else:
            tags = self.tags
        return {
            "name": name,
            "values": tags,
        }

    def save(self, name, sort=False):
        target = self.__class__.load_from_config(name)
        target.add(*self.tags)
        self.storage.store_tag_category_values(
            name, sorted(target.tags) if sort else target.tags
        )

    def __repr__(self):
        return "{{{}: {}}}".format(self.__class__, self.tags)
