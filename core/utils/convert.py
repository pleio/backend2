import json
import logging

import bleach
from bleach.linkifier import Linker
from django.conf import settings
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe
from django.utils.text import Truncator
from lxml import etree as ET
from prosemirror.model import DOMParser, DOMSerializer, Node

from core.lib import get_base_url, is_valid_json
from core.utils.tiptap_schema import tiptap_schema

logger = logging.getLogger(__name__)


def is_tiptap(json_string):
    if not is_valid_json(json_string):
        return False

    data = json.loads(json_string)

    return isinstance(data, dict) and data.get("type", None) == "doc"


def tiptap_to_html(maybe_json):
    """
    Convert json to html.
    The output of this method must be safe html.
    """

    if not is_tiptap(maybe_json):
        # The input may be unfiltered html.
        return strip_tags(maybe_json)

    doc = json.loads(maybe_json)

    try:
        doc_node = Node.from_json(tiptap_schema, doc)
        html = DOMSerializer.from_schema(tiptap_schema).serialize_fragment(
            doc_node.content
        )
        return str(html)
    except Exception as e:
        logger.error(e)
        return strip_tags(maybe_json)


def tiptap_to_text(json_string, with_links=False):  # noqa: C901
    """
    Convert TipTap to text. Mostly used for search indexing.
    """
    if not is_tiptap(json_string):
        return json_string

    doc = json.loads(json_string)

    def get_link(marks):
        for mark in marks:
            if mark["type"] == "link":
                return mark["attrs"]["href"]
        return ""

    def get_text(node):
        text = ""
        if node["type"] == "text":
            text += node["text"]
            if with_links:
                url = get_link(node.get("marks", []))
                if url:
                    text += " {} ".format(url)
        elif node["type"] == "hardBreak":
            text += "\n"
        elif node["type"] == "mention":
            text += "@" + node["attrs"]["label"]
        else:
            for item in node.get("content", []):
                text += get_text(item)
            text += "\n"
        return text

    text = ""

    for node in doc.get("content", []):
        text += get_text(node)
        text += "\n"

    return text


def truncate_rich_description(description):
    return Truncator(tiptap_to_text(description)).words(26)


def filter_html_mail_input(insecure_html):
    def allow_authentic_urls(attrs, new=False):
        if (None, "href") in attrs:
            href = attrs[(None, "href")]
            if href == attrs["_text"]:
                return attrs
            if href.startswith(get_base_url()):
                return attrs
        return None

    try:
        clean_html = bleach.clean(
            insecure_html,
            tags=settings.BLEACH_EMAIL_TAGS,
            attributes=settings.BLEACH_EMAIL_ATTRIBUTES,
            strip=True,
        )
        clean_html = Linker(callbacks=[allow_authentic_urls]).linkify(clean_html)

        return mark_safe(clean_html)
    except TypeError:
        pass
    return mark_safe("")


def html_to_tiptap(html):
    """
    Convert html to json.
    The output of this method must be safe json, None or the original string.
    """
    if html is None:
        return html
    if not html:
        return json.dumps({"type": "doc", "content": []})

    try:
        parser = DOMParser.from_schema(tiptap_schema)
        tree = ET.fromstring(html, parser=ET.HTMLParser())
        doc = parser.parse(tree)
        return json.dumps(doc.to_json())
    except Exception as e:
        logger.error(e)
        return html
