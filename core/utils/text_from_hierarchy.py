class TextFromHierarchy:
    def __init__(self, hierarchy):
        self.hierarchy = hierarchy

    def get_text(self) -> list:
        yield from self.get_text_from_hierarchy(self.hierarchy, "")

    def get_text_from_dict(self, branch, key):
        for sub_key, value in branch.items():
            yield from self.get_text_from_hierarchy(value, "{}{}.".format(key, sub_key))

    def get_text_from_list(self, branch, key):
        for sub_key, value in enumerate(branch):
            yield from self.get_text_from_hierarchy(value, "{}{}.".format(key, sub_key))

    def get_text_from_hierarchy(self, branch, key):
        if isinstance(branch, dict):
            yield from self.get_text_from_dict(branch, key)
        elif isinstance(branch, list):
            yield from self.get_text_from_list(branch, key)
        elif key == "text." or key.endswith(".text."):
            yield key, branch


class HierarchyFromText:
    def __init__(self, texts):
        self.texts = texts

    @staticmethod
    def _has_lang_mark(branch):
        """
        Check if a branch has a lang mark. The lang-marked text is translated for
        improved translation quality. But the text should not be replaced by its
        translation. Else the lang-attribute becomes invalid.
        """
        if isinstance(branch, dict) and "marks" in branch:
            for mark in branch["marks"]:
                if mark.get("type") == "lang":
                    return True
        return False

    @staticmethod
    def _is_code_block(branch):
        """
        Check if the branch is a code block. Code blocks should not be translated.
        """
        if isinstance(branch, dict) and "type" in branch:
            return branch["type"] in ("codeBlock", "code")
        return False

    def apply_to(self, hierarchy):
        for path, text in self.texts:
            if "." in path:
                left, right = self._split_path(path)
                hierarchy[left] = self.apply_path_text(hierarchy[left], right, text)
            else:
                return text
        return hierarchy

    def apply_path_text(self, source, path, text):
        try:
            assert not self._has_lang_mark(source), "Dont overwrite lang marked text"
            assert not self._is_code_block(source), "Dont overwrite code block"
            if path == "":
                return text or " "
            elif "." not in path:
                source[path] = text
            else:
                left, right = self._split_path(path)
                source[left] = self.apply_path_text(source[left], right, text)
        except AssertionError:
            pass
        return source

    @staticmethod
    def _maybe_index(key):
        try:
            return int(key)
        except ValueError:
            return key

    @classmethod
    def _split_path(cls, path):
        left, right = path.split(".", 1)
        return cls._maybe_index(left), right
