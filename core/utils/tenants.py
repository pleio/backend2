from django.db import connection
from django_tenants.utils import get_tenant_model


class SiteDisabledError(Exception):
    pass


def assert_site_active(exc):
    tenant = get_tenant_model().objects.get(schema_name=connection.schema_name)

    if not tenant.is_active:
        raise exc
