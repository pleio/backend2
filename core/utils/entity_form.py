import uuid

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from core.constances import COULD_NOT_FIND
from core.lib import FieldTypes, format_list, is_valid_json
from core.resolvers.scalars.datetime_scalar import parse_datetime_value
from core.resolvers.scalars.secure_rich_text import secure_prosemirror_value_parser


class EntityFormWrapper:
    def __init__(self, specs):
        self.specs = specs

    @property
    def title(self):
        return self.specs.get("title", "")

    @property
    def description(self):
        return self.specs.get("description", "")

    @property
    def fields(self):
        return [EntityFormFieldWrapper(spec) for spec in self.specs.get("fields", [])]

    def get_field(self, field_guid):
        for field in self.fields:
            if field.guid == field_guid:
                return field

    def get_manditory_fields(self):
        return [field for field in self.fields if field.is_mandatory]

    def serialize(self):
        if not self.specs:
            return None
        return {
            "title": self.title,
            "description": self.description,
            "fields": [field.serialize() for field in self.fields],
        }

    def __str__(self):
        return self.title

    def __repr__(self):
        return "<EntityForm %s (%s fields)>" % (self.title, len(self.fields))


class EntityFormFieldWrapper:
    def __init__(self, specs):
        self.specs = specs
        assert self.field_type in [e.value for e in FieldTypes], (
            "Field type not found [%s]" % self.field_type
        )

    @property
    def title(self):
        return self.specs.get("title", "")

    @property
    def description(self):
        return self.specs.get("description", "")

    @property
    def field_type(self):
        return self.specs.get("fieldType", "")

    @property
    def guid(self):
        return self.specs.get("guid") or str(uuid.uuid4())

    @property
    def field_options(self):
        return self.specs.get("fieldOptions", [])

    @property
    def is_mandatory(self):
        return self.specs.get("isMandatory", False)

    def serialize(self):
        return {
            "title": self.title,
            "description": self.description,
            "fieldType": self.field_type,
            "guid": self.guid,
            "fieldOptions": self.field_options,
            "isMandatory": self.is_mandatory,
        }


class UpdateUserFormFieldCollection:
    def __init__(self, user, email, entity):
        self.user = user
        self.email = email
        self.entity = entity
        self.form = EntityFormWrapper(entity.form_specs)
        self.fields = []
        self.errors = []

    def get_field_response(self, field_guid):
        qs = self.entity.entity_form_user_answers.all()
        qs = qs.filter_respondent(self.user, self.email)
        return qs.filter(form_field=field_guid).first()

    def check_mandatory_fields(self, fields):
        to_be_filled_guids = [d["guid"] for d in fields if "guid" in d]
        mandatory_fields = self.form.get_manditory_fields()

        for mandatory_field in mandatory_fields:
            if mandatory_field.guid not in to_be_filled_guids and not (
                self.get_field_response(mandatory_field.guid)
            ):
                self.errors.append(
                    {
                        "guid": mandatory_field.guid,
                        "message": _("This field is mandatory"),
                    }
                )

    def add_field(self, field_data):
        try:
            field = self.form.get_field(field_data["guid"])
            assert field, "Field not found"
            self.fields.append(
                {
                    "field": field,
                    **field_data,
                    **self.overwrite_value(field_data, field),
                }
            )
        except AssertionError:
            self.errors.append({"guid": field_data["guid"], "message": COULD_NOT_FIND})
        except ValidationError as e:
            self.errors.append(
                {"guid": field_data["guid"], "message": ", ".join(e.messages)}
            )

    def apply(self):
        from core.models.form import EntityUserFormField

        for field in self.fields:
            form_field, created = EntityUserFormField.objects.get_or_create(
                user=self.user,
                email=self.email,
                form_field=field["field"].guid,
                entity=self.entity,
            )
            form_field.value = field["value"]
            form_field.save()

    def overwrite_value(self, field_data, field):
        value = field_data["value"]

        if not isinstance(value, str):
            value = ""
        else:
            value = self._maybe_html_value(field, value)
            value = self._maybe_invalid_option(field, value)
            value = self._maybe_date(field, value)
            value = self._maybe_date_time(field, value)
            value = self._maybe_multi_select(field, value)

        return {"value": value}

    @staticmethod
    def _maybe_html_value(field, value):
        if value and field.field_type == "htmlField":
            if not is_valid_json(value):
                raise ValidationError(_("Invalid text format"))
            return secure_prosemirror_value_parser(value)
        return value

    @staticmethod
    def _maybe_invalid_option(field, value):
        if (
            value
            and field.field_type in ["selectField", "radioField"]
            and value not in field.field_options
        ):
            raise ValidationError(
                _("%(value)s is not in field options. Try one of %(options)s")
                % {
                    "value": value,
                    "options": format_list(field.field_options, exclusive=True),
                }
            )
        return value

    @staticmethod
    def _maybe_date(field, value):
        if value and field.field_type == "dateField":
            try:
                return parse_datetime_value(value).date().isoformat()
            except Exception:
                raise ValidationError(_("Invalid date format. Expecting Y-m-d"))
        return value

    @staticmethod
    def _maybe_date_time(field, value):
        if value and field.field_type == "dateTimeField":
            try:
                return parse_datetime_value(value).isoformat()
            except Exception:
                raise ValidationError(_("Invalid date format. Expecting Y-m-d H:M:S"))
        return value

    @staticmethod
    def _maybe_multi_select(field, value):
        if value and field.field_type in ["multiSelectField", "checkboxField"]:
            for selected in value.split(","):
                if selected not in field.field_options:
                    raise ValidationError(
                        _("%(value)s is not in field options. Try one of %(options)s")
                        % {
                            "value": selected,
                            "options": format_list(field.field_options, exclusive=True),
                        }
                    )
        return value
