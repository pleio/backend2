import logging

from django.http import StreamingHttpResponse

logger = logging.getLogger(__name__)


class PleioStreamingHttpResponse(StreamingHttpResponse):
    @staticmethod
    def attachment_or_inline(mimetype):
        if not mimetype:
            return "attachment"
        elif mimetype in ["text/html"]:
            return "attachment"
        elif mimetype.startswith("image/"):
            return "attachment"
        return "inline"

    @classmethod
    def is_attachment(cls, mimetype):
        return cls.attachment_or_inline(mimetype) == "attachment"

    def __init__(self, file_name, **kwargs):
        super().__init__(**kwargs)
        self["Content-Disposition"] = "{}; filename={}".format(
            self.attachment_or_inline(kwargs.get("content_type")), file_name
        )
