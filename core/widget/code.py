from core.lib import html_to_text
from core.widget.base import BaseWidgetToText


class CodeWidgetToText(BaseWidgetToText):
    type = "html"

    @staticmethod
    def to_text(widget) -> str:
        for setting in widget.get("settings", []):
            if setting.get("value"):
                return html_to_text(setting.get("value")).strip()
        return ""
