from core.widget.base import BaseWidgetToText


class LinkListWidgetToText(BaseWidgetToText):
    type = "linkList"

    @staticmethod
    def to_text(widget) -> str:
        settings = {s.get("key"): s for s in widget.get("settings", [])}
        lines = []

        if "title" in settings:
            lines.append(settings["title"]["value"])

        if "links" in settings:
            for link in settings["links"].get("links", []):
                if link.get("label"):
                    lines.append(link.get("label"))
                if link.get("url"):
                    lines.append(link.get("url"))

        return "\n".join(lines)
