from core.widget.birth_days import BirthDaysWidgetToText
from core.widget.call_to_action import CallToActionWidgetToText
from core.widget.code import CodeWidgetToText
from core.widget.events import EventsWidgetToText
from core.widget.featured import FeaturedWidgetToText
from core.widget.footer import FooterWidgetToText
from core.widget.lead import LeadWidgetToText
from core.widget.link_list import LinkListWidgetToText
from core.widget.objects import ObjectsWidgetToText
from core.widget.poll import PollWidgetToText
from core.widget.text import TextWidgetToText


def widget_to_text(widget):
    resolvers = [
        BirthDaysWidgetToText,
        CallToActionWidgetToText,
        EventsWidgetToText,
        FeaturedWidgetToText,
        FooterWidgetToText,
        LeadWidgetToText,
        LinkListWidgetToText,
        ObjectsWidgetToText,
        PollWidgetToText,
        TextWidgetToText,
        CodeWidgetToText,
    ]

    for resolver in resolvers:
        if resolver.type == widget["type"]:
            return resolver.to_text(widget).strip()
