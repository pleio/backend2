from core.widget.base import BaseWidgetToText


class ObjectsWidgetToText(BaseWidgetToText):
    type = "objects"

    @staticmethod
    def to_text(widget) -> str:
        for setting in widget.get("settings", []):
            if setting.get("key") == "title":
                return setting.get("value", "")
        return ""
