from core.widget.base import BaseWidgetToText


class BirthDaysWidgetToText(BaseWidgetToText):
    type = "birthdays"

    @staticmethod
    def to_text(widget) -> str:
        lines = []
        for setting in widget.get("settings", []):
            if setting.get("key") == "title":
                lines.append(setting.get("value"))
            if setting.get("key") == "description":
                lines.append(setting.get("value"))
        return "\n".join(lines)
