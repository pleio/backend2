from core.widget.base import BaseWidgetToText
from entities.poll.models import Poll


class PollWidgetToText(BaseWidgetToText):
    type = "poll"

    @staticmethod
    def to_text(widget) -> str:
        for setting in widget.get("settings", []):
            if setting.get("key") == "pollGuid":
                try:
                    poll = Poll.objects.get(pk=setting.get("value"))
                    return poll.title
                except Poll.DoesNotExist:
                    pass
        return ""
