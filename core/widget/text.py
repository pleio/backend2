from core.utils.convert import tiptap_to_text
from core.widget.base import BaseWidgetToText


class TextWidgetToText(BaseWidgetToText):
    type = "text"

    @staticmethod
    def to_text(widget) -> str:
        for setting in widget.get("settings") or []:
            if setting["key"] == "richDescription":
                return tiptap_to_text(
                    setting["richDescription"] or setting["value"] or "",
                    with_links=True,
                ).strip()
        return ""
