from core.utils.convert import tiptap_to_text
from core.widget.base import BaseWidgetToText


class CallToActionWidgetToText(BaseWidgetToText):
    type = "callToAction"

    containers = (
        ("title", "value"),
        ("description", "value"),
        ("description", "richDescription"),
        ("buttonText", "value"),
        ("imageAlt", "value"),
    )

    @staticmethod
    def to_text(widget) -> str:
        settings = {s.get("key"): s for s in widget.get("settings", [])}
        result = ""
        for key, value in CallToActionWidgetToText.containers:
            text = settings.get(key, {}).get(value)
            if text and key == "richDescription":
                result = "%s\n%s" % (result, tiptap_to_text(text, with_links=True))
            elif text:
                result = "%s\n%s" % (result, text)
        return result.strip()
