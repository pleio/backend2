from core.widget.base import BaseWidgetToText


class FeaturedWidgetToText(BaseWidgetToText):
    type = "featured"

    @staticmethod
    def to_text(widget) -> str:
        for setting in widget.get("settings", []):
            if setting.get("key") == "title":
                return setting.get("value", "")
        return ""
