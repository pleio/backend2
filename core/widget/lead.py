from core.widget.base import BaseWidgetToText


class LeadWidgetToText(BaseWidgetToText):
    type = "lead"

    containers = (
        ("title", "value"),
        ("link", "value"),
        ("imageAlt", "value"),
    )

    @staticmethod
    def to_text(widget) -> str:
        settings = {s.get("key"): s for s in widget.get("settings", [])}
        lines = []

        for key, value in LeadWidgetToText.containers:
            if key in settings:
                lines.append(settings[key][value])

        return "\n".join(lines)
