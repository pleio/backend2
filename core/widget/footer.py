import json

from core.widget.base import BaseWidgetToText


class FooterWidgetToText(BaseWidgetToText):
    type = "footer"

    @staticmethod
    def to_text(widget) -> str:
        settings = {s.get("key"): s for s in widget.get("settings", [])}
        lines = []

        if settings.get("links"):
            try:
                links = json.loads(settings["links"]["value"])
                for link in links:
                    if link.get("label"):
                        lines.append(link.get("label"))
            except json.JSONDecodeError:
                pass

        return "\n".join(lines)
