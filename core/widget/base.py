class BaseWidgetToText:
    type = None

    @staticmethod
    def to_text(widget) -> str:
        raise NotImplementedError()
