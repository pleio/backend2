from django.utils.translation import pgettext

from core.widget.base import BaseWidgetToText


class EventsWidgetToText(BaseWidgetToText):
    type = "events"

    @staticmethod
    def to_text(widget) -> str:
        return pgettext("widget-type", "Events")
