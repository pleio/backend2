import os.path
from pathlib import Path

from django.conf import settings
from django.core.management import BaseCommand

from core.utils.object_storage import ObjectStorageClient


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.backup_root = Path(settings.BACKUP_PATH)
        self.client = ObjectStorageClient(settings.BACKUP_BUCKET)

    def add_arguments(self, parser):
        parser.add_argument("--direction")
        parser.add_argument("folder")

    def handle(self, folder, direction, *args, **kwargs):
        assert direction in ["up", "down"], "Please provide a valid direction"

        if direction == "up":
            self.upload(folder)
        else:
            self.download(folder)

    def upload(self, folder):
        assert os.path.exists(self.backup_root / folder), (
            f"Folder {self.backup_root / folder} does not exist"
        )

        if os.path.isdir(self.backup_root / folder):
            self.client.upload_sync(self.backup_root, folder)
        else:
            self.client.upload(self.backup_root, folder)

    def download(self, folder):
        assert self.client.exists(folder), (
            f"Folder {folder} does not exist on the object storage"
        )

        if self.client.is_folder(folder):
            self.client.download_sync(self.backup_root, folder)
        else:
            self.client.download(self.backup_root, folder)
