import traceback
from traceback import format_exc

import elasticsearch
from celery import chain
from celery.utils.log import get_task_logger
from django.apps import apps
from django_elasticsearch_dsl.registries import registry
from django_tenants.utils import schema_context
from elasticsearch import ConnectionError as ElasticsearchConnectionError
from elasticsearch_dsl import Q, Search, connections

from backend2 import celery_app as app
from core.lib import get_model_by_subtype, tenant_schema
from core.models.search import SearchIndexLog
from core.utils.elasticsearch import list_documents, list_indices
from core.utils.entity import load_entity_by_id
from tenants.models import Client

logger = get_task_logger(__name__)


def _get_models_by_index_name(index_name):
    if index_name == "image":
        return [apps.get_model("file", "FileFolder")]
    return [get_model_by_subtype(index_name)]


@app.task(ignore_result=True)
def elasticsearch_recreate_indices(index_name=None):
    """
    Delete indexes, creates indexes
    """

    if index_name:
        models = _get_models_by_index_name(index_name)
    else:
        models = _all_models()

    # delete indexes
    for index in list_indices(models):
        try:
            index.delete()
            logger.info("deleted index %s", index._name)
        except Exception:
            logger.info("index %s does not exist", index._name)

        try:
            index.create()
            logger.info("created index %s", index._name)
        except Exception:
            logger.error("index %s already exists", index._name)


@app.task(ignore_result=True)
def elasticsearch_install_new_indices():
    models = _all_models()

    for index in list_indices(models):
        try:
            index.create()
            logger.info("created index %s", index._name)

            for client in Client.objects.exclude(schema_name="public"):
                elasticsearch_index_data_for_tenant.delay(
                    client.schema_name, index._name
                )
        except elasticsearch.RequestError:
            logger.info("index %s already exists", index._name)


@app.task(ignore_result=True)
def elasticsearch_rebuild_all(index_name=None):
    """
    Delete indexes, creates indexes and populate tenants.
    Sites in random order, indexes in sequential order.

    No option passed then all indices are rebuild
    Options: ['news', 'file', 'image', 'question' 'wiki', 'discussion', 'page', 'event', 'blog', 'user', 'group']
    """

    elasticsearch_recreate_indices(index_name)

    for client in Client.objects.exclude(schema_name="public"):
        elasticsearch_index_data_for_tenant.delay(client.schema_name, index_name)


@app.task
def elasticsearch_rebuild_all_per_index():
    """
    Delete indexes, create indexes and populate tenants.

    Indexes in parallel, sites in serial.
    """

    signatures = []
    for index in all_indexes():
        signatures.append(elasticsearch_rebuild_all_at_index.si(index._name))
    chain(*signatures).apply_async()


@app.task
def elasticsearch_rebuild_all_at_index(index_name):
    elasticsearch_recreate_indices(index_name)

    signatures = []
    for client in Client.objects.exclude(schema_name="public"):
        args = (client.schema_name, index_name)
        signatures.append(elasticsearch_index_data_for_tenant.si(*args))
    chain(*signatures).apply_async()


@app.task(ignore_result=True)
def elasticsearch_rebuild_for_tenant(schema_name, index_name=None):
    """
    Rebuild index for tenant
    """
    with schema_context(schema_name):
        if not index_name:
            logger.info("elasticsearch_rebuild_for_tenant '%s'", schema_name)
            elasticsearch_delete_data_for_tenant(schema_name, index_name)
            elasticsearch_index_data_for_tenant(schema_name, index_name)
        else:
            for name in index_name.split(","):
                logger.info(
                    "elasticsearch_rebuild_for_tenant '%s' '%s'", name, schema_name
                )
                elasticsearch_delete_data_for_tenant(schema_name, name)
                elasticsearch_index_data_for_tenant(schema_name, name)


@app.task(ignore_result=True)
def elasticsearch_index_data_for_all(index_name=None):
    for client in Client.objects.exclude(schema_name="public"):
        elasticsearch_index_data_for_tenant.delay(client.schema_name, index_name)


@app.task(ignore_result=True)
def elasticsearch_index_data_for_tenant(schema_name, index_name=None):
    logger.info(
        "elasticsearch_index_data_for_tenant '%s' '%s'", index_name, schema_name
    )
    with schema_context(schema_name):
        if index_name:
            models = _get_models_by_index_name(index_name)
        else:
            models = _all_models()

        for doc in list_documents(models):
            _update_document_index(doc())


def _update_document_index(doc_instance):
    logger.info(
        "indexing %i '%s' objects",
        doc_instance.get_queryset().count(),
        doc_instance.__class__.django.model.__name__,
    )
    for instance in doc_instance.get_queryset().iterator(chunk_size=500):
        _update_instance(instance, doc_instance)


def _get_doc_instance(object_instance):
    # One model may have several indices...
    for doc in registry.get_documents([object_instance.__class__]):
        yield doc()


def _update_instance(instance, doc_instance=None):
    try:
        if not doc_instance:
            # If not specified, this may be more then one.
            for doc_instance in _get_doc_instance(instance):
                _update_instance(instance, doc_instance)
        else:
            doc_instance.update(instance)
    except ElasticsearchConnectionError:
        raise
    except Exception as e:
        logger.error(
            "exception at elasticsearch_update_instance_index %s %s: %s",
            tenant_schema(),
            e.__class__,
            e,
        )
        logger.error(format_exc())
        SearchIndexLog.objects.add_log(
            message="at _update_instance",
            traceback=traceback.format_exc(),
            object=instance,
        )


def _delete_document_if_found(uuid, type=None):
    for index in list_indices():
        try:
            result = index.search().filter(Q("term", id=uuid)).execute()
            if result:
                result[0].delete()
        except ElasticsearchConnectionError:
            raise
        except Exception as e:
            logger.error(
                "exception at elasticsearch_delete_instance_index %s %s: %s",
                tenant_schema(),
                e.__class__,
                e,
            )
            logger.error(format_exc())
            SearchIndexLog.objects.add_log(
                message="at _delete_document_if_found",
                traceback=traceback.format_exc(),
                object=uuid,
                default_type=type,
            )


@app.task(ignore_result=True)
def elasticsearch_delete_data_for_tenant(schema_name, index_name=None):
    """
    Delete tenant data from elasticsearch
    """

    if not schema_name:
        return

    logger.info("elasticsearch_delete_data_for_tenant '%s'", schema_name)

    if index_name:
        models = [get_model_by_subtype(index_name)]
    else:
        models = _all_models()

    es_client = connections.get_connection()
    for index in list_indices(models):
        body = Search().query().filter("term", tenant_name=schema_name).to_dict()
        es_client.delete_by_query(index=index._name, body=body, conflicts="proceed")
        es_client.indices.refresh(index=index._name)


@app.task(ignore_result=True)
def elasticsearch_complement_index(schema_name, index_name):
    with schema_context(schema_name):
        documents = {dc.Index.name: dc for dc in list_documents()}
        if index_name not in documents:
            return
        index_document = documents[index_name]()
        queryset = index_document.get_queryset()
        model_label = index_document.Django.model._meta.label
        for guid in [str(id) for id in queryset.values_list("id", flat=True)]:
            result = (
                Search(index="file")
                .filter(Q("term", id=guid))
                .filter("term", tenant_name=schema_name)
                .execute()
            )
            if result["hits"]["total"]["value"] < 1:
                elasticsearch_index_document.delay(schema_name, guid, model_label)
                logger.info("Retry %s:%s @%s", model_label, guid, schema_name)


@app.task(
    autoretry_for=(ElasticsearchConnectionError,),
    retry_backoff=10,
    max_retries=10,
    rate_limit="100/m",
)
def elasticsearch_index_document(schema_name, document_guid, document_classname):
    with schema_context(schema_name):
        instance = load_entity_by_id(
            document_guid,
            ["core.Group", "core.Entity", "core.User"],
            fail_if_not_found=False,
        )
        if instance:
            _update_instance(instance)
        else:
            _delete_document_if_found(document_guid, document_classname)
        return f"{schema_name}.{document_classname}.{document_guid}"


def _all_models():
    def model_weight(model):
        name = model._meta.object_name
        if name == "Group":
            return 0
        if name == "User":
            return 1
        if name == "FileFolder":
            return 100
        return 10

    return sorted(registry.get_models(), key=model_weight)


def all_indexes():
    def index_weight(index):
        name = index._name
        if name == "group":
            return 0
        if name == "user":
            return 1
        if name == "file":
            return 100
        return 10

    return sorted(list_indices(), key=index_weight)
