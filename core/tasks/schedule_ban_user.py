import celery.utils.log
from celery import shared_task
from django.utils import timezone
from django_tenants.utils import schema_context

from user.models import User

logger = celery.utils.log.get_task_logger(__name__)


@shared_task
def process_scheduled_ban_users(schema_name):
    logger.info("Enter process_scheduled_ban_users for schema %s", schema_name)
    with schema_context(schema_name):
        for user in User.objects.filter(
            is_active=True,
            schedule_ban_at__isnull=False,
            schedule_ban_at__lte=timezone.now(),
        ):
            user.is_active = False
            user.banned_at = timezone.now()
            user.ban_reason = user.schedule_ban_reason
            user.schedule_ban_at = None
            user.schedule_ban_reason = None
            user.save()
            logger.info("User %s has been banned", user)
