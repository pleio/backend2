import uuid
from urllib.parse import urlparse

from celery import shared_task
from django_tenants.utils import schema_context

from core.constances import ENTITY_STATUS
from core.lib import get_access_id
from core.models import BrokenLink, Entity, Group
from core.models.rich_fields import RichFieldsMixin, TipTapLinks
from core.utils.tenants import SiteDisabledError, assert_site_active
from tenants.models import Client
from user.models import User


def get_concrete_classes(cls):
    for subclass in cls.__subclasses__():
        if subclass._meta.abstract:
            yield from get_concrete_classes(subclass)
        else:
            yield subclass


def is_internal_link(link, local_domain=""):
    netloc = urlparse(link).netloc
    if netloc:
        if netloc.split(":")[0] == local_domain:
            return True
        return False
    return True


def get_link_status(url, source) -> BrokenLink.Reasons:
    status = None
    uuid = TipTapLinks.get_entity_uuid(url)
    if uuid:
        entity = Entity.objects.filter(id=uuid).first()

        if entity:
            if entity.status_published == ENTITY_STATUS.ARCHIVED:
                status = BrokenLink.Reasons.ARCHIVED
            elif entity.status_published == ENTITY_STATUS.DRAFT:
                status = BrokenLink.Reasons.DRAFT
            elif hasattr(source, "read_access"):
                if get_access_id(source.read_access) > get_access_id(
                    entity.read_access
                ):
                    status = BrokenLink.Reasons.SOURCE_MORE_PRIVILEGES
        else:
            # test user / group links
            user_or_group = None
            if "group" in url:
                user_or_group = Group.objects.filter(id=uuid).first()
            elif "user" in url:
                user_or_group = User.objects.filter(id=uuid).first()

            if not user_or_group:
                status = BrokenLink.Reasons.DOES_NOT_EXIST

    return status


@shared_task
def check_internal_links(schema_name):
    try:
        assert_site_active(SiteDisabledError("Site is not active"))
        scan_id = uuid.uuid4()
        with schema_context(schema_name):
            tenant = Client.objects.get(schema_name=schema_name)
            tenant_domain = tenant.get_primary_domain().domain

            rich_field_models = list(set(get_concrete_classes(RichFieldsMixin)))
            for model in rich_field_models:
                for instance in model.objects.iterator(chunk_size=500):
                    for link in instance.lookup_links():
                        if not is_internal_link(link, tenant_domain):
                            continue

                        status = get_link_status(link, instance)
                        if status:
                            link, created = BrokenLink.objects.get_or_create(
                                source=instance.url,
                                target=link,
                                defaults={"reason": status, "scan_id": scan_id},
                            )
                            if (
                                not created
                            ):  # defaults are set when created, update when not created
                                link.reason = status
                                link.scan_id = scan_id
                                link.save()

            # delete all broken links that are not found in the current scan
            BrokenLink.objects.exclude(scan_id=scan_id).delete()
    except SiteDisabledError:
        pass
