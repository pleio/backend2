import csv
import os
from collections import defaultdict
from io import BytesIO
from os.path import basename

from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.files.base import ContentFile
from django.utils import translation
from django_tenants.utils import schema_context
from PIL import Image

from core import config
from core.lib import get_file_checksum
from core.mail_builders.user_import_failed_mailer import schedule_user_import_failed
from core.mail_builders.user_import_success_mailer import schedule_user_import_success
from core.models import ResizedImage
from core.utils.user_importer import UserImporter
from entities.file.helpers.images import strip_exif
from entities.file.models import FileFolder
from user.models import User

logger = get_task_logger(__name__)


@shared_task(ignore_result=True)
def import_users(schema_name, fields, csv_location, performing_user_guid):
    """
    Import users
    """

    with schema_context(schema_name):
        if config.LANGUAGE:
            translation.activate(config.LANGUAGE)

        performing_user = User.objects.get(id=performing_user_guid)

        stats = defaultdict(lambda: 0)

        logger.info("Start import on tenant %s by user", performing_user.email)

        success = True
        error_message = ""

        try:
            with open(csv_location) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=";")

                # cleanup empty rows
                reader = [row for row in reader if any(row.values())]

                for row in reader:
                    importer = UserImporter(row, fields)
                    importer.apply()

                    stats[importer.status] += 1
                    stats[importer.idp_status] += 1

                    stats["processed"] += 1

                    if stats["processed"] % 100 == 0:
                        logger.info("Batch users imported: %s", stats)

            os.remove(csv_location)

            logger.info("Import done with stats: %s ", stats)
        except Exception as e:
            success = False
            error_message = "Import failed with message %s" % e
            logger.error(error_message)

        if success:
            schedule_user_import_success(performing_user, stats)
        else:
            schedule_user_import_failed(performing_user, stats, error_message)


@shared_task(bind=True)
def image_resize(self, schema_name, resize_image_id):
    with schema_context(schema_name):
        try:
            resized_image = ResizedImage.objects.get(id=resize_image_id)
        except Exception as e:
            logger.error(e)
            return

        if resized_image.status == ResizedImage.OK:
            return

        try:
            infile = resized_image.original.upload_field.open()
            im = Image.open(infile)

            # Set the smallest dimension to the requested size to avoid pixelly images
            if im.width > resized_image.size:
                thumbnail_size = (resized_image.size, im.height)
                im.thumbnail(thumbnail_size, Image.LANCZOS)

            output = BytesIO()

            # Not all plugins support animated images (https://pillow.readthedocs.io/en/stable/reference/Image.html#PIL.Image.Image.is_animated)
            is_animated = getattr(im, "is_animated", False)
            im.save(output, im.format, save_all=is_animated)
            contents = output.getvalue()
            im.close()

            resized_image.mime_type = Image.MIME[im.format]
            resized_image.upload.save(
                basename(resized_image.original.upload_field.name),
                ContentFile(contents),
            )
            resized_image.status = ResizedImage.OK
            resized_image.save()

        except Exception as e:
            resized_image.status = ResizedImage.FAILED
            resized_image.message = str(e)
            resized_image.save()
            logger.error(e)


@shared_task
def strip_exif_from_file(schema, file_folder_guid=None):
    with schema_context(schema):
        if file_folder_guid:
            instance = FileFolder.objects.get(id=file_folder_guid)
            logger.info("Strip exif from %s", instance.upload.name)
            strip_exif(instance.upload)


@shared_task(bind=True, ignore_result=True)
def update_file_checksum(self, schema, file_folder_guid):
    with schema_context(schema):
        try:
            instance = FileFolder.objects.get(id=file_folder_guid)
            FileFolder.objects.filter(id=file_folder_guid).update(
                checksum=get_file_checksum(instance.upload)
            )
        except Exception as e:
            self.retry(exc=e, countdown=5, max_retries=3)
            raise
