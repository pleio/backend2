import logging

from celery import shared_task
from django.apps import apps
from django.utils import timezone, translation
from django_tenants.utils import schema_context
from notifications.models import Notification
from notifications.signals import notify

from chat.models import ChatMessage
from core import config
from core.lib import get_model_name
from core.models.entity import Entity
from core.models.group import GroupMembership
from core.models.mixin import NotificationMixin
from core.models.push_notification import WebPushSubscription
from core.utils.push_notification import (
    get_notification_payload,
    send_web_push_notification,
)
from user.models import User

LOGGER = logging.getLogger(__name__)


@shared_task()
def create_notifications_for_range_events(schema_name):
    with schema_context(schema_name):
        Model = apps.get_model("event.Event")
        events = Model.objects.filter(
            range_starttime__isnull=False,
            range_starttime__lte=timezone.now() + timezone.timedelta(days=31),
            group__isnull=False,
        )

        for instance in events:
            # there are already notifications for this instance.id.
            instance_notifications = Notification.objects.filter(
                action_object_object_id=instance.id
            )
            if instance_notifications.exists():
                continue

            if not instance.should_notify_on_create:
                continue

            create_notification.delay(
                schema_name,
                "created",
                get_model_name(instance),
                instance.id,
                instance.owner.id,
            )


@shared_task()
def create_notifications_for_scheduled_content(schema_name):
    with schema_context(schema_name):
        for instance in (
            Entity.objects.filter(
                notifications_created=False, published__lte=timezone.now()
            )
            .exclude(published=None)
            .select_subclasses()
        ):
            # instance has no NotificationMixin impemented. Set notifications_created True so it is skipped next time.
            if instance.__class__ not in NotificationMixin.__subclasses__():
                instance.notifications_created = True
                instance.save()
                continue

            # instance has no group. Set notifications_created True so it is skipped next time.
            if not instance.group:
                instance.notifications_created = True
                instance.save()
                continue

            # there are already notifications for this instance.id.
            if Notification.objects.filter(
                action_object_object_id=instance.id
            ).exists():
                instance.notifications_created = True
                instance.save()
                continue

            create_notification.delay(
                schema_name,
                "created",
                get_model_name(instance),
                instance.id,
                instance.owner.id,
            )


def send_push(recipient, sender, verb, instance):
    translation.activate(recipient.get_language())
    payload = get_notification_payload(sender, verb, instance)
    if payload:
        for subscription in WebPushSubscription.objects.filter(user=recipient):
            send_web_push_notification(subscription, payload)


@shared_task(bind=True, ignore_result=True)
def create_notification(  # noqa: C901
    self, schema_name, verb, model_name, entity_id, sender_id
):
    """
    task for creating a notification. If the content of the notification is in a group and the recipient has configured direct notifications
    for this group. An email task wil be triggered with this notification
    """
    with schema_context(schema_name):
        Model = apps.get_model(model_name)
        instance = Model.objects.filter(id=entity_id).first()
        sender = User.objects.filter(id=sender_id).first()

        if not instance:
            LOGGER.warning("Entity with id %s does not exist.")
            return

        if not sender:
            # sender is probably no active user don't create notifications for inactive users
            LOGGER.warning("Sender with id %s is not active.")
            return

        if verb == "created":
            recipients = []
            if not getattr(instance, "should_notify_on_create", True):
                return

            if not instance.group:
                return

            for member in instance.group.members.filter(type="member").exclude(
                is_notifications_enabled=False
            ):
                if sender == member.user:
                    continue
                if not instance.can_read(member.user):
                    continue
                recipients.append(member.user)

            if not instance.notifications_created:
                instance.notifications_created = True
                instance.save()

        elif verb == "commented":
            recipients = []
            if hasattr(instance, "followers"):
                for follower in instance.followers():
                    if sender == follower:
                        continue
                    if not instance.can_read(follower):
                        continue
                    # do not create not if is_comment_notifications_enabled is false
                    if not follower.profile.is_comment_notifications_enabled:
                        continue
                    recipients.append(follower)
        elif verb == "mentioned":
            recipients = []
            unmentioned_users = User.objects.get_unmentioned_users(
                instance.mentioned_users, instance
            )
            for user in unmentioned_users:
                if sender == user:
                    continue
                if not instance.can_read(user):
                    continue
                # do not create not if is_mention_notifications_enabled is false
                if not user.profile.is_mention_notifications_enabled:
                    continue
                recipients.append(user)
        elif verb == "custom":
            recipients = instance.recipients()
        else:
            return

        # tuple with list is returned, get the notification created
        notifications = notify.send(
            sender, recipient=recipients, verb=verb, action_object=instance
        )[0][1]

        # send direct mail and push notification where enabled
        from core.mail_builders.notifications import (
            MailTypeEnum,
            schedule_notification_mail,
        )

        for notification in notifications:
            push_enabled, direct_mail_enabled = False, False
            try:
                recipient = User.objects.get(id=notification.recipient_id)
                if verb == "created" and getattr(instance, "group", None):
                    membership = GroupMembership.objects.get(
                        user=recipient, group=instance.group
                    )
                    push_enabled = membership.is_notification_push_enabled
                    direct_mail_enabled = membership.is_notification_direct_mail_enabled
                elif verb == "mentioned":
                    push_enabled = (
                        recipient.profile.is_mention_notification_push_enabled
                    )
                    direct_mail_enabled = (
                        recipient.profile.is_mention_notification_direct_mail_enabled
                    )
                elif verb == "commented":
                    push_enabled = (
                        recipient.profile.is_comment_notification_push_enabled
                    )
                    direct_mail_enabled = (
                        recipient.profile.is_comment_notification_direct_mail_enabled
                    )
            except Exception:
                continue

            # send email direct and mark emailed as True
            if direct_mail_enabled:
                schedule_notification_mail(
                    recipient, [notification], MailTypeEnum.DIRECT
                )

            # send push notification
            if push_enabled and config.PUSH_NOTIFICATIONS_ENABLED:
                send_push(recipient, sender, verb, instance)


@shared_task(bind=True, ignore_result=True)
def create_chat_webpush_notification(
    self, schema_name, verb, model_name, entity_id, sender_id
):
    with schema_context(schema_name):
        instance = ChatMessage.objects.filter(id=entity_id).first()

        sender = User.objects.filter(id=sender_id).first()
        if config.PUSH_NOTIFICATIONS_ENABLED:
            if instance.chat.type == "GROUP":
                recipients = instance.chat.group.members.filter(
                    user___profile__is_chat_group_notification_push_enabled=True
                ).exclude(user=sender)
                for recipient in recipients:
                    if instance.chat.get_is_notification_push_enabled(recipient.user):
                        send_push(recipient.user, sender, verb, instance)

            if instance.chat.type == "USER":
                recipients = instance.chat.users.filter(
                    user___profile__is_chat_users_notification_push_enabled=True
                ).exclude(user=sender)
                for recipient in recipients:
                    if instance.chat.get_is_notification_push_enabled(recipient.user):
                        send_push(recipient.user, sender, verb, instance)
