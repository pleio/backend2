import logging

from django import template

register = template.Library()
logger = logging.getLogger(__name__)


@register.simple_tag(name="concat")
def do_concat(*parts):
    return "".join(map(str, parts))


@register.simple_tag()
def define(value):
    return value
