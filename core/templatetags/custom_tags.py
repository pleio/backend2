from django import template
from django.utils.html import mark_safe
from django_tenants.utils import schema_context

from core.lib import registration_url
from core.utils.color import hex_color_tint as hex_color_tint_processor
from core.utils.convert import tiptap_to_html as tiptap_to_html_processor

register = template.Library()


@register.simple_tag(
    name="tiptap_to_html",
)
def tiptap_to_html(data):
    return mark_safe(tiptap_to_html_processor(data))


@register.simple_tag(
    name="hex_color_tint",
)
def hex_color_tint(color, tint=0.5):
    return hex_color_tint_processor(color, tint)


@register.simple_tag(
    name="registration_url",
)
def registration_url_tag():
    return registration_url()


@register.filter(
    name="commaint",
)
def commaint_tag(number):
    return "{:g}".format(number)


@register.simple_tag(
    name="piwik_container_url",
)
def piwik_container_url(piwik_url=""):
    with schema_context("public"):
        from control.models import Configuration

        config = Configuration.objects.filter(
            id="piwik_container_url_%s" % piwik_url
        ).first()
        if config:
            return config.value

    return None
