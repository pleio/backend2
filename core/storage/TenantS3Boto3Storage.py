from botocore.config import Config
from botocore.exceptions import ClientError
from django.db import connection
from storages.backends.s3boto3 import S3Boto3Storage


class TenantS3Boto3Storage(S3Boto3Storage):
    def __init__(self, **settings):
        super().__init__(**settings)
        self.client_config = Config(
            s3={"addressing_style": self.addressing_style},
            signature_version=self.signature_version,
            proxies=self.proxies,
            max_pool_connections=100,
        )

    def _get_or_create_bucket(self, name):
        """
        Retrieves a bucket if it exists, otherwise creates it.
        """
        bucket = self.connection.Bucket(name)
        try:
            bucket.meta.client.head_bucket(Bucket=name)
        except ClientError:
            bucket.create()
        return bucket

    @property
    def bucket(self):
        """
        Get the current bucket if it is not already set.
        """
        if self._bucket is None:
            self._bucket = self._get_or_create_bucket(self.bucket_name)

        return self._bucket

    @property
    def location(self):
        return connection.schema_name
