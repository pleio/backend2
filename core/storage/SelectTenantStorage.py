import logging

from django.conf import settings
from django.core.files.storage import Storage, storages

from core.lib import tenant_instance

logger = logging.getLogger(__name__)


class SelectTenantStorage(Storage):
    def __init__(self, *args, **kwargs):
        super(Storage, self).__init__(*args, **kwargs)

    def _get_storage(self):
        tenant = None
        try:
            tenant = tenant_instance()
        except Exception:
            pass

        if tenant and tenant.custom_file_storage:
            if settings.STORAGES.get(tenant.custom_file_storage):
                return storages[tenant.custom_file_storage]
            else:
                logger.error(
                    "Invalid custom_file_storage: '%s' for tenant: %s - falling back to default storage.",
                    tenant.custom_file_storage,
                    tenant.schema_name,
                )

        return storages[settings.DEFAULT_STORAGE]

    def _get_storage_path(self, name):
        storage = self._get_storage()
        return storage._get_storage_path(name)

    def _open(self, name, mode="rb"):
        return self._get_storage()._open(name, mode)

    def _save(self, name, content):
        return self._get_storage().save(name, content)

    def get_valid_name(self, name):
        return self._get_storage().get_valid_name(name)

    def get_alternative_name(self, name):
        return self._get_storage().get_alternative_name(name)

    def get_available_name(self, name, max_length=None):
        return self._get_storage().get_available_name(name, max_length)

    def generate_filename(self, filename):
        return self._get_storage().generate_filename(filename)

    def path(self, name):
        return self._get_storage().path(name)

    def delete(self, name):
        return self._get_storage().delete(name)

    def exists(self, name):
        return self._get_storage().exists(name)

    def listdir(self, path):
        return self._get_storage().listdir(path)

    def size(self, name):
        return self._get_storage().size(name)

    def url(self, name):
        return self._get_storage().url(name)

    def get_access_time(self, name):
        return self._get_storage().get_access_time(name)

    def get_created_time(self, name):
        return self._get_storage().get_created_time(name)

    def get_modified_time(self, name):
        return self._get_storage().get_modified_time(name)
