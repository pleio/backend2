# Generated by Django 4.2.10 on 2024-02-27 16:34

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0113_alter_group_featured_alt_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="entity",
            name="published",
            field=models.DateTimeField(
                blank=True, default=django.utils.timezone.now, null=True
            ),
        ),
    ]
