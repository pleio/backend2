# Generated by Django 4.2.4 on 2023-11-02 14:30

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0101_translationcache_entity_input_language"),
    ]

    operations = [
        migrations.AddField(
            model_name="groupmembership",
            name="roles",
            field=django.contrib.postgres.fields.ArrayField(
                base_field=models.CharField(max_length=256),
                blank=True,
                default=list,
                size=None,
            ),
        ),
    ]
