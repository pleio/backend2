# Generated by Django 4.2.16 on 2024-12-30 10:57

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0132_siteaccessrequest_next_siteaccessrequest_processed_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="group",
            name="featured_video_thumbnail_url",
            field=models.TextField(blank=True, null=True),
        ),
    ]
