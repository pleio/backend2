# Generated by Django 4.2.5 on 2023-10-19 11:29

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0100_alter_group__start_page"),
    ]

    operations = [
        migrations.CreateModel(
            name="TranslationCache",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("reference", models.CharField(max_length=128)),
                ("hash", models.CharField(max_length=32)),
                ("language_code", models.CharField(max_length=16)),
                ("value", models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name="entity",
            name="input_language",
            field=models.CharField(blank=True, max_length=8, null=True),
        ),
    ]
