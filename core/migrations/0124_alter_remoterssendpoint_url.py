# Generated by Django 4.2.11 on 2024-08-16 07:22

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0123_userprofile_widget_settings"),
    ]

    operations = [
        migrations.AlterField(
            model_name="remoterssendpoint",
            name="url",
            field=models.TextField(),
        ),
    ]
