# Generated by Django 4.2.4 on 2023-09-01 14:53

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0098_alter_group_start_page_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="group",
            name="file_notifications",
            field=models.BooleanField(default=False),
        ),
    ]
