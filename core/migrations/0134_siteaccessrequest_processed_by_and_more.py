# Generated by Django 4.2.17 on 2025-01-17 13:09

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("core", "0133_group_featured_video_thumbnail_url"),
    ]

    operations = [
        migrations.AddField(
            model_name="siteaccessrequest",
            name="processed_by",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="processed_site_access_requests",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="siteaccessrequest",
            name="user_created",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="site_access_requests",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
