from django.utils.module_loading import import_string


class MailerBase:
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def send(self):
        msg = f"Please implement 'send()' for {self.__class__}"
        raise NotImplementedError(msg)

    def get_receiver(self):
        msg = f"Please implement 'get_receiver()' for {self.__class__}"
        raise NotImplementedError(msg)

    def get_receiver_email(self):
        msg = f"Please implement 'get_receiver_email()' for {self.__class__}"
        raise NotImplementedError(msg)

    def get_sender(self):
        msg = f"Please implement 'get_sender()' for {self.__class__}"
        raise NotImplementedError(msg)

    def get_subject(self):
        msg = f"Please implement 'get_subject()' for {self.__class__}"
        raise NotImplementedError(msg)

    class FailSilentlyError(Exception):
        pass

    class IgnoreInactiveUserMailError(FailSilentlyError):
        pass

    @staticmethod
    def assert_not_known_inactive_user(email):
        """Test if the user is not known to be inactive."""
        from user.models import User

        if User.objects.filter(is_active=False, email=email).exists():
            msg = f"Did not send mail to {email}"
            raise MailerBase.IgnoreInactiveUserMailError(msg)
        return True

    @classmethod
    def class_id(cls):
        return f"{cls.__module__}.{cls.__qualname__}"


def assert_valid_mailer_subclass(mailer):
    if isinstance(mailer, str):
        mailer_class = import_string(mailer)
    else:
        mailer_class = mailer
    assert issubclass(mailer_class, MailerBase), (
        f"{mailer_class} is not a subclass of core.mail_builders.base.MailerBase"
    )
