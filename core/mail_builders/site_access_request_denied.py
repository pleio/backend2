from django.utils.translation import gettext as _

from core import config
from core.mail_builders.template_mailer import TemplateMailerBase
from core.utils.entity import load_entity_by_id


def schedule_site_access_request_denied_mail(email, name, sender, message):
    from core.models import MailInstance

    MailInstance.objects.submit(
        SiteAccessRequestDeniedMailer,
        {"email": email, "name": name, "sender": sender.guid, "message": message},
    )


class SiteAccessRequestDeniedMailer(TemplateMailerBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.email = kwargs["email"]
        self.name = kwargs["name"]
        self.message = kwargs["message"]
        self.sender = load_entity_by_id(kwargs["sender"], ["user.User"])

    def get_context(self):
        return {
            **super().get_context(),
            **self.add_local_context(user=self.sender),
            "request_name": self.name,
            "message": self.message,
            "intro": config.SITE_MEMBERSHIP_DENIED_INTRO,
        }

    def get_language(self):
        return config.LANGUAGE

    def get_template(self):
        return "email/site_access_request_denied.html"

    def get_receiver(self):
        return None

    def get_receiver_email(self):
        return self.email

    def get_sender(self):
        return self.sender

    def get_subject(self):
        """
        Get the subject for this email.

        Accepts the variable `[site_name]`, which will be replaced with the site name from config.
        """
        subject = config.SITE_MEMBERSHIP_DENIED_SUBJECT
        if subject:
            return subject.replace("[site_name]", config.NAME)

        return _("Membership request declined for: %(site_name)s") % {
            "site_name": config.NAME
        }
