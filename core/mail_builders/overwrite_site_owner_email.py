from django.utils.translation import gettext

from core import config
from core.mail_builders.template_mailer import TemplateMailerBase
from user.models import User


def schedule_overwrite_site_owner_notification(sender, current_owner):
    from core.models import MailInstance

    MailInstance.objects.submit(
        OverwriteSiteOwnerMailer,
        {
            "sender": sender.guid,
            "current_owner": current_owner,
        },
    )


class OverwriteSiteOwnerMailer(TemplateMailerBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.sender = User.objects.get(id=self.kwargs["sender"])

    def get_context(self):
        return {
            **super().get_context(),
            **self.add_local_context(user=self.get_sender()),
        }

    def get_language(self):
        return config.LANGUAGE

    def get_template(self):
        return "email/overwrite_site_owner_email.html"

    def get_receiver(self):
        return None

    def get_receiver_email(self):
        return self.kwargs["current_owner"]

    def get_sender(self):
        return self.sender

    def get_subject(self):
        return gettext("Site owner changed")
