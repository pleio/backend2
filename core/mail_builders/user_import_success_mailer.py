from django.utils.translation import gettext as _

from core.mail_builders.template_mailer import TemplateMailerBase
from core.utils import user_importer
from user.models import User


def schedule_user_import_success(user, stats):
    from core.models import MailInstance

    MailInstance.objects.submit(
        UserImportSuccessMailer,
        {
            "user": user.guid,
            "stats": stats,
        },
    )


class UserImportSuccessMailer(TemplateMailerBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.user = User.objects.get(id=kwargs.get("user"))
        self.stats = kwargs["stats"]

    def get_context(self):
        context = {
            **super().get_context(),
            **self.add_local_context(user=self.user),
            "errors": {},
        }
        for key, value in self.stats.items():
            if key == "processed":
                context["processed"] = value or 0
            elif key == user_importer.STATUS_CREATED:
                context["stats_created"] = value or 0
            elif key == user_importer.STATUS_UPDATED:
                context["stats_updated"] = value or 0
            elif key == user_importer.STATUS_INCOMPLETE_INPUT:
                context["stats_incomplete"] = value or 0
            elif key == user_importer.IDP_STATUS_ALREADY_EXISTS:
                context["idp_exists"] = value or 0
            elif key == user_importer.IDP_STATUS_CREATED:
                context["idp_added"] = value or 0
            else:
                context["errors"][key] = value or 0

        return context

    def get_language(self):
        return self.user.get_language()

    def get_template(self):
        return "email/user_import_success.html"

    def get_receiver(self):
        return self.user

    def get_receiver_email(self):
        return self.user.email

    def get_sender(self):
        pass

    def get_subject(self):
        return _("Import was a success")
