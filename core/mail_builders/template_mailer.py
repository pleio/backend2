from email.utils import formataddr

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.utils import translation

from core import config
from core.context_processor import mail_context_processor
from core.lib import html_to_text
from core.mail_builders.base import MailerBase
from core.utils.color import hex_black_or_white
from core.utils.mail import EmailSettingsTokenizer
from core.utils.tenants import assert_site_active


class TemplateMailerBase(MailerBase):
    def add_local_context(self, mail_info=None, user=None):
        from core.lib import get_base_url, get_full_url

        context = {
            "site_url": get_base_url(),
            "site_name": config.NAME,
            "primary_color": config.COLOR_PRIMARY,
            "header_color": (
                config.COLOR_HEADER if config.COLOR_HEADER else config.COLOR_PRIMARY
            ),
            "header_text_color": hex_black_or_white(
                config.COLOR_HEADER if config.COLOR_HEADER else config.COLOR_PRIMARY
            ),
        }
        if user:
            mail_info = user.as_mailinfo()
            tokenizer = EmailSettingsTokenizer()
            context["mail_settings_url"] = get_full_url(tokenizer.create_url(user))
            context["user_url"] = get_full_url(user.url)
        if mail_info:
            context["user_name"] = mail_info["name"]
        return context

    def get_context(self):
        return mail_context_processor(None)

    def get_language(self):
        msg = f"Please implement 'get_language()' for {self}"
        raise NotImplementedError(msg)

    def get_template(self):
        msg = f"Please implement 'get_template()' for {self}"
        raise NotImplementedError(msg)

    def get_headers(self):
        return {}

    def pre_send(self, email):
        pass

    def send(self):
        assert_site_active(self.FailSilentlyError("Site is not active"))
        self.assert_not_known_inactive_user(self.get_receiver_email())
        with translation.override(self.get_language()):
            html_template = get_template(self.get_template())
            html_content = html_template.render(self.get_context())
            text_content = html_to_text(html_content)

            from_mail = formataddr((config.NAME, settings.FROM_EMAIL))

            email = EmailMultiAlternatives(
                subject=self.get_subject(),
                body=text_content,
                from_email=from_mail,
                to=[self.get_receiver_email()],
                headers=self.get_headers(),
            )
            email.attach_alternative(html_content, "text/html")

            self.pre_send(email)
            email.send()
