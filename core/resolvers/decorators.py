import logging
import traceback
from functools import wraps

from django_tenants.utils import tenant_context
from graphql import GraphQLError

logger = logging.getLogger(__name__)


def resolver_without_object_info(f):
    """
    Decorator to simplify mutation and query resolvers.
    """

    @wraps(f)
    def wrapper(obj, info, **kwargs):
        return f(**kwargs)

    return wrapper


def resolve_property(f):
    """
    Decorator for property resolvers that dont need the info argument.
    """

    @wraps(f)
    def wrapper(obj, info, **kwargs):
        return f(obj, **kwargs)

    return wrapper


def resolver_with_request(f):
    """
    Decorator to simplify mutation and query resolvers.
    1. Skip obj and info arguments.
    2. Put the request as first parameter, similar to views.
    """

    @wraps(f)
    def wrapper(obj, info, **kwargs):
        return f(info.context["request"], **kwargs)

    return wrapper


def resolve_obj_request(f):
    """
    Decorator to simplify mutation and query resolvers.
    1. Get the request property from the info.context.
    """

    @wraps(f)
    def wrapper(obj, info, **kwargs):
        return f(obj, info.context["request"], **kwargs)

    return wrapper


def resolve_obj(f):
    """
    Decorator to simplify mutation and query resolvers.
    1. Skip info argument.
    """

    @wraps(f)
    def wrapper(obj, info, **kwargs):
        return f(obj, **kwargs)

    return wrapper


def add_exception_debug_backtrace(f):
    """
    Decorator to report exceptions in resolvers.
    The backtrace is not always given. This decorator ensures that the backtrace is always given.
    It's intended use is for debugging purposes during development.
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)

        except Exception as e:
            logger.error("Graphql error: %s", traceback.format_exc())

            if isinstance(e, GraphQLError):
                raise

            raise Exception(traceback.format_exc())

    return wrapper


def resolve_in_tenant_context(f):
    """
    Decorator to ensure that the tenant context is set in graphql subscriptions.
    """

    @wraps(f)
    def wrapper(obj, info, **kwargs):
        if (
            hasattr(info.context["request"], "scope")
            and "tenant" in info.context["request"].scope
        ):
            with tenant_context(info.context["request"].scope["tenant"]):
                return f(obj, info, **kwargs)
        return f(obj, info, **kwargs)

    return wrapper
