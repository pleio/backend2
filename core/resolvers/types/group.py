from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum

from core import config
from core.constances import MEMBERSHIP
from core.lib import get_access_ids
from core.models import GroupInvitation, ProfileField, Subgroup
from core.resolvers import decorators, shared
from entities.file.models import FileFolder
from user.models import User

group = ObjectType("Group")


@group.field("startPage")
def resolve_group_start_page(obj, info):
    return shared.obj_if_access(obj.start_page, info)


@group.field("description")
def resolve_group_description(obj, info):
    return obj.description


@group.field("welcomeMessage")
def resolve_welcome_message(obj, info):
    return obj.welcome_message


@group.field("requiredProfileFieldsMessage")
def resolve_required_fields_message(obj, info):
    return obj.required_fields_message


@group.field("introduction")
def resolve_introduction(obj, info):
    user = info.context["request"].user

    if obj.is_introduction_public:
        return obj.introduction

    if user.is_authenticated and (obj.is_full_member(user) or user.is_site_admin):
        return obj.introduction

    return ""


@group.field("isIntroductionPublic")
def resolve_is_introduction_public(obj, info):
    return obj.is_introduction_public


@group.field("url")
def resolve_group_url(obj, info):
    return obj.url


@group.field("isClosed")
def resolve_group_is_closed(obj, info):
    return obj.is_closed


@group.field("isHidden")
def resolve_group_is_hidden(obj, info):
    return obj.is_hidden


@group.field("isMembershipOnRequest")
def resolve_group_is_membership_on_request(obj, info):
    return obj.is_membership_on_request


@group.field("autoNotification")
def auto_notification(obj, info):
    return obj.auto_notification


@group.field("fileNotification")
def file_notification(obj, info):
    return bool(obj.file_notifications)


@group.field("isFeatured")
def resolve_group_is_featured(obj, info):
    return obj.is_featured


@group.field("isLeavingGroupDisabled")
def resolve_group_is_leaving_group_disabled(obj, info):
    return obj.is_leaving_group_disabled


@group.field("isMenuAlwaysVisible")
def resolve_group_is_menu_always_visible(obj, info):
    return obj.is_menu_always_visible


@group.field("isAutoMembershipEnabled")
def resolve_group_is_auto_membership_enabled(obj, info):
    return obj.is_auto_membership_enabled


@group.field("canChangeOwnership")
def resolve_group_can_change_ownership(obj, info):
    user = info.context["request"].user
    if not user.is_authenticated:
        return False

    if user == obj.owner or user.is_site_admin:
        return True

    return False


@group.field("isNotificationsEnabled")
def resolve_group_is_notifications_enabled(obj, info):
    user = info.context["request"].user

    if not user.is_authenticated:
        return None

    try:
        return obj.members.get(user=user).is_notifications_enabled
    except ObjectDoesNotExist:
        return None


@group.field("isNotificationDirectMailEnabled")
def resolve_group_is_notification_direct_mail_enabled(obj, info):
    user = info.context["request"].user

    if not user.is_authenticated:
        return None

    try:
        return obj.members.get(user=user).is_notification_direct_mail_enabled
    except ObjectDoesNotExist:
        return None


@group.field("isNotificationPushEnabled")
def resolve_group_is_notification_push_enabled(obj, info):
    user = info.context["request"].user

    if not user.is_authenticated:
        return None

    try:
        return obj.members.get(user=user).is_notification_push_enabled
    except ObjectDoesNotExist:
        return None


@group.field("subgroups")
def resolve_group_subgroups(obj, info):
    return obj.subgroups.all()


@group.field("invite")
def resolve_group_invite(obj, info, q=None, offset=0, limit=10):
    # TODO: schema must be altered, this should not be type InvitedList
    request_user = info.context["request"].user

    if not obj.can_write(request_user):
        return {"total": 0, "edges": []}

    if q:
        users = User.objects.filter_name(q)[offset : offset + limit]
    else:
        users = User.objects.all()[offset : offset + limit]

    invites = []

    for user in users:
        if user == info.context["request"].user:
            continue
        if obj.is_member(user):
            continue
        invite = GroupInvitation(invited_user=user)
        invite.invited = False
        invites.append(invite)

    edges = invites[offset : offset + limit]

    return {"total": len(invites), "edges": edges}


@group.field("invited")
def resolve_group_invited(obj, info, q=None, offset=0, limit=10):
    request_user = info.context["request"].user

    if not obj.can_write(request_user):
        return {"total": 0, "edges": []}

    invited = obj.invitations.filter(group=obj)

    edges = invited[offset : offset + limit]
    return {"total": invited.count(), "edges": edges}


@group.field("membershipRequests")
def resolve_group_membership_requests(obj, info):
    request_user = info.context["request"].user

    if not obj.can_write(request_user):
        return {"total": 0, "edges": []}

    membership_requests = obj.members.filter(type="pending")
    users = []
    for m in membership_requests:
        users.append(m.user)
    return {"total": len(users), "edges": users}


@group.field("memberCount")
def resolve_group_member_count(group, info):
    return group.members.filter(
        type="member",
        user__is_superadmin=False,
        user__is_active=True,
    ).count()


@group.field("isOwner")
def resolve_is_owner(obj, info):
    return obj.is_owner(info.context["request"].user)


@group.field("members")
def resolve_group_members(
    group, info, q=None, offset=0, limit=5, inSubgroupId=None, notInSubgroupId=None
):
    request_user = info.context["request"].user
    if not request_user.is_authenticated:
        return {"total": 0, "edges": [], "fieldsInOverview": []}

    members = group.members.filter(
        type="member",
        user__is_superadmin=False,
        user__is_active=True,
    )

    if inSubgroupId:
        subgroup_members = Subgroup.objects.get(id=inSubgroupId).members.filter(
            is_superadmin=False, is_active=True
        )
        members = members.filter(user__in=subgroup_members)

    if notInSubgroupId:
        subgroup_members = Subgroup.objects.get(id=notInSubgroupId).members.filter(
            is_superadmin=False, is_active=True
        )
        members = members.exclude(user__in=subgroup_members)

    if q:
        members = members.filter(user__name__icontains=q)

    edges = members.order_by("admin_weight", "user__name")[offset : offset + limit]

    return {
        "total": members.count(),
        "edges": edges,
        "fieldsInOverview": shared.resolve_group_fields_in_overview(group),
    }


@group.field("membership")
def resolve_membership(group, info):
    user = info.context["request"].user

    if group.is_full_member(user):
        return MEMBERSHIP.joined

    if group.is_pending_member(user):
        return MEMBERSHIP.requested

    return MEMBERSHIP.not_joined


@group.field("readAccessIds")
def resolve_access_ids(group, info):
    return get_access_ids(group)


@group.field("writeAccessIds")
def resolve_write_access_ids(group, info):
    return get_access_ids(group, exclude=[2])


@group.field("defaultReadAccessId")
def resolve_default_access_id(group, info):
    if group.is_closed:
        return 4

    return config.DEFAULT_ACCESS_ID


@group.field("defaultWriteAccessId")
def resolve_default_write_access_id(group, info):
    return 0


@group.field("icon")
def resolve_icon(group, info):
    return group.icon


@group.field("showMemberProfileFields")
def resolve_profile_fields_filter(group, info):
    return [
        setting.profile_field
        for setting in group.profile_field_settings.filter(show_field=True)
    ]


@group.field("requiredProfileFields")
def resolve_required_profile_fields_filter(group, info):
    return [
        setting.profile_field
        for setting in group.profile_field_settings.filter(is_required=True)
    ]


@group.field("isChatEnabled")
def resolve_is_chat_anabled(obj, info):
    return obj.is_chat_enabled


@group.field("defaultTags")
def resolve_default_tags(obj, info):
    return obj.content_presets.get("defaultTags")


@group.field("defaultTagCategories")
def resolve_default_tag_categories(obj, info):
    return obj.content_presets.get("defaultTagCategories")


@group.field("chat")
def resolve_chat(obj, info):
    user = info.context["request"].user

    if obj.is_chat_enabled and obj.is_full_member(user):
        try:
            return obj.chat
        except Exception:
            return None

    return None


@group.field("isJoinButtonVisible")
def resolve_is_join_button_hidden(obj, info):
    return obj.is_join_button_enabled


@group.field("fileDiskUsage")
def resolve_file_disk_usage(obj, info):
    try:
        user = info.context["request"].user
        assert user.is_authenticated
        assert user.is_site_admin or obj.is_admin(user) or obj.is_owner(user)

        file_folder_size = 0

        f = FileFolder.objects.filter(type=FileFolder.Types.FILE, group=obj).aggregate(
            total_size=Sum("size")
        )
        if f.get("total_size", 0):
            file_folder_size = f.get("total_size", 0)
        return file_folder_size
    except Exception:
        pass
    return 0


@group.field("autoMembershipFields")
@decorators.resolve_property
def resolve_auto_membership_fields(obj):
    values = {f["guid"]: f["value"] for f in obj.auto_membership_fields}
    return [
        {
            "field": profile_field_item,
            "value": values.get(profile_field_item.guid, []),
        }
        for profile_field_item in ProfileField.objects.filter(
            is_in_auto_group_membership=True
        )
    ]


group.set_field("richDescription", shared.resolve_entity_rich_description)
group.set_field("excerpt", shared.resolve_entity_excerpt)
group.set_field("tags", shared.resolve_entity_tags)
group.set_field("tagCategories", shared.resolve_entity_categories)
group.set_field("canEdit", shared.resolve_entity_can_edit)
group.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
group.set_field("featured", shared.resolve_entity_featured)
