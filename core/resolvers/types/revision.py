from ariadne import ObjectType

from core.lib import str_to_datetime
from core.models import Group
from core.resolvers import shared
from core.utils.entity import load_entity_by_id
from user.models import User

revision = ObjectType("Revision")
content_version = ObjectType("ContentVersion")

resolvers = [revision, content_version]


@revision.field("authors")
def resolve_coauthors(obj, info):
    if obj.coauthors:
        return User.objects.filter(id__in=obj.coauthors)
    return [obj.author]


@revision.field("timeCreated")
def resolve_revsion_time_created(obj, info):
    return obj.created_at


@revision.field("statusPublishedChanged")
def resolve_status_published_changed(obj, info):
    if "statusPublished" in obj.content:
        return obj.content["statusPublished"]
    return None


@revision.field("changedFields")
def resolve_changed_fields(obj, info):
    return [str(key) for key in obj.content.keys() if key != "statusPublished"]


@revision.field("content")
def resolve_content(obj, info):
    content = {}
    content.update(obj.unchanged)
    content.update(obj.content)
    return content


@revision.field("originalContent")
def resolve_original_content(obj, info):
    content = {}
    content.update(obj.unchanged)
    content.update(obj.previous_content)
    return content


@revision.field("type")
def resolve_type(obj, info):
    return "update" if obj.is_update else "create"


@revision.field("container")
def resolve_container(obj, info):
    return shared.obj_if_access(obj.container, info)


@content_version.field("timeCreated")
def resolve_time_created(obj, info):
    return str_to_datetime(obj.get("timeCreated"))


@content_version.field("timePublished")
def resolve_time_published(obj, info):
    return str_to_datetime(obj.get("timePublished"))


@content_version.field("scheduleArchiveEntity")
def resolve_time_archive_entity(obj, info):
    return str_to_datetime(obj.get("scheduleArchiveEntity"))


@content_version.field("scheduleDeleteEntity")
def resolve_time_delete_entity(obj, info):
    return str_to_datetime(obj.get("scheduleDeleteEntity"))


@content_version.field("group")
def resolve_group(obj, info):
    if obj.get("groupGuid"):
        return Group.objects.get(id=obj.get("groupGuid"))


@content_version.field("owner")
def resolve_owner(obj, info):
    if obj.get("ownerGuid"):
        return User.objects.get(id=obj.get("ownerGuid"))


@content_version.field("suggestedItems")
def resolve_suggested_items(obj, info):
    if obj.get("suggestedItems"):
        return [
            load_entity_by_id(guid, ["core.Entity"])
            for guid in obj.get("suggestedItems")
        ]


@content_version.field("parent")
def resolve_parent(obj, info):
    if obj.get("containerGuid"):
        container = load_entity_by_id(obj.get("containerGuid"), ["core.Entity"])
        return shared.obj_if_access(container, info)
