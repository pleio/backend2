from ariadne import ObjectType

from core.models.featured import FeaturedCoverMixin
from core.resolvers.decorators import resolve_property

video_metadata = ObjectType("VideoMetadata")


@video_metadata.field("thumbnailUrl")
@resolve_property
def resolve_thumbnail_url(obj):
    return FeaturedCoverMixin.get_video_thumbnail_url(obj["_url"])
