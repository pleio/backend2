import re

import bleach
from ariadne import ObjectType

rss_item = ObjectType("RssItem")


@rss_item.field("guid")
def resolve_id(obj, *_):
    return obj.get("id")


@rss_item.field("description")
def resolve_description(obj, *_):
    try:
        clean_html = bleach.clean(
            obj.get("summary") or "",
            tags=["b", "strong", "i", "em", "a"],
            attributes=["href"],
            strip=True,
        )
        return re.sub(r"\s+", " ", clean_html).strip()
    except TypeError:
        pass
    return ""
