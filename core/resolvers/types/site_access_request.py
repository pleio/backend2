import logging

from ariadne import ObjectType

from core.resolvers import decorators
from core.utils.onboarding import OnboardingProfile

site_access_request = ObjectType("SiteAccessRequest")

logger = logging.getLogger(__name__)


@site_access_request.field("profileFields")
@decorators.resolve_obj
def resolve_profile_fields(obj):
    return [*OnboardingProfile(obj.profile).get_fields()]


@site_access_request.field("status")
@decorators.resolve_obj
def resolve_site_access_request_status(obj):
    return obj.status


@site_access_request.field("reason")
@decorators.resolve_obj
def resolve_site_access_request_reason(obj):
    return obj.reason


@site_access_request.field("processedBy")
@decorators.resolve_obj
def resolve_site_access_request_processed_by(obj):
    return obj.processed_by


@site_access_request.field("timeCreated")
@decorators.resolve_obj
def resolve_site_access_request_time_created_at(obj):
    return obj.created_at


@site_access_request.field("timeUpdated")
@decorators.resolve_obj
def resolve_site_access_request_time_updated(obj):
    return obj.updated_at
