from ariadne import ObjectType

from core.resolvers import shared
from user.models import User

notification = ObjectType("Notification")


@notification.field("id")
def resolve_id(obj, info):
    return obj.id


@notification.field("action")
def resolve_action(obj, info):
    return obj.verb


@notification.field("performer")
def resolve_performer(obj, info):
    if obj.verb == "created" and getattr(obj.action_object, "hide_owner", False):
        return None
    return User.objects.with_deleted().get(id=obj.actor_object_id)


@notification.field("entity")
def resolve_entity(obj, info):
    if not obj.action_object or obj.verb == "custom":
        return None

    return shared.obj_if_access(obj.action_object, info)


@notification.field("customMessage")
def resolve_custom_message(obj, info):
    if obj.verb != "custom":
        return

    return obj.action_object.custom_message()


@notification.field("container")
def resolve_container(obj, info):
    action_object = shared.obj_if_access(obj.action_object, info)
    if action_object and hasattr(action_object, "group"):
        return action_object.group

    return None


@notification.field("timeCreated")
def resolve_timestamp(obj, info):
    return obj.timestamp


@notification.field("isUnread")
def resolve_isUnread(obj, info):
    return obj.unread
