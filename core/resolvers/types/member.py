from ariadne import ObjectType

from core.constances import GROUP_MEMBER_ROLES

member = ObjectType("Member")


@member.field("roles")
def resolve_roles(obj, info):
    roles = []
    if GROUP_MEMBER_ROLES.GROUP_ADMIN in obj.roles:
        roles.append("admin")
    if GROUP_MEMBER_ROLES.GROUP_NEWS_EDITOR in obj.roles:
        roles.append("newsEditor")

    return roles


@member.field("isOwner")
def resolve_is_owner(obj, info):
    return obj.group.is_owner(obj.user)


@member.field("email")
def resolve_email(obj, info):
    request_user = info.context["request"].user

    if obj.user == request_user or obj.group.can_write(request_user):
        return obj.user.email
    return None


@member.field("user")
def resolve_user(obj, info):
    return obj.user


@member.field("memberSince")
def resolve_member_since(obj, info):
    return obj.created_at
