from ariadne import ObjectType

video_call = ObjectType("VideoCall")


@video_call.field("guid")
def resolve_guid(obj, *_):
    return obj.guid


@video_call.field("isModerator")
def resolve_is_moderator(obj, info):
    return obj.is_moderator(info.context["request"].user)


@video_call.field("jwtToken")
def resolve_jwt_token(obj, info):
    return obj.get_jwt_token(info.context["request"].user)


@video_call.field("title")
def resolve_entity_video_call_title(obj, info):
    return obj.get_title()
