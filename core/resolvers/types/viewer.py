import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core import config, constances
from core.lib import get_model_by_subtype
from core.models import Entity, Group
from core.resolvers import decorators, shared
from core.utils.admin_task_counters import (
    CommentPublicationRequestsCount,
    ContentPublicationRequestsCount,
    DeleteAccountRequestsCount,
    FilePublicationRequestsCount,
    SiteAccessRequestsCount,
)
from core.utils.entity import load_entity_by_id
from core.utils.rich_text_editor_features import FEATURES_AVAILABLE
from user.admin_permissions import (
    HasAccessRequestManagementPermission,
    HasAdminAccessPermission,
    HasContentManagementPermission,
    HasFullControlPermission,
    HasPublicationRequestManagementPermission,
    HasUserManagementPermission,
)
from user.models import User

logger = logging.getLogger(__name__)

viewer = ObjectType("Viewer")


@viewer.field("user")
def resolve_user(_, info):
    user = info.context["request"].user
    if user.is_authenticated:
        return user
    return None


@viewer.field("canWriteToContainer")
def resolve_can_write_to_container(
    obj,
    info,
    subtype=None,
    containerGuid=None,
):
    """
    Test if user can create content.
    """
    user = info.context["request"].user

    if not user.is_authenticated:
        return False

    if user.is_site_admin:
        return True

    group = None
    if containerGuid:
        try:
            entity = load_entity_by_id(
                containerGuid,
                [Entity, "core.Group", User],
            )

            if isinstance(entity, User):
                return entity.id == user.id

            if isinstance(entity, Entity):
                return entity.can_write(user)

            if isinstance(entity, Group):
                # Container is a group; The group is used to test create-subtype permission.
                group = entity
            else:
                raise GraphQLError(constances.COULD_NOT_FIND)

        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)

    if model := get_model_by_subtype(subtype):
        return model.can_add(user, group=group)

    return False


@viewer.field("profileSetManager")
def resolve_user_profile_set_manager(_, info):
    user = info.context["request"].user
    return user.profile_sets.all()


@viewer.field("canAccessAdmin")
def resolve_can_access_admin(_, info, permission):
    permission_classes = [
        HasAdminAccessPermission,
        HasAccessRequestManagementPermission,
        HasFullControlPermission,
        HasUserManagementPermission,
        HasPublicationRequestManagementPermission,
        HasContentManagementPermission,
    ]

    for permission_class in permission_classes:
        if permission == permission_class.PERMISSION:
            test = permission_class(info.context["request"].user)
            return test.has_permission()


@viewer.field("canUpdateAccessLevel")
@decorators.resolver_with_request
def resolve_can_update_access_level(request, groupGuid=None):
    user = request.user
    group = (groupGuid and load_entity_by_id(groupGuid, ["core.Group"])) or None

    return shared.update_access_level_allowed(user, group)


@viewer.field("richTextEditorFeature")
def resolve_rich_text_editor_features(_, info, feature):
    user = info.context["request"].user

    for feature_test in [feature_class(user) for feature_class in FEATURES_AVAILABLE]:
        if feature_test.key == feature:
            return feature_test.is_enabled()

    return False


@viewer.field("showSuspendAdminPrivilegesToggle")
@decorators.resolver_with_request
def resolve_show_suspend_admin_privileges_toggle(request):
    """
    Read user role directly from the user object. Else we can not release
    suspension of the admin privileges.
    """
    return getattr(request.user, "can_suspend_admin_privileges", False)


@viewer.field("isSuspendAdminPrivileges")
@decorators.resolver_with_request
def resolve_is_suspend_admin_privileges(request):
    user = request.user
    return user.is_authenticated and user.suspend_admin_privileges


@viewer.field("requiresContentModeration")
@decorators.resolver_with_request
def resolve_requires_content_moderation(request, subtype, groupGuid=None):
    if request.user.is_authenticated and request.user.is_editor:
        return False

    if Group.objects.is_group_staff(groupGuid, request.user):
        return False

    return (
        subtype in config.REQUIRE_CONTENT_MODERATION_FOR
        and config.CONTENT_MODERATION_ENABLED
    )


@viewer.field("requiresCommentModeration")
@decorators.resolver_with_request
def resolve_requires_comment_moderation(request, subtype, groupGuid=None):
    if request.user.is_authenticated and request.user.is_editor:
        return False

    if Group.objects.is_group_staff(groupGuid, request.user):
        return False

    return (
        subtype in config.REQUIRE_COMMENT_MODERATION_FOR
        and config.COMMENT_MODERATION_ENABLED
    )


@viewer.field("canInsertMedia")
@decorators.resolver_with_request
def resolve_can_insert_media(request, subtype):
    if subtype == "question" and config.PREVENT_INSERT_MEDIA_AT_QUESTIONS:
        # Prevent insert media at questions unless user is site-admin.
        return request.user.is_authenticated and request.user.is_site_admin

    return True


@viewer.field("countAdminTasks")
@decorators.resolver_with_request
def resolve_count_admin_tasks(request, kind):
    kinds_of_tasks = {
        counter.key: counter
        for counter in [
            DeleteAccountRequestsCount,
            SiteAccessRequestsCount,
            ContentPublicationRequestsCount,
            CommentPublicationRequestsCount,
            FilePublicationRequestsCount,
        ]
    }

    if kind not in kinds_of_tasks:
        return

    return kinds_of_tasks[kind](request.user).count()
