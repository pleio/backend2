from ariadne import ObjectType

subgroup = ObjectType("Subgroup")


@subgroup.field("id")
def resolve_id(obj, info):
    return obj.id


@subgroup.field("name")
def resolve_name(obj, info):
    return obj.name


@subgroup.field("members")
def resolve_members(obj, info):
    return obj.members.all()
