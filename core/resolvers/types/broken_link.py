from ariadne import ObjectType

from core.constances import BROKEN_LINK_REASON

broken_link = ObjectType("BrokenLink")


@broken_link.field("source")
def resolve_source(obj, info):
    return obj.source


@broken_link.field("target")
def resolve_target(obj, info):
    return obj.target


@broken_link.field("reason")
def resolve_reason(obj, info):
    return BROKEN_LINK_REASON[obj.reason]
