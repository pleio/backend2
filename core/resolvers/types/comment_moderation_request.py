from ariadne import ObjectType

from core.resolvers import decorators

object_type = ObjectType("CommentModerationRequest")


@object_type.field("comments")
@decorators.resolve_obj_request
def resolve_comments(obj, request):
    return obj.comments.visible(user=request.user)


@object_type.field("status")
@decorators.resolve_obj
def resolve_status(obj):
    if obj.is_closed:
        return "closed"
    elif obj.is_assigned:
        return "assigned"
    else:
        return "open"


@object_type.field("assignedTo")
@decorators.resolve_obj
def resolve_assigned_to(obj):
    if obj.is_assigned:
        return obj.assigned_to
