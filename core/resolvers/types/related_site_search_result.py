from ariadne import ObjectType

from core.lib import datetime_isoformat
from core.resolvers import shared

related_site_search_result = ObjectType("RelatedSiteSearchResult")


@related_site_search_result.field("siteName")
def resolve_site_name(obj, info):
    return obj.site_name


@related_site_search_result.field("siteUrl")
def resolve_site_url(obj, info):
    return obj.site_url


@related_site_search_result.field("url")
def resolve_absolute_url(obj, info):
    return obj.url


@related_site_search_result.field("startDate")
def resolve_start_date(obj, info):
    return datetime_isoformat(obj.start_date)


@related_site_search_result.field("endDate")
def resolve_end_date(obj, info):
    return datetime_isoformat(obj.start_date)


@related_site_search_result.field("mimeType")
def resolve_mimetype(obj, info):
    return obj.mime_type


@related_site_search_result.field("download")
def resolve_download(obj, info):
    return getattr(obj, "download_url", None)


related_site_search_result.set_field(
    "timePublished", shared.resolve_entity_time_published
)
