from ariadne import ObjectType

subgroup_list = ObjectType("SubgroupList")


@subgroup_list.field("total")
def resolve_total(obj, info):
    return len(obj)


@subgroup_list.field("edges")
def resolve_edges(obj, info):
    return obj
