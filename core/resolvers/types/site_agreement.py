from ariadne import ObjectType

site_agreement = ObjectType("SiteAgreement")


@site_agreement.field("name")
def resolve_name(obj, info):
    return obj.name


@site_agreement.field("description")
def resolve_description(obj, info):
    return obj.description


@site_agreement.field("accepted")
def resolve_accepted(obj, info):
    return obj.latest_accepted_for_current_tenant


@site_agreement.field("versions")
def resolve_versions(obj, info):
    return obj.versions.all()
