from ariadne import ObjectType
from django.conf import settings
from django.templatetags.static import static
from django.utils import timezone
from django.utils.translation import gettext_lazy
from django_tenants.utils import schema_context
from graphql import GraphQLError

import core.lib
from core import config, constances
from core.constances import USER_ROLES
from core.lib import (
    get_access_ids,
    get_activity_filters,
    get_base_url,
    get_entity_filters,
    get_exportable_content_types,
    get_exportable_user_fields,
    get_language_options,
    get_page_tag_filters,
    get_search_filters,
)
from core.models import (
    ProfileField,
    ProfileFieldValidator,
    ProfileSet,
    SiteAccessRequest,
    SiteInvitation,
    UserProfile,
)
from core.resolvers import decorators, shared
from entities.cms.models import Page
from entities.cms.row_resolver import RowSerializer
from user.admin_permissions import (
    HasAccessRequestManagementPermission,
    HasFullControlPermission,
    HasUserManagementPermission,
)
from user.models import User

site_settings_private = ObjectType("SiteSettings")
site_settings_public = ObjectType("Site")

resolvers = [
    site_settings_private,
    site_settings_public,
]


@site_settings_public.field("guid")
def resolve_guid(obj, info):
    return 1


@site_settings_public.field("name")
@site_settings_private.field("name")
def resolve_name(obj, info):
    return config.NAME


@site_settings_private.field("description")
def resolve_description(obj, info):
    return config.DESCRIPTION


@site_settings_public.field("language")
@site_settings_private.field("language")
def resolve_language(obj, info):
    return config.LANGUAGE


@site_settings_public.field("contentTranslation")
def resolve_content_translation(obj, info):
    return config.CONTENT_TRANSLATION


@site_settings_public.field("languageOptions")
def resolve_language_options_public(obj, info):
    active_languages = config.EXTRA_LANGUAGES
    active_languages.append(config.LANGUAGE)
    return [i for i in get_language_options() if i["value"] in active_languages]


@site_settings_private.field("languageOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_language_options(obj, info):
    return get_language_options()


@site_settings_private.field("extraLanguages")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_extra_languages(obj, info):
    return config.EXTRA_LANGUAGES


@site_settings_private.field("isClosed")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_is_closed(obj, info):
    return config.IS_CLOSED


@site_settings_private.field("allowRegistration")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_allow_registration(obj, info):
    return config.ALLOW_REGISTRATION


@site_settings_private.field("directRegistrationDomains")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_direct_registration_domains(obj, info):
    return config.DIRECT_REGISTRATION_DOMAINS


@site_settings_public.field("defaultReadAccessId")
@site_settings_private.field("defaultReadAccessId")
def resolve_default_access_id(obj, info):
    if config.IS_CLOSED and config.DEFAULT_ACCESS_ID == 2:
        config.DEFAULT_ACCESS_ID = 1
    return config.DEFAULT_ACCESS_ID


@site_settings_public.field("defaultWriteAccessId")
def resolve_default_write_access_id(obj, info):
    return 0


@site_settings_private.field("defaultReadAccessIdOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_default_access_id_options(obj, info):
    defaultAccessIdOptions = [
        {"value": 0, "label": gettext_lazy("Just me")},
        {"value": 1, "label": gettext_lazy("Logged in users")},
    ]

    if not config.IS_CLOSED:
        defaultAccessIdOptions.append({"value": 2, "label": gettext_lazy("Public")})
    else:
        # Reset default access ID when site is closed!
        if config.DEFAULT_ACCESS_ID == 2:
            config.DEFAULT_ACCESS_ID = 1

    return defaultAccessIdOptions


@site_settings_private.field("searchEngineIndexingEnabled")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_searchengine_indexing_enabled(obj, info):
    return config.ENABLE_SEARCH_ENGINE_INDEXING


@site_settings_private.field("piwikUrl")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_piwik_url(obj, info):
    return config.PIWIK_URL


@site_settings_private.field("piwikId")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_piwik_id(obj, info):
    return config.PIWIK_ID


@site_settings_private.field("fontHeading")
def resolve_font(obj, info):
    return config.FONT


@site_settings_private.field("fontBody")
def resolve_font_body(obj, info):
    return config.FONT_BODY or config.FONT


@site_settings_private.field("colorPrimary")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_color_primary(obj, info):
    return config.COLOR_PRIMARY


@site_settings_private.field("colorSecondary")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_color_secondary(obj, info):
    return config.COLOR_SECONDARY


@site_settings_private.field("colorHeader")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_color_header(obj, info):
    return config.COLOR_HEADER if config.COLOR_HEADER else config.COLOR_PRIMARY


@site_settings_public.field("theme")
@site_settings_private.field("theme")
def resolve_theme(obj, info):
    return config.THEME


@site_settings_private.field("themeOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_theme_options(obj, info):
    return config.THEME_OPTIONS


@site_settings_private.field("fontOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_font_options(obj, info):
    return [
        {"value": "Arial", "label": "Arial"},
        {"value": "General Sans", "label": "General Sans"},
        {"value": "Montserrat", "label": "Montserrat"},
        {"value": "Open Sans", "label": "Open Sans"},
        {"value": "Palanquin", "label": "Palanquin"},
        {"value": "PT Sans", "label": "PT Sans"},
        {"value": "Rijksoverheid Sans", "label": "Rijksoverheid Sans"},
        {"value": "Roboto", "label": "Roboto"},
        {"value": "Source Sans Pro", "label": "Source Sans Pro"},
        {"value": "Source Serif Pro", "label": "Source Serif Pro"},
    ]


@site_settings_public.field("logo")
@site_settings_private.field("logo")
def resolve_logo(obj, info):
    return config.LOGO


@site_settings_public.field("logoAlt")
@site_settings_private.field("logoAlt")
def resolve_logo_alt(obj, info):
    return config.LOGO_ALT


@site_settings_private.field("favicon")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_favicon(obj, info):
    return config.FAVICON


@site_settings_private.field("likeIcon")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_like_icon(obj, info):
    return config.LIKE_ICON


@site_settings_private.field("startPageOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_start_page_options(obj, info):
    return [
        {"value": "activity", "label": gettext_lazy("Activity stream")},
        {"value": "cms", "label": gettext_lazy("Page")},
    ]


@site_settings_public.field("startpage")
@site_settings_private.field("startPage")
def resolve_start_page(obj, info):
    return config.STARTPAGE


@site_settings_private.field("startPageCmsOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_start_page_cms_options(obj, info):
    start_page_cms_options = []
    for page in Page.objects.all().order_by("title"):
        start_page_cms_options.append({"value": page.guid, "label": page.title})

    return start_page_cms_options


@site_settings_private.field("startPageCms")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_start_page_cms(obj, info):
    return config.STARTPAGE_CMS


@site_settings_private.field("anonymousStartPage")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_anonymous_start_page(obj, info):
    return config.ANONYMOUS_START_PAGE


@site_settings_private.field("anonymousStartPageCms")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_anonymous_start_page_cms(obj, info):
    return config.ANONYMOUS_START_PAGE_CMS


@site_settings_public.field("icon")
@site_settings_private.field("icon")
def resolve_icon(obj, info):
    return config.ICON if config.ICON else static("icon.svg")


@site_settings_public.field("iconAlt")
@site_settings_private.field("iconAlt")
def resolve_icon_alt(obj, info):
    return config.ICON_ALT


@site_settings_public.field("showIcon")
@site_settings_private.field("showIcon")
def resolve_show_icon(obj, info):
    return config.ICON_ENABLED


class MenuFilter:
    def __init__(self, user):
        self.user = user

    def has_access(self, access):
        if not self.user:
            return True
        if access == "publicOnly":
            return not self.user.is_authenticated
        elif access == "loggedIn":
            return self.user.is_authenticated
        elif access == "administrator":
            return self.user.is_authenticated and self.user.is_site_admin
        # Assume public
        return True

    def clean_menu(self, menu_items):
        for menu_item in menu_items or []:
            if not self.has_access(
                menu_item.get("access") or shared.default_menu_access()
            ):
                continue
            result = {}
            for key, value in menu_item.items():
                if key == "children" and value:
                    result["children"] = [*self.clean_menu(value)]
                else:
                    result[key] = value
            result["access"] = result.get("access") or shared.default_menu_access()

            yield result


@site_settings_public.field("menu")
def resolve_menu_public(obj, info):
    menu_filter = MenuFilter(info.context["request"].user)
    return [*menu_filter.clean_menu(config.MENU)]


@site_settings_private.field("menu")
def resolve_menu(obj, info):
    menu_filter = MenuFilter(None)
    return [*menu_filter.clean_menu(config.MENU)]


@site_settings_public.field("menuState")
@site_settings_private.field("menuState")
def resolve_menu_state(obj, info):
    return config.MENU_STATE


@site_settings_private.field("numberOfFeaturedItems")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_number_of_featured_items(obj, info):
    return config.NUMBER_OF_FEATURED_ITEMS


@site_settings_private.field("enableFeedSorting")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_enable_feed_sorting(obj, info):
    return config.ENABLE_FEED_SORTING


@site_settings_public.field("showExtraHomepageFilters")
@site_settings_private.field("showExtraHomepageFilters")
def resolve_show_extra_homepage_filters(obj, info):
    return config.ACTIVITY_FEED_FILTERS_ENABLED


@site_settings_public.field("showLeader")
@site_settings_private.field("showLeader")
def resolve_show_leader(obj, info):
    return config.LEADER_ENABLED


@site_settings_public.field("showLeaderButtons")
@site_settings_private.field("showLeaderButtons")
def resolve_show_leader_buttons(obj, info):
    return config.LEADER_BUTTONS_ENABLED


@site_settings_public.field("subtitle")
@site_settings_private.field("subtitle")
def resolve_subtitle(obj, info):
    return config.SUBTITLE


@site_settings_public.field("leaderImage")
@site_settings_private.field("leaderImage")
def resolve_leader_image(obj, info):
    return config.LEADER_IMAGE


@site_settings_public.field("showInitiative")
@site_settings_private.field("showInitiative")
def resolve_show_initiative(obj, info):
    return config.INITIATIVE_ENABLED


@site_settings_public.field("initiativeTitle")
@site_settings_private.field("initiativeTitle")
def resolve_initiative_title(obj, info):
    return config.INITIATIVE_TITLE


@site_settings_public.field("initiativeDescription")
@site_settings_private.field("initiativeDescription")
def resolve_initiative_description(obj, info):
    return config.INITIATIVE_DESCRIPTION


@site_settings_public.field("initiativeImage")
@site_settings_private.field("initiativeImage")
def resolve_initiative_image(obj, info):
    return config.INITIATIVE_IMAGE


@site_settings_public.field("initiativeImageAlt")
@site_settings_private.field("initiativeImageAlt")
def resolve_initiative_image_alt(obj, info):
    return config.INITIATIVE_IMAGE_ALT


@site_settings_private.field("initiativeLink")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_initiative_link(obj, info):
    return config.INITIATIVE_LINK


@site_settings_public.field("directLinks")
@site_settings_private.field("directLinks")
def resolve_direct_links(obj, info):
    return config.DIRECT_LINKS


@site_settings_public.field("footer")
@site_settings_private.field("footer")
def resolve_footer(obj, info):
    return config.FOOTER


def _resolve_footer_page():
    try:
        if not config.FOOTER_PAGE:
            owner = User.objects.last_active_site_admin()
            assert owner, "No active site admin found"
            page = Page.objects.create(
                owner=owner,
                page_type="campagne",
                system_setting=True,
                read_access=[constances.ACCESS_TYPE.public],
            )
            config.FOOTER_PAGE = page.guid
            return page
        return Page.objects.get(id=config.FOOTER_PAGE)
    except Page.DoesNotExist:
        config.FOOTER_PAGE = None
        return None
    except AssertionError:
        return None


@site_settings_private.field("footerPage")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_footer_page(obj, info):
    return _resolve_footer_page()


@site_settings_private.field("footerPageEnabled")
@site_settings_public.field("footerPageEnabled")
def resolve_footer_page_enabled(obj, info):
    return config.FOOTER_PAGE_ENABLED


@site_settings_public.field("footerRows")
def resolve_footer_rows(obj, info):
    if page := _resolve_footer_page():
        return [RowSerializer(r) for r in page.row_repository]
    return []


@site_settings_private.field("redirects")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_redirects(obj, info):
    return [{"source": k, "destination": v} for k, v in config.REDIRECTS.items()]


@site_settings_public.field("profile")
@site_settings_private.field("profile")
def resolve_profile(obj, info):
    profile_fields = []
    for field in config.PROFILE:
        try:
            profile_fields.append(ProfileField.objects.get(key=field["key"]))
        except Exception:
            continue
    return profile_fields


@site_settings_public.field("profileSections")
@site_settings_private.field("profileSections")
def resolve_profile_sections(obj, info):
    return config.PROFILE_SECTIONS


@site_settings_public.field("profileFields")
def resolve_profilefields_public(obj, info):
    profile_section_guids = []

    for section in config.PROFILE_SECTIONS:
        profile_section_guids.extend(section["profileFieldGuids"])

    return ProfileField.objects.filter(id__in=profile_section_guids)


@site_settings_public.field("autoMembershipProfileFields")
@decorators.resolver_without_object_info
def resolve_auto_membership_profile_fields():
    return ProfileField.objects.filter(is_in_auto_group_membership=True)


@site_settings_private.field("profileFields")
@shared.PermissionRequired([HasUserManagementPermission])
def resolve_profilefields(obj, info):
    return ProfileField.objects.all()


@site_settings_private.field("profileFieldValidators")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_profile_field_validators(obj, info):
    return ProfileFieldValidator.objects.all()


@site_settings_public.field("tagCategories")
def resolve_tag_categories(obj, info):
    from core.utils.tags import TagCategoryStorage

    storage = TagCategoryStorage()
    return storage.all_tag_categories()


@site_settings_private.field("tagCategorySettings")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_tag_category_settings(obj, info):
    from core.utils.tags import TagCategoryStorage

    storage = TagCategoryStorage()
    return storage.all_settings()


@site_settings_public.field("showTagsInFeed")
@site_settings_private.field("showTagsInFeed")
def resolve_show_tags_in_feed(obj, info):
    return config.SHOW_TAGS_IN_FEED


@site_settings_public.field("showTagsInDetail")
@site_settings_private.field("showTagsInDetail")
def resolve_show_tags_in_detail(obj, info):
    return config.SHOW_TAGS_IN_DETAIL


@site_settings_public.field("showCustomTagsInFeed")
@site_settings_private.field("showCustomTagsInFeed")
def resolve_show_custom_tags_in_feed(obj, info):
    return config.SHOW_CUSTOM_TAGS_IN_FEED


@site_settings_public.field("showCustomTagsInDetail")
@site_settings_private.field("showCustomTagsInDetail")
def resolve_show_custom_tags_in_detail(obj, info):
    return config.SHOW_CUSTOM_TAGS_IN_DETAIL


@site_settings_private.field("defaultEmailOverviewFrequencyOptions")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_default_email_overview_frequency_options(obj, info):
    return [
        {"value": "daily", "label": gettext_lazy("Daily")},
        {"value": "weekly", "label": gettext_lazy("Weekly")},
        {"value": "monthly", "label": gettext_lazy("Monthly")},
        {"value": "never", "label": gettext_lazy("Never")},
    ]


@site_settings_private.field("defaultEmailOverviewFrequency")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_default_email_overview_frequency(obj, info):
    return config.EMAIL_OVERVIEW_DEFAULT_FREQUENCY


@site_settings_private.field("emailOverviewSubject")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_email_overview_subject(obj, info):
    return config.EMAIL_OVERVIEW_SUBJECT


@site_settings_private.field("emailOverviewTitle")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_email_overview_title(obj, info):
    return config.EMAIL_OVERVIEW_TITLE


@site_settings_private.field("emailOverviewIntro")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_email_overview_intro(obj, info):
    return config.EMAIL_OVERVIEW_INTRO


@site_settings_private.field("emailOverviewEnableFeatured")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_email_overview_enable_featured(obj, info):
    return config.EMAIL_OVERVIEW_ENABLE_FEATURED


@site_settings_private.field("emailOverviewFeaturedTitle")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_email_overview_featured_title(obj, info):
    return config.EMAIL_OVERVIEW_FEATURED_TITLE


@site_settings_private.field("emailNotificationShowExcerpt")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_email_notification_show_excerpt(obj, info):
    return config.EMAIL_NOTIFICATION_SHOW_EXCERPT


@site_settings_private.field("exportableUserFields")
@shared.PermissionRequired([HasUserManagementPermission])
def resolve_exportable_user_fields(obj, info):
    return get_exportable_user_fields()


@site_settings_private.field("exportableContentTypes")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_exportable_content_types(obj, info):
    return get_exportable_content_types()


@site_settings_private.field("showLoginRegister")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_show_login_register(obj, info):
    return config.SHOW_LOGIN_REGISTER


@site_settings_public.field("customTagsAllowed")
@site_settings_private.field("customTagsAllowed")
def resolve_custom_tags_allowed(obj, info):
    return config.CUSTOM_TAGS_ENABLED


@site_settings_private.field("showUpDownVoting")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_show_up_down_voting(obj, info):
    return config.SHOW_UP_DOWN_VOTING


@site_settings_private.field("enableSharing")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_enable_sharing(obj, info):
    return config.ENABLE_SHARING


@site_settings_private.field("showViewsCount")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_show_views_count(obj, info):
    return config.SHOW_VIEW_COUNT


@site_settings_private.field("newsletter")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_newsletter(obj, info):
    return config.NEWSLETTER


@site_settings_public.field("cancelMembershipEnabled")
@site_settings_private.field("cancelMembershipEnabled")
def resolve_cancel_membership_enabled(obj, info):
    return config.CANCEL_MEMBERSHIP_ENABLED


@site_settings_private.field("showExcerptInNewsCard")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_show_excerpt_in_news_card(obj, info):
    return config.SHOW_EXCERPT_IN_NEWS_CARD


@site_settings_private.field("commentsOnNews")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_comments_on_news(obj, info):
    return config.COMMENT_ON_NEWS


@site_settings_private.field("eventExport")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_event_export(obj, info):
    return config.EVENT_EXPORT


@site_settings_private.field("eventTiles")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_event_tiles(obj, info):
    return config.EVENT_TILES


@site_settings_private.field("questionerCanChooseBestAnswer")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_questioner_can_choose_best_answer(obj, info):
    return config.QUESTIONER_CAN_CHOOSE_BEST_ANSWER


@site_settings_private.field("statusUpdateGroups")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_status_update_groups(obj, info):
    return config.STATUS_UPDATE_GROUPS


@site_settings_private.field("subgroups")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_subgroups(obj, info):
    return config.SUBGROUPS


@site_settings_private.field("groupMemberExport")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_group_member_export(obj, info):
    return config.GROUP_MEMBER_EXPORT


@site_settings_private.field("limitedGroupAdd")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_limited_group_add(obj, info):
    return config.LIMITED_GROUP_ADD


@site_settings_public.field("showSuggestedItems")
@site_settings_private.field("showSuggestedItems")
def resolve_show_suggested_items(obj, info):
    return config.SHOW_SUGGESTED_ITEMS


@site_settings_public.field("readAccessIds")
def resolve_access_ids(obj, info):
    return get_access_ids()


@site_settings_public.field("writeAccessIds")
def resolve_write_access_ids(obj, info):
    return get_access_ids(None, exclude=[2])


@site_settings_public.field("initiatorLink")
def resolve_initiator_link(obj, info):
    return config.INITIATIVE_LINK


@site_settings_public.field("style")
def resolve_style(obj, info):
    return {
        "fontHeading": resolve_font(obj, info),
        "fontBody": resolve_font_body(obj, info),
        "colorPrimary": config.COLOR_PRIMARY,
        "colorSecondary": config.COLOR_SECONDARY,
        "colorHeader": (
            config.COLOR_HEADER if config.COLOR_HEADER else config.COLOR_PRIMARY
        ),
    }


@site_settings_public.field("activityFilter")
def resolve_activity_filter(obj, info):
    return {"contentTypes": _order_by_value(get_activity_filters())}


@site_settings_public.field("entityFilter")
def resolve_entity_filter(obj, info):
    return {"contentTypes": _order_by_value(get_entity_filters())}


@site_settings_public.field("searchFilter")
def resolve_search_filter(obj, info):
    return {
        "contentTypes": _order_by_value(
            _filter_search_excluded_content_types(get_search_filters())
        )
    }


@site_settings_private.field("searchContentTypes")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_search_filter_private(obj, info):
    return {"contentTypes": _order_by_value(get_search_filters())}


def _order_by_value(types):
    return sorted(types, key=lambda x: x["value"])


def _filter_search_excluded_content_types(types):
    return filter(
        lambda x: x["key"] not in config.SEARCH_EXCLUDED_CONTENT_TYPES,
        types,
    )


@site_settings_public.field("usersOnline")
def resolve_users_online(obj, info):
    ten_minutes_ago = timezone.now() - timezone.timedelta(minutes=10)
    return UserProfile.objects.filter(last_online__gte=ten_minutes_ago).count()


@site_settings_public.field("onboardingEnabled")
@site_settings_private.field("onboardingEnabled")
def resolve_onboarding_enabled(obj, info):
    return config.ONBOARDING_ENABLED


@site_settings_private.field("onboardingForceExistingUsers")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_onboarding_force_existing_users(obj, info):
    return config.ONBOARDING_FORCE_EXISTING_USERS


@site_settings_public.field("onboardingIntro")
@site_settings_private.field("onboardingIntro")
def resolve_onboarding_intro(obj, info):
    return config.ONBOARDING_INTRO


@site_settings_private.field("siteInvites")
@shared.PermissionRequired([HasUserManagementPermission])
def resolve_site_invites(obj, info):
    return {"edges": SiteInvitation.objects.all()}


@site_settings_private.field("cookieConsent")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_cookie_consent(obj, info):
    return config.COOKIE_CONSENT


@site_settings_private.field("loginIntro")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_login_intro(obj, info):
    return config.LOGIN_INTRO


@site_settings_private.field("roleOptions")
@shared.PermissionRequired([HasUserManagementPermission])
def resolve_role_options(obj, info):
    return [
        {"value": USER_ROLES.ADMIN, "label": gettext_lazy("Administrator")},
        {"value": USER_ROLES.EDITOR, "label": gettext_lazy("Editor")},
        {
            "value": USER_ROLES.QUESTION_MANAGER,
            "label": gettext_lazy("Question expert"),
        },
        {
            "value": USER_ROLES.NEWS_EDITOR,
            "label": gettext_lazy("News editor"),
        },
        {"value": USER_ROLES.USER_ADMIN, "label": gettext_lazy("User administrator")},
        {
            "value": USER_ROLES.REQUEST_MANAGER,
            "label": gettext_lazy("Access request manager"),
        },
    ]


@site_settings_private.field("siteAccessRequests")
@shared.PermissionRequired([HasAccessRequestManagementPermission])
def resolve_site_access_requests(obj, info, status=None):
    return {"edges": SiteAccessRequest.objects.filter_status(status)}


@site_settings_private.field("deleteAccountRequests")
@shared.PermissionRequired([HasUserManagementPermission])
def resolve_delete_account_requests(obj, info):
    return {"edges": User.objects.filter(is_delete_requested=True)}


@site_settings_private.field("profileSyncEnabled")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_profile_sync_enabled(obj, info):
    return config.PROFILE_SYNC_ENABLED


@site_settings_private.field("profileSyncToken")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_profile_sync_token(obj, info):
    return config.PROFILE_SYNC_TOKEN


@site_settings_private.field("customCss")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_custom_css(obj, info):
    return config.CUSTOM_CSS


@site_settings_private.field("walledGardenByIpEnabled")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_walled_garden_by_ip_enabled(obj, info):
    return config.WALLED_GARDEN_BY_IP_ENABLED


@site_settings_private.field("whitelistedIpRanges")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_whitelisted_ip_ranges(obj, info):
    return config.WHITELISTED_IP_RANGES


@site_settings_private.field("siteMembershipAcceptedSubject")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_site_membership_accepted_subject(obj, info):
    return config.SITE_MEMBERSHIP_ACCEPTED_SUBJECT


@site_settings_private.field("siteMembershipAcceptedIntro")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_site_membership_accepted_intro(obj, info):
    return config.SITE_MEMBERSHIP_ACCEPTED_INTRO


@site_settings_private.field("siteMembershipDeniedSubject")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_site_membership_denied_subject(obj, info):
    return config.SITE_MEMBERSHIP_DENIED_SUBJECT


@site_settings_private.field("siteMembershipDeniedIntro")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_site_membership_denied_intro(obj, info):
    return config.SITE_MEMBERSHIP_DENIED_INTRO


@shared.PermissionRequired([HasFullControlPermission])
@site_settings_private.field("idpId")
def resolve_idp_id(obj, info):
    return config.IDP_ID


@site_settings_private.field("idpName")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_idp_name(obj, info):
    return config.IDP_NAME


@site_settings_private.field("autoApproveSSO")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_auto_approve_sso(obj, info):
    return config.AUTO_APPROVE_SSO


@site_settings_private.field("require2FA")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_require_2fa(obj, info):
    return config.REQUIRE_2FA


# TODO: remove after flow connects to general api
@site_settings_private.field("flowEnabled")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_flow_enabled(obj, info):
    return config.FLOW_ENABLED


@site_settings_private.field("flowSubtypes")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_flow_subtypes(obj, info):
    return config.FLOW_SUBTYPES


@site_settings_private.field("flowAppUrl")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_flow_app_url(obj, info):
    return config.FLOW_APP_URL


@site_settings_private.field("flowToken")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_flow_token(obj, info):
    return config.FLOW_TOKEN


@site_settings_private.field("flowCaseId")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_flow_case_id(obj, info):
    return config.FLOW_CASE_ID


@site_settings_private.field("flowUserGuid")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_flow_user_guid(obj, info):
    return config.FLOW_USER_GUID


@site_settings_public.field("editUserNameEnabled")
@site_settings_private.field("editUserNameEnabled")
def resolve_edit_user_name_enabled(obj, info):
    return config.EDIT_USER_NAME_ENABLED


@site_settings_public.field("commentWithoutAccountEnabled")
@site_settings_private.field("commentWithoutAccountEnabled")
def resolve_comment_without_account_enabled(obj, info):
    return config.COMMENT_WITHOUT_ACCOUNT_ENABLED


@site_settings_public.field("questionLockAfterActivity")
@site_settings_private.field("questionLockAfterActivity")
def resolve_question_lock_after_activity(obj, info):
    return config.QUESTION_LOCK_AFTER_ACTIVITY


@site_settings_public.field("questionLockAfterActivityLink")
@site_settings_private.field("questionLockAfterActivityLink")
def resolve_question_lock_after_activity_link(obj, info):
    return config.QUESTION_LOCK_AFTER_ACTIVITY_LINK


@site_settings_private.field("kalturaVideoEnabled")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_kaltura_video_enabled(obj, info):
    return config.KALTURA_VIDEO_ENABLED


@site_settings_private.field("kalturaVideoPartnerId")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_kaltura_video_partner_id(obj, info):
    return config.KALTURA_VIDEO_PARTNER_ID


@site_settings_private.field("kalturaVideoPlayerId")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_kaltura_video_player_id(obj, info):
    return config.KALTURA_VIDEO_PLAYER_ID


@site_settings_public.field("fileDescriptionFieldEnabled")
@site_settings_private.field("fileDescriptionFieldEnabled")
def resolve_file_description_field_enabled(obj, info):
    return "enable_file_description" in config.FILE_OPTIONS


@site_settings_public.field("pdfCheckerEnabled")
@site_settings_private.field("pdfCheckerEnabled")
def resolve_pdf_checker_enabled(obj, info):
    return config.PDF_CHECKER_ENABLED


@site_settings_private.field("securityText")
def resolve_security_text(obj, info):
    return config.SECURITY_TEXT


@site_settings_private.field("securityTextEnabled")
def resolve_security_text_enabled(obj, info):
    return config.SECURITY_TEXT_ENABLED


@site_settings_private.field("securityTextPGP")
def resolve_security_text_pgp(obj, info):
    return config.SECURITY_TEXT_PGP


@site_settings_private.field("securityTextRedirectEnabled")
def resolve_security_text_redirect_enabled(obj, info):
    return config.SECURITY_TEXT_REDIRECT_ENABLED


@site_settings_private.field("securityTextRedirectUrl")
def resolve_security_text_redirect_url(obj, info):
    return config.SECURITY_TEXT_REDIRECT_URL


@site_settings_public.field("maxCharactersInAbstract")
@site_settings_private.field("maxCharactersInAbstract")
def resolve_max_characters_in_abstract(obj, info):
    return config.MAX_CHARACTERS_IN_ABSTRACT


@site_settings_public.field("collabEditingEnabled")
@site_settings_private.field("collabEditingEnabled")
def resolve_collab_editing_enabled(obj, info):
    return config.COLLAB_EDITING_ENABLED


@site_settings_public.field("preserveFileExif")
@site_settings_private.field("preserveFileExif")
def resolve_preserve_file_exif(obj, info):
    return config.PRESERVE_FILE_EXIF


@site_settings_public.field("scheduleAppointmentEnabled")
def resolve_schedule_appointment_enabled(obj, info):
    return config.ONLINEAFSPRAKEN_ENABLED


@site_settings_public.field("videocallEnabled")
def resolve_schedule_videocall_enabled(obj, info):
    return config.VIDEOCALL_ENABLED


@site_settings_public.field("videocallProfilepage")
def resolve_videocall_profilepage(*args):
    return config.VIDEOCALL_PROFILEPAGE


@site_settings_public.field("sitePlanName")
@site_settings_private.field("sitePlanName")
def resolve_site_plan_name(obj, info):
    try:
        return constances.SitePlanChoices[config.SITE_PLAN].value
    except KeyError:
        return ""


@site_settings_public.field("sitePlanType")
@site_settings_private.field("sitePlanType")
def resolve_site_plan_type(obj, info):
    if config.SITE_PLAN:
        return str(config.SITE_PLAN)
    return ""


@site_settings_private.field("siteCategory")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_site_category(obj, info):
    try:
        return constances.SiteCategoryChoices[config.SITE_CATEGORY].value
    except KeyError:
        return ""


@site_settings_private.field("siteNotes")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_site_notes(obj, info):
    return config.SITE_NOTES


@site_settings_private.field("supportContractEnabled")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_support_contract_enabled(obj, info):
    return config.SUPPORT_CONTRACT_ENABLED


@site_settings_private.field("supportContractHoursRemaining")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_support_contract_hours_remaining(obj, info):
    return config.SUPPORT_CONTRACT_HOURS_REMAINING


@site_settings_private.field("appointmentTypeVideocall")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_appointment_type_videocall(obj, info):
    try:
        shared.assert_meetings_enabled()
        return shared.resolve_load_appointment_types()
    except GraphQLError:
        pass


@site_settings_private.field("profileSets")
@shared.PermissionRequired([HasUserManagementPermission])
def resolve_profile_sets(obj, info):
    return ProfileSet.objects.all()


@site_settings_public.field("searchPublishedFilterEnabled")
@site_settings_private.field("searchPublishedFilterEnabled")
def resolve_search_published_filter_enabled(obj, info):
    return config.SEARCH_PUBLISHED_FILTER_ENABLED


@site_settings_public.field("searchArchiveOption")
@site_settings_private.field("searchArchiveOption")
def resolve_search_archive_option(obj, info):
    return config.SEARCH_ARCHIVE_OPTION


@site_settings_private.field("searchExcludedContentTypes")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_search_excluded_content_types(obj, info):
    return config.SEARCH_EXCLUDED_CONTENT_TYPES


@site_settings_public.field("recommendedType")
@site_settings_private.field("recommendedType")
def resolve_recommended_type(obj, info):
    return config.RECOMMENDED_TYPE


@site_settings_public.field("blockedUserIntroMessage")
@site_settings_private.field("blockedUserIntroMessage")
def resolve_blocked_user_intro_message(obj, info):
    return config.BLOCKED_USER_INTRO_MESSAGE


@site_settings_public.field("pushNotificationsEnabled")
@site_settings_private.field("pushNotificationsEnabled")
def resolve_push_notifications_enabled(obj, info):
    return config.PUSH_NOTIFICATIONS_ENABLED


@site_settings_public.field("datahubExternalContentEnabled")
def resolve_datahub_external_content_enabled(obj, info):
    return config.DATAHUB_EXTERNAL_CONTENT_ENABLED


@site_settings_public.field("recurringEventsEnabled")
def resolve_recurring_events_enabled(obj, info):
    return config.RECURRING_EVENTS_ENABLED


@site_settings_private.field("pageTagFilters")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_page_tag_filters(obj, info):
    return get_page_tag_filters()


@site_settings_public.field("pageTagFilters")
def resolve_page_tag_filters_subtype(obj, info, contentType):
    for page_tag_filter in get_page_tag_filters():
        if page_tag_filter["contentType"] == contentType:
            return page_tag_filter
    return {"contentType": contentType, "showTagFilter": True, "showTagCategories": []}


@site_settings_public.field("chatEnabled")
def resolve_chat_enabled(obj, info):
    return config.CHAT_ENABLED


@site_settings_public.field("integratedVideocallEnabled")
def resolve_integrated_videocall_enabled(obj, info):
    return config.INTEGRATED_VIDEOCALL_ENABLED


@site_settings_public.field("integratedVideocallUrl")
def resolve_integrated_video_call_url(obj, info):
    return settings.INTEGRATED_VIDEO_CALL_URL


@site_settings_public.field("integratedTokenOnlyVideocallUrl")
def resolve_integrated_token_only_video_call_url(obj, info):
    return settings.INTEGRATED_TOKEN_ONLY_VIDEO_CALL_URL


@site_settings_public.field("searchRelatedSitesEnabled")
def resolve_search_related_sites_enabled(obj, info):
    if len(config.SEARCH_RELATED_SCHEMAS) > 0:
        return True
    return False


@site_settings_public.field("searchRelatedSites")
def resolve_search_related_sites(obj, info):
    sites = []
    for schema_name in config.SEARCH_RELATED_SCHEMAS:
        try:
            with schema_context(schema_name):
                related_site = {}
                related_site["name"] = config.NAME
                related_site["url"] = get_base_url()
                sites.append(related_site)
        except Exception:
            pass
    return sites


@site_settings_private.field("hideContentOwner")
@shared.PermissionRequired([HasFullControlPermission])
def resolve_hide_content_owner(obj, info):
    return config.HIDE_CONTENT_OWNER


@site_settings_private.field("hideAccessLevelSelect")
@decorators.resolver_without_object_info
def resolve_hide_access_level_select():
    return config.HIDE_ACCESS_LEVEL_SELECT


@site_settings_private.field("siteOwnerName")
@shared.PermissionRequired([HasFullControlPermission])
@decorators.resolver_without_object_info
def resolve_site_owner_name():
    tenant = core.lib.tenant_instance()
    return tenant.administrative_details.get("site_owner")


@site_settings_private.field("siteOwnerEmail")
@shared.PermissionRequired([HasFullControlPermission])
@decorators.resolver_without_object_info
def resolve_site_owner_email():
    tenant = core.lib.tenant_instance()
    return tenant.administrative_details.get("site_owner_email")


@site_settings_public.field("showOriginalFileName")
@site_settings_private.field("showOriginalFileName")
@decorators.resolver_without_object_info
def resolve_show_original_file_name():
    return config.SHOW_ORIGINAL_FILE_NAME


@site_settings_private.field("openForCreateContentTypes")
@shared.PermissionRequired([HasFullControlPermission])
@decorators.resolver_without_object_info
def resolve_open_for_create_content_types():
    return config.OPEN_FOR_CREATE_CONTENT_TYPES


@site_settings_public.field("contentModerationEnabled")
@site_settings_private.field("contentModerationEnabled")
@decorators.resolver_without_object_info
def resolve_content_moderation_enabled():
    return config.CONTENT_MODERATION_ENABLED


@site_settings_public.field("requireContentModerationFor")
@site_settings_private.field("requireContentModerationFor")
@decorators.resolver_without_object_info
def resolve_require_content_moderation_for():
    return config.REQUIRE_CONTENT_MODERATION_FOR or []


@site_settings_public.field("commentModerationEnabled")
@site_settings_private.field("commentModerationEnabled")
@decorators.resolver_without_object_info
def resolve_comment_moderation_enabled():
    return config.COMMENT_MODERATION_ENABLED


@site_settings_public.field("requireCommentModerationFor")
@site_settings_private.field("requireCommentModerationFor")
@decorators.resolver_without_object_info
def resolve_require_comment_moderation_for():
    return config.REQUIRE_COMMENT_MODERATION_FOR or []
