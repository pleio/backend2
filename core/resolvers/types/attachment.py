import logging

from ariadne import ObjectType

logger = logging.getLogger(__name__)

attachment = ObjectType("Attachment")


@attachment.field("id")
def resolve_id(obj, info):
    return obj.guid


@attachment.field("url")
def resolve_url(obj, info):
    return obj.attachment_url


@attachment.field("downloadUrl")
def resolve_download_url(obj, info):
    return obj.download_url


@attachment.field("mimeType")
def resolve_mime_type(obj, info):
    return obj.mime_type


@attachment.field("name")
def resolve_name(obj, info):
    return obj.title
