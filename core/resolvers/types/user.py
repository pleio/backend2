from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core import config
from core.constances import ACCESS_TYPE, COULD_NOT_FIND
from core.models import (
    Group,
    GroupProfileFieldSetting,
    ProfileField,
    UserProfile,
    UserProfileField,
)
from core.resolvers import decorators, shared
from user.admin_permissions import HasUserManagementPermission

user = ObjectType("User")


def is_user_logged_in(obj, info):
    return info.context["request"].user.is_authenticated


def is_user_or_admin(obj, info):
    request_user = info.context["request"].user

    if request_user.is_authenticated and (
        request_user == obj or request_user.is_site_admin
    ):
        return True
    return False


@user.field("guid")
@decorators.resolve_obj
def resolve_guid(obj):
    return obj.guid


@user.field("url")
def resolve_url(obj, info):
    return obj.url


@user.field("profile")
def resolve_profile(obj, info):
    user_profile_fields = []

    # only get configured profile fields
    profile_section_guids = []

    for section in config.PROFILE_SECTIONS:
        profile_section_guids.extend(section["profileFieldGuids"])

    for guid in profile_section_guids:
        field = ProfileField.objects.get(id=guid)
        user_profile_fields.append(
            obj.profile.profile_field_value(field, info.context["request"].user)
        )

    return user_profile_fields


@user.field("stats")
def resolve_stats(obj, info):
    return []


@user.field("groupNotifications")
def resolve_group_notifications(obj, info):
    if is_user_or_admin(obj, info):
        groups = []
        for membership in obj.memberships.filter(type="member"):
            groups.append(
                {
                    "guid": membership.group.guid,
                    "name": membership.group.name,
                    "isNotificationsEnabled": membership.is_notifications_enabled,
                    "isNotificationDirectMailEnabled": membership.is_notification_direct_mail_enabled,
                    "isNotificationPushEnabled": membership.is_notification_push_enabled,
                }
            )
        return groups
    return []


@user.field("emailNotifications")
def resolve_email_notifications(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.receive_notification_email
    return None


@user.field("isMentionNotificationsEnabled")
def resolve_is_mention_notifications_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_mention_notifications_enabled
    return None


@user.field("isMentionNotificationPushEnabled")
def resolve_is_mention_notification_push_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_mention_notification_push_enabled
    return None


@user.field("isMentionNotificationDirectMailEnabled")
def resolve_is_mention_notification_direct_mail_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_mention_notification_direct_mail_enabled
    return None


@user.field("isCommentNotificationsEnabled")
def resolve_is_comment_notifications_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_comment_notifications_enabled
    return None


@user.field("isCommentNotificationPushEnabled")
def resolve_is_comment_notification_push_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_comment_notification_push_enabled
    return None


@user.field("isCommentNotificationDirectMailEnabled")
def resolve_is_comment_notification_direct_mail_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_comment_notification_direct_mail_enabled
    return None


@user.field("isChatGroupNotificationPushEnabled")
def resolve_is_chat_group_notification_push_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_chat_group_notification_push_enabled
    return None


@user.field("isChatUsersNotificationPushEnabled")
def resolve_is_chat_users_notification_push_enabled(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_chat_users_notification_push_enabled
    return None


@user.field("isChatOnline")
def resolve_is_chat_online(obj, info):
    if is_user_logged_in(obj, info):
        return not obj.profile.is_chat_online_override and obj.profile.is_chat_online
    return None


@user.field("isChatOnlineOverride")
def resolve_is_chat_online_override(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.is_chat_online_override
    return None


@user.field("emailNotificationsFrequency")
def resolve_email_notifications_frequency(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.notification_email_interval_hours
    return None


@user.field("emailOverview")
def resolve_email_overview(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile
    return None


@user.field("getsNewsletter")
def resolve_gets_newsletter(obj, info):
    if is_user_or_admin(obj, info):
        return obj.profile.receive_newsletter
    return None


@user.field("icon")
@decorators.resolve_in_tenant_context
def resolve_icon(obj, info):
    return obj.icon


@user.field("canEdit")
def resolve_can_edit(obj, info):
    if is_user_or_admin(obj, info):
        return True
    return False


@user.field("roles")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_roles(obj, info):
    return obj.roles


@user.field("isSuperadmin")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_is_superadmin(obj, info):
    return obj.is_superadmin


@user.field("username")
def resolve_username(obj, info):
    return obj.guid


@user.field("requestDelete")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_request_delete(obj, info):
    return obj.is_delete_requested


@user.field("language")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_language(obj, info):
    return obj.get_language()


@user.field("fieldsInOverview")
def resolve_fields_in_overview(obj, info, groupGuid=None):
    if groupGuid:
        try:
            group = Group.objects.get(id=groupGuid)
        except Group.DoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)
    else:
        group = None

    user_profile_fields = []

    # only get configured profile fields
    profile_section_guids = []

    for section in config.PROFILE_SECTIONS:
        profile_section_guids.extend(section["profileFieldGuids"])

    if group:
        for setting in group.profile_field_settings.filter(show_field=True):
            if setting.profile_field.guid in profile_section_guids:
                try:
                    qs = UserProfileField.objects.visible(info.context["request"].user)
                    setting.profile_field.value = qs.get(
                        profile_field=setting.profile_field, user_profile=obj.profile
                    ).value
                except ObjectDoesNotExist:
                    pass

                user_profile_fields.append(setting.profile_field)
    else:
        for field in ProfileField.objects.filter(
            id__in=profile_section_guids, is_in_overview=True
        ):
            field.value = ""
            field.label = field.name
            try:
                qs = UserProfileField.objects.visible(info.context["request"].user)
                field.value = qs.get(
                    profile_field=field, user_profile=obj.profile
                ).value
            except ObjectDoesNotExist:
                pass

            user_profile_fields.append(field)

    return user_profile_fields


@user.field("email")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_email(obj, info):
    return obj.email


@user.field("lastOnline")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_last_online(obj, info):
    return obj.profile.last_online


@user.field("vcard")
def resolve_vcard(obj, info):
    vcard = []
    # using this query because it's preloaded vcards are used in async context in chat subscriptions
    for field in obj.profile.user_profile_fields.all():
        if (
            field.profile_field.is_on_vcard
            and field.can_read(info.context["request"].user)
            and field.value
        ):
            field.value = field.profile_field.sane_value(field.value)
            vcard.append(field)
    return vcard


@user.field("memberSince")
def resolve_created_at(obj, info):
    return obj.created_at


@user.field("missingProfileFields")
def resolve_missing_profile_fields(obj, info, groupGuid):
    if getattr(info.context["request"].user, "is_superadmin", False):
        return []

    class ModalNotRequiredSignal(Exception):
        pass

    try:
        group = Group.objects.get(id=groupGuid)

        required_profile_fields = [
            setting.profile_field.id
            for setting in GroupProfileFieldSetting.objects.filter(
                group=group, is_required=True
            )
        ]

        if not required_profile_fields:
            raise ModalNotRequiredSignal()

        profile = UserProfile.objects.get(user=obj)
        existing_profile_fields = [
            field.profile_field.id
            for field in UserProfileField.objects.filter(user_profile=profile)
            if not field.is_empty
        ]
        missing_profile_fields = [
            field_id
            for field_id in required_profile_fields
            if field_id not in existing_profile_fields
        ]

        missing_profile_fields_result = []
        for field in ProfileField.objects.filter(id__in=missing_profile_fields):
            field.read_access = []
            try:
                user_profile_field = UserProfileField.objects.visible(
                    info.context["request"].user
                ).get(profile_field=field, user_profile=obj.profile)
                field.read_access = user_profile_field.read_access
            except ObjectDoesNotExist:
                field.read_access = [ACCESS_TYPE.logged_in]
            missing_profile_fields_result.append(field)

        if len(missing_profile_fields) == 0:
            raise ModalNotRequiredSignal()

        return missing_profile_fields_result

    except (ModalNotRequiredSignal, UserProfile.DoesNotExist):
        pass

    return []


@user.field("profileSetManager")
def resolve_user_profile_set_manager(obj, info):
    return obj.profile_sets.all()


@user.field("banReason")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_ban_reason(obj, info):
    return obj.ban_reason


@user.field("bannedSince")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_banned_since(obj, info):
    return obj.banned_at


def is_permanent_user(obj, info):
    """
    See if the obj-user is permanent from the viewpoint of the acting user.
    """
    acting_user = info.context["request"].user

    if obj.guid == acting_user.guid:
        return True

    if getattr(acting_user, "is_staff", False):
        return False

    return obj.is_superadmin


@user.field("canDelete")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_can_delete(obj, info):
    return not is_permanent_user(obj, info)


@user.field("canBan")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_can_ban(obj, info):
    return not is_permanent_user(obj, info)


@user.field("isBanned")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_is_banned(obj, info):
    return not obj.is_active


@user.field("banScheduledAt")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_ban_scheduled_at(obj, info):
    return obj.schedule_ban_at


@user.field("banScheduledReason")
@shared.GuidOrPermissionMatchRequired([HasUserManagementPermission])
def resolve_user_ban_scheduled_reason(obj, info):
    return obj.schedule_ban_reason
