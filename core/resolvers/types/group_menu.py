from ariadne import InterfaceType, ObjectType
from django.core.exceptions import ValidationError

from core.constances import GroupPlugin
from entities.cms.models import Page

group = ObjectType("Group")
group_menu_item = InterfaceType("GroupMenuItem")
group_menu_plugin_item = ObjectType("GroupMenuPluginItem")
group_menu_page_item = ObjectType("GroupMenuPageItem")
group_menu_link_item = ObjectType("GroupMenuLinkItem")
group_submenu_item = ObjectType("GroupSubmenuItem")

resolvers = [
    group,
    group_menu_item,
    group_menu_plugin_item,
    group_menu_page_item,
    group_menu_link_item,
    group_submenu_item,
]


def apply_access_rules(menu, obj, acting_user):
    def valid_item(item):
        if "access" not in item or item["access"] == "public":
            return True
        if item["access"] == "loggedIn":
            return acting_user.is_authenticated
        if item["access"] == "members":
            return obj.is_member(acting_user) or obj.can_write(acting_user)
        return False

    for item in menu:
        if valid_item(item):
            yield {
                **item,
                "submenu": [
                    *apply_access_rules(item.get("submenu", []), obj, acting_user)
                ],
            }


@group.field("menu")
def resolve_group_menu(obj, info):
    acting_user = info.context["request"].user
    return [*apply_access_rules(obj.menu, obj, acting_user)]


@group_menu_item.type_resolver
def group_menu_item_type_resolver(obj, *args):
    if obj["type"] == "plugin":
        return "GroupMenuPluginItem"
    elif obj["type"] == "page":
        return "GroupMenuPageItem"
    elif obj["type"] == "link":
        return "GroupMenuLinkItem"
    elif obj["type"] == "submenu":
        return "GroupSubmenuItem"
    return None


@group_menu_item.field("type")
def resolve_group_menu_item_type(obj, info):
    return obj["type"]


@group_menu_item.field("access")
def resolve_group_menu_item_access(obj, info):
    return obj.get("access", "public")


@group_menu_plugin_item.field("plugin")
def resolve_group_menu_item_plugin(obj, info):
    return obj["id"]


@group_menu_plugin_item.field("label")
def resolve_group_menu_plugin_item_label(obj, info):
    try:
        plugin = GroupPlugin[obj["id"]]
        return plugin.value
    except KeyError:
        return ""


@group_menu_page_item.field("page")
def resolve_group_menu_item_page(obj, info):
    try:
        acting_user = info.context["request"].user
        return Page.objects.visible(acting_user).get(id=obj["id"])
    except (Page.DoesNotExist, ValidationError):
        pass


@group_submenu_item.field("id")
def resolve_submenu_item_id(obj, info):
    return obj["label"]


@group_menu_link_item.field("link")
def resolve_group_menu_link_item_link(obj, info):
    return obj["id"]
