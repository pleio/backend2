from .access_item import resolvers as access_item_resolvers
from .attachment import attachment as _attachment
from .broken_link import broken_link as _broken_link
from .comment import comment as _comment
from .comment_moderation_request import object_type as _comment_moderation_request
from .email_overview import email_overview as _email_overview
from .entity import entity as _entity
from .featured import featured as _featured
from .filters import filters as _filters
from .group import group as _group
from .group_menu import resolvers as group_menu_resolvers
from .invite import invite as _invite
from .member import member as _member
from .menu import resolvers as menu_resolvers
from .notification import notification as _notification
from .notifications_list import notifications_list as _notifications_list
from .profile_field_validator import profile_field_validator as _profile_field_validator
from .profile_item import profile_item as _profile_item
from .publish_request import publish_request as _publish_request
from .related_site_search_result import (
    related_site_search_result as _related_site_search_result,
)
from .revision import resolvers as revision_resolvers
from .rss_item import rss_item as _rss_item
from .rss_mutation import mutation as _rss_mutation
from .search_result import search_result as _search_result
from .site_access_request import site_access_request as _site_access_request
from .site_agreement import site_agreement as _site_agreement
from .site_agreement_version import site_agreement_version as _site_agreement_version
from .site_settings import resolvers as site_settings_resolvers
from .site_stats import site_stats as _site_stats
from .subgroup import subgroup as _subgroup
from .subgroup_list import subgroup_list as _subgroup_list
from .user import user as _user
from .video_call import video_call as _video_call
from .video_metadata import video_metadata as _video_metadata
from .viewer import viewer as _viewer

resolvers = [
    *access_item_resolvers,
    _attachment,
    _broken_link,
    _comment,
    _comment_moderation_request,
    *revision_resolvers,
    _email_overview,
    _entity,
    _featured,
    _filters,
    _group,
    *group_menu_resolvers,
    _invite,
    _member,
    *menu_resolvers,
    _notification,
    _notifications_list,
    _profile_field_validator,
    _profile_item,
    _publish_request,
    _related_site_search_result,
    _rss_item,
    _rss_mutation,
    _search_result,
    _site_access_request,
    _site_agreement,
    _site_agreement_version,
    *site_settings_resolvers,
    _site_stats,
    _subgroup,
    _subgroup_list,
    _user,
    _video_call,
    _video_metadata,
    _viewer,
]
