from ariadne import ObjectType

publish_request = ObjectType("PublishRequest")


@publish_request.field("author")
def resolve_author(obj, info):
    return obj.entity.owner


@publish_request.field("assignedTo")
def resolve_assigned_to(obj, info):
    return obj.assigned_to


@publish_request.field("publishedAt")
def resolve_published_at(obj, info):
    return obj.time_published
