from ariadne import InterfaceType, ObjectType
from django.core.exceptions import ValidationError
from django.db.models import Q

from core.models import Group, Subgroup
from user.models import User

access_item = InterfaceType("AccessItem")
access_item_user = ObjectType("AccessItemUser")
access_item_group = ObjectType("AccessItemGroup")
access_item_subgroup = ObjectType("AccessItemSubGroup")

resolvers = [
    access_item,
    access_item_user,
    access_item_group,
    access_item_subgroup,
]


@access_item.type_resolver
def access_type_resolver(obj, *args):
    if obj["type"] == "user":
        return "AccessItemUser"
    elif obj["type"] == "group":
        return "AccessItemGroup"
    elif obj["type"] == "subGroup":
        return "AccessItemSubGroup"
    return "DefaultAccessItem"


@access_item_user.field("user")
def resolve_access_item_user(obj, info):
    try:
        return User.objects.get(pk=obj["guid"])
    except (User.DoesNotExist, ValidationError):
        pass


@access_item_group.field("group")
def resolve_access_item_group(obj, info):
    try:
        return Group.objects.get(pk=obj["guid"])
    except (Group.DoesNotExist, ValidationError):
        pass


@access_item_subgroup.field("subGroup")
def resolve_access_item_subgroup(obj, info):
    try:
        return Subgroup.objects.get(Q(pk=int(obj["guid"])) | Q(access_id=obj["guid"]))
    except (Subgroup.DoesNotExist, ValidationError):
        pass
