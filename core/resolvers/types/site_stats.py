from ariadne import ObjectType
from elasticsearch_dsl import Search

from core.lib import get_acl, tenant_schema
from core.models import SiteStat

site_stats = ObjectType("SiteStats")


@site_stats.field("dbUsage")
def resolve_site_stats_db_usage(obj, info):
    try:
        user = info.context["request"].user
        assert user.is_authenticated
        assert user.is_site_admin

        return SiteStat.objects.filter(stat_type="DB_SIZE").latest("created_at").value
    except Exception:
        pass
    return 0


@site_stats.field("fileDiskUsage")
def resolve_file_disk_usage(obj, info):
    try:
        user = info.context["request"].user
        assert user.is_authenticated
        assert user.is_site_admin

        return SiteStat.objects.filter(stat_type="DISK_SIZE").latest("created_at").value
    except Exception:
        pass
    return 0


@site_stats.field("archiveSize")
def resolve_archive_size(obj, info):
    user = info.context["request"].user
    query = (
        Search(index="_all")
        .query()
        .filter("terms", read_access=list(get_acl(user)))
        .filter("term", tenant_name=tenant_schema())
        .filter("term", is_archived=True)
        .exclude("term", is_active=False)
    )
    result = query.execute()

    return result.hits.total["value"]
