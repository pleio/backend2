from ariadne import ObjectType
from django.apps import apps
from graphql import GraphQLError

from core import constances
from core.models import RemoteRssEndpoint
from core.resolvers import decorators, shared
from core.utils.entity import load_entity_by_id

mutation = ObjectType("Mutation")


# Even aan denken: Het moet een get_or_create worden.
@mutation.field("addRssEndpoint")
@decorators.resolver_with_request
def resolve_add_rss_endpoint(request, url, groupGuid=None):
    group = (
        load_entity_by_id(groupGuid, ["core.Group"], fail_if_not_found=False)
        if groupGuid
        else None
    )

    shared.assert_authenticated(request.user)
    shared.assert_can_create(request.user, apps.get_model("cms.Page"), group)

    endpoint_instance = RemoteRssEndpoint(url=url)
    if not endpoint_instance.is_valid():
        raise GraphQLError(constances.INVALID_VALUE)

    if not RemoteRssEndpoint.objects.filter(url=url).exists():
        endpoint_instance.save()

    return {"success": True}
