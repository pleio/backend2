from ariadne import ObjectType
from validators import ValidationError

from entities.file.models import FileFolder

featured = ObjectType("Featured")


@featured.field("image")
def resolve_image(obj, info):
    if obj and "image" in obj:
        if isinstance(obj["image"], FileFolder):
            return obj["image"]
        elif isinstance(obj["image"], dict):
            return _image_from_dict(obj["image"])


def _image_from_dict(image):
    try:
        if image and "guid" in image:
            return FileFolder.objects.get(id=image["guid"])
    except (FileFolder.DoesNotExist, ValidationError):
        pass
    return None
