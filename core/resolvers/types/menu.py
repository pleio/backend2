from ariadne import ObjectType

from core import config
from core.resolvers import decorators

menu = ObjectType("MenuItem")

resolvers = [menu]


@menu.field("localLabel")
@decorators.resolve_obj_request
def resolve_local_label(obj, request):
    if not request.user.is_authenticated:
        return

    for translation in obj.get("translations") or []:
        language, translation = translation.get("language"), translation.get("label")
        if language == request.user.profile.language and translation:
            return translation


@menu.field("label")
@decorators.resolve_property
def resolve_label(obj):
    for translation in obj.get("translations") or []:
        language, translation = translation.get("language"), translation.get("label")
        if language == config.LANGUAGE and translation:
            return translation
    return obj.get("label") or ""


@menu.field("translations")
@decorators.resolve_property
def resolve_translations(obj):
    for translation in obj.get("translations") or []:
        if translation["language"] != config.LANGUAGE:
            yield translation
