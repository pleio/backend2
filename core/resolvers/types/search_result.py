from ariadne import UnionType

search_result = UnionType("SearchResult")


@search_result.type_resolver
def resolvere_search_result_type(obj, *_):
    from core.utils.elasticsearch import RelatedSiteSearchResult

    if isinstance(obj, RelatedSiteSearchResult):
        return "RelatedSiteSearchResult"

    if obj._meta.object_name == "FileFolder":
        return obj.type

    return obj._meta.object_name
