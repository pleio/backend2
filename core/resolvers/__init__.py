from .mutations import resolvers as mutation_resolvers
from .queries import resolvers as query_resolvers
from .scalars import resolvers as scalar_resolvers
from .types import resolvers as type_resolvers

resolvers = [
    *type_resolvers,
    *mutation_resolvers,
    *query_resolvers,
    *scalar_resolvers,
]
