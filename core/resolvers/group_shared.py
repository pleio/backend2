import logging

from django.core.exceptions import ValidationError
from graphql import GraphQLError

from chat.models import Chat
from core import constances
from core.constances import INVALID_PROFILE_FIELD_GUID, GroupPlugin
from core.models import GroupProfileFieldSetting, ProfileField
from entities.cms.models import Page

logger = logging.getLogger(__name__)


def update_name(group, clean_input):
    if "name" in clean_input:
        group.name = clean_input.get("name")


def update_is_leaving_group_disabled(group, clean_input):
    if "isLeavingGroupDisabled" in clean_input:
        group.is_leaving_group_disabled = clean_input.get("isLeavingGroupDisabled")


def update_is_auto_membership_enabled(group, clean_input):
    if "isAutoMembershipEnabled" in clean_input:
        if (
            not group.is_auto_membership_enabled
            and clean_input["isAutoMembershipEnabled"]
        ):
            from user.models import User

            for u in User.objects.filter(is_active=True):
                if not group.is_full_member(u):
                    group.join(u, "member")

        group.is_auto_membership_enabled = clean_input["isAutoMembershipEnabled"]


def update_auto_membership_fields(group, clean_input):
    if "autoMembershipFields" in clean_input:
        try:
            # Given profile fields must exist, and the options must be valid.
            options_dict = {}
            for setting in clean_input["autoMembershipFields"]:
                profile_field = ProfileField.objects.get(id=setting.get("guid"))
                options_dict[profile_field.guid] = profile_field.field_options or []
            group.auto_membership_fields = [
                {
                    "guid": f.get("guid"),
                    "value": list(
                        {*(f.get("value") or [])} & {*options_dict.get(f.get("guid"))}
                    ),
                }
                for f in clean_input["autoMembershipFields"] or []
            ]
        except (ProfileField.DoesNotExist, ValidationError, KeyError):
            raise GraphQLError(INVALID_PROFILE_FIELD_GUID)


def update_is_hidden(group, clean_input):
    if "isHidden" in clean_input:
        group.is_hidden = clean_input.get("isHidden")


def update_required_profile_fields(group, clean_input):
    if "requiredProfileFieldGuids" in clean_input:
        for profile_field_id in clean_input.get("requiredProfileFieldGuids"):
            try:
                profile_field = ProfileField.objects.get(id=profile_field_id)
                setting, created = GroupProfileFieldSetting.objects.get_or_create(
                    profile_field=profile_field, group=group
                )
                setting.is_required = True
                setting.save()
            except ProfileField.DoesNotExist:
                raise GraphQLError(INVALID_PROFILE_FIELD_GUID)
            except ValidationError as e:
                raise GraphQLError(", ".join(e.messages))
        # disable other
        group.profile_field_settings.exclude(
            profile_field__id__in=clean_input.get("requiredProfileFieldGuids")
        ).update(is_required=False)


def update_show_member_profile_fields(group, clean_input):
    if "showMemberProfileFieldGuids" in clean_input:
        for profile_field_id in clean_input.get("showMemberProfileFieldGuids"):
            try:
                profile_field = ProfileField.objects.get(id=profile_field_id)
                setting, created = GroupProfileFieldSetting.objects.get_or_create(
                    profile_field=profile_field, group=group
                )
                setting.show_field = True
                setting.save()
            except ProfileField.DoesNotExist:
                raise GraphQLError(INVALID_PROFILE_FIELD_GUID)
            except ValidationError as e:
                raise GraphQLError(", ".join(e.messages))
        # disable other
        group.profile_field_settings.exclude(
            profile_field__id__in=clean_input.get("showMemberProfileFieldGuids")
        ).update(show_field=False)


def update_plugins(group, clean_input):
    if "plugins" in clean_input:
        # Update the plugin list.
        group.plugins = [
            plugin
            for plugin in clean_input.get("plugins")
            if GroupPlugin.exists(plugin)
        ]


def update_menu(group, clean_input):
    def _validate_group_menu_items(group, items):
        for item in items:
            if item["type"] in ["plugin", "page", "link"] and not item.get("id"):
                raise GraphQLError(constances.GROUP_MENU_MISSING_ID)

            if item["type"] in ["submenu", "link"] and not item.get("label"):
                raise GraphQLError(constances.GROUP_MENU_MISSING_LABEL)

            if item["type"] == "plugin" and not GroupPlugin.exists(item.get("id")):
                raise GraphQLError(constances.GROUP_MENU_INVALID_PLUGIN)

            if (
                item["type"] == "page"
                and not Page.objects.filter(group=group, id=item["id"]).exists()
            ):
                raise GraphQLError(constances.GROUP_MENU_INVALID_PAGE)

            if item["type"] == "link" and not item["id"].startswith(
                ("http://", "https://", "/")
            ):
                raise GraphQLError(constances.GROUP_MENU_INVALID_LINK)

            if item["type"] == "submenu":
                _validate_group_menu_items(group, item["submenu"])

    if "menu" in clean_input:
        _validate_group_menu_items(group, clean_input["menu"])
        group.menu = clean_input["menu"]


def update_file_notifications(group, clean_input):
    group.file_notifications = clean_input.get(
        "fileNotification", group.file_notifications
    )


def update_start_page(group, clean_input):
    if "startPageGuid" in clean_input:
        try:
            group._start_page = Page.objects.get(
                id=clean_input["startPageGuid"], group=group
            )
        except (Page.DoesNotExist, ValidationError):
            raise GraphQLError(constances.COULD_NOT_FIND_START_PAGE)


def update_is_chat_enabled(group, clean_input):
    if "isChatEnabled" in clean_input:
        group.is_chat_enabled = clean_input.get("isChatEnabled")
        if group.is_chat_enabled:
            Chat.objects.get_or_create(group=group)


def update_auto_notification(group, clean_input):
    if "autoNotification" in clean_input:
        group.auto_notification = clean_input.get("autoNotification")


def update_is_membership_on_request(group, clean_input):
    if "isMembershipOnRequest" in clean_input:
        group.is_membership_on_request = clean_input.get("isMembershipOnRequest")


def update_is_closed(group, clean_input):
    if "isClosed" in clean_input:
        group.is_closed = clean_input.get("isClosed")


def update_is_introduction_public(group, clean_input):
    if "isIntroductionPublic" in clean_input:
        group.is_introduction_public = clean_input.get("isIntroductionPublic")


def update_welcome_message(group, clean_input):
    if "welcomeMessage" in clean_input:
        group.welcome_message = clean_input.get("welcomeMessage")


def update_required_profile_fields_message(group, clean_input):
    if "requiredProfileFieldsMessage" in clean_input:
        group.required_fields_message = clean_input.get(
            "requiredProfileFieldsMessage", ""
        )


def update_is_join_button_hidden(group, clean_input):
    if "isJoinButtonVisible" in clean_input:
        group.is_join_button_enabled = clean_input.get("isJoinButtonVisible")


def update_is_menu_always_visible(group, clean_input):
    if "isMenuAlwaysVisible" in clean_input:
        group.is_menu_always_visible = clean_input.get("isMenuAlwaysVisible")
