from ariadne import ObjectType
from django.utils import dateparse
from graphql import GraphQLError

from core.constances import INVALID_DATE
from core.resolvers import shared
from user.admin_permissions import HasUserManagementPermission
from user.models import User

query = ObjectType("Query")


@query.field("siteUsers")
def resolve_site_users(
    _,
    info,
    q=None,
    role=None,
    isDeleteRequested=None,
    isBanned=False,
    filterScheduledForBan=None,
    lastOnlineBefore=None,
    memberSince=None,
    groupGuid=None,
    orderBy=None,
    orderDirection=None,
    offset=0,
    limit=20,
):
    user = info.context["request"].user

    shared.assert_authenticated(user)

    if isBanned:
        shared.assert_admin_permission(user, HasUserManagementPermission)

    last_online_before = None
    if lastOnlineBefore:
        try:
            last_online_before = dateparse.parse_datetime(lastOnlineBefore)
        except ValueError:
            raise GraphQLError(INVALID_DATE)

    users = User.objects.get_filtered_users(
        q=q,
        role=role,
        is_delete_requested=isDeleteRequested,
        is_banned=isBanned,
        last_online_before=last_online_before,
        member_since=memberSince,
        include_superadmin=False,
        group_id=groupGuid,
        acting_user=user,
    )

    if filterScheduledForBan == "isScheduled":
        users = users.filter(schedule_ban_at__isnull=False)
    elif filterScheduledForBan == "isNotScheduled":
        users = users.filter(schedule_ban_at__isnull=True)

    users = _ordered_query(users, orderBy, orderDirection)

    edges = users[offset : offset + limit]

    return {"total": users.count(), "edges": edges}


def _ordered_query(qs, orderBy=None, orderDirection=None):  # noqa: C901
    if orderBy:
        order = None
        if orderBy == "memberSince":
            order = "created_at"
        elif orderBy == "name":
            order = "name"
        elif orderBy == "email":
            order = "email"
        elif orderBy == "banReason":
            order = "ban_reason"
        elif orderBy == "bannedSince":
            order = "banned_at"
        elif orderBy == "lastOnline":
            order = "_profile__last_online"
        elif orderBy == "banScheduledAt":
            order = "schedule_ban_at"

        if order:
            if orderDirection.lower() == "desc":
                order = "-%s" % order
            return qs.order_by(order)

    return qs
