from ariadne import ObjectType

query = ObjectType("Query")


@query.field("notifications")
def resolve_notifications(_, info, offset=0, limit=20, unread=None):
    """Returns notifications"""

    return {"offset": offset, "limit": limit, "unread": unread}
