from ariadne import ObjectType

from core.resolvers import shared
from core.resolvers.decorators import resolver_with_request

query = ObjectType("Query")


@query.field("fetchVideoMetadata")
@resolver_with_request
def resolve_fetch_video_metadata(request, input):
    shared.assert_authenticated(request.user)

    return {
        "metadata": {
            "_url": input["url"],
        }
    }
