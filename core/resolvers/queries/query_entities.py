import logging
from copy import deepcopy

from ariadne import ObjectType
from django.db.models import F, Q
from django.db.models.functions import Coalesce, Lower
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_USE_EVENT_FILTER,
    INVALID_SUBTYPE,
    ORDER_BY,
    ORDER_DIRECTION,
)
from core.lib import early_this_morning
from core.models import Entity
from core.resolvers.queries import shared_filters as filters
from core.resolvers.queries.shared_order_by import order_by_entity_column
from entities.event.lib import complement_expected_range

query = ObjectType("Query")

logger = logging.getLogger(__name__)

QUERY_SUBTYPES = [
    filters.MagazineEntityFilter,
    filters.MagazineIssueEntityFilter,
    filters.NewsEntityFilter,
    filters.BlogEntityFilter,
    filters.PodcastEntityFilter,
    filters.EpisodeEntityFilter,
    filters.EventEntityFilter,
    filters.DiscussionEntityFilter,
    filters.StatusupdateEntityFilter,
    filters.QuestionEntityFilter,
    filters.PollEntityFilter,
    filters.WikiEntityFilter,
    filters.PageEntityFilter,
    filters.LandingPagesFilter,
    filters.FileEntityFilter,
    filters.FolderEntityFilter,
    filters.PadEntityFilter,
    filters.TaskEntityFilter,
    filters.ExternalcontentEntityFilter,
]


def conditional_subtypes_filter(subtypes):
    """
    Filter multiple subtypes
    """
    query = Q()

    for entity_filter in QUERY_SUBTYPES:
        entity_filter(subtypes).add_if_applicable(query)

    return query


def conditional_page_parent(parentGuid):
    # Is dit hetzelfde als voor groepen vs alleen content in wat voor groep dan ook?
    if parentGuid == "1":
        return Q(page__parent__isnull=True)
    if parentGuid:
        return Q(page__parent=parentGuid)
    return Q()


def conditional_is_featured_filter(is_featured):
    """
    Only filter is_featured on news list
    """
    if is_featured:
        return Q(is_featured=True)

    return Q()


def conditional_event_filter(date_filter):
    if date_filter == "previous":
        return Q(event__end_date__lt=early_this_morning())
    return Q(event__end_date__gte=early_this_morning())


def conditional_owner_filter(owner_id):
    if owner_id:
        return Q(owner_id=owner_id)
    return Q()


@query.field("entities")
def resolve_entities(  # noqa: C901
    _,
    info,
    offset=0,
    limit=20,
    q="",
    subtype=None,
    subtypes=None,
    eventFilter=None,
    tags=None,
    tagCategories=None,
    matchStrategy="any",
    orderBy=ORDER_BY.timePublished,
    orderDirection=ORDER_DIRECTION.desc,
    isFeatured=None,
    sortPinned=False,
    statusPublished=None,
    userGuid=None,
    containerGuid=None,
    parentGuid=None,
    groupFilter=None,
):
    # merge all in subtypes list
    if not subtypes and subtype:
        subtypes = [subtype]
    elif not subtypes and not subtype:
        subtypes = []

    acting_user = info.context["request"].user

    Model = Entity

    if not Model:
        raise GraphQLError(INVALID_SUBTYPE)

    order_by_processor = order_by_entity_column(orderBy, subtypes)
    order_column = order_by_processor.column()

    if statusPublished and len(statusPublished) > 0:
        entities = Model.objects.status_published(statusPublished, acting_user)
    else:
        entities = Model.objects.visible(acting_user)

    entities = entities.filter(
        conditional_is_featured_filter(isFeatured)
        & filters.conditional_group_guid(containerGuid)
        & filters.conditional_groups_filter(groupFilter, acting_user)
        & conditional_subtypes_filter(subtypes)
        & filters.conditional_tags_filter(tags, matchStrategy == "any")
        & filters.conditional_tag_lists_filter(tagCategories, matchStrategy != "all")
        & conditional_owner_filter(userGuid)
    )

    if subtypes == ["event"] and eventFilter == "upcoming":
        qs2 = deepcopy(entities).annotate(start_date=F("event__start_date"))
        complement_expected_range(qs2, offset, limit)

    if eventFilter:
        if subtypes == ["event"]:
            entities = entities.filter(conditional_event_filter(eventFilter))
        else:
            raise GraphQLError(COULD_NOT_USE_EVENT_FILTER)

    if parentGuid and (subtypes == ["magazine_issue"] or subtype == "magazine_issue"):
        entities = entities.filter(magazineissue__container_id=parentGuid)

    # When page is selected change sorting and only return pages without parent.
    if filters.is_combination_of(subtypes, ["page", "landingpage"]):
        entities = entities.filter(conditional_page_parent(parentGuid))
        entities = entities.annotate(sort_title=Lower("page__title"))
    elif subtype and subtype == "wiki":
        # Only return wiki's without parent.
        entities = entities.filter(wiki__parent=parentGuid)
        entities = entities.annotate(sort_title=Lower("wiki__title"))
    else:
        entities = entities.annotate(
            sort_title=Lower(
                Coalesce(
                    "magazine__title",
                    "magazineissue__title",
                    "news__title",
                    "blog__title",
                    "filefolder__title",
                    "poll__title",
                    "statusupdate__title",
                    "wiki__title",
                    "page__title",
                    "question__title",
                    "discussion__title",
                    "event__title",
                    "externalcontent__title",
                    "task__title",
                    "podcast__title",
                    "episode__title",
                )
            )
        )

    # we use sort_title to search for all entities
    if q:
        entities = entities.filter(Q(sort_title__icontains=q))

    order_by = [order_column]
    if orderDirection == ORDER_DIRECTION.desc:
        order_by = ("-%s" % c for c in order_by)

    if sortPinned:
        order_by = ["-is_pinned", *order_by]

    entities = order_by_processor.extend_query(entities).order_by(*order_by)

    edges = entities.select_subclasses()[offset : offset + limit]

    return {
        "total": entities.count(),
        "edges": edges,
    }
