from ariadne import ObjectType
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from graphql import GraphQLError

from core.constances import INVALID_SUBTYPE, NOT_LOGGED_IN
from core.lib import get_model_by_subtype
from core.models import Annotation
from entities.file.models import FileFolder

query = ObjectType("Query")


def conditional_subtype_filter(subtype):
    if model := get_model_by_subtype(subtype):
        filter_content_type = ContentType.objects.get_for_model(model)
        return Q(content_type=filter_content_type)

    raise GraphQLError(INVALID_SUBTYPE)


@query.field("bookmarks")
def resolve_bookmarks(_, info, offset=0, limit=20, subtype=None):
    user = info.context["request"].user

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    qs = Annotation.objects
    qs = qs.filter(user=user, key="bookmarked")
    if not subtype or subtype == "all":
        qs = qs.exclude(content_type=ContentType.objects.get_for_model(FileFolder))
    else:
        qs = qs.filter(conditional_subtype_filter(subtype))
    total = qs.count()
    qs = qs[offset : offset + limit]

    entities = [item.content_object for item in qs]

    return {
        "total": total,
        "edges": entities,
    }
