from ariadne import ObjectType
from django.apps import apps

from core.models import RemoteRssEndpoint
from core.resolvers import decorators, shared
from core.utils.entity import load_entity_by_id

query = ObjectType("Query")


@query.field("testRssEndpoint")
@decorators.resolver_with_request
def resolve_test_rss_endpoint(request, url, groupGuid=None):
    group = (
        load_entity_by_id(groupGuid, ["core.Group"], fail_if_not_found=False)
        if groupGuid
        else None
    )

    shared.assert_authenticated(request.user)
    shared.assert_can_create(request.user, apps.get_model("cms.Page"), group)

    endpoint_instance = RemoteRssEndpoint(url=url)
    return {"isFeed": endpoint_instance.is_valid()}


@query.field("fetchRssEndpoint")
def resolve_fetch_rss_endpoint(*_, url, limit=5, offset=0):
    try:
        endpoint_instance = RemoteRssEndpoint.objects.get(url=url)
        edges = endpoint_instance.fetch()
        return {
            "total": len(edges),
            "edges": edges[offset:][:limit],
        }

    except RemoteRssEndpoint.DoesNotExist:
        return {
            "total": 0,
            "edges": [],
        }
