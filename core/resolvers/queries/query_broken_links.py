from ariadne import ObjectType

from core.models import BrokenLink
from core.resolvers import shared

query = ObjectType("Query")


@query.field("brokenLinks")
def resolve_broken_links(
    _,
    info,
    offset=0,
    limit=20,
):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    links = BrokenLink.objects.all()

    edges = links[offset : offset + limit]

    return {
        "total": links.count(),
        "edges": edges,
    }
