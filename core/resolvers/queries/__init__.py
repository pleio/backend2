from .query_bookmarks import query as _bookmarks
from .query_breadcrumb import query as _breadcrumb
from .query_broken_links import query as _broken_links
from .query_comment_moderation import query as _comment_moderation
from .query_content_snapshots import query as _content_snapshots
from .query_entities import query as _entities
from .query_entity import query as _entity
from .query_filters import query as _filters
from .query_groups import query as _groups
from .query_meetings import query as _meetings
from .query_members import query as _members
from .query_notifications import query as _notifications
from .query_publish_requests import query as _publish_requests
from .query_recommended import query as _recommended
from .query_revisions import query as _revisions
from .query_rss import query as _rss
from .query_search import query as _search
from .query_site import query as _site
from .query_site_agreements import query as _site_agreements
from .query_site_users import query as _site_users
from .query_tags import query as _tags
from .query_top import query as _top
from .query_trending import query as _trending
from .query_users import query as _users
from .query_users_by_birth_date import query as _users_by_birth_date
from .query_video_call import query as _video_call
from .query_video_metadata import query as _video_metadata
from .query_viewer import query as _viewer

resolvers = [
    _bookmarks,
    _breadcrumb,
    _broken_links,
    _comment_moderation,
    _content_snapshots,
    _entities,
    _entity,
    _filters,
    _groups,
    _meetings,
    _members,
    _notifications,
    _publish_requests,
    _recommended,
    _revisions,
    _rss,
    _search,
    _site,
    _site_agreements,
    _site_users,
    _tags,
    _top,
    _trending,
    _users,
    _users_by_birth_date,
    _video_call,
    _video_metadata,
    _viewer,
]
