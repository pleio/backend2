import os

from ariadne import ObjectType

from core.models.agreement import CustomAgreement
from core.resolvers import shared
from tenants.models import Agreement

query = ObjectType("Query")


@query.field("siteAgreements")
def resolve_site_agreements(_, info):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    return Agreement.objects.all()


@query.field("siteCustomAgreements")
def resolve_site_custom_agreements(_, info):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    agreements = CustomAgreement.objects.all()
    custom_agreements = []
    for agreement in agreements:
        custom_agreements.append(
            {
                "name": agreement.name,
                "fileName": os.path.basename(agreement.document.name),
                "url": agreement.url,
            }
        )
    return custom_agreements
