from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist

from core.models import Entity

query = ObjectType("Query")


@query.field("breadcrumb")
def resolve_breadcrumb(_, info, guid):
    path = []

    entity = None

    try:
        entity = Entity.objects.get_subclass(id=guid)
    except ObjectDoesNotExist:
        pass

    if entity:
        path.append(entity)
        if entity.parent:
            parent = entity.parent
            while parent:
                path.append(parent)
                parent = parent.parent

    path.reverse()

    return path
