from ariadne import ObjectType

from core.lib import get_model_by_subtype

query = ObjectType("Query")


@query.field("recommended")
def resolve_recommended(_, info, offset=0, limit=20):
    Model = get_model_by_subtype("blog")
    entities = Model.objects.visible(info.context["request"].user)
    entities = entities.filter(is_recommended=True)

    edges = entities[offset : offset + limit]

    return {
        "total": entities.count(),
        "edges": edges,
    }
