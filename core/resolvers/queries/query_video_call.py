from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.models.videocall import EntityVideoCall

query = ObjectType("Query")


@query.field("videoCall")
def resolve_video_call(_, info, guid=None):
    try:
        video_call = EntityVideoCall.objects.get(id=guid)
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    return video_call
