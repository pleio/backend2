from ariadne import ObjectType
from django_tenants.utils import parse_tenant_config_path
from elasticsearch_dsl import Q, Search
from graphql import GraphQLError

from core import config, constances
from core.lib import get_acl
from core.models import ProfileField
from core.resolvers import shared
from user.models import User

query = ObjectType("Query")


@query.field("users")
def resolve_users(  # noqa: C901
    _,
    info,
    q="",
    profileSetGuid=None,
    groupGuid=None,
    filters=None,
    offset=0,
    limit=20,
):
    user = info.context["request"].user
    tenant_name = parse_tenant_config_path("")

    shared.assert_authenticated(user)
    if profileSetGuid:
        shared.assert_is_profile_set_manager(user, profileSetGuid)

    apply_acl = not (user.is_authenticated and user.is_site_admin)

    query_filter = Q("term", tenant_name=tenant_name) & Q("term", is_active=True)
    if apply_acl:
        query_filter = query_filter & Q("terms", read_access=list(get_acl(user)))

    if q:
        profile_filter = Q("match", user_profile_fields__value=q)
        acl_filter = Q(
            "bool",
            should=[
                Q("match", user_profile_fields__read_access=acl_token)
                for acl_token in get_acl(user)
            ],
        )

        query_filter = query_filter & (
            Q(
                "simple_query_string",
                query=q,
                fields=[*shared.get_user_match_fields(acting_user=user)],
            )
            | Q(
                "nested",
                path="user_profile_fields",
                query=Q(
                    "bool",
                    must=[profile_filter] + ([acl_filter] if apply_acl else []),
                ),
            )
        )

    s = Search(index="user").query(query_filter)

    if profileSetGuid:
        profile_set = user.profile_sets.filter(pk=profileSetGuid).first()

        user_profile_field = user.profile.user_profile_fields.filter(
            profile_field__key=profile_set.field.key
        ).first()
        if not user_profile_field or not user_profile_field.value:
            raise GraphQLError(
                constances.MISSING_REQUIRED_FIELD % profile_set.field.key
            )

        if user_profile_field.value_list_field_indexing:
            set_filter = Q(
                "terms",
                user_profile_fields__value_list=user_profile_field.value_list_field_indexing,
            )
        else:
            set_filter = Q(
                "terms",
                user_profile_fields__value__raw=[
                    user_profile_field.value_field_indexing
                ],
            )

        s = s.filter(
            Q(
                "nested",
                path="user_profile_fields",
                query=Q(
                    "bool",
                    must=[
                        Q("match", user_profile_fields__key=profile_set.field.key)
                        & set_filter
                    ],
                ),
            )
        )

    if filters:
        for f in filters:
            s = s.filter(
                Q(
                    "nested",
                    path="user_profile_fields",
                    query=Q(
                        "bool",
                        must=[
                            Q("match", user_profile_fields__key=f["name"])
                            & (
                                Q("terms", user_profile_fields__value__raw=f["values"])
                                | Q(
                                    "terms", user_profile_fields__value_list=f["values"]
                                )
                            )
                        ],
                    ),
                )
            )

    if groupGuid:
        s = s.filter(
            Q(
                "nested",
                path="memberships",
                query=Q(
                    "bool",
                    must=[Q("match", memberships__group_id=groupGuid)],
                    must_not=[Q("match", memberships__type="pending")],
                ),
            )
        )

    total = s.count()

    # Sort on name is score is equal
    s = s.sort(
        "_score",
        {"name.raw": {"order": "asc"}},
    )

    s = s[offset : offset + limit]
    response = s.execute()

    sorted_objects = User.objects.users_in_order([hit["id"] for hit in response])

    # only get configured profile fields
    profile_section_guids = []

    for section in config.PROFILE_SECTIONS:
        profile_section_guids.extend(section["profileFieldGuids"])

    fields_in_overview = [
        *ProfileField.objects.filter(is_in_overview=True, id__in=profile_section_guids)
    ]

    return {
        "total": total,
        "edges": sorted_objects,
        "filterCount": [],
        "fieldsInOverview": fields_in_overview,
    }
