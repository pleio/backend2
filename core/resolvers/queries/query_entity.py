from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.models import F
from django.utils import timezone
from graphql import GraphQLError

from core.constances import USER_NOT_MEMBER_OF_GROUP
from core.models import Entity, EntityView, EntityViewCount, Group
from user.models import User

query = ObjectType("Query")


@query.field("entity")
def resolve_entity(  # noqa: C901
    _, info, guid=None, username=None, incrementViewCount=False
):
    user = info.context["request"].user

    entity = None

    try:
        entity = Entity.objects.visible(user).get_subclass(id=guid)
        if (
            entity.group
            and entity.group.is_closed
            and not entity.group.is_full_member(user)
            and not user.is_site_admin
        ):
            raise GraphQLError(USER_NOT_MEMBER_OF_GROUP)
    except (ObjectDoesNotExist, ValidationError):
        pass

    # Also try to get User, Group, Comment
    if not entity:
        try:
            entity = Group.objects.visible(user).get(id=guid)
        except (ObjectDoesNotExist, ValidationError):
            pass

    if not entity:
        try:
            if username:
                guid = username
            entity = User.objects.visible(user).get(id=guid)
        except (ObjectDoesNotExist, ValidationError):
            pass

    # check if draft exists
    if not entity:
        if user.is_authenticated:
            try:
                entity = Entity.objects.draft(user).get_subclass(id=guid)
            except (ObjectDoesNotExist, ValidationError):
                pass

    if not entity:
        try:
            entity = Entity.objects.archived(user).get_subclass(id=guid)
        except (ObjectDoesNotExist, ValidationError):
            pass

    if not entity:
        return None

    if incrementViewCount:
        increment_view_count(entity, info.context["request"])

    return entity


def increment_view_count(entity, request):
    # Increase view count of entities
    user = request.user

    if user.is_authenticated:
        EntityView.objects.create(entity=entity, viewer=user)
        views = EntityView.objects.filter(entity=entity, viewer=user).count()
    else:
        sessionid = request.COOKIES.get("sessionid", None)
        EntityView.objects.create(entity=entity, session=sessionid)
        views = EntityView.objects.filter(entity=entity, session=sessionid).count()

    if views == 1:
        if EntityViewCount.objects.filter(entity_id=entity.guid).exists():
            EntityViewCount.objects.filter(entity_id=entity.guid).update(
                views=F("views") + 1, updated_at=timezone.now()
            )
        else:
            EntityViewCount.objects.create(entity_id=entity.guid, views=1)
