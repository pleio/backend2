import logging

from django.db.models import Case, CharField, F, Max, Value, When
from django.db.models.functions import Lower
from django.utils.translation import pgettext
from graphql import GraphQLError

from core.constances import (
    ACCESS_TYPE,
    COULD_NOT_ORDER_BY_SIZE,
    COULD_NOT_ORDER_BY_START_DATE,
    ORDER_BY,
)
from core.resolvers.queries import shared_filters as filters

logger = logging.getLogger(__name__)


def order_by_entity_column(field, subtypes):
    ORDER_BY_CLASSES = [
        OrderByTimeCreated,
        OrderByTimeUpdated,
        OrderByTimePublished,
        OrderByLastAction,
        OrderByStartDate,
        OrderByLastSeen,
        OrderByTitle,
        OrderByGroupName,
        OrderByOwnerName,
        OrderByFileSize,
        OrderByContentType,
        OrderByReadAccess,
    ]
    order_by_class = (
        _get_class_by_field_name(field, ORDER_BY_CLASSES) or OrderByTimePublished
    )
    return order_by_class(subtypes)


def _get_class_by_field_name(field_name, order_by_classes):
    if not field_name:
        return None

    for class_name in order_by_classes:
        if field_name == class_name.FIELD_NAME:
            return class_name


class OrderByBase:
    FIELD_NAME = None

    def __init__(self, subtypes):
        self.order_by_columns = []
        self.is_ascending = True
        self.subtypes = subtypes

    def column(self):
        raise NotImplementedError()

    def extend_query(self, query):
        return query


class OrderByTimeCreated(OrderByBase):
    FIELD_NAME = ORDER_BY.timeCreated

    def column(self):
        return "created_at"


class OrderByTimeUpdated(OrderByBase):
    FIELD_NAME = ORDER_BY.timeUpdated

    def column(self):
        return "updated_at"


class OrderByTimePublished(OrderByBase):
    FIELD_NAME = ORDER_BY.timePublished

    def column(self):
        return "published"


class OrderByLastAction(OrderByBase):
    FIELD_NAME = ORDER_BY.lastAction

    def column(self):
        return "last_action"


class OrderByStartDate(OrderByBase):
    FIELD_NAME = ORDER_BY.startDate

    def column(self):
        if self.subtypes != ["event"]:
            raise GraphQLError(COULD_NOT_ORDER_BY_START_DATE)

        return "event__start_date"


class OrderByLastSeen(OrderByBase):
    FIELD_NAME = ORDER_BY.lastSeen

    def column(self):
        return "last_seen_date"

    def extend_query(self, query):
        return query.annotate(
            last_seen_date=Max("views__created_at"),
        )


class OrderByTitle(OrderByBase):
    FIELD_NAME = ORDER_BY.title

    def column(self):
        return "sort_title"


class OrderByGroupName(OrderByBase):
    FIELD_NAME = ORDER_BY.groupName

    def column(self):
        return "group__name"


class OrderByOwnerName(OrderByBase):
    FIELD_NAME = ORDER_BY.ownerName

    def column(self):
        return "owner__name"


class OrderByFileSize(OrderByBase):
    FIELD_NAME = ORDER_BY.fileSize

    def column(self):
        if not filters.is_combination_of(self.subtypes, ["file", "folder", "pad"]):
            raise GraphQLError(COULD_NOT_ORDER_BY_SIZE)

        return "filefolder__size"


class OrderByContentType(OrderByBase):
    FIELD_NAME = ORDER_BY.contentType

    def column(self):
        return "content_type_weight"

    def extend_query(self, query):
        return query.annotate(
            content_type_weight=Case(
                When(
                    statusupdate__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "statusupdate")),
                ),
                When(
                    blog__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "blog")),
                ),
                When(
                    page__page_type="campagne",
                    then=Value(pgettext("lowercase_sortable", "page/widget")),
                ),
                When(
                    page__page_type="text",
                    then=Value(pgettext("lowercase_sortable", "page/text")),
                ),
                When(
                    discussion__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "discussion")),
                ),
                When(
                    event__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "event")),
                ),
                When(
                    externalcontent__source__isnull=False,
                    then=Lower(F("externalcontent__source__name")),
                ),
                When(
                    filefolder__type="File",
                    then=Value(pgettext("lowercase_sortable", "file/file")),
                ),
                When(
                    filefolder__type="Folder",
                    then=Value(pgettext("lowercase_sortable", "file/folder")),
                ),
                When(
                    filefolder__type="Pad",
                    then=Value(pgettext("lowercase_sortable", "file/pad")),
                ),
                When(
                    magazine__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "magazine/01.magazine")),
                ),
                When(
                    magazineissue__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "magazine/02.issue")),
                ),
                When(
                    news__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "news")),
                ),
                When(
                    podcast__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "podcast/podcast")),
                ),
                When(
                    episode__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "podcast/episode")),
                ),
                When(
                    poll__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "poll")),
                ),
                When(
                    task__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "task")),
                ),
                When(
                    question__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "question")),
                ),
                When(
                    wiki__isnull=False,
                    then=Value(pgettext("lowercase_sortable", "wiki")),
                ),
                default=None,
                output_field=CharField(),
            )
        )


class OrderByReadAccess(OrderByBase):
    FIELD_NAME = ORDER_BY.readAccess

    def column(self):
        return "read_access_weight"

    def extend_query(self, query):
        """
        * First the public stuff,
        * Then the logged in stuff,
        * Then the group stuff,
        * Then the user stuff
        """
        return query.annotate(
            read_access_weight=Case(
                When(read_access__regex=r"user:.*", then=Value("3")),
                When(read_access__contains=[ACCESS_TYPE.logged_in], then=Value("1")),
                When(read_access__contains=[ACCESS_TYPE.public], then=Value("0")),
                # group:.* and subgroup:.* are caught by the same regex.
                When(read_access__regex=r"group:.*", then=Value("2")),
                default=Value("2"),
                output_field=CharField(),
            )
        )
