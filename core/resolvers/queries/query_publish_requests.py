from ariadne import ObjectType

from core.models.entity import PublishRequest
from core.resolvers import decorators

query = ObjectType("Query")


@query.field("publishRequests")
@decorators.resolver_with_request
def resolve_publish_requests(
    request, offset=0, limit=20, status=None, files=False, userGuid=None
):
    acting_user = request.user

    requests = PublishRequest.objects.visible(acting_user).order_by("-time_published")

    if userGuid:
        requests = requests.filter_owner(userGuid)

    if not files:
        requests = requests.exclude_files()
    else:
        requests = requests.filter_files()

    if status:
        requests = requests.filter_status(status)
    else:
        requests = requests.exclude_status("published")
    return {
        "total": requests.count(),
        "edges": requests[offset : offset + limit],
    }
