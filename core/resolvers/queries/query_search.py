from ariadne import ObjectType
from django.utils import timezone
from django_tenants.utils import schema_context
from graphql import GraphQLError

from core import config
from core.constances import INVALID_DATE, INVALID_SUBTYPE, ORDER_DIRECTION
from core.lib import get_search_filters
from core.models import Entity, Group, SearchQueryJournal
from core.resolvers import shared
from core.utils.elasticsearch import QueryBuilder, RelatedSiteSearchResult
from user.models import User

query = ObjectType("Query")


@query.field("search")
def resolve_search(  # noqa: C901
    _,
    info,
    q=None,
    containerGuid=None,
    type=None,
    subtype=None,
    subtypes=None,
    dateFrom=None,
    dateTo=None,
    offset=0,
    limit=20,
    tags=None,
    tagCategories=None,
    matchStrategy="any",
    filterArchived=False,
    orderBy=None,
    orderDirection=ORDER_DIRECTION.asc,
    ownerGuids=None,
    searchRelatedSites=False,
    isRecommendedInSearch=False,
):
    total = 0
    totals = []

    request = info.context["request"]

    user = request.user
    sessionid = request.COOKIES.get("sessionid", None)

    if q:
        SearchQueryJournal.objects.maybe_log_query(
            query=q,
            session=sessionid,
        )

    q = q or "*"

    if type in ["group", "user"]:
        subtype = type

    subtypes_available = {ct["key"]: ct for ct in get_search_filters()}
    subtype_keys = list(subtypes_available.keys())

    if subtypes and [type for type in subtypes if type not in subtype_keys]:
        raise GraphQLError(INVALID_SUBTYPE)

    if subtype and subtype not in subtype_keys:
        raise GraphQLError(INVALID_SUBTYPE)

    query_builder = QueryBuilder(q, user, searchRelatedSites)
    query_builder.maybe_filter_owners(ownerGuids)
    query_builder.maybe_filter_subtypes(subtypes)
    query_builder.maybe_filter_container(containerGuid)
    query_builder.maybe_filter_tags(tags, matchStrategy)
    query_builder.maybe_filter_categories(tagCategories, matchStrategy)
    query_builder.maybe_filter_recommended_in_search(isRecommendedInSearch)
    query_builder.filter_archived(filterArchived)
    query_builder.filter_from_to_dates(dateFrom, dateTo)
    query_builder.order_by(orderBy, orderDirection)
    query_builder.add_aggregation()

    search_query, response = query_builder.execute(offset, limit)
    buckets = []

    for t in response.aggregations.type_terms.buckets:
        buckets.append(t.key)
        totals.append(
            {
                "subtype": t.key,
                "total": t.doc_count,
                "title": subtypes_available[t.key]["plural"],
            }
        )
        if t.key != "image":
            total = total + t.doc_count

    if subtype:
        response = search_query.filter("term", type=subtype).execute()
    elif "image" in buckets:
        response = search_query.exclude("term", type="image").execute()

    ids = []
    for hit in response:
        ids.append(hit["id"])

    id_dict = {}

    entities = Entity.objects.visible_all(user).select_subclasses()
    id_dict.update({e.guid: e for e in entities.filter(id__in=ids)})

    visible_users = User.objects.visible(user)
    id_dict.update({e.guid: e for e in visible_users.filter(id__in=ids)})

    visible_groups = Group.objects.visible(user)
    id_dict.update({e.guid: e for e in visible_groups.filter(id__in=ids)})

    if searchRelatedSites:
        for schema in config.SEARCH_RELATED_SCHEMAS:
            with schema_context(schema):
                id_dict.update(
                    {
                        e.guid: RelatedSiteSearchResult.from_model(e)
                        for e in Entity.objects.filter(id__in=ids).select_subclasses()
                    }
                )
                id_dict.update(
                    {
                        e.guid: RelatedSiteSearchResult.from_model(e)
                        for e in User.objects.filter(id__in=ids)
                    }
                )
                id_dict.update(
                    {
                        e.guid: RelatedSiteSearchResult.from_model(e)
                        for e in Group.objects.filter(id__in=ids)
                    }
                )

    sorted_objects = [id_dict[id] for id in ids if id in id_dict]
    return {"total": total, "totals": totals, "edges": sorted_objects}


@query.field("searchJournal")
def resolve_search_journal(
    _, info, dateTimeFrom=None, dateTimeTo=None, limit=None, offset=None
):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    if dateTimeTo is None:
        dateTimeTo = timezone.now()

    if dateTimeFrom is None:
        dateTimeFrom = dateTimeTo - timezone.timedelta(weeks=4)

    if dateTimeFrom > dateTimeTo:
        raise GraphQLError(INVALID_DATE)

    summary = list(
        SearchQueryJournal.objects.summary(
            start=dateTimeFrom,
            end=dateTimeTo,
        )
    )
    total = len(summary)
    if offset is not None:
        summary = summary[offset:]
    if limit is not None:
        summary = summary[:limit]
    return {
        "total": total,
        "edges": summary,
    }
