from ariadne import ObjectType

from core.models.tags import Tag
from core.resolvers import decorators

query = ObjectType("Query")


@query.field("tags")
@decorators.resolver_without_object_info
def resolve_query_tags():
    return get_query_tags()


def get_query_tags():
    return [
        {"label": t.label, "synonyms": [s.label for s in t.synonyms.all()]}
        for t in Tag.objects.all()
    ]
