from ariadne import ObjectType
from django.db.models import F, Q
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, GROUP_MEMBER_ROLES, NOT_LOGGED_IN
from core.models import Group, GroupMembership, UserProfileField
from core.resolvers import shared

query = ObjectType("Query")


@query.field("members")
def resolve_members(
    _,
    info,
    groupGuid,
    q="",
    filters=None,
    offset=0,
    limit=20,
    roleFilter=None,
    orderBy="name",
    orderDirection="asc",
):
    try:
        user = info.context["request"].user

        if not user.is_authenticated:
            raise GraphQLError(NOT_LOGGED_IN)

        group = Group.objects.get(id=groupGuid)
        shared.assert_group_member(user, group)

        query = GroupMembership.objects.filter(
            group_id=groupGuid, user__is_superadmin=False
        ).exclude(type="pending")

        if q:
            user_matches = Q(user__name__icontains=q) | Q(user__email__icontains=q)

            fields_in_overview = shared.resolve_group_fields_in_overview(group)
            query_profile = (
                UserProfileField.objects.visible(user)
                .filter(
                    profile_field__key__in=[f.key for f in fields_in_overview],
                    value__icontains=q,
                )
                .values_list("user_profile__user_id", flat=True)
            )
            profile_field_matches = Q(user_id__in=query_profile)

            query = query.filter(user_matches | profile_field_matches)

        if filters:
            for f in filters:
                values_match: Q = None
                for value in f["values"]:
                    value_match = Q(value=value)
                    value_match |= Q(value__startswith=value + ",")
                    value_match |= Q(value__contains="," + value + ",")
                    value_match |= Q(value__endswith="," + value)
                    if values_match:
                        values_match |= value_match
                    else:
                        values_match = value_match
                query = query.filter(
                    user_id__in=UserProfileField.objects.filter(
                        Q(profile_field__key=f["name"]) & values_match
                    ).values_list("user_profile__user_id", flat=True)
                )
        query = _filtered_by_role(query, roleFilter)
        query = _ordered_query(query, orderBy, orderDirection)
        total = query.count()

        return {
            "total": total,
            "edges": query[offset : offset + limit],
            "fieldsInOverview": shared.resolve_group_fields_in_overview(group),
        }
    except Group.DoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)


def _ordered_query(qs, orderBy=None, orderDirection=None):
    if orderBy:
        order = None
        if orderBy == "name":
            order = "user__name"
        elif orderBy == "memberSince":
            order = "created_at"
        if orderDirection.lower() == "desc":
            order = "-%s" % order
        if order:
            return qs.order_by(order)

    return qs


def _filtered_by_role(qs, roleFilter):
    if roleFilter:
        if roleFilter == "admin":
            return qs.filter(roles__contains=[GROUP_MEMBER_ROLES.GROUP_ADMIN])
        elif roleFilter == "newsEditor":
            return qs.filter(roles__contains=[GROUP_MEMBER_ROLES.GROUP_NEWS_EDITOR])
        elif roleFilter == "owner":
            return qs.filter(group__owner=F("user"))
    return qs
