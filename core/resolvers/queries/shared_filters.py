from django.db.models import F, Q

from core.models import Tag
from core.models.tags import flat_category_tags
from entities.external_content.utils import is_external_content_source


def is_combination_of(items, allowed_items):
    if not allowed_items:
        return False

    if [item for item in items if item not in allowed_items]:
        return False

    if not [item for item in items if item in allowed_items]:
        return False

    return True


def conditional_groups_filter(group_filter, user):
    if group_filter == ["all"]:
        return Q(group__isnull=False)
    if group_filter == ["mine"]:
        groups = []
        for membership in user.memberships.filter(type="member"):
            groups.append(membership.group.id)
        return Q(group__in=groups)
    return Q()


def conditional_group_guid(group_guid):
    """
    Filter only items in group.
    """
    if group_guid == "1":
        # Or not in a group.
        return Q(group=None)
    if group_guid:
        return Q(group__id=group_guid)

    return Q()


def conditional_tags_filter(tags, match_any):
    if tags:
        filters = Q()
        if match_any:
            filters.add(Q(_tag_summary__overlap=Tag.translate_tags(tags)), Q.AND)
        else:
            for tag in Tag.translate_tags(tags):
                filters.add(Q(_tag_summary__overlap=[tag]), Q.AND)  # of Q.OR

        return filters
    return Q()


def conditional_tag_lists_filter(categorytag_lists, match_any):
    if not categorytag_lists:
        return Q()

    filters = Q()
    for category in categorytag_lists:
        matches = [*flat_category_tags(category)]
        if not matches:
            continue
        if match_any:
            filters.add(Q(_category_summary__overlap=matches), Q.AND)
        else:
            for match in matches:
                filters.add(Q(_category_summary__overlap=[match]), Q.AND)
    return filters


class EntityFilterBase:
    key = None

    def __init__(self, subtypes):
        self.subtypes = subtypes

    def add_if_applicable(self, query):
        if not self.subtypes or self.key in self.subtypes:
            self.add(query)

    def add(self, query):
        raise NotImplementedError()


class MagazineEntityFilter(EntityFilterBase):
    key = "magazine"

    def add(self, query):
        query.add(~Q(magazine__isnull=True), Q.OR)


class MagazineIssueEntityFilter(EntityFilterBase):
    key = "magazine_issue"

    def add(self, query):
        query.add(~Q(magazineissue__isnull=True), Q.OR)


class NewsEntityFilter(EntityFilterBase):
    key = "news"

    def add(self, query):
        query.add(~Q(news__isnull=True), Q.OR)


class BlogEntityFilter(EntityFilterBase):
    key = "blog"

    def add(self, query):
        query.add(~Q(blog__isnull=True), Q.OR)


class PodcastEntityFilter(EntityFilterBase):
    key = "podcast"

    def add_if_applicable(self, query):
        if self.subtypes and self.key in self.subtypes:
            self.add(query)

    def add(self, query):
        query.add(~Q(podcast__isnull=True), Q.OR)


class EpisodeEntityFilter(EntityFilterBase):
    key = "episode"

    def add(self, query):
        query.add(~Q(episode__isnull=True), Q.OR)


class EventEntityFilter(EntityFilterBase):
    key = "event"

    def add_if_applicable(self, query):
        if self.subtypes == ["event"]:
            self.add_all_events(query)
        else:
            super().add_if_applicable(query)

    @staticmethod
    def add_all_events(query):
        query.add(~Q(event__isnull=True) & ~Q(event__parent__isnull=False), Q.OR)

    def add(self, query):
        query.add(
            ~Q(event__isnull=True)
            & ~Q(event__parent__isnull=False)
            & Q(event__index_item=True),
            Q.OR,
        )


class DiscussionEntityFilter(EntityFilterBase):
    key = "discussion"

    def add(self, query):
        query.add(~Q(discussion__isnull=True), Q.OR)


class StatusupdateEntityFilter(EntityFilterBase):
    key = "statusupdate"

    def add(self, query):
        query.add(~Q(statusupdate__isnull=True), Q.OR)


class QuestionEntityFilter(EntityFilterBase):
    key = "question"

    def add(self, query):
        query.add(~Q(question__isnull=True), Q.OR)


class PollEntityFilter(EntityFilterBase):
    key = "poll"

    def add(self, query):
        query.add(~Q(poll__isnull=True), Q.OR)


class WikiEntityFilter(EntityFilterBase):
    key = "wiki"

    def add(self, query):
        query.add(~Q(wiki__isnull=True), Q.OR)


def page_filter():
    # ~page__isnull means that it should be a page
    #   - NOT ("is a page" is null) should be True)
    #   - Filter entities where page has a value.
    # ~page__system_setting means that it should not be a system setting
    #   - NOT ("is a system setting" is True) should be True
    #   - Filter entities where system_setting is False
    return ~Q(page__isnull=True) & ~Q(page__system_setting=True)


class PageEntityFilter(EntityFilterBase):
    key = "page"

    def add(self, query):
        query.add(page_filter() & ~Q(start_page_for=F("group")), Q.OR)


class LandingPagesFilter(EntityFilterBase):
    key = "landingpage"

    def add_if_applicable(self, query):
        if self.subtypes and self.key in self.subtypes:
            self.add(query)

    def add(self, query):
        query.add(page_filter() & Q(start_page_for=F("group")), Q.OR)


class FileEntityFilter(EntityFilterBase):
    key = "file"

    def add(self, query):
        query.add(~Q(filefolder__isnull=True) & Q(filefolder__type="File"), Q.OR)


class FolderEntityFilter(EntityFilterBase):
    key = "folder"

    def add(self, query):
        query.add(~Q(filefolder__isnull=True) & Q(filefolder__type="Folder"), Q.OR)


class PadEntityFilter(EntityFilterBase):
    key = "pad"

    def add(self, query):
        query.add(~Q(filefolder__isnull=True) & Q(filefolder__type="Pad"), Q.OR)


class TaskEntityFilter(EntityFilterBase):
    key = "task"

    def add(self, query):
        query.add(~Q(task__isnull=True), Q.OR)


class ExternalcontentEntityFilter(EntityFilterBase):
    def add_if_applicable(self, query):
        if self.subtypes:
            self.add_matching(query, self.subtypes)
        else:
            self.add(query)

    @staticmethod
    def add_matching(query, subtypes):
        for object_type in subtypes:
            if is_external_content_source(object_type):
                query.add(
                    ~Q(externalcontent__isnull=True)
                    & Q(externalcontent__source_id=object_type),
                    Q.OR,
                )

    def add(self, query):
        query.add(~Q(externalcontent__isnull=True), Q.OR)
