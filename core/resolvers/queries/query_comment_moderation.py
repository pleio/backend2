from ariadne import ObjectType

from core.models.comment import CommentModerationRequest
from core.resolvers import decorators

query = ObjectType("Query")


@query.field("commentModerationRequests")
@decorators.resolver_with_request
def resolve_comment_moderation_requests(
    request, offset=0, limit=20, status=None, userGuid=None
):
    requests = CommentModerationRequest.objects.visible(request.user)
    requests = requests.order_by("-created_at")

    if userGuid:
        requests = requests.filter_owner(userGuid)

    if status == "open":
        requests = requests.filter_open_requests()
    elif status == "assigned":
        requests = requests.filter_assigned_requests()
    elif status == "closed":
        requests = requests.filter_closed_requests()

    return {
        "total": requests.count(),
        "edges": requests[offset:][:limit],
    }
