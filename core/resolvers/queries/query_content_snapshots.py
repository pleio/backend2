from ariadne import ObjectType

from core.resolvers import shared
from entities.file.models import FileFolder

query = ObjectType("Query")


@query.field("contentSnapshots")
def resolve_content_snapshots(obj, info):
    user = info.context["request"].user
    shared.assert_authenticated(user)

    return {"success": True, "edges": FileFolder.objects.content_snapshots(user=user)}
