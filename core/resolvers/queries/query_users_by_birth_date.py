import logging
from datetime import timedelta

from ariadne import ObjectType
from django.core.exceptions import ValidationError
from django.utils import timezone

from core.exceptions import InvalidFieldException
from user.models import User

logger = logging.getLogger(__name__)
query = ObjectType("Query")


@query.field("usersByBirthDate")
def resolve_users_by_birth_date(
    _,
    info,
    profileFieldGuid,
    groupGuid=None,
    futureDays=30,
    offset=0,
    limit=20,
):
    user = info.context["request"].user

    start_day = timezone.now() - timedelta(days=2)
    end_day = timezone.now() + timedelta(days=futureDays)

    try:
        queryset = User.objects
        if groupGuid:
            queryset = queryset.filter_group(groupGuid)

        return queryset.get_upcoming_birthday_users(
            profileFieldGuid, user, start_day, end_day, offset, limit
        )
    except (InvalidFieldException, ValidationError):
        pass
    return []
