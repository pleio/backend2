from urllib.parse import urljoin

from ariadne import ObjectType
from django.conf import settings

from core.resolvers import decorators

query = ObjectType("Query")


@query.field("viewer")
@decorators.resolver_with_request
def resolve_viewer(request):
    return user_viewer_adapter(request)


def user_viewer_adapter(request):
    default_viewer = {
        "isBanned": False,
    }
    try:
        if "pleio_user_is_banned" in request.session:
            default_viewer["isBanned"] = True
    except Exception:
        pass

    if request.user.is_authenticated:
        return logged_in_user_viewer_adapter(request.user, **default_viewer)
    return anonymous_user_viewer_adapter(**default_viewer)


def logged_in_user_viewer_adapter(user, **kwargs):
    return {
        **kwargs,
        "guid": "viewer:{}".format(user.id),
        "loggedIn": True,
        "isSubEditor": user.is_editor,
        "isNewsEditor": user.is_news_editor,
        "isAdmin": user.is_site_admin,
        "has2faEnabled": user.has_2fa_enabled,
        "tags": [],
        "categories": [],
        "updateAccountUrl": urljoin(settings.PROFILE_PICTURE_URL, "profile"),
        "user": user,
    }


def anonymous_user_viewer_adapter(**kwargs):
    return {
        **kwargs,
        "guid": "viewer:0",
        "loggedIn": False,
        "isSubEditor": False,
        "isNewsEditor": False,
        "isAdmin": False,
        "has2faEnabled": False,
        "tags": [],
        "categories": [],
        "user": {"guid": "0"},
    }
