from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.models import Group
from core.resolvers.queries import shared_filters as filters
from user.models import User

query = ObjectType("Query")


@query.field("groups")
def resolve_groups(
    _,
    info,
    q=None,
    filter=None,
    tags=None,
    tagCategories=None,
    matchStrategy="any",
    userGuid=None,
    offset=0,
    limit=20,
):
    user = info.context["request"].user

    groups = Group.objects.visible(user)
    if q:
        groups = groups.filter(name__icontains=q)

    if filter == "mine":
        group_ids = user.memberships.filter(type="member").values_list(
            "group", flat=True
        )
        groups = groups.filter(id__in=group_ids)

    if userGuid:
        try:
            filter_user = User.objects.get(id=userGuid)
        except ObjectDoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)

        group_ids = filter_user.memberships.filter(type="member").values_list(
            "group", flat=True
        )
        groups = groups.filter(id__in=group_ids)

    if tags or tagCategories:
        groups = groups.filter(
            filters.conditional_tags_filter(tags, matchStrategy == "any")
            & filters.conditional_tag_lists_filter(
                tagCategories, matchStrategy != "all"
            )
        )

    edges = groups.order_by("-is_featured", "name")[offset : offset + limit]

    return {"total": groups.count(), "edges": edges}
