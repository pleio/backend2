from ariadne import ObjectType

query = ObjectType("Query")


@query.field("filters")
def resolve_filters(*_):
    return {"guid": 1}
