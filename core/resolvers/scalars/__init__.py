from .datetime_scalar import datetime_scalar as _datetime_scalar
from .secure_rich_text import secure_rich_text as _secure_rich_text

resolvers = [
    _datetime_scalar,
    _secure_rich_text,
]
