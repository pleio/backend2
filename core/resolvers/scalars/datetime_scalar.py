from datetime import datetime
from typing import Any, List, Optional

import dateutil.parser
from ariadne import ScalarType
from django.forms.utils import from_current_timezone
from django.utils import formats
from django.utils.translation import gettext_lazy as _

datetime_scalar = ScalarType("DateTime")


def parse_value(value: Any, formats: List[str]) -> Optional[datetime]:
    for format_str in formats:
        try:
            return datetime.strptime(value, format_str)
        except (ValueError, TypeError):
            continue

    # fallback to using dateutil parser
    try:
        return dateutil.parser.parse(value)
    except (ValueError, TypeError):
        return None


datetime_input_formats = formats.get_format_lazy("DATETIME_INPUT_FORMATS")


@datetime_scalar.serializer
def serialize_datetime(value: datetime) -> str:
    return value.isoformat()


@datetime_scalar.value_parser
def parse_datetime_value(value: Any) -> datetime:
    parsed_value = parse_value(value, datetime_input_formats)
    if not parsed_value:
        raise ValueError(_("Enter a valid date/time."))
    return from_current_timezone(parsed_value)
