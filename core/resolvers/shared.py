import logging
from functools import wraps

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.files.storage import default_storage
from django.utils import timezone
from django.utils.translation import gettext
from elasticsearch_dsl import Search
from graphql import GraphQLError
from online_planner.meetings_api import MeetingsApi

from core import config, constances
from core.constances import ACCESS_TYPE, FILE_NOT_CLEAN
from core.exceptions import AttachmentVirusScanError
from core.lib import access_id_to_acl, exclude_root_acl, get_access_id, html_to_text
from core.models import Entity, EntityViewCount, Group, VideoCallGuest
from core.models.mixin import HierarchicalEntityMixin
from core.models.revision import Revision
from core.models.videocall import EntityVideoCall
from core.resolvers import decorators
from core.resolvers.decorators import resolve_obj_request
from core.tasks.cleanup_tasks import cleanup_featured_image_files
from core.utils.acl import AclToGql, GqlToAcl
from core.utils.convert import is_tiptap, tiptap_to_text, truncate_rich_description
from core.utils.entity import load_entity_by_id
from core.utils.entity_form import EntityFormWrapper, UpdateUserFormFieldCollection
from core.utils.tiptap_parser import Tiptap
from core.utils.translation.lib import should_use_foreign_language
from core.utils.translation.translatables import (
    AbstractTranslatable,
    DescriptionTranslatable,
    RichDescriptionTranslatable,
    SubTitleTranslatable,
    TitleTranslatable,
)
from entities.cms.row_resolver import RowSerializer
from entities.file.helpers.images import strip_exif
from entities.file.models import FileFolder
from user.admin_permissions import (
    HasAccessRequestManagementPermission,
    HasFullControlPermission,
    HasUserManagementPermission,
)
from user.models import User

logger = logging.getLogger(__name__)


def resolve_entity_show_owner(obj, info):
    return not getattr(obj, "hide_owner", False)


def resolve_entity_input_language(obj, info):
    return getattr(obj, "input_language", config.LANGUAGE)


def resolve_entity_access_id(obj, info):
    return get_access_id(obj.read_access)


def resolve_entity_access_control(obj, info):
    if obj.can_write(info.context["request"].user):
        acl_list = AclToGql()
        acl_list.add_read_acl(exclude_root_acl(obj, getattr(obj, "read_access", [])))
        acl_list.add_write_acl(exclude_root_acl(obj, getattr(obj, "write_access", [])))
        return [*acl_list.as_array()]
    return []


def resolve_entity_write_access_id(obj, info):
    if obj.group and obj.group.subgroups:
        for subgroup in obj.group.subgroups.all():
            if ACCESS_TYPE.subgroup.format(subgroup.access_id) in obj.write_access:
                return subgroup.access_id
    if obj.group and ACCESS_TYPE.group.format(obj.group.id) in obj.write_access:
        return 4
    if ACCESS_TYPE.public in obj.write_access:
        return 2
    if ACCESS_TYPE.logged_in in obj.write_access:
        return 1
    return 0


@decorators.resolve_obj_request
def resolve_entity_can_edit(obj, request):
    try:
        return obj.can_write(request.user)
    except AttributeError:
        return False


@decorators.resolve_obj_request
def resolve_entity_can_archive(obj, request):
    try:
        return obj.can_archive(request.user)
    except AttributeError:
        return False


def resolve_entity_can_edit_advanced(obj, info):
    actor = info.context["request"].user
    return bool(
        actor.is_authenticated
        and (actor.is_site_admin or (obj.group and obj.group.can_write(actor)))
    )


def resolve_entity_can_edit_group(obj, info):
    actor = info.context["request"].user
    return actor.is_authenticated and actor.is_site_admin


def resolve_entity_featured(obj, info):
    return {
        "image": obj.featured_image,
        "video": obj.featured_video,
        "videoTitle": obj.featured_video_title,
        "videoThumbnailUrl": obj.featured_video_thumbnail_url,
        "positionY": obj.featured_position_y,
        "alt": obj.featured_alt,
        "caption": obj.featured_caption,
    }


def resolve_entity_suggested_items(obj, info):
    suggested = []
    if obj.suggested_items:
        for item in obj.suggested_items:
            entity = load_entity_by_id(item, ["core.Entity"], fail_if_not_found=False)
            if entity:
                suggested.append(entity)

    return suggested


@decorators.resolve_obj
def resolve_entity_guid(obj):
    return obj.guid


@decorators.resolver_without_object_info
def resolve_entity_status():
    return 200


@decorators.resolve_obj
def resolve_entity_type_as_string(obj):
    return getattr(obj, "type_to_string", "")


@decorators.resolve_obj
def resolve_entity_url_from_object(obj):
    return getattr(obj, "url", "")


@decorators.resolve_obj
def resolve_entity_is_featured_property(obj):
    return obj.is_featured


@decorators.resolve_obj
def resolve_entity_is_recommended_property(obj):
    return obj.is_recommended


def resolve_entity_title(obj, info):
    return obj.title or ""


def resolve_entity_local_title(obj, info):
    if should_use_foreign_language(obj.input_language) and obj.is_translation_enabled:
        return TitleTranslatable(obj).translate()


def resolve_update_sub_title(entity, clean_input):
    if "subtitle" in clean_input:
        entity.sub_title = clean_input.get("subtitle")


def resolve_entity_sub_title(obj, info):
    return obj.sub_title or ""


def resolve_entity_local_sub_title(obj, info):
    if should_use_foreign_language(obj.input_language) and obj.is_translation_enabled:
        return SubTitleTranslatable(obj).translate()


def resolve_entity_abstract(obj, info):
    return getattr(obj, "abstract", None) or None


def resolve_entity_local_abstract(obj, info):
    if should_use_foreign_language(obj.input_language) and obj.is_translation_enabled:
        return AbstractTranslatable(obj).translate()


def resolve_entity_description(obj, info):
    return tiptap_to_text(obj.rich_description)


def resolve_entity_local_description(obj, info):
    description = resolve_entity_description(obj, info)
    if (
        description
        and should_use_foreign_language(obj.input_language)
        and obj.is_translation_enabled
    ):
        translatable = DescriptionTranslatable(obj, description)
        return translatable.translate()


def resolve_entity_rich_description(obj, info):
    return obj.rich_description or ""


def resolve_entity_local_rich_description(obj, info):
    if should_use_foreign_language(obj.input_language) and obj.is_translation_enabled:
        return RichDescriptionTranslatable(obj).translate()


def resolve_entity_excerpt(obj, info):
    if abstract := resolve_entity_abstract(obj, info):
        return tiptap_to_text(abstract)
    if rich_description := resolve_entity_rich_description(obj, info):
        return truncate_rich_description(rich_description)
    return ""


def resolve_entity_local_excerpt(obj, info):
    if abstract := resolve_entity_local_abstract(obj, info):
        return tiptap_to_text(abstract)
    if rich_description := resolve_entity_local_rich_description(obj, info):
        return truncate_rich_description(rich_description)
    return ""


def resolve_entity_is_translation_enabled(obj, info):
    return obj.is_translation_enabled


def update_is_translation_enabled(obj, clean_input):
    if "isTranslationEnabled" in clean_input:
        obj.is_translation_enabled = clean_input.get("isTranslationEnabled")


def resolve_entity_tags(obj, info):
    return obj.tags


def resolve_entity_time_created(obj, info):
    return obj.created_at


def resolve_entity_time_updated(obj, info):
    return obj.updated_at


def resolve_entity_time_published(obj, info):
    return obj.published


def resolve_entity_schedule_archive_entity(obj, _):
    return obj.schedule_archive_after


def resolve_entity_schedule_delete_entity(obj, _):
    return obj.schedule_delete_after


def resolve_entity_status_published(obj, info):
    return obj.status_published


def resolve_entity_can_vote(obj, info):
    try:
        return obj.can_vote(info.context["request"].user)
    except AttributeError:
        return False


def resolve_entity_can_bookmark(obj, info):
    try:
        return obj.can_bookmark(info.context["request"].user)
    except AttributeError:
        return False


def resolve_entity_has_voted(obj, info):
    user = info.context["request"].user
    try:
        return obj.has_voted(user)
    except AttributeError:
        return None


def resolve_entity_votes(obj, info):
    try:
        return obj.vote_count()
    except AttributeError:
        return 0


def resolve_entity_is_bookmarked(obj, info):
    user = info.context["request"].user
    try:
        return obj.is_bookmarked(user)
    except AttributeError:
        return False


def resolve_entity_can_comment(obj, info):
    try:
        return obj.can_comment(info.context["request"].user)
    except AttributeError:
        return False


@resolve_obj_request
def resolve_entity_comments(obj, request):
    try:
        return obj.comments.visible(request.user)
    except AttributeError:
        return []


def resolve_entity_comment_count(obj, info):
    count = _comment_count_from_index(obj)
    if count is None:
        return _comment_count_from_object(obj)
    return count


def resolve_entity_last_seen(obj, info):
    return obj.last_seen


def _comment_count_from_index(obj):
    try:
        query = (
            Search(index="_all").query("match", id=obj.guid).source(["id", "comments"])
        )
        for match in query.execute():
            if match.id == obj.guid:
                return len(match.comments)
    except Exception as e:
        logger.error(e)
    return None


def _comment_count_from_object(obj):
    try:
        return len([c.id for c in obj.get_flat_comment_list()])
    except AttributeError:
        pass
    return 0


def resolve_entity_is_following(obj, info):
    try:
        return obj.is_following(info.context["request"].user)
    except AttributeError:
        return False


def resolve_entity_views(obj, info):
    try:
        return EntityViewCount.objects.get(entity=obj).views
    except ObjectDoesNotExist:
        return 0


def resolve_entity_owner(obj, info):
    return obj.owner


def resolve_entity_in_group(obj, info):
    return bool(getattr(obj, "group", None))


def resolve_entity_group(obj, info):
    return getattr(obj, "group", None)


def resolve_group_fields_in_overview(obj, info=None):
    field_settings = obj.profile_field_settings.filter(show_field=True)
    return [fs.profile_field for fs in field_settings]


def resolve_entity_is_pinned(obj, info):
    if hasattr(obj, "is_pinned"):
        return obj.is_pinned
    return False


def resolve_entity_categories(obj, info):
    return obj.category_tags


def resolve_add_suggested_items(entity, clean_input):
    clean_input.setdefault("suggestedItems", [])
    resolve_update_suggested_items(entity, clean_input)


def resolve_start_revision(entity, user, coauthors=None):
    if entity.has_revisions():
        revision = Revision()
        revision.author = user
        revision.coauthors = coauthors
        revision.start_tracking_changes(entity)
        return revision


def store_update_revision(revision, container):
    if revision:
        revision.store_update_revision(container)


def store_initial_revision(container):
    if container.has_revisions():
        Revision().store_initial_version(container)


# Update
def resolve_update_suggested_items(entity, clean_input):
    if "suggestedItems" in clean_input:
        entity.suggested_items = clean_input.get("suggestedItems")


def update_updated_at(entity):
    if isinstance(entity, FileFolder):
        entity.update_updated_at()  # update parent folder dates
    else:
        entity.updated_at = timezone.now()


def update_publication_dates(entity, acting_user, clean_input):
    if "timePublished" in clean_input:
        entity.published = clean_input.get("timePublished")
        entity.update_last_action(entity.published)

    if "scheduleArchiveEntity" in clean_input:
        migration = ManageChangingArchiveDate(entity, acting_user)
        migration.update_schedule_archive_date(
            clean_input.get("scheduleArchiveEntity") or None
        )

    if "scheduleDeleteEntity" in clean_input:
        entity.schedule_delete_after = clean_input.get("scheduleDeleteEntity") or None

    if entity.schedule_delete_after and entity.schedule_archive_after:
        if entity.schedule_delete_after < entity.schedule_archive_after:
            raise GraphQLError(constances.INVALID_ARCHIVE_AFTER_DATE)


def update_featured_image(entity, clean_input):
    if "featured" not in clean_input:
        return
    featured = clean_input["featured"]

    if featured.get("imageGuid"):
        try:
            maybe_image = FileFolder.objects.get(id=featured["imageGuid"])
            if not maybe_image.is_image():
                raise GraphQLError(constances.NOT_AN_IMAGE)
            featured["image"] = {"guid": maybe_image.guid}
            del featured["imageGuid"]
        except (FileFolder.DoesNotExist, ValidationError):
            raise GraphQLError(constances.COULD_NOT_FIND)

    if featured.get("video"):
        featured["videoThumbnailUrl"] = entity.get_video_thumbnail_url(
            featured["video"]
        )

    original = entity.serialize_featured()
    entity.unserialize_featured(featured)

    if not hasattr(entity, "has_revisions") or not entity.has_revisions():
        if entity.is_featured_image_changed(original):
            cleanup_featured_image_files((original.get("image", {}) or {}).get("guid"))


def resolve_update_title(entity, clean_input):
    if "title" in clean_input:
        title = clean_input.get("title").strip()
        assert_valid_title(title)
        entity.title = title


def resolve_update_rich_description(entity, clean_input):
    if "richDescription" in clean_input:
        entity.rich_description = clean_input.get("richDescription")


def resolve_update_introduction(entity, clean_input):
    if "introduction" in clean_input:
        entity.introduction = clean_input.get("introduction")


def resolve_update_tags(entity, clean_input):
    if "tags" in clean_input:
        entity.tags = clean_input["tags"]

    if "tagCategories" in clean_input:
        entity.category_tags = clean_input["tagCategories"]


def update_access_level_allowed(user, group):
    if config.HIDE_ACCESS_LEVEL_SELECT:
        if group:
            return group.can_write(user)
        return user.is_authenticated and user.is_site_admin
    return user.is_authenticated


def resolve_add_access_id(entity, clean_input, user):
    clean_input.setdefault("writeAccessId", 0)

    container = None
    if "containerGuid" in clean_input:
        container = load_entity_by_id(
            clean_input["containerGuid"],
            ["magazine.MagazineIssue"],
            fail_if_not_found=False,
        )

    if container:
        clean_input.setdefault("accessId", get_access_id(container.read_access))
    else:
        clean_input.setdefault("accessId", config.DEFAULT_ACCESS_ID)

    user_can_update_access_level = update_access_level_allowed(
        user, getattr(entity, "group", None)
    )

    if not user_can_update_access_level:
        entity.read_access = access_id_to_acl(entity, config.DEFAULT_ACCESS_ID)
    resolve_update_access_id(entity, clean_input, user)


def recursive_update_entity_children(entity, user, callback):
    if hasattr(entity, "get_all_children"):
        for child in entity.get_all_children():
            if not child.can_write(user):
                continue
            revision = resolve_start_revision(child, user)
            before = child.serialize()
            callback(child)
            if child.serialize() != before:
                child.save()
                store_update_revision(revision, child)


def resolve_update_access_id(entity, clean_input, user):
    user_can_update_access_level = update_access_level_allowed(
        user, getattr(entity, "group", None)
    )

    if "accessControl" in clean_input:
        gql_to_acl = GqlToAcl(entity)

        for spec in clean_input["accessControl"]:
            gql_to_acl.add_spec(spec)

        if user_can_update_access_level:
            entity.read_access = gql_to_acl.get_read_access()
        entity.write_access = gql_to_acl.get_write_access()

    else:
        if "accessId" in clean_input and user_can_update_access_level:
            entity.read_access = access_id_to_acl(entity, clean_input.get("accessId"))
        if "writeAccessId" in clean_input:
            entity.write_access = access_id_to_acl(
                entity, clean_input.get("writeAccessId")
            )

    if clean_input.get("isAccessRecursive"):

        def update_read_write_access(e):
            e.write_access = entity.write_access
            e.read_access = entity.read_access

        recursive_update_entity_children(entity, user, update_read_write_access)


def resolve_add_input_language(entity, clean_input):
    if "inputLanguage" not in clean_input:
        entity.input_language = config.LANGUAGE
    else:
        resolve_update_input_language(entity, clean_input)


def resolve_update_input_language(entity, clean_input):
    if "inputLanguage" in clean_input:
        new_language = clean_input["inputLanguage"]
        if entity.input_language != new_language and new_language not in [
            entity.input_language,
            config.LANGUAGE,
            *config.EXTRA_LANGUAGES,
        ]:
            raise GraphQLError(constances.INVALID_VALUE)
        entity.input_language = clean_input["inputLanguage"]


def resolve_update_group(entity, clean_input):
    if "groupGuid" in clean_input:
        if clean_input["groupGuid"]:
            try:
                entity.group = Group.objects.get(id=clean_input["groupGuid"])
            except Group.DoesNotExist:
                raise GraphQLError(constances.COULD_NOT_FIND)
        else:
            entity.group = None


def resolve_update_owner(entity, clean_input):
    if "ownerGuid" in clean_input:
        try:
            currentOwnerGuid = entity.owner.guid if entity.owner else None
            if clean_input["ownerGuid"] != currentOwnerGuid:
                previous_owner_formatted = ACCESS_TYPE.user.format(entity.owner.id)
                owner = User.objects.get(id=clean_input.get("ownerGuid"))
                entity.owner = owner
                owner_formatted = ACCESS_TYPE.user.format(owner.id)
                if previous_owner_formatted in entity.read_access:
                    entity.read_access = [
                        i for i in entity.read_access if i != previous_owner_formatted
                    ]
                    entity.read_access.append(owner_formatted)
                if previous_owner_formatted in entity.write_access:
                    entity.write_access = [
                        i for i in entity.write_access if i != previous_owner_formatted
                    ]
                    entity.write_access.append(owner_formatted)
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)


def resolve_update_time_created(entity, clean_input):
    if "timeCreated" in clean_input:
        entity.created_at = clean_input["timeCreated"]


def resolve_update_abstract(entity, clean_input):
    if "abstract" in clean_input:
        abstract = clean_input.get("abstract") or ""
        assert_valid_abstract(abstract)
        entity.abstract = abstract


def update_is_featured(entity, user, clean_input):
    if "isFeatured" in clean_input and (
        user.is_site_admin
        or user.is_editor
        or (entity._meta.model_name == "news" and user.is_news_editor)
    ):
        entity.is_featured = clean_input.get("isFeatured")


def update_is_recommended(entity, user, clean_input):
    if "isRecommended" in clean_input and user.is_editor:
        entity.is_recommended = clean_input.get("isRecommended")


def update_is_recommended_in_search(entity, user, clean_input):
    if "isRecommendedInSearch" in clean_input and user.is_editor:
        entity.is_recommended_in_search = clean_input.get("isRecommendedInSearch")


def resolve_update_video_call_enabled(entity, clean_input):
    if "videoCallEnabled" in clean_input:
        if clean_input.get("videoCallEnabled") is True:
            EntityVideoCall.objects.get_or_create(entity=entity)
        elif clean_input.get("videoCallEnabled") is False:
            EntityVideoCall.objects.filter(entity=entity).delete()


def resolve_update_video_call_moderators(entity, clean_input):
    if "videoCallModeratorUserGuids" in clean_input:
        try:
            video_call = EntityVideoCall.objects.get(entity=entity)
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND)

        video_call.update_moderators(clean_input.get("videoCallModeratorUserGuids"))


# Group
def get_group(clean_input):
    if clean_input.get("groupGuid"):
        try:
            group = Group.objects.get(id=clean_input["groupGuid"])
            return group
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND_GROUP)
    else:
        return None


def assert_graphql(condition, message):
    if not condition:
        raise GraphQLError(message)


def assert_is_subclass(instance, cls, err):
    if not isinstance(instance, cls):
        raise GraphQLError(err)


# Checks
def assert_administrator(user):
    if not HasFullControlPermission(user).has_permission():
        raise GraphQLError(constances.USER_NOT_SITE_ADMIN)


def assert_user_manager(user):
    if not HasUserManagementPermission(user).has_permission():
        raise GraphQLError(constances.USER_NOT_SITE_ADMIN)


def assert_access_request_permission(user):
    if not HasAccessRequestManagementPermission(user).has_permission():
        raise GraphQLError(constances.USER_NOT_SITE_ADMIN)


def assert_authenticated(user):
    if not user.is_authenticated:
        raise GraphQLError(constances.NOT_LOGGED_IN)


def assert_user_match_or_admin(acting_user, target_user):
    if not acting_user == target_user and not acting_user.is_site_admin:
        raise GraphQLError(constances.NOT_AUTHORIZED)


def assert_admin_permission(user, *perms):
    for permission_class in perms:
        if permission_class(user).has_permission():
            return
    raise GraphQLError(constances.NOT_AUTHORIZED)


def assert_can_create(user, model, group):
    try:
        assert model.can_add(user, group)
    except AssertionError:
        raise GraphQLError(constances.COULD_NOT_ADD)


def assert_can_edit_group(user, groupGuid):
    if groupGuid:
        try:
            group = Group.objects.get(id=groupGuid)
            if not group.can_write(user):
                raise GraphQLError(constances.NOT_AUTHORIZED)
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND_GROUP)


def assert_superadmin(user):
    if not user.is_staff:
        raise GraphQLError(constances.USER_NOT_SUPERADMIN)


def assert_is_profile_set_manager(user, profileSetGuid):
    if not user.profile_sets.filter(pk=profileSetGuid).exists():
        raise GraphQLError(constances.NOT_AUTHORIZED)


def resolve_entity_is_recommended_in_search(obj, info):
    return obj.is_recommended_in_search


def assert_delete_possible(entity, user):
    try:
        assert entity.can_delete(user), constances.COULD_NOT_DELETE
    except AttributeError:
        pass
    except AssertionError as e:
        raise GraphQLError(str(e))


def load_user(guid):
    try:
        return User.objects.get(id=guid)
    except User.DoesNotExist:
        raise GraphQLError(constances.COULD_NOT_FIND_USER)


def assert_write_access(entity, user):
    if not entity.can_write(user):
        raise GraphQLError(constances.COULD_NOT_SAVE)


def assert_archive_access(entity, user):
    if not entity.can_archive(user):
        raise GraphQLError(constances.COULD_NOT_SAVE)


def assert_not_attachment(entity):
    """
    File has no group or account references (anymore)
    """
    if entity._meta.label == "file.FileFolder" and not entity.root_container:
        raise GraphQLError(constances.COULD_NOT_SAVE)


def assert_group_member(user, group):
    if group and not group.is_full_member(user) and not user.is_site_admin:
        raise GraphQLError(constances.USER_NOT_MEMBER_OF_GROUP)


def assert_valid_title(title):
    if len(title) == 0 or len(title) > 256:
        raise GraphQLError(constances.INVALID_VALUE)


def assert_valid_abstract(abstract):
    if is_tiptap(abstract):
        text = (tiptap_to_text(abstract) or "").strip()
    else:
        text = html_to_text(abstract).strip()
    if len(text) > config.MAX_CHARACTERS_IN_ABSTRACT:
        raise GraphQLError(constances.TEXT_TOO_LONG)


def assert_isnt_me(left_user, right_user):
    if left_user == right_user:
        raise GraphQLError(constances.COULD_NOT_SAVE)


def assert_its_me(left_user, right_user_id):
    if left_user.guid != right_user_id:
        raise GraphQLError(constances.COULD_NOT_FIND)


def resolve_update_is_editable(profile_field, clean_input):
    if "isEditable" in clean_input:
        profile_field.is_editable_by_user = clean_input["isEditable"]


def resolve_update_is_filter(profile_field, clean_input):
    if "isFilter" in clean_input:
        profile_field.is_filter = clean_input["isFilter"]


def resolve_update_is_in_overview(profile_field, clean_input):
    if "isInOverview" in clean_input:
        profile_field.is_in_overview = clean_input["isInOverview"]


def resolve_update_is_on_v_card(profile_field, clean_input):
    if "isOnVcard" in clean_input:
        profile_field.is_on_vcard = clean_input["isOnVcard"]


def resolve_update_is_in_auto_group_membership(profile_field, clean_input):
    if "isInAutoGroupMembership" in clean_input:
        profile_field.is_in_auto_group_membership = clean_input[
            "isInAutoGroupMembership"
        ]


def resolve_update_is_in_onboarding(profile_field, clean_input):
    if "isInOnboarding" in clean_input:
        profile_field.is_in_onboarding = clean_input["isInOnboarding"]


def resolve_update_is_mandatory(profile_field, clean_input):
    if "isMandatory" in clean_input:
        profile_field.is_mandatory = clean_input["isMandatory"]


def resolve_update_autocomplete(profile_field, clean_input):
    if "autocomplete" in clean_input:
        profile_field.autocomplete = clean_input["autocomplete"]


def resolve_update_rows(entity, clean_input, user):
    try:
        if "rows" in clean_input:
            entity.row_repository = []
            for row in clean_input["rows"]:
                rowSerializer = RowSerializer(row, acting_user=user, writing=True)
                entity.row_repository.append(rowSerializer.serialize())
    except AttachmentVirusScanError as e:
        raise GraphQLError(constances.FILE_NOT_CLEAN.format(str(e)))


def post_upload_file(entity: FileFolder):
    if not config.PRESERVE_FILE_EXIF and entity.is_image():
        strip_exif(entity.upload)


def scan_file(file, delete_if_virus=False, delete_from_disk=False):
    if file.is_file() and not file.scan():
        filename = file.title
        if delete_from_disk:
            default_storage.delete(file.upload.name)
        if delete_if_virus:
            file.delete()
        raise GraphQLError(FILE_NOT_CLEAN.format(filename))


def assert_meetings_enabled():
    if not config.ONLINEAFSPRAKEN_ENABLED:
        raise GraphQLError(constances.MEETINGS_NOT_ENABLED)


def assert_videocall_enabled():
    if not config.VIDEOCALL_ENABLED:
        raise GraphQLError(constances.VIDEOCALL_NOT_ENABLED)


def assert_videocall_profilepage():
    if not config.VIDEOCALL_PROFILEPAGE:
        raise GraphQLError(constances.VIDEOCALL_PROFILEPAGE_NOT_AVAILABLE)


def assert_videocall_limit():
    last_hour = VideoCallGuest.objects.filter(
        created_at__gte=timezone.localtime() - timezone.timedelta(hours=1)
    ).count()
    if last_hour >= config.VIDEOCALL_THROTTLE:
        raise GraphQLError(constances.VIDEOCALL_LIMIT_REACHED)


def resolve_load_appointment_types():
    appointement_types = MeetingsApi().get_appointment_types()
    has_videocall = {
        s["id"]: s["hasVideocall"] for s in config.VIDEOCALL_APPOINTMENT_TYPE
    }
    return [
        {
            "id": t["Id"],
            "name": t["Name"] or gettext("Appointment"),
            "hasVideocall": has_videocall.get(t["Id"]) or False,
        }
        for t in appointement_types
    ]


def resolve_load_agendas():
    agendas = MeetingsApi().get_agendas()
    return [{"id": a["Id"], "name": a["Name"] or gettext("Agenda")} for a in agendas]


def copy_attachment_references(entity, parent=None, group=None):
    """
    Call this method when the parent or group of a file changes.
    """
    if not group and (not parent or parent.referenced_by.is_personal_file()):
        # it became a my-files file.
        entity.persist_file()
    else:
        # it became a group file.
        entity.referenced_by.personal_references().delete()

    if parent:
        for ref in {
            *parent.referenced_by.filter_entities(),
            *entity.referenced_by.filter_entities(),
        }:
            ref.container.update_attachments_links()


def obj_if_access(obj, info):
    try:
        acting_user = info.context["request"].user
        if obj.can_read(acting_user):
            return obj
    except AttributeError:
        # Can't test access on obj.
        return obj

    return None


def resolve_entity_video_call_enabled(obj, info):
    if EntityVideoCall.objects.filter(entity=obj).first():
        return True
    return False


def resolve_entity_video_call_moderators(obj, info):
    video_call = EntityVideoCall.objects.filter(entity=obj).first()
    if video_call:
        return video_call.get_moderators()
    return []


class ResolverRequiresPermissionBase:
    def __init__(self, permission, message=None):
        self.permission = permission
        self.message = message

    def __call__(self, f):
        raise NotImplementedError()

    def _test_for_permission(self, acting_user):
        for permission_class in self.permission:
            test = permission_class(acting_user)
            if test.has_permission():
                return True
        return False


class GuidOrPermissionMatchRequired(ResolverRequiresPermissionBase):
    def __call__(self, f):
        @wraps(f)
        def wrapper(obj, info, **kwargs):
            acting_user = info.context["request"].user
            if obj:
                if acting_user.is_authenticated and obj.guid == acting_user.guid:
                    return f(obj, info, **kwargs)
            if self._test_for_permission(acting_user):
                return f(obj, info, **kwargs)
            if self.message:
                raise GraphQLError(self.message)

        return wrapper


class PermissionRequired(ResolverRequiresPermissionBase):
    def __call__(self, f):
        @wraps(f)
        def wrapper(obj, info, **kwargs):
            acting_user = info.context["request"].user
            if self._test_for_permission(acting_user):
                return f(obj, info, **kwargs)
            if self.message:
                raise GraphQLError(self.message)

        return wrapper


def default_menu_access():
    return "public"


def resolve_toggle_archived_with_revision(entity, user, timestamp):
    """
    Toggle archived status and record the event if recording entity changes applies.
    """
    revision = resolve_start_revision(entity, user)
    resolve_toggle_is_archived(entity, timestamp)
    entity.save()
    store_update_revision(revision, entity)


def resolve_toggle_is_archived(entity, timestamp):
    """
    Toggle archived status.
    """
    entity.is_archived = not entity.is_archived
    if entity.is_archived:
        entity.schedule_archive_after = timestamp
    else:
        entity.schedule_archive_after = None


def resolve_toggle_archived_at_subpages(entity, user, timestamp):
    """
    Toggle archived status for all children in the hierarchy.
    """
    if ManageChangingArchiveDate.is_hierarchical_entity(entity):
        child_entities = [
            child
            for child in entity.get_all_children()
            if child.schedule_archive_after == entity.schedule_archive_after
            and child.is_archived == entity.is_archived
            and child.can_write(user)
        ]
        for child in child_entities:
            resolve_toggle_archived_with_revision(child, user, timestamp)


class ManageChangingArchiveDate:
    @staticmethod
    def is_hierarchical_entity(entity):
        is_hierarchical = isinstance(entity, HierarchicalEntityMixin)
        is_entity = isinstance(entity, Entity)
        return is_hierarchical and is_entity

    def __init__(self, entity, acting_user):
        self.entity = entity
        self.acting_user = acting_user

    def update_schedule_archive_date(self, new_date):
        if new_date and new_date <= timezone.now():
            self._manage_due_date(new_date)
        else:
            self._manage_future_date(new_date)

    def _manage_due_date(self, new_date):
        if self.entity.is_archived:
            # Already archived, nothing changes.
            return
        else:
            self._archive_entity(new_date)

    def _archive_entity(self, archive_after):
        # Archive entity and children immediately.
        resolve_toggle_archived_at_subpages(
            self.entity, self.acting_user, archive_after
        )
        self.entity.schedule_archive_after = archive_after
        self.entity.is_archived = True

    def _manage_future_date(self, new_date):
        if self.entity.is_archived:
            # self.entity should be de-archived, and get a new date
            # all children that have the same archive date should be
            # released. (clear archive date)
            self._release_children()
            self.entity.is_archived = False

        self.entity.schedule_archive_after = new_date

    def _release_children(self):
        if self.is_hierarchical_entity(self.entity):
            for child in self.entity.get_all_children():
                if (
                    child.is_archived
                    and child.schedule_archive_after
                    == self.entity.schedule_archive_after
                    and child.can_write(self.acting_user)
                ):
                    revision = resolve_start_revision(child, self.acting_user)
                    child.schedule_archive_after = None
                    child.is_archived = False
                    child.save()
                    store_update_revision(revision, child)


@resolve_obj_request
def resolve_entity_publish_request(obj, request):
    if request.user.is_authenticated:
        return obj.publish_requests.first()


def assert_can_upload_media(user, entity, clean_input):
    if (
        clean_input.get("richDescription")
        and entity.type_to_string == "question"
        and config.PREVENT_INSERT_MEDIA_AT_QUESTIONS
        and not (user.is_authenticated and user.is_site_admin)
    ):
        parser = Tiptap(clean_input["richDescription"])
        if len([*parser.get_media()]) > 0:
            raise GraphQLError(constances.COULD_NOT_SAVE)


@resolve_obj_request
# shared entity form resolvers
@decorators.resolve_obj
def resolve_entity_form_enabled(obj):
    return obj.is_form_enabled


@decorators.resolve_obj
def resolve_entity_form(obj):
    return obj.form_specs or None


def update_is_form_enabled(obj, clean_input):
    if "isFormEnabled" in clean_input:
        obj.is_form_enabled = clean_input.get("isFormEnabled")


def resolve_update_entity_form(obj, clean_input):
    if "form" in clean_input:
        obj.form_specs = EntityFormWrapper(clean_input["form"]).serialize()


def update_user_form(obj, user, fields, email=None):
    form_fields_updater = UpdateUserFormFieldCollection(user, email, obj)

    form_fields_updater.check_mandatory_fields(fields)

    for field in fields:
        form_fields_updater.add_field(field)

    if form_fields_updater.errors:
        return False, form_fields_updater.errors

    form_fields_updater.apply()
    return True, []


def resolve_update_background_color(entity, clean_input):
    if "backgroundColor" in clean_input:
        entity.background_color = clean_input.get("backgroundColor")


@decorators.resolve_obj
def resolve_entity_background_color(obj):
    return getattr(obj, "background_color", None)


def assert_valid_magazine_issue_container(acting_user, clean_input):
    try:
        if "containerGuid" not in clean_input:
            return
        issue = load_entity_by_id(
            clean_input["containerGuid"], ["magazine.MagazineIssue"]
        )
        if not issue.can_write(acting_user):
            raise GraphQLError(constances.NOT_AUTHORIZED)
    except ObjectDoesNotExist:
        raise GraphQLError(constances.COULD_NOT_ADD)


def resolve_add_entity_to_container(entity, clean_input):
    """
    After save the entity try to add it to a container.
    """
    try:
        if "containerGuid" not in clean_input:
            return
        container = load_entity_by_id(
            clean_input["containerGuid"], ["magazine.MagazineIssue"]
        )
        container.append_article(entity.id)
    except ObjectDoesNotExist:
        return


def get_user_match_fields(acting_user):
    yield "name^3"
    constraint = HasUserManagementPermission(acting_user)
    if constraint.has_permission():
        yield "email"


def resolve_update_entity_icon(entity, clean_input):
    if "iconGuid" in clean_input:
        try:
            if clean_input["iconGuid"]:
                entity.icon = FileFolder.objects.get(id=clean_input["iconGuid"])
            else:
                entity.icon = None
        except (FileFolder.DoesNotExist, ValidationError):
            raise GraphQLError(constances.COULD_NOT_FIND_ICON_FILE)
