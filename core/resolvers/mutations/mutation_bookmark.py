from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.models import Entity

mutation = ObjectType("Mutation")


@mutation.field("bookmark")
def resolve_bookmark(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        entity = Entity.objects.visible(user).get_subclass(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    bookmark = entity.get_bookmark(user)

    if not clean_input.get("isAdding"):
        if bookmark:
            bookmark.delete()
    else:
        if not bookmark:
            entity.add_bookmark(user)

    return {"object": entity}
