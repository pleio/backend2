from .mutation_accept_group_invitation import mutation as _accept_group_invitation
from .mutation_accept_membership_request import mutation as _accept_membership_request
from .mutation_add_attachment import mutation as _add_attachment
from .mutation_add_comment_without_account import (
    mutation as _add_comment_without_account,
)
from .mutation_add_entity import mutation as _add_entity
from .mutation_add_group import mutation as _add_group
from .mutation_add_site_setting_profile_field import (
    mutation as _add_site_setting_profile_field,
)
from .mutation_add_site_setting_profile_field_validator import (
    mutation as _add_site_setting_profile_field_validator,
)
from .mutation_add_subgroup import mutation as _add_subgroup
from .mutation_add_web_push_subscription import mutation as _add_web_push_subscription
from .mutation_archive_entity import mutation as _archive_entity
from .mutation_avatar_exports import mutation as _avatar_exports
from .mutation_bookmark import mutation as _bookmark
from .mutation_change_group_owner import mutation as _change_group_owner
from .mutation_comment_moderation import mutation as _comment_moderation
from .mutation_content_snapshots import mutation as _content_snapshots
from .mutation_copy_entity import mutation as _copy_entity
from .mutation_delete_entity import mutation as _delete_entity
from .mutation_delete_group_invitation import mutation as _delete_group_invitation
from .mutation_delete_site_setting_profile_field import (
    mutation as _delete_site_setting_profile_field,
)
from .mutation_delete_site_setting_profile_field_validator import (
    mutation as _delete_site_setting_profile_field_validator,
)
from .mutation_delete_subgroup import mutation as _delete_subgroup
from .mutation_delete_users import mutation as _delete_users
from .mutation_edit_email_overview import mutation as _edit_email_overview
from .mutation_edit_entity import mutation as _edit_entity
from .mutation_edit_group import mutation as _edit_group
from .mutation_edit_group_notifications import mutation as _edit_group_notifications
from .mutation_edit_notifications import mutation as _edit_notifications
from .mutation_edit_profile_field import mutation as _edit_profile_field
from .mutation_edit_site_setting import mutation as _edit_site_setting
from .mutation_edit_site_setting_profile_field import (
    mutation as _edit_site_setting_profile_field,
)
from .mutation_edit_site_setting_profile_field_validator import (
    mutation as _edit_site_setting_profile_field_validator,
)
from .mutation_edit_subgroup import mutation as _edit_subgroup
from .mutation_edit_user import mutation as _edit_user_name
from .mutation_edit_users import mutation as _edit_users
from .mutation_follow import mutation as _follow
from .mutation_handle_delete_account_request import (
    mutation as _handle_delete_account_request,
)
from .mutation_handle_site_access_request import mutation as _handle_site_access_request
from .mutation_import_users import mutation as _import_users
from .mutation_initiate_videocall import mutation as _initiate_videocall
from .mutation_invite_to_group import mutation as _invite_to_group
from .mutation_invite_to_site import mutation as _invite_to_site
from .mutation_join_group import mutation as _join_group
from .mutation_leave_group import mutation as _leave_group
from .mutation_mark_as_read import mutation as _mark_as_read
from .mutation_overwrite_site_owner import mutation as _overwrite_site_owner
from .mutation_profileset_manager import mutation as _profileset_manager
from .mutation_publish_request import mutation as _publish_request
from .mutation_reject_membership_request import mutation as _reject_membership_request
from .mutation_remove_group_members import mutation as _remove_group_members
from .mutation_reorder import mutation as _reorder
from .mutation_resend_group_invitation import mutation as _resend_group_invitation
from .mutation_revisions import mutation as _revisions
from .mutation_revoke_invite_to_site import mutation as _revoke_invite_to_site
from .mutation_schedule_appointment import mutation as _schedule_appointment
from .mutation_send_message_to_group import mutation as _send_message_to_group
from .mutation_send_message_to_user import mutation as _send_message_to_user
from .mutation_sign_site_agreement_version import (
    mutation as _sign_site_agreement_version,
)
from .mutation_tag_category import mutation as _tag_category
from .mutation_tags import mutation as _tags
from .mutation_toggle_entity_is_pinned import mutation as _toggle_entity_is_pinned
from .mutation_toggle_group_member_role import mutation as _toggle_group_member_role
from .mutation_toggle_is_chat_online_override import (
    mutation as _toggle_is_chat_online_override,
)
from .mutation_toggle_request_delete_user import mutation as _toggle_request_delete_user
from .mutation_toggle_suspend_admin_privileges import (
    mutation as _toggle_suspend_admin_privileges,
)
from .mutation_toggle_user_role import mutation as _toggle_user_role
from .mutation_update_user_widget_settings import (
    mutation as _update_user_widget_settings,
)
from .mutation_vote import mutation as _vote

resolvers = [
    _accept_group_invitation,
    _accept_membership_request,
    _add_attachment,
    _add_comment_without_account,
    _add_entity,
    _add_group,
    _add_site_setting_profile_field,
    _add_site_setting_profile_field_validator,
    _add_subgroup,
    _add_web_push_subscription,
    _archive_entity,
    _avatar_exports,
    _bookmark,
    _change_group_owner,
    _comment_moderation,
    _content_snapshots,
    _copy_entity,
    _delete_entity,
    _delete_group_invitation,
    _delete_site_setting_profile_field,
    _delete_site_setting_profile_field_validator,
    _delete_subgroup,
    _delete_users,
    _edit_email_overview,
    _edit_entity,
    _edit_group,
    _edit_group_notifications,
    _edit_notifications,
    _edit_profile_field,
    _edit_site_setting,
    _edit_site_setting_profile_field,
    _edit_site_setting_profile_field_validator,
    _edit_subgroup,
    _edit_user_name,
    _edit_users,
    _follow,
    _handle_delete_account_request,
    _handle_site_access_request,
    _import_users,
    _initiate_videocall,
    _invite_to_group,
    _invite_to_site,
    _join_group,
    _leave_group,
    _mark_as_read,
    _overwrite_site_owner,
    _profileset_manager,
    _publish_request,
    _reject_membership_request,
    _remove_group_members,
    _reorder,
    _resend_group_invitation,
    _revisions,
    _revoke_invite_to_site,
    _schedule_appointment,
    _send_message_to_group,
    _send_message_to_user,
    _sign_site_agreement_version,
    _tag_category,
    _tags,
    _toggle_entity_is_pinned,
    _toggle_group_member_role,
    _toggle_is_chat_online_override,
    _toggle_request_delete_user,
    _toggle_suspend_admin_privileges,
    _toggle_user_role,
    _update_user_widget_settings,
    _vote,
]
