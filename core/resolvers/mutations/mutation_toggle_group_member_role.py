import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_FIND,
    COULD_NOT_SAVE,
    GROUP_MEMBER_ROLES,
    NOT_LOGGED_IN,
    USER_NOT_MEMBER_OF_GROUP,
)
from core.lib import clean_graphql_input
from core.models import Group
from user.models import User

logger = logging.getLogger(__name__)

mutation = ObjectType("Mutation")


@mutation.field("toggleGroupMemberRole")
def resolve_toggle_group_member_role(_, info, input):  # noqa: C901
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not group.can_write(user):
        raise GraphQLError(COULD_NOT_SAVE)

    try:
        changing_user = User.objects.get(id=clean_input.get("userGuid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not group.is_member(changing_user):
        raise GraphQLError(USER_NOT_MEMBER_OF_GROUP)

    if clean_input.get("role") not in ["admin", "newsEditor"]:
        raise GraphQLError(COULD_NOT_SAVE)

    role_dict = {
        "admin": GROUP_MEMBER_ROLES.GROUP_ADMIN,
        "newsEditor": GROUP_MEMBER_ROLES.GROUP_NEWS_EDITOR,
    }

    membership = group.members.get(user=changing_user)

    def toggle_role(membership, role):
        if role in membership.roles:
            membership.roles = [x for x in membership.roles if x != role]
            membership.save()
        else:
            membership.roles.append(role)
            membership.save()

    if clean_input.get("role") == "admin":
        toggle_role(membership, role_dict["admin"])
    elif clean_input.get("role") == "newsEditor":
        toggle_role(membership, role_dict["newsEditor"])

    return {"group": group}
