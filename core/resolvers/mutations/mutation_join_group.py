from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import (
    ALREADY_MEMBER_OF_GROUP,
    COULD_NOT_FIND,
    GROUP_MEMBER_ROLES,
    NOT_LOGGED_IN,
)
from core.lib import clean_graphql_input
from core.mail_builders.group_access_request import schedule_group_access_request_mail
from core.models import Group

mutation = ObjectType("Mutation")


@mutation.field("joinGroup")
def resolve_join_group(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if group.is_full_member(user):
        raise GraphQLError(ALREADY_MEMBER_OF_GROUP)

    if group.can_join(user):
        group.join(user, "member")
    else:
        group.join(user, "pending")

        receiving_members = group.members.filter(
            roles__contains=[GROUP_MEMBER_ROLES.GROUP_ADMIN]
        ) | group.members.filter(user=group.owner)
        for receiving_member in list(set(receiving_members)):
            schedule_group_access_request_mail(
                user=user, receiver=receiving_member.user, group=group
            )

    return {"group": group}
