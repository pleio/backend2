from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.db import connection
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.resolvers import shared
from tenants.models import AgreementAccept, AgreementVersion, Client

mutation = ObjectType("Mutation")


@mutation.field("signSiteAgreementVersion")
def resolve_sign_site_agreement_version(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    try:
        agreement_version = AgreementVersion.objects.get(id=clean_input.get("id", None))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if agreement_version.accepted_for_current_tenant:
        msg = "already_accepted"
        raise GraphQLError(msg)

    if not clean_input.get("accept"):
        msg = "not_accepted"
        raise GraphQLError(msg)

    tenant = Client.objects.get(schema_name=connection.schema_name)

    AgreementAccept.objects.create(
        client=tenant,
        agreement_version=agreement_version,
        accept_name=user.name,
        accept_user_id=user.id,
    )

    return {"siteAgreementVersion": agreement_version}
