from ariadne import ObjectType

from core.lib import tenant_instance
from core.mail_builders.overwrite_site_owner_email import (
    schedule_overwrite_site_owner_notification as send_mail,
)
from core.resolvers import decorators, shared

mutation = ObjectType("Mutation")


@mutation.field("overwriteSiteOwner")
@decorators.resolver_with_request
def resolve_overwrite_site_owner(request, **kwargs):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    tenant = tenant_instance()
    current_owner = tenant.administrative_details.get("site_owner_email")

    tenant.administrative_details = {
        **tenant.administrative_details,
        "site_owner": kwargs["input"]["siteOwnerName"],
        "site_owner_email": kwargs["input"]["siteOwnerEmail"],
    }
    tenant.save()

    if current_owner:
        send_mail(
            sender=request.user,
            current_owner=current_owner,
        )
    # SiteSettings object.
    return {}
