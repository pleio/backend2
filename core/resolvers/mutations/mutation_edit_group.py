import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.models import Group
from core.resolvers import group_shared, shared
from core.utils.auto_member_profile_field import AutoMemberProfileFieldAtGroup

logger = logging.getLogger(__name__)
mutation = ObjectType("Mutation")


@mutation.field("editGroup")
def resolve_edit_group(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(
        input,
        [
            "iconGuid",
            "defaultTags",
            "defaultTagCategories",
            "isAutoMembershipEnabled",
            "widgets",
        ],
    )

    shared.assert_authenticated(user)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    auto_membership_fields = AutoMemberProfileFieldAtGroup(group)

    shared.assert_write_access(group, user)

    group_shared.update_name(group, clean_input)
    shared.resolve_update_entity_icon(group, clean_input)
    shared.update_featured_image(group, clean_input)
    shared.resolve_update_rich_description(group, clean_input)
    shared.resolve_update_introduction(group, clean_input)
    shared.resolve_update_tags(group, clean_input)

    if "defaultTags" in clean_input:
        group.content_presets["defaultTags"] = clean_input["defaultTags"] or []
    if "defaultTagCategories" in clean_input:
        group.content_presets["defaultTagCategories"] = (
            clean_input["defaultTagCategories"] or []
        )

    group_shared.update_is_introduction_public(group, clean_input)
    group_shared.update_welcome_message(group, clean_input)
    group_shared.update_required_profile_fields_message(group, clean_input)
    group_shared.update_is_closed(group, clean_input)
    group_shared.update_is_membership_on_request(group, clean_input)
    group_shared.update_auto_notification(group, clean_input)
    group_shared.update_plugins(group, clean_input)
    group_shared.update_menu(group, clean_input)
    group_shared.update_show_member_profile_fields(group, clean_input)
    group_shared.update_required_profile_fields(group, clean_input)
    group_shared.update_is_chat_enabled(group, clean_input)
    group_shared.update_start_page(group, clean_input)
    group_shared.update_file_notifications(group, clean_input)
    group_shared.update_is_join_button_hidden(group, clean_input)
    group_shared.update_auto_membership_fields(group, clean_input)
    group_shared.update_is_menu_always_visible(group, clean_input)

    if user.is_site_admin:
        shared.update_is_featured(group, user, clean_input)
        group_shared.update_is_leaving_group_disabled(group, clean_input)
        group_shared.update_is_auto_membership_enabled(group, clean_input)
        group_shared.update_is_hidden(group, clean_input)

    shared.update_updated_at(group)

    group.save()
    auto_membership_fields.apply()

    return {"group": group}
