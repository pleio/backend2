from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.mail_builders.user_delete_complete import schedule_user_delete_complete_mail
from core.resolvers import shared
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("deleteUsers")
def resolve_delete_users(_, info, input):
    performing_user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(performing_user)
    shared.assert_administrator(performing_user)

    try:
        users = User.objects.filter(id__in=clean_input.get("guids"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    # first check if superadmin deleted by superadmin
    if users.filter(is_superadmin=True):
        shared.assert_superadmin(performing_user)

    for user in users:
        # TODO: Is dit echt dubbele code?
        is_deleted_user_admin = user.is_site_admin
        user_mailinfo = user.as_mailinfo()
        user.delete()

        schedule_user_delete_complete_mail(
            user_info=user_mailinfo,
            receiver_info=user_mailinfo,
            sender=performing_user,
            to_admin=False,
        )

        # Send email to admins if user which is deleted is also an admin
        if is_deleted_user_admin:
            admin_users = User.objects.filter(roles__contains=["ADMIN"])
            for admin_user in admin_users:
                schedule_user_delete_complete_mail(
                    user_info=user_mailinfo,
                    receiver_info=admin_user.as_mailinfo(),
                    sender=performing_user,
                    to_admin=True,
                )

    return {"success": True}
