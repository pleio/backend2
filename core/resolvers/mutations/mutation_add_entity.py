from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_ADD, COULD_NOT_FIND, INVALID_SUBTYPE
from core.lib import clean_graphql_input
from core.models import Comment, CommentMixin, Entity
from core.resolvers import shared
from entities.activity.resolvers.mutation import resolve_add_status_update
from entities.blog.resolvers.mutation import resolve_add_blog
from entities.discussion.resolvers.mutation import resolve_add_discussion
from entities.event.resolvers.mutation_event_add import resolve_add_event
from entities.file.resolvers.mutation import resolve_add_folder
from entities.magazine.resolvers.mutations.issue import resolve_add_magazine_issue
from entities.magazine.resolvers.mutations.magazine import resolve_add_magazine
from entities.news.resolvers.mutation import resolve_add_news
from entities.question.resolvers.mutation import resolve_add_question
from entities.task.resolvers.mutation import resolve_add_task
from entities.wiki.resolvers.mutation import resolve_add_wiki

mutation = ObjectType("Mutation")


@mutation.field("addEntity")
def resolve_add_entity(_, info, input):  # noqa: C901
    clean_input = clean_graphql_input(input)
    request = info.context["request"]

    if clean_input.get("subtype") == "blog":
        return resolve_add_blog(_, info, input)

    if clean_input.get("subtype") == "comment":
        return resolve_add_comment(_, info, input)

    if clean_input.get("subtype") == "event":
        return resolve_add_event(_, info, input)

    if clean_input.get("subtype") == "discussion":
        return resolve_add_discussion(_, info, input)

    if clean_input.get("subtype") == "statusupdate":
        return resolve_add_status_update(_, info, input)

    if clean_input.get("subtype") == "task":
        return resolve_add_task(_, info, input)

    if clean_input.get("subtype") == "folder":
        return resolve_add_folder(_, info, input)

    if clean_input.get("subtype") == "wiki":
        return resolve_add_wiki(_, info, input)

    if clean_input.get("subtype") == "news":
        return resolve_add_news(_, info, input)

    if clean_input.get("subtype") == "question":
        return resolve_add_question(_, info, input)

    if clean_input.get("subtype") == "magazine_issue":
        return resolve_add_magazine_issue(request, input)

    if clean_input.get("subtype") == "magazine":
        return resolve_add_magazine(request, input)

    raise GraphQLError(INVALID_SUBTYPE)


def resolve_add_comment(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_graphql(bool(clean_input.get("containerGuid")), "NO_CONTAINER_GUID")

    container = get_container(clean_input.get("containerGuid"))

    shared.assert_graphql(bool(container), COULD_NOT_FIND)
    shared.assert_is_subclass(container, CommentMixin, COULD_NOT_ADD)
    shared.assert_graphql(container.can_comment(user), COULD_NOT_ADD)
    shared.assert_can_upload_media(user, _get_root_container(container), input)

    comment = Comment.objects.create(
        container=container,
        owner=user,
        rich_description=clean_input.get("richDescription"),
    )

    return {"entity": comment}


def get_container(guid):
    try:
        return Entity.objects.get_subclass(id=guid)
    except ObjectDoesNotExist:
        try:
            return Comment.objects.get(id=guid)
        except ObjectDoesNotExist:
            pass


def _get_root_container(container):
    try:
        return container.get_root_container()
    except AttributeError:
        return container
