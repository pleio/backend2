from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE, NOT_LOGGED_IN
from core.models import Entity
from core.resolvers import shared

mutation = ObjectType("Mutation")


@mutation.field("toggleEntityArchived")
def resolve_toggle_archive_entity(_, info, guid):
    user = info.context["request"].user

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        entity = Entity.objects.get_subclass(id=guid)
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if entity.is_archived:
        shared.assert_write_access(entity, user)
    else:
        shared.assert_archive_access(entity, user)

    # Shared time property to group archived entities.
    timestamp = timezone.now()

    # Do the toggle for the subpages first.
    shared.resolve_toggle_archived_at_subpages(entity, user, timestamp)

    # Then do the toggle for the entity.
    shared.resolve_toggle_archived_with_revision(entity, user, timestamp)

    return {"success": True}


@mutation.field("archiveEntities")
def resolve_archive_multiple_entities(_, info, input):
    user = info.context["request"].user

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    guids = input.get("guids", [])

    entities = Entity.objects.filter(
        id__in=guids, is_archived=False
    ).select_subclasses()

    for entity in entities:
        if not entity.can_archive(user):
            raise GraphQLError(COULD_NOT_SAVE)

    # Shared time property to group archived entities.
    timestamp = timezone.now()

    for entity in entities:
        # Do the toggle for the child entities first.
        shared.resolve_toggle_archived_at_subpages(entity, user, timestamp)

        # Then toggle the parent entity.
        shared.resolve_toggle_archived_with_revision(entity, user, timestamp)

    return {"edges": Entity.objects.filter(id__in=guids).select_subclasses()}
