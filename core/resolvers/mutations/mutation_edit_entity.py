from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, INVALID_SUBTYPE, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.models import Comment, Entity
from core.resolvers import decorators, shared
from entities.activity.resolvers.mutation import resolve_edit_status_update
from entities.blog.resolvers.mutation import resolve_edit_blog
from entities.discussion.resolvers.mutation import resolve_edit_discussion
from entities.event.resolvers.mutation_event_edit import resolve_edit_event
from entities.magazine.resolvers.mutations.issue import resolve_edit_magazine_issue
from entities.magazine.resolvers.mutations.magazine import resolve_edit_magazine
from entities.news.resolvers.mutation import resolve_edit_news
from entities.question.resolvers.mutation import resolve_edit_question
from entities.task.resolvers.mutation import resolve_edit_task
from entities.wiki.resolvers.mutation import resolve_edit_wiki

mutation = ObjectType("Mutation")


@mutation.field("editEntity")
@decorators.add_exception_debug_backtrace
def resolve_edit_entity(_, info, input):  # noqa: C901
    clean_input = clean_graphql_input(input)
    request = info.context["request"]

    if not info.context["request"].user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        entity = Entity.objects.get_subclass(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        # TODO: update frontend to use editComment
        return resolve_edit_comment(_, info, input)

    if entity._meta.model_name == "blog":
        return resolve_edit_blog(_, info, input)

    if entity._meta.model_name == "event":
        return resolve_edit_event(_, info, input)

    if entity._meta.model_name == "discussion":
        return resolve_edit_discussion(_, info, input)

    if entity._meta.model_name == "statusupdate":
        return resolve_edit_status_update(_, info, input)

    if entity._meta.model_name == "task":
        return resolve_edit_task(_, info, input)

    if entity._meta.model_name == "wiki":
        return resolve_edit_wiki(_, info, input)

    if entity.type_to_string == "magazine":
        return resolve_edit_magazine(request, input)

    if entity.type_to_string == "magazine_issue":
        return resolve_edit_magazine_issue(request, input)

    if entity._meta.model_name == "news":
        return resolve_edit_news(_, info, input)

    if entity._meta.model_name == "question":
        return resolve_edit_question(_, info, input)

    raise GraphQLError(INVALID_SUBTYPE)


def resolve_edit_comment(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        comment = Comment.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    shared.assert_write_access(comment, user)
    shared.assert_can_upload_media(user, comment.container, clean_input)

    shared.resolve_update_rich_description(comment, clean_input)

    shared.update_updated_at(comment)
    comment.save()

    return {"entity": comment}
