from ariadne import ObjectType

from core.resolvers import decorators
from core.resolvers.queries.query_viewer import user_viewer_adapter

mutation = ObjectType("Mutation")


@mutation.field("toggleSuspendAdminPrivileges")
@decorators.resolver_with_request
def resolve_toggle_suspend_admin_privileges(request):
    acting_user = request.user

    if getattr(acting_user, "can_suspend_admin_privileges", False):
        acting_user.suspend_admin_privileges = not acting_user.suspend_admin_privileges
        acting_user.save()

    return user_viewer_adapter(request)
