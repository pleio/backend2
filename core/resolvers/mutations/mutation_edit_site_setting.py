import ipaddress
import logging

from ariadne import ObjectType
from django.core.cache import cache
from django.db import connection
from django.utils import timezone
from django_tenants.utils import parse_tenant_config_path
from graphql import GraphQLError

from core import config
from core.constances import (
    ACCESS_TYPE,
    COULD_NOT_SAVE,
    INVALID_VALUE,
    PROTECTED_CONTENT_TYPES,
    REDIRECTS_HAS_DUPLICATE_SOURCE,
    REDIRECTS_HAS_LOOP,
    ConfigurationChoices,
)
from core.lib import (
    clean_graphql_input,
    get_language_options,
    is_full_url,
    is_valid_domain,
    is_valid_url_or_path,
)
from core.mail_builders.site_access_changed import schedule_site_access_changed_mail
from core.models import ProfileField, Setting
from core.models.user import validate_profile_sections
from core.resolvers import shared
from entities.file.helpers.images import resize_and_save_as_png
from entities.file.models import FileFolder, FileReference
from user.models import User

mutation = ObjectType("Mutation")
logger = logging.getLogger(__name__)


def get_or_create_profile_field(test_key, initial_name):
    try:
        # Valideren
        return ProfileField.objects.get(key=test_key)
    except ProfileField.DoesNotExist:
        return ProfileField.objects.create(key=test_key, name=initial_name)


def save_setting(key, value):
    setting, created = Setting.objects.get_or_create(key=key)
    setting.value = value
    setting.save()
    cache.set("%s%s" % (connection.schema_name, key), value)


def get_menu_item(menu, item, depth=0):
    children = get_menu_children(menu, item["id"], depth)
    access = item.get("access")

    return {
        "label": item["label"],
        "translations": [*_get_menu_translations(item)],
        "link": item["link"],
        "children": children,
        "access": access,
    }


def _get_menu_translations(item):
    yield {"language": config.LANGUAGE, "label": item.get("label") or ""}
    for translation in item.get("translations") or []:
        if translation["language"] != config.LANGUAGE:
            yield translation


def get_menu_children(menu, item_id, depth=0):
    if depth == 3:
        return []
    depth = depth + 1

    children = []
    for item in menu:
        if item["parentId"] == item_id:
            children.append(get_menu_item(menu, item, depth))

    return children


def validate_redirects(redirects):
    sources = []
    destinations = []
    redirects_dict = {}

    # check no more than 2000 redirects
    if len(redirects) > 2000:
        raise GraphQLError(INVALID_VALUE)

    for redirect in redirects:
        source = redirect["source"]
        destination = redirect["destination"]
        sources.append(source)
        destinations.append(destination)
        if not is_valid_url_or_path(source) or not is_valid_url_or_path(destination):
            raise GraphQLError(INVALID_VALUE)

        # save redirects as dict, because source can not be duplicate
        try:
            redirects_dict[source] = destination
        except Exception:
            raise GraphQLError(REDIRECTS_HAS_DUPLICATE_SOURCE)

    # check if loop can occur
    if any(x in sources for x in destinations):
        raise GraphQLError(REDIRECTS_HAS_LOOP)
    return redirects_dict


@mutation.field("editSiteSetting")
def resolve_edit_site_setting(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    setting_keys = {
        "name": "NAME",
        "description": "DESCRIPTION",
        "allowRegistration": "ALLOW_REGISTRATION",
        "defaultReadAccessId": "DEFAULT_ACCESS_ID",
        "footerPageEnabled": "FOOTER_PAGE_ENABLED",
        "searchEngineIndexingEnabled": "ENABLE_SEARCH_ENGINE_INDEXING",
        "piwikUrl": "PIWIK_URL",
        "piwikId": "PIWIK_ID",
        "theme": "THEME",
        "logoAlt": "LOGO_ALT",
        "likeIcon": "LIKE_ICON",
        "fontHeading": "FONT",
        "fontBody": "FONT_BODY",
        "colorPrimary": "COLOR_PRIMARY",
        "colorSecondary": "COLOR_SECONDARY",
        "colorHeader": "COLOR_HEADER",
        "startPage": "STARTPAGE",
        "startPageCms": "STARTPAGE_CMS",
        "anonymousStartPage": "ANONYMOUS_START_PAGE",
        "anonymousStartPageCms": "ANONYMOUS_START_PAGE_CMS",
        "showIcon": "ICON_ENABLED",
        "iconAlt": "ICON_ALT",
        "menuState": "MENU_STATE",
        "numberOfFeaturedItems": "NUMBER_OF_FEATURED_ITEMS",
        "enableFeedSorting": "ENABLE_FEED_SORTING",
        "showExtraHomepageFilters": "ACTIVITY_FEED_FILTERS_ENABLED",
        "showLeader": "LEADER_ENABLED",
        "showLeaderButtons": "LEADER_BUTTONS_ENABLED",
        "leaderImage": "LEADER_IMAGE",
        "subtitle": "SUBTITLE",
        "showInitiative": "INITIATIVE_ENABLED",
        "initiativeTitle": "INITIATIVE_TITLE",
        "initiativeDescription": "INITIATIVE_DESCRIPTION",
        "initiativeImage": "INITIATIVE_IMAGE",
        "initiativeImageAlt": "INITIATIVE_IMAGE_ALT",
        "initiativeLink": "INITIATIVE_LINK",
        "directLinks": "DIRECT_LINKS",
        "footer": "FOOTER",
        "showTagsInFeed": "SHOW_TAGS_IN_FEED",
        "showTagsInDetail": "SHOW_TAGS_IN_DETAIL",
        "showCustomTagsInFeed": "SHOW_CUSTOM_TAGS_IN_FEED",
        "showCustomTagsInDetail": "SHOW_CUSTOM_TAGS_IN_DETAIL",
        "defaultEmailOverviewFrequency": "EMAIL_OVERVIEW_DEFAULT_FREQUENCY",
        "emailOverviewSubject": "EMAIL_OVERVIEW_SUBJECT",
        "emailOverviewTitle": "EMAIL_OVERVIEW_TITLE",
        "emailOverviewIntro": "EMAIL_OVERVIEW_INTRO",
        "emailOverviewEnableFeatured": "EMAIL_OVERVIEW_ENABLE_FEATURED",
        "emailOverviewFeaturedTitle": "EMAIL_OVERVIEW_FEATURED_TITLE",
        "emailNotificationShowExcerpt": "EMAIL_NOTIFICATION_SHOW_EXCERPT",
        "showLoginRegister": "SHOW_LOGIN_REGISTER",
        "customTagsAllowed": "CUSTOM_TAGS_ENABLED",
        "showUpDownVoting": "SHOW_UP_DOWN_VOTING",
        "enableSharing": "ENABLE_SHARING",
        "showViewsCount": "SHOW_VIEW_COUNT",
        "newsletter": "NEWSLETTER",
        "cancelMembershipEnabled": "CANCEL_MEMBERSHIP_ENABLED",
        "showExcerptInNewsCard": "SHOW_EXCERPT_IN_NEWS_CARD",
        "commentsOnNews": "COMMENT_ON_NEWS",
        "hideContentOwner": "HIDE_CONTENT_OWNER",
        "eventExport": "EVENT_EXPORT",
        "eventTiles": "EVENT_TILES",
        "questionerCanChooseBestAnswer": "QUESTIONER_CAN_CHOOSE_BEST_ANSWER",
        "statusUpdateGroups": "STATUS_UPDATE_GROUPS",
        "subgroups": "SUBGROUPS",
        "groupMemberExport": "GROUP_MEMBER_EXPORT",
        "limitedGroupAdd": "LIMITED_GROUP_ADD",
        "showSuggestedItems": "SHOW_SUGGESTED_ITEMS",
        "onboardingEnabled": "ONBOARDING_ENABLED",
        "onboardingForceExistingUsers": "ONBOARDING_FORCE_EXISTING_USERS",
        "onboardingIntro": "ONBOARDING_INTRO",
        "profileSyncEnabled": "PROFILE_SYNC_ENABLED",
        "profileSyncToken": "PROFILE_SYNC_TOKEN",
        "cookieConsent": "COOKIE_CONSENT",
        "loginIntro": "LOGIN_INTRO",
        "siteMembershipAcceptedSubject": "SITE_MEMBERSHIP_ACCEPTED_SUBJECT",
        "siteMembershipAcceptedIntro": "SITE_MEMBERSHIP_ACCEPTED_INTRO",
        "siteMembershipDeniedSubject": "SITE_MEMBERSHIP_DENIED_SUBJECT",
        "siteMembershipDeniedIntro": "SITE_MEMBERSHIP_DENIED_INTRO",
        "idpId": "IDP_ID",
        "idpName": "IDP_NAME",
        "autoApproveSSO": "AUTO_APPROVE_SSO",
        "require2FA": "REQUIRE_2FA",
        "flowEnabled": "FLOW_ENABLED",
        "flowSubtypes": "FLOW_SUBTYPES",
        "flowAppUrl": "FLOW_APP_URL",
        "flowToken": "FLOW_TOKEN",
        "flowCaseId": "FLOW_CASE_ID",
        "flowUserGuid": "FLOW_USER_GUID",
        "commentWithoutAccountEnabled": "COMMENT_WITHOUT_ACCOUNT_ENABLED",
        "questionLockAfterActivity": "QUESTION_LOCK_AFTER_ACTIVITY",
        "questionLockAfterActivityLink": "QUESTION_LOCK_AFTER_ACTIVITY_LINK",
        "kalturaVideoEnabled": "KALTURA_VIDEO_ENABLED",
        "kalturaVideoPartnerId": "KALTURA_VIDEO_PARTNER_ID",
        "kalturaVideoPlayerId": "KALTURA_VIDEO_PLAYER_ID",
        "pdfCheckerEnabled": "PDF_CHECKER_ENABLED",
        "securityText": "SECURITY_TEXT",
        "securityTextEnabled": "SECURITY_TEXT_ENABLED",
        "securityTextPGP": "SECURITY_TEXT_PGP",
        "securityTextRedirectEnabled": "SECURITY_TEXT_REDIRECT_ENABLED",
        "preserveFileExif": "PRESERVE_FILE_EXIF",
        "searchPublishedFilterEnabled": "SEARCH_PUBLISHED_FILTER_ENABLED",
        "searchArchiveOption": "SEARCH_ARCHIVE_OPTION",
        "searchExcludedContentTypes": "SEARCH_EXCLUDED_CONTENT_TYPES",
        "recommendedType": "RECOMMENDED_TYPE",
        "appointmentTypeVideocall": "VIDEOCALL_APPOINTMENT_TYPE",
        "blockedUserIntroMessage": "BLOCKED_USER_INTRO_MESSAGE",
        "pushNotificationsEnabled": "PUSH_NOTIFICATIONS_ENABLED",
        "pageTagFilters": "PAGE_TAG_FILTERS",
        "collabEditingEnabled": "COLLAB_EDITING_ENABLED",
        "hideAccessLevelSelect": "HIDE_ACCESS_LEVEL_SELECT",
        "showOriginalFileName": "SHOW_ORIGINAL_FILE_NAME",
    }

    resolve_update_keys(setting_keys, clean_input)
    resolve_update_menu(clean_input)
    resolve_update_profile(clean_input)
    resolve_update_redirects(clean_input)
    resolve_update_security_text_redirect_url(clean_input)
    resolve_update_logo(user, clean_input)
    resolve_update_remove_logo(clean_input)
    resolve_update_icon(user, clean_input)
    resolve_update_remove_icon(clean_input)
    resolve_update_favicon(user, clean_input)
    resolve_update_remove_favicon(clean_input)
    resolve_update_direct_registration_domains(clean_input)
    resolve_update_profile_sections(clean_input)
    resolve_update_custom_css(clean_input)
    resolve_update_walled_garden_by_ip_enabled(clean_input)
    resolve_update_white_listed_ip_ranges(clean_input)
    resolve_update_language(clean_input)
    resolve_update_file_description_field_enabled(clean_input)
    resolve_update_is_closed(user, clean_input)
    resolve_update_max_characters_in_abstract(clean_input)
    resolve_update_content_moderation_for(clean_input)
    resolve_update_comment_moderation_for(clean_input)

    resolve_sync_sitename(clean_input)
    resolve_update_open_for_create_content_types(clean_input)

    return {"siteSettings": {}}


# Sub-resolvers:


def resolve_update_menu(clean_input):
    if "menu" in clean_input:
        menu = []
        for item in clean_input.get("menu"):
            if item["parentId"] is None:
                menu.append(get_menu_item(clean_input.get("menu"), item))
        save_setting("MENU", menu)


def resolve_update_profile(clean_input):
    if "profile" in clean_input:
        for field in clean_input.get("profile"):
            profile_field = get_or_create_profile_field(field["key"], field["name"])
            profile_field.name = field["name"]
            profile_field.is_filter = field.get("isFilter") or False
            profile_field.is_in_overview = field.get("isInOverview") or False
            profile_field.is_on_vcard = field.get("isOnVcard") or False
            profile_field.is_in_auto_group_membership = (
                field.get("isInAutoGroupMembership") or False
            )
            profile_field.save()

        save_setting("PROFILE", clean_input.get("profile"))


def resolve_update_redirects(clean_input):
    if "redirects" in clean_input:
        redirects = validate_redirects(clean_input.get("redirects"))
        save_setting("REDIRECTS", redirects)


def resolve_update_security_text_redirect_url(clean_input):
    if "securityTextRedirectUrl" in clean_input:
        url = clean_input.get("securityTextRedirectUrl")
        if not is_full_url(url):
            raise GraphQLError(INVALID_VALUE)
        save_setting("SECURITY_TEXT_REDIRECT_URL", url)


def create_configuration_file(upload_name, configuration_name, owner):
    try:
        file = FileFolder.objects.create(
            owner=owner,
            upload=upload_name,
            write_access=[ACCESS_TYPE.user.format(owner.guid)],
        )
        shared.scan_file(file, delete_if_virus=True)
        shared.post_upload_file(file)
        FileReference.objects.update_configuration(configuration_name, file)
        return file
    except GraphQLError:
        raise
    except Exception:
        raise GraphQLError(COULD_NOT_SAVE)


def resolve_update_logo(user, clean_input):
    if "logo" in clean_input:
        if not clean_input.get("logo"):
            msg = "NO_FILE"
            raise GraphQLError(msg)

        logo = create_configuration_file(
            clean_input.get("logo"),
            configuration_name=ConfigurationChoices.LOGO.name,
            owner=user,
        )
        save_setting("LOGO", logo.embed_url)


def resolve_update_remove_logo(clean_input):
    if "removeLogo" in clean_input:
        try:
            FileFolder.objects.file_by_path(config.LOGO).delete()
        except Exception:
            pass
        save_setting("LOGO", "")


def resolve_update_icon(user, clean_input):
    if "icon" in clean_input:
        if not clean_input.get("icon"):
            msg = "NO_FILE"
            raise GraphQLError(msg)

        icon = create_configuration_file(
            clean_input.get("icon"),
            configuration_name=ConfigurationChoices.ICON.name,
            owner=user,
        )

        save_setting("ICON", icon.embed_url)


def resolve_update_remove_icon(clean_input):
    if "removeIcon" in clean_input:
        try:
            FileFolder.objects.file_by_path(config.ICON).delete()
        except Exception:
            pass
        save_setting("ICON", "")


def resolve_update_favicon(user, clean_input):
    if "favicon" in clean_input:
        if not clean_input.get("favicon"):
            msg = "NO_FILE"
            raise GraphQLError(msg)

        favicon = create_configuration_file(
            clean_input.get("favicon"),
            configuration_name=ConfigurationChoices.FAVICON.name,
            owner=user,
        )
        resize_and_save_as_png(favicon, 512, 512)
        save_setting("FAVICON", favicon.download_url)


def resolve_update_remove_favicon(clean_input):
    if "removeFavicon" in clean_input:
        try:
            FileFolder.objects.file_by_path(config.FAVICON).delete()
        except Exception:
            pass
        save_setting("FAVICON", "")


def resolve_update_direct_registration_domains(clean_input):
    if "directRegistrationDomains" in clean_input:
        for domain in clean_input.get("directRegistrationDomains"):
            if not is_valid_domain(domain):
                raise GraphQLError(INVALID_VALUE)
        save_setting(
            "DIRECT_REGISTRATION_DOMAINS", clean_input.get("directRegistrationDomains")
        )


def resolve_update_profile_sections(clean_input):
    if "profileSections" in clean_input:
        save_setting(
            "PROFILE_SECTIONS",
            validate_profile_sections(clean_input.get("profileSections")),
        )


def resolve_update_custom_css(clean_input):
    if "customCss" in clean_input:
        save_setting("CUSTOM_CSS", clean_input.get("customCss"))
        save_setting("CUSTOM_CSS_TIMESTAMP", int(timezone.now().timestamp()))


def resolve_update_walled_garden_by_ip_enabled(clean_input):
    if "walledGardenByIpEnabled" in clean_input:
        save_setting(
            "WALLED_GARDEN_BY_IP_ENABLED", clean_input.get("walledGardenByIpEnabled")
        )

        # if walled garden by ip is enabled, turn of indexing
        if clean_input.get("walledGardenByIpEnabled"):
            save_setting("ENABLE_SEARCH_ENGINE_INDEXING", False)


def resolve_update_white_listed_ip_ranges(clean_input):
    if "whitelistedIpRanges" in clean_input:
        for ip_range in clean_input.get("whitelistedIpRanges"):
            try:
                ipaddress.ip_network(ip_range)
            except ValueError:
                raise GraphQLError(INVALID_VALUE)
        save_setting("WHITELISTED_IP_RANGES", clean_input.get("whitelistedIpRanges"))


def resolve_update_language(clean_input):
    if "language" in clean_input:
        save_setting("LANGUAGE", clean_input["language"])

    if "extraLanguages" in clean_input:
        language_options = [option["value"] for option in get_language_options()]

        def extra_languages():
            for language in clean_input["extraLanguages"]:
                if language not in language_options:
                    raise GraphQLError(INVALID_VALUE)
                if language == config.LANGUAGE:
                    continue
                yield language

        save_setting("EXTRA_LANGUAGES", [*extra_languages()])


def resolve_update_file_description_field_enabled(clean_input):
    if "fileDescriptionFieldEnabled" in clean_input:
        options = [m for m in config.FILE_OPTIONS if m != "enable_file_description"]
        if clean_input.get("fileDescriptionFieldEnabled"):
            options.append("enable_file_description")
        save_setting("FILE_OPTIONS", options)


def resolve_update_is_closed(user, clean_input):
    if "isClosed" in clean_input:
        if clean_input.get("isClosed") != config.IS_CLOSED:
            # mail to admins to notify about site access change
            for admin_user in User.objects.filter(roles__contains=["ADMIN"]):
                schedule_site_access_changed_mail(
                    is_closed=clean_input.get("isClosed"), admin=admin_user, sender=user
                )
            save_setting("IS_CLOSED", clean_input.get("isClosed"))


def resolve_update_keys(setting_keys, clean_input):
    for k, v in setting_keys.items():
        if k in clean_input:
            save_setting(v, clean_input.get(k))


def resolve_sync_sitename(clean_input):
    if {"favicon", "name", "description"} & set(clean_input.keys()):
        from concierge.tasks import sync_site

        sync_site.delay(parse_tenant_config_path(""))


def resolve_update_max_characters_in_abstract(clean_input):
    if "maxCharactersInAbstract" in clean_input:
        if int(clean_input.get("maxCharactersInAbstract")) > 1000:
            raise GraphQLError(INVALID_VALUE)
        save_setting(
            "MAX_CHARACTERS_IN_ABSTRACT", clean_input.get("maxCharactersInAbstract")
        )


def resolve_update_open_for_create_content_types(clean_input):
    if "openForCreateContentTypes" in clean_input:
        save_setting(
            "OPEN_FOR_CREATE_CONTENT_TYPES",
            [
                t
                for t in clean_input.get("openForCreateContentTypes")
                if t not in PROTECTED_CONTENT_TYPES
            ],
        )


def resolve_update_content_moderation_for(clean_input):
    ALLOWED_TYPES = [
        "blog",
        "discussion",
        "event",
        "file",
        "news",
        "episode",
        "podcast",
        "question",
        "wiki",
    ]

    if "requireContentModerationFor" in clean_input:
        save_setting(
            "REQUIRE_CONTENT_MODERATION_FOR",
            [
                m
                for m in clean_input.get("requireContentModerationFor")
                if m in ALLOWED_TYPES
            ],
        )


def resolve_update_comment_moderation_for(clean_input):
    ALLOWED_TYPES = [
        "blog",
        "discussion",
        "event",
        "news",
        "question",
    ]

    if "requireCommentModerationFor" in clean_input:
        save_setting(
            "REQUIRE_COMMENT_MODERATION_FOR",
            [
                m
                for m in clean_input.get("requireCommentModerationFor")
                if m in ALLOWED_TYPES
            ],
        )
