from typing import TYPE_CHECKING

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core import constances
from core.constances import INVALID_SUBTYPE
from core.lib import clean_graphql_input
from core.utils.entity import load_entity_by_id
from entities.cms.models import Page
from entities.cms.resolvers.mutation_copy_page import resolve_copy_page
from entities.event.models import Event
from entities.event.resolvers.mutation_event_copy import resolve_copy_event
from entities.wiki.models import Wiki
from entities.wiki.resolvers.mutation_copy_wiki import resolve_copy_wiki

if TYPE_CHECKING:
    from core.models import Entity

mutation = ObjectType("Mutation")


@mutation.field("copyEntity")
def resolve_copy_entity(_, info, input):
    try:
        clean_input = clean_graphql_input(input)
        entity: Entity = load_entity_by_id(clean_input.get("guid"), ["core.Entity"])
        if isinstance(entity, (Event,)):
            return resolve_copy_event(_, info, input)
        if isinstance(entity, (Page,)):
            return resolve_copy_page(_, info, input)
        if isinstance(entity, (Wiki,)):
            return resolve_copy_wiki(_, info, input)
    except ObjectDoesNotExist:
        raise GraphQLError(constances.COULD_NOT_FIND)

    raise GraphQLError(INVALID_SUBTYPE)
