from typing import Union

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE, NOT_LOGGED_IN
from core.models import Comment, Entity, Group
from core.models.mixin import HierarchicalEntityMixin
from core.resolvers import decorators, shared
from core.utils.cleanup import schedule_cleanup_group_content_featured_images
from entities.event.models import Event
from entities.file.models import FileFolder

mutation = ObjectType("Mutation")


@mutation.field("deleteEntity")
@decorators.resolver_with_request
def resolve_delete_entity(request, input):
    user = request.user

    shared.assert_authenticated(user)

    entity: Union[Entity, None] = None

    try:
        entity = Group.objects.get(id=input.get("guid"))
    except ObjectDoesNotExist:
        pass

    try:
        entity = Entity.objects.get_subclass(id=input.get("guid"))
    except ObjectDoesNotExist:
        pass

    if not entity:
        return resolve_delete_comment(request, input)

    if isinstance(entity, Group):
        schedule_cleanup_group_content_featured_images(entity)

    resolver = get_resolver(entity, input, request)

    return resolver.resolve()


def get_resolver(entity, parameters, request):
    if isinstance(entity, FileFolder):
        from entities.file.resolvers.resolve_delete_file import (
            RecursiveDeleteFileResolver,
        )

        return RecursiveDeleteFileResolver(entity, parameters, request)
    elif isinstance(entity, Event):
        from entities.event.resolvers.mutation_delete_event import DeleteEventResolver

        return DeleteEventResolver(entity, parameters, request)

    return DefaultDeleteEntityResolver(entity, parameters, request)


class DefaultDeleteEntityResolver:
    def __init__(self, entity, parameters, request):
        self.entity = entity
        self.parameters = parameters
        self.request = request
        self.report = []

    def resolve(self):
        recursive = self.parameters.get("recursive") or "forceDelete"
        if recursive == "tryDelete":
            return self.resolve_try_delete()

        if recursive == "softDelete":
            return self.resolve_soft_delete()

        return self.resolve_force_delete()

    def resolve_report(self):
        if not self.entity.can_delete(self.request.user):
            self.report.append(
                {
                    "entity": self.entity,
                    "next": "softDelete",
                }
            )
        if isinstance(self.entity, HierarchicalEntityMixin):
            for child in self.entity.get_children():
                resolver = get_resolver(child, self.parameters, self.request)
                resolver.resolve_report()
                self.report.extend(resolver.report)

    def _recursive(self):
        if isinstance(self.entity, HierarchicalEntityMixin):
            for child in self.entity.get_children():
                resolver = get_resolver(child, self.parameters, self.request)
                response = resolver.resolve()
                if not response.get("success"):
                    self.report.append(resolver.report)

    def resolve_try_delete(self):
        self.resolve_report()
        if len(self.report) > 0:
            return {
                "success": False,
                "errors": self.report,
            }

        self._recursive()
        self._delete_if_single()
        return {"success": True}

    def resolve_soft_delete(self):
        self._recursive()
        self._delete_if_single()
        return {"success": True}

    def resolve_force_delete(self):
        self.resolve_report()
        if len(self.report) > 0:
            msg = "could_not_delete"
            raise GraphQLError(msg)
        self._recursive()
        self._delete_if_single()
        return {"success": True}

    def _delete_if_single(self):
        try:
            if self.entity.has_children():
                return
        except AttributeError:
            pass
        if self.entity.can_delete(self.request.user):
            self.entity.delete()


@mutation.field("deleteEntities")
@decorators.resolver_with_request
def resolve_delete_entities(request, input):
    user = request.user

    shared.assert_authenticated(user)

    enabled_entity_types = [
        "blog",
        "page",
        "discussion",
        "event",
        "magazine",
        "magazine_issue",
        "news",
        "episode",
        "podcast",
        "poll",
        "question",
        "wiki",
    ]

    entities = Entity.objects.filter(id__in=input.get("guids")).select_subclasses()
    for entity in entities:
        shared.assert_archive_access(entity, user)
        shared.assert_delete_possible(entity, user)
        if getattr(entity, "type_to_string", None) not in enabled_entity_types:
            msg = f"not_supported:{entity.type_to_string}"
            raise GraphQLError(msg)

    report = []
    for entity in entities:
        resolver = get_resolver(entity, input, request)
        resolver.resolve()
        report = [*report, *resolver.report]

    if not report:
        return {"success": True}

    return {
        "success": False,
        "errors": report,
    }


def resolve_delete_comment(request, input):
    user = request.user

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        comment = Comment.objects.get(id=input.get("guid"))
        container = comment.get_root_container()
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not comment.can_write(user):
        raise GraphQLError(COULD_NOT_SAVE)

    comment.delete()

    # trigger index update.
    container.save()

    return {"success": True}
