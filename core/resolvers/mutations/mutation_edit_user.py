from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from graphql import GraphQLError

from core import config
from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE, INVALID_VALUE
from core.resolvers import decorators, shared
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("editUserName")
@decorators.resolver_with_request
def resolve_edit_user_name(request, **kwargs):
    assert_edit_username_enabled()

    current_user = request.user
    shared.assert_authenticated(current_user)

    user = resolve_user_guid(kwargs["input"])
    shared.assert_user_match_or_admin(current_user, user)

    resolve_update_username(user, kwargs["input"])

    user.save()
    return {"user": user}


def assert_edit_username_enabled():
    if not config.EDIT_USER_NAME_ENABLED:
        raise GraphQLError(COULD_NOT_SAVE)


def resolve_update_username(user, clean_input):
    if not clean_input.get("name"):
        raise GraphQLError(INVALID_VALUE)

    if len(clean_input.get("name")) > 100:
        raise GraphQLError(INVALID_VALUE)

    user.name = clean_input.get("name")


def resolve_user_guid(clean_input):
    try:
        return User.objects.get(id=clean_input.get("guid"))
    except (ObjectDoesNotExist, ValidationError):
        raise GraphQLError(COULD_NOT_FIND)
