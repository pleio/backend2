from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.models import Group
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("acceptMembershipRequest")
def resolve_accept_membership_request(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        group = Group.objects.get(id=clean_input.get("groupGuid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)
    if not group.can_write(user):
        raise GraphQLError(COULD_NOT_SAVE)

    try:
        requesting_user = User.objects.get(id=clean_input.get("userGuid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if group.is_full_member(requesting_user):
        return {"group": group}

    group.join(requesting_user, "member")

    from core.mail_builders.group_membership_approved import (
        schedule_group_membership_approved_mail,
    )

    schedule_group_membership_approved_mail(
        group=group, user=requesting_user, sender=user
    )

    return {"group": group}
