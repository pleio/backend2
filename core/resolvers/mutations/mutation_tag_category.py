from ariadne import ObjectType
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_ADD,
    COULD_NOT_FIND,
    DUPLICATE_CATEGORY_NAME,
    DUPLICATE_TAG_NAME,
)
from core.resolvers import decorators, shared
from core.utils.tags import (
    WEIGHT_NOT_SPECIFIED,
    AddTagCategory,
    DeleteTagCategory,
    DeleteTagCategoryTag,
    DuplicateValueError,
    ExtendTagCategory,
    UpdateTagCategory,
    UpdateTagCategoryTag,
    ValueNotFoundError,
)

mutation = ObjectType("Mutation")


@mutation.field("addTagCategory")
@decorators.resolver_with_request
def mutation_add_tag_category(request, input):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    try:
        content_migration = AddTagCategory(input["name"])
        content_migration.process()
    except KeyError:
        raise GraphQLError(COULD_NOT_ADD)
    except DuplicateValueError:
        raise GraphQLError(DUPLICATE_CATEGORY_NAME)

    return {"siteSettings": {}}


@mutation.field("updateTagCategory")
@decorators.resolver_with_request
def mutation_update_tag_category(request, name, input):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    try:
        content_migration = UpdateTagCategory(
            old_name=name,
            name=input.get("name"),
            weight=input.get("weight", WEIGHT_NOT_SPECIFIED),
        )
        content_migration.process()
    except DuplicateValueError:
        raise GraphQLError(DUPLICATE_CATEGORY_NAME)
    except ValueNotFoundError:
        raise GraphQLError(COULD_NOT_FIND)

    return {"siteSettings": {}}


@mutation.field("extendTagCategory")
@decorators.resolver_with_request
def mutation_extend_tag_category(request, name, input):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    try:
        content_migration = ExtendTagCategory(
            name=name,
            value=input["value"],
            synonyms=input.get("synonyms", []),
            weight=input.get("weight", WEIGHT_NOT_SPECIFIED),
        )
        content_migration.process()
    except KeyError:
        raise GraphQLError(COULD_NOT_ADD)
    except DuplicateValueError:
        raise GraphQLError(DUPLICATE_TAG_NAME)
    except ValueNotFoundError:
        raise GraphQLError(COULD_NOT_FIND)

    return {"siteSettings": {}}


@mutation.field("updateTagCategoryTag")
@decorators.resolver_with_request
def mutation_update_tag_category_tag(request, name, value, input):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    try:
        content_migration = UpdateTagCategoryTag(
            name=name,
            value=value,
            new_value=input.get("value"),
            synonyms=input.get("synonyms", []),
            weight=input.get("weight", WEIGHT_NOT_SPECIFIED),
        )
        content_migration.process()
    except DuplicateValueError:
        raise GraphQLError(DUPLICATE_TAG_NAME)
    except ValueNotFoundError:
        raise GraphQLError(COULD_NOT_FIND)

    return {"siteSettings": {}}


@mutation.field("deleteTagCategory")
@decorators.resolver_with_request
def mutation_delete_tag_category(request, name, keepTags=False):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    try:
        content_migration = DeleteTagCategory(name, keepTags)
        content_migration.process()
    except ValueNotFoundError:
        raise GraphQLError(COULD_NOT_FIND)

    return {"siteSettings": {}}


@mutation.field("deleteTagCategoryTag")
@decorators.resolver_with_request
def mutation_delete_tag_category_tag(request, name, value, keepTag=False):
    shared.assert_authenticated(request.user)
    shared.assert_administrator(request.user)

    try:
        content_migration = DeleteTagCategoryTag(name, value, keepTag)
        content_migration.process()
    except ValueNotFoundError:
        raise GraphQLError(COULD_NOT_FIND)

    return {"siteSettings": {}}
