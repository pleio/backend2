import logging

from ariadne import ObjectType
from django.core.exceptions import ValidationError
from graphql import GraphQLError

from chat.models import Chat
from core.constances import INVALID_PROFILE_FIELD_GUID
from core.lib import clean_graphql_input
from core.models import Group, GroupProfileFieldSetting, ProfileField
from core.resolvers import group_shared, shared
from core.utils.auto_member_profile_field import AutoMemberProfileFieldAtGroup
from user.models import User

logger = logging.getLogger(__name__)
mutation = ObjectType("Mutation")


@mutation.field("addGroup")
def resolve_add_group(_, info, input):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_can_create(user, Group, None)

    clean_input = clean_graphql_input(input)

    group = Group()
    group.owner = user
    group.name = clean_input.get("name", "")
    auto_membership_fields = AutoMemberProfileFieldAtGroup(group)

    shared.resolve_update_entity_icon(group, clean_input)
    shared.update_featured_image(group, clean_input)
    shared.resolve_update_tags(group, clean_input)

    group.rich_description = clean_input.get("richDescription", "")
    group.introduction = clean_input.get("introduction", "")
    group.is_introduction_public = clean_input.get("isIntroductionPublic", False)
    group.welcome_message = clean_input.get("welcomeMessage", "")
    group.required_fields_message = clean_input.get("requiredProfileFieldsMessage", "")

    group.is_closed = clean_input.get("isClosed", False)
    group.is_hidden = clean_input.get("isHidden", False)
    group.is_membership_on_request = clean_input.get("isMembershipOnRequest", False)
    group.auto_notification = clean_input.get("autoNotification", False)
    group_shared.update_plugins(group, clean_input)
    group_shared.update_menu(group, clean_input)
    group_shared.update_file_notifications(group, clean_input)
    group_shared.update_auto_membership_fields(group, clean_input)

    group.content_presets["defaultTags"] = clean_input.get("defaultTags", [])
    group.content_presets["defaultTagCategories"] = clean_input.get(
        "defaultTagCategories", []
    )
    group_shared.update_is_join_button_hidden(group, clean_input)
    group_shared.update_is_menu_always_visible(group, clean_input)

    if user.is_site_admin:
        group.is_featured = clean_input.get("isFeatured", False)
        group.is_leaving_group_disabled = clean_input.get(
            "isLeavingGroupDisabled", False
        )
        group.is_auto_membership_enabled = clean_input.get(
            "isAutoMembershipEnabled", False
        )

    group.tags = clean_input.get("tags", [])
    group.is_chat_enabled = clean_input.get("isChatEnabled", False)

    group.save()
    auto_membership_fields.apply()

    group._start_page = group.create_group_start_page()
    group.save()

    if user.is_site_admin and group.is_auto_membership_enabled:
        users = User.objects.filter(is_active=True)
        for u in users:
            if not group.is_full_member(u):
                group.join(u, "member")

    if "showMemberProfileFieldGuids" in clean_input:
        _update_profile_field_show_field(
            group, clean_input.get("showMemberProfileFieldGuids")
        )

    if "requiredProfileFieldGuids" in clean_input:
        _update_profile_field_required(
            group, clean_input.get("requiredProfileFieldGuids")
        )

    group.join(user)

    if group.is_chat_enabled:
        Chat.objects.get_or_create(group=group)

    return {"group": group}


def _update_profile_field_show_field(group, guids):
    for profile_field_id in guids:
        try:
            profile_field = ProfileField.objects.get(id=profile_field_id)
            setting, created = GroupProfileFieldSetting.objects.get_or_create(
                profile_field=profile_field, group=group
            )
            setting.show_field = True
            setting.save()
        except ProfileField.DoesNotExist:
            raise GraphQLError(INVALID_PROFILE_FIELD_GUID)
        except ValidationError as e:
            raise GraphQLError(", ".join(e.messages))


def _update_profile_field_required(group, guids):
    for profile_field_id in guids:
        try:
            profile_field = ProfileField.objects.get(id=profile_field_id)
            setting, created = GroupProfileFieldSetting.objects.get_or_create(
                profile_field=profile_field, group=group
            )
            setting.is_required = True
            setting.save()
        except ProfileField.DoesNotExist:
            raise GraphQLError(INVALID_PROFILE_FIELD_GUID)
        except ValidationError as e:
            raise GraphQLError(", ".join(e.messages))
