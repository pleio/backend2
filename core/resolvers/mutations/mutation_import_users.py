import codecs
import csv
import os
import tempfile

from ariadne import ObjectType
from django.utils.translation import gettext_lazy
from graphql import GraphQLError

from core import config
from core.constances import INVALID_KEY
from core.lib import (
    clean_graphql_input,
    get_tmp_file_path,
    safe_file_path,
    tenant_schema,
)
from core.models import ProfileField
from core.resolvers import shared
from user.admin_permissions import HasUserManagementPermission

mutation = ObjectType("Mutation")


def get_user_fields():
    # user fields
    user_fields = [
        {"value": "id", "label": "guid"},
        {"value": "email", "label": gettext_lazy("email")},
        {"value": "name", "label": gettext_lazy("name")},
    ]

    for field in ProfileField.objects.all():
        user_fields.append({"value": str(field.id), "label": field.name})

    return user_fields


def get_access_ids():
    # get access ids
    accessIdOptions = [
        {"value": 0, "label": gettext_lazy("Just me")},
        {"value": 1, "label": gettext_lazy("Logged in users")},
    ]
    if not config.IS_CLOSED:
        accessIdOptions.append({"value": 2, "label": gettext_lazy("Public")})

    return accessIdOptions


@mutation.field("importUsersStep1")
def resolve_import_users_step_1(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_admin_permission(user, HasUserManagementPermission)

    csv_file = clean_input.get("usersCsv")
    csv_reader = csv.reader(codecs.iterdecode(csv_file, "utf-8"), delimiter=";")

    csv_header = []
    for row in csv_reader:
        csv_header = row
        break

    # Save file in temp user folder
    temp_file_path = get_tmp_file_path(user, ".csv")

    with open(temp_file_path, "wb+") as destination:
        for chunk in csv_file.chunks():
            destination.write(chunk)

    # This can later be a database record?
    import_id = os.path.basename(temp_file_path)

    return {
        "importId": import_id,
        "csvColumns": csv_header,
        "userFields": get_user_fields(),
        "accessIdOptions": get_access_ids(),
    }


@mutation.field("importUsersStep2")
def resolve_import_users_step_2(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_admin_permission(user, HasUserManagementPermission)

    # Validate importId
    csv_directory = safe_file_path(tempfile.gettempdir(), tenant_schema(), user.guid)
    csv_location = safe_file_path(csv_directory, clean_input.get("importId"))

    if not os.path.isfile(csv_location):
        raise GraphQLError(INVALID_KEY)

    valid_column_values = []

    with open(csv_location, mode="r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")
        for row in csv_reader:
            valid_column_values = row
            break

    valid_user_field_values = [d["value"] for d in get_user_fields() if "value" in d]
    valid_access_id_values = [d["value"] for d in get_access_ids() if "value" in d]

    fields = clean_input.get("fields")

    for field in fields:
        if field["userField"] not in valid_user_field_values:
            msg = "invalid_user_field"
            raise GraphQLError(msg)
        if (
            field["userField"] not in ["id", "name", "email"]
            and field["accessId"] not in valid_access_id_values
        ):
            msg = "invalid_access_id"
            raise GraphQLError(msg)
        if field["csvColumn"] not in valid_column_values:
            msg = "invalid_csv_column"
            raise GraphQLError(msg)
        if "forceAccess" not in field:
            field["forceAccess"] = False

    from core.tasks import import_users

    import_users.delay(tenant_schema(), fields, csv_location, user.guid)

    return {"success": True}
