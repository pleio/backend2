from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.utils.timezone import now
from django.utils.translation import gettext
from graphql import GraphQLError

from core import constances
from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE
from core.lib import clean_graphql_input
from core.resolvers import shared
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("editUsers")
def resolve_edit_users(_, info, input):
    performing_user = info.context["request"].user
    clean_input = clean_graphql_input(input)
    action = clean_input.get("action")

    shared.assert_authenticated(performing_user)
    shared.assert_user_manager(performing_user)

    try:
        users = User.objects.filter(id__in=clean_input.get("guids"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    # can not ban yourself
    if performing_user in users:
        raise GraphQLError(COULD_NOT_SAVE)

    for user in users:
        if action == "ban":
            user.is_active = False
            user.ban_reason = input.get("banReason") or gettext("Blocked by admin")
            user.banned_at = now()
        elif action == "unban":
            user.is_active = True
            user.ban_reason = ""
            user.banned_at = None
        user.save()

    return {"success": True}


@mutation.field("scheduleBanUsers")
def resolve_schedule_ban_users(_, info, input):
    performing_user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(performing_user)
    shared.assert_user_manager(performing_user)

    if (
        clean_input.get("banAt") is not None
        and clean_input.get("banAt") < timezone.now()
    ):
        raise GraphQLError(constances.EXPECT_FUTURE_DATE)

    has_date = bool(clean_input.get("banAt"))
    has_reason = bool(clean_input.get("banReason"))

    # Both values should be present or absent.
    if (not has_reason and has_date) or (has_reason and not has_date):
        raise GraphQLError(constances.INVALID_VALUE)

    User.objects.filter(id__in=clean_input.get("guids"), is_active=True).update(
        schedule_ban_at=clean_input.get("banAt"),
        schedule_ban_reason=clean_input.get("banReason"),
    )

    return {"users": User.objects.filter(id__in=clean_input.get("guids"))}
