from ariadne import ObjectType
from django.core.exceptions import ValidationError
from graphql import GraphQLError

from core import constances
from core.models import PublishRequest
from core.resolvers import decorators, shared
from user.admin_permissions import HasPublicationRequestManagementPermission

mutation = ObjectType("Mutation")


@mutation.field("assignPublishRequest")
@decorators.resolver_with_request
def resolve_assign_publish_request(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        publish_request = PublishRequest.objects.get(id=guid)

        publish_request.assign_request(request.user)

        return {
            "success": True,
            "node": publish_request,
        }
    except (PublishRequest.DoesNotExist, ValidationError):
        raise GraphQLError(constances.COULD_NOT_FIND)


@mutation.field("resetPublishRequest")
@decorators.resolver_with_request
def resolve_reset_publish_request(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        publish_request = PublishRequest.objects.get(id=guid)

        publish_request.reset_request()

        return {
            "success": True,
            "node": publish_request,
        }
    except (PublishRequest.DoesNotExist, ValidationError):
        raise GraphQLError(constances.COULD_NOT_FIND)


@mutation.field("confirmPublishRequest")
@decorators.resolver_with_request
def resolve_confirm_publish_request(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        publish_request = PublishRequest.objects.get(id=guid)

        _assert_is_not_published(publish_request)

        publish_request.confirm_request()

        return {
            "success": True,
            "node": publish_request,
        }
    except (PublishRequest.DoesNotExist, ValidationError):
        raise GraphQLError(constances.COULD_NOT_FIND)


def _assert_is_not_published(publish_request):
    if publish_request.entity.published:
        raise GraphQLError(constances.COULD_NOT_FIND)


@mutation.field("rejectPublishRequest")
@decorators.resolver_with_request
def resolve_reject_publish_request(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        publish_request = PublishRequest.objects.get(id=guid)

        publish_request.reject_request()

        return {
            "success": True,
            "node": None,
        }
    except (PublishRequest.DoesNotExist, ValidationError):
        raise GraphQLError(constances.COULD_NOT_FIND)
