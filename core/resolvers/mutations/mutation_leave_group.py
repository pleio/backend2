from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_FIND,
    COULD_NOT_LEAVE,
    LEAVING_GROUP_IS_DISABLED,
    NOT_LOGGED_IN,
    USER_NOT_MEMBER_OF_GROUP,
)
from core.lib import clean_graphql_input
from core.models import Group

mutation = ObjectType("Mutation")


@mutation.field("leaveGroup")
def resolve_leave_group(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if group.is_leaving_group_disabled:
        raise GraphQLError(LEAVING_GROUP_IS_DISABLED)

    if not group.is_member(user):
        raise GraphQLError(USER_NOT_MEMBER_OF_GROUP)

    if group.is_owner(user):
        raise GraphQLError(COULD_NOT_LEAVE)

    group.leave(user, recursive=True)

    return {"group": group}
