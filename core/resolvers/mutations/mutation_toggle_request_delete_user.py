from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.mail_builders.user_cancel_delete_for_admin import (
    schedule_user_cancel_delete_for_admin_mail,
)
from core.mail_builders.user_cancel_delete_for_user import (
    schedule_user_cancel_delete_for_user_mail,
)
from core.mail_builders.user_request_delete_for_admin import (
    schedule_user_request_delete_for_admin_mail,
)
from core.mail_builders.user_request_delete_for_user import (
    schedule_user_request_delete_for_user_mail,
)
from core.resolvers import decorators
from core.resolvers.queries.query_viewer import user_viewer_adapter
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("toggleRequestDeleteUser")
@decorators.resolver_with_request
def resolve_toggle_request_delete_user(request, input):
    user = request.user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        requested_user = User.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not requested_user == user:
        raise GraphQLError(COULD_NOT_SAVE)

    admin_users = User.objects.filter(roles__contains=["ADMIN"])
    if user.is_delete_requested:
        user.is_delete_requested = False
        user.save()

        schedule_user_cancel_delete_for_user_mail(user=user)

        # mail to admins to notify about removed admin
        for admin_user in admin_users:
            schedule_user_cancel_delete_for_admin_mail(user=user, admin=admin_user)

    else:
        user.is_delete_requested = True
        user.save()

        schedule_user_request_delete_for_user_mail(user=user)

        # mail to admins to notify about removed admin
        for admin_user in admin_users:
            schedule_user_request_delete_for_admin_mail(user=user, admin=admin_user)

    return {"viewer": user_viewer_adapter(request)}
