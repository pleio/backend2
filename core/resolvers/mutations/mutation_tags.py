import uuid

from ariadne import ObjectType
from graphql import GraphQLError

from core.elasticsearch import schedule_index_document
from core.models.tags import EntityTag, Tag, TagSynonym
from core.resolvers import decorators, shared
from core.resolvers.queries.query_tags import get_query_tags

mutation = ObjectType("Mutation")


@mutation.field("mergeTags")
@decorators.resolver_with_request
def resolve_merge_tags(request, **kwargs):
    shared.assert_administrator(request.user)

    clean_input = kwargs["input"]

    assert_tag_exists(clean_input.get("tag"))
    assert_tag_exists(clean_input.get("synonym"))

    # 1) Tag(synonym) krijgt een tijdelijke andere naam, om conflicten te voorkomen
    s_tag = Tag.objects.get(label=clean_input["synonym"])
    s_tag.label = str(uuid.uuid4())
    s_tag.save()

    # 2) Synonym(synonym) van Tag(tag)
    tag = Tag.objects.get(label=clean_input["tag"])
    TagSynonym.objects.create(label=clean_input["synonym"], tag=tag)
    for s_synonym in s_tag.synonyms.all():
        s_synonym.tag = tag
        s_synonym.save()

    for ref in EntityTag.objects.filter(tag=s_tag):
        if not EntityTag.objects.filter(entity_id=ref.entity_id, tag=tag).exists():
            # 3) EntityTag's moeten het id van Tag(tag) krijgen
            ref.tag = tag
            ref.save()
        else:
            # 3b) Tag bestaat al op de entity, dan mag deze weg.
            ref.delete()

        # 3c) ... en de _tag_summary moet worden geüpdate
        ref.entity.save()

    s_tag.delete()

    # 4) update de index van alle documenten.
    for ref in EntityTag.objects.filter(tag=tag):
        schedule_index_document(ref.entity)

    return get_query_tags()


@mutation.field("extractTagSynonym")
@decorators.resolver_with_request
def resolve_extract_tag_synonym(request, **kwargs):
    clean_input = kwargs.get("input")
    shared.assert_administrator(request.user)

    assert_tag_exists(clean_input.get("tag"))
    assert_synonym_exists(clean_input.get("synonym"))

    # 0) Tag waar het synonym in zit, voor updaten van content
    tag = Tag.objects.get(label=clean_input["tag"])

    # 1) Synonym(synonym) wordt opgeheven.
    TagSynonym.objects.filter(label=clean_input["synonym"]).delete()

    # 2) Nieuwe Tag(synonym)
    s_tag = Tag.objects.create(label=clean_input["synonym"])

    # 3) EntityTag's met author_label(synonym) krijgen Tag(synonym)
    for ref in EntityTag.objects.filter(author_label__iexact=clean_input["synonym"]):
        ref.tag = s_tag
        ref.save()

        # 3b) ... en _tag_summary moet worden geüpdate
        ref.entity.save()

        # 4a) update de index van de gewijzigde documenten.
        schedule_index_document(ref.entity)

    # 4b) update de index van de andere documenten.
    for ref in EntityTag.objects.filter(tag=tag):
        schedule_index_document(ref.entity)

    return get_query_tags()


def assert_tag_exists(tag):
    if Tag.objects.filter(label=tag).exists():
        return

    msg = "Tag does not exist"
    raise GraphQLError(msg)


def assert_synonym_exists(synonym):
    if TagSynonym.objects.filter(label=synonym).exists():
        return

    msg = "Synonym does not exist"
    raise GraphQLError(msg)
