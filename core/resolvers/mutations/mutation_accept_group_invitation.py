from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import INVALID_CODE, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.models import GroupInvitation

mutation = ObjectType("Mutation")


@mutation.field("acceptGroupInvitation")
def resolve_accept_group_invitation(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    invitation = None
    try:
        invitation = GroupInvitation.objects.get(
            invited_user=user, code=clean_input.get("code")
        )
    except ObjectDoesNotExist:
        pass

    try:
        invitation = GroupInvitation.objects.get(
            email=user.email, code=clean_input.get("code")
        )
    except ObjectDoesNotExist:
        pass

    if not invitation:
        raise GraphQLError(INVALID_CODE)

    group = invitation.group

    if invitation.role == "owner":
        group.change_ownership(
            invitation.invited_user, invitation.acting_user or group.owner
        )
    else:
        if not group.is_full_member(user):
            group.join(user, invitation.role or "member")

    invitation.delete()

    return {"group": group}
