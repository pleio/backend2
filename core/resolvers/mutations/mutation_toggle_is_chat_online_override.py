from ariadne import ObjectType

from core.resolvers import shared

mutation = ObjectType("Mutation")


@mutation.field("toggleIsChatOnlineOverride")
def resolve_toggle_is_chat_online_override(_, info):
    performing_user = info.context["request"].user

    shared.assert_authenticated(performing_user)

    performing_user.profile.is_chat_online_override = (
        not performing_user.profile.is_chat_online_override
    )
    performing_user.profile.save()

    return {"isChatOnlineOverride": performing_user.profile.is_chat_online_override}
