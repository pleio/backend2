from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_INVITE, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.models import GroupInvitation

mutation = ObjectType("Mutation")


@mutation.field("deleteGroupInvitation")
def resolve_delete_group_invitation(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        invitation = GroupInvitation.objects.get(id=clean_input.get("id"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    group = invitation.group

    if not group.can_write(user):
        raise GraphQLError(COULD_NOT_INVITE)

    invitation.delete()

    return {"group": group}
