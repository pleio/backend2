from ariadne import ObjectType
from graphql import GraphQLError

from core import config
from core.constances import COULD_NOT_ADD, NOT_LOGGED_IN
from core.lib import clean_graphql_input
from core.models import WebPushSubscription

mutation = ObjectType("Mutation")


@mutation.field("addWebPushSubscription")
def resolve_add_web_push_subscription(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    if not config.PUSH_NOTIFICATIONS_ENABLED:
        raise GraphQLError(COULD_NOT_ADD)

    WebPushSubscription.objects.create(
        browser=clean_input.get("browser"),
        endpoint=clean_input.get("endpoint"),
        auth=clean_input.get("auth"),
        p256dh=clean_input.get("p256dh"),
        user=user,
    )

    return {"success": True}
