from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.models import ProfileFieldValidator
from core.resolvers import shared

mutation = ObjectType("Mutation")


@mutation.field("deleteSiteSettingProfileFieldValidator")
def resolve_delete_site_setting_profile_field_validator(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    try:
        validator = ProfileFieldValidator.objects.get(id=clean_input.get("id"))
        validator.delete()
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    return {"success": True}
