from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.resolvers import decorators, shared
from core.utils.entity import load_entity_by_id

mutation = ObjectType("Mutation")


@mutation.field("updateUserWidgetSettings")
@decorators.resolver_with_request
def resolve_update_user_widget_settings(request, input):
    performing_user = request.user
    shared.assert_authenticated(performing_user)

    try:
        container = load_entity_by_id(
            input.get("containerGuid"), class_references=["core.Entity"]
        )

        performing_user.profile.set_widget_settings(
            input.get("guid"), input.get("settings")
        )
        performing_user.profile.save()

        return {
            "success": True,
            "entity": container,
        }
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)
