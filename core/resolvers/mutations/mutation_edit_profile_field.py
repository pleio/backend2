from datetime import datetime

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.translation import gettext as _
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE
from core.exceptions import FloodOverflowError
from core.lib import access_id_to_acl, clean_graphql_input, format_list, is_valid_json
from core.models import ProfileField, UserProfileField
from core.resolvers import shared
from core.resolvers.scalars.secure_rich_text import secure_prosemirror_value_parser
from core.utils.auto_member_profile_field import AutoMemberProfileField
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("editProfileField")
def resolve_edit_profile_field(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        requested_user = User.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not requested_user == user and not user.is_site_admin:
        raise GraphQLError(COULD_NOT_SAVE)

    profile_fields_updater = UpdateProfileFieldCollection(
        user, requested_user, request=info.context["request"]
    )

    for field in clean_input.get("fields"):
        profile_fields_updater.add_field(field)

    if profile_fields_updater.errors:
        return {
            "user": requested_user,
            "success": False,
            "fields": profile_fields_updater.errors,
        }

    profile_fields_updater.apply()
    return {
        "user": requested_user,
        "success": True,
    }


class UpdateProfileFieldCollection:
    def __init__(self, acting_user, changing_user, request):
        self.acting_user = acting_user
        self.changing_user = changing_user
        self.request = request
        self.fields = []
        self.errors = []
        self.auto_member = AutoMemberProfileField(changing_user)

    def add_field(self, field_data):
        try:
            field = ProfileField.objects.get(key=field_data["key"])

            if field.is_editable_by_user:
                self.fields.append(
                    {
                        "field": field,
                        **field_data,
                        **self.overwrite_value(field_data, field),
                        **self.add_access(field_data.get("accessId", 1)),
                    }
                )

        except ProfileField.DoesNotExist:
            self.errors.append({"key": field_data["key"], "message": COULD_NOT_FIND})
        except ValidationError as e:
            self.errors.append(
                {"key": field_data["key"], "message": ", ".join(e.messages)}
            )

    def apply(self):
        for field in self.fields:
            user_profile_field, created = UserProfileField.objects.get_or_create(
                user_profile=self.changing_user.profile, profile_field=field["field"]
            )
            self.auto_member.log_previous_value(user_profile_field)
            user_profile_field.read_access = field["read_access"]
            if created:
                user_profile_field.write_access = field["write_access"]
            user_profile_field.value = field["value"]
            user_profile_field.save()

            self.auto_member.apply(user_profile_field)

    def overwrite_value(self, field_data, field):
        value = field_data["value"]

        if not isinstance(value, str):
            value = ""
        else:
            value = self._maybe_html_value(field, value)
            value = self._maybe_invalid_option(field, value)
            value = self._maybe_date(field, value)
            value = self._maybe_multi_select(field, value)
            value = self._maybe_custom_validator(field, value, self.request)

        return {"value": value}

    def add_access(self, access_id):
        return {
            "read_access": access_id_to_acl(self.changing_user, access_id),
            "write_access": access_id_to_acl(self.changing_user, 0),
        }

    @staticmethod
    def _maybe_html_value(field, value):
        if value and field.field_type == "html_field":
            if not is_valid_json(value):
                raise ValidationError(_("Invalid text format"))
            return secure_prosemirror_value_parser(value)
        return value

    @staticmethod
    def _maybe_invalid_option(field, value):
        if (
            value
            and field.field_type == "select_field"
            and value not in field.field_options
        ):
            raise ValidationError(
                _("%(value)s is not in field options. Try one of %(options)s")
                % {
                    "value": value,
                    "options": format_list(field.field_options, exclusive=True),
                }
            )
        return value

    @staticmethod
    def _maybe_date(field, value):
        if value and field.field_type == "date_field":
            try:
                datetime.strptime(value, "%Y-%m-%d")
            except Exception:
                raise ValidationError(_("Invalid date format. Expecting Y-m-d"))
        return value

    @staticmethod
    def _maybe_multi_select(field, value):
        if value and field.field_type == "multi_select_field":
            for selected in value.split(","):
                if selected not in field.field_options:
                    raise ValidationError(
                        _("%(value)s is not in field options. Try one of %(options)s")
                        % {
                            "value": selected,
                            "options": format_list(field.field_options, exclusive=True),
                        }
                    )
        return value

    @staticmethod
    def _maybe_custom_validator(field, value, request):
        try:
            if value and not field.validate(value, request):
                raise ValidationError(_("Value does not meet validation requirements"))
        except FloodOverflowError as e:
            raise ValidationError(
                _("To many failed attempts, try again in %(minutes)s minutes")
                % {"minutes": e.expire_in_minutes}
            )
        return value
