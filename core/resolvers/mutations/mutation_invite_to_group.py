import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError
from django.db.models import Q
from graphql import GraphQLError

from concierge.api.queries import get_or_create_user
from core import config
from core.constances import COULD_NOT_FIND, COULD_NOT_INVITE, USER_NOT_SITE_ADMIN
from core.lib import clean_graphql_input, generate_code, tenant_schema
from core.mail_builders.group_invite_to_group import schedule_invite_to_group_mail
from core.models import Group, GroupInvitation
from core.resolvers import shared
from user.models import User

logger = logging.getLogger(__name__)


mutation = ObjectType("Mutation")


@mutation.field("inviteToGroup")
def resolve_invite_to_group(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not group.can_write(user):
        raise GraphQLError(COULD_NOT_INVITE)

    if clean_input.get("directAdd") and not user.is_site_admin:
        raise GraphQLError(USER_NOT_SITE_ADMIN)

    mutation = InviteToGroupMutation(clean_input, user, group)
    mutation.process()

    return {"group": group, "status": mutation.report}


class InviteToGroupMutation:
    def __init__(self, clean_input, acting_user, group):
        self.clean_input = clean_input
        self.acting_user = acting_user
        self.group = group
        self.receiving_role = None
        self.receiving_guid = None
        self.receiving_email = None
        self.receiving_user = None
        self.report = None

    def process(self):
        self.report = []
        for user_input in self.clean_input.get("users"):
            self.receiving_role = user_input.get("role")
            self.receiving_guid = user_input.get("guid")
            self.receiving_email = user_input.get("email")
            self.receiving_user = None
            if self.receiving_guid:
                try:
                    self.receiving_user = User.objects.get(id=self.receiving_guid)
                    self.receiving_email = self.receiving_user.email
                except ObjectDoesNotExist:
                    pass
            elif self.receiving_email:
                try:
                    self.receiving_user = User.objects.get(email=self.receiving_email)
                except Exception:
                    pass

            if self.clean_input.get("directAdd"):
                self.mutation_add_directly()
            else:
                self.mutation_invite_user()

    def get_or_create_remote_user(self):
        try:
            assert config.GET_CREATE_GROUP_MEMBERS, "Functionality is disabled"
            if (
                User.objects.with_deleted()
                .filter(is_active=False, email=self.receiving_email)
                .exists()
            ):
                return None
            claims = get_or_create_user(
                email=self.receiving_email,
                actor_name=self.acting_user.name,
                next=self.group.url,
            )
            assert "error" not in claims, "Errors during create user."
            return User.objects.get_or_create_claims(claims)
        except (IntegrityError, ValidationError, AssertionError):
            pass

    def mutation_add_directly(self):
        if not self.receiving_user and self.receiving_email:
            self.receiving_user = self.get_or_create_remote_user()

        if self.receiving_user:
            self.group.join(self.receiving_user, self.receiving_role or "member")
            self.add_to_report("member")
        else:
            self.mutation_invite_user()

    def add_to_report(self, status):
        self.report.append(
            {
                "item": (
                    self.receiving_user.name
                    if self.receiving_user
                    else (self.receiving_guid or self.receiving_email)
                ),
                "status": status,
            }
        )

    def get_or_create_invitation(self):
        invitation_query = GroupInvitation.objects.filter(group=self.group).filter(
            Q(invited_user__email=self.receiving_email) | Q(email=self.receiving_email)
        )

        if invitation := invitation_query.first():
            changed = False
            if not invitation.code:
                changed = True
                invitation.code = generate_code()
            if invitation.role != self.receiving_role:
                changed = True
                invitation.role = self.receiving_role
            if changed:
                invitation.save()
            return invitation

        return GroupInvitation.objects.create(
            code=generate_code(),
            acting_user=self.acting_user,
            invited_user=self.receiving_user,
            group=self.group,
            email=self.receiving_email,
            role=self.receiving_role,
        )

    def mutation_invite_user(self):
        if not self.receiving_email:
            self.add_to_report("invalidEmail")
            return

        logger.info("Sending to %s", self.receiving_email)

        try:
            invitation = self.get_or_create_invitation()
            schedule_invite_to_group_mail(
                user=self.receiving_user,
                sender=self.acting_user,
                code=invitation.code,
                group=self.group,
                language=config.LANGUAGE if not self.receiving_user else None,
                email=self.receiving_email if not self.receiving_user else None,
            )
            self.add_to_report("invited")
        except Exception as e:
            logger.error(
                "Error while sending invite to group mail %s %s %s",
                tenant_schema(),
                e.__class__,
                str(e),
            )
            self.add_to_report("notInvited")
