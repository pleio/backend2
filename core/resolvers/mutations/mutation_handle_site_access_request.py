from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import now

from core.lib import clean_graphql_input
from core.mail_builders.site_access_request_accepted import (
    schedule_site_access_request_accepted_mail,
)
from core.mail_builders.site_access_request_denied import (
    schedule_site_access_request_denied_mail,
)
from core.models import SiteAccessRequest
from core.resolvers import shared

mutation = ObjectType("Mutation")


@mutation.field("handleSiteAccessRequest")
def resolve_handle_site_access_request(_, info, input):
    user = info.context["request"].user

    shared.assert_authenticated(user)
    shared.assert_access_request_permission(user)

    clean_input = clean_graphql_input(input)

    try:
        access_request = SiteAccessRequest.objects.get(email=clean_input.get("email"))
    except ObjectDoesNotExist:
        return {"success": False}

    # User already exists so status can't change anymore
    if access_request.status == "accepted":
        return {"success": False}

    accepted = clean_input.get("accept")

    if not clean_input.get("silent", False):
        if accepted:
            schedule_site_access_request_accepted_mail(
                sender=user,
                name=access_request.claims.get("name"),
                email=access_request.claims.get("email"),
                message=clean_input.get("message", ""),
            )
        else:
            schedule_site_access_request_denied_mail(
                sender=user,
                name=access_request.claims.get("name"),
                email=access_request.claims.get("email"),
                message=clean_input.get("message", ""),
            )

    access_request.accepted = accepted
    access_request.processed = True
    access_request.processed_by = user
    access_request.updated_at = now()
    access_request.reason = clean_input.get("message") or ""
    access_request.save()

    return {"success": True}
