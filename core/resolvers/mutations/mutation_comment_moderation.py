import logging

from ariadne import ObjectType
from django.core.exceptions import ValidationError
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.models.comment import Comment, CommentModerationRequest
from core.resolvers import decorators, shared
from user.admin_permissions import HasPublicationRequestManagementPermission

logger = logging.getLogger(__name__)

mutation = ObjectType("Mutation")


@mutation.field("assignCommentModerationRequest")
@decorators.resolver_with_request
def resolve_assign_comment_moderation_request(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        moderation_request = CommentModerationRequest.objects.get(id=guid)

        moderation_request.assign_to(request.user)
        moderation_request.save()

        return {
            "success": True,
            "node": moderation_request,
        }
    except (ValidationError, CommentModerationRequest.DoesNotExist):
        raise GraphQLError(COULD_NOT_FIND)


@mutation.field("resetCommentModerationRequest")
@decorators.resolver_with_request
def resolve_reset_comment_moderation_request(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        moderation_request = CommentModerationRequest.objects.get(id=guid)

        moderation_request.reset_assignee()
        moderation_request.save()

        return {
            "success": True,
            "node": moderation_request,
        }
    except (ValidationError, CommentModerationRequest.DoesNotExist):
        raise GraphQLError(COULD_NOT_FIND)


@mutation.field("confirmComment")
@decorators.resolver_with_request
def resolve_confirm_comment(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        comment = Comment.objects.get(id=guid)
        comment_moderation_request = comment.comment_moderation_request

        comment.comment_moderation_request = None
        comment.save()

        if comment_moderation_request:
            comment_moderation_request.reset_if_no_comments()

        return {
            "success": True,
        }
    except (ValidationError, Comment.DoesNotExist):
        raise GraphQLError(COULD_NOT_FIND)


@mutation.field("rejectComment")
@decorators.resolver_with_request
def resolve_reject_comment(request, guid):
    try:
        shared.assert_admin_permission(
            request.user, HasPublicationRequestManagementPermission
        )
        comment = Comment.objects.get(id=guid)
        comment_moderation_request = comment.comment_moderation_request

        comment.delete()

        if comment_moderation_request:
            comment_moderation_request.reset_if_no_comments()

        return {
            "success": True,
        }
    except (ValidationError, Comment.DoesNotExist):
        raise GraphQLError(COULD_NOT_FIND)
