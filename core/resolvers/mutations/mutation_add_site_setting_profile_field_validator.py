import codecs
import csv
import logging

from ariadne import ObjectType
from graphql import GraphQLError

from core.constances import INVALID_TYPE, INVALID_VALUE, NO_FILE
from core.lib import clean_graphql_input
from core.models import ProfileFieldValidator
from core.resolvers import shared

logger = logging.getLogger(__name__)


mutation = ObjectType("Mutation")


@mutation.field("addSiteSettingProfileFieldValidator")
def resolve_add_site_setting_profile_field_validator(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    if clean_input.get("type") not in ["inList"]:
        raise GraphQLError(INVALID_TYPE)

    validator_data = (
        []
        if clean_input.get("type") == "inList"
        else clean_input.get("validationString", "")
    )

    if clean_input.get("type") == "inList":
        if not clean_input.get("validationListFile"):
            raise GraphQLError(NO_FILE)

        try:
            csv_file = clean_input.get("validationListFile")
            csv_reader = csv.reader(codecs.iterdecode(csv_file, "utf-8"), delimiter=";")

            for row in csv_reader:
                try:
                    validator_data.append(row[0])
                except IndexError:
                    pass

        except Exception as e:
            logger.error(e)
            logger.error(e.__class__)
            raise GraphQLError(INVALID_VALUE)

    validator = ProfileFieldValidator.objects.create(
        name=clean_input.get("name"),
        validator_type=clean_input.get("type"),
        validator_data=validator_data,
    )

    return {"profileFieldValidator": validator}
