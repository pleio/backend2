import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_FIND,
    COULD_NOT_SAVE,
    NOT_LOGGED_IN,
    USER_NOT_MEMBER_OF_GROUP,
)
from core.lib import clean_graphql_input
from core.models import Group
from user.models import User

logger = logging.getLogger(__name__)


mutation = ObjectType("Mutation")


@mutation.field("changeGroupOwner")
def resolve_change_group_owner(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not group.can_write(user):
        raise GraphQLError(COULD_NOT_SAVE)

    try:
        changing_user = User.objects.get(id=clean_input.get("userGuid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not group.is_member(changing_user):
        raise GraphQLError(USER_NOT_MEMBER_OF_GROUP)

    group.change_ownership(changing_user, user)

    return {"group": group}
