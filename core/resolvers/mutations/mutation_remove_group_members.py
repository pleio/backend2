import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.models import Group, GroupMembership
from core.resolvers import shared

logger = logging.getLogger(__name__)


mutation = ObjectType("Mutation")


@mutation.field("removeGroupMembers")
def resolve_remove_group_members(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        group = Group.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    shared.assert_write_access(group, user)

    guids = clean_input.get("userGuids")

    for guid in guids:
        try:
            user_membership = GroupMembership.objects.get(group=group, user_id=guid)
            user_membership.delete()
        except ObjectDoesNotExist:
            pass

    return {"group": group}
