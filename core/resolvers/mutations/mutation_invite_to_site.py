from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_email
from graphql import GraphQLError

from core.constances import INVALID_EMAIL
from core.lib import clean_graphql_input, generate_code
from core.mail_builders.invite_to_site import schedule_invite_to_site_mail
from core.models import SiteInvitation
from core.resolvers import shared
from user.admin_permissions import HasUserManagementPermission

mutation = ObjectType("Mutation")


def validate_email_addresses(email_addresses):
    if not email_addresses:
        return False
    for email in email_addresses:
        try:
            validate_email(email)
        except ValidationError:
            return False
    return True


@mutation.field("inviteToSite")
def resolve_invite_to_site(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_admin_permission(user, HasUserManagementPermission)

    email_addresses = clean_input.get("emailAddresses")

    if not validate_email_addresses(email_addresses):
        raise GraphQLError(INVALID_EMAIL)

    for email in email_addresses:
        code = generate_code()
        try:
            invite = SiteInvitation.objects.get(email=email)
            invite.code = code
            invite.save()
        except ObjectDoesNotExist:
            SiteInvitation.objects.create(email=email, code=code)

        schedule_invite_to_site_mail(
            email=email, sender=user, message=clean_input.get("message")
        )

    return {"success": True}
