from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE
from core.lib import clean_graphql_input
from core.models import Entity
from core.resolvers import shared

mutation = ObjectType("Mutation")


@mutation.field("toggleEntityIsPinned")
def resolve_toggle_entity_is_pinned(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        entity = Entity.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not entity.group:
        raise GraphQLError(COULD_NOT_SAVE)

    if not entity.group.is_owner(user) and not entity.group.is_admin(user):
        raise GraphQLError(COULD_NOT_SAVE)

    entity.is_pinned = not entity.is_pinned
    entity.save()

    return {"success": True}
