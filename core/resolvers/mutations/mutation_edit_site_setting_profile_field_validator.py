import codecs
import csv
import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, INVALID_VALUE
from core.lib import clean_graphql_input
from core.models import ProfileFieldValidator
from core.resolvers import shared

logger = logging.getLogger(__name__)

mutation = ObjectType("Mutation")


@mutation.field("editSiteSettingProfileFieldValidator")
def resolve_edit_site_setting_profile_field_validator(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_administrator(user)

    try:
        validator = ProfileFieldValidator.objects.get(id=clean_input.get("id"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    validator_data = (
        []
        if validator.validator_type == "inList"
        else clean_input.get("validationString", "")
    )

    if validator.validator_type == "inList":
        if "validationListFile" in clean_input:
            validator_data = []
            try:
                csv_file = clean_input.get("validationListFile")
                csv_reader = csv.reader(
                    codecs.iterdecode(csv_file, "utf-8"), delimiter=";"
                )

                for row in csv_reader:
                    if row[0]:  # skip empy rows
                        validator_data.append(row[0])

            except Exception as e:
                logger.error(e)
                raise GraphQLError(INVALID_VALUE)

            validator.validator_data = validator_data
    else:
        if "validationString" in clean_input:
            validator.validator_data = clean_input.get("validationString")

    if "name" in clean_input:
        validator.name = clean_input.get("name")

    validator.save()

    return {"profileFieldValidator": validator}
