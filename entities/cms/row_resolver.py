from core.widget_resolver import WidgetSerializer, WidgetSerializerBase


class RowSerializer(WidgetSerializerBase):
    @property
    def isFullWidth(self):
        return self.data.get("isFullWidth") or False

    @property
    def backgroundColor(self):
        return self.data.get("backgroundColor") or ""

    @property
    def has_columns(self):
        return bool(self.data.get("columns"))

    @property
    def columns(self):
        return [
            ColumnSerializer(column, self.acting_user, self.writing)
            for column in self.data.get("columns", [])
        ]

    def serialize(self):
        return {
            "isFullWidth": self.isFullWidth,
            "backgroundColor": self.backgroundColor,
            "columns": [c.serialize() for c in self.columns],
        }

    def attachments(self):
        for column in self.columns:
            yield from column.attachments()

    def rich_fields(self):
        for column in self.columns:
            yield from column.rich_fields()


class ColumnSerializer(WidgetSerializerBase):
    @property
    def width(self):
        return self.data.get("width")

    @property
    def widgets(self):
        for w in self.data.get("widgets") or []:
            yield WidgetSerializer(w, self.acting_user, self.writing)

    def serialize(self):
        return {
            "width": self.width,
            "widgets": [w.serialize() for w in self.widgets],
        }

    def attachments(self):
        for widget in self.widgets:
            yield from widget.attachments()

    def rich_fields(self):
        for widget in self.widgets:
            yield from widget.rich_fields()
