from .assign_widget_ids import task as assign_widget_ids_task
from .cms_to_pages_migration import (
    migrate_configuration,
    migrate_rich_description,
    migrate_widgets,
)
from .linklist_to_links_settings import (
    task as linklist_to_links_settings_migration_task,
)
from .migrate_widget_link_items import task as migrate_widget_link_items_task
