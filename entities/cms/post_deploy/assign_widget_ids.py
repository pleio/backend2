from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.cms.models import Page
from entities.cms.row_resolver import RowSerializer


@post_deploy_action
def task():
    if is_schema_public():
        return

    for page in Page.objects.filter_campagne():
        before = page.serialize()
        page.row_repository = [
            RowSerializer(row, writing=True).serialize() for row in page.row_repository
        ]
        if page.serialize() != before:
            page.save()
