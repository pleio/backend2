import json
import uuid

from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.cms.models import Page


@post_deploy_action
def task():
    if is_schema_public():
        return

    for page in Page.objects.filter_campagne():
        before = page.serialize()

        page.update_widgets(_widget_migration)
        if page.serialize() != before:
            page.save()


def _widget_migration(widget):
    new_settings = []
    for setting in widget.get("settings", []):
        if setting["key"] == "links" and not setting.get("links"):
            new_settings.append(_link_set_migration(setting))
        else:
            new_settings.append(setting)
    widget["settings"] = new_settings
    return widget


def _link_set_migration(setting):
    result = {"key": setting["key"], "links": []}
    try:
        for link in json.loads(setting["value"]):
            link["id"] = str(uuid.uuid4())
            result["links"].append(link)
    except json.JSONDecodeError:
        pass
    return result
