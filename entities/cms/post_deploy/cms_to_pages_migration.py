import json

from post_deploy import post_deploy_action

from core import config
from core.lib import is_schema_public
from core.models import Entity
from core.utils.tiptap_parser import Tiptap
from entities.cms.models import Page


def translate_link(link):
    if link == "/cms":
        return "/pages"
    return link.replace("/cms/view/", "/page/view/")


@post_deploy_action
def migrate_rich_description():
    """
    Zoek in alle content met rich-description naar urls in relevant_urls.
    """
    if is_schema_public():
        return

    def rich_description_migration(rich_description):
        migration = RichDescriptionMigration(rich_description)
        return migration.migrate()

    for entity in Entity.objects.select_subclasses():
        entity.refresh_from_db()
        if hasattr(entity, "map_rich_text_fields"):
            before = entity.serialize()
            entity.map_rich_text_fields(rich_description_migration)
            if entity.serialize() != before:
                entity.save()


class RichDescriptionMigration:
    def __init__(self, rich_description):
        self.rich_description = rich_description

    @staticmethod
    def get_link_attrs(node):
        for mark in node.get("marks", []):
            if mark.get("type") == "link":
                return mark.get("attrs", {})
        return {}

    def migrate(self):
        parser = Tiptap(self.rich_description)
        if not parser.is_tiptap:
            return self.rich_description

        for node in parser.get_nodes("text"):
            attrs = self.get_link_attrs(node)
            if "href" in attrs:
                attrs["href"] = translate_link(attrs["href"])

        return json.dumps(parser.tiptap_json)


@post_deploy_action
def migrate_widgets():
    """
    Zoek in alle widget pagina's naar urls in relevant_urls.
    """
    if is_schema_public():
        return

    def widget_migration(widget):
        """
        lees uit welk type widget het is, en roep de juiste migratie aan.
        - CallToAction
        - Footer
        - Lead
        - LinkLijst
        """
        testers = [
            CallToActionWidgetMigration,
            FooterWidgetMigration,
            LeadWidgetMigration,
            LinkListWidgetMigration,
        ]
        for tester in testers:
            if widget.get("type") == tester.key:
                return tester(widget).migrate()
        return widget

    for page in Page.objects.filter_campagne():
        page.refresh_from_db()
        before = page.serialize()

        page.update_widgets(widget_migration)
        if page.serialize() != before:
            page.save()


class CallToActionWidgetMigration:
    key = "callToAction"

    def __init__(self, widget):
        self.widget = widget

    def migrate(self):
        """
        Zoek in de widget naar urls in relevant_urls.
        """
        for setting in self.widget.get("settings", []):
            if setting["key"] == "link":
                setting["value"] = translate_link(setting.get("value"))

        return self.widget


class FooterWidgetMigration:
    key = "footer"

    def __init__(self, widget):
        self.widget = widget

    def migrate(self):
        """
        Zoek in de widget naar urls in relevant_urls.
        """
        for setting in self.widget.get("settings", []):
            try:
                if setting["key"] != "links":
                    continue
                links = json.loads(setting["value"])
                for link in links:
                    if "url" in link:
                        link["url"] = translate_link(link.get("url"))
                setting["value"] = json.dumps(links)
            except json.JSONDecodeError:
                pass

        return self.widget


class LeadWidgetMigration(CallToActionWidgetMigration):
    key = "lead"


class LinkListWidgetMigration(FooterWidgetMigration):
    key = "linkList"


@post_deploy_action
def migrate_configuration():
    """
    Zoek in de configuratie naar oude-stijl urls.

    - INITIATIVE_LINK ✅(type=str)
    - DIRECT_LINKS ✅ (type=dict) [{"link": "/cms/view/b64e1e60-1f2c-47c0-92e2-22541b616a59/1-objecten-met-tag-widget", "title": "Known page"}]
    - FOOTER ✅ (type=dict) [{"link": "/cms/view/b64e1e60-1f2c-47c0-92e2-22541b616a59/1-objecten-met-tag-widget", "title": "Onderaan"}, {"link": "/cms/view/b64e1e60-1f2c-47c0-92e2-22541b616a59/1-objecten-met-tag-widget", "title": "Tweede"}]
    - MENU ✅ (type=dict) elk item heeft een "link"; recursief uitvoeren op "children"
    """
    if is_schema_public():
        return

    InitiativeLinkConfigurationMigration().migrate()
    DirectLinksConfigurationMigration().migrate()
    FooterConfigurationMigration().migrate()
    MenuConfigurationMigration().migrate()


class InitiativeLinkConfigurationMigration:
    @staticmethod
    def migrate():
        config.INITIATIVE_LINK = translate_link(config.INITIATIVE_LINK)


class DirectLinksConfigurationMigration:
    @staticmethod
    def _migrate(value):
        new_links = []
        for link in value:
            link["link"] = translate_link(link.get("link"))
            new_links.append(link)
        return new_links

    def migrate(self):
        config.DIRECT_LINKS = self._migrate(config.DIRECT_LINKS)


class FooterConfigurationMigration(DirectLinksConfigurationMigration):
    def migrate(self):
        config.FOOTER = self._migrate(config.FOOTER)


class MenuConfigurationMigration:
    @classmethod
    def _migrate(cls, menu):
        for item in menu:
            if "link" in item:
                item["link"] = translate_link(item.get("link"))
            if "children" in item:
                item["children"] = cls._migrate(item.get("children", []))
        return menu

    def migrate(self):
        config.MENU = self._migrate(config.MENU)
