from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.cms.models import Page


@post_deploy_action
def task():
    if is_schema_public():
        return

    for page in Page.objects.filter_campagne():
        before = page.serialize()

        page.update_widgets(_widget_migration)

        if page.serialize() != before:
            page.save()


def _widget_migration(widget):
    for setting in widget.get("settings", []):
        if setting.get("links"):
            for link in setting["links"]:
                link["title"] = link["label"]
                del link["label"]
    return widget
