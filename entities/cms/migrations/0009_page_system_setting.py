# Generated by Django 4.2.18 on 2025-01-21 14:31

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cms", "0008_remove_column_page_remove_column_position_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="page",
            name="system_setting",
            field=models.BooleanField(default=False),
        ),
    ]
