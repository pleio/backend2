from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from entities.cms.models import Page
from user.models import User


def TextPageFactory(**attributes) -> Page:
    return _common_page_factory(page_type="text", **attributes)


def CampagnePageFactory(**attributes) -> Page:
    return _common_page_factory(page_type="campagne", **attributes)


def _common_page_factory(**attributes) -> Page:
    assert isinstance(attributes.get("owner"), User), "owner is a required property"
    assert Page.can_add(attributes["owner"], attributes.get("group")), (
        "The page-owner should be editor or, when it is group-content, member and editor or group-owner."
    )
    attributes.setdefault("read_access", [ACCESS_TYPE.public])
    attributes.setdefault(
        "write_access", [ACCESS_TYPE.user.format(attributes["owner"].guid)]
    )
    return mixer.blend(Page, **attributes)
