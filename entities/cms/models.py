import logging
import uuid
from copy import deepcopy

from auditlog.registry import auditlog
from django.db import models
from model_utils.managers import InheritanceQuerySet

from core.entity_access import (
    DenyAnonymousVisitors,
    GrantAdministrators,
    GrantAuthorizedUsers,
    GrantUsersThatCreate,
)
from core.models import AttachmentMixin, Entity, RevisionMixin
from core.models.entity import EntityManager
from core.models.mixin import (
    HierarchicalEntityMixin,
    RichDescriptionMediaMixin,
    TitleMixin,
)
from core.models.rich_fields import ReplaceAttachments
from core.widget import widget_to_text
from core.widget_resolver import WidgetSerializer
from entities.cms.row_resolver import RowSerializer

logger = logging.getLogger(__name__)


class PageQuerySet(InheritanceQuerySet):
    def filter_campagne(self):
        return self.filter(page_type="campagne")

    def filter_text(self):
        return self.exclude(page_type="campagne")


class PageManager(EntityManager):
    def get_queryset(self):
        return PageQuerySet(model=Page, using=self._db)

    def filter_campagne(self):
        return self.get_queryset().filter_campagne()

    def filter_text(self):
        return self.get_queryset().filter_text()


class Page(
    HierarchicalEntityMixin,
    RichDescriptionMediaMixin,
    TitleMixin,
    AttachmentMixin,
    RevisionMixin,
    Entity,
):
    """
    Page for CMS
    """

    PAGE_TYPES = (("campagne", "Campagne"), ("text", "Text"))

    class Meta:
        # When positions are equal sort old -> new (used for menu's)
        ordering = ["position", "published"]

    objects = PageManager()

    rich_description = models.TextField(null=True, blank=True)

    page_type = models.CharField(max_length=256, choices=PAGE_TYPES)
    parent = models.ForeignKey(
        "self", blank=True, null=True, related_name="children", on_delete=models.CASCADE
    )

    row_repository = models.JSONField(null=True, default=list)

    system_setting = models.BooleanField(default=False)

    position = models.IntegerField(null=False, default=0)

    def has_children(self):
        if self.children.count() > 0:
            return True
        return False

    def get_children(self):
        return self.children.all()

    def get_parent(self):
        return self.parent

    def _get_write_validators(self, user):
        return [
            DenyAnonymousVisitors(user, entity=self),
            GrantAdministrators(user, entity=self),
            GrantUsersThatCreate(user, entity=self),
            GrantAuthorizedUsers(user, entity=self),
        ]

    def can_delete(self, user):
        if self.group and self.group._start_page_id == self.id:
            return False
        return self.can_write(user)

    @classmethod
    def can_add(cls, user, group=None):
        if group and (group.is_owner(user) or group.is_admin(user)):
            return True

        return super().can_add(user, group) and (user.is_editor or user.is_site_admin)

    def has_revisions(self):
        return self.page_type == "text"

    def __str__(self):
        return f"Page[{self.title}]"

    @property
    def url(self):
        return "{}/page/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    @property
    def type_to_string(self):
        return "page"

    @property
    def parents(self):
        parents = []
        child = self
        while child.parent:
            if child.parent in parents:
                break
            parents.append(child.parent)
            child = child.parent
        return list(reversed(parents))

    @property
    def rich_fields(self):
        return list(self.attachments_from_rich_fields())

    def widgets_to_text(self):
        lines = []

        def add_widget_to_stream(widget):
            widget_text = widget_to_text(widget)
            if widget_text:
                lines.append(widget_text)
            return widget

        self.update_widgets(add_widget_to_stream)

        return "\n\n".join(lines)

    def description_index_value(self):
        description = super().description_index_value()
        if self.is_campaignpage:
            description = self.widgets_to_text() + "\n\n" + description
        return description.strip()

    def attachments_from_rich_fields(self):
        if self.rich_description:
            yield self.rich_description
        for row in self.row_repository or []:
            yield from RowSerializer(row).rich_fields()

    def lookup_attachments(self):
        yield from super().lookup_attachments()
        yield from self.attachments_in_rows()

    def attachments_in_rows(self):
        for row in [RowSerializer(r) for r in self.row_repository]:
            yield from row.attachments()

    def update_widgets(self, callback):
        new_rows = []
        for row in self.row_repository:
            new_columns = []
            for column in row["columns"]:
                column["widgets"] = [callback(w) for w in column.get("widgets") or []]
                new_columns.append(column)
            row["columns"] = new_columns
            new_rows.append(row)
        self.row_repository = new_rows

    def replace_attachments(self, attachment_map: ReplaceAttachments):
        # not covered.
        super().replace_attachments(attachment_map)

        def update_attachments(widget):
            new_settings = []
            for setting in widget.get("settings", []):
                current_id = setting.get("attachmentId")
                if attachment_map.has_attachment(current_id):
                    setting["attachmentId"] = attachment_map.translate(current_id)
                if setting["key"] == "richDescription" or setting.get(
                    "richDescription"
                ):
                    setting["richDescription"] = attachment_map.replace(
                        setting["richDescription"] or setting["value"]
                    )
                    setting["value"] = None
                new_settings.append(setting)
            widget["settings"] = new_settings
            return widget

        self.update_widgets(update_attachments)

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)

        def change_rich_widget(widget):
            widget_serializer = WidgetSerializer(widget)
            widget_serializer.map_rich_fields(callback)
            return widget_serializer.serialize()

        if self.row_repository:
            self.update_widgets(change_rich_widget)

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "parentGuid": self.parent.guid if self.parent else "",
            "position": self.position,
            "rows": deepcopy(self.row_repository),
            **super().serialize(),
        }

    def get_media_status(self):
        return self.is_textpage and super().get_media_status()

    @property
    def is_textpage(self):
        return self.page_type == "text"

    @property
    def is_campaignpage(self):
        return self.page_type == "campagne"


class Column(models.Model):
    """
    Obsolete model, can not be removed because of old migrations.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


auditlog.register(Page)
