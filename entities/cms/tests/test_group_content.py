from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_group_content import Template
from entities.cms.factories import CampagnePageFactory
from user.factories import EditorFactory, UserFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return CampagnePageFactory(**kwargs)

    def build_owner(self):
        return EditorFactory()


class TestPagesAreProtectedWhenQueriedViaGroupMenuTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.visitor = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.group.join(self.visitor)

        self.hidden_page = CampagnePageFactory(
            owner=self.owner,
            group=self.group,
            read_access=["user:{}".format(self.owner.guid)],
        )
        self.available_page = CampagnePageFactory(
            owner=self.owner,
            group=self.group,
        )
        self.group.menu.append(
            {
                "type": "page",
                "id": self.hidden_page.guid,
            }
        )
        self.group.menu.append(
            {
                "type": "page",
                "id": self.available_page.guid,
            }
        )
        self.group.save()

        self.query = """
        query GetGroupPage($guid: String!) {
            entity(guid: $guid) {
                ... on Group {
                    menu {
                    ...MenuItemParts
                    }
                }
            }
        }
        fragment MenuItemParts on GroupMenuItem {
            id
            ... on GroupMenuPageItem {
                page { guid }
            }
        }
        """
        self.variables = {"guid": self.group.guid}

    def test_pages_are_protected_when_queried_via_group_menu(self):
        self.graphql_client.force_login(self.visitor)
        result = self.graphql_client.post(self.query, self.variables)
        self.assertEqual(
            result["data"]["entity"]["menu"],
            [
                {"id": "members"},
                {
                    "id": self.hidden_page.guid,
                    "page": None,
                },
                {
                    "id": self.available_page.guid,
                    "page": {"guid": self.available_page.guid},
                },
            ],
        )
