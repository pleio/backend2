from core.tests.helpers import PleioTenantTestCase
from entities.cms.row_resolver import RowSerializer


class TestRowSerializerTestCase(PleioTenantTestCase):
    def test_empty_row(self):
        row = RowSerializer({})
        self.assertFalse(row.isFullWidth)
        self.assertEqual(row.backgroundColor, "")
        self.assertFalse(row.has_columns)
        self.assertEqual(row.columns, [])

    def test_is_full_width_property(self):
        row = RowSerializer({"isFullWidth": True})
        self.assertTrue(row.isFullWidth)

    def test_background_color_property(self):
        row = RowSerializer({"backgroundColor": "blah"})
        self.assertEqual(row.backgroundColor, "blah")

    def test_has_no_columns_property(self):
        row = RowSerializer({"columns": []})
        self.assertFalse(row.has_columns)

    def test_has_columns_property(self):
        row = RowSerializer({"columns": [{}]})
        self.assertTrue(row.has_columns)
