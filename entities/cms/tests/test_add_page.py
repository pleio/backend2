from unittest import mock

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import CampagnePageFactory, TextPageFactory
from entities.file.models import FileFolder
from user.factories import AdminFactory, EditorFactory, UserFactory


class AddTextPageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.user = UserFactory()
        self.admin = AdminFactory()
        self.editor = EditorFactory()
        self.page = TextPageFactory(owner=self.editor)

        self.mutation = """
            mutation AddPage($input: addPageInput!) {
                addPage(input: $input) {
                    entity {
                        guid
                        ... on Page {
                            inputLanguage
                            isTranslationEnabled
                            showOwner
                            pageType
                            canEdit
                            canArchiveAndDelete
                            title
                            url
                            richDescription
                            tags
                            parent {
                                guid
                            }
                            accessId
                            timePublished
                            statusPublished
                            isRecommendedInSearch
                        }
                    }
                    __typename
                }
            }
        """

        self.variables = {
            "input": {
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "text",
                "pageType": "text",
                "accessId": 1,
                "tags": [],
                "isRecommendedInSearch": True,
                "richDescription": '{"blocks":[{"key":"6sb64","text":"test","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
            }
        }

    def test_add_page(self):
        for user, msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)

            entity = result["data"]["addPage"]["entity"]
            self.assertEqual(entity["showOwner"], False)
            self.assertEqual(entity["inputLanguage"], "en")
            self.assertEqual(entity["isTranslationEnabled"], False)
            self.assertEqual(entity["title"], "text", msg=msg)
            self.assertEqual(
                entity["richDescription"],
                '{"blocks":[{"key":"6sb64","text":"test","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
                msg=msg,
            )
            self.assertEqual(entity["pageType"], "text", msg=msg)
            self.assertEqual(entity["tags"], [], msg=msg)
            self.assertEqual(entity["accessId"], 1, msg=msg)
            self.assertEqual(entity["canEdit"], True, msg=msg)
            self.assertEqual(entity["canArchiveAndDelete"], True, msg=msg)
            self.assertTrue(entity["isRecommendedInSearch"])

    def test_add_sub_page(self):
        self.variables["input"]["containerGuid"] = self.page.guid
        for user, msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)

            entity = result["data"]["addPage"]["entity"]
            self.assertEqual(entity["parent"]["guid"], self.page.guid, msg=msg)
            self.page.refresh_from_db()
            children_guids = list(self.page.children.all().values_list("id", flat=True))
            self.assertIn(entity["guid"], [str(o) for o in children_guids])

    def test_add_page_by_anonymous(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_page_by_user(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_page_draft(self):
        for user, msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            self.variables["input"]["timePublished"] = None

            result = self.graphql_client.post(self.mutation, self.variables)
            entity = result["data"]["addPage"]["entity"]
            self.assertEqual(entity["statusPublished"], "draft", msg=msg)
            self.assertEqual(entity["timePublished"], None, msg=msg)


class AddCampagnePageTestCase(PleioTenantTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.admin = AdminFactory()
        self.editor = EditorFactory()
        self.page = CampagnePageFactory(owner=self.editor)

        self.mutation = """
        mutation AddPage($input: addPageInput!) {
            addPage(input: $input) {
                entity {
                    guid
                    ... on Page {
                        showOwner
                        pageType
                        title
                        url
                        richDescription
                        tags
                        parent { guid }
                        canEdit
                        canArchiveAndDelete
                        accessId
                        accessControl { ...AccessItemFragment }
                        timePublished
                        statusPublished
                        isRecommendedInSearch
                        rows { ...RowFragment }
                    }
                }
                __typename
            }
        }
        fragment AccessItemFragment on AccessItem {
            type
            grant
            guid
            ... on AccessItemUser {
                user { guid }
            }
            ... on AccessItemGroup {
                group { guid }
            }
            ... on AccessItemSubGroup {
                subGroup { id }
            }
        }
        fragment RowFragment on Row {
            isFullWidth
            backgroundColor
            columns {
                width
                widgets {
                    type
                    settings {
                        key
                        value
                        richDescription
                        attachment {
                            id
                            mimeType
                            url
                            downloadUrl
                            name
                        }
                    }
                }
            }
        }
        """

        self.variables = {
            "input": {
                "title": "test",
                "pageType": "campagne",
                "accessId": 1,
                "isRecommendedInSearch": True,
                "richDescription": '{"blocks":[{"key":"6sb64","text":"test","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
            }
        }

    def test_add_campaign_page_by_admin(self):
        for user, msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)
            entity = result["data"]["addPage"]["entity"]

            self.assertEqual(entity["showOwner"], False)
            self.assertEqual(entity["title"], "test", msg=msg)
            self.assertEqual(
                entity["richDescription"],
                '{"blocks":[{"key":"6sb64","text":"test","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}',
                msg=msg,
            )
            self.assertEqual(entity["pageType"], "campagne", msg=msg)
            self.assertEqual(entity["tags"], [], msg=msg)
            self.assertEqual(entity["accessId"], 1, msg=msg)
            self.assertEqual(entity["canEdit"], True, msg=msg)
            self.assertEqual(entity["canArchiveAndDelete"], True, msg=msg)
            self.assertEqual(entity["parent"], None, msg=msg)
            self.assertTrue(entity["isRecommendedInSearch"])

    def test_add_campaign_page_by_anonymous(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_campaign_page_by_user(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_sub_page(self):
        self.variables["input"]["containerGuid"] = self.page.guid
        for user, msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)

            entity = result["data"]["addPage"]["entity"]
            self.assertEqual(entity["parent"]["guid"], self.page.guid, msg=msg)
            self.page.refresh_from_db()
            children_guids = list(self.page.children.all().values_list("id", flat=True))
            self.assertIn(entity["guid"], [str(o) for o in children_guids])

    @mock.patch("entities.file.models.FileFolder.scan")
    def test_add_page_with_widgets(self, mocked_scan):
        file_mock = self.use_mock_file("attachment.jpg")
        file_mock.name = "attachment.jpg"
        file_mock.content_type = "image/jpg"

        self.variables["input"]["rows"] = [
            {
                "isFullWidth": True,
                "backgroundColor": "orange",
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "demo",
                                "settings": [
                                    {"key": "title", "value": "Demo widget"},
                                    {
                                        "key": "richDescription",
                                        "richDescription": self.tiptap_paragraph(
                                            "Some description"
                                        ),
                                    },
                                    {
                                        "key": "attachment",
                                        "attachment": file_mock,
                                    },
                                ],
                            }
                        ]
                    }
                ],
            }
        ]

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)
        rows = result["data"]["addPage"]["entity"]["rows"]
        expected_attachment: FileFolder = FileFolder.objects.first()
        self.assertDictEqual(
            {"data": rows},
            {
                "data": [
                    {
                        "isFullWidth": True,
                        "backgroundColor": "orange",
                        "columns": [
                            {
                                "width": None,
                                "widgets": [
                                    {
                                        "type": "demo",
                                        "settings": [
                                            {
                                                "key": "title",
                                                "value": "Demo widget",
                                                "richDescription": None,
                                                "attachment": None,
                                            },
                                            {
                                                "key": "richDescription",
                                                "value": None,
                                                "richDescription": self.tiptap_paragraph(
                                                    "Some description"
                                                ),
                                                "attachment": None,
                                            },
                                            {
                                                "key": "attachment",
                                                "value": None,
                                                "richDescription": None,
                                                "attachment": {
                                                    "id": str(expected_attachment.id),
                                                    "mimeType": expected_attachment.mime_type,
                                                    "name": expected_attachment.title,
                                                    "url": expected_attachment.attachment_url,
                                                    "downloadUrl": expected_attachment.download_url,
                                                },
                                            },
                                        ],
                                    }
                                ],
                            }
                        ],
                    }
                ]
            },
        )
        self.assertTrue(mocked_scan.called)

    def test_add_campaign_draft_page_by_admin(self):
        for user, msg in ((self.admin, "as admin"), (self.editor, "as editor")):
            self.graphql_client.force_login(user)
            self.variables["input"]["timePublished"] = None
            result = self.graphql_client.post(self.mutation, self.variables)
            entity = result["data"]["addPage"]["entity"]
            self.assertEqual(entity["statusPublished"], "draft", msg=msg)
            self.assertEqual(entity["timePublished"], None, msg=msg)

    def test_complex_access_specification(self):
        group = GroupFactory(owner=self.user)
        subgroup = group.subgroups.create(name="Test")
        other_user = UserFactory()

        self.variables["input"]["accessControl"] = [
            {"type": "loggedIn", "grant": "read"},
            {"type": "group", "grant": "read", "guid": group.guid},
            {"type": "subGroup", "grant": "readWrite", "guid": str(subgroup.id)},
            {"type": "user", "grant": "readWrite", "guid": other_user.guid},
        ]

        self.graphql_client.force_login(self.editor)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["addPage"]["entity"]
        self.assertEqual(
            entity["accessControl"],
            [
                {
                    "type": "loggedIn",
                    "grant": "read",
                    "guid": None,
                },
                {
                    "type": "group",
                    "grant": "read",
                    "guid": group.guid,
                    "group": {"guid": group.guid},
                },
                {
                    "type": "subGroup",
                    "grant": "readWrite",
                    "guid": subgroup.access_id,
                    "subGroup": {"id": subgroup.id},
                },
                {
                    "type": "user",
                    "grant": "readWrite",
                    "guid": other_user.guid,
                    "user": {"guid": other_user.guid},
                },
            ],
        )


class TestAddCampagnePageToGroup(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.mutation = """
        mutation AddPage($input: addPageInput!) {
            addPage(input: $input) {
                entity {
                    ...PageFragment
                }
            }
        }
        fragment PageFragment on Page {
            inGroup
            group {
                guid
            }
        }
        """

        self.variables = {
            "input": {
                "pageType": "campagne",
                "title": "Foo bar",
                "groupGuid": self.group.guid,
            }
        }

    def test_add_group(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)

        self.assertEqual(result["data"]["addPage"]["entity"]["inGroup"], True)
        self.assertEqual(
            result["data"]["addPage"]["entity"]["group"]["guid"], self.group.guid
        )
