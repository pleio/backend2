import json
import uuid
from contextlib import contextmanager

from django.contrib.auth.models import AnonymousUser
from django.test import tag
from django.utils.text import slugify

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import CampagnePageFactory, TextPageFactory
from entities.file.factories import FileFactory
from entities.file.models import FileFolder
from user.factories import AdminFactory, EditorFactory, UserFactory


class PageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = EditorFactory()
        self.user2 = EditorFactory()
        self.page_parent = TextPageFactory(
            owner=self.user1,
            title="Test parent page",
            rich_description="JSON to string",
        )
        self.page_child = TextPageFactory(
            owner=self.user1,
            title="Test child page",
            rich_description="JSON to string",
            parent=self.page_parent,
        )
        self.page_child2 = TextPageFactory(
            owner=self.user2,
            read_access=[ACCESS_TYPE.user.format(self.user2.id)],
            write_access=[ACCESS_TYPE.user.format(self.user2.id)],
            title="Test child page other user",
            rich_description="JSON to string",
            parent=self.page_parent,
        )
        self.page_child_child = TextPageFactory(
            owner=self.user1,
            title="Test child of child page",
            rich_description="JSON to string",
            parent=self.page_child,
        )
        self.page_draft = TextPageFactory(
            owner=self.user1,
            title="Test draft page",
            rich_description="JSON to string",
            published=None,
        )

        self.query = """
            query PageItem($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...PageDetailFragment
                    __typename
                }
            }

            fragment PageDetailFragment on Page {
                pageType
                isTranslationEnabled
                canEdit
                canArchiveAndDelete
                preventDeleteAndArchive
                title
                url
                richDescription
                tags
                timePublished
                statusPublished
                isRecommendedInSearch
                accessId
                parent {
                    guid
                }
                hasChildren
                children {
                    guid
                    title
                    canEdit
                    children {
                        guid
                        title
                        canEdit
                        children {
                            guid
                            title
                        }
                        owner {
                            guid
                            name
                        }
                    }
                    owner {
                        guid
                        name
                    }
                }
                owner {
                    guid
                    name
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_parent_page_by_anonymous(self):
        variables = {"guid": self.page_parent.guid}
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["title"], "Test parent page")
        self.assertEqual(data["entity"]["isTranslationEnabled"], True)
        self.assertEqual(data["entity"]["richDescription"], "JSON to string")
        self.assertEqual(data["entity"]["tags"], [])
        self.assertEqual(data["entity"]["accessId"], 2)
        self.assertEqual(data["entity"]["canEdit"], False)
        self.assertEqual(data["entity"]["canArchiveAndDelete"], False)
        self.assertEqual(data["entity"]["preventDeleteAndArchive"], True)
        self.assertEqual(data["entity"]["parent"], None)
        self.assertEqual(data["entity"]["hasChildren"], True)
        self.assertEqual(
            data["entity"]["url"],
            "/page/view/{}/{}".format(
                self.page_parent.guid, slugify(self.page_parent.title)
            ),
        )
        self.assertEqual(len(data["entity"]["children"]), 1)
        self.assertEqual(data["entity"]["children"][0]["guid"], self.page_child.guid)
        self.assertEqual(
            data["entity"]["children"][0]["owner"]["guid"], self.user1.guid
        )
        self.assertEqual(
            data["entity"]["children"][0]["children"][0]["guid"],
            self.page_child_child.guid,
        )
        self.assertEqual(
            data["entity"]["children"][0]["children"][0]["owner"]["guid"],
            self.user1.guid,
        )
        self.assertEqual(data["entity"]["owner"]["guid"], self.user1.guid)
        self.assertEqual(data["entity"]["owner"]["name"], self.user1.name)

    def test_child_page_by_owner(self):
        variables = {"guid": self.page_child.guid}

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, variables)
        data = result["data"]

        self.assertEqual(data["entity"]["title"], "Test child page")
        self.assertEqual(data["entity"]["richDescription"], "JSON to string")
        self.assertEqual(data["entity"]["tags"], [])
        self.assertEqual(data["entity"]["accessId"], 2)
        self.assertEqual(data["entity"]["canEdit"], True)
        self.assertEqual(data["entity"]["canArchiveAndDelete"], True)
        self.assertEqual(data["entity"]["parent"]["guid"], self.page_parent.guid)
        self.assertEqual(data["entity"]["hasChildren"], True)
        self.assertEqual(
            data["entity"]["url"],
            "/page/view/{}/{}".format(
                self.page_child.guid, slugify(self.page_child.title)
            ),
        )
        self.assertEqual(
            data["entity"]["children"][0]["guid"], self.page_child_child.guid
        )
        self.assertEqual(data["entity"]["owner"]["guid"], self.user1.guid)
        self.assertEqual(data["entity"]["owner"]["name"], self.user1.name)
        self.assertEqual(data["entity"]["statusPublished"], "published")

    def test_page_draft_by_owner(self):
        variables = {"guid": self.page_draft.guid}

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, variables)
        data = result["data"]

        self.assertEqual(data["entity"]["title"], "Test draft page")
        self.assertEqual(data["entity"]["richDescription"], "JSON to string")
        self.assertEqual(data["entity"]["owner"]["guid"], self.user1.guid)
        self.assertEqual(data["entity"]["owner"]["name"], self.user1.name)
        self.assertEqual(data["entity"]["timePublished"], None)
        self.assertEqual(data["entity"]["statusPublished"], "draft")
        self.assertEqual(data["entity"]["isRecommendedInSearch"], False)


@tag("TestCasesWithWidgets")
class TestCampagnePageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.user1 = EditorFactory()
        self.user2 = EditorFactory()
        self.ROWS = [
            {
                "isFullWidth": False,
                "backgroundColor": "orange",
                "columns": [
                    {
                        "width": [1],
                        "widgets": [
                            {
                                "guid": None,
                                "type": "title",
                                "settings": [
                                    {
                                        "key": "title",
                                        "value": "Foo",
                                        "links": [],
                                        "richDescription": None,
                                        "attachmentId": None,
                                    }
                                ],
                            },
                        ],
                    }
                ],
            },
            {"isFullWidth": False, "backgroundColor": "orange", "columns": []},
        ]

        # Rows come back from the API a little different.
        self.ROWS_RESULT = json.loads(json.dumps(self.ROWS))
        del self.ROWS_RESULT[0]["columns"][0]["widgets"][0]["settings"][0][
            "attachmentId"
        ]
        self.ROWS_RESULT[0]["columns"][0]["widgets"][0]["settings"][0]["attachment"] = (
            None
        )
        self.ROWS_RESULT[0]["columns"][0]["widgets"][0]["userSettings"] = []

        self.page_parent = CampagnePageFactory(
            owner=self.user1, row_repository=self.ROWS
        )

        self.page_child = CampagnePageFactory(
            owner=self.user1,
            title="Test child page",
            rich_description="JSON to string",
            parent=self.page_parent,
        )
        self.page_child2 = CampagnePageFactory(
            owner=self.user2,
            read_access=[ACCESS_TYPE.user.format(self.user2.id)],
            write_access=[ACCESS_TYPE.user.format(self.user2.id)],
            title="Test child page",
            rich_description="JSON to string",
            parent=self.page_parent,
        )
        self.page_child_child = CampagnePageFactory(
            owner=self.user1,
            title="Test child of child page",
            rich_description="JSON to string",
            parent=self.page_child,
        )

        self.page_draft = CampagnePageFactory(
            owner=self.user1,
            title="Test parent page",
            rich_description="JSON to string",
            published=None,
        )
        self.query = """
            query PageItem($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ...PageDetailFragment
                    __typename
                }
            }

            fragment PageDetailFragment on Page {
                pageType
                title
                timePublished
                statusPublished
                canEdit
                canArchiveAndDelete
                preventDeleteAndArchive
                isRecommendedInSearch
                parent {
                    guid
                }
                hasChildren
                children {
                    guid
                    title
                    canEdit
                    children {
                        guid
                        title
                        canEdit
                        children {
                            guid
                            title
                        }
                        owner {
                            guid
                            name
                        }
                    }
                    owner {
                        guid
                        name
                    }
                }
                owner {
                    guid
                    name
                }
                rows {
                    isFullWidth
                    backgroundColor
                    columns {
                        width
                        widgets {
                            guid
                            type
                            settings { ...WidgetSettingFragment }
                            userSettings { ...WidgetSettingFragment }
                        }
                    }
                }
            }
            fragment WidgetSettingFragment on WidgetSetting {
                key
                value
                richDescription
                links { id }
                attachment {
                    id
                    mimeType
                    url
                    name
                }
            }
        """

    def test_load_campagne_page(self):
        variables = {"guid": self.page_parent.guid}
        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.maxDiff = None

        self.assertEqual(entity["guid"], self.page_parent.guid)
        self.assertEqual(entity["pageType"], "campagne")
        self.assertEqual(entity["title"], self.page_parent.title)
        self.assertEqual(entity["owner"]["guid"], self.user1.guid)
        self.assertEqual(entity["rows"], [self.ROWS_RESULT[0]])
        self.assertEqual(entity["statusPublished"], "published")
        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])
        self.assertFalse(entity["preventDeleteAndArchive"])
        self.assertFalse(entity["isRecommendedInSearch"])

    def test_parent_page_by_anonymous(self):
        variables = {"guid": self.page_parent.guid}
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["entity"]["guid"], str(self.page_parent.guid))
        self.assertEqual(data["entity"]["parent"], None)
        self.assertEqual(data["entity"]["hasChildren"], True)
        self.assertEqual(len(data["entity"]["children"]), 1)
        self.assertEqual(data["entity"]["children"][0]["guid"], self.page_child.guid)
        self.assertEqual(
            data["entity"]["children"][0]["owner"]["guid"], self.user1.guid
        )
        self.assertEqual(
            data["entity"]["children"][0]["children"][0]["guid"],
            self.page_child_child.guid,
        )
        self.assertEqual(
            data["entity"]["children"][0]["children"][0]["owner"]["guid"],
            self.user1.guid,
        )
        self.assertEqual(data["entity"]["owner"]["guid"], self.user1.guid)
        self.assertEqual(data["entity"]["owner"]["name"], self.user1.name)

    def test_load_campagne_draft_page(self):
        variables = {"guid": self.page_draft.guid}

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], self.page_draft.guid)
        self.assertEqual(entity["pageType"], "campagne")
        self.assertEqual(entity["title"], self.page_draft.title)
        self.assertEqual(entity["owner"]["guid"], self.user1.guid)
        self.assertEqual(entity["timePublished"], None)
        self.assertEqual(entity["statusPublished"], "draft")

    def test_load_group_start_page(self):
        group = GroupFactory(owner=self.user1)
        variables = {"guid": group.start_page.guid}

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])
        self.assertTrue(entity["preventDeleteAndArchive"])


@tag("TestCasesWithWidgets")
class TestCampagnePageWithUserSettings(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory(email="owner@example.com")
        self.actor = UserFactory(email="actor@example.com")

        self.attachment1 = FileFactory(owner=self.owner)
        self.attachment2 = FileFactory(owner=self.actor)

        self.actor.profile.set_widget_settings(
            "1234", [{"key": "attachment", "attachmentId": self.attachment2.guid}]
        )
        self.actor.save()

        self.page = CampagnePageFactory(
            owner=self.owner,
            row_repository=[
                {
                    "columns": [
                        {
                            "widgets": [
                                {
                                    "guid": "1234",
                                    "type": "custom",
                                    "settings": [
                                        {
                                            "key": "attachment",
                                            "attachmentId": self.attachment1.guid,
                                        }
                                    ],
                                }
                            ]
                        }
                    ]
                }
            ],
        )

        self.query = """
            query PageItem($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ...PageDetailFragment
                }
            }
            fragment PageDetailFragment on Page {
                rows {
                    columns {
                        widgets {
                            guid
                            settings { ...WidgetFragment }
                            userSettings { ...WidgetFragment }
                        }
                    }
                }
            }
            fragment WidgetFragment on WidgetSetting {
                key
                attachment { id }
            }
        """

    def test_load_campagne_page_with_user_settings(self):
        self.graphql_client.force_login(self.actor)
        result = self.graphql_client.post(self.query, {"guid": self.page.guid})

        self.assertHasUserSpecificSettings(result)

    def test_load_campagne_page_without_user_settings_at_the_page_owner(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"guid": self.page.guid})

        self.assertHasNotUserSpecificSettings(result)

    def test_load_campagne_page_without_user_settings_for_another_user(self):
        self.graphql_client.force_login(UserFactory())
        result = self.graphql_client.post(self.query, {"guid": self.page.guid})

        self.assertHasNotUserSpecificSettings(result)

    def assertHasUserSpecificSettings(self, result):
        """
        Test that there are userSettings with the attachment id of attachment2 and a normal widget with the original attachment.
        """
        widget = result["data"]["entity"]["rows"][0]["columns"][0]["widgets"][0]

        self.assertEqual(widget["guid"], "1234")
        self.assertEqual(
            widget["userSettings"][0]["attachment"]["id"],
            self.attachment2.guid,
        )
        self.assertEqual(
            widget["settings"][0]["attachment"]["id"], self.attachment1.guid
        )

    def assertHasNotUserSpecificSettings(self, result):
        """
        Test that there aren't userSettings but only a normal widget with the original attachment.
        """
        widget = result["data"]["entity"]["rows"][0]["columns"][0]["widgets"][0]

        self.assertEqual(widget["guid"], "1234")
        self.assertEqual(
            widget["settings"][0]["attachment"]["id"], self.attachment1.guid
        )
        self.assertEqual(widget["userSettings"], [])


class TestPagePropertiesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory()
        self.page = CampagnePageFactory(owner=self.owner)

    def create_attachment(self):
        return FileFolder.objects.create(
            upload=self.use_mock_file("attachment.jpg"), owner=self.owner
        )

    def test_zero_attachments_via_rows(self):
        found_attachments = list(self.page.lookup_attachments())
        self.assertQuerysetEqual(found_attachments, [])

    def test_one_attachment_via_rows(self):
        attachment = self.create_attachment()
        self.page.row_repository = [
            {
                "columns": [
                    {"widgets": [{"settings": [{"attachmentId": str(attachment.id)}]}]}
                ]
            }
        ]
        found_attachments = list(self.page.lookup_attachments())
        self.assertQuerysetEqual(found_attachments, [str(attachment.pk)])

    def test_rich_text_fields(self):
        self.page.rich_description = self.tiptap_paragraph("rich_description")
        self.page.row_repository = [
            {
                "columns": [
                    {
                        "widgets": [
                            {
                                "settings": [
                                    {
                                        "richDescription": self.tiptap_paragraph(
                                            "widget_rich_description"
                                        )
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]

        self.assertEqual(
            self.page.rich_fields,
            [
                self.tiptap_paragraph("rich_description"),
                self.tiptap_paragraph("widget_rich_description"),
            ],
        )


class TestCampagnePageWithAdvancedAccessSpecification(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(IS_CLOSED=False)

        self.owner = EditorFactory(email="owner@example.com")
        self.user = UserFactory(email="user@example.com")
        self.editor = EditorFactory(email="editor@example.com")
        self.admin = AdminFactory(email="admin@example.com")

        self.page = CampagnePageFactory(owner=self.owner)

        self.query = """
        query EntityQuery($guid: String!) {
            entity(guid: $guid) {
                guid
                ... on Page {
                    canEdit
                    canArchiveAndDelete
                    accessControl { type }
                }
            }
        }
        """
        self.variables = {"guid": self.page.guid}

    def test_standard_access(self):
        """
        Test that only owner, editor and admin have write-access when a Page is
        configured in the default pleio behaviour.
        """
        self.page.write_access = []
        self.page.save()

        self.assertValidGenericBehaviour()

        with self.post_query(self.user) as result:
            self.assertNoAccess(result)
            self.assertCanNotViewAccessDetails(result)

    def test_logged_in_access(self):
        """
        Test that all authenticated visitors have write-access when a Page is
        configured to allow write-access to logged in users.
        """
        self.page.write_access = [ACCESS_TYPE.logged_in]
        self.page.save()

        self.assertSafeAccessToAllBehaviour()

    def test_public_access(self):
        """
        Test that only authenticated visitors have write-access when a Page is
        configured in the most open configuration possible.
        """
        self.page.write_access = [ACCESS_TYPE.public]
        self.page.save()

        self.assertSafeAccessToAllBehaviour()

    def test_group_access(self):
        """
        Test that members of a group have write-access when a Page is
        configured to support group access.
        """
        group = GroupFactory(owner=self.owner)
        group_member = UserFactory(email="group_member@example.com")
        group.join(group_member)

        self.page.write_access = [ACCESS_TYPE.group.format(group.guid)]
        self.page.save()

        self.assertValidGenericBehaviour()

        with self.post_query(group_member) as result:
            self.assertAccess(result)
            self.assertCanViewAccessDetails(result)

        with self.post_query(self.user) as result:
            self.assertNoAccess(result)
            self.assertCanNotViewAccessDetails(result)

    def test_subgroup_access(self):
        """
        Test that members of subgroups have write-access when a Page is configured to
        grant access to members of the given subgroup.
        """
        group = GroupFactory(owner=self.owner)
        subgroup = group.subgroups.create(name="Test")
        group_member = UserFactory(email="group_member@example.com")
        subgroup_member = UserFactory(email="subgroupmember@example.com")
        group.join(group_member)
        group.join(subgroup_member)
        subgroup.members.add(subgroup_member)

        self.page.write_access = [ACCESS_TYPE.subgroup.format(subgroup.access_id)]
        self.page.save()

        self.assertValidGenericBehaviour()

        for user in [self.user, group_member]:
            with self.post_query(user) as result:
                self.assertNoAccess(result)
                self.assertCanNotViewAccessDetails(result)

        with self.post_query(subgroup_member) as result:
            self.assertAccess(result)
            self.assertCanViewAccessDetails(result)

    def test_user_access(self):
        """
        Test that an authorized user has write-access when a Page is configured to
        grant access to the given user.
        """
        authorized_user = UserFactory(email="authorized_user@example.com")

        self.page.write_access = [ACCESS_TYPE.user.format(authorized_user.guid)]
        self.page.save()

        self.assertValidGenericBehaviour()

        with self.post_query(self.user) as result:
            self.assertNoAccess(result)
            self.assertCanNotViewAccessDetails(result)

        with self.post_query(authorized_user) as result:
            self.assertAccess(result)
            self.assertCanViewAccessDetails(result)

    """
    Helper methods.
    """

    def assertValidGenericBehaviour(self):
        """
        Always behave like below - whatever the setup of the application:
        """
        with self.post_query(AnonymousUser()) as result:
            self.assertNoAccess(result)
            self.assertCanNotViewAccessDetails(result)

        for user in [self.owner, self.admin, self.editor]:
            with self.post_query(user) as result:
                self.assertAccess(result)
                self.assertCanViewAccessDetails(result)

    def assertSafeAccessToAllBehaviour(self):
        """
        Assert a not-recommended, but theoretically possible access setting.
        """
        self.assertValidGenericBehaviour()

        with self.post_query(self.user) as result:
            self.assertAccess(result)
            self.assertCanViewAccessDetails(result)

    def assertAccess(self, result):
        self.assertTrue(
            result["data"]["entity"]["canEdit"],
            msg=self.build_msg("has no write access"),
        )

    def assertCanViewAccessDetails(self, result):
        self.assertTrue(
            len(result["data"]["entity"]["accessControl"]) > 0,
            msg=self.build_msg("has no access to access details"),
        )

    def assertNoAccess(self, result):
        self.assertFalse(
            result["data"]["entity"]["canEdit"], msg=self.build_msg("has write access")
        )

    def assertCanNotViewAccessDetails(self, result):
        self.assertEqual(
            result["data"]["entity"]["accessControl"],
            [],
            msg=self.build_msg("has access to access details"),
        )

    @contextmanager
    def post_query(self, user):
        self.active_user = user
        self.graphql_client.force_login(user)
        yield self.graphql_client.post(self.query, self.variables)

    def build_msg(self, msg):
        return "{} unexpectedly {}".format(
            getattr(self.active_user, "email", "[anonymous]"), msg
        )


class TestCampagnePageWithAndWithoutExcerptTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory()
        self.page = CampagnePageFactory(owner=self.owner)

        self.query = """
        query EntityQuery($guid: String!) {
            entity(guid: $guid) {
                guid
                ... on Page {
                    excerpt
                }
            }
        }
        """
        self.variables = {"guid": self.page.guid}

    def test_page_without_abstract(self):
        self.page.rich_description = None
        self.page.save()

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["excerpt"], "")

    def test_page_with_abstract(self):
        self.page.rich_description = self.tiptap_paragraph("Rich description")
        self.page.save()

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["excerpt"], "Rich description")


@tag("TestCasesWithWidgets")
class TestCampagnePageWidgetGuids(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory()

        self.mutation = """
        fragment PageDetailFragment on Page {
            rows {
                columns {
                    widgets {
                        guid
                    }
                }
            }
        }
        mutation EditPage($input: editPageInput!) {
            editPage(input: $input) {
                entity { ...PageDetailFragment }
            }
        }
        """

    def test_add_guid_if_new(self):
        rows = [{"isFullWidth": False, "columns": [{"widgets": [{"type": "title"}]}]}]
        page = CampagnePageFactory(owner=self.owner)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(
            self.mutation, {"input": {"guid": page.guid, "rows": rows}}
        )

        self.assertEqual(
            len(
                result["data"]["editPage"]["entity"]["rows"][0]["columns"][0][
                    "widgets"
                ][0]["guid"]
            ),
            36,
        )

    def test_keep_guid_if_exists(self):
        exprected_id = str(uuid.uuid4())
        rows = [
            {
                "isFullWidth": False,
                "columns": [{"widgets": [{"guid": exprected_id, "type": "title"}]}],
            }
        ]
        page = CampagnePageFactory(owner=self.owner)
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(
            self.mutation, {"input": {"guid": page.guid, "rows": rows}}
        )

        self.assertEqual(
            result["data"]["editPage"]["entity"]["rows"][0]["columns"][0]["widgets"][0][
                "guid"
            ],
            exprected_id,
        )
