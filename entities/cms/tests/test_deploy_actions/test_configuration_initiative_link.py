from core import config
from core.tests.helpers import PleioTenantTestCase


class TestMigrateInitiativeLinkTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            INITIATIVE_LINK="/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
        )
        self.expected_content = (
            "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
        )

    def test_migrate(self):
        from ...post_deploy.cms_to_pages_migration import (
            InitiativeLinkConfigurationMigration,
        )

        InitiativeLinkConfigurationMigration().migrate()
        self.assertEqual(config.INITIATIVE_LINK, self.expected_content)
