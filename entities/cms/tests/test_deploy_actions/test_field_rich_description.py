from core.tests.helpers import PleioTenantTestCase

from . import Wrapper


class TestMigrateRichDescription(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.content = Wrapper.compact_json(
            """
        {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "attrs": {
                "intro": false
              },
              "content": [
                {
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                        "target": "_blank",
                        "rel": null,
                        "class": "mark-link"
                      }
                    }
                  ],
                  "text": "Dit is een tekst widget"
                },
                {
                  "type": "text",
                  "text": " Die opent in een nieuw tabblad"
                }
              ]
            },
            {
              "type": "paragraph",
              "attrs": {
                "intro": false
              },
              "content": [
                {
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/cms/view/166d7ba3-a1b3-4ea6-b106-68f3fcfcd9bb/eerste-niveau",
                        "target": null,
                        "rel": null,
                        "class": "mark-link"
                      }
                    }
                  ],
                  "text": "Deze andere link opent in hetzelfde scherm"
                }
              ]
            },
            {
              "type": "paragraph",
              "attrs": {
                "intro": false
              },
              "content": [
                {
                  "type": "text",
                  "marks": [
                    {
                      "type": "anchor",
                      "attrs": {
                        "id": "plek-op-het-scherm"
                      }
                    }
                  ],
                  "text": "En deze geeft gewoon een plek op het scherm aan"
                }
              ]
            },
            {
              "type": "table",
              "content": [
                {
                  "type": "tableRow",
                  "content": [
                    {
                      "type": "tableHeader",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "text": "Kop"
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "tableHeader",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "text": "Voet"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableRow",
                  "content": [
                    {
                      "type": "tableCell",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "marks": [
                                {
                                  "type": "link",
                                  "attrs": {
                                    "href": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                                    "target": null,
                                    "rel": null,
                                    "class": "mark-link"
                                  }
                                }
                              ],
                              "text": "Met een link"
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "tableCell",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "text": "Zonder link"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
        """
        )
        self.expected_result = Wrapper.compact_json(
            """
        {
          "type": "doc",
          "content": [
            {
              "type": "paragraph",
              "attrs": {
                "intro": false
              },
              "content": [
                {
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                        "target": "_blank",
                        "rel": null,
                        "class": "mark-link"
                      }
                    }
                  ],
                  "text": "Dit is een tekst widget"
                },
                {
                  "type": "text",
                  "text": " Die opent in een nieuw tabblad"
                }
              ]
            },
            {
              "type": "paragraph",
              "attrs": {
                "intro": false
              },
              "content": [
                {
                  "type": "text",
                  "marks": [
                    {
                      "type": "link",
                      "attrs": {
                        "href": "/page/view/166d7ba3-a1b3-4ea6-b106-68f3fcfcd9bb/eerste-niveau",
                        "target": null,
                        "rel": null,
                        "class": "mark-link"
                      }
                    }
                  ],
                  "text": "Deze andere link opent in hetzelfde scherm"
                }
              ]
            },
            {
              "type": "paragraph",
              "attrs": {
                "intro": false
              },
              "content": [
                {
                  "type": "text",
                  "marks": [
                    {
                      "type": "anchor",
                      "attrs": {
                        "id": "plek-op-het-scherm"
                      }
                    }
                  ],
                  "text": "En deze geeft gewoon een plek op het scherm aan"
                }
              ]
            },
            {
              "type": "table",
              "content": [
                {
                  "type": "tableRow",
                  "content": [
                    {
                      "type": "tableHeader",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "text": "Kop"
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "tableHeader",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "text": "Voet"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableRow",
                  "content": [
                    {
                      "type": "tableCell",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "marks": [
                                {
                                  "type": "link",
                                  "attrs": {
                                    "href": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                                    "target": null,
                                    "rel": null,
                                    "class": "mark-link"
                                  }
                                }
                              ],
                              "text": "Met een link"
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "tableCell",
                      "attrs": {
                        "colspan": 1,
                        "rowspan": 1,
                        "colwidth": null,
                        "width": null
                      },
                      "content": [
                        {
                          "type": "paragraph",
                          "attrs": {
                            "intro": false
                          },
                          "content": [
                            {
                              "type": "text",
                              "text": "Zonder link"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
        """
        )

    def test_migrate_rich_description(self):
        from entities.cms.post_deploy.cms_to_pages_migration import (
            RichDescriptionMigration,
        )

        migrator = RichDescriptionMigration(self.content)
        result = migrator.migrate()

        self.maxDiff = None
        self.assertEqual(result, self.expected_result)
