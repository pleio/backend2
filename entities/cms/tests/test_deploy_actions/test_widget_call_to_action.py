from core.tests.helpers import PleioTenantTestCase


class TestMigrateCallToAction(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.content = {
            "type": "callToAction",
            "settings": [
                {
                    "key": "link",
                    "value": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

        self.expected_content = {
            "type": "callToAction",
            "settings": [
                {
                    "key": "link",
                    "value": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

    def test_migrate(self):
        """
        Zoek in de widget naar urls in relevant_urls.
        """
        from ...post_deploy.cms_to_pages_migration import CallToActionWidgetMigration

        result = CallToActionWidgetMigration(self.content).migrate()
        self.assertEqual(result, self.expected_content)
