from core.tests.helpers import PleioTenantTestCase


class TestMigrateLead(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.content = {
            "type": "lead",
            "settings": [
                {
                    "key": "link",
                    "value": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

        self.expected_content = {
            "type": "lead",
            "settings": [
                {
                    "key": "link",
                    "value": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

    def test_migrate(self):
        from ...post_deploy.cms_to_pages_migration import LeadWidgetMigration

        result = LeadWidgetMigration(self.content).migrate()
        self.assertEqual(result, self.expected_content)
