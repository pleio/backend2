from core import config
from core.lib import get_full_url
from core.tests.helpers import PleioTenantTestCase


class TestMigrateDirectLinksTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            FOOTER=[
                {
                    "link": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                    "label": "Korte link",
                },
                {
                    "link": get_full_url(
                        "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
                    ),
                    "label": "Lange link",
                },
            ]
        )

        self.expected_content = [
            {
                "link": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                "label": "Korte link",
            },
            {
                "link": get_full_url(
                    "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
                ),
                "label": "Lange link",
            },
        ]

    def test_migrate(self):
        from ...post_deploy.cms_to_pages_migration import FooterConfigurationMigration

        FooterConfigurationMigration().migrate()
        self.assertEqual(config.FOOTER, self.expected_content)
