from core import config
from core.lib import get_full_url
from core.tests.helpers import PleioTenantTestCase


class TestMigrateDirectLinksTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            MENU=[
                {
                    "link": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                    "label": "Korte link",
                    "children": [
                        {
                            "label": "Meer menu items",
                            "children": [
                                {
                                    "link": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                                    "label": "Nog een korte link",
                                }
                            ],
                        }
                    ],
                },
                {
                    "label": "Lange link",
                    "link": get_full_url(
                        "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
                    ),
                    "children": [],
                },
            ]
        )

        self.expected_content = [
            {
                "link": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                "label": "Korte link",
                "children": [
                    {
                        "label": "Meer menu items",
                        "children": [
                            {
                                "link": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                                "label": "Nog een korte link",
                            }
                        ],
                    }
                ],
            },
            {
                "label": "Lange link",
                "link": get_full_url(
                    "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
                ),
                "children": [],
            },
        ]

    def test_migrate(self):
        from ...post_deploy.cms_to_pages_migration import MenuConfigurationMigration

        MenuConfigurationMigration().migrate()
        self.assertEqual(config.MENU, self.expected_content)
