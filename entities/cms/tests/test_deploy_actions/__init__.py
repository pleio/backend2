import json
from unittest import mock

from core.tests.helpers import PleioTenantTestCase


class Wrapper:
    @staticmethod
    def compact_json(json_string):
        return json.dumps(json.loads(json_string))

    class BaseTestCase(PleioTenantTestCase):
        pass
