import json

from core.lib import get_full_url
from core.tests.helpers import PleioTenantTestCase


class TestMigrateLinkList(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.content = {
            "type": "linkList",
            "settings": [
                {
                    "key": "links",
                    "value": json.dumps(
                        [
                            {
                                "url": "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                                "label": "Korte link",
                            },
                            {
                                "url": get_full_url(
                                    "/cms/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
                                ),
                                "label": "Lange link",
                            },
                        ]
                    ),
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }
        self.expected_content = {
            "type": "linkList",
            "settings": [
                {
                    "key": "links",
                    "value": json.dumps(
                        [
                            {
                                "url": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                                "label": "Korte link",
                            },
                            {
                                "url": get_full_url(
                                    "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets"
                                ),
                                "label": "Lange link",
                            },
                        ]
                    ),
                    "attachmentId": None,
                    "richDescription": None,
                }
            ],
        }

    def test_migrate(self):
        from ...post_deploy.cms_to_pages_migration import LinkListWidgetMigration

        result = LinkListWidgetMigration(self.content).migrate()
        self.assertEqual(result, self.expected_content)
