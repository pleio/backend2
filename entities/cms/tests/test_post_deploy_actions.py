import uuid
from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from entities.cms.post_deploy.linklist_to_links_settings import _widget_migration


class TestLinkListToLinksMigrationTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.EXPECTED_UUID = uuid.uuid4()
        self.mocked_uuid = mock.patch(
            "entities.cms.post_deploy.linklist_to_links_settings.uuid.uuid4",
            return_value=self.EXPECTED_UUID,
        ).start()

    def test_footer_widget_migration(self):
        widget = {
            "type": "footer",
            "settings": [
                {
                    "key": "title",
                    "value": "",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "links",
                    "value": '[{"url": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets", "label": "Korte link"}, {"url": "https://test1.pleio-test.nl/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets", "label": "Volledige link"}]',
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }
        self.maxDiff = None
        self.assertEqual(
            _widget_migration(widget),
            {
                "settings": [
                    {
                        "attachmentId": None,
                        "key": "title",
                        "richDescription": None,
                        "value": "",
                    },
                    {
                        "key": "links",
                        "links": [
                            {
                                "id": str(self.EXPECTED_UUID),
                                "label": "Korte link",
                                "url": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                            },
                            {
                                "id": str(self.EXPECTED_UUID),
                                "label": "Volledige link",
                                "url": "https://test1.pleio-test.nl/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                            },
                        ],
                    },
                ],
                "type": "footer",
            },
        )

    def test_linklist_widget_migration(self):
        widget = {
            "type": "linkList",
            "settings": [
                {
                    "key": "title",
                    "value": "Link lijst",
                    "attachmentId": None,
                    "richDescription": None,
                },
                {
                    "key": "links",
                    "value": '[{"url": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets", "label": "Korte link"}, {"url": "https://test1.pleio-test.nl/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets", "label": "Lange link"}]',
                    "attachmentId": None,
                    "richDescription": None,
                },
            ],
        }
        self.maxDiff = None
        self.assertEqual(
            _widget_migration(widget),
            {
                "settings": [
                    {
                        "attachmentId": None,
                        "key": "title",
                        "richDescription": None,
                        "value": "Link lijst",
                    },
                    {
                        "key": "links",
                        "links": [
                            {
                                "id": str(self.EXPECTED_UUID),
                                "label": "Korte link",
                                "url": "/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                            },
                            {
                                "id": str(self.EXPECTED_UUID),
                                "label": "Lange link",
                                "url": "https://test1.pleio-test.nl/page/view/d400cf1a-1901-4a26-82f2-dc16f43723ab/alle-widgets",
                            },
                        ],
                    },
                ],
                "type": "linkList",
            },
        )
