from django.contrib.auth.models import AnonymousUser

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import CampagnePageFactory
from user.factories import AdminFactory, EditorFactory, UserFactory


class TestDeletePageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = EditorFactory()
        self.query = """
        mutation deletePage($input: deleteEntityInput!) {
            deleteEntity(input: $input) {
                success
            }
        }
        """

    def test_delete_page(self):
        for user, msg in (
            (self.owner, "Owner"),
            (EditorFactory(), "Another editor"),
            (AdminFactory(), "An administrator"),
        ):
            page = CampagnePageFactory(owner=self.owner)
            self.graphql_client.force_login(user)
            response = self.graphql_client.post(
                self.query, {"input": {"guid": page.guid}}
            )
            self.assertEqual(response["data"]["deleteEntity"]["success"], True, msg)

    def test_not_deleting_page(self):
        page = CampagnePageFactory(owner=self.owner)
        for user, msg in (
            (AnonymousUser(), "Anonymous user"),
            (UserFactory(), "Another user"),
        ):
            self.graphql_client.force_login(user)
            with self.assertGraphQlError(msg=msg):
                self.graphql_client.post(self.query, {"input": {"guid": page.guid}})


class TestDeleteGroupPage(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.query = """
        mutation deleteEntity($input: deleteEntityInput!) {
            deleteEntity(input: $input) {
                success
            }
        }
        """

    def test_delete_group_landing_page(self):
        self.graphql_client.force_login(self.owner)

        with self.assertGraphQlError("could_not_delete"):
            self.graphql_client.post(
                self.query, {"input": {"guid": self.group.start_page.guid}}
            )

    def test_delete_normal_page(self):
        page = CampagnePageFactory(owner=self.owner, group=self.group)
        self.graphql_client.force_login(self.owner)

        response = self.graphql_client.post(self.query, {"input": {"guid": page.guid}})
        self.assertEqual(response["data"]["deleteEntity"]["success"], True)
