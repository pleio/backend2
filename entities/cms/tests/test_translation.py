import logging

from core.tests.helpers.test_translation import Wrapper
from entities.cms.factories import TextPageFactory
from user.factories import EditorFactory

logger = logging.getLogger(__name__)


class TestTextPageTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Page"
    EXPECTED_ORIGINAL_EXCERPT = "Rich description"

    def build_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def build_entity(self, **kwargs):
        try:
            return TextPageFactory(**kwargs)
        except Exception:
            from traceback import format_exc

            logger.error(format_exc())
