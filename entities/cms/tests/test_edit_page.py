from unittest import mock

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.lib import (
    entity_exists_in_parent_tree,
    get_access_id,
    is_max_parent_depth_reached,
)
from core.tests.helpers import PleioTenantTestCase
from entities.cms.factories import CampagnePageFactory, TextPageFactory
from entities.file.models import FileFolder
from user.factories import AdminFactory, EditorFactory, UserFactory


class EditTextPageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.user = UserFactory()
        self.editor = EditorFactory()
        self.parent_page1 = TextPageFactory(owner=self.editor)
        self.parent_page2 = TextPageFactory(owner=self.editor)
        self.page = TextPageFactory(owner=self.editor, parent=self.parent_page1)

        self.mutation = """
        mutation EditPage($input: editPageInput!) {
            editPage(input: $input) {
                entity {
                    guid
                    ... on Page {
                        inputLanguage
                        isTranslationEnabled
                        pageType
                        canEdit
                        canArchiveAndDelete
                        preventDeleteAndArchive
                        title
                        url
                        richDescription
                        tags
                        parent {
                            guid
                        }
                        accessId
                        timePublished
                        statusPublished
                        isRecommendedInSearch
                    }
                    __typename
                }
                __typename
            }
        }
        """
        self.variables = {
            "input": {
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "guid": self.page.guid,
                "title": "test",
                "accessId": 1,
                "tags": ["tag_1"],
                "richDescription": self.tiptap_paragraph("Test123"),
                "containerGuid": self.parent_page2.guid,
                "isRecommendedInSearch": True,
            }
        }

    def assert_text_page_update_ok(self):
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPage"]["entity"]

        self.assertEqual(
            entity["inputLanguage"], self.variables["input"]["inputLanguage"]
        )
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], self.variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], self.variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], self.variables["input"]["tags"])
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["preventDeleteAndArchive"], False)
        self.assertEqual(entity["parent"]["guid"], str(self.parent_page2.id))
        self.assertEqual(entity["isRecommendedInSearch"], True)

    def test_edit_group_page_by_superadmin(self):
        group = GroupFactory(owner=self.user)
        self.page.group = group
        self.page.save()

        self.graphql_client.force_login(
            UserFactory(
                name="indended superadmin",
                email="intended-superadmin@example.com",
                is_superadmin=True,
            )
        )
        self.assert_text_page_update_ok()

    def test_edit_page_by_admin(self):
        self.graphql_client.force_login(AdminFactory())
        self.assert_text_page_update_ok()

    def test_edit_page_by_editor(self):
        self.graphql_client.force_login(self.editor)
        self.assert_text_page_update_ok()

    def test_edit_page_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_page_by_user(self):
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_draft_page_by_admin(self):
        self.graphql_client.force_login(AdminFactory())
        self.variables["input"]["timePublished"] = None
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPage"]["entity"]

        self.assertEqual(entity["title"], self.variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], self.variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], self.variables["input"]["tags"])
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["parent"]["guid"], str(self.parent_page2.id))
        self.assertEqual(entity["timePublished"], None)
        self.assertEqual(entity["statusPublished"], "draft")


class EditCampagnePageTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.editor = EditorFactory()
        self.admin = AdminFactory()
        self.parent_page1 = TextPageFactory(owner=self.editor)
        self.parent_page2 = TextPageFactory(owner=self.editor)
        self.page = CampagnePageFactory(owner=self.editor, parent=self.parent_page1)
        self.sub_page = CampagnePageFactory(owner=self.editor, parent=self.page)

        self.mutation = """
            fragment AttachmentFragment on Attachment {
                id
                mimeType
                url
                downloadUrl
                name
            }
            fragment LinkFragment on WidgetLink {
                id
                title
                description
                url
                image { ... AttachmentFragment }
                imageAlt
                buttonText
            }
            mutation EditPage($input: editPageInput!) {
                editPage(input: $input) {
                    entity {
                        guid
                        ... on Page {
                            pageType
                            canEdit
                            canArchiveAndDelete
                            title
                            url
                            richDescription
                            tags
                            parent {
                                guid
                            }
                            accessId
                            timePublished
                            statusPublished
                            isRecommendedInSearch
                            rows {
                                isFullWidth
                                backgroundColor
                                columns {
                                    width
                                    widgets {
                                        type
                                        settings {
                                            key
                                            value
                                            richDescription
                                            attachment { ... AttachmentFragment }
                                            links { ... LinkFragment }
                                        }
                                    }
                                }
                            }
                        }
                        __typename
                    }
                    __typename
                }
            }
        """
        self.variables = {
            "input": {
                "guid": self.page.guid,
                "title": "test",
                "accessId": 1,
                "tags": ["tag_1"],
                "richDescription": self.tiptap_paragraph("Test123"),
                "containerGuid": self.parent_page2.guid,
                "isRecommendedInSearch": True,
            }
        }

    def assert_text_page_update_ok(self):
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPage"]["entity"]

        self.assertEqual(entity["title"], self.variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], self.variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], self.variables["input"]["tags"])
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["parent"]["guid"], str(self.parent_page2.id))
        self.assertEqual(entity["rows"], [])
        self.assertTrue(entity["isRecommendedInSearch"])

    @mock.patch("entities.file.models.FileFolder.scan")
    def test_update_rows(self, mocked_scan):
        file_mock = self.use_mock_file("attachment.jpg")
        file_mock.name = "attachment.jpg"
        file_mock.content_type = "image/jpg"
        file_mock2 = self.use_mock_file("attachment2.jpg")
        file_mock2.name = "attachment2.jpg"
        file_mock2.content_type = "image/jpg"

        self.variables["input"]["rows"] = [
            {
                "isFullWidth": True,
                "backgroundColor": "orange",
                "columns": [
                    {
                        "widgets": [
                            {
                                "type": "demo",
                                "settings": [
                                    {
                                        "key": "title",
                                        "value": "Demo widget",
                                    },
                                    {
                                        "key": "richDescription",
                                        "richDescription": self.tiptap_paragraph(
                                            "Some description"
                                        ),
                                    },
                                    {
                                        "key": "attachment",
                                        "attachment": file_mock,
                                    },
                                    {
                                        "key": "linkList",
                                        "links": [
                                            {
                                                "title": "Foo",
                                                "description": "Bar baz cuq",
                                                "url": "https://example.com",
                                                "image": file_mock2,
                                                "imageAlt": "Alternative text",
                                                "buttonText": "Click me",
                                            }
                                        ],
                                    },
                                ],
                            }
                        ]
                    }
                ],
            }
        ]

        self.graphql_client.force_login(AdminFactory())

        result = self.graphql_client.post(self.mutation, self.variables)
        rows = result["data"]["editPage"]["entity"]["rows"]
        attachments = FileFolder.objects.order_by("created_at")
        expected_attachment: FileFolder = attachments.first()
        expected_link_image: FileFolder = attachments.last()

        self.assertEqual(len(rows), 1)
        self.assertEqual(rows[0]["isFullWidth"], True)
        self.assertEqual(rows[0]["backgroundColor"], "orange")
        self.assertEqual(len(rows[0]["columns"]), 1)
        self.assertEqual(rows[0]["columns"][0]["width"], None)
        self.assertEqual(len(rows[0]["columns"][0]["widgets"]), 1)

        widget = rows[0]["columns"][0]["widgets"][0]
        self.assertEqual(widget["type"], "demo")
        self.assertEqual(len(widget["settings"]), 4)

        settings = widget["settings"]

        self.assertEqual(settings[0]["key"], "title")
        self.assertEqual(settings[0]["value"], "Demo widget")

        self.assertEqual(settings[1]["key"], "richDescription")
        self.assertEqual(
            settings[1]["richDescription"],
            self.tiptap_paragraph("Some description"),
        )

        self.assertEqual(settings[2]["key"], "attachment")
        self.assertEqual(settings[2]["attachment"]["id"], str(expected_attachment.id))
        self.assertEqual(
            settings[2]["attachment"]["mimeType"],
            expected_attachment.mime_type,
        )
        self.assertEqual(settings[2]["attachment"]["name"], expected_attachment.title)
        self.assertEqual(
            settings[2]["attachment"]["url"],
            expected_attachment.attachment_url,
        )
        self.assertEqual(
            settings[2]["attachment"]["downloadUrl"],
            expected_attachment.download_url,
        )
        self.assertEqual(settings[3]["key"], "linkList")
        self.assertEqual(len(settings[3]["links"]), 1)
        links = settings[3]["links"]
        self.assertIsNotNone(links[0]["id"])
        self.assertEqual(links[0]["buttonText"], "Click me")
        self.assertEqual(links[0]["title"], "Foo")
        self.assertEqual(links[0]["imageAlt"], "Alternative text")
        self.assertEqual(links[0]["description"], "Bar baz cuq")
        self.assertEqual(links[0]["url"], "https://example.com")
        self.assertEqual(
            links[0]["image"],
            {
                "id": str(expected_link_image.id),
                "mimeType": expected_link_image.mime_type,
                "name": expected_link_image.title,
                "url": expected_link_image.attachment_url,
                "downloadUrl": expected_link_image.download_url,
            },
        )

        self.assertTrue(mocked_scan.called)

        attachments = [*self.page.attachments.all().values_list("file__id", flat=True)]
        self.assertIn(expected_attachment.id, attachments)
        self.assertIn(expected_link_image.id, attachments)

    def test_edit_page_by_admin(self):
        self.graphql_client.force_login(self.admin)
        self.assert_text_page_update_ok()

    def test_edit_page_by_editor(self):
        self.graphql_client.force_login(self.editor)
        self.assert_text_page_update_ok()

    def test_edit_page_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_page_by_user(self):
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_draft_page_by_admin(self):
        self.graphql_client.force_login(AdminFactory())
        self.variables["input"]["timePublished"] = None
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPage"]["entity"]

        self.assertEqual(entity["title"], self.variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], self.variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], self.variables["input"]["tags"])
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["parent"]["guid"], str(self.parent_page2.id))
        self.assertEqual(entity["rows"], [])
        self.assertEqual(entity["timePublished"], None)
        self.assertEqual(entity["statusPublished"], "draft")

    def test_is_max_parent_depth_reached(self):
        self.assertEqual(is_max_parent_depth_reached(self.sub_page, max_depth=1), True)
        self.assertEqual(is_max_parent_depth_reached(self.sub_page, max_depth=3), False)

    def test_entity_exists_in_parent_tree(self):
        new_parent_page = CampagnePageFactory(owner=self.editor)
        self.assertEqual(
            entity_exists_in_parent_tree(self.parent_page1, self.sub_page), True
        )
        self.assertEqual(
            entity_exists_in_parent_tree(self.sub_page, new_parent_page), False
        )


class TestAdvancedAccessControlForCampagnePages(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(IS_CLOSED=False)

        self.owner = EditorFactory(email="owner@example.com")
        self.user = UserFactory(email="user@example.com")
        self.group = GroupFactory(owner=self.owner)
        self.subgroup = self.group.subgroups.create(name="Test")
        self.page = CampagnePageFactory(owner=self.owner)

        self.mutation = """
        mutation EditCampagnePage($input: editPageInput!) {
            editPage(input: $input) {
                entity {
                    guid
                    ... on Page {
                        accessId
                        canEdit
                        canArchiveAndDelete
                        accessControl { ...AccessItemFragment }
                    }
                }
            }
        }
        fragment AccessItemFragment on AccessItem {
            type
            grant
            guid
            ... on AccessItemUser {
                user { guid }
            }
            ... on AccessItemGroup {
                group { guid }
            }
            ... on AccessItemSubGroup {
                subGroup { id }
            }
        }
        """

        self.variables = {
            "input": {
                "guid": self.page.guid,
                "accessControl": [
                    {"type": "group", "guid": self.group.guid, "grant": "read"},
                    {
                        "type": "subGroup",
                        "grant": "readWrite",
                        "guid": str(self.subgroup.id),
                    },
                    {
                        "type": "user",
                        "grant": "readWrite",
                        "guid": self.user.guid,
                    },
                ],
            }
        }

    def test_access_with_plain_access_setup(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPage"]["entity"]

        self.maxDiff = None

        self.assertEqual(
            entity,
            {
                "guid": self.page.guid,
                # Expect public access as primary access-id
                "accessId": 4,
                "canEdit": True,
                "canArchiveAndDelete": True,
                "accessControl": [
                    {
                        "type": "group",
                        "grant": "read",
                        "guid": self.group.guid,
                        "group": {"guid": self.group.guid},
                    },
                    {
                        "type": "subGroup",
                        "grant": "readWrite",
                        "guid": self.subgroup.access_id,
                        "subGroup": {"id": self.subgroup.id},
                    },
                    {
                        "type": "user",
                        "grant": "readWrite",
                        "guid": self.user.guid,
                        "user": {"guid": self.user.guid},
                    },
                ],
            },
        )


class TestRecursiveAccessControlForTextpages(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory(email="owner@example.com")

        self.initial_access = {
            "read_access": [ACCESS_TYPE.user.format(self.owner.guid)],
            "write_access": [ACCESS_TYPE.user.format(self.owner.guid)],
        }
        self.changed_access = {
            "read_access": [ACCESS_TYPE.public],
            "write_access": [ACCESS_TYPE.logged_in],
        }
        self.grand_father = TextPageFactory(
            owner=self.owner,
            **self.initial_access,
        )
        self.father = TextPageFactory(
            owner=self.owner,
            parent=self.grand_father,
            **self.initial_access,
        )
        self.child = TextPageFactory(
            owner=self.owner,
            parent=self.father,
            **self.initial_access,
        )
        self.child2 = TextPageFactory(
            owner=self.owner,
            parent=self.father,
            **self.initial_access,
        )

        self.mutation = """
        mutation EditPage($input: editPageInput!) {
            editPage(input: $input) {
                entity { guid }
            }
        }
        """

        self.variables = {
            "input": {
                "guid": self.father.guid,
                "accessId": get_access_id(self.changed_access["read_access"]),
                "writeAccessId": get_access_id(self.changed_access["write_access"]),
            }
        }

    def assertAccess(self, entity, read_access, write_access):
        entity.refresh_from_db()
        for access_item in read_access:
            self.assertIn(
                access_item,
                entity.read_access,
                msg=f"{access_item} not in {entity.read_access}",
            )
        for access_item in write_access:
            self.assertIn(
                access_item,
                entity.write_access,
                msg=f"{access_item} not in {entity.write_access}",
            )

    def test_update_parent(self):
        self.graphql_client.force_login(self.owner)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertAccess(self.grand_father, **self.initial_access)
        self.assertAccess(self.father, **self.changed_access)
        self.assertAccess(self.child, **self.initial_access)
        self.assertAccess(self.child2, **self.initial_access)

    def test_update_parent_recursive(self):
        self.variables["input"]["isAccessRecursive"] = True
        self.graphql_client.force_login(self.owner)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertAccess(self.grand_father, **self.initial_access)
        self.assertAccess(self.father, **self.changed_access)
        self.assertAccess(self.child, **self.changed_access)
        self.assertAccess(self.child2, **self.changed_access)
