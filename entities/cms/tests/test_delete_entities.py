from core.tests.helpers.test_delete_entities import Wrapper
from entities.cms.factories import CampagnePageFactory, TextPageFactory
from user.factories import EditorFactory


class TestDeleteEntities(Wrapper.TestDeleteEntities):
    def build_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def prepare_test(self):
        self.entities = [
            TextPageFactory(owner=self.user),
            TextPageFactory(owner=self.user),
            CampagnePageFactory(owner=self.user),
            CampagnePageFactory(owner=self.user),
            CampagnePageFactory(owner=self.user),
        ]
        self.user = EditorFactory()
        text_page = self.entities[0]
        child = self.entities[1]
        child.parent = text_page
        child.save()
        widget_page = self.entities[2]

        self.all_entity_ids = [entity.id for entity in self.entities]

        self.delete_entity_ids = [text_page.guid, widget_page.guid]

        self.expected_entity_ids = [
            e.id
            for e in self.entities
            if e.id not in [text_page.id, widget_page.id, child.id]
        ]
