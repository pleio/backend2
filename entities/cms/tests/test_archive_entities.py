from core.tests.helpers.test_archive_entities import Wrapper
from entities.cms.factories import CampagnePageFactory, TextPageFactory
from user.factories import EditorFactory


class CreateTextPageMixin:
    def create_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def create_entity(self, **kwargs):
        return TextPageFactory(**kwargs)


class TestArchiveTextPageEntities(CreateTextPageMixin, Wrapper.TestArchiveEntities):
    pass


class TestArchiveWithSubpages(CreateTextPageMixin, Wrapper.TestArchiveNestedEntities):
    test_not_set_archive_date_on_children = None
    test_archive_children_directly_too_when_date_is_due = None


class TestArchiveWidgetPageEntities(Wrapper.TestArchiveEntities):
    def create_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def create_entity(self, **kwargs):
        return CampagnePageFactory(**kwargs)
