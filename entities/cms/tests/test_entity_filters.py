from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.entity_filters import Template
from entities.blog.factories import BlogFactory
from entities.cms.factories import CampagnePageFactory, TextPageFactory
from user.factories import EditorFactory


class TestTextPageFilters(Template.TestEntityFiltersTestCase):
    def get_subtype(self):
        return "page"

    def get_owner(self):
        return EditorFactory()

    def subtype_factory(self, **kwargs):
        return TextPageFactory(**kwargs)

    def reference_factory(self, **kwargs):
        return BlogFactory(**kwargs)


class TestCampagnePageFilters(Template.TestEntityFiltersTestCase):
    include_activity_query = False

    def get_subtype(self):
        return "page"

    def get_owner(self):
        return EditorFactory()

    def subtype_factory(self, **kwargs):
        return CampagnePageFactory(**kwargs)

    def reference_factory(self, **kwargs):
        return BlogFactory(**kwargs)


class TestQueryParentPagesInEntitiesQuery(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory(email="owner@example.com")
        self.parent1 = TextPageFactory(owner=self.owner, title="Parent")
        self.parent2 = TextPageFactory(owner=self.owner, title="Another parent")
        self.sub_page1 = TextPageFactory(
            owner=self.owner, title="Sub page", parent=self.parent1
        )
        self.sub_page2 = TextPageFactory(
            owner=self.owner, title="Sub page1", parent=self.parent2
        )

        self.query = """
        query QueryPagesByEntitiesQuery($parent: String, $subtypes: [String!]) {
            entities(parentGuid: $parent, subtypes: $subtypes) {
                edges {
                    guid
                }
            }
        }
        """

    def post_query(self, **kwargs):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(
            self.query,
            {
                "subtypes": ["page"],
                **kwargs,
            },
        )
        return {e["guid"] for e in result["data"]["entities"]["edges"]}

    def test_no_parent_filter_gives_back_all_pages(self):
        self.assertEqual(
            self.post_query(),
            {
                self.parent1.guid,
                self.sub_page1.guid,
                self.parent2.guid,
                self.sub_page2.guid,
            },
        )

    def test_parent_one_gives_back_only_parent_pages(self):
        self.assertEqual(
            self.post_query(parent="1"),
            {
                self.parent1.guid,
                self.parent2.guid,
            },
        )

    def test_filter_specific_parent(self):
        self.assertEqual(
            self.post_query(parent=self.parent1.guid),
            {
                self.sub_page1.guid,
            },
        )
