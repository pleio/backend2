from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_FIND,
    MAX_DEPTH_PARENT_REACHED,
    PARENT_ALREADY_IN_ANCESTORS,
)
from core.lib import (
    clean_graphql_input,
    entity_exists_in_parent_tree,
    is_max_parent_depth_reached,
)
from core.resolvers import shared
from core.utils.entity import load_entity_by_id
from entities.cms.models import Page

mutation = ObjectType("Mutation")


@mutation.field("editPage")
def resolve_edit_page(_, info, input):
    user = info.context["request"].user
    entity: Page = load_entity_by_id(input["guid"], [Page])

    clean_input = clean_graphql_input(input, ["containerGuid"])

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)

    revision = shared.resolve_start_revision(entity, user)

    shared.resolve_update_input_language(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)
    shared.resolve_update_title(entity, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.resolve_update_rows(entity, clean_input, user)
    shared.update_updated_at(entity)
    resolve_update_parent(entity, clean_input)

    entity.save()
    shared.store_update_revision(revision, entity)

    return {"entity": entity}


def resolve_update_parent(entity, clean_input):
    if "containerGuid" in clean_input:
        if clean_input.get("containerGuid") is None:
            entity.parent = None
        else:
            if is_max_parent_depth_reached(entity):
                raise GraphQLError(MAX_DEPTH_PARENT_REACHED)
            try:
                entity.parent = Page.objects.get(id=clean_input.get("containerGuid"))
                if entity_exists_in_parent_tree(entity, entity.parent):
                    raise GraphQLError(PARENT_ALREADY_IN_ANCESTORS)
            except ObjectDoesNotExist:
                raise GraphQLError(COULD_NOT_FIND)
