from ariadne import ObjectType

from core.lib import get_access_id
from core.resolvers import decorators, shared
from entities.cms.row_resolver import RowSerializer

page = ObjectType("Page")


@page.field("pageType")
def resolve_page_type(obj, info):
    return obj.page_type


@page.field("hasChildren")
def resolve_has_children(obj, info):
    return obj.has_children()


@page.field("children")
def resolve_children(obj, info):
    return obj.children.visible(info.context["request"].user)


@page.field("parent")
def resolve_parent(obj, info):
    return shared.obj_if_access(obj.parent, info)


@page.field("url")
def resolve_url(obj, info):
    return obj.url


@page.field("menu")
def resolve_menu(obj, info):
    user = info.context["request"].user
    top_parent = obj if not obj.parent else obj.parents[0]
    return build_menu(top_parent, user)


def build_menu(page, user):
    return {
        "title": page.title,
        "link": page.url,
        "guid": page.guid,
        "children": [build_menu(c, user) for c in page.children.visible(user)],
        "accessId": get_access_id(page.read_access),
    }


@page.field("preventDeleteAndArchive")
def resolve_prevent_delete_and_archive(obj, info):
    return not obj.can_delete(info.context["request"].user)


@page.field("rows")
@decorators.resolve_obj_request
def resolve_rows(obj, request):
    rows = [RowSerializer(row, request.user) for row in obj.row_repository or []]
    return filter(lambda row: row.has_columns, rows)


page.set_field("inputLanguage", shared.resolve_entity_input_language)
page.set_field("guid", shared.resolve_entity_guid)
page.set_field("status", shared.resolve_entity_status)
page.set_field("title", shared.resolve_entity_title)
page.set_field("description", shared.resolve_entity_description)
page.set_field("localDescription", shared.resolve_entity_local_description)
page.set_field("richDescription", shared.resolve_entity_rich_description)
page.set_field("excerpt", shared.resolve_entity_excerpt)
page.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
page.set_field("tags", shared.resolve_entity_tags)
page.set_field("tagCategories", shared.resolve_entity_categories)
page.set_field("timeCreated", shared.resolve_entity_time_created)
page.set_field("timeUpdated", shared.resolve_entity_time_updated)
page.set_field("timePublished", shared.resolve_entity_time_published)
page.set_field("statusPublished", shared.resolve_entity_status_published)
page.set_field("isPinned", shared.resolve_entity_is_pinned)
page.set_field("lastSeen", shared.resolve_entity_last_seen)
page.set_field("owner", shared.resolve_entity_owner)
page.set_field("group", shared.resolve_entity_group)
page.set_field("inGroup", shared.resolve_entity_in_group)
page.set_field("accessId", shared.resolve_entity_access_id)
page.set_field("accessControl", shared.resolve_entity_access_control)
page.set_field("showOwner", shared.resolve_entity_show_owner)
page.set_field("localTitle", shared.resolve_entity_local_title)
page.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
page.set_field("isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search)
page.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
page.set_field("canEdit", shared.resolve_entity_can_edit)
page.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
