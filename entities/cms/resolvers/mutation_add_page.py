from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_FIND,
    MAX_DEPTH_PARENT_REACHED,
    PARENT_ALREADY_IN_ANCESTORS,
)
from core.lib import (
    access_id_to_acl,
    clean_graphql_input,
    entity_exists_in_parent_tree,
    include_root_acl,
    is_max_parent_depth_reached,
)
from core.models import Group
from core.resolvers import shared
from entities.cms.models import Page

mutation = ObjectType("Mutation")


@mutation.field("addPage")
def resolve_add_page(_, info, input):
    clean_input = clean_graphql_input(input)
    user = info.context["request"].user

    group = _try_load_group(input.get("groupGuid"))
    shared.assert_can_create(user, Page, group)

    parent = _try_load_page(input.get("containerGuid"))
    if parent:
        shared.assert_write_access(parent, user)

    entity = Page()
    entity.page_type = clean_input["pageType"]
    entity.group = group

    entity.owner = user

    shared.resolve_add_input_language(entity, clean_input)
    shared.resolve_update_group(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.resolve_update_rows(entity, clean_input, user)
    resolve_update_access(entity, clean_input, user)
    resolve_update_parent(entity, clean_input)

    entity.save()
    shared.store_initial_revision(entity)

    return {"entity": entity}


def _try_load_group(group_id):
    try:
        if group_id:
            return Group.objects.get(id=group_id)
    except (Group.DoesNotExist, ValidationError):
        pass


def _try_load_page(page_id):
    try:
        if page_id:
            return Page.objects.get(id=page_id)
    except (Page.DoesNotExist, ValidationError):
        pass


def resolve_update_parent(entity, clean_input):
    if "containerGuid" in clean_input:
        if is_max_parent_depth_reached(entity):
            raise GraphQLError(MAX_DEPTH_PARENT_REACHED)
        try:
            entity.parent = Page.objects.get(id=clean_input.get("containerGuid"))
            if entity_exists_in_parent_tree(entity, entity.parent):
                raise GraphQLError(PARENT_ALREADY_IN_ANCESTORS)
        except ObjectDoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)


def resolve_update_access(entity, clean_input, user):
    assert entity.owner, "Owner is required to set permissions."
    shared.resolve_update_access_id(entity, clean_input, user)

    if not entity.read_access:
        entity.read_access = access_id_to_acl(entity, 0)
    if not entity.write_access:
        entity.write_access = include_root_acl(entity, [])
