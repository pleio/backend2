"""
Widget attribute resolvers
"""

from ariadne import ObjectType
from django.core.exceptions import ValidationError

from entities.file.models import FileFolder

widget_link = ObjectType("WidgetLink")

resolvers = [widget_link]


@widget_link.field("image")
def resolve_image(obj, info):
    try:
        request = info.context["request"]
        file = FileFolder.objects.get(id=obj.get("imageId"))
        if file.can_read(request.user):
            return file
    except (FileFolder.DoesNotExist, ValidationError, AttributeError):
        pass
    return None
