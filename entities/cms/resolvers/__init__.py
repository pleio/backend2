from .mutation_add_page import mutation as mutation_add_page
from .mutation_edit_page import mutation as mutation_edit_page
from .page import page
from .widget import resolvers as widget_resolvers

resolvers = [page, mutation_add_page, mutation_edit_page, *widget_resolvers]
