from django.apps import AppConfig


class QuestionConfig(AppConfig):
    name = "entities.question"
    label = "question"
