from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE
from core.lib import clean_graphql_input
from core.models import Comment
from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from core.utils.entity import load_entity_by_id
from entities.question.models import Question


def resolve_toggle_best_answer(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        comment = Comment.objects.get(id=clean_input.get("guid"))
        question = Question.objects.visible(user).get(id=comment.object_id)
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not question.can_choose_best_answer(user):
        raise GraphQLError(COULD_NOT_SAVE)

    if question.best_answer == comment:
        question.best_answer = None
    else:
        question.best_answer = comment
    question.save()

    return {"entity": question}


def resolve_toggle_item_closed(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        question = Question.objects.visible(user).get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not question.can_close(user):
        raise GraphQLError(COULD_NOT_SAVE)

    question.is_closed = not question.is_closed
    question.save()

    return {"entity": question}


def resolve_add_question(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    group = shared.get_group(clean_input)
    shared.assert_can_create(user, Question, group)

    entity = Question(owner=user, group=group)

    track_publication_date = ContentModerationTrackTimePublished(
        entity, user, is_new=True
    )

    shared.assert_can_upload_media(user, entity, clean_input)

    shared.resolve_add_input_language(entity, clean_input)
    shared.resolve_add_access_id(entity, clean_input, user)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_abstract(entity, clean_input)
    shared.update_featured_image(entity, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.update_is_featured(entity, user, clean_input)
    shared.resolve_add_suggested_items(entity, clean_input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    entity.add_follow(user)
    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}


def resolve_edit_question(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [Question])
    track_publication_date = ContentModerationTrackTimePublished(entity, user)

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)
    shared.assert_can_upload_media(user, entity, clean_input)

    shared.resolve_update_input_language(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_abstract(entity, clean_input)
    shared.update_featured_image(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.update_is_featured(entity, user, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.resolve_update_suggested_items(entity, clean_input)
    shared.update_updated_at(entity)

    if user.is_site_admin or (entity.group and entity.group.can_write(user)):
        shared.resolve_update_owner(entity, clean_input)
        shared.resolve_update_time_created(entity, clean_input)

    if user.is_site_admin:
        shared.resolve_update_group(entity, clean_input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}
