from ariadne import ObjectType

from core import config
from core.resolvers import decorators, shared

question = ObjectType("Question")


@question.field("subtype")
@decorators.resolve_obj
def resolve_subtype(obj):
    return obj.type_to_string


@question.field("isFeatured")
@decorators.resolve_obj
def resolve_is_featured(obj):
    return obj.is_featured


@question.field("isHighlighted")
@decorators.resolver_without_object_info
def resolve_is_highlighted():
    """Deprecated: not used in frontend"""
    return False


@question.field("isRecommended")
@decorators.resolve_obj
def resolve_is_recommended(obj):
    return obj.is_recommended


@question.field("url")
@decorators.resolve_obj
def resolve_url(obj):
    return obj.url


@question.field("isClosed")
@decorators.resolve_obj
def resolve_is_closed(obj):
    return obj.is_closed


@question.field("canClose")
@decorators.resolve_obj_request
def resolve_can_close(obj, request):
    return obj.can_close(request.user)


@question.field("canChooseBestAnswer")
@decorators.resolve_obj_request
def resolve_can_choose_best_answer(obj, request):
    return obj.can_choose_best_answer(request.user)


@question.field("comments")
@decorators.resolve_obj_request
def resolve_comments(obj, request):
    comments = list(obj.comments.visible(request.user))
    if obj.best_answer and config.QUESTIONER_CAN_CHOOSE_BEST_ANSWER:
        try:
            comments.remove(obj.best_answer)
            comments.insert(0, obj.best_answer)
        except Exception:
            pass
    return comments


@question.field("isLocked")
@decorators.resolve_obj
def resolve_is_locked(obj):
    return obj.is_locked


question.set_field("inputLanguage", shared.resolve_entity_input_language)
question.set_field("guid", shared.resolve_entity_guid)
question.set_field("status", shared.resolve_entity_status)
question.set_field("title", shared.resolve_entity_title)
question.set_field("localTitle", shared.resolve_entity_local_title)
question.set_field("abstract", shared.resolve_entity_abstract)
question.set_field("localAbstract", shared.resolve_entity_local_abstract)
question.set_field("description", shared.resolve_entity_description)
question.set_field("localDescription", shared.resolve_entity_local_description)
question.set_field("richDescription", shared.resolve_entity_rich_description)
question.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
question.set_field("excerpt", shared.resolve_entity_excerpt)
question.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
question.set_field("tags", shared.resolve_entity_tags)
question.set_field("tagCategories", shared.resolve_entity_categories)
question.set_field("timeCreated", shared.resolve_entity_time_created)
question.set_field("timeUpdated", shared.resolve_entity_time_updated)
question.set_field("timePublished", shared.resolve_entity_time_published)
question.set_field("statusPublished", shared.resolve_entity_status_published)
question.set_field(
    "scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity
)
question.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
question.set_field("canEdit", shared.resolve_entity_can_edit)
question.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
question.set_field("canEditAdvanced", shared.resolve_entity_can_edit_advanced)
question.set_field("canEditGroup", shared.resolve_entity_can_edit_group)
question.set_field("canComment", shared.resolve_entity_can_comment)
question.set_field("canVote", shared.resolve_entity_can_vote)
question.set_field("canBookmark", shared.resolve_entity_can_bookmark)
question.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
question.set_field("accessId", shared.resolve_entity_access_id)
question.set_field("writeAccessId", shared.resolve_entity_write_access_id)
question.set_field("featured", shared.resolve_entity_featured)
question.set_field("votes", shared.resolve_entity_votes)
question.set_field("hasVoted", shared.resolve_entity_has_voted)
question.set_field("canComment", shared.resolve_entity_can_comment)
question.set_field("commentCount", shared.resolve_entity_comment_count)
question.set_field("isFollowing", shared.resolve_entity_is_following)
question.set_field("views", shared.resolve_entity_views)
question.set_field("owner", shared.resolve_entity_owner)
question.set_field("isPinned", shared.resolve_entity_is_pinned)
question.set_field("lastSeen", shared.resolve_entity_last_seen)
question.set_field("suggestedItems", shared.resolve_entity_suggested_items)
question.set_field("group", shared.resolve_entity_group)
question.set_field("inGroup", shared.resolve_entity_in_group)
question.set_field("showOwner", shared.resolve_entity_show_owner)
question.set_field(
    "isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search
)
question.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
question.set_field("publishRequest", shared.resolve_entity_publish_request)
