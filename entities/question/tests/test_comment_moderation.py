from core.tests.helpers.test_comment_moderation import CommentModerationTestCases
from entities.question.factories import QuestionFactory


class TestAddCommentForModerationTestCase(
    CommentModerationTestCases.TestAddCommentForModerationTestCase
):
    type_to_string = "question"

    def entity_factory(self, **kwargs):
        return QuestionFactory(**kwargs)
