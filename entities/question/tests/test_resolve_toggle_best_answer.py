from django.core.cache import cache

from core.constances import ACCESS_TYPE
from core.models import Comment
from core.tests.helpers import PleioTenantTestCase
from entities.question.models import Question
from user.factories import AdminFactory, QuestionManagerFactory, UserFactory


class ToggleBestAnswerTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = UserFactory()
        self.admin = AdminFactory()
        self.question_manager = QuestionManagerFactory()

        self.question = Question.objects.create(
            title="Test1",
            rich_description="",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_closed=False,
        )

        self.answer = Comment.objects.create(
            rich_description="", owner=self.authenticatedUser, container=self.question
        )

        self.query = """
            mutation ($input: toggleBestAnswerInput!) {
                toggleBestAnswer(input: $input) {
                    entity {
                        guid
                        comments {
                            isBestAnswer
                        }
                    }
                }
            }
        """
        self.variables = {
            "input": {
                "guid": self.answer.guid,
            }
        }

    def tearDown(self):
        cache.clear()
        super().tearDown()

    def test_toggle_best_answer_owner(self):
        self.override_config(QUESTIONER_CAN_CHOOSE_BEST_ANSWER=True)

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["toggleBestAnswer"]["entity"]["guid"], self.question.guid)
        self.assertTrue(
            data["toggleBestAnswer"]["entity"]["comments"][0]["isBestAnswer"]
        )

        self.question.refresh_from_db()

        self.assertEqual(self.question.best_answer, self.answer)

        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["toggleBestAnswer"]["entity"]["guid"], self.question.guid)
        self.assertFalse(
            data["toggleBestAnswer"]["entity"]["comments"][0]["isBestAnswer"]
        )

        self.question.refresh_from_db()

        self.assertIsNone(self.question.best_answer)

    def test_toggle_best_answer_admin(self):
        self.override_config(QUESTIONER_CAN_CHOOSE_BEST_ANSWER=True)

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["toggleBestAnswer"]["entity"]["guid"], self.question.guid)
        self.assertTrue(
            data["toggleBestAnswer"]["entity"]["comments"][0]["isBestAnswer"]
        )

        self.question.refresh_from_db()

        self.assertEqual(self.question.best_answer, self.answer)

        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["toggleBestAnswer"]["entity"]["guid"], self.question.guid)
        self.assertFalse(
            data["toggleBestAnswer"]["entity"]["comments"][0]["isBestAnswer"]
        )

        self.question.refresh_from_db()

        self.assertIsNone(self.question.best_answer)

    def test_toggle_best_answer_question_manager(self):
        self.override_config(QUESTIONER_CAN_CHOOSE_BEST_ANSWER=True)

        self.graphql_client.force_login(self.question_manager)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["toggleBestAnswer"]["entity"]["guid"], self.question.guid)
        self.assertTrue(
            data["toggleBestAnswer"]["entity"]["comments"][0]["isBestAnswer"]
        )

        self.question.refresh_from_db()

        self.assertEqual(self.question.best_answer, self.answer)

        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]
        self.assertEqual(data["toggleBestAnswer"]["entity"]["guid"], self.question.guid)
        self.assertFalse(
            data["toggleBestAnswer"]["entity"]["comments"][0]["isBestAnswer"]
        )

        self.question.refresh_from_db()

        self.assertIsNone(self.question.best_answer)
