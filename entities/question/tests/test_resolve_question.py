from django.core.cache import cache
from django.utils.text import slugify
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.models import Comment
from core.tests.comment.test_comments import SpecificCommentTestCases
from core.tests.helpers import PleioTenantTestCase, override_config
from entities.question.factories import QuestionFactory
from entities.question.models import Question
from user.models import User


class QuestionTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = mixer.blend(User)

        self.questionPublic = Question.objects.create(
            title="Test public question",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_closed=False,
        )

        self.questionPrivate = Question.objects.create(
            title="Test private question",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_closed=False,
            is_featured=True,
        )

        self.comment1 = mixer.blend(Comment, container=self.questionPrivate)
        self.comment2 = mixer.blend(Comment, container=self.questionPrivate)
        self.comment3 = mixer.blend(Comment, container=self.questionPrivate)

        self.questionPrivate.best_answer = self.comment2
        self.questionPrivate.save()

        self.query = """
            fragment QuestionParts on Question {
                title
                richDescription
                excerpt
                accessId
                timeCreated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    caption
                }
                canEdit
                canArchiveAndDelete
                # TODO: test scenarios with owner, user, groupadmin and superadmin
                canEditAdvanced
                canEditGroup
                tags
                url
                isRecommendedInSearch
                isTranslationEnabled
                views
                votes
                hasVoted
                isBookmarked
                isFollowing
                isFeatured
                isClosed
                canBookmark
                canComment
                canChooseBestAnswer
                comments {
                    guid
                    richDescription
                    isBestAnswer
                    canEdit
                    timeCreated
                    hasVoted
                    votes
                }
                owner {
                    guid
                }
                group {
                    guid
                }
                isLocked
                publishRequest { guid }
            }
            query GetQuestion($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...QuestionParts
                }
            }
        """

    def tearDown(self):
        cache.clear()
        super().tearDown()

    @override_config(QUESTIONER_CAN_CHOOSE_BEST_ANSWER=True)
    def test_question_anonymous(self):
        variables = {"guid": self.questionPublic.guid}

        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.questionPublic.guid)
        self.assertEqual(entity["title"], self.questionPublic.title)
        self.assertEqual(
            entity["richDescription"], self.questionPublic.rich_description
        )
        self.assertEqual(entity["excerpt"], "JSON to string")
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(
            entity["timeCreated"], self.questionPublic.created_at.isoformat()
        )
        self.assertEqual(entity["isClosed"], self.questionPublic.is_closed)
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["views"], 0)
        self.assertEqual(entity["votes"], 0)
        self.assertEqual(entity["hasVoted"], None)
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFollowing"], False)
        self.assertEqual(entity["isFeatured"], False)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isTranslationEnabled"], True)
        self.assertEqual(entity["canBookmark"], False)
        self.assertEqual(entity["canEdit"], False)
        self.assertFalse(entity["canArchiveAndDelete"])
        self.assertEqual(entity["canComment"], False)
        self.assertEqual(entity["isLocked"], False)
        self.assertEqual(entity["canChooseBestAnswer"], False)
        self.assertEqual(entity["owner"]["guid"], self.questionPublic.owner.guid)
        self.assertEqual(
            entity["url"],
            "/questions/view/{}/{}".format(
                self.questionPublic.guid, slugify(self.questionPublic.title)
            ),
        )
        self.assertIsNotNone(entity["timePublished"])
        self.assertIsNone(entity["scheduleArchiveEntity"])
        self.assertIsNone(entity["scheduleDeleteEntity"])
        self.assertIsNone(entity["publishRequest"])

        variables = {"guid": self.questionPrivate.guid}

        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertEqual(entity, None)

    @override_config(
        QUESTIONER_CAN_CHOOSE_BEST_ANSWER=True, QUESTION_LOCK_AFTER_ACTIVITY=False
    )
    def test_question_owner(self):
        variables = {"guid": self.questionPrivate.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.questionPrivate.guid)
        self.assertEqual(entity["title"], self.questionPrivate.title)
        self.assertEqual(
            entity["richDescription"], self.questionPrivate.rich_description
        )
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(
            entity["timeCreated"], self.questionPrivate.created_at.isoformat()
        )
        self.assertEqual(entity["isClosed"], self.questionPrivate.is_closed)
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["views"], 0)
        self.assertEqual(entity["votes"], 0)
        self.assertEqual(entity["hasVoted"], None)
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFollowing"], False)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["canBookmark"], True)
        self.assertEqual(entity["canEdit"], True)
        self.assertTrue(entity["canArchiveAndDelete"])
        self.assertEqual(entity["canComment"], True)
        self.assertEqual(entity["canChooseBestAnswer"], True)
        self.assertEqual(entity["isLocked"], False)
        self.assertEqual(entity["owner"]["guid"], self.questionPrivate.owner.guid)
        self.assertEqual(
            entity["url"],
            "/questions/view/{}/{}".format(
                self.questionPrivate.guid, slugify(self.questionPrivate.title)
            ),
        )
        self.assertEqual(entity["comments"][0]["guid"], self.comment2.guid)
        self.assertIsNone(entity["publishRequest"])

    @override_config(
        QUESTIONER_CAN_CHOOSE_BEST_ANSWER=True, QUESTION_LOCK_AFTER_ACTIVITY=True
    )
    def test_question_owner_locked(self):
        variables = {"guid": self.questionPrivate.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.questionPrivate.guid)
        self.assertEqual(entity["title"], self.questionPrivate.title)
        self.assertEqual(
            entity["richDescription"], self.questionPrivate.rich_description
        )
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(
            entity["timeCreated"], self.questionPrivate.created_at.isoformat()
        )
        self.assertEqual(entity["isClosed"], self.questionPrivate.is_closed)
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["views"], 0)
        self.assertEqual(entity["votes"], 0)
        self.assertEqual(entity["hasVoted"], None)
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFollowing"], False)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["canBookmark"], True)
        self.assertEqual(entity["canEdit"], False)
        self.assertFalse(entity["canArchiveAndDelete"])
        self.assertEqual(entity["canComment"], True)
        self.assertEqual(entity["canChooseBestAnswer"], True)
        self.assertEqual(entity["isLocked"], True)
        self.assertEqual(entity["owner"]["guid"], self.questionPrivate.owner.guid)
        self.assertEqual(
            entity["url"],
            "/questions/view/{}/{}".format(
                self.questionPrivate.guid, slugify(self.questionPrivate.title)
            ),
        )
        self.assertEqual(entity["comments"][0]["guid"], self.comment2.guid)
        self.assertIsNone(entity["publishRequest"])

    def test_question_without_rich_description(self):
        self.questionPublic.rich_description = None
        self.questionPublic.save()

        self.graphql_client.force_login(self.questionPublic.owner)
        result = self.graphql_client.post(
            self.query, {"guid": self.questionPublic.guid}
        )

        self.assertEqual(result["data"]["entity"]["richDescription"], "")
        self.assertEqual(result["data"]["entity"]["excerpt"], "")

    def test_question_with_publish_request_anonymous(self):
        self.questionPublic.publish_requests.create(
            time_published=self.questionPublic.published
        )

        result = self.graphql_client.post(
            self.query, {"guid": self.questionPublic.guid}
        )
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], self.questionPublic.guid)
        self.assertIsNone(entity["publishRequest"])

    def test_question_with_publish_request_authenticated(self):
        pr = self.questionPublic.publish_requests.create(
            time_published=self.questionPublic.published
        )

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(
            self.query, {"guid": self.questionPublic.guid}
        )
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], self.questionPublic.guid)
        self.assertEqual(entity["publishRequest"]["guid"], pr.guid)


class TestCommentsInModeration(SpecificCommentTestCases.TestCommentsInModeration):
    QUERY_CONTENT_TYPE = "Question"

    def build_entity(self, **kwargs):
        return QuestionFactory(**kwargs)
