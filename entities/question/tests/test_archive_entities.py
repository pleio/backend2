from core.tests.helpers.test_archive_entities import Wrapper
from entities.question.factories import QuestionFactory as EntityFactory


class TestArchiveEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return EntityFactory(**kwargs)
