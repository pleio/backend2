from core.tests.helpers.test_translation import Wrapper
from entities.question.factories import QuestionFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Question"

    def build_entity(self, **kwargs):
        return QuestionFactory(**kwargs)
