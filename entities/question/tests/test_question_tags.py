from core.tests.helpers.tags_testcase import Template
from entities.question.models import Question


class TestQuestionTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "Question"
    model = Question
    subtype = "question"

    def variables_add(self):
        return {
            "input": {
                "title": "Any questions?",
                "subtype": "question",
            }
        }

    include_site_search = True
    include_entity_search = True
    include_activity_search = True
