from unittest.mock import patch

from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.models import Group
from core.tests._shared.mixin import MixinTestCase
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory
from user.models import User


@tag("createEntity")
class AddQuestionTestCase(MixinTestCase.AssertCanUploadMedia, PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.authenticated_user = mixer.blend(User)
        self.group = mixer.blend(
            Group, owner=self.authenticated_user, is_membership_on_request=False
        )
        self.group.join(self.authenticated_user, "owner")
        self.suggested_item = BlogFactory(owner=self.authenticated_user)

        self.data = {
            "input": {
                "subtype": "question",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "My first Question",
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "featured": {
                    "positionY": 10,
                    "video": "https://www.youtube.com/watch?v=12345",
                    "videoTitle": "testVideoTitle",
                    "alt": "testAlt",
                    "caption": "(c) Pleio, 1996",
                },
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.suggested_item.guid],
            }
        }
        self.mutation = """
            fragment QuestionParts on Question {
                showOwner
                inputLanguage
                isTranslationEnabled
                title
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                url
                inGroup
                featured {
                    image { guid }
                    video
                    videoTitle
                    videoThumbnailUrl
                    positionY
                    alt
                    caption
                }
                group {
                    guid
                }
                isClosed
                isFeatured
                isRecommendedInSearch
                suggestedItems {
                    guid
                }
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...QuestionParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_add_question(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["showOwner"], True)
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isClosed"], False)
        self.assertEqual(entity["isFeatured"], False)  # only with editor or admin role
        self.assertEqual(
            entity["isRecommendedInSearch"], False
        )  # only with editor or admin role
        self.assertEqual(entity["featured"]["positionY"], 10)
        self.assertEqual(
            entity["featured"]["video"], "https://www.youtube.com/watch?v=12345"
        )
        self.assertEqual(
            entity["featured"]["videoThumbnailUrl"],
            "https://i.ytimg.com/vi/12345/maxresdefault.jpg",
        )
        self.assertEqual(entity["featured"]["videoTitle"], "testVideoTitle")
        self.assertEqual(entity["featured"]["alt"], "testAlt")
        self.assertEqual(entity["featured"]["caption"], "(c) Pleio, 1996")
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"], [{"guid": self.suggested_item.guid}])

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

        self.assert_can_upload_media_called_once_with(
            user=self.authenticated_user,
            entity_valid=lambda e: e.type_to_string == "question"
            or ("Entity type is not question but %s" % e.type_to_string),
            input=variables["input"],
        )

    def test_add_question_to_group(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["inGroup"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)

    def test_add_minimal_entity(self):
        variables = {
            "input": {
                "title": "Simple question",
                "subtype": "question",
            }
        }

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])

    def test_add_entity_by_admin(self):
        admin = AdminFactory()

        self.graphql_client.force_login(admin)
        result = self.graphql_client.post(self.mutation, self.data)
        entity = result["data"]["addEntity"]["entity"]

        self.assertTrue(entity["canEdit"])
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)
