from unittest.mock import patch

from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, USER_ROLES
from core.models import Group
from core.tests._shared.mixin import MixinTestCase
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_advanced_tab import TestAdvancedTab
from entities.blog.factories import BlogFactory
from entities.question.models import Question
from user.models import User

from ..factories import QuestionFactory


class EditQuestionTestCase(MixinTestCase.AssertCanUploadMedia, PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.authenticated_user = mixer.blend(User)
        self.user2 = mixer.blend(User)
        self.admin = mixer.blend(User, roles=[USER_ROLES.ADMIN])
        self.group = mixer.blend(Group)
        self.suggested_item = BlogFactory(owner=self.authenticated_user)

        self.question = Question.objects.create(
            title="Test public question",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticated_user.id)],
            owner=self.authenticated_user,
        )

        self.data = {
            "input": {
                "guid": self.question.guid,
                "title": "My first Question",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "featured": {
                    "positionY": 2,
                    "video": "testVideo2",
                    "videoTitle": "testVideoTitle2",
                    "alt": "testAlt2",
                    "caption": "(c) Pleio, 1996",
                },
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.suggested_item.guid],
            }
        }
        self.mutation = """
            fragment QuestionParts on Question {
                title
                inputLanguage
                isTranslationEnabled
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                tags
                url
                inGroup
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    alt
                    caption
                }
                group {
                    guid
                }
                owner {
                    guid
                }
                isClosed
                isFeatured
                isRecommendedInSearch
                suggestedItems {
                    guid
                }
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...QuestionParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_edit_question(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isClosed"], False)
        self.assertEqual(entity["isFeatured"], False)  # only with editor or admin role
        self.assertEqual(
            entity["isRecommendedInSearch"], False
        )  # only with editor or admin role
        self.assertEqual(entity["featured"]["positionY"], 2)
        self.assertEqual(entity["featured"]["video"], "testVideo2")
        self.assertEqual(entity["featured"]["videoTitle"], "testVideoTitle2")
        self.assertEqual(entity["featured"]["alt"], "testAlt2")
        self.assertEqual(entity["featured"]["caption"], "(c) Pleio, 1996")
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"], [{"guid": self.suggested_item.guid}])

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()
        self.assert_can_upload_media_called_once_with(
            user=self.authenticated_user,
            entity_valid=lambda e: e == self.question
            or "Entity is not expected question but %s" % e,
            input=variables["input"],
        )

    def test_edit_question_by_admin(self):
        variables = self.data
        variables["input"]["timeCreated"] = "2018-12-10T23:00:00.000Z"
        variables["input"]["groupGuid"] = self.group.guid
        variables["input"]["ownerGuid"] = self.user2.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isClosed"], False)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")

        self.question.refresh_from_db()

        self.assertEqual(entity["title"], self.question.title)
        self.assertEqual(entity["richDescription"], self.question.rich_description)
        self.assertEqual(entity["tags"], self.question.tags)
        self.assertEqual(entity["isClosed"], self.question.is_closed)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")

    def test_edit_event_group_null_by_admin(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["group"]["guid"], self.group.guid)

        variables["input"]["groupGuid"] = None

        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["group"], None)


class EditQuestionAdvancedTabTestCase(TestAdvancedTab.TestCase):
    TYPE = "Question"

    def build_entity(self, **kwargs):
        return QuestionFactory(**kwargs)
