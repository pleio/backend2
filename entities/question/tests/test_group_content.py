from core.tests.helpers.test_group_content import Template
from entities.question.factories import QuestionFactory
from user.factories import UserFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return QuestionFactory(**kwargs)

    def build_owner(self):
        return UserFactory()
