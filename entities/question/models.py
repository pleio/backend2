from auditlog.registry import auditlog
from django.db import models

from core import config
from core.entity_access import (
    AccessTestBase,
    DenyAnonymousVisitors,
    DenyNonContentModerators,
    EntityAccessResult,
    GrantAdministrators,
    GrantAuthorizedUsers,
    GrantContentModerators,
    GrantGroupOwnersAndAdmins,
)
from core.models import (
    ArticleMixin,
    AttachmentMixin,
    BookmarkMixin,
    Comment,
    CommentMixin,
    Entity,
    FollowMixin,
    MentionMixin,
    VoteMixin,
)
from core.models.featured import FeaturedCoverMixin
from core.models.mixin import RichDescriptionMediaMixin, TitleMixin


class Question(
    RichDescriptionMediaMixin,
    TitleMixin,
    VoteMixin,
    BookmarkMixin,
    FollowMixin,
    CommentMixin,
    MentionMixin,
    FeaturedCoverMixin,
    ArticleMixin,
    AttachmentMixin,
    Entity,
):
    """
    Question
    """

    class Meta:
        ordering = ["-published"]

    is_closed = models.BooleanField(default=False)

    best_answer = models.ForeignKey(
        Comment, on_delete=models.SET_NULL, blank=True, null=True
    )

    def can_close(self, user):
        if not user.is_authenticated:
            return False

        if user.is_site_admin or user.is_question_manager or user == self.owner:
            return True

        return False

    def can_choose_best_answer(self, user):
        if not user.is_authenticated:
            return False

        if self.is_closed:
            return False

        if (
            user.is_site_admin or user.is_question_manager or user == self.owner
        ) and config.QUESTIONER_CAN_CHOOSE_BEST_ANSWER:
            return True

        return False

    def _get_write_validators(self, user):
        return [
            DenyAnonymousVisitors(user, entity=self),
            GrantAdministrators(user, entity=self),
            GrantContentModerators(user, entity=self),
            DenyNonContentModerators(user, entity=self),
            GrantGroupOwnersAndAdmins(user, entity=self),
            DenyAccessIfContentIsLocked(user, entity=self),
            GrantAuthorizedUsers(user, entity=self),
        ]

    @property
    def is_locked(self):
        if config.QUESTION_LOCK_AFTER_ACTIVITY and self.comments.count() > 0:
            return True

        return False

    def __str__(self):
        return f"Question[{self.title}]"

    @property
    def url(self):
        return "{}/questions/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    @property
    def type_to_string(self):
        return "question"

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def serialize(self):
        return {
            "title": self.title,
            "richDescription": self.rich_description,
            "abstract": self.abstract,
            "featured": self.serialize_featured(),
            **super().serialize(),
        }


class DenyAccessIfContentIsLocked(AccessTestBase):
    def test(self):
        if self.entity.is_locked:
            raise EntityAccessResult(False)


auditlog.register(Question)
