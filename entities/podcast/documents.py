from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl.registries import registry

from core.documents import DefaultDocument, custom_analyzer

from .models import Episode, Podcast


@registry.register_document
class PodcastDocument(DefaultDocument):
    id = fields.KeywordField()
    is_archived = fields.BooleanField()
    tags = fields.ListField(fields.TextField(fields={"raw": fields.KeywordField()}))
    tags_matches = fields.ListField(
        fields.TextField(fields={"raw": fields.KeywordField()})
    )
    category_tags = fields.ListField(fields.KeywordField(attr="category_tags_index"))

    read_access = fields.ListField(fields.KeywordField())
    type = fields.KeywordField(attr="type_to_string")
    title = fields.TextField(
        analyzer=custom_analyzer,
        search_analyzer="standard",
        boost=2,
        fields={"raw": fields.KeywordField()},
    )
    description = fields.TextField(analyzer=custom_analyzer, search_analyzer="standard")
    is_recommended_in_search = fields.BooleanField()

    def prepare_description(self, instance):
        return instance.description_index_value()

    def prepare_tags(self, instance):
        return [x.lower() for x in instance.tags]

    class Index:
        name = "podcast"

    class Django:
        model = Podcast

        fields = ["created_at", "updated_at", "published"]


@registry.register_document
class EpisodeDocument(DefaultDocument):
    id = fields.KeywordField()
    is_archived = fields.BooleanField()
    tags = fields.ListField(fields.TextField(fields={"raw": fields.KeywordField()}))
    tags_matches = fields.ListField(
        fields.TextField(fields={"raw": fields.KeywordField()})
    )
    category_tags = fields.ListField(fields.KeywordField(attr="category_tags_index"))

    read_access = fields.ListField(fields.KeywordField())
    type = fields.KeywordField(attr="type_to_string")
    title = fields.TextField(
        analyzer=custom_analyzer,
        search_analyzer="standard",
        boost=2,
        fields={"raw": fields.KeywordField()},
    )
    description = fields.TextField(analyzer=custom_analyzer, search_analyzer="standard")
    is_recommended_in_search = fields.BooleanField()

    def prepare_description(self, instance):
        return instance.description_index_value()

    def prepare_tags(self, instance):
        return [x.lower() for x in instance.tags]

    class Index:
        name = "episode"

    class Django:
        model = Episode

        fields = ["created_at", "updated_at", "published"]
