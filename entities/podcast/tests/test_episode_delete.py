from django.contrib.auth.models import AnonymousUser

from core.constances import COULD_NOT_DELETE, NOT_LOGGED_IN
from core.tests.helpers import PleioTenantTestCase
from entities.podcast.factories import EpisodeFactory, PodcastFactory
from entities.podcast.models import Episode
from user.factories import AdminFactory, UserFactory


class DeleteEpisodeTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()

        self.episode = EpisodeFactory(
            owner=self.owner,
            _podcast=PodcastFactory(owner=self.owner),
        )

        self.mutation = """
            mutation deleteEntity($input: deleteEntityInput!) {
                deleteEntity(input: $input) {
                    success
                }
            }
        """

    def _delete_episode_by(self, user):
        # -- Arrange --
        variables = {"input": {"guid": self.episode.guid}}
        nr_episodes_before = Episode.objects.count()

        # -- Act --
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, variables)

        # -- Assert --
        self.assertTrue(response["data"]["deleteEntity"]["success"])
        self.assertEqual(Episode.objects.count(), nr_episodes_before - 1)

    def test_delete_episode_by_admin(self):
        """Admin can delete episode of 'owner'."""
        admin = AdminFactory()
        self._delete_episode_by(admin)

    def test_delete_episode_by_owner(self):
        """Owner can delete episode of 'owner'."""
        self._delete_episode_by(self.owner)

    def test_delete_episode_by_other_user(self):
        """Other user can not delete episode of 'owner'."""
        # -- Arrange --
        user = UserFactory()
        variables = {"input": {"guid": self.episode.guid}}
        nr_episodes_before = Episode.objects.count()

        # -- Assumptions --
        self.assertNotEqual(self.episode.owner, user)

        # -- Act --
        with self.assertGraphQlError(COULD_NOT_DELETE):
            self.graphql_client.force_login(user)
            self.graphql_client.post(self.mutation, variables)

        self.assertEqual(Episode.objects.count(), nr_episodes_before)

    def test_delete_episode_by_anonymous_user(self):
        """Anonymous user can not delete the episode of 'owner'."""
        # -- Arrange --
        variables = {"input": {"guid": self.episode.guid}}

        # -- Assumptions --
        nr_episodes_before = Episode.objects.count()
        self.assertIsInstance(self.graphql_client.request.user, AnonymousUser)

        # -- Act --
        with self.assertGraphQlError(NOT_LOGGED_IN):
            response = self.graphql_client.post(self.mutation, variables)

            # -- Assert --
            self.assertFalse(response["data"]["deleteEntity"]["success"])

        self.assertEqual(Episode.objects.count(), nr_episodes_before)
