from django.contrib.auth.models import AnonymousUser

from core.constances import ACCESS_TYPE, ORDER_DIRECTION
from core.models import Entity
from core.tests.helpers import PleioTenantTestCase
from entities.podcast.factories import EpisodeFactory, PodcastFactory
from entities.podcast.models import Podcast
from user.factories import UserFactory


class GetPodcastTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()

        self.query = """
            fragment EpisodeParts on Episode {
                title
            }
            fragment PodcastParts on Podcast {
                accessId
                authors {
                    name
                }
                canEdit
                canArchiveAndDelete
                group {
                    guid
                }
                icon {
                    guid
                    title
                }
                isRecommended
                owner {
                    guid
                }
                richDescription
                scheduleArchiveEntity
                scheduleDeleteEntity
                tags
                timeCreated
                timePublished
                title
                url
                views
                rssUrl
                showOwner
                canVote
                isBookmarked
                isFollowing
                isRecommendedInSearch
                canBookmark
                hasVoted
                votes
                publishRequest { guid }
            }
            query GetPodcast($guid: String!, $offset: Int, $limit: Int) {
                entity(guid: $guid) {
                    ... on Podcast {
                        guid
                        status
                        ...PodcastParts
                        episodes(offset: $offset, limit: $limit) {
                            total
                            edges {
                                ...EpisodeParts
                            }
                        }
                    }
                }
            }
        """

    def test_get_public_podcast_anonymous(self):
        """An anonymous user is able to retrieve public podcasts"""

        # -- Arrange --
        icon_file = self.file_factory("fake_file_path")
        public_podcast = PodcastFactory(
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
            owner=self.owner,
            is_recommended=True,
            icon=icon_file,
        )

        # Attach episodes to the public podcast
        for i in range(3):
            title = f"Episode{i}"
            EpisodeFactory(title=title, owner=self.owner, _podcast=public_podcast)

        # One more with owner different from containing podcast
        EpisodeFactory(title="Episode3", owner=UserFactory(), _podcast=public_podcast)

        variables = {
            "guid": public_podcast.guid,
            "offset": 1,
            "limit": 2,
        }

        # -- Assumptions --
        self.assertIsInstance(self.graphql_client.request.user, AnonymousUser)

        # -- Act --
        response = self.graphql_client.post(self.query, variables)

        # -- Assert --
        response = response["data"]["entity"]
        self.assertEqual(response["guid"], public_podcast.guid)
        self.assertEqual(response["title"], public_podcast.title)
        self.assertEqual(response["richDescription"], "")
        self.assertEqual(response["accessId"], 2)
        self.assertEqual(response["timeCreated"], public_podcast.created_at.isoformat())
        self.assertEqual(response["isRecommended"], public_podcast.is_recommended)
        self.assertEqual(response["tags"], [])
        self.assertEqual(response["views"], 0)
        self.assertFalse(response["canEdit"])
        self.assertEqual(response["canArchiveAndDelete"], False)
        self.assertEqual(response["owner"]["guid"], public_podcast.owner.guid)
        self.assertEqual(response["url"], public_podcast.url)
        self.assertDateEqual(response["timePublished"], str(public_podcast.published))
        self.assertIsNone(response["scheduleArchiveEntity"])
        self.assertIsNone(response["scheduleDeleteEntity"])
        self.assertEqual(response["icon"]["guid"], icon_file.guid)
        self.assertEqual(response["showOwner"], True)
        self.assertEqual(response["canVote"], False)
        self.assertEqual(response["isBookmarked"], False)
        self.assertEqual(response["isRecommendedInSearch"], False)
        self.assertEqual(response["isFollowing"], False)
        self.assertEqual(response["canBookmark"], False)
        self.assertEqual(response["hasVoted"], None)
        self.assertEqual(response["votes"], 0)
        self.assertIsNone(response["publishRequest"])

        # pagination of related episodes
        start = variables["offset"]
        end = variables["offset"] + variables["limit"]
        expected_episodes = [e.title for e in public_podcast.episodes.all()[start:end]]
        returned_episodes = [e["title"] for e in response["episodes"]["edges"]]
        self.assertEqual(expected_episodes, returned_episodes)

    def test_get_private_podcast_anonymous(self):
        """An anonymous user is not able to retrieve private podcasts"""

        # -- Arrange --
        private_podcast = PodcastFactory(
            title="Test private podcast",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.owner.id)],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
            owner=self.owner,
            is_recommended=False,
        )
        variables = {"guid": private_podcast.guid}

        # -- Assumptions --
        self.assertIsInstance(self.graphql_client.request.user, AnonymousUser)

        # -- Act --
        response = self.graphql_client.post(self.query, variables)

        # -- Assert --
        self.assertIsNone(response["data"]["entity"])

    def test_get_multiple_podcasts(self):
        """Test on getting multiple podcasts (all)"""

        # -- Arrange --
        query = """
            fragment PodcastParts on Podcast {
                guid
                title
            }
            query GetPodcast($subtypes: [String]) {
                entities(subtypes: $subtypes) {
                    edges {
                        ...PodcastParts
                    }
                    total
                }
            }
        """
        variables = {"subtypes": ["podcast"]}
        PodcastFactory(owner=self.owner)
        EpisodeFactory(owner=self.owner, _podcast=PodcastFactory(owner=self.owner))

        # -- Assumptions --
        # The risk of fetching more entities then desired exists
        self.assertTrue(Entity.objects.count() > Podcast.objects.count())

        # -- Act --
        response = self.graphql_client.post(query, variables)

        # -- Assert --
        self.assertEqual(
            {podcast.guid for podcast in Podcast.objects.all()},
            {edge["guid"] for edge in response["data"]["entities"]["edges"]},
        )

        self.assertEqual(response["data"]["entities"]["total"], Podcast.objects.count())

    def test_get_multiple_podcasts_paginated(self):
        """Test on getting a selection of podcasts, and from each podcast a selection
        of episodes
        """

        # -- Arrange --
        query = """
            fragment EpisodeParts on Episode {
                title
            }
            fragment PodcastParts on Podcast {
                title
            }
            query GetPodcast($subtypes: [String],
                             $offset: Int,
                             $limit: Int,
                             $offset_episodes: Int,
                             $limit_episodes: Int
                             $order_direction: OrderDirection
                             $order_direction_episodes: OrderDirection) {
                entities(subtypes: $subtypes,
                         offset: $offset,
                         limit: $limit,
                         orderDirection: $order_direction) {
                    total
                    edges {
                        ... on Podcast {
                            ...PodcastParts
                            episodes(offset: $offset_episodes,
                                     limit: $limit_episodes
                                     orderDirection: $order_direction_episodes) {
                                total
                                edges {
                                    ...EpisodeParts
                                }
                            }
                        }
                    }
                }
            }
        """
        variables = {
            "subtypes": ["podcast"],
            "offset": 2,
            "limit": 3,
            "order_direction": ORDER_DIRECTION.asc,
            "offset_episodes": 1,
            "limit_episodes": 2,
            "order_direction_episodes": ORDER_DIRECTION.asc,
        }

        # Create multiple podcasts with multiple episodes each
        for i in range(6):
            title = f"Podcast{i}"
            podcast = PodcastFactory(title=title, owner=self.owner)
            for j in range(4):
                title = f"Podcast{i} - Episode{j}"
                EpisodeFactory(title=title, owner=self.owner, _podcast=podcast)

        # -- Act --
        response = self.graphql_client.post(query, variables)

        # -- Assert --
        response = response["data"]["entities"]

        self.assertEqual(response["total"], Podcast.objects.count())

        # Expected
        start_podcasts = variables["offset"]
        end_podcasts = variables["offset"] + variables["limit"]
        start_episodes = variables["offset_episodes"]
        end_episodes = variables["offset_episodes"] + variables["limit_episodes"]
        expected_episodes = []
        for i in range(start_podcasts, end_podcasts):
            for j in range(start_episodes, end_episodes):
                expected_episodes.append(f"Podcast{i} - Episode{j}")

        # Returned
        returned_episodes = []
        for edge in response["edges"]:
            for episode in edge["episodes"]["edges"]:
                returned_episodes.append(episode["title"])

        self.assertEqual(expected_episodes, returned_episodes)

    def test_get_podcast_with_publish_request_anonymous(self):
        public_podcast = PodcastFactory(
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
            owner=self.owner,
        )
        public_podcast.publish_requests.create(
            time_published=public_podcast.created_at,
        )

        response = self.graphql_client.post(self.query, {"guid": public_podcast.guid})
        response = response["data"]["entity"]

        self.assertEqual(response["guid"], public_podcast.guid)
        self.assertIsNone(response["publishRequest"])

    def test_get_podcast_with_publish_request_authenticated(self):
        public_podcast = PodcastFactory(
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
            owner=self.owner,
        )
        pr = public_podcast.publish_requests.create(
            time_published=public_podcast.created_at,
        )

        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.query, {"guid": public_podcast.guid})
        response = response["data"]["entity"]

        self.assertEqual(response["guid"], public_podcast.guid)
        self.assertEqual(response["publishRequest"]["guid"], pr.guid)
