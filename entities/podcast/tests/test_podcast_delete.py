from django.contrib.auth.models import AnonymousUser

from core.constances import COULD_NOT_DELETE, NOT_LOGGED_IN
from core.tests.helpers import PleioTenantTestCase
from entities.podcast.factories import EpisodeFactory, PodcastFactory
from entities.podcast.models import Episode, Podcast
from user.factories import AdminFactory, UserFactory


class DeletePodcastTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()

        self.podcast = PodcastFactory(title="Podcast to delete", owner=self.owner)
        EpisodeFactory(owner=self.owner, _podcast=self.podcast)
        EpisodeFactory(owner=UserFactory(), _podcast=self.podcast)
        self.mutation = """
            mutation deleteEntity($input: deleteEntityInput!) {
                deleteEntity(input: $input) {
                    success
                }
            }
        """

    def _delete_podcast_by(self, user):
        # -- Arrange --
        variables = {"input": {"guid": self.podcast.guid}}

        # -- Act --
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, variables)

        # -- Assert --
        self.assertTrue(response["data"]["deleteEntity"]["success"])
        self.assertEqual(Podcast.objects.count(), 0)
        self.assertEqual(Episode.objects.count(), 0)

    def test_delete_podcast_by_admin(self):
        """Admin can delete podcast of 'owner'. Related episodes get deleted as well,
        regardless of owner.
        """
        admin = AdminFactory()
        self._delete_podcast_by(admin)

    def test_delete_podcast_by_owner(self):
        """Owner can delete podcast of 'owner'. Related episodes get deleted as well,
        regardless of owner.
        """
        self._delete_podcast_by(self.owner)

    def test_delete_podcast_by_other_user(self):
        """Other user can not delete podcast of 'owner'. Related episodes will remain,
        regardless of owner.
        """
        # -- Arrange --
        user = UserFactory()
        variables = {"input": {"guid": self.podcast.guid}}

        # -- Assumptions --
        nr_podcasts_before = Podcast.objects.count()
        nr_episodes_before = Episode.objects.count()

        self.assertEqual(Podcast.objects.count(), 1)
        self.assertEqual(Episode.objects.count(), 2)
        self.assertNotEqual(self.podcast.owner, user)

        # -- Act --
        with self.assertGraphQlError(COULD_NOT_DELETE):
            self.graphql_client.force_login(user)
            response = self.graphql_client.post(self.mutation, variables)

            # -- Assert --
            self.assertFalse(response["data"]["deleteEntity"]["success"])

        self.assertEqual(Podcast.objects.count(), nr_podcasts_before)
        self.assertEqual(Episode.objects.count(), nr_episodes_before)

    def test_delete_podcast_by_anonymous_user(self):
        """Anonymous user can not delete the podcast of 'owner'. Related episodes
        will remain, regardless of owner.
        """
        # -- Arrange --
        variables = {"input": {"guid": self.podcast.guid}}
        nr_podcasts_before = Podcast.objects.count()
        nr_episodes_before = Episode.objects.count()

        # -- Assumptions --
        self.assertIsInstance(self.graphql_client.request.user, AnonymousUser)

        # -- Act --
        with self.assertGraphQlError(NOT_LOGGED_IN):
            response = self.graphql_client.post(self.mutation, variables)

            # -- Assert --
            self.assertFalse(response["data"]["deleteEntity"]["success"])

        self.assertEqual(Podcast.objects.count(), nr_podcasts_before)
        self.assertEqual(Episode.objects.count(), nr_episodes_before)
