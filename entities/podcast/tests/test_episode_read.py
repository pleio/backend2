from django.contrib.auth.models import AnonymousUser

from core.constances import ACCESS_TYPE
from core.models import Entity
from core.tests.helpers import PleioTenantTestCase
from entities.podcast.factories import BuildPodcastOnceFactoryMixin, EpisodeFactory
from entities.podcast.models import Episode
from user.factories import UserFactory


class GetEpisodeTestCase(BuildPodcastOnceFactoryMixin, PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()

        self.query = """
            fragment EpisodeParts on Episode {
                abstract
                accessId
                canEdit
                canArchiveAndDelete
                file {
                    guid
                    title
                }
                group {
                    guid
                }
                isFeatured
                isRecommendedInSearch
                isTranslationEnabled
                inGroup
                isRecommended
                owner {
                    guid
                }
                podcast {
                    guid
                    title
                }
                richDescription
                scheduleArchiveEntity
                scheduleDeleteEntity
                statusPublished
                suggestedItems {
                    guid
                }
                tags
                timeCreated
                timePublished
                timeUpdated
                title
                url
                writeAccessId
                views
                showOwner
                publishRequest { guid }
            }
            query GetEpisode($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...EpisodeParts
                }
            }
        """

    def test_get_public_episode_anonymous(self):
        """An anonymous user is able to retrieve public episodes"""

        # -- Arrange --
        public_episode = EpisodeFactory(
            **self.add_podcast_to_episode(
                owner=self.owner,
                is_recommended=True,
                file=self.file_factory("fake_file_path"),
            )
        )
        variables = {"guid": public_episode.guid}

        # -- Assumptions --
        self.assertIsInstance(self.graphql_client.request.user, AnonymousUser)

        # -- Act --
        response = self.graphql_client.post(self.query, variables)

        # -- Assert --
        response = response["data"]["entity"]
        self.assertEqual(response["guid"], public_episode.guid)
        self.assertEqual(response["title"], public_episode.title)
        self.assertEqual(response["richDescription"], "")
        self.assertEqual(response["accessId"], 2)
        self.assertEqual(response["timeCreated"], public_episode.created_at.isoformat())
        self.assertEqual(response["isRecommended"], public_episode.is_recommended)
        self.assertEqual(response["isRecommendedInSearch"], False)
        self.assertEqual(response["isTranslationEnabled"], True)
        self.assertEqual(response["tags"], [])
        self.assertEqual(response["views"], 0)
        self.assertFalse(response["canEdit"])
        self.assertEqual(response["canArchiveAndDelete"], False)
        self.assertEqual(response["file"]["guid"], public_episode.file.guid)
        self.assertEqual(response["file"]["title"], public_episode.file.title)
        self.assertEqual(response["owner"]["guid"], public_episode.owner.guid)
        self.assertEqual(response["podcast"]["guid"], public_episode.podcast.guid)
        self.assertEqual(response["podcast"]["title"], public_episode.podcast.title)
        self.assertEqual(response["url"], public_episode.url)
        self.assertDateEqual(response["timePublished"], str(public_episode.published))
        self.assertIsNone(response["scheduleArchiveEntity"])
        self.assertIsNone(response["scheduleDeleteEntity"])
        self.assertEqual(response["showOwner"], True)
        self.assertIsNone(response["publishRequest"])

    def test_get_private_episode_anonymous(self):
        """An anonymous user is not able to retrieve private episodes"""

        # -- Arrange --
        private_episode = EpisodeFactory(
            **self.add_podcast_to_episode(
                owner=self.owner,
                read_access=[ACCESS_TYPE.user.format(self.owner.id)],
                is_recommended=False,
            )
        )

        variables = {"guid": private_episode.guid}

        # -- Assumptions --
        self.assertIsInstance(self.graphql_client.request.user, AnonymousUser)

        # -- Act --
        response = self.graphql_client.post(self.query, variables)

        # -- Assert --
        self.assertIsNone(response["data"]["entity"])

    def test_get_multiple_episodes(self):
        """Test on getting multiple episodes"""

        # -- Arrange --
        for _ in range(3):
            EpisodeFactory(**self.add_podcast_to_episode(owner=self.owner))

        # -- Assumptions --
        # The risk of fetching more entities then desired exists
        self.assertTrue(Entity.objects.count() > Episode.objects.count())

        query = """
            fragment EpisodeParts on Episode {
                guid
                title
            }
            query GetEpisodes($subtypes: [String]) {
                entities(subtypes: $subtypes) {
                    edges {
                        ...EpisodeParts
                    }
                    total
                }
            }
        """
        variables = {"subtypes": ["episode"]}

        # -- Act --
        response = self.graphql_client.post(query, variables)

        # -- Assert --
        self.assertEqual(
            {episode.guid for episode in Episode.objects.all()},
            {edge["guid"] for edge in response["data"]["entities"]["edges"]},
        )
        self.assertEqual(response["data"]["entities"]["total"], Episode.objects.count())

    def test_episode_with_publish_request_anonymous(self):
        episode = EpisodeFactory(**self.add_podcast_to_episode(owner=self.owner))
        episode.publish_requests.create(time_published=episode.published)
        variables = {"guid": episode.guid}

        response = self.graphql_client.post(self.query, variables)
        response = response["data"]["entity"]

        self.assertEqual(response["guid"], episode.guid)
        self.assertIsNone(response["publishRequest"])

    def test_episode_with_publish_request_authenticated(self):
        episode = EpisodeFactory(**self.add_podcast_to_episode(owner=self.owner))
        pr = episode.publish_requests.create(time_published=episode.published)

        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.query, {"guid": episode.guid})
        response = response["data"]["entity"]

        self.assertEqual(response["guid"], episode.guid)
        self.assertEqual(response["publishRequest"]["guid"], pr.guid)
