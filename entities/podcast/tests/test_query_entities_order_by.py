from core.tests.queries.entities.test_order_by import OrderByBaseTestCases
from entities.podcast.factories import PodcastFactory


class BuildEntityMixin(OrderByBaseTestCases.OrderByTestCaseBase):
    def build_entity(self, **kwargs):
        return PodcastFactory(**kwargs)

    def variables(self):
        return {**super().variables(), "subTypes": ["podcast"]}


class TestOrderByTimeCreated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeCreated,
):
    pass


class TestOrderByTimeUpdated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeUpdated,
):
    pass


class TestOrderByTimePublished(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimePublished,
):
    pass


class TestOrderByLastAction(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastAction,
):
    pass


class TestOrderByLastSeen(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastSeen,
):
    pass


class TestOrderByStartDateNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByStartDateNotAvailable,
):
    pass


class TestOrderByFileSizeNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByFileSizeNotAvailable,
):
    pass


class TestOrderByTitle(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTitle,
):
    pass


class TestOrderByGroupName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByGroupName,
):
    pass


class TestOrderByOwnerName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByOwnerName,
):
    pass


class TestOrderByReadAccess(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByReadAccess,
):
    pass
