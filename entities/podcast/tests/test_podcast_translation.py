from core.tests.helpers.test_translation import Wrapper
from entities.podcast.factories import PodcastFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Podcast"

    def build_entity(self, **kwargs):
        return PodcastFactory(**kwargs)
