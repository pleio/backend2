from unittest.mock import patch

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.podcast.factories import EpisodeFactory, PodcastFactory
from user.factories import AdminFactory, UserFactory


class EditPodcastTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.site_admin = AdminFactory(name="Site Admin")
        self.group = GroupFactory(owner=self.site_admin)
        self.new_group = GroupFactory(owner=self.site_admin)

        self.admin = UserFactory(name="Group Admin")
        self.group.join(self.admin, "admin")

        self.owner = UserFactory(name="Initial owner")
        self.group.join(self.owner)

        self.new_owner = UserFactory(name="New owner")
        self.group.join(self.new_owner)

        self.podcast = PodcastFactory(owner=self.owner, group=self.group)
        self.new_owner = UserFactory()

        self.mutation = """
            mutation ($input: editPodcastInput!) {
                editPodcast(input: $input) {
                    podcast {
                        title
                        canEdit
                        canEditAdvanced
                        canEditGroup
                        canArchiveAndDelete
                        isRecommendedInSearch
                        isTranslationEnabled
                        group { guid }
                        owner { guid }
                        episodes {
                            edges {
                                guid
                                group { guid }
                            }
                        }
                    }
                }
            }
        """

        self.variables = {
            "input": {
                "guid": self.podcast.guid,
                "title": "new title",
                "groupGuid": self.new_group.guid,
                "ownerGuid": self.new_owner.guid,
                "isRecommendedInSearch": True,
                "isTranslationEnabled": False,
            }
        }
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_edit_podcast_by_anonymous(self):
        """An anonymous user can not edit a podcast"""
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

        self.assertFalse(self.mocked_revert_published.called)
        self.assertFalse(self.mocked_revert_published.called)

    def test_edit_podcast_by_not_owner(self):
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.new_owner)
            self.graphql_client.post(self.mutation, self.variables)

        self.assertFalse(self.mocked_revert_published.called)
        self.assertFalse(self.mocked_revert_published.called)

    def test_edit_podcast_by_owner(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPodcast"]["podcast"]

        self.assertEqual(entity["title"], "new title")
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.owner.guid)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canEditAdvanced"], False)
        self.assertEqual(entity["canEditGroup"], False)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isTranslationEnabled"], False)

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_podcast_by_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPodcast"]["podcast"]

        self.assertEqual(entity["title"], "new title")
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.new_owner.guid)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canEditAdvanced"], True)
        self.assertEqual(entity["canEditGroup"], False)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["isRecommendedInSearch"], False)

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_podcast_by_site_admin(self):
        self.graphql_client.force_login(self.site_admin)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editPodcast"]["podcast"]

        self.assertEqual(entity["title"], "new title")
        self.assertEqual(entity["group"]["guid"], self.new_group.guid)
        self.assertEqual(entity["owner"]["guid"], self.new_owner.guid)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canEditAdvanced"], True)
        self.assertEqual(entity["canEditGroup"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_podcast_group(self):
        """Should also update the group of episodes."""
        EpisodeFactory(
            owner=self.podcast.owner,
            _podcast=self.podcast,
            group=self.podcast.group,
        )

        self.graphql_client.force_login(self.site_admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        podcast = result["data"]["editPodcast"]["podcast"]
        episode = podcast["episodes"]["edges"][0]

        self.assertEqual(podcast["group"]["guid"], self.new_group.guid)
        self.assertEqual(episode["group"]["guid"], self.new_group.guid)
