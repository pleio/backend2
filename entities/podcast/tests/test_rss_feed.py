from datetime import timedelta

from django.urls import reverse

from core.constances import ACCESS_TYPE
from core.tests.helpers import PleioTenantTestCase
from entities.podcast.factories import EpisodeFactory, PodcastFactory
from user.factories import AdminFactory, UserFactory


class PodcastFeedTestCase(PleioTenantTestCase):
    def _response_to_string(self, response):
        result = response.content.decode("utf-8")
        result = result.replace("\\n", "\n")
        return result

    def test_podcast_public(self):
        """The podcast and its episodes and content are visible in the rss feed
        if they are public."""

        # -- Arrange --
        owner = UserFactory()
        author = UserFactory()
        podcast = PodcastFactory(
            owner=owner,
            rich_description=self.tiptap_paragraph("description for podcast"),
            icon=self.file_factory("fake_file_path_icon"),
            authors=[
                {"guid": author.guid, "name": author.name},
                {"name": "User without account"},
            ],
        )
        episode1 = EpisodeFactory(
            owner=owner,
            rich_description=self.tiptap_paragraph("description for episode1"),
            file=self.file_factory("fake_file_path_mp3", duration=timedelta(seconds=3)),
            tags=[
                "foo",
                "bar",
            ],
            _podcast=podcast,
        )
        EpisodeFactory(
            owner=UserFactory(),
            rich_description=self.tiptap_paragraph(4000 * "description for episode2 -"),
            file=self.file_factory("fake_file_path_mp3", duration=timedelta(seconds=2)),
            _podcast=podcast,
        )
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Act --
        response = self.client.get(url)

        # -- Assert --
        # General (Podcast level)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "application/rss+xml; charset=utf-8")

        response_content = self._response_to_string(response)

        self.assertIn(f"<title>{podcast.title}</title>", response_content)
        self.assertIn(f"{podcast.url}</link>", response_content)
        self.assertIn(
            f"<description>{podcast.description}</description>", response_content
        )
        self.assertIn("<itunes:image href=", response_content)
        self.assertIn(
            f"<itunes:author>{podcast.authors_to_string}</itunes:author>",
            response_content,
        )

        # Item specific (Episode level)
        self.assertIn(f"<title>{episode1.title}</title>", response_content)
        self.assertIn(f"{episode1.url}</link>", response_content)
        self.assertIn(
            f"<description>{episode1.description}</description>",
            response_content,
        )
        self.assertIn("<enclosure length=", response_content)
        datetime_format = "%a, %d %b %Y %H:%M:%S %z"
        self.assertIn(
            f"<pubDate>{episode1.published.strftime(datetime_format)}</pubDate>",
            response_content,
        )
        self.assertIn(
            f"<itunes:duration>{episode1.file.duration}</itunes:duration>",
            response_content,
        )

    def test_podcast_does_not_exist(self):
        # -- Arrange --
        url = reverse(
            "podcast_feed",
            kwargs={"podcast_id": "18959168-1234-1234-1234-e64ad84f67b5"},
        )

        # -- Act --
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 404)

    def test_podcast_not_visible_to_user(self):
        # -- Arrange --
        owner = UserFactory()
        podcast = PodcastFactory(
            owner=owner,
            read_access=[ACCESS_TYPE.user.format(owner.guid)],
        )
        user = UserFactory()
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Act --
        self.client.force_login(user)
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 403)

    def test_public_podcast_visible_to_user(self):
        # -- Arrange --
        owner = UserFactory()
        podcast = PodcastFactory(
            owner=owner,
            read_access=[ACCESS_TYPE.user.format(owner.guid), ACCESS_TYPE.public],
        )
        user = UserFactory()
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Act --
        self.client.force_login(user)
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 200)

    def test_podcast_not_visible_for_user(self):
        # -- Arrange --
        owner = UserFactory()
        podcast = PodcastFactory(owner=owner, read_access=[])
        EpisodeFactory(owner=owner, _podcast=podcast)
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Act --
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 403)

    def test_podcast_visible_to_owner(self):
        # -- Arrange --
        owner = UserFactory()
        podcast = PodcastFactory(
            owner=owner,
            read_access=[ACCESS_TYPE.user.format(owner.guid)],
        )
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Act --
        self.client.force_login(owner)
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 200)

    def test_podcast_visible_to_admin(self):
        # -- Arrange --
        owner = UserFactory()
        admin = AdminFactory()
        podcast = PodcastFactory(owner=owner, read_access=[])
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Act --
        self.client.force_login(admin)
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 200)

    def test_episode_not_public(self):
        # -- Arrange --
        owner = UserFactory()
        podcast = PodcastFactory(owner=owner)
        episode = EpisodeFactory(owner=owner, _podcast=podcast, read_access=[])
        url = reverse("podcast_feed", kwargs={"podcast_id": podcast.guid})

        # -- Assume --
        self.assertIn(ACCESS_TYPE.public, podcast.read_access)
        self.assertNotIn(ACCESS_TYPE.public, episode.read_access)

        # -- Act --
        response = self.client.get(url)

        # -- Assert --
        self.assertEqual(response.status_code, 200)
        response_content = self._response_to_string(response)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("<item>", response_content)

    def test_file_not_public(self):
        """The file of a public episode will automatically be public too"""
        # -- Arrange --
        owner = UserFactory()
        podcast = PodcastFactory(owner=owner)
        file = self.file_factory("fake_file_path_mp3", read_access=[])
        episode = EpisodeFactory(owner=owner, _podcast=podcast, file=file)

        # -- Assume --
        self.assertIn(ACCESS_TYPE.public, podcast.read_access)
        self.assertIn(ACCESS_TYPE.public, episode.read_access)
        self.assertNotIn(ACCESS_TYPE.public, file.read_access)

        # -- Assert --
        self.assertIn(ACCESS_TYPE.public, podcast.episodes.first().file.read_access)
