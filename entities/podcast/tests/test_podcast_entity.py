from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_entity_with_publish_request import PublishRequestTestCases
from entities.podcast.factories import PodcastFactory
from user.factories import UserFactory


class PodcastEntityTestCase(PleioTenantTestCase):
    TITLE = "Demo title"
    CONTENT = "Demo content"
    ABSTRACT = "Demo abstract"

    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.podcast = PodcastFactory(
            owner=self.owner,
            title=self.TITLE,
            rich_description=self.CONTENT,
            abstract=self.ABSTRACT,
        )

    def tearDown(self):
        super().tearDown()

    @mock.patch("core.models.Entity.serialize")
    def test_serialize(self, serialize):
        serialize.return_value = {}

        self.assertEqual(
            self.podcast.serialize(),
            {
                "abstract": self.ABSTRACT,
                "icon": "",
                "richDescription": self.CONTENT,
                "title": self.TITLE,
                "isTranslationEnabled": True,
            },
        )

    def test_map_rich_text_fields(self):
        before = self.podcast.serialize()
        expected = self.podcast.serialize()
        expected["richDescription"] = f"new {self.CONTENT}"
        expected["abstract"] = f"new {self.ABSTRACT}"

        self.podcast.map_rich_text_fields(lambda v: f"new {v}")
        snapshot = self.podcast.serialize()

        self.assertNotEqual(snapshot, before)
        self.assertEqual(snapshot, expected)


class TestEditEntityWithPublishRequest(
    PublishRequestTestCases.TestEditEntityWithPublishRequest
):
    TYPE_TO_STRING = "podcast"

    def entity_factory(self, **kwargs):
        return PodcastFactory(**kwargs)
