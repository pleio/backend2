from core.tests.helpers.test_delete_entities import Wrapper
from entities.podcast.factories import EpisodeFactory as EntityFactory
from entities.podcast.factories import PodcastFactory


class TestDeleteEntities(Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return EntityFactory(**kwargs, podcast=self.podcast)

    def prepare_test(self):
        self.podcast = PodcastFactory(owner=self.user)
        super().prepare_test()
