from unittest.mock import patch

from django.db import connection
from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.factories import GroupFactory
from core.lib import get_file_extension
from core.tests.helpers import PleioTenantTestCase
from core.utils.clamav import FILE_SCAN
from entities.news.models import News
from entities.podcast.factories import PodcastFactory
from entities.podcast.models import Episode
from entities.podcast.tasks import schedule_convert_to_mp3
from user.factories import AdminFactory, UserFactory


@tag("createEntity")
class AddEpisodeTestCase(PleioTenantTestCase):
    @classmethod
    def asset_path(cls, filename):
        return cls.relative_path(__file__, ["assets", filename])

    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.owner = UserFactory()
        self.admin = AdminFactory()
        self.group = GroupFactory(owner=self.owner)
        self.suggestedNews = mixer.blend(News)
        self.podcast = PodcastFactory(owner=self.owner)
        self.file = self.file_factory(self.asset_path("file_example_MP3_700KB.mp3"))
        self.input_vars = {
            "abstract": "Abstract of 'Adding an Episode'",
            "inputLanguage": "en",
            "isTranslationEnabled": False,
            "accessId": 0,
            "isFeatured": True,
            "isRecommended": True,
            "isRecommendedInSearch": True,
            "richDescription": self.tiptap_paragraph(
                "The 'richDescription' for 'Adding a Episode'"
            ),
            "scheduleArchiveEntity": str(
                timezone.localtime() + timezone.timedelta(days=10)
            ),
            "scheduleDeleteEntity": str(
                timezone.localtime() + timezone.timedelta(days=20)
            ),
            "suggestedItems": [self.suggestedNews.guid],
            "tags": ["tag1", "tag2"],
            "timePublished": str(timezone.localtime()),
            "title": "Adding a Episode",
            "writeAccessId": 0,
            "podcastGuid": self.podcast.guid,
            "fileGuid": self.file.guid,
        }

        self.mutation = """
            fragment EpisodeParts on Episode {
                inputLanguage
                isTranslationEnabled
                abstract
                accessId
                canEdit
                canArchiveAndDelete
                group {
                    guid
                }
                isFeatured
                inGroup
                isRecommended
                isRecommendedInSearch
                richDescription
                scheduleArchiveEntity
                scheduleDeleteEntity
                statusPublished
                suggestedItems {
                    guid
                }
                tags
                timeCreated
                timePublished
                timeUpdated
                title
                url
                writeAccessId
                podcast {
                    guid
                    title
                }
                file {
                    guid
                    title
                }
                showOwner
            }
            mutation ($input: addEpisodeInput!) {
                addEpisode(input: $input) {
                    episode {
                        guid
                        status
                        ...EpisodeParts
                    }
                }
            }
        """

        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def _add_episode_by(self, user):
        """Test creating an episode."""

        # -- Assumptions --
        self.assertEqual(Episode.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        response = response["data"]["addEpisode"]["episode"]
        input_vars = self.input_vars
        self.assertEqual(response["inputLanguage"], input_vars["inputLanguage"])
        self.assertEqual(response["isTranslationEnabled"], False)
        self.assertEqual(response["abstract"], input_vars["abstract"])
        self.assertEqual(response["accessId"], input_vars["accessId"])
        self.assertEqual(response["richDescription"], input_vars["richDescription"])
        self.assertEqual(response["statusPublished"], "published")
        self.assertDateEqual(
            response["scheduleArchiveEntity"], input_vars["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            response["scheduleDeleteEntity"], input_vars["scheduleDeleteEntity"]
        )
        self.assertEqual(response["suggestedItems"][0]["guid"], self.suggestedNews.guid)
        self.assertEqual(response["tags"], input_vars["tags"])
        self.assertDateEqual(response["timePublished"], input_vars["timePublished"])
        self.assertEqual(response["title"], input_vars["title"])
        self.assertEqual(response["writeAccessId"], input_vars["writeAccessId"])
        self.assertEqual(response["podcast"]["guid"], self.podcast.guid)
        self.assertEqual(response["podcast"]["title"], self.podcast.title)
        self.assertEqual(response["file"]["guid"], self.file.guid)
        self.assertEqual(response["file"]["title"], self.file.title)
        self.assertEqual(response["showOwner"], True)

        if set(user.roles) & {"ADMIN", "EDITOR"}:
            self.assertEqual(response["isFeatured"], input_vars["isFeatured"])
            self.assertEqual(response["isRecommended"], input_vars["isRecommended"])
            self.assertEqual(
                response["isRecommendedInSearch"], input_vars["isRecommendedInSearch"]
            )
        else:
            self.assertNotEqual(response["isFeatured"], input_vars["isFeatured"])
            self.assertNotEqual(response["isRecommended"], input_vars["isRecommended"])
            self.assertNotEqual(
                response["isRecommendedInSearch"], input_vars["isRecommendedInSearch"]
            )

        # -- Assertions on created episode --
        episode = Episode.objects.last()
        self.assertEqual(episode.abstract, input_vars["abstract"])
        self.assertTrue(user.guid in episode.read_access[0])
        self.assertEqual(episode.rich_description, input_vars["richDescription"])
        self.assertEqual(episode.status_published.value, "published")
        self.assertDateEqual(
            str(episode.schedule_archive_after), input_vars["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            str(episode.schedule_delete_after), input_vars["scheduleDeleteEntity"]
        )
        self.assertEqual(str(episode.suggested_items[0]), self.suggestedNews.guid)
        self.assertEqual(episode.tags, input_vars["tags"])
        (self.assertDateEqual(str(episode.published), input_vars["timePublished"]),)
        self.assertEqual(episode.title, input_vars["title"])
        self.assertTrue(user.guid in episode.write_access[0])
        self.assertEqual(episode.podcast.guid, self.podcast.guid)
        self.assertEqual(episode.podcast.title, self.podcast.title)
        self.assertEqual(episode.file.guid, self.file.guid)
        self.assertEqual(episode.file.title, self.file.title)

        if set(user.roles) & {"ADMIN", "EDITOR"}:
            self.assertEqual(episode.is_featured, input_vars["isFeatured"])
            self.assertEqual(episode.is_recommended, input_vars["isRecommended"])
        else:
            self.assertNotEqual(episode.is_featured, input_vars["isFeatured"])
            self.assertNotEqual(episode.is_recommended, input_vars["isRecommended"])

        self.mocked_revert_published.assert_called_once()
        self.mocked_revert_published.reset_mock()
        self.mocked_create_publish_request.assert_called_once()
        self.mocked_create_publish_request.reset_mock()

    def test_add_episode_by_not_admin(self):
        self._add_episode_by(self.owner)

    def test_add_episode_by_admin(self):
        self._add_episode_by(self.admin)

    def test_add_episode_to_group(self):
        # -- Arrange --
        self.input_vars["groupGuid"] = self.group.guid

        # -- Assumptions --
        self.assertEqual(Episode.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        response = response["data"]["addEpisode"]["episode"]
        self.assertTrue(response["inGroup"])
        self.assertEqual(response["group"]["guid"], self.group.guid)

        # -- Assertions on created episode --
        episode = Episode.objects.last()
        self.assertEqual(episode.group, self.group)

    def test_add_unpublished_episode(self):
        # -- Arrange --
        self.input_vars["timePublished"] = None

        # -- Assumptions --
        self.assertEqual(Episode.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        response = response["data"]["addEpisode"]["episode"]
        self.assertIsNone(response["timePublished"])
        self.assertEqual(response["statusPublished"], "draft")

        # -- Assertions on created episode --
        episode = Episode.objects.last()
        self.assertIsNone(episode.published)
        self.assertEqual(episode.status_published.value, "draft")

    def test_add_minimal_episode(self):
        # -- Arrange --
        input_vars = {
            "title": "Simple episode",
            "podcastGuid": PodcastFactory(owner=self.owner).guid,
        }

        # -- Assumptions --
        self.assertEqual(Episode.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": input_vars})

        # -- Assertions on response --
        response = response["data"]["addEpisode"]["episode"]
        self.assertEqual(response["podcast"]["guid"], input_vars["podcastGuid"])

        # -- Assertions on created episode --
        episode = Episode.objects.last()
        self.assertEqual(episode.title, input_vars["title"])
        self.assertEqual(episode.podcast.guid, input_vars["podcastGuid"])

    def test_add_episode_with_attachment(self):
        # -- Arrange --
        attachment = self.file_factory("fake_file_path")
        self.input_vars["richDescription"] = self.tiptap_attachment(attachment)

        # -- Assumptions --
        self.assertEqual(Episode.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        self.assertIsNotNone(response["data"]["addEpisode"])

        # -- Assertions on created episode --
        episode = Episode.objects.last()
        self.assertTrue(episode.attachments.filter(file_id=attachment.id).exists())

    @patch("core.utils.clamav.scan")
    def test_add_episode_with_none_mp3_attachment(self, mocked_scan):
        mocked_scan.return_value = FILE_SCAN.CLEAN
        # -- Arrange --
        file = self.file_factory(self.asset_path("file_example_MP3_700KB.m4a"))
        self.assertEqual(get_file_extension(file.upload.name), ".m4a")
        self.input_vars["fileGuid"] = file.guid
        # -- Assumptions --
        self.assertEqual(Episode.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, {"input": self.input_vars})
        episode = Episode.objects.get(
            id=result["data"]["addEpisode"]["episode"]["guid"]
        )
        schedule_convert_to_mp3.s(connection.schema_name, episode.file.guid).apply()
        episode.refresh_from_db()
        self.assertEqual(get_file_extension(episode.file.upload.name), ".mp3")
