from core.tests.helpers.test_delete_entities import Wrapper
from entities.podcast.factories import EpisodeFactory
from entities.podcast.factories import PodcastFactory as EntityFactory
from entities.podcast.models import Episode


class TestDeleteEntities(Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return EntityFactory(**kwargs)

    def prepare_test(self):
        super().prepare_test()

        self.kept_episodes = [
            EpisodeFactory(_podcast_id=self.expected_entity_ids[0], owner=self.user),
            EpisodeFactory(_podcast_id=self.expected_entity_ids[0], owner=self.user),
        ]

        self.removed_episodes = [
            EpisodeFactory(_podcast_id=self.delete_entity_ids[0], owner=self.user),
            EpisodeFactory(_podcast_id=self.delete_entity_ids[0], owner=self.user),
        ]

    def test_delete_entities(self):
        super().test_delete_entities()

        self.assertEqual(
            {e.id for e in self.kept_episodes},
            {e.id for e in Episode.objects.all()},
        )
