from core.tests.queries.entities.test_order_by import OrderByBaseTestCases
from entities.podcast.factories import EpisodeFactory, PodcastFactory


class BuildEntityMixin:
    def __init__(self, *args, **kwargs):
        self._podcast = None
        self.owner = None
        super().__init__(*args, **kwargs)

    @property
    def podcast(self):
        if not self._podcast:
            self._podcast = PodcastFactory(owner=self.owner)
        return self._podcast

    def build_entity(self, **kwargs):
        if "podcast" not in kwargs:
            kwargs["podcast"] = self.podcast
        return EpisodeFactory(**kwargs)


class TestOrderByTimeCreated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeCreated,
):
    pass


class TestOrderByTimeUpdated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeUpdated,
):
    pass


class TestOrderByTimePublished(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimePublished,
):
    pass


class TestOrderByLastAction(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastAction,
):
    pass


class TestOrderByLastSeen(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastSeen,
):
    pass


class TestOrderByStartDateNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByStartDateNotAvailable,
):
    pass


class TestOrderByFileSizeNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByFileSizeNotAvailable,
):
    pass


class TestOrderByTitle(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTitle,
):
    pass


class TestOrderByGroupName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByGroupName,
):
    pass


class TestOrderByOwnerName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByOwnerName,
):
    pass


class TestOrderByReadAccess(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByReadAccess,
):
    pass
