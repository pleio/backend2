from core.tests.helpers.entity_filters import Template
from entities.discussion.factories import DiscussionFactory
from entities.podcast.factories import PodcastFactory


class TestPodcastFilters(Template.TestEntityFiltersTestCase):
    def get_subtype(self):
        return "podcast"

    def subtype_factory(self, **kwargs):
        return PodcastFactory(**kwargs)

    def reference_factory(self, **kwargs):
        return DiscussionFactory(**kwargs)

    def test_entity_query(self):
        if not self.include_entity_query:  # pragma: no cover
            return

        query = """
        query EntityQuery($subtype: String) {
            entities(subtype: $subtype) {
                edges {
                    guid
                }
            }
        }
        """

        self.graphql_client.force_login(self.visitor)
        result = self.graphql_client.post(query, {})

        guids = {e["guid"] for e in result["data"]["entities"]["edges"]}
        self.assertEqual(
            guids,
            {
                self.reference1.guid,
                self.reference2.guid,
            },
        )

        self.graphql_client.force_login(self.visitor)
        result = self.graphql_client.post(query, {"subtype": self.get_subtype()})

        guids = {e["guid"] for e in result["data"]["entities"]["edges"]}
        self.assertEqual(guids, {self.article1.guid, self.article2.guid})

    def test_activity_query(self):
        if not self.include_activity_query:  # pragma: no cover
            return

        query = """
        query EntityQuery($subtypes: [String]) {
            activities(subtypes: $subtypes) {
                edges {
                    guid
                }
            }
        }
        """

        def activities(*guids):
            return {"activity:%s" % id for id in guids}

        self.graphql_client.force_login(self.visitor)
        result = self.graphql_client.post(query, {})

        guids = {e["guid"] for e in result["data"]["activities"]["edges"]}
        self.assertEqual(
            guids,
            activities(
                self.reference1.guid,
                self.reference2.guid,
            ),
        )

        self.graphql_client.force_login(self.visitor)
        result = self.graphql_client.post(query, {"subtypes": [self.get_subtype()]})

        guids = {e["guid"] for e in result["data"]["activities"]["edges"]}
        self.assertEqual(guids, activities(self.article1.guid, self.article2.guid))
