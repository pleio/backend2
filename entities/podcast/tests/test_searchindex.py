from core.tests.helpers.search_index_test_template import Template


class TestUserSearchIndexTestCase(Template.SearchIndexTestTestCase):
    index_name = "podcast"
    expected_hook = "entities.podcast.core_hooks.test_elasticsearch_index"
