from core.tests.helpers.test_translation import Wrapper
from entities.podcast.factories import BuildPodcastOnceFactoryMixin, EpisodeFactory


class TestTranslationFields(
    BuildPodcastOnceFactoryMixin,
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Episode"

    def build_entity(self, **kwargs):
        return EpisodeFactory(**self.add_podcast_to_episode(**kwargs))
