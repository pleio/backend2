from core.tests.helpers.test_entity_with_publish_request import PublishRequestTestCases
from entities.podcast.factories import BuildPodcastOnceFactoryMixin, EpisodeFactory


class TestEditEntityWithPublishRequest(
    BuildPodcastOnceFactoryMixin,
    PublishRequestTestCases.TestEditEntityWithPublishRequest,
):
    TYPE_TO_STRING = "episode"

    def entity_factory(self, **kwargs):
        return EpisodeFactory(**self.add_podcast_to_episode(**kwargs))
