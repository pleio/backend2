from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory
from entities.podcast.factories import (
    BuildPodcastOnceFactoryMixin,
    EpisodeFactory,
    PodcastFactory,
)


class TestSearchWithExcludedEpisodesTestCase(
    BuildPodcastOnceFactoryMixin,
    Template.TestSearchWithExcludedContentTypesTestCase,
):
    EXCLUDE_TYPES = ["episode", "podcast"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return EpisodeFactory(
            title=title,
            owner=self.owner,
            podcast=self.get_podcast(owner=self.owner),
        )


class TestSearchWithExcludedPodcastsTestCase(
    Template.TestSearchWithExcludedContentTypesTestCase
):
    EXCLUDE_TYPES = ["podcast"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return PodcastFactory(title=title, owner=self.owner)
