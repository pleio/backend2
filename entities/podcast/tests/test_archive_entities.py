from core.tests.helpers.test_archive_entities import Wrapper
from entities.podcast.factories import (
    BuildPodcastOnceFactoryMixin,
    EpisodeFactory,
    PodcastFactory,
)


class TestArchiveEpisodeEntities(
    BuildPodcastOnceFactoryMixin, Wrapper.TestArchiveEntities
):
    def create_entity(self, **kwargs):
        return EpisodeFactory(**self.add_podcast_to_episode(**kwargs))


class TestArchivePodcastEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return PodcastFactory(**kwargs)
