from unittest.mock import patch

from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.news.models import News
from entities.podcast.models import Podcast
from user.factories import AdminFactory, UserFactory


@tag("createEntity")
class AddPodcastTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.owner = UserFactory()
        self.admin = AdminFactory()
        self.group = GroupFactory(owner=self.owner)
        self.group.join(self.owner, "owner")
        self.suggestedNews = mixer.blend(News)
        icon_file = self.file_factory("fake_file_path")

        self.input_vars = {
            "abstract": "Abstract of 'Adding a Podcast'",
            "inputLanguage": "en",
            "isTranslationEnabled": False,
            "accessId": 0,
            "authors": [{"name": self.owner.name}, {"name": "Arthur Dent"}],
            "iconGuid": icon_file.guid,
            "isFeatured": True,
            "isRecommended": True,
            "isRecommendedInSearch": True,
            "richDescription": self.tiptap_paragraph(
                "The 'richDescription' for 'Adding a Podcast'"
            ),
            "scheduleArchiveEntity": str(
                timezone.localtime() + timezone.timedelta(days=10)
            ),
            "scheduleDeleteEntity": str(
                timezone.localtime() + timezone.timedelta(days=20)
            ),
            "suggestedItems": [self.suggestedNews.guid],
            "tags": ["tag1", "tag2"],
            "timePublished": str(timezone.localtime()),
            "title": "Adding a Podcast",
            "writeAccessId": 0,
        }

        self.mutation = """
            fragment PodcastParts on Podcast {
                inputLanguage
                isTranslationEnabled
                abstract
                accessId
                authors {
                    name
                }
                canEdit
                canArchiveAndDelete
                group {
                    guid
                }
                icon {
                    guid
                    title
                }
                isFeatured
                isRecommendedInSearch
                inGroup
                isRecommended
                richDescription
                scheduleArchiveEntity
                scheduleDeleteEntity
                statusPublished
                suggestedItems {
                    guid
                }
                tags
                timeCreated
                timePublished
                timeUpdated
                title
                url
                writeAccessId
                showOwner
            }
            mutation ($input: addPodcastInput!) {
                addPodcast(input: $input) {
                    podcast {
                        guid
                        status
                        ...PodcastParts
                    }
                }
            }
        """

        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def _add_podcast_by(self, user):
        """Test creating a podcast."""

        # -- Assumptions --
        self.assertEqual(Podcast.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        response = response["data"]["addPodcast"]["podcast"]
        input_vars = self.input_vars

        self.assertEqual(response["inputLanguage"], input_vars["inputLanguage"])
        self.assertEqual(response["isTranslationEnabled"], False)
        self.assertEqual(response["abstract"], input_vars["abstract"])
        self.assertEqual(response["accessId"], input_vars["accessId"])
        self.assertEqual(response["richDescription"], input_vars["richDescription"])
        self.assertEqual(response["statusPublished"], "published")
        self.assertDateEqual(
            response["scheduleArchiveEntity"], input_vars["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            response["scheduleDeleteEntity"], input_vars["scheduleDeleteEntity"]
        )
        self.assertEqual(response["suggestedItems"][0]["guid"], self.suggestedNews.guid)
        self.assertEqual(response["tags"], input_vars["tags"])
        self.assertDateEqual(response["timePublished"], input_vars["timePublished"])
        self.assertEqual(response["title"], input_vars["title"])
        self.assertEqual(response["writeAccessId"], input_vars["writeAccessId"])
        self.assertEqual(response["icon"]["guid"], input_vars["iconGuid"])
        self.assertEqual(response["authors"], input_vars["authors"])
        self.assertEqual(response["showOwner"], True)

        if set(user.roles) & {"ADMIN", "EDITOR"}:
            self.assertEqual(response["isFeatured"], input_vars["isFeatured"])
            self.assertEqual(response["isRecommended"], input_vars["isRecommended"])
        else:
            self.assertNotEqual(response["isFeatured"], input_vars["isFeatured"])
            self.assertNotEqual(response["isRecommended"], input_vars["isRecommended"])

        # -- Assertions on created podcast --
        podcast = Podcast.objects.last()
        self.assertEqual(podcast.abstract, input_vars["abstract"])
        self.assertTrue(user.guid in podcast.read_access[0])
        self.assertEqual(podcast.rich_description, input_vars["richDescription"])
        self.assertEqual(podcast.icon.guid, input_vars["iconGuid"])
        self.assertEqual(podcast.status_published.value, "published")
        self.assertDateEqual(
            str(podcast.schedule_archive_after), input_vars["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            str(podcast.schedule_delete_after), input_vars["scheduleDeleteEntity"]
        )
        self.assertEqual(str(podcast.suggested_items[0]), self.suggestedNews.guid)
        self.assertEqual(podcast.tags, input_vars["tags"])
        (self.assertDateEqual(str(podcast.published), input_vars["timePublished"]),)
        self.assertEqual(podcast.title, input_vars["title"])
        self.assertTrue(user.guid in podcast.write_access[0])
        self.assertEqual(podcast.authors, input_vars["authors"])

        if set(user.roles) & {"ADMIN", "EDITOR"}:
            self.assertEqual(podcast.is_featured, input_vars["isFeatured"])
            self.assertEqual(podcast.is_recommended, input_vars["isRecommended"])
            self.assertEqual(
                podcast.is_recommended_in_search, input_vars["isRecommendedInSearch"]
            )
        else:
            self.assertNotEqual(podcast.is_featured, input_vars["isFeatured"])
            self.assertNotEqual(podcast.is_recommended, input_vars["isRecommended"])
            self.assertNotEqual(
                podcast.is_recommended_in_search, input_vars["isRecommendedInSearch"]
            )

        self.mocked_revert_published.assert_called_once()
        self.mocked_revert_published.reset_mock()
        self.mocked_create_publish_request.assert_called_once()
        self.mocked_create_publish_request.reset_mock()

    def test_add_podcast_by_not_admin(self):
        self._add_podcast_by(self.owner)

    def test_add_podcast_by_admin(self):
        self._add_podcast_by(self.admin)

    def test_add_podcast_to_group(self):
        # -- Arrange --
        self.input_vars["groupGuid"] = self.group.guid

        # -- Assumptions --
        self.assertEqual(Podcast.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        response = response["data"]["addPodcast"]["podcast"]
        self.assertTrue(response["inGroup"])
        self.assertEqual(response["group"]["guid"], self.group.guid)

        # -- Assertions on created podcast --
        podcast = Podcast.objects.last()
        self.assertEqual(podcast.group, self.group)

    def test_add_unpublished_podcast(self):
        # -- Arrange --
        self.input_vars["timePublished"] = None

        # -- Assumptions --
        self.assertEqual(Podcast.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        response = response["data"]["addPodcast"]["podcast"]
        self.assertIsNone(response["timePublished"])
        self.assertEqual(response["statusPublished"], "draft")

        # -- Assertions on created podcast --
        podcast = Podcast.objects.last()
        self.assertIsNone(podcast.published)
        self.assertEqual(podcast.status_published.value, "draft")

    def test_add_minimal_podcast(self):
        # -- Arrange --
        input_vars = {"title": "Simple podcast"}

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": input_vars})

        # -- Assertions on response --
        self.assertIsNotNone(response["data"]["addPodcast"])

        # -- Assertions on created podcast --
        podcast = Podcast.objects.last()
        self.assertEqual(podcast.title, input_vars["title"])

    def test_add_podcast_with_attachment(self):
        # -- Arrange --
        attachment = self.file_factory("fake_file_path")
        self.input_vars["richDescription"] = self.tiptap_attachment(attachment)

        # -- Assumptions --
        self.assertEqual(Podcast.objects.count(), 0)

        # -- Act --
        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, {"input": self.input_vars})

        # -- Assertions on response --
        self.assertIsNotNone(response["data"]["addPodcast"])

        # -- Assertions on created podcast --
        podcast = Podcast.objects.last()
        self.assertTrue(podcast.attachments.filter(file_id=attachment.id).exists())
