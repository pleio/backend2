from core.tests.helpers.entity_filters import Template
from entities.discussion.factories import DiscussionFactory
from entities.podcast.factories import BuildPodcastOnceFactoryMixin, EpisodeFactory


class TestEpisodeFilters(
    BuildPodcastOnceFactoryMixin, Template.TestEntityFiltersTestCase
):
    def get_subtype(self):
        return "episode"

    def subtype_factory(self, **kwargs):
        return EpisodeFactory(**self.add_podcast_to_episode(**kwargs))

    def reference_factory(self, **kwargs):
        return DiscussionFactory(**kwargs)
