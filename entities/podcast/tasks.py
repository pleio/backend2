from io import BytesIO
from os.path import basename

from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django_tenants.utils import schema_context
from pydub import AudioSegment

from entities.file.models import FileFolder

logger = get_task_logger(__name__)


@shared_task
def schedule_convert_to_mp3(schema_name, file_guid):
    def convert_to_mp3(file, target_filetype="mp3", bitrate="192k"):
        original_extension = file.name.split(".")[-1]
        mp3_converted_file = AudioSegment.from_file(file.open(), original_extension)

        output = BytesIO()
        mp3_converted_file.export(output, format=target_filetype, bitrate=bitrate)

        new_filename = basename(file.name)[:-3] + target_filetype
        contents = output.getvalue()
        converted_audiofile = ContentFile(contents, new_filename)
        converted_audiofile.name = new_filename
        return converted_audiofile

    with schema_context(schema_name):
        try:
            file_object = FileFolder.objects.get(id=file_guid)
        except ObjectDoesNotExist:
            logger.error(
                "convert_mp3, in schema %s, the file with guid %s does not exist.",
                schema_name,
                file_guid,
            )
        try:
            file_object.upload = convert_to_mp3(file_object.upload)
            file_object.save()
        except Exception as e:
            logger.error(
                "convert_to_mp3 failed in schema %s, for file with guid %s: %s",
                schema_name,
                file_guid,
                e,
            )
