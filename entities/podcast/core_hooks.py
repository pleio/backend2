from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from core.exceptions import ExceptionDuringQueryIndex, IgnoreIndexError
from core.tests.helpers import GraphQLClient
from user.models import User


def get_entity_filters():
    yield {
        "key": "podcast",
        "value": _("Podcast"),
    }
    yield {
        "key": "episode",
        "value": _("Podcast episode"),
    }


def get_activity_filters():
    # only return podcast episode in activity filters
    yield {
        "key": "episode",
        "value": _("Podcast episode"),
    }


def get_search_filters():
    yield {
        "key": "podcast",
        "value": _("Podcast"),
        "plural": pgettext_lazy("plural", "Podcasts"),
    }
    yield {
        "key": "episode",
        "value": _("Podcast episode"),
        "plural": pgettext_lazy("plural", "Podcast episodes"),
    }


def test_elasticsearch_index(index_name):
    if index_name != "podcast":
        raise IgnoreIndexError()

    try:
        client = GraphQLClient()
        client.force_login(User.objects.filter(is_superadmin=True).first())
        client.post(
            """
            query ElasticsearchQuery($type: String) {
                search(subtype: $type, subtypes: [$type]) {
                    total
                    totals {
                        title
                        subtype
                        total
                    }
                    edges {
                        ... on Podcast {
                            guid
                            title
                        }
                    }
                }
            }
            """,
            {
                "type": "podcast",
            },
        )
    except Exception as e:
        raise ExceptionDuringQueryIndex(str(e))
