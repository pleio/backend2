from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from entities.podcast.models import Episode, Podcast
from user.models import User


def PodcastFactory(**attributes) -> Podcast:
    assert isinstance(attributes.get("owner"), User), "owner is a required property"
    attributes.setdefault("read_access", [ACCESS_TYPE.public])
    attributes.setdefault(
        "write_access", [ACCESS_TYPE.user.format(attributes["owner"].guid)]
    )
    return mixer.blend(Podcast, **attributes)


class BuildPodcastOnceFactoryMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._podcast = None

    def get_podcast(self, **kwargs):
        if not self._podcast:
            self._podcast = PodcastFactory(**kwargs)
        return self._podcast

    def add_podcast_to_episode(self, **kwargs):
        if "podcast" not in kwargs:
            kwargs["podcast"] = self.get_podcast(owner=kwargs["owner"])
        return kwargs


def EpisodeFactory(**attributes) -> Episode:
    assert isinstance(attributes.get("owner"), User), "owner is a required property"
    if "podcast" in attributes:
        assert isinstance(attributes.get("podcast"), Podcast), (
            "podcast is a required property"
        )
    elif "_podcast" in attributes:
        assert isinstance(attributes.get("_podcast"), Podcast), (
            "_podcast is a required property"
        )
    elif "_podcast_id" in attributes:
        pass
    else:
        msg = "Ppodcast is a required property (add podcast, _podcast or _podcast_id)"
        raise AssertionError(msg)
    attributes.setdefault("read_access", [ACCESS_TYPE.public])
    attributes.setdefault(
        "write_access", [ACCESS_TYPE.user.format(attributes["owner"].guid)]
    )
    if "podcast" in attributes:
        attributes["_podcast"] = attributes["podcast"]
        del attributes["podcast"]
    if "_podcast" not in attributes:
        attributes["_podcast"] = PodcastFactory(owner=attributes["owner"])
    return mixer.blend(Episode, **attributes)
