from django.contrib.syndication.views import Feed
from django.core.exceptions import PermissionDenied
from django.utils.feedgenerator import Rss201rev2Feed

from core.constances import ACCESS_TYPE
from core.lib import get_full_url
from entities.podcast.models import Podcast


class ItunesPodcastRssFeed(Rss201rev2Feed):
    def rss_attributes(self):
        attributes = super().rss_attributes()
        attributes["xmlns:itunes"] = "http://www.itunes.com/dtds/podcast-1.0.dtd"
        attributes["xmlns:podcast"] = "https://podcastindex.org/namespace/1.0"
        attributes["xmlns:content"] = "http://purl.org/rss/1.0/modules/content/"
        return attributes

    def add_root_elements(self, handler):
        super().add_root_elements(handler)
        handler.addQuickElement(
            "itunes:image", attrs={"href": self.feed.get("icon", "")}
        )
        handler.addQuickElement("podcast:guid", self.feed.get("guid", ""))
        handler.addQuickElement("itunes:explicit", "false")
        handler.addQuickElement(
            "itunes:category", attrs={"text": self.feed.get("itunes_category", "Books")}
        )
        handler.addQuickElement("itunes:author", self.feed.get("authors"))
        language = self.feed.get("language")
        if language:
            handler.addQuickElement("language", language)

    def add_item_elements(self, handler, item):
        super().add_item_elements(handler, item)
        if tags := item.get("tags"):
            for tag in tags:
                handler.addQuickElement("tag", tag)
        if duration := item.get("duration"):
            handler.addQuickElement("itunes:duration", str(duration))
        if file := item.get("file"):
            handler.addQuickElement("file", file)


class PodcastFeed(Feed):
    """This rss feed aims to conform to the PSP-1 standards as described here
    https://github.com/Podcast-Standards-Project/PSP-1-Podcast-RSS-Specification#required-channel-elements
    """

    feed_type = ItunesPodcastRssFeed

    def get_object(self, request, podcast_id):
        podcast = Podcast.objects.get(id=podcast_id)
        if ACCESS_TYPE.public in podcast.read_access:
            return podcast
        try:
            return Podcast.objects.visible_all(request.user).get(id=podcast_id)
        except Podcast.DoesNotExist:
            pass
        raise PermissionDenied()

    def feed_extra_kwargs(self, podcast):
        return {
            "icon": self.get_icon(podcast),
            "guid": podcast.guid,
            "authors": self.get_authors(podcast),
        }

    def title(self, podcast):
        return podcast.title

    def description(self, podcast):
        return self._description_or_excerpt(podcast)

    def link(self, podcast):
        return podcast.url

    def get_icon(self, podcast):
        if podcast.icon:
            return get_full_url(podcast.icon.download_url)
        return ""

    def get_authors(self, podcast):
        return podcast.authors_to_string

    def items(self, podcast):
        return podcast.episodes.filter(
            read_access__contains=[ACCESS_TYPE.public]
        ).filter(file__read_access__contains=[ACCESS_TYPE.public])

    def item_extra_kwargs(self, episode):
        extra_kwargs = super().item_extra_kwargs(episode)
        extra_kwargs["tags"] = self.get_tags(episode)
        if episode.file:
            extra_kwargs["duration"] = episode.file.duration
        return extra_kwargs

    def item_title(self, episode):
        return episode.title

    def item_description(self, episode):
        return self._description_or_excerpt(episode)

    def item_link(self, episode):
        return episode.url

    def item_pubdate(self, episode):
        return episode.published

    def get_tags(self, episode):
        return episode.tags

    def item_enclosure_url(self, episode):
        if episode.file:
            return get_full_url(episode.file.download_url)
        return ""

    def item_enclosure_length(self, episode):
        if episode.file:
            return episode.file.size
        return ""

    def item_enclosure_mime_type(self, episode):
        if episode.file:
            return episode.file.mime_type
        return ""

    def item_guid(self, episode):
        return episode.guid

    def _description_or_excerpt(self, obj):
        if obj.description:
            description_bytes = obj.description.encode("utf-8")
            if len(description_bytes) < 4000:
                return obj.description
            return obj.excerpt
