from post_deploy import post_deploy_action

from core.lib import is_schema_public
from entities.podcast.models import Podcast


@post_deploy_action
def create_references_for_icons():
    if is_schema_public():
        return

    for podcast in Podcast.objects.filter(icon__isnull=False):
        podcast.update_attachments_links()
