from django.urls import path

from entities.podcast.views import PodcastFeed

urlpatterns = [
    path("<str:podcast_id>/rss/", PodcastFeed(), name="podcast_feed"),
]
