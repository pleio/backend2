# Generated by Django 4.2.16 on 2024-11-20 13:34

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("podcast", "0004_podcast_authors"),
    ]

    operations = [
        migrations.AddField(
            model_name="episode",
            name="sub_title",
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="podcast",
            name="sub_title",
            field=models.TextField(blank=True, null=True),
        ),
    ]
