# Generated by Django 4.2.2 on 2023-06-28 09:34

import uuid

import django.contrib.postgres.fields
import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models

import core.models.mixin


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("core", "0094_revision_coauthors"),
        ("file", "0016_filereference_created_at"),
    ]

    operations = [
        migrations.CreateModel(
            name="Podcast",
            fields=[
                (
                    "entity_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="core.entity",
                    ),
                ),
                ("title", models.CharField(max_length=256)),
                ("abstract", models.TextField(blank=True, null=True)),
                ("rich_description", models.TextField(blank=True, null=True)),
                ("featured_video", models.TextField(blank=True, null=True)),
                ("featured_video_title", models.CharField(default="", max_length=256)),
                ("featured_position_y", models.IntegerField(default=0)),
                ("featured_alt", models.CharField(default="", max_length=256)),
                (
                    "featured_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="file.filefolder",
                    ),
                ),
            ],
            options={
                "ordering": ["-published"],
            },
            bases=(
                core.models.mixin.RichDescriptionMediaMixin,
                "core.entity",
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="Episode",
            fields=[
                ("title", models.CharField(max_length=256)),
                ("abstract", models.TextField(blank=True, null=True)),
                ("rich_description", models.TextField(blank=True, null=True)),
                (
                    "_tag_summary",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.CharField(max_length=256),
                        blank=True,
                        db_column="tags",
                        default=list,
                        size=None,
                    ),
                ),
                (
                    "_category_summary",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.CharField(max_length=256),
                        blank=True,
                        db_column="categories",
                        default=list,
                        size=None,
                    ),
                ),
                ("category_tags", models.JSONField(blank=True, default=list)),
                ("created_at", models.DateTimeField(default=django.utils.timezone.now)),
                ("updated_at", models.DateTimeField(default=django.utils.timezone.now)),
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "file",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="file.filefolder",
                    ),
                ),
                (
                    "podcast",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="episodes",
                        to="podcast.podcast",
                    ),
                ),
            ],
            options={
                "ordering": ["-created_at"],
            },
            bases=(core.models.mixin.RichDescriptionMediaMixin, models.Model),
        ),
    ]
