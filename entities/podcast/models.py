from auditlog.registry import auditlog
from django.db import models
from django.utils.text import slugify

from core.models import (
    ArticleMixin,
    AttachmentMixin,
    BookmarkMixin,
    Entity,
    MentionMixin,
    VoteMixin,
)
from core.models.mixin import FollowMixin, TitleMixin


class Podcast(
    TitleMixin,
    MentionMixin,
    AttachmentMixin,
    ArticleMixin,
    Entity,
    VoteMixin,
    BookmarkMixin,
    FollowMixin,
):
    class Meta:
        ordering = ["-published"]

    icon = models.ForeignKey(
        "file.FileFolder",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    authors = models.JSONField(default=list, blank=True, null=True)

    def __str__(self):
        return f"Podcast[{self.title}]"

    def has_revisions(self):
        return True

    def lookup_attachments(self):
        yield from super().lookup_attachments()
        if self.icon:
            yield self.icon.guid

    @property
    def type_to_string(self):
        return "podcast"

    @property
    def guid(self):
        return str(self.id)

    @property
    def url(self):
        if self.group:
            prefix = f"/groups/view/{self.group.guid}/{slugify(self.group.name)}"
        else:
            prefix = ""

        return f"{prefix}/podcasts/view/{self.guid}/{self.slug}".lower()

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    @property
    def authors_to_string(self):
        return ", ".join(author.get("name") for author in self.authors)

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "icon": self.icon.url if self.icon else "",
            "abstract": self.abstract or "",
            "isTranslationEnabled": self.is_translation_enabled,
            **super().serialize(),
        }


class Episode(TitleMixin, MentionMixin, AttachmentMixin, ArticleMixin, Entity):
    class Meta:
        ordering = ["-published"]

    file = models.ForeignKey(
        "file.FileFolder",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    _podcast = models.ForeignKey(
        "podcast.Podcast",
        on_delete=models.CASCADE,
        related_name="episodes",
        null=False,
        blank=False,
    )

    def __str__(self):
        return f"Episode[{self.title}]"

    @property
    def podcast(self):
        """Episode inherits from 'Entity' and gets a 'podcast' field from that.
        This causes a name conflict with the foreignkey field to podcasts.
        This property solves that issue"""
        return self._podcast

    @podcast.setter
    def set_podcast(self, podcast):
        self._podcast = podcast

    @property
    def type_to_string(self):
        return "episode"

    @property
    def url(self):
        if self.group:
            prefix = f"/groups/view/{self.group.guid}/{slugify(self.group.name)}"
        else:
            prefix = ""

        return f"{prefix}/podcasts/episodes/view/{self.guid}/{self.slug}".lower()

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "abstract": self.abstract or "",
            "file": self.file.url if self.file else "",
            **super().serialize(),
        }

    def lookup_attachments(self):
        yield from super().lookup_attachments()
        if self.file:
            yield self.file.guid


auditlog.register(Podcast)
auditlog.register(Episode)
