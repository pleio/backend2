from ariadne import ObjectType

from .episode import episode
from .mutation import (
    resolve_add_episode,
    resolve_add_podcast,
    resolve_edit_episode,
    resolve_edit_podcast,
)
from .podcast import podcast

mutation = ObjectType("Mutation")
mutation.set_field("addPodcast", resolve_add_podcast)
mutation.set_field("editPodcast", resolve_edit_podcast)
mutation.set_field("addEpisode", resolve_add_episode)
mutation.set_field("editEpisode", resolve_edit_episode)

resolvers = [mutation, podcast, episode]
