from ariadne import ObjectType

from core.resolvers import shared

episode = ObjectType("Episode")


@episode.field("subtype")
def resolve_subtype(obj, info):
    return "episode"


@episode.field("inGroup")
def resolve_in_group(obj, info):
    return obj.group is not None


@episode.field("group")
def resolve_group(obj, info):
    return obj.group


@episode.field("isFeatured")
def resolve_is_featured(obj, info):
    return obj.is_featured


@episode.field("isRecommended")
def resolve_is_recommended(obj, info):
    return obj.is_recommended


@episode.field("url")
def resolve_url(obj, info):
    return obj.url


@episode.field("canEditGroup")
def resolve_can_edit_group(obj, info):
    """Editing of the group is done via the podcast object."""
    return False


episode.set_field("inputLanguage", shared.resolve_entity_input_language)
episode.set_field("guid", shared.resolve_entity_guid)
episode.set_field("status", shared.resolve_entity_status)
episode.set_field("title", shared.resolve_entity_title)
episode.set_field("localTitle", shared.resolve_entity_local_title)
episode.set_field("abstract", shared.resolve_entity_abstract)
episode.set_field("localAbstract", shared.resolve_entity_local_abstract)
episode.set_field("description", shared.resolve_entity_description)
episode.set_field("localDescription", shared.resolve_entity_local_description)
episode.set_field("richDescription", shared.resolve_entity_rich_description)
episode.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
episode.set_field("excerpt", shared.resolve_entity_excerpt)
episode.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
episode.set_field("tags", shared.resolve_entity_tags)
episode.set_field("tagCategories", shared.resolve_entity_categories)
episode.set_field("timeCreated", shared.resolve_entity_time_created)
episode.set_field("timeUpdated", shared.resolve_entity_time_updated)
episode.set_field("timePublished", shared.resolve_entity_time_published)
episode.set_field(
    "scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity
)
episode.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
episode.set_field("statusPublished", shared.resolve_entity_status_published)
episode.set_field("canEdit", shared.resolve_entity_can_edit)
episode.set_field("canEditAdvanced", shared.resolve_entity_can_edit_advanced)
episode.set_field("accessId", shared.resolve_entity_access_id)
episode.set_field("writeAccessId", shared.resolve_entity_write_access_id)
episode.set_field("views", shared.resolve_entity_views)
episode.set_field("owner", shared.resolve_entity_owner)
episode.set_field("isPinned", shared.resolve_entity_is_pinned)
episode.set_field("suggestedItems", shared.resolve_entity_suggested_items)
episode.set_field("lastSeen", shared.resolve_entity_last_seen)
episode.set_field("showOwner", shared.resolve_entity_show_owner)
episode.set_field(
    "isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search
)
episode.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
episode.set_field("publishRequest", shared.resolve_entity_publish_request)
episode.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
