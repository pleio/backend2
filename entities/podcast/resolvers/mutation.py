from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input, get_file_extension, tenant_schema
from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from core.utils.entity import load_entity_by_id
from entities.file.models import FileFolder
from entities.podcast.models import Episode, Podcast
from entities.podcast.tasks import schedule_convert_to_mp3


def resolve_add_podcast(_, info, input):
    user = info.context["request"].user
    clean_input = input

    shared.assert_authenticated(user)
    group = shared.get_group(clean_input)
    shared.assert_group_member(user, group)

    podcast = Podcast(owner=user, group=group)

    track_publication_date = ContentModerationTrackTimePublished(
        podcast, user, is_new=True
    )

    if authors := clean_input.get("authors"):
        podcast.authors = authors
    shared.resolve_update_entity_icon(podcast, clean_input)
    shared.resolve_add_input_language(podcast, clean_input)
    shared.resolve_add_access_id(podcast, clean_input, user)
    shared.resolve_update_tags(podcast, clean_input)
    shared.resolve_update_title(podcast, clean_input)
    shared.resolve_update_rich_description(podcast, clean_input)
    shared.update_is_translation_enabled(podcast, clean_input)
    shared.resolve_update_abstract(podcast, clean_input)
    shared.update_featured_image(podcast, clean_input)
    shared.resolve_add_suggested_items(podcast, clean_input)
    shared.update_publication_dates(podcast, user, clean_input)
    shared.update_is_recommended(podcast, user, clean_input)
    shared.update_is_recommended_in_search(podcast, user, clean_input)
    shared.update_is_featured(podcast, user, clean_input)

    track_publication_date.maybe_revert_time_published()
    podcast.save()

    track_publication_date.maybe_create_publish_request()

    return {"podcast": podcast}


def resolve_edit_podcast(_, info, input):
    user = info.context["request"].user
    clean_input = input
    shared.assert_authenticated(user)
    podcast = load_entity_by_id(input["guid"], [Podcast])
    track_publication_date = ContentModerationTrackTimePublished(podcast, user)

    if authors := clean_input.get("authors"):
        podcast.authors = authors

    shared.resolve_update_entity_icon(podcast, clean_input)
    shared.assert_write_access(podcast, user)
    shared.resolve_update_input_language(podcast, clean_input)
    shared.resolve_update_tags(podcast, clean_input)
    shared.resolve_update_access_id(podcast, clean_input, user)
    shared.resolve_update_title(podcast, clean_input)
    shared.resolve_update_rich_description(podcast, clean_input)
    shared.update_is_translation_enabled(podcast, clean_input)
    shared.resolve_update_abstract(podcast, clean_input)
    shared.update_featured_image(podcast, clean_input)
    shared.update_publication_dates(podcast, user, clean_input)
    shared.resolve_update_suggested_items(podcast, clean_input)
    shared.update_is_recommended(podcast, user, clean_input)
    shared.update_is_recommended_in_search(podcast, user, clean_input)
    shared.update_is_featured(podcast, user, clean_input)
    shared.update_updated_at(podcast)

    if user.is_site_admin or (podcast.group and podcast.group.can_write(user)):
        shared.resolve_update_owner(podcast, clean_input)
        shared.resolve_update_time_created(podcast, clean_input)

    if user.is_site_admin:
        current_group_guid = podcast.group.guid if podcast.group else None
        if (
            "groupGuid" in clean_input
            and clean_input["groupGuid"] != current_group_guid
        ):
            shared.resolve_update_group(podcast, clean_input)
            for episode in podcast.episodes.all():
                shared.resolve_update_group(episode, clean_input)
                episode.save()

    track_publication_date.maybe_revert_time_published()
    podcast.save()

    track_publication_date.maybe_create_publish_request()

    return {"podcast": podcast}


def resolve_add_episode(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)
    shared.assert_authenticated(user)

    group = shared.get_group(clean_input)
    shared.assert_group_member(user, group)

    episode = Episode()
    episode.owner = user
    track_publication_date = ContentModerationTrackTimePublished(
        episode, user, is_new=True
    )

    if group:
        episode.group = group

    episode._podcast_id = clean_input.get("podcastGuid")

    shared.resolve_add_input_language(episode, clean_input)
    shared.resolve_add_access_id(episode, clean_input, user)
    shared.resolve_update_tags(episode, clean_input)
    shared.resolve_update_title(episode, clean_input)
    shared.resolve_update_rich_description(episode, clean_input)
    shared.update_is_translation_enabled(episode, clean_input)
    shared.resolve_update_abstract(episode, clean_input)
    shared.resolve_add_suggested_items(episode, clean_input)
    shared.update_publication_dates(episode, user, clean_input)
    shared.update_is_recommended(episode, user, clean_input)
    shared.update_is_recommended_in_search(episode, user, clean_input)
    shared.update_is_featured(episode, user, clean_input)
    episode_file = None
    if "fileGuid" in clean_input:
        try:
            episode_file = FileFolder.objects.get(id=clean_input.get("fileGuid"))
        except ObjectDoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)
    episode.file = episode_file

    track_publication_date.maybe_revert_time_published()
    episode.save()

    track_publication_date.maybe_create_publish_request()

    if episode_file and get_file_extension(episode_file.upload.name) != ".mp3":
        schedule_convert_to_mp3.delay(tenant_schema(), episode_file.guid)
    return {"episode": episode}


def _resolve_foreignkey(obj, key, fieldname, related_model, clean_input):
    foreignkey = clean_input.get(key)
    if foreignkey:
        try:
            setattr(obj, fieldname, related_model.objects.get(id=foreignkey))
        except ObjectDoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)


def resolve_edit_episode(_, info, input):
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)
    shared.assert_authenticated(user)
    episode = load_entity_by_id(input["guid"], [Episode])

    track_publication_date = ContentModerationTrackTimePublished(episode, user)

    _resolve_foreignkey(episode, "podcastGuid", "_podcast_id", Podcast, clean_input)
    _resolve_foreignkey(episode, "fileGuid", "file_id", FileFolder, clean_input)

    episode_file = None
    if "fileGuid" in clean_input:
        try:
            episode_file = FileFolder.objects.get(id=clean_input.get("fileGuid"))
        except ObjectDoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)
        episode.file = episode_file

    shared.assert_write_access(episode, user)
    shared.resolve_update_input_language(episode, clean_input)
    shared.resolve_update_tags(episode, clean_input)
    shared.resolve_update_access_id(episode, clean_input, user)
    shared.resolve_update_title(episode, clean_input)
    shared.resolve_update_rich_description(episode, clean_input)
    shared.update_is_translation_enabled(episode, clean_input)
    shared.resolve_update_abstract(episode, clean_input)
    shared.update_publication_dates(episode, user, clean_input)
    shared.resolve_update_suggested_items(episode, clean_input)
    shared.update_is_recommended(episode, user, clean_input)
    shared.update_is_recommended_in_search(episode, user, clean_input)
    shared.update_is_featured(episode, user, clean_input)
    shared.update_updated_at(episode)

    if user.is_site_admin or (episode.group and episode.group.can_write(user)):
        shared.resolve_update_owner(episode, clean_input)
        shared.resolve_update_time_created(episode, clean_input)

    track_publication_date.maybe_revert_time_published()
    episode.save()

    track_publication_date.maybe_create_publish_request()
    if episode_file and get_file_extension(episode_file.upload.name) != ".mp3":
        schedule_convert_to_mp3.delay(tenant_schema(), episode_file.guid)

    return {"episode": episode}
