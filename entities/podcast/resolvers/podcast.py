from ariadne import ObjectType

from core.constances import ORDER_BY, ORDER_DIRECTION
from core.lib import get_full_url
from core.resolvers import shared

podcast = ObjectType("Podcast")


@podcast.field("subtype")
def resolve_subtype(obj, info):
    return "podcast"


@podcast.field("inGroup")
def resolve_in_group(obj, info):
    return obj.group is not None


@podcast.field("group")
def resolve_group(obj, info):
    return obj.group


@podcast.field("isFeatured")
def resolve_is_featured(obj, info):
    return obj.is_featured


@podcast.field("isRecommended")
def resolve_is_recommended(obj, info):
    return obj.is_recommended


@podcast.field("url")
def resolve_url(obj, info):
    return obj.url


@podcast.field("authors")
def resolve_authors(obj, info):
    return obj.authors


@podcast.field("rssUrl")
def resolve_rss_url(obj, info):
    return f"{get_full_url(obj.url)}/rss/"


@podcast.field("episodes")
def resolve_episodes(
    obj,
    info,
    offset=0,
    limit=10,
    orderBy=ORDER_BY.timePublished,
    orderDirection=ORDER_DIRECTION.desc,
):
    user = info.context["request"].user
    episodes = obj.episodes.visible(user)

    order_options = {
        ORDER_BY.timeUpdated: "updated_at",
        ORDER_BY.timeCreated: "created_at",
        ORDER_BY.title: "sort_title",
    }
    order_by = order_options.get(orderBy, "published")
    if orderDirection == ORDER_DIRECTION.desc:
        order_by = f"-{order_by}"
    episodes = episodes.order_by(order_by)

    return {
        "total": len(episodes),
        "edges": episodes[offset : offset + limit],
    }


podcast.set_field("inputLanguage", shared.resolve_entity_input_language)
podcast.set_field("guid", shared.resolve_entity_guid)
podcast.set_field("status", shared.resolve_entity_status)
podcast.set_field("title", shared.resolve_entity_title)
podcast.set_field("localTitle", shared.resolve_entity_local_title)
podcast.set_field("abstract", shared.resolve_entity_abstract)
podcast.set_field("localAbstract", shared.resolve_entity_local_abstract)
podcast.set_field("description", shared.resolve_entity_description)
podcast.set_field("localDescription", shared.resolve_entity_local_description)
podcast.set_field("richDescription", shared.resolve_entity_rich_description)
podcast.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
podcast.set_field("excerpt", shared.resolve_entity_excerpt)
podcast.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
podcast.set_field("tags", shared.resolve_entity_tags)
podcast.set_field("tagCategories", shared.resolve_entity_categories)
podcast.set_field("timeCreated", shared.resolve_entity_time_created)
podcast.set_field("timeUpdated", shared.resolve_entity_time_updated)
podcast.set_field("timePublished", shared.resolve_entity_time_published)
podcast.set_field(
    "scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity
)
podcast.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
podcast.set_field("statusPublished", shared.resolve_entity_status_published)
podcast.set_field("votes", shared.resolve_entity_votes)
podcast.set_field("hasVoted", shared.resolve_entity_has_voted)
podcast.set_field("isFollowing", shared.resolve_entity_is_following)
podcast.set_field("canVote", shared.resolve_entity_can_vote)
podcast.set_field("canBookmark", shared.resolve_entity_can_bookmark)
podcast.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
podcast.set_field("canEdit", shared.resolve_entity_can_edit)
podcast.set_field("canEditAdvanced", shared.resolve_entity_can_edit_advanced)
podcast.set_field("canEditGroup", shared.resolve_entity_can_edit_group)
podcast.set_field("accessId", shared.resolve_entity_access_id)
podcast.set_field("writeAccessId", shared.resolve_entity_write_access_id)
podcast.set_field("views", shared.resolve_entity_views)
podcast.set_field("owner", shared.resolve_entity_owner)
podcast.set_field("isPinned", shared.resolve_entity_is_pinned)
podcast.set_field("suggestedItems", shared.resolve_entity_suggested_items)
podcast.set_field("lastSeen", shared.resolve_entity_last_seen)
podcast.set_field("showOwner", shared.resolve_entity_show_owner)
podcast.set_field(
    "isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search
)
podcast.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
podcast.set_field("publishRequest", shared.resolve_entity_publish_request)
podcast.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
