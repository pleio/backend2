# Generated by Django 4.2.10 on 2024-03-04 15:24

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("file", "0018_filefolder_file_contents"),
        ("external_content", "0003_add_last_known_good_sync"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="externalcontent",
            options={"ordering": ("-created_at",)},
        ),
        migrations.AlterModelOptions(
            name="externalcontentfetchlog",
            options={"ordering": ("-created_at",)},
        ),
        migrations.AddField(
            model_name="externalcontent",
            name="featured_alt",
            field=models.CharField(blank=True, default="", max_length=256, null=True),
        ),
        migrations.AddField(
            model_name="externalcontent",
            name="featured_image",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="file.filefolder",
            ),
        ),
        migrations.AddField(
            model_name="externalcontent",
            name="featured_position_y",
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name="externalcontent",
            name="featured_video",
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="externalcontent",
            name="featured_video_title",
            field=models.CharField(blank=True, default="", max_length=256, null=True),
        ),
        migrations.AddField(
            model_name="externalcontent",
            name="init_data",
            field=models.JSONField(blank=True, default=dict),
        ),
    ]
