from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "entities.external_content"
    label = "external_content"
