from django import template
from django.utils.timezone import datetime

register = template.Library()


@register.filter
def article_published_datetime(entity):
    if entity.published:
        return entity.published
    return (
        datetime.fromisoformat(entity.init_data.get("published_time"))
        if entity.init_data.get("published_time")
        else entity.created_at
    )
