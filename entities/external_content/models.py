import logging
import uuid
from traceback import format_exc
from typing import TYPE_CHECKING

from django.db import models
from django.utils import timezone

from core import config
from core.constances import ACCESS_TYPE
from core.models import AttachmentMixin
from core.models.entity import Entity
from core.models.featured import FeaturedCoverMixin

if TYPE_CHECKING:
    from entities.external_content.api_handlers import ApiHandlerBase

logger = logging.getLogger(__name__)


class ExternalContent(AttachmentMixin, FeaturedCoverMixin, Entity):
    class Meta:
        ordering = ("-created_at",)

    source = models.ForeignKey(
        "external_content.ExternalContentSource",
        on_delete=models.CASCADE,
        related_name="content",
    )

    title = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)
    remote_id = models.CharField(max_length=256)
    canonical_url = models.URLField()
    init_data = models.JSONField(default=dict, blank=True)

    def can_write(self, user):
        return False

    def can_archive(self, user):
        return False

    @property
    def guid(self):
        return str(self.id)

    @property
    def url(self):
        return self.canonical_url

    @property
    def type_to_string(self):
        return self.source.guid

    def description_index_value(self):
        return (
            (self.description or "") + "\n\n" + super().description_index_value()
        ).strip()

    @property
    def rich_fields(self):
        return []

    def save(self, *args, **kwargs):
        if self._state.adding:
            self._sync_access_arrays()
        self.full_clean()
        super().save(*args, **kwargs)

    def _sync_access_arrays(self):
        self.read_access = (
            [ACCESS_TYPE.logged_in] if config.IS_CLOSED else [ACCESS_TYPE.public]
        )
        self.write_access = [ACCESS_TYPE.user.format(self.owner.guid)]

    def __str__(self):
        return "%s: %s" % (self.source.name, self.title)

    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__, self)


class ExternalContentSource(AttachmentMixin, models.Model):
    @property
    def rich_fields(self):
        return []

    class Meta:
        ordering = (
            "handler_id",
            "name",
        )

    handlers = {}

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=256)
    plural_name = models.CharField(max_length=256)
    handler_id = models.CharField(max_length=128)
    settings = models.JSONField(default=dict)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    last_full_sync = models.DateTimeField(default=None, null=True)
    last_update_sync = models.DateTimeField(default=None, null=True)

    @property
    def guid(self):
        return str(self.id)

    def get_handler(self):
        assert self.handler_id in self.get_handlers()
        return self.get_handlers()[self.handler_id](self)

    def set_full_sync(self, value):
        ExternalContentSource.objects.filter(id=self.id).update(last_full_sync=value)

    def set_update_sync(self, value):
        ExternalContentSource.objects.filter(id=self.id).update(last_update_sync=value)

    @classmethod
    def get_handlers(cls):
        from entities.external_content.utils import find_handlers

        if not cls.handlers:
            cls.handlers = dict(find_handlers())
        return cls.handlers

    def pull(self):
        try:
            handler: ApiHandlerBase = self.get_handler()
            handler.pull()
            ExternalContentFetchLog.objects.create(source=self, success=True)
        except Exception as e:
            raise
            logger.error(e)
            ExternalContentFetchLog.objects.create(
                source=self, success=False, message=format_exc()
            )

    def lookup_attachments(self):
        for item in self.settings.get("tag_images", []):
            if item.get("image_guid"):
                yield item["image_guid"]
        yield from super().lookup_attachments()

    def get_read_access(self):
        return [ACCESS_TYPE.public]

    def __str__(self):
        return self.plural_name

    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__, self)


class ExternalContentFetchLog(models.Model):
    class Meta:
        ordering = ("-created_at",)

    created_at = models.DateTimeField(default=timezone.now)
    source = models.ForeignKey(
        "external_content.ExternalContentSource", on_delete=models.CASCADE
    )
    success = models.BooleanField()
    message = models.TextField()

    def __str__(self):
        return "%s: %s" % (self.created_at, self.message)
