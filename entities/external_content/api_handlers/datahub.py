import json
import logging
import urllib.parse
from collections import defaultdict

import requests
from django.core.exceptions import ValidationError
from django.utils import timezone

from core import config
from core.utils.tags import TagCategoryWrapper
from entities.external_content.api_handlers import ApiHandlerBase, ApiHandlerError
from entities.external_content.models import ExternalContent
from entities.external_content.utils import get_or_create_default_author

logger = logging.getLogger(__name__)


class DataHubClient:
    def __init__(self, base_url, batch_size=None):
        self.base_url = base_url
        self.batch_size = batch_size or 50

    def get(self, resource, since: timezone.datetime = None):
        response = requests.get(
            urllib.parse.urljoin(self.base_url, resource),
            {
                "limit": self.batch_size,
                "format": "json",
                **({"modified_after": since.isoformat()} if since else {}),
            },
            timeout=300,
        )
        if not response.ok:
            raise ApiHandlerError("%s: %s" % (response.status_code, response.reason))
        yield from self.iterate_results(response.json())

    def iterate_results(self, data):
        for record in data.get("results") or []:
            yield record

        if data.get("next"):
            response = requests.get(data["next"], timeout=300)
            if not response.ok:
                raise ApiHandlerError(
                    "%s: %s" % (response.status_code, response.reason)
                )
            yield from self.iterate_results(response.json())


class ApiHandler(ApiHandlerBase):
    ID = "datahub"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.study_zones = {}
        self.tag_categories = defaultdict(TagCategoryWrapper)
        self.client = DataHubClient(
            self.source.settings["apiUrl"], self.source.settings["batchSize"]
        )
        self.imported_files = []

    def should_do_full_sync(self):
        return (
            not self.source.last_full_sync
            or self.source.last_full_sync < timezone.now() - timezone.timedelta(days=1)
        )

    def should_do_update(self):
        return (
            self.source.last_update_sync
            and self.source.last_update_sync
            < timezone.now() - timezone.timedelta(hours=1)
        )

    def pull(self):
        if not config.DATAHUB_EXTERNAL_CONTENT_ENABLED:
            return

        if self.should_do_full_sync():
            self.full_sync()
        elif self.should_do_update():
            self.update(self.source.last_update_sync - timezone.timedelta(hours=1))

    def full_sync(self):
        self.pull_study_zones()
        self.pull_files()
        self.apply_tags()
        self.cleanup_files()

        self.source.set_full_sync(timezone.now())
        self.source.set_update_sync(timezone.now())

    def update(self, since):
        self.pull_study_zones()
        self.pull_files(since)

        self.source.set_update_sync(timezone.now())

    def pull_study_zones(self):
        self.study_zones = {}
        for study in self.client.get("studies"):
            self.study_zones[study["name"]] = []
            for zone in study.get("zones") or []:
                self.study_zones[study["name"]].append(zone["name"])

    def pull_files(self, since=None):
        for record in self.client.get("files", since):
            self.import_file(record)

    def import_file(self, record):
        try:
            tag_categories = filter(
                bool,
                [
                    self._load_tags("classification", record),
                    self._load_tags("sensor", record),
                    self._load_tags("study", record),
                    self._load_tags("extension", record),
                    self._load_tags("chapter", record),
                    self._load_zone(record),
                ],
            )

            remote_id = "%s:%s" % (self.source.guid, record["id"])
            file = ExternalContent.objects.filter(remote_id=remote_id).first()
            if not file:
                file = ExternalContent()
                file.source = self.source
                file.owner = get_or_create_default_author()
                file.remote_id = remote_id
                file.created_at = record["date_created"]

            file.title = record["name"] or ""
            file.updated_at = record["date_modified"]
            file.description = record["description"] or ""
            file.category_tags = list(tag_categories)
            file.canonical_url = self._get_canonical_url(record)
            file.save()
            self.imported_files.append(file.guid)
        except ValidationError:
            raise Exception("Record: %s" % json.dumps(record))

    def _load_tags(self, category, record):
        try:
            wrapper = TagCategoryWrapper(record[category]["name"])
            if wrapper.tags:
                self.tag_categories[category].add(*wrapper.tags)
                return wrapper.as_dict(category, sort=True)
        except (AttributeError, KeyError, TypeError):
            pass

    def _load_zone(self, record):
        try:
            study = record["study"]["name"]
            wrapper = TagCategoryWrapper(*self.study_zones[study])
            if wrapper.tags:
                self.tag_categories["zone"].add(*wrapper.tags)
                return wrapper.as_dict("zone", sort=True)
        except (AttributeError, KeyError, TypeError):
            pass

    def _get_canonical_url(self, record):
        pk = str(record["id"])
        prefix = self.source.settings["frontendUrl"]
        return urllib.parse.urljoin(prefix, pk)

    def apply_tags(self):
        for name, tags in self.tag_categories.items():
            tags.save(name, sort=True)

    def cleanup_files(self):
        ExternalContent.objects.exclude(id__in=self.imported_files).delete()
