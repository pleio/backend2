import json
import logging
from collections import defaultdict

import feedparser

from core.utils.tags import TagCategoryWrapper
from entities.external_content.api_handlers import ApiHandlerBase
from entities.external_content.utils import get_or_create_default_author

from ...models import ExternalContent
from .article_similarity import ArticleSimilarityClient
from .entry_wrapper import EntryWrapper

logger = logging.getLogger(__name__)


class ApiHandler(ApiHandlerBase):
    ID = "rss"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.settings = self.source.settings
        self._defaults = {}

    def default_featured_images(self):
        if not self._defaults:
            for tag_image in self.settings.get("tag_images", []):
                key = "%s|%s" % (tag_image.get("category"), tag_image.get("tag"))
                self._defaults[key] = tag_image
        return self._defaults

    def sync_categories(self, category_tags):
        for category in category_tags:
            wrapper = TagCategoryWrapper(*category["values"])
            wrapper.save(category["name"], sort=True)

    def feeds(self):
        base_url = self.settings["feed_url"]
        if not self.settings["feed_filters"]:
            yield base_url, {}
        for feed_filter in self.settings["feed_filters"]:
            yield get_full_rss_path(base_url, feed_filter["filter"]), feed_filter

    def pull(self):
        for feed_url, feed_filter in self.feeds():
            feed = feedparser.parse(feed_url)
            for entry in feed.entries:
                self._create_external_content(
                    entry,
                    feed_filter.get("categories"),
                )

    def _create_external_content(self, entry, categories):
        from entities.external_content.models import ExternalContent

        try:
            wrapper = EntryWrapper(
                entry=entry, source=self.source, default_categories=categories
            )

            assert not ExternalContent.objects.filter(
                source=self.source,
                remote_id=wrapper.remote_id,
            ).exists()

            if has_similar_content := self.lookup_similar_external_content(
                title=wrapper.title,
                published=wrapper.published,
            ):
                self.apply_similar_content_tags(
                    wrapper.category_tags, has_similar_content
                )

            featured_image = (
                wrapper.get_featured_image(
                    wrapper.category_tags, self.default_featured_images()
                )
                or {}
            )

            ExternalContent.objects.create(
                title=wrapper.title,
                description=wrapper.description,
                featured_image_id=featured_image.get("image_guid"),
                featured_position_y=featured_image.get("y_position", 50),
                featured_alt=featured_image.get("alt_text", ""),
                published=None if bool(has_similar_content) else wrapper.published,
                source=self.source,
                owner=get_or_create_default_author(),
                remote_id=wrapper.remote_id,
                canonical_url=wrapper.canonical_url,
                category_tags=wrapper.category_tags,
                init_data={
                    "published_time": wrapper.published.isoformat(),
                    "similar_content": has_similar_content,
                    "default_categories": categories,
                    "entry": json.loads(json.dumps(entry)),
                },
                read_access=self.source.get_read_access(),
            )

            self.sync_categories(wrapper.category_tags)
        except AssertionError:
            pass

    @staticmethod
    def apply_similar_content_tags(target_categories, similar_content):
        for item in similar_content:
            entity = ExternalContent.objects.get(id=item["id"])
            categories = defaultdict(TagCategoryWrapper)
            for category in entity.category_tags:
                categories[category["name"]].add(*category["values"])
            for category in target_categories:
                categories[category["name"]].add(*category["values"])
            entity.category_tags = [
                values.as_dict(name, sort=True) for name, values in categories.items()
            ]
            entity.save()

    def lookup_similar_external_content(self, title, published, exclude_id=None):
        from entities.external_content.models import ExternalContent

        qs = ExternalContent.objects.filter(
            source=self.source,
            published__isnull=False,
            published__date=published.date(),
        )
        if exclude_id:
            qs = qs.exclude(id=exclude_id)

        similarity_client = ArticleSimilarityClient(title)
        result = []
        for item in qs:
            similarity_result = similarity_client.compare(item.title)
            if similarity_result["value"]:
                result.append(
                    {
                        "id": item.guid,
                        "title": item.title,
                        "url": item.canonical_url,
                        "created_at": item.created_at.isoformat(),
                        "published_at": item.published.isoformat(),
                        "category_tags": item.category_tags,
                        "reason": similarity_result["evaluation"],
                    }
                )

        return result


def get_full_rss_path(base_url, filter):
    if filter and filter.startswith("https://"):
        return filter
    if not filter:
        return base_url or ""
    if not base_url:
        return ""
    return base_url + filter
