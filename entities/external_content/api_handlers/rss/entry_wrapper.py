import mimetypes
import time

import requests
from django.core.files.base import ContentFile
from django.utils import timezone
from django.utils.timezone import localtime

from core.utils.tags import TagCategoryWrapper
from entities.external_content.utils import get_or_create_default_author
from entities.file.models import FileFolder


class EntryWrapper:
    def __init__(self, entry, source, default_categories=None):
        self.entry = entry
        self.source = source
        self.default_categories = default_categories
        self._category_tags = None

    @property
    def title(self):
        return self.entry.title

    @property
    def description(self):
        return self.entry.summary

    @property
    def published(self):
        if "published_parsed" in self.entry:
            created_at_time = _struct_to_time(self.entry.published_parsed, localtime())
        elif "updated_parsed" in self.entry:
            created_at_time = _struct_to_time(self.entry.updated_parsed, localtime())
        else:
            return timezone.localtime()
        return timezone.make_aware(created_at_time)

    @property
    def remote_id(self):
        return self.entry.id

    @property
    def canonical_url(self):
        return self.entry.link

    def get_featured_image(self, category_tags, defaults):
        if media := self.create_media_content():
            return {
                "image_guid": media.guid,
            }
        for category in category_tags:
            for tag in category["values"]:
                key = "%s|%s" % (category["name"], tag)
                if key in defaults:
                    return defaults[key]
        return None

    def create_media_content(self):
        try:
            for enclosure in self.entry.enclosures:
                if not enclosure.type.startswith("image"):
                    continue
                result = requests.get(enclosure.href, timeout=10)
                if result.ok:
                    extension = mimetypes.guess_extension(enclosure.type)
                    return FileFolder.objects.create(
                        type=FileFolder.Types.FILE,
                        upload=ContentFile(
                            result.content, name=f"enclosure{extension}"
                        ),
                        read_access=self.source.get_read_access(),
                        owner=get_or_create_default_author(),
                    )
        except Exception:
            pass

    @property
    def category_tags(self):
        if self._category_tags is None:
            target_categories = self._apply_default_category(
                {
                    **self._author_category(),
                    **self._subject_category(),
                }
            )
            self._category_tags = [
                values.as_dict(name) for name, values in target_categories.items()
            ]
        return self._category_tags

    def _author_category(self):
        target_categories = TagCategoryWrapper()
        if author_category := self.source.settings["author_category_name"]:
            if source := getattr(self.entry, "source", None):
                target_categories.add(source.get("title", ""))
            elif authors := getattr(self.entry, "authors", None):
                for author in authors:
                    target_categories.add(author["name"])
            return {author_category: target_categories}
        return {}

    def _subject_category(self):
        target_categories = TagCategoryWrapper
        if subject_category := self.source.settings["subject_category_name"]:
            if tags := getattr(self.entry, "tags", None):
                for tag in tags:
                    target_categories.add(tag["term"])
            return {subject_category: target_categories}
        return {}

    def _apply_default_category(self, target_categories):
        if self.default_categories:
            for category in self.default_categories:
                if category["name"] not in target_categories:
                    target_categories[category["name"]] = TagCategoryWrapper()
                target_categories[category["name"]].add(*category["values"])
        return target_categories


def _struct_to_time(struct_time, default=None):
    if not struct_time:
        return default
    return timezone.datetime.fromtimestamp(time.mktime(struct_time))
