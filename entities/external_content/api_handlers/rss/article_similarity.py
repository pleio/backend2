import re
from difflib import SequenceMatcher


class ArticleSimilarityClient:
    def __init__(self, orginal_text):
        self.original_text = orginal_text

    def compare(self, maybe_similar_text):
        matchers = [
            SimpleTitleMatcher,
            SortedTitleMatcher,
        ]
        for matcher_class in matchers:
            matcher = matcher_class(self.original_text, maybe_similar_text)
            if matcher.isSimilar():
                return {
                    "value": True,
                    "evaluation": matcher.report(),
                }
        return {
            "value": False,
            "evaluation": "No matchers found a match",
        }


class SimpleTitleMatcher:
    def __init__(self, text1, text2):
        self.text1 = text1
        self.text2 = text2
        self.score = None

    def isSimilar(self):
        if self.score is None:
            self.score = SequenceMatcher(None, self.text1, self.text2).ratio()
        return self.score > 0.9

    def report(self):
        return "{me} score: {score}".format(
            me=self.__class__.__name__, score=self.score
        )


class SortedTitleMatcher(SimpleTitleMatcher):
    def __init__(self, text1, text2):
        super().__init__(self._sort(text1), self._sort(text2))

    @staticmethod
    def _sort(text):
        return " ".join(sorted(re.findall(r"\w+", text)))
