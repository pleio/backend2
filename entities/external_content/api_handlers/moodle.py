import datetime
import logging
import time
import urllib.parse

import requests
from django.utils import timezone

from entities.external_content.api_handlers import ApiHandlerBase, ApiHandlerError
from entities.external_content.models import ExternalContent
from entities.external_content.utils import get_or_create_default_author, get_root_url

logger = logging.getLogger(__name__)


class MoodleClient:
    def __init__(self, base_url, token):
        self.base_url = base_url
        self.batch_size = 50
        self.token = token

    def get(self):
        response = requests.get(
            self.base_url,
            {
                "wsfunction": "core_course_get_courses_by_field",
                "moodlewsrestformat": "json",
                "wstoken": self.token,
            },
            timeout=10,
        )
        if not response.ok:
            raise ApiHandlerError("%s: %s" % (response.status_code, response.reason))
        return response.json()


class ApiHandler(ApiHandlerBase):
    ID = "moodle"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = MoodleClient(
            self.source.settings["feed_url"],
            self.source.settings["feed_token"],
        )

    def pull(self):
        for course in self.client.get()["courses"]:
            try:
                external_content = ExternalContent.objects.filter(
                    source=self.source,
                    remote_id=course["id"],
                ).first()

                if external_content:
                    if self.source.settings.get("last_updated_at"):
                        if external_content.updated_at < self._get_datetime_object(
                            self.source.settings.get("last_updated_at")
                        ):
                            continue
                    external_content.title = course["fullname"]
                    external_content.description = course["summary"]
                    external_content.updated_at = self._get_datetime_object(
                        course["timemodified"]
                    )
                    external_content.save()
                else:
                    ExternalContent.objects.create(
                        remote_id=course["id"],
                        title=course["fullname"],
                        description=course["summary"],
                        canonical_url=self._get_canonical_url(course["id"]),
                        source=self.source,
                        owner=get_or_create_default_author(),
                        read_access=self.source.get_read_access(),
                        created_at=self._get_datetime_object(course["timecreated"]),
                        updated_at=self._get_datetime_object(course["timemodified"]),
                    )
            except Exception as e:
                print(e)
        last_updated_object = (
            ExternalContent.objects.all().order_by("-updated_at").first().updated_at
        )
        self.source.settings["last_updated_at"] = int(
            time.mktime(last_updated_object.timetuple())
        )
        self.source.save(update_fields=["settings"])

    def _get_canonical_url(self, id):
        pk = str(id)
        feed_url = self.source.settings["feed_url"]
        path = "/course/view.php?id=" + pk
        return urllib.parse.urljoin(get_root_url(feed_url), path)

    def _get_datetime_object(self, timestamp):
        dt_object = datetime.datetime.fromtimestamp(timestamp)
        return timezone.make_aware(dt_object, timezone.get_current_timezone())
