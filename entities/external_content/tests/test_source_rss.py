from unittest import mock
from unittest.mock import call, patch

from django.core.files.base import ContentFile

from core.tests.helpers import PleioTenantTestCase
from entities.external_content.factories import ExternalContentFactory
from entities.external_content.models import ExternalContentSource
from entities.file.models import FileFolder
from user.factories import UserFactory


class TestSourceRssTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            TAG_CATEGORIES=[
                {
                    "name": "Foo Category",
                    "values": [
                        {"value": "Foo", "synonyms": []},
                        {"value": "Bar", "synonyms": []},
                    ],
                },
            ]
        )

        self.source = ExternalContentSource.objects.create(
            name="News item",
            plural_name="News items",
            handler_id="rss",
            settings={
                "author_category_name": "Foo Category",
                "subject_category_name": "",
                "feed_url": "http://example.com/rss.xml",
                "feed_filters": [
                    {
                        "filter": "?filter=value",
                        "categories": [
                            {"name": "Baz Category", "values": ["Qux"]},
                            {"name": "Bar Category", "values": ["Bar", "Baz", "Duq"]},
                        ],
                    }
                ],
            },
        )
        self.mocked_feeds = mock.patch(
            "entities.external_content.api_handlers.rss.ApiHandler.feeds"
        ).start()
        self.mocked_feeds.return_value = [
            (
                self.relative_path(__file__, ["assets", "rss", "rss.xml"]),
                self.source.settings["feed_filters"][0],
            )
        ]

    @patch("core.utils.tags.TagCategoryStorage.store_tag_category_values")
    def test_import(self, store_tag_category_values):
        # When
        self.source.pull()

        # Then
        self.assertTrue(self.mocked_feeds.called)
        self.assertEqual(self.source.content.count(), 3)

        self.maxDiff = None

        self.assertIn(
            call("Foo Category", ["Bar", "Foo", "Kuk"]),
            store_tag_category_values.mock_calls,
        )
        self.assertIn(
            call("Baz Category", ["Qux"]), store_tag_category_values.mock_calls
        )
        self.assertIn(
            call("Bar Category", ["Bar", "Baz", "Duq"]),
            store_tag_category_values.mock_calls,
        )

    def test_second_entry_extends_categories_of_existing_entry(self):
        existing = ExternalContentFactory(
            source=self.source,
            title="Dit artikel bestaat al in het systeem",
            published="2024-02-21T15:46:00+01:00",
            category_tags=[
                {"name": "Foo Category", "values": ["Foo first"]},
                {"name": "Baz Category", "values": ["Baz first"]},
                {"name": "Bar Category", "values": ["Bar", "Baz", "Duq"]},
            ],
        )
        self.source.pull()

        existing.refresh_from_db()
        self.assertEqual(
            existing.category_tags,
            [
                {"name": "Foo Category", "values": ["Foo", "Foo first"]},
                {"name": "Baz Category", "values": ["Baz first", "Qux"]},
                {"name": "Bar Category", "values": ["Bar", "Baz", "Duq"]},
            ],
        )


class RssSourceWithCategoryTagImages(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.file = FileFolder.objects.create(
            type=FileFolder.Types.FILE,
            upload=ContentFile(b"", "empty-file.txt"),
            owner=self.owner,
        )

    def test_source_creates_references_at_file(self):
        self.assertEqual(self.file.referenced_by.count(), 0)
        source = ExternalContentSource(
            name="Source",
            plural_name="Sources",
            handler_id="rss",
            settings={
                "tag_images": [
                    {
                        "category": "Foo Category",
                        "tag": "Bar",
                        "image_guid": self.file.guid,
                    }
                ],
            },
        )

        source.save()

        self.assertEqual(self.file.referenced_by.count(), 1)
        self.assertEqual(self.file.referenced_by.first().container, source)

    def test_entry_creates_references_at_file(self):
        source = ExternalContentSource.objects.create(
            name="Source",
            plural_name="Entries",
            handler_id="rss",
            settings={
                "tag_images": [
                    {
                        "category": "Foo Category",
                        "tag": "Bar",
                        "image_guid": self.file.guid,
                    }
                ],
            },
        )
        self.assertEqual(self.file.referenced_by.count(), 1)

        entry = source.content.create(
            featured_image_id=self.file.id,
            title="Entry",
            owner=self.owner,
            canonical_url="http://example.com",
            remote_id="demo",
        )

        self.assertIn(source, [r.container for r in self.file.referenced_by.all()])
        self.assertIn(entry, [r.container for r in self.file.referenced_by.all()])
        self.assertEqual(self.file.referenced_by.count(), 2)
