from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory
from entities.external_content.factories import (
    ExternalContentFactory,
    ExternalContentSourceFactory,
)


class IncludeExternalContentSourcesMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.included_source = None
        self.excluded_source = None

    def _get_included_source(self):
        if not self.included_source:
            self.included_source = ExternalContentSourceFactory()
        return self.included_source

    def _get_excluded_source(self):
        if not self.excluded_source:
            self.excluded_source = ExternalContentSourceFactory()
        return self.excluded_source


class TestSearchWithExcludedContentTypesTestCase(
    IncludeExternalContentSourcesMixin,
    Template.TestSearchWithExcludedContentTypesTestCase,
):
    def get_exclude_types(self):
        return [self._get_excluded_source().guid]

    def build_included_article(self, title):
        return ExternalContentFactory(
            title=title, owner=self.owner, source=self._get_included_source()
        )

    def build_excluded_article(self, title):
        return ExternalContentFactory(
            title=title, owner=self.owner, source=self._get_excluded_source()
        )


class TestSearchWithOtherExcludedContentTypesTestCase(
    IncludeExternalContentSourcesMixin,
    Template.TestSearchWithExcludedContentTypesTestCase,
):
    def get_exclude_types(self):
        return [self._get_excluded_source().guid]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return ExternalContentFactory(
            title=title, owner=self.owner, source=self._get_excluded_source()
        )
