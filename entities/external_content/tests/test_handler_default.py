from core.tests.helpers import PleioTenantTestCase
from entities.external_content.api_handlers.default import ApiHandler
from entities.external_content.factories import ExternalContentSourceFactory
from entities.external_content.models import ExternalContent


class TestHandlerDefaultTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.source = ExternalContentSourceFactory()
        self.handler = ApiHandler(self.source)

    def tearDown(self):
        super().tearDown()

    def test_pull(self):
        self.handler.pull()

        self.assertTrue(ExternalContent.objects.exists())
