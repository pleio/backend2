from core.tests.queries.entities.test_order_by import OrderByBaseTestCases
from entities.external_content.factories import (
    ExternalContentFactory,
    ExternalContentSourceFactory,
)


class BuildEntityMixin:
    def __init__(self, *args, **kwargs):
        self._source = None
        super().__init__(*args, **kwargs)

    @property
    def source(self):
        if not self._source:
            self._source = ExternalContentSourceFactory(name="external_content")
        return self._source

    def build_entity(self, **kwargs):
        return ExternalContentFactory(source=self.source, **kwargs)


class TestOrderByTimeCreated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeCreated,
):
    pass


class TestOrderByTimeUpdated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeUpdated,
):
    pass


class TestOrderByTimePublished(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimePublished,
):
    pass


class TestOrderByLastAction(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastAction,
):
    pass


class TestOrderByLastSeen(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastSeen,
):
    pass


class TestOrderByStartDateNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByStartDateNotAvailable,
):
    pass


class TestOrderByFileSizeNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByFileSizeNotAvailable,
):
    pass


class TestOrderByTitle(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTitle,
):
    pass


class TestOrderByGroupName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByGroupName,
):
    pass


class TestOrderByOwnerName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByOwnerName,
):
    pass
