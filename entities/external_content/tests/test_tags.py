from core.tests.helpers.tags_testcase import Template
from entities.external_content.factories import ExternalContentSourceFactory
from entities.external_content.models import ExternalContent
from user.models import User


class TestExternalContentTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "ExternalContent"
    model = ExternalContent

    def __init__(self, *args, **kwargs):
        self._source = None
        super().__init__(*args, **kwargs)

    @property
    def source(self):
        if not self._source:
            self._source = ExternalContentSourceFactory(name="Demo")
        return self._source

    def article_factory(self, owner: User, **kwargs):
        kwargs.setdefault("source", self.source)
        return super().article_factory(owner, **kwargs)

    include_add_edit = False
    include_site_search = True
    include_entity_search = True
    include_activity_search = False
