from post_deploy import post_deploy_action


@post_deploy_action
def remove_external_content_index():
    try:
        from elasticsearch import Elasticsearch

        es = Elasticsearch()
        es.indices.delete(index="external_content", ignore=[400, 404])
    except Exception:
        pass
