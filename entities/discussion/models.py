from auditlog.registry import auditlog
from django.db import models

from core.models import (
    ArticleMixin,
    AttachmentMixin,
    BookmarkMixin,
    CommentMixin,
    Entity,
    FollowMixin,
    MentionMixin,
    VoteMixin,
)
from core.models.featured import FeaturedCoverMixin
from core.models.mixin import RichDescriptionMediaMixin, TitleMixin


class Discussion(
    RichDescriptionMediaMixin,
    TitleMixin,
    VoteMixin,
    BookmarkMixin,
    FollowMixin,
    CommentMixin,
    MentionMixin,
    FeaturedCoverMixin,
    ArticleMixin,
    AttachmentMixin,
    Entity,
):
    class Meta:
        ordering = ["-published"]

    title = models.CharField(max_length=256)

    def __str__(self):
        return f"Discussion[{self.title}]"

    @property
    def type_to_string(self):
        return "discussion"

    @property
    def url(self):
        return "{}/discussion/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)

    def serialize(self):
        return {
            "title": self.title,
            "richDescription": self.rich_description,
            **super().serialize(),
        }


auditlog.register(Discussion)
