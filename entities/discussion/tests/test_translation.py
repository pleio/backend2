from core.tests.helpers.test_translation import Wrapper
from entities.discussion.factories import DiscussionFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Discussion"

    def build_entity(self, **kwargs):
        return DiscussionFactory(**kwargs)
