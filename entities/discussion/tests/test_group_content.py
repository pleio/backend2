from core.tests.helpers.test_group_content import Template
from entities.discussion.factories import DiscussionFactory
from user.factories import UserFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return DiscussionFactory(**kwargs)

    def build_owner(self):
        return UserFactory()
