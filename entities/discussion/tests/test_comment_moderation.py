from core.tests.helpers.test_comment_moderation import CommentModerationTestCases
from entities.discussion.factories import DiscussionFactory


class TestAddCommentForModerationTestCase(
    CommentModerationTestCases.TestAddCommentForModerationTestCase
):
    type_to_string = "discussion"

    def entity_factory(self, **kwargs):
        return DiscussionFactory(**kwargs)
