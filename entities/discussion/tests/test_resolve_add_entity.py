from unittest.mock import patch

from django.contrib.auth.models import AnonymousUser
from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import USER_ROLES
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from user.models import User


@tag("createEntity")
class AddDiscussionTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.anonymousUser = AnonymousUser()
        self.authenticated_user = mixer.blend(User, roles=[USER_ROLES.NEWS_EDITOR])
        self.editorUser = mixer.blend(User, roles=[USER_ROLES.EDITOR])
        self.suggested_item = BlogFactory(owner=self.authenticated_user)
        self.group = mixer.blend(
            Group, owner=self.authenticated_user, is_membership_on_request=False
        )
        self.group.join(self.authenticated_user, "owner")

        self.data = {
            "input": {
                "subtype": "discussion",
                "title": "My first discussion",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "featured": {
                    "positionY": 10,
                    "video": "testVideo",
                    "videoTitle": "testTitle",
                    "alt": "testAlt",
                    "caption": "(c) Pleio, 1996",
                },
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.suggested_item.guid],
            }
        }
        self.mutation = """
            fragment DiscussionParts on Discussion {
                showOwner
                inputLanguage
                isTranslationEnabled
                title
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                isRecommendedInSearch
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    alt
                    caption
                }
                canEdit
                canArchiveAndDelete
                tags
                url
                inGroup
                group {
                    guid
                }
                isFeatured
                suggestedItems {
                    guid
                }
                showOwner
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...DiscussionParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_add_discussion(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["showOwner"], True)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["isFeatured"], False)  # only editor or admin can set
        self.assertEqual(entity["featured"]["positionY"], 10)
        self.assertEqual(entity["featured"]["video"], "testVideo")
        self.assertEqual(entity["featured"]["videoTitle"], "testTitle")
        self.assertEqual(entity["featured"]["alt"], "testAlt")
        self.assertEqual(entity["featured"]["caption"], "(c) Pleio, 1996")
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"], [{"guid": self.suggested_item.guid}])
        self.assertEqual(entity["showOwner"], True)

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_add_discussion_editor(self):
        variables = self.data

        self.graphql_client.force_login(self.editorUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)

    def test_add_discussion_to_group(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["inGroup"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)

    def test_add_minimal_entity(self):
        variables = {
            "input": {
                "title": "Simple discussion",
                "subtype": "discussion",
            }
        }

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertTrue(entity["canEdit"])
