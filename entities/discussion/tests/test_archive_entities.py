from core.tests.helpers.test_archive_entities import Wrapper
from entities.discussion.factories import DiscussionFactory as EntityFactory


class TestArchiveEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return EntityFactory(**kwargs)
