from core.tests.helpers.tags_testcase import Template
from entities.discussion.models import Discussion


class TestDiscussionTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "Discussion"
    model = Discussion
    subtype = "discussion"

    def variables_add(self):
        return {
            "input": {
                "title": "Test discussion",
                "subtype": "discussion",
            }
        }

    include_site_search = True
    include_entity_search = True
    include_activity_search = True
