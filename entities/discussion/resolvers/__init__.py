from ariadne import InterfaceType, ObjectType

from .entity import discussion

resolvers = [discussion]
