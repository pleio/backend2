from django.apps import AppConfig


class DiscussionConfig(AppConfig):
    name = "entities.discussion"
    label = "discussion"
