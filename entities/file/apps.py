from django.apps import AppConfig


class FileConfig(AppConfig):
    name = "entities.file"
    label = "file"
