from unittest import mock

from django.core.files.base import ContentFile

from core.constances import ACCESS_TYPE
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory, PadFactory
from entities.file.models import FileFolder
from user.factories import UserFactory
from user.models import User


class Base:
    class TestFileModelTestCaseBase(PleioTenantTestCase):
        owner: User = None

        TITLE = "Demo blog"
        CONTENT = "Demo content"

        def setUp(self):
            super().setUp()

            self.owner = UserFactory()
            self.folder = FolderFactory(owner=self.owner)
            self.entity = self.file_entity_factory(
                owner=self.owner,
                parent=self.folder,
                title=self.TITLE,
                rich_description=self.CONTENT,
            )

        def tearDown(self):
            super().tearDown()

        def file_entity_factory(self, **kwargs) -> FileFolder:  # pragma: no cover
            raise NotImplementedError()

        @mock.patch("core.models.Entity.serialize")
        def test_serialize(self, serialize):
            serialize.return_value = {}

            self.assertEqual(
                self.entity.serialize(),
                {
                    "file": None,
                    "mimeType": None,
                    "parentGuid": self.folder.guid,
                    "richDescription": self.CONTENT,
                    "size": 0,
                    "title": self.TITLE,
                },
            )

        def test_map_rich_text_fields(self):
            before = self.entity.serialize()
            expected = self.entity.serialize()
            expected["richDescription"] = f"new {self.CONTENT}"

            self.entity.map_rich_text_fields(lambda v: "new %s" % v)
            after = self.entity.serialize()

            self.assertNotEqual(after, before)
            self.assertEqual(after, expected)


class TestDiskFileModelTestCase(Base.TestFileModelTestCaseBase):
    def file_entity_factory(self, **kwargs):
        kwargs["upload"] = ContentFile(b"Abcdefghijklmnopqrstuvwxyz", "demo.txt")
        return FileFactory(**kwargs)

    @mock.patch("core.models.Entity.serialize")
    def test_serialize(self, serialize):
        serialize.return_value = {}

        self.assertEqual(
            self.entity.serialize(),
            {
                "file": self.entity.upload.name,
                "mimeType": "text/plain",
                "parentGuid": self.folder.guid,
                "richDescription": self.CONTENT,
                "size": 26,
                "title": self.TITLE,
            },
        )

    def test_is_file(self):
        self.assertTrue(self.entity.is_file())

    def test_standard_filename(self):
        file: FileFolder = self.file_factory(
            self.relative_path(__file__, ["assets", "grass.jpg"]), title="grass.jpg"
        )
        self.assertEqual(file.clean_filename(), "grass.jpg")

        file.title = "Something else.jpg"
        self.assertEqual(file.clean_filename(), "something-else.jpg")

        file.title = "Something else.JPG"
        self.assertEqual(file.clean_filename(), "something-else.jpg")

        file.title = "Something else"
        self.assertEqual(file.clean_filename(), "something-else.jpg")

        file.title = "Another ext.jpeg"
        self.assertEqual(file.clean_filename(), "another-ext.jpg")

    def test_original_filename(self):
        file: FileFolder = self.file_factory(
            self.relative_path(__file__, ["assets", "grass.jpg"]), title="grass.jpg"
        )

        file.title = "Something else.jpg"
        self.assertEqual(file.clean_filename(), "something-else.jpg")
        self.assertEqual(file.original_filename(), "grass.jpg")


class TestFolderFileModelTestCase(Base.TestFileModelTestCaseBase):
    def file_entity_factory(self, **kwargs):
        return FolderFactory(**kwargs)

    def test_is_not_a_file(self):
        self.assertFalse(self.entity.is_file())


class TestPadFileModelTestCase(Base.TestFileModelTestCaseBase):
    def file_entity_factory(self, **kwargs):
        return PadFactory(**kwargs)

    def test_is_not_a_file(self):
        self.assertFalse(self.entity.is_file())


class TestAttachmentFilePropertiesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory())
        self.file = self.remember(FileFactory(owner=self.owner))

        # Causes the refresh_read_access to be called
        self.remember(
            BlogFactory(
                owner=self.owner,
                read_access=[ACCESS_TYPE.user.format(self.owner.guid)],
                rich_description=self.tiptap_attachment(self.file),
            )
        )

    def test_closed_access_hosts(self):
        other_owner = UserFactory()
        self.remember(
            BlogFactory(
                owner=other_owner,
                read_access=[ACCESS_TYPE.user.format(other_owner.guid)],
                rich_description=self.tiptap_attachment(self.file),
            )
        )
        self.file.refresh_from_db()

        self.assertEqual(
            {*self.file.read_access},
            {
                ACCESS_TYPE.user.format(self.owner.guid),
                ACCESS_TYPE.user.format(other_owner.guid),
            },
        )

    def test_one_open_access_host(self):
        other_owner = UserFactory()
        self.remember(
            BlogFactory(
                owner=other_owner,
                read_access=[
                    ACCESS_TYPE.public,
                    ACCESS_TYPE.user.format(other_owner.guid),
                ],
                rich_description=self.tiptap_attachment(self.file),
            )
        )
        self.file.refresh_from_db()

        self.assertEqual(self.file.read_access, [ACCESS_TYPE.public])
