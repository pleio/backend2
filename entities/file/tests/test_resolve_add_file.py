import json
from unittest.mock import patch

from django.core.files.base import ContentFile
from django.test import tag
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, USER_NOT_MEMBER_OF_GROUP
from core.models import Group
from core.resolvers import shared
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FolderFactory
from entities.file.models import FileFolder
from user.models import User


@tag("createEntity")
class AddFileTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            EXTRA_LANGUAGES=["en", "nl", "de"],
            LANGUAGE="de",
        )

        self.authenticatedUser = mixer.blend(User)
        self.authenticatedUser2 = mixer.blend(User)
        self.authenticatedUser3 = mixer.blend(User)
        self.group = mixer.blend(
            Group, owner=self.authenticatedUser, is_membership_on_request=False
        )
        self.group.join(self.authenticatedUser, "owner")
        self.group.join(self.authenticatedUser3, "member")

        self.folder = FileFolder.objects.create(
            title="images",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            type=FileFolder.Types.FILE,
            group=self.group,
        )

        self.RICH_DESCRIPTION = json.dumps(
            {
                "type": "doc",
                "content": [
                    {
                        "type": "paragraph",
                        "content": [
                            {
                                "type": "text",
                                "text": "expected text",
                            }
                        ],
                    }
                ],
            }
        )
        self.DESCRIPTION = "expected text\n\n"

        self.file_mock = ContentFile(b"test", "test.gif")

        self.data = {
            "input": {
                "richDescription": self.RICH_DESCRIPTION,
                "containerGuid": self.group.guid,
                "file": self.file_mock,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            }
        }
        self.mutation = """
            mutation ($input: addFileInput!) {
                addFile(input: $input) {
                    entity {
                        ... on FileFolder {
                            inputLanguage
                            isTranslationEnabled
                            showOwner
                            title
                            description
                            richDescription
                            canEdit
                            canArchiveAndDelete
                            accessId
                            writeAccessId
                            group { guid }
                            rootContainer { guid }
                            container { guid }
                            isPersonalFile
                        }
                        ... on File {
                            inputLanguage
                            isTranslationEnabled
                            mimeType
                        }
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    @patch("entities.file.models.FileFolder.scan")
    @patch("core.resolvers.shared.strip_exif")
    @patch("entities.file.models.FileFolder.persist_file")
    def test_add_file(self, persist_file, mocked_strip_exif, mocked_scan):
        mocked_scan.return_value = True

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, self.data)
        entity = result["data"]["addFile"]["entity"]

        self.assertEqual(
            entity,
            {
                "title": "test.gif",
                "showOwner": True,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "mimeType": "image/gif",
                "description": self.DESCRIPTION,
                "richDescription": self.RICH_DESCRIPTION,
                "canEdit": True,
                "canArchiveAndDelete": True,
                "accessId": 1,
                "writeAccessId": 0,
                "isPersonalFile": False,
                "group": {"guid": self.group.guid},
                "rootContainer": {"guid": self.group.guid},
                "container": {"guid": self.group.guid},
            },
        )
        self.assertTrue(mocked_strip_exif.called)
        self.assertTrue(mocked_scan.called)
        self.assertFalse(persist_file.called)

    @patch("entities.file.models.FileFolder.scan")
    @patch("entities.file.models.FileFolder.persist_file")
    def test_add_personal_file(self, persist_file, mocked_scan):
        mocked_scan.return_value = True

        self.data["input"]["containerGuid"] = None
        self.graphql_client.force_login(self.authenticatedUser)
        self.graphql_client.post(self.mutation, self.data)

        self.assertTrue(persist_file.called)

    def test_add_file_not_writable_group(self):
        self.graphql_client.force_login(self.authenticatedUser2)
        with self.assertGraphQlError(USER_NOT_MEMBER_OF_GROUP):
            self.graphql_client.post(self.mutation, self.data)

    def test_add_file_not_writable_folder(self):
        variables = {
            "input": {
                "containerGuid": self.folder.guid,
                "file": self.file_mock,
                "tags": ["tag_one", "tag_two"],
            }
        }

        self.graphql_client.force_login(self.authenticatedUser3)
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.post(self.mutation, variables)

    def test_add_folder_not_writable_group(self):
        mutation = """
            mutation addFolder($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                        guid
                        __typename
                    }
                __typename
                }
            }
        """
        variables = {
            "input": {
                "containerGuid": self.group.guid,
                "subtype": "folder",
                "title": "testfolder",
                # "accessId": 1,
                # "writeAccessId": 0,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser2)
        with self.assertGraphQlError(USER_NOT_MEMBER_OF_GROUP):
            self.graphql_client.post(mutation, variables)

    def test_add_folder_not_writable_folder(self):
        mutation = """
            mutation addFolder($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                        guid
                        __typename
                    }
                __typename
                }
            }
        """
        variables = {
            "input": {
                "containerGuid": self.folder.guid,
                "subtype": "folder",
                "title": "testfolder",
            }
        }

        self.graphql_client.force_login(self.authenticatedUser3)
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.post(mutation, variables)

    @patch("core.resolvers.shared.copy_attachment_references")
    def test_add_folder(self, copy_attachment_references):
        mutation = """
            mutation addFolder($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                        guid
                        ...on FileFolder {
                            title
                            isTranslationEnabled
                            isPersonalFile
                            rootContainer { guid }
                        }
                    }
                }
            }
        """
        variables = {
            "input": {
                "containerGuid": self.group.guid,
                "subtype": "folder",
                "title": "testfolder",
                "isTranslationEnabled": False,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser3)
        result = self.graphql_client.post(mutation, variables)
        self.assertEqual(result["data"]["addEntity"]["entity"]["title"], "testfolder")
        self.assertTrue(copy_attachment_references.called)

    @patch("entities.file.models.FileFolder.scan")
    def test_add_personal_file_2(self, scan):
        scan.return_value = True

        self.data["input"]["containerGuid"] = self.authenticatedUser.guid
        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, self.data)
        entity = result["data"]["addFile"]["entity"]

        self.maxDiff = None

        self.assertEqual(
            entity,
            {
                "accessId": 1,
                "canEdit": True,
                "canArchiveAndDelete": True,
                "container": {"guid": self.authenticatedUser.guid},
                "description": self.DESCRIPTION,
                "group": None,
                "isPersonalFile": True,
                "mimeType": "image/gif",
                "richDescription": self.RICH_DESCRIPTION,
                "rootContainer": {"guid": self.authenticatedUser.guid},
                "showOwner": True,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "test.gif",
                "writeAccessId": 0,
            },
        )
        self.assertTrue(scan.called)

    def test_add_file_to_folder(self):
        folder = FolderFactory(owner=self.authenticatedUser, group=self.group)
        variables = {
            "input": {
                "file": ContentFile(b"test", "image.jpg"),
                "containerGuid": folder.guid,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            }
        }
        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addFile"]["entity"]

        self.assertEqual(
            entity,
            {
                "accessId": 1,
                "canEdit": True,
                "canArchiveAndDelete": True,
                "container": {"guid": folder.guid},
                "description": None,
                "group": {"guid": self.group.guid},
                "isPersonalFile": False,
                "mimeType": "image/jpeg",
                "richDescription": "",
                "rootContainer": {"guid": self.group.guid},
                "showOwner": True,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "image.jpg",
                "writeAccessId": 0,
            },
        )

    @patch(
        "core.resolvers.shared.copy_attachment_references",
        wraps=shared.copy_attachment_references,
    )
    @patch("entities.file.models.FileFolder.scan")
    def test_add_minimal_entity(self, scan, copy_attachment_references):
        variables = {
            "input": {
                "file": ContentFile(b"test", "image.jpg"),
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addFile"]["entity"]

        self.assertEqual(
            entity,
            {
                "accessId": 1,
                "canEdit": True,
                "canArchiveAndDelete": True,
                "container": {"guid": self.authenticatedUser.guid},
                "description": None,
                "group": None,
                "isPersonalFile": True,
                "mimeType": "image/jpeg",
                "richDescription": "",
                "rootContainer": {"guid": self.authenticatedUser.guid},
                "showOwner": True,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "image.jpg",
                "writeAccessId": 0,
            },
        )
        self.assertTrue(scan.called)
        self.assertTrue(copy_attachment_references.called)

        self.assertTrue(self.mocked_create_publish_request.called)
        self.assertTrue(self.mocked_revert_published.called)
