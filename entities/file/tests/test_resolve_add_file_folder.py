from django.test import tag
from mixer.backend.django import mixer

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.models import User


@tag("createEntity")
class AddFileTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = mixer.blend(User)
        self.mutation = """
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                        ... on FileFolder {
                            showOwner
                            title
                            canEdit
                            canArchiveAndDelete
                            group { guid }
                            rootContainer { guid }
                            isPersonalFile
                            isTranslationEnabled
                        }
                        ... on File {
                            mimeType
                        }
                    }
                }
            }
        """

    def test_add_minimal_folder(self):
        variables = {
            "input": {
                "title": "Simple folder",
                "subtype": "folder",
                "isTranslationEnabled": False,
            }
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(
            entity,
            {
                "canEdit": True,
                "canArchiveAndDelete": True,
                "group": None,
                "isPersonalFile": True,
                "rootContainer": {"guid": self.owner.guid},
                "showOwner": True,
                "title": "Simple folder",
                "isTranslationEnabled": False,
            },
        )

    def test_add_minimal_group_folder(self):
        group = GroupFactory(owner=self.owner)
        variables = {
            "input": {
                "title": "Simple folder",
                "subtype": "folder",
                "containerGuid": group.guid,
                "isTranslationEnabled": False,
            }
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(
            entity,
            {
                "title": "Simple folder",
                "showOwner": True,
                "canEdit": True,
                "canArchiveAndDelete": True,
                "rootContainer": {"guid": group.guid},
                "group": {"guid": group.guid},
                "isPersonalFile": False,
                "isTranslationEnabled": False,
            },
        )
