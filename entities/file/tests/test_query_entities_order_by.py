from core.factories import GroupFactory
from core.tests.queries.entities.test_order_by import OrderByBaseTestCases
from entities.file.factories import FileFactory
from user.factories import UserFactory


class BuildEntityMixin:
    def __init__(self, *args, **kwargs):
        self._group = None
        super().__init__(*args, **kwargs)

    @property
    def group(self):
        if not self._group:
            self._group = GroupFactory(name="repository", owner=UserFactory())
        return self._group

    def build_entity(self, **kwargs):
        if "group" not in kwargs:
            kwargs["group"] = self.group
        if "owner" in kwargs:
            self.group.join(kwargs["owner"])
        return FileFactory(**kwargs)


class TestOrderByTimeCreated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeCreated,
):
    pass


class TestOrderByTimeUpdated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeUpdated,
):
    pass


class TestOrderByTimePublished(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimePublished,
):
    pass


class TestOrderByLastAction(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastAction,
):
    pass


class TestOrderByLastSeen(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastSeen,
):
    pass


class TestOrderByStartDateNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByStartDateNotAvailable,
):
    pass


class TestOrderByFileSize(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByFileSize,
):
    def variables(self):
        return {
            **super().variables(),
            "subTypes": ["file"],
        }


class TestOrderByFileSizeNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByFileSizeNotAvailable,
):
    """When no subtype is provided, the order by file size is not available."""

    pass


class TestOrderByTitle(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTitle,
):
    pass


class TestOrderByGroupName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByGroupName,
):
    pass


class TestOrderByOwnerName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByOwnerName,
):
    pass


class TestOrderByReadAccess(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByReadAccess,
):
    pass
