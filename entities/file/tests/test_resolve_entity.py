import os
from unittest import mock

from django.core.files.base import ContentFile
from mixer.backend.django import mixer

from core import constances
from core.constances import ACCESS_TYPE, ConfigurationChoices
from core.factories import GroupFactory
from core.models import Comment
from core.tests.helpers import PleioTenantTestCase
from entities.activity.models import StatusUpdate
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory, PadFactory
from entities.file.models import FileFolder, FileReference
from user.factories import UserFactory
from user.models import User


class FileFolderTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = mixer.blend(User)

        self.folder = FileFolder.objects.create(
            title="images",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            type=FileFolder.Types.FOLDER,
            rich_description="rich description",
        )

        self.query = """
            query GetFile($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ... on File {
                        isTranslationEnabled
                        title
                        subtype
                        timeCreated
                        timeUpdated
                        timePublished
                        scheduleArchiveEntity
                        scheduleDeleteEntity
                        accessId
                        writeAccessId
                        canEdit
                        canArchiveAndDelete
                        tags
                        url
                        inGroup
                        parentFolder {
                            guid
                        }
                        group {
                            guid
                        }
                        hasChildren
                        mimeType
                        thumbnail
                        download
                        lastDownload
                        referenceCount
                        originalFileName
                        richDescription
                        excerpt
                    }
                    ... on Folder {
                        isTranslationEnabled
                        title
                        subtype
                        timeCreated
                        timeUpdated
                        timePublished
                        scheduleArchiveEntity
                        scheduleDeleteEntity
                        accessId
                        writeAccessId
                        canEdit
                        canArchiveAndDelete
                        tags
                        url
                        inGroup
                        parentFolder {
                            guid
                        }
                        group {
                            guid
                        }
                        hasChildren
                        lastDownload
                        richDescription
                        excerpt
                    }
                }
            }
        """

    def test_file(self):
        file_mock = ContentFile(b"test", name="test.gif")

        self.file = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
        )
        self.file.title = "other_name.gif"
        self.file.rich_description = "rich description"
        self.file.save()

        variables = {"guid": self.file.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], self.file.guid)
        self.assertEqual(entity["isTranslationEnabled"], True)
        self.assertEqual(entity["title"], "other_name.gif")
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(entity["timeCreated"], self.file.created_at.isoformat())
        self.assertEqual(entity["tags"], self.file.tags)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(
            entity["url"],
            "/files/view/{}/{}".format(
                self.file.guid, os.path.basename(self.file.upload.name)
            ),
        )
        self.assertEqual(entity["parentFolder"], None)
        self.assertEqual(entity["subtype"], "file")
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["mimeType"], "image/gif")
        self.assertEqual(entity["thumbnail"], self.file.thumbnail_url)
        self.assertEqual(entity["download"], self.file.download_url)
        self.assertIsNotNone(entity["timePublished"])
        self.assertIsNone(entity["scheduleArchiveEntity"])
        self.assertIsNone(entity["scheduleDeleteEntity"])
        self.assertIsNone(entity["lastDownload"])
        self.assertEqual(entity["referenceCount"], 0)
        self.assertEqual(entity["originalFileName"], "test.gif")
        self.assertEqual(entity["richDescription"], "rich description")
        self.assertEqual(entity["excerpt"], "rich description")

    def test_folder(self):
        variables = {"guid": self.folder.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.folder.guid)
        self.assertEqual(entity["isTranslationEnabled"], True)
        self.assertEqual(entity["title"], self.folder.title)
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(entity["timeCreated"], self.folder.created_at.isoformat())
        self.assertEqual(entity["tags"], self.folder.tags)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(
            entity["url"],
            "/files/{}".format(self.folder.guid),
        )
        self.assertEqual(entity["parentFolder"], None)
        self.assertEqual(entity["subtype"], "folder")
        self.assertIsNone(entity["lastDownload"])
        self.assertEqual(entity["richDescription"], "rich description")
        self.assertEqual(entity["excerpt"], "rich description")

    def test_file_in_folder(self):
        file_mock = ContentFile(b"test", name="test.pdf")

        self.file_in_folder = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
            parent=self.folder,
        )
        variables = {"guid": self.file_in_folder.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], self.file_in_folder.guid)
        self.assertEqual(entity["title"], file_mock.name)
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(
            entity["timeCreated"], self.file_in_folder.created_at.isoformat()
        )
        self.assertEqual(entity["tags"], self.file_in_folder.tags)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(
            entity["url"],
            "/files/view/{}/{}".format(
                self.file_in_folder.guid,
                os.path.basename(self.file_in_folder.upload.name),
            ),
        )
        self.assertEqual(entity["parentFolder"]["guid"], self.folder.guid)
        self.assertEqual(entity["subtype"], "file")
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["mimeType"], "application/pdf")
        self.assertEqual(entity["referenceCount"], 0)

    def test_file_access(self):
        file_mock = ContentFile(b"test", name="test.gif")

        self.file = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.logged_in],
            write_access=[ACCESS_TYPE.logged_in],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
        )

        variables = {"guid": self.file.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], self.file.guid)
        self.assertEqual(entity["title"], file_mock.name)
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["writeAccessId"], 1)

    def test_personal_files_folder(self):
        # Given.
        self.folder.persist_file()

        # When.
        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, {"guid": self.folder.guid})
        entity = result["data"]["entity"]

        # Then.
        self.assertEqual(
            entity["url"],
            "/user/{}/files/{}".format(self.folder.owner.guid, self.folder.guid),
        )

    def test_folder_access(self):
        folder = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.logged_in],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            type=FileFolder.Types.FOLDER,
        )

        variables = {"guid": folder.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)
        entity = result["data"]["entity"]

        self.assertEqual(entity["guid"], folder.guid)
        self.assertEqual(entity["title"], folder.title)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["writeAccessId"], 1)


class FileFolderReferencesTemplate:
    class BaseTestCase(PleioTenantTestCase):
        def factory(self, **kwargs):
            # pragma: no cover
            raise NotImplementedError()

        def setUp(self):
            super().setUp()
            self.owner = self.remember(UserFactory())
            self.file = self.remember(self.factory(owner=self.owner))
            self.query = """
                query GetFile($guid: String!) {
                    entity(guid: $guid) {
                        guid
                        status
                        ... on FileFolder {
                            references { referenceType, entityType, guid, label, url }
                            referenceCount
                        }
                        ... on File {
                            isPersonalFile
                            isFeaturedImage
                        }
                    }
                }
            """
            self.variables = {
                "guid": self.file.guid,
            }

        def test_entity_reference(self):
            blog = self.remember(BlogFactory(owner=self.owner))
            FileReference.objects.create(container=blog, file=self.file)

            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                result["data"]["entity"]["references"],
                [
                    {
                        "referenceType": "entity",
                        "entityType": "blog",
                        "label": blog.title,
                        "guid": blog.guid,
                        "url": blog.url,
                    }
                ],
            )
            self.assertEqual(result["data"]["entity"]["referenceCount"], 1)

        @mock.patch("core.models.comment.truncate_rich_description")
        def test_comment_reference(self, truncate_rich_description):
            truncate_rich_description.return_value = ""
            blog = self.remember(BlogFactory(owner=self.owner))
            comment = self.remember(
                Comment.objects.create(
                    owner=self.owner,
                    container=blog,
                    rich_description=self.tiptap_attachment(self.file),
                )
            )

            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                result["data"]["entity"]["references"],
                [
                    {
                        "referenceType": "entity",
                        "entityType": "comment",
                        "label": "",
                        "guid": comment.guid,
                        "url": comment.url,
                    }
                ],
            )
            self.assertEqual(result["data"]["entity"]["referenceCount"], 1)
            self.assertTrue(truncate_rich_description.called)

        @mock.patch("entities.activity.models.truncate_rich_description")
        def test_status_update_reference(self, truncate_rich_description):
            truncate_rich_description.return_value = ""
            group = self.remember(GroupFactory(owner=self.owner))
            statusupdate = self.remember(
                StatusUpdate.objects.create(
                    owner=self.owner,
                    group=group,
                    rich_description=self.tiptap_attachment(self.file),
                )
            )

            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                result["data"]["entity"]["references"],
                [
                    {
                        "referenceType": "entity",
                        "entityType": "statusupdate",
                        "label": "",
                        "guid": statusupdate.guid,
                        "url": statusupdate.url,
                    }
                ],
            )
            self.assertEqual(result["data"]["entity"]["referenceCount"], 1)
            self.assertTrue(truncate_rich_description.called)

        def test_configuration_reference(self):
            FileReference.objects.update_configuration(
                constances.ConfigurationChoices.FAVICON.name, self.file
            )

            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                result["data"]["entity"]["references"],
                [
                    {
                        "referenceType": "setting",
                        "entityType": None,
                        "label": "Geconfigureerd als favicon",
                        "guid": None,
                        "url": None,
                    }
                ],
            )
            self.assertEqual(result["data"]["entity"]["referenceCount"], 1)

        def test_multi_reference_count(self):
            blog1 = self.remember(BlogFactory(owner=self.owner))
            blog2 = self.remember(BlogFactory(owner=self.owner))

            FileReference.objects.create(file=self.file, container=blog1)
            FileReference.objects.create(file=self.file, container=blog2)

            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(result["data"]["entity"]["referenceCount"], 2)


class TestResolveFileEntityReferencesTestCase(
    FileFolderReferencesTemplate.BaseTestCase
):
    def factory(self, **kwargs):
        return FileFactory(**kwargs)

    def test_setting_reference(self):
        FileReference.objects.create(
            configuration=ConfigurationChoices.FAVICON.name, file=self.file
        )

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(
            result["data"]["entity"]["references"],
            [
                {
                    "referenceType": "setting",
                    "entityType": None,
                    "label": "Geconfigureerd als favicon",
                    "guid": None,
                    "url": None,
                }
            ],
        )

    def test_just_a_file_file(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["isPersonalFile"], False)
        self.assertEqual(result["data"]["entity"]["isFeaturedImage"], False)
        self.assertEqual(result["data"]["entity"]["referenceCount"], 0)

    def test_a_personal_file(self):
        FileReference.objects.create(
            file=self.file, configuration=constances.PERSONAL_FILE
        )

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["isPersonalFile"], True)
        self.assertEqual(result["data"]["entity"]["isFeaturedImage"], False)
        self.assertEqual(result["data"]["entity"]["referenceCount"], 0)

    def test_a_featured_image(self):
        self.remember(BlogFactory(owner=self.owner, featured_image=self.file))

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertEqual(result["data"]["entity"]["isPersonalFile"], False)
        self.assertEqual(result["data"]["entity"]["isFeaturedImage"], True)
        self.assertEqual(result["data"]["entity"]["referenceCount"], 1)


class TestResolveFolderEntityReferencesTestCase(
    FileFolderReferencesTemplate.BaseTestCase
):
    def factory(self, **kwargs):
        return FolderFactory(**kwargs)


class TestResolvePadEntityReferencesTestCase(FileFolderReferencesTemplate.BaseTestCase):
    def factory(self, **kwargs):
        return PadFactory(**kwargs)
