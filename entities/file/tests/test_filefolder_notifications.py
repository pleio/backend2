from unittest import mock

from django.core.files.base import ContentFile

from core.factories import GroupFactory
from core.tasks import create_notification
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestFileNotificationsOnCreateTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_setting(ENV="unit-test")

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.member = UserFactory()
        self.group.join(self.member)

        self.file_name = self.use_mock_file("test.txt")

        self.mutation = """
        mutation AddFile($input: addFileInput!) {
            addFile(input: $input) {
                entity {
                    ... on File {
                        guid
                    }
                }
            }
        }
        """

        self.variables = {
            "input": {
                "file": self.file_name,
            }
        }

    def create_file(self, group=None):
        if group:
            self.variables["input"]["containerGuid"] = group.guid
        self.graphql_client.force_login(self.owner)
        self.graphql_client.post(self.mutation, self.variables)

    @mock.patch("core.tasks.create_notification.delay")
    def test_send_notifications_is_called_during_file_create(
        self,
        send_notifications,
    ):
        self.create_file(self.group)

        self.assertTrue(send_notifications.called)

    @mock.patch("core.tasks.create_notification.delay")
    def test_send_notifications_is_called_during_non_group_file_create(
        self,
        send_notifications,
    ):
        self.create_file()

        self.assertFalse(send_notifications.called)


class TestFileNotificationsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.group = GroupFactory(
            owner=self.owner,
            auto_notification=True,
            file_notifications=True,
        )

        self.file = FileFactory(
            owner=self.owner,
            group=self.group,
            upload=ContentFile(b"test\n", "testfile.txt"),
        )

        self.member = UserFactory()
        self.group.join(self.member)

    def test_send_notifications_sends_notification_to_members(self):
        """
        Members should get a notification when a file is created.
        """
        self.member.notifications.all().delete()

        create_notification(
            self.tenant.schema_name,
            "created",
            "file.FileFolder",
            self.file.guid,
            self.file.owner.guid,
        )

        self.assertEqual(self.member.notifications.count(), 1)

    def test_send_notifications_when_disabled(self):
        """
        Members should not get a notification if the functionality is not enabled.
        """
        self.group.file_notifications = False
        self.group.save()
        self.member.notifications.all().delete()

        create_notification(
            self.tenant.schema_name,
            "created",
            "file.FileFolder",
            self.file.guid,
            self.file.owner.guid,
        )

        self.assertEqual(self.member.notifications.count(), 0)

    def test_no_notifications_are_sent_to_non_members(self):
        """
        Non-members should not get a notification when a file is created.
        """
        user = UserFactory()
        user.notifications.all().delete()

        create_notification(
            self.tenant.schema_name,
            "created",
            "file.FileFolder",
            self.file.guid,
            self.file.owner.guid,
        )

        self.assertEqual(user.notifications.count(), 0)
