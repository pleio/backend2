from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import UserFactory


class TestBookmarkFileTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = self.remember(UserFactory())
        self.file = self.remember(FileFactory(owner=self.owner))

    def test_bookmark_file(self):
        query = """
        mutation BookmarkFile($input: bookmarkInput!) {
            bookmark(input: $input) {
                object {
                    guid
                    ... on File {
                        isBookmarked
                    }
                }
            }
        }
        """
        variables = {"input": {"guid": self.file.guid, "isAdding": True}}

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(query, variables)

        self.assertEqual(result["data"]["bookmark"]["object"]["guid"], self.file.guid)
        self.assertTrue(result["data"]["bookmark"]["object"]["isBookmarked"])

    def test_query_bookmarked_files(self):
        self.remember(FileFactory(owner=self.remember(UserFactory())))
        self.file.add_bookmark(self.owner)
        query = """
        query QueryBookmarkedFiles {
            bookmarks(subtype: "file") {
                total
                edges {
                    guid
                }
            }
        }
        """

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(query, {})

        self.assertEqual(result["data"]["bookmarks"]["total"], 1)
        self.assertEqual(
            result["data"]["bookmarks"]["edges"][0]["guid"], self.file.guid
        )

    def test_remove_bookmarked_file(self):
        self.file.add_bookmark(self.remember(UserFactory()))

        query = """
        mutation DeleteBookmarkedFile($input: deleteEntityInput!) {
            deleteEntity(input: $input) {
                success
            }
        }
        """
        variables = {
            "input": {
                "guid": self.file.guid,
            }
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(query, variables)

        self.assertTrue(result["data"]["deleteEntity"]["success"])
