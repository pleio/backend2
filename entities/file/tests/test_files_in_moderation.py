from django.contrib.auth.models import AnonymousUser
from django.core.files.base import ContentFile
from django.utils import timezone

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from user.factories import (
    AdminFactory,
    EditorFactory,
    NewsEditorFactory,
    QuestionManagerFactory,
    UserFactory,
    UserManagerFactory,
)


class TestFilesInModerationTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            IS_CLOSED=False,
            CONTENT_MODERATION_ENABLED=True,
            REQUIRE_CONTENT_MODERATION_FOR=["file"],
        )
        self.file_owner = UserFactory(email="owner")
        self.group_owner = UserFactory(email="group_owner")

        self.group = GroupFactory(owner=self.group_owner)
        self.group.join(self.file_owner)

        self.file = FileFactory(
            owner=self.file_owner,
            group=self.group,
            upload=ContentFile(b"123", "test.txt"),
            published=None,
            read_access=[ACCESS_TYPE.public],
        )
        self.file.publish_requests.create(time_published=timezone.now())

    def test_files_in_moderation_download(self):
        # Demonstrate that a file in moderation can be downloaded by
        # - The author
        # - An editor
        # - The site admin

        self.assertHasFileAccess(self.file_owner)
        self.assertHasFileAccess(EditorFactory())
        self.assertHasFileAccess(AdminFactory())

    def test_files_in_moderation_download_denied(self):
        # Files should not be accessible by
        # - Anonymous users
        # - Other users
        # - The group owner
        # - QuestionManagers
        # - News editors
        # - User managers

        self.assertNotHasFileAccess(None)
        self.assertNotHasFileAccess(UserFactory())
        self.assertNotHasFileAccess(self.group_owner)
        self.assertNotHasFileAccess(QuestionManagerFactory())
        self.assertNotHasFileAccess(NewsEditorFactory())
        self.assertNotHasFileAccess(UserManagerFactory())

    def assertHasFileAccess(self, user):
        response = self.fetchFile(user)
        self.assertEqual(
            response.status_code,
            200,
            msg="%s has no access to the file" % (user or AnonymousUser()),
        )

    def assertNotHasFileAccess(self, user):
        response = self.fetchFile(user)
        self.assertNotEqual(
            response.status_code,
            200,
            msg="%s has access to the file" % (user or AnonymousUser()),
        )

    def fetchFile(self, user):
        if user:
            self.client.force_login(user)
        else:
            self.client.logout()
        return self.client.get(self.file.download_url)
