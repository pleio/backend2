from unittest import mock

from django.contrib.auth.models import AnonymousUser

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory, PadFactory
from entities.file.models import FileFolder
from entities.file.resolvers.resolve_delete_file import RecursiveDeleteFileResolver
from user.factories import UserFactory


class Template:
    class TestDeleteFileTestCase(PleioTenantTestCase):
        def build_entity(self, **kwargs):  # pragma: no cover
            raise NotImplementedError()

        def setUp(self):
            super().setUp()
            self.owner = self.remember(UserFactory(email="owner@example.com"))
            self.entity = self.remember(self.build_entity(owner=self.owner))

            self.mutation = """
            mutation DeleteFile($input: deleteEntityInput!) {
                deleteEntity(input: $input) {
                    success
                }
            }
            """
            self.variables = {"input": {"guid": self.entity.guid}}

        @mock.patch(
            "entities.file.resolvers.resolve_delete_file.RecursiveDeleteFileResolver.resolve"
        )
        def test_delete_entity(self, delete_file):
            delete_file.return_value = {"success": True}
            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.mutation, self.variables)

            self.assertEqual(result["data"]["deleteEntity"]["success"], True)
            self.assertTrue(delete_file.called)


class TestDeleteFileTestCase(Template.TestDeleteFileTestCase):
    def build_entity(self, **kwargs):
        return FileFactory(**kwargs)


class TestDeleteFolderTestCase(Template.TestDeleteFileTestCase):
    def build_entity(self, **kwargs):
        return FolderFactory(**kwargs)


class TestDeletePadTestCase(Template.TestDeleteFileTestCase):
    def build_entity(self, **kwargs):
        return PadFactory(**kwargs)


class MockRequest:
    def __init__(self, user=None):
        self.user = user or AnonymousUser()


class TestResolveDeleteGroupFolderWithReferencesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.group = self.remember(GroupFactory(owner=self.owner, name="Group"))
        owner_group = {"owner": self.owner, "group": self.group}
        self.folder1 = self.remember(FolderFactory(**owner_group, title="Folder1"))
        self.file1 = self.remember(
            FileFactory(**owner_group, parent=self.folder1, title="File1")
        )
        self.file2 = self.remember(
            FileFactory(**owner_group, parent=self.folder1, title="File2")
        )
        self.folder2 = self.remember(
            FolderFactory(**owner_group, parent=self.folder1, title="Folder2")
        )
        self.file3 = self.remember(
            FileFactory(**owner_group, parent=self.folder2, title="File3")
        )

        self.blog = self.remember(
            BlogFactory(
                owner=self.owner,
                group=self.group,
                rich_description=self.tiptap_attachment(self.file1, self.folder2),
            )
        )

    def test_try_delete_folder(self):
        resolver = RecursiveDeleteFileResolver(
            self.folder1, {}, MockRequest(self.owner)
        )
        result = resolver.resolve()

        self.assertEqual(result["success"], False)
        self.assertEqual(
            sorted(result["errors"], key=lambda x: x["entity"].created_at),
            [
                {"entity": self.file1, "next": "softDelete"},
                {"entity": self.folder2, "next": "softDelete"},
            ],
        )

    def test_try_delete_soft_deleted_file(self):
        self.file1.remove_soft_references()
        self.file1.save()
        resolver = RecursiveDeleteFileResolver(self.file1, {}, MockRequest(self.owner))
        result = resolver.resolve()

        self.assertEqual(
            result,
            {
                "success": False,
                "errors": [
                    {"entity": self.file1, "next": "forceDelete"},
                ],
            },
        )

    @mock.patch(
        "entities.file.resolvers.resolve_delete_file.schedule_update_referencing_entities"
    )
    def test_soft_delete(self, update_references):
        # When.
        resolver = RecursiveDeleteFileResolver(
            self.folder1, {"recursive": "softDelete"}, MockRequest(self.owner)
        )
        result = resolver.resolve()

        # Then.
        self.assertEqual(result, {"success": True})
        self.assertFalse(FileFolder.objects.filter(pk=self.folder1.pk).exists())
        self.assertFalse(FileFolder.objects.filter(pk=self.file2.pk).exists())
        self.assertTrue(FileFolder.objects.filter(pk=self.file1.pk).exists())
        self.assertTrue(FileFolder.objects.filter(pk=self.folder2.pk).exists())
        self.assertTrue(FileFolder.objects.filter(pk=self.file3.pk).exists())
        self.file1.refresh_from_db()
        self.assertIsNone(self.file1.group)
        self.assertIsNone(self.file1.parent)
        self.assertTrue(update_references.called_with(self.file1))
        self.assertTrue(update_references.called_with(self.file3))
        self.assertTrue(update_references.called_with(self.folder2))

    @mock.patch(
        "entities.file.resolvers.resolve_delete_file.schedule_delete_from_referencing_entities"
    )
    @mock.patch(
        "entities.file.resolvers.resolve_delete_file.schedule_update_referencing_entities"
    )
    def test_force_delete(self, update_references, cleanup_references):
        resolver = RecursiveDeleteFileResolver(
            self.folder2, {"recursive": "forceDelete"}, MockRequest(self.owner)
        )
        result = resolver.resolve()

        self.assertEqual(result, {"success": True})
        self.assertTrue(FileFolder.objects.filter(pk=self.folder1.pk).exists())
        self.assertFalse(FileFolder.objects.filter(pk=self.folder2.pk).exists())
        self.assertFalse(FileFolder.objects.filter(pk=self.file3.pk).exists())
        self.assertTrue(cleanup_references.called_with(self.folder2))
        self.assertTrue(cleanup_references.called_with(self.file3))
        self.assertFalse(update_references.called)

    @mock.patch(
        "entities.file.resolvers.resolve_delete_file.schedule_delete_from_referencing_entities"
    )
    @mock.patch(
        "entities.file.resolvers.resolve_delete_file.schedule_update_referencing_entities"
    )
    def test_force_delete_soft_deleted_file(
        self, update_references, cleanup_references
    ):
        self.file1.remove_soft_references()
        self.file1.save()
        resolver = RecursiveDeleteFileResolver(
            self.file1, {"recursive": "forceDelete"}, MockRequest(self.owner)
        )
        result = resolver.resolve()

        self.assertEqual(result, {"success": True})
        self.assertFalse(FileFolder.objects.filter(pk=self.file1.pk).exists())
        self.assertTrue(cleanup_references.called_with(self.file1))
        self.assertFalse(update_references.called)

    def test_force_delete_featured_image(self):
        self.blog.featured_image = self.file1
        self.blog.save()
        resolver = RecursiveDeleteFileResolver(
            self.file1, {"recursive": "forceDelete"}, MockRequest(self.owner)
        )
        result = resolver.resolve()

        self.assertEqual(result, {"success": True})
        self.assertFalse(FileFolder.objects.filter(pk=self.file1.pk).exists())
        self.blog.refresh_from_db()
        self.assertIsNone(self.blog.featured_image)


class TestResolveDeleteGroupFolderWithSomeFilesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = self.remember(UserFactory(email="user@example.com"))
        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.group = self.remember(GroupFactory(owner=self.owner, name="Group"))
        owner_group = {"owner": self.owner, "group": self.group}
        self.folder1 = self.remember(FolderFactory(**owner_group, title="Folder1"))
        self.file1 = self.remember(
            FileFactory(**owner_group, parent=self.folder1, title="File1")
        )

    def execute_query(self, user, file_id):
        query = """
        mutation DeleteFolder($input: deleteEntityInput!) {
            deleteEntity(input: $input) {
                success
            }
        }
        """
        variables = {"input": {"guid": file_id, "recursive": "tryDelete"}}
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(query, variables)
        return response["data"]

    def test_delete_folder_with_permission_error(self):
        with self.assertGraphQlError("missing_permissions"):
            self.execute_query(self.user, self.folder1.guid)

        self.assertTrue(
            FileFolder.objects.filter(pk=self.file1.pk).exists(),
            "File was unexpectedly deleted",
        )
        self.assertTrue(
            FileFolder.objects.filter(pk=self.folder1.pk).exists(),
            "Folder was unexpectedly deleted",
        )

    def test_delete_file(self):
        self.execute_query(self.owner, self.file1.guid)

        self.assertFalse(
            FileFolder.objects.filter(pk=self.file1.pk).exists(),
            "File unexpectedly still exists",
        )
        self.assertTrue(
            FileFolder.objects.filter(pk=self.folder1.pk).exists(),
            "Folder was unexpectedly deleted",
        )

    def test_delete_folder(self):
        self.execute_query(self.owner, self.folder1.guid)

        self.assertFalse(
            FileFolder.objects.filter(pk=self.file1.pk).exists(),
            "File unexpectedly still exists",
        )
        self.assertFalse(
            FileFolder.objects.filter(pk=self.folder1.pk).exists(),
            "Folder unexpectedly still exists",
        )
