from unittest import mock
from unittest.mock import patch

from django.core.files.base import ContentFile
from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.helpers.compression import get_download_filename
from user.factories import UserFactory
from user.models import User

from ..factories import FileFactory
from ..models import FileFolder


class EditFileFolderTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(
            EXTRA_LANGUAGES=["en", "nl", "de"],
            LANGUAGE="de",
        )

        self.authenticatedUser = mixer.blend(User, name="Aut Hen Ticated")
        self.user1 = mixer.blend(User, name="User One")
        self.user2 = mixer.blend(User, name="Someone Else")

        self.PREVIOUS_DESCRIPTION = "PREVIOUS_DESCRIPTION"
        self.EXPECTED_DESCRIPTION = "EXPECTED_DESCRIPTION"

        self.group = mixer.blend(Group, owner=self.authenticatedUser)
        self.group.join(self.user1, "member")

        self.folder1 = FileFolder.objects.create(
            title="Folder 1 (L1)",
            owner=self.authenticatedUser,
            rich_description=self.PREVIOUS_DESCRIPTION,
            upload=None,
            type=FileFolder.Types.FOLDER,
            group=self.group,
            parent=None,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
        )
        self.folder2 = FileFolder.objects.create(
            title="Folder 2 (L1.1)",
            owner=self.authenticatedUser,
            rich_description=self.PREVIOUS_DESCRIPTION,
            upload=None,
            type=FileFolder.Types.FOLDER,
            group=self.group,
            parent=self.folder1,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
        )
        self.folder3 = FileFolder.objects.create(
            title="Folder 3 (L2)",
            owner=self.user1,
            rich_description=self.PREVIOUS_DESCRIPTION,
            upload=None,
            type=FileFolder.Types.FOLDER,
            group=self.group,
            parent=None,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user1.id)],
        )

        self.file2 = FileFolder.objects.create(
            title="File 2 in folder 3",
            owner=self.authenticatedUser,
            rich_description=self.PREVIOUS_DESCRIPTION,
            upload=None,
            type=FileFolder.Types.FILE,
            group=self.group,
            parent=self.folder3,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
        )
        self.file3 = FileFolder.objects.create(
            title="File 3 in folder 2",
            owner=self.authenticatedUser,
            rich_description=self.PREVIOUS_DESCRIPTION,
            upload=None,
            type=FileFolder.Types.FILE,
            group=self.group,
            parent=self.folder2,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
        )
        self.file4 = FileFolder.objects.create(
            title="File 4 in folder 2",
            owner=self.authenticatedUser,
            rich_description=self.PREVIOUS_DESCRIPTION,
            upload=None,
            type=FileFolder.Types.FILE,
            group=self.group,
            parent=self.folder2,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
        )
        self.file5 = FileFolder.objects.create(
            owner=self.user2,
            rich_description=self.PREVIOUS_DESCRIPTION,
            type=FileFolder.Types.FILE,
            group=self.group,
            parent=self.folder1,
            title="File 5 in folder 2 - Someone Elses file",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.user2.id)],
        )
        self.data = {
            "input": {
                "guid": None,
                "richDescription": self.EXPECTED_DESCRIPTION,
                "title": "",
                "file": "",
                "ownerGuid": "",
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
            }
        }
        self.mutation = """
             mutation ($input: editFileFolderInput!) {
                editFileFolder(input: $input) {
                    entity {
                    guid
                    status
                    ... on File {
                        inputLanguage
                        isTranslationEnabled
                        title
                        description
                        richDescription
                        timeCreated
                        timeUpdated
                        timePublished
                        scheduleArchiveEntity
                        scheduleDeleteEntity
                        accessId
                        writeAccessId
                        canEdit
                        canArchiveAndDelete
                        tags
                        url
                        inGroup
                        group {
                            guid
                        }
                        owner {
                            guid
                        }
                        mimeType
                    }
                    ... on Folder {
                        inputLanguage
                        isTranslationEnabled
                        title
                        description
                        richDescription
                        timeCreated
                        timeUpdated
                        timePublished
                        scheduleArchiveEntity
                        scheduleDeleteEntity
                        accessId
                        writeAccessId
                        canEdit
                        canArchiveAndDelete
                        tags
                        url
                        inGroup
                        group {
                            guid
                        }
                        owner {
                            guid
                        }
                    }
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    @patch("entities.file.models.FileFolder.scan")
    @patch("core.resolvers.shared.strip_exif")
    def test_edit_file_title(self, mocked_strip_exif, mocked_scan):
        test_file = FileFolder.objects.create(
            rich_description=self.PREVIOUS_DESCRIPTION,
            read_access=[ACCESS_TYPE.logged_in],
            group=self.group,
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=ContentFile(b"Image!", "example.jpg"),
        )

        variables = self.data

        newPublishedTime = timezone.now() + timezone.timedelta(days=-1)

        variables["input"]["guid"] = test_file.guid
        variables["input"]["isTranslationEnabled"] = False
        variables["input"]["title"] = "test123.gif"
        variables["input"]["tags"] = ["tag_one", "tag_two"]
        variables["input"]["timePublished"] = newPublishedTime.isoformat()
        variables["input"]["ownerGuid"] = self.user1.guid
        variables["input"]["inputLanguage"] = "en"

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editFileFolder"]["entity"]
        self.assertEqual(entity["inputLanguage"], "en")
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(entity["description"], self.EXPECTED_DESCRIPTION)
        self.assertEqual(entity["richDescription"], self.EXPECTED_DESCRIPTION)
        self.assertEqual(entity["mimeType"], test_file.mime_type)
        self.assertEqual(entity["tags"][0], "tag_one")
        self.assertEqual(entity["tags"][1], "tag_two")
        self.assertEqual(entity["owner"]["guid"], self.user1.guid)
        self.assertDateEqual(entity["timePublished"], str(newPublishedTime))
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )

        # file is not updated, so these methods are not called:
        self.assertFalse(mocked_strip_exif.called)
        self.assertFalse(mocked_scan.called)

        test_file.refresh_from_db()
        self.assertEqual(test_file.title, variables["input"]["title"])
        self.assertEqual(test_file.tags, variables["input"]["tags"])
        self.assertEqual(test_file.published, newPublishedTime)
        self.assertEqual(test_file.owner.guid, self.user1.guid)
        self.assertIn(ACCESS_TYPE.user.format(self.user1.guid), test_file.write_access)

        self.assertTrue(self.mocked_create_publish_request.called)
        self.assertTrue(self.mocked_revert_published.called)

    def test_edit_folder_access_ids_recursive(self):
        mutation = """
            mutation editFileFolder($input: editFileFolderInput!) {
                editFileFolder(input: $input) {
                    entity {
                        guid
                        ... on File {
                            inputLanguage
                            isTranslationEnabled
                            accessId
                            writeAccessId
                        }
                        ... on Folder {
                            inputLanguage
                            isTranslationEnabled
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """
        variables = {
            "input": {
                "guid": self.folder1.guid,
                "accessId": 1,
                "writeAccessId": 1,
                "isAccessRecursive": True,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editFileFolder"]["entity"]
        self.assertEqual(entity["guid"], self.folder1.guid)
        self.assertEqual(entity["inputLanguage"], "en")
        self.assertEqual(entity["isTranslationEnabled"], False)

        query = """
            query OpenFolder($guid: String, $typeFilter: [String]) {
                files(containerGuid: $guid, typeFilter: $typeFilter) {
                    total
                    edges {
                        guid
                        ... on File {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                        ... on Folder {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """

        variables = {"guid": self.folder1.guid, "typeFilter": ["folder"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.folder2.guid)
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["writeAccessId"], 1)

        variables = {"guid": self.folder2.guid, "typeFilter": ["file"], "limit": 1}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.file3.guid)
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["writeAccessId"], 1)
        self.assertEqual(result["data"]["files"]["total"], 2)

    def test_edit_folder_access_id_recursive(self):
        mutation = """
            mutation editFileFolder($input: editFileFolderInput!) {
                editFileFolder(input: $input) {
                    entity {
                        guid
                        ... on File {
                            accessId
                            writeAccessId
                        }
                        ... on Folder {
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """

        variables = {
            "input": {
                "guid": self.folder1.guid,
                "accessId": 1,
                "isAccessRecursive": True,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editFileFolder"]["entity"]
        self.assertEqual(entity["guid"], self.folder1.guid)

        query = """
            query OpenFolder($guid: String, $typeFilter: [String]) {
                files(containerGuid: $guid, typeFilter: $typeFilter) {
                    edges {
                        guid
                        ... on File {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                        ... on Folder {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                    }
                }
            }
        """

        variables = {"guid": self.folder1.guid, "typeFilter": ["folder"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.folder2.guid)
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["writeAccessId"], 0)

        variables = {"guid": self.folder2.guid, "typeFilter": ["file"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.file3.guid)
        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["writeAccessId"], 0)

    def test_edit_folder_write_access_id_recursive(self):
        mutation = """
            mutation editFileFolder($input: editFileFolderInput!) {
                editFileFolder(input: $input) {
                    entity {
                        guid
                        ... on File {
                            title
                            accessId
                            writeAccessId
                        }
                        ... on Folder {
                            title
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """
        variables = {
            "input": {
                "guid": self.folder1.guid,
                "writeAccessId": 1,
                "isAccessRecursive": True,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editFileFolder"]["entity"]
        self.assertEqual(entity["guid"], self.folder1.guid)

        query = """
            query OpenFolder($guid: String, $typeFilter: [String]) {
                files(containerGuid: $guid, typeFilter: $typeFilter) {
                    edges {
                        guid
                        ... on File {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                        ... on Folder {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                    }
                }
            }
        """

        variables = {"guid": self.folder1.guid, "typeFilter": ["folder"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.folder2.guid)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["writeAccessId"], 1)

        variables = {"guid": self.folder2.guid, "typeFilter": ["file"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.file3.guid)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["writeAccessId"], 1)

    def test_edit_folder_access_ids_not_recursive(self):
        """
        Test that when authenticatedUser updates the access of folder1
          with the recursive=false option
          then the folder2
          and the file3
            are not updated.
        """
        mutation = """
            mutation editFileFolder($input: editFileFolderInput!) {
                editFileFolder(input: $input) {
                    entity {
                        guid
                        ... on File {
                            accessId
                            writeAccessId
                        }
                        ... on Folder {
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """

        variables = {
            "input": {
                "guid": self.folder1.guid,
                "accessId": 1,
                "writeAccessId": 1,
                "isAccessRecursive": False,
            }
        }
        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editFileFolder"]["entity"]
        self.assertEqual(entity["guid"], self.folder1.guid)

        query = """
            query OpenFolder($guid: String, $typeFilter: [String]) {
                files(containerGuid: $guid, typeFilter: $typeFilter) {
                    edges {
                        guid
                        ... on File {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                        ... on Folder {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """

        variables = {"guid": self.folder1.guid, "typeFilter": ["folder"]}

        result = self.graphql_client.post(query, variables)

        entity = result["data"]["files"]["edges"][0]
        self.assertEqual(entity["guid"], self.folder2.guid)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["writeAccessId"], 0)

        variables = {"guid": self.folder2.guid, "typeFilter": ["file"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.file3.guid)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["writeAccessId"], 0)

    def test_edit_folder_access_ids_recursive_no_read_access_file(self):
        """
        Test that file2 - protected by authenticatedUser
           can't be edited by user1
           as he recursively updates his folder3
        """
        mutation = """
            mutation editFileFolder($input: editFileFolderInput!) {
                editFileFolder(input: $input) {
                    entity {
                        guid
                        ... on File {
                            accessId
                            writeAccessId
                        }
                        ... on Folder {
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """
        variables = {
            "input": {
                "guid": self.folder3.guid,
                "accessId": 1,
                "writeAccessId": 1,
                "isAccessRecursive": True,
            }
        }

        self.graphql_client.force_login(self.user1)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editFileFolder"]["entity"]
        self.assertEqual(entity["guid"], self.folder3.guid)

        query = """
            query OpenFolder($guid: String, $typeFilter: [String]) {
                files(containerGuid: $guid, typeFilter: $typeFilter) {
                    edges {
                        guid
                        ... on File {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                            mimeType
                        }
                        ... on Folder {
                            hasChildren
                            title
                            subtype
                            url
                            accessId
                            writeAccessId
                        }
                    }
                }
            }
        """

        variables = {"guid": self.folder3.guid, "typeFilter": ["file"]}

        result = self.graphql_client.post(query, variables)
        entity = result["data"]["files"]["edges"][0]

        self.assertEqual(entity["guid"], self.file2.guid)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["writeAccessId"], 0)

    @patch("entities.file.tasks.post_process_file_attributes.delay")
    @patch("entities.file.models.is_upload_complete")
    def test_retry_if_not_ok(
        self, mock_is_upload_complete, mock_post_process_file_attributes
    ):
        file_mock = ContentFile(b"Image!", "icon-image.jpg")
        mock_is_upload_complete.return_value = False

        self.file = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
        )

        assert mock_post_process_file_attributes.called

    def test_get_download_filename(self):
        file_mock = ContentFile(b"Image!", "icon-name.jpg")

        self.file = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
            title="icon-name.jpg",
        )

        self.assertEqual(get_download_filename(self.file), "icon-name.jpg")

        self.file.title = "iconnewname"
        self.file.save()

        self.assertEqual(get_download_filename(self.file), "iconnewname.jpg")

        self.file.title = "iconnewname.txt"
        self.file.save()

        self.assertEqual(get_download_filename(self.file), "iconnewname.txt.jpg")

    def test_get_download_filename_csv(self):
        file_mock = ContentFile(b"text", "csv-name.csv")

        self.file = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
            title="csv-name.csv",
        )

        self.assertEqual(get_download_filename(self.file), "csv-name.csv")

    def test_get_download_filename_no_mimetype(self):
        file_mock = ContentFile(b"text", "csv-name.weird")

        self.file = FileFolder.objects.create(
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            tags=["tag1", "tag2"],
            upload=file_mock,
            title="csv-name.weird",
        )

        self.assertEqual(get_download_filename(self.file), "csv-name.weird")

    def test_update_ownership_updates_only_current_file_folder(self):
        query = """
        mutation editFileFolder($input: editFileFolderInput!) {
            editFileFolder(input: $input) {
                entity {
                    guid
                }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.folder1.guid,
                "ownerGuid": self.user1.guid,
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        self.graphql_client.post(query, variables)

        expected_access = ACCESS_TYPE.user.format(self.user1.guid)
        for entity, message in (
            (self.folder2, "Folder2 %s access is unexpectedly updated"),
            (self.file3, "File3 %s access is unexpectedly updated"),
            (self.file4, "File4 %s access is unexpectedly updated"),
            (self.file5, "File4 %s access is unexpectedly updated"),
        ):
            entity.refresh_from_db()
            self.assertNotIn(
                expected_access, entity.write_access, msg=message % "write"
            )

        self.folder1.refresh_from_db()
        self.assertIn(
            expected_access,
            self.folder1.write_access,
            msg="Folder1 is not updated correctly",
        )

    def test_update_ownership_updates_recursive(self):
        query = """
        mutation editFileFolder($input: editFileFolderInput!) {
            editFileFolder(input: $input) {
                entity {
                    guid
                }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.folder1.guid,
                "ownerGuid": self.user1.guid,
                "ownerGuidRecursive": "updateAllFiles",
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        self.graphql_client.post(query, variables)

        expected_access = ACCESS_TYPE.user.format(self.user1.guid)
        for entity, message in (
            (self.folder1, "Folder1 %s access is not updated correctly"),
            (self.folder2, "Folder2 %s access is not updated correctly"),
            (self.file3, "File3 %s access is not updated correctly"),
            (self.file4, "File4 %s access is not updated correctly"),
            (self.file5, "File5 %s access is not updated correctly"),
        ):
            entity.refresh_from_db()
            self.assertNotIn(expected_access, entity.read_access, msg=message % "read")
            self.assertIn(expected_access, entity.write_access, msg=message % "write")

    def test_update_ownership_updates_recursive_by_owner(self):
        query = """
        mutation editFileFolder($input: editFileFolderInput!) {
            editFileFolder(input: $input) {
                entity {
                    guid
                }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.folder1.guid,
                "ownerGuid": self.user1.guid,
                "ownerGuidRecursive": "updateOwnerFiles",
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        self.graphql_client.post(query, variables)

        expected_access = ACCESS_TYPE.user.format(self.user1.guid)
        for entity, message in (
            (self.folder1, "Folder1 %s access is not updated correctly"),
            (self.folder2, "Folder2 %s access is not updated correctly"),
            (self.file3, "File3 %s access is not updated correctly"),
            (self.file4, "File4 %s access is not updated correctly"),
        ):
            entity.refresh_from_db()
            self.assertNotIn(expected_access, entity.read_access, msg=message % "read")
            self.assertIn(expected_access, entity.write_access, msg=message % "write")

        self.file5.refresh_from_db()
        self.assertNotIn(
            expected_access,
            self.file5.write_access,
            msg="File5 is updated unexpectedly",
        )


class TestEditAttachmentFileTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = self.remember(UserFactory())
        self.attachment = self.remember(
            FileFactory(
                owner=self.owner, upload=ContentFile(b"not a real xls", "is-a-demo.xls")
            )
        )
        self.blog = self.remember(
            BlogFactory(
                owner=self.owner,
                rich_description=self.tiptap_attachment(self.attachment),
            )
        )

        self.mutation = """
        mutation EditFile($input: editFileFolderInput!) {
            editFile: editFileFolder(input: $input) {
                entity { guid }
            }
        }
        """
        self.variables = {
            "input": {
                "guid": self.attachment.guid,
            }
        }

    @mock.patch("entities.file.resolvers.mutation.schedule_update_referencing_entities")
    def test_attachment_is_read_only(self, update_referencing_entities):
        self.variables["input"]["title"] = "not-a-demo"

        with self.assertGraphQlError():
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)

        self.assertFalse(update_referencing_entities.called)
