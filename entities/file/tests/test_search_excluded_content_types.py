from django.core.files.base import ContentFile

from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory, FolderFactory


class TestSearchWithExcludedFilesTestCase(
    Template.TestSearchWithExcludedContentTypesTestCase
):
    EXCLUDE_TYPES = ["file"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return FileFactory(
            title=title, owner=self.owner, upload=ContentFile(b"test", "test.txt")
        )


class TestSearchWithExcludedFoldersTestCase(
    Template.TestSearchWithExcludedContentTypesTestCase
):
    EXCLUDE_TYPES = ["folder"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return FolderFactory(title=title, owner=self.owner)
