from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from entities.file.mail_builders.file_scan_found import FileScanFoundMailer
from user.factories import AdminFactory


class TestMailerFileScanFoundTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_setting(ENV="unit-test")
        self.switch_language("en")

        self.admin = AdminFactory()
        self.virus_count = 42
        self.error_count = 555

        self.mailer = FileScanFoundMailer(
            admin=self.admin.guid,
            virus_count=self.virus_count,
            error_count=self.error_count,
        )

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    @mock.patch("entities.file.mail_builders.file_scan_found.get_control_url")
    def test_properties(self, get_control_url, add_local_context, get_context):
        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}
        get_control_url.return_value = mock.MagicMock()

        self.assertDictEqual(
            self.mailer.get_context(),
            {
                "error_count": self.error_count,
                "virus_count": self.virus_count,
                "scanlog_url": get_control_url.return_value,
                "add_local_context": "mocked",
                "get_context": "mocked",
            },
        )
        self.assertEqual(self.mailer.get_language(), self.admin.get_language())
        self.assertEqual(self.mailer.get_template(), "email/file_scan_found.html")
        self.assertEqual(self.mailer.get_receiver(), self.admin)
        self.assertEqual(self.mailer.get_receiver_email(), self.admin.email)
        self.assertEqual(self.mailer.get_sender(), None)
        self.assertEqual(
            self.mailer.get_subject(),
            "Filescan found suspicous files on https://%s" % self.tenant.primary_domain,
        )
        get_control_url.assert_called_once_with("scanlog/", site=self.tenant)
