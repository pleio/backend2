from core.tests.helpers.test_archive_entities import Wrapper
from entities.file.factories import FileFactory, FolderFactory, PadFactory


class TestArchiveFileEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return FileFactory(**kwargs)


class TestArchiveFolderEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return FolderFactory(**kwargs)


class TestArchivePadEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return PadFactory(**kwargs)
