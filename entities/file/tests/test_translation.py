from core.tests.helpers.test_translation import Wrapper
from entities.file.factories import FileFactory, FolderFactory


class TestFileTranslationFields(
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "File"

    def build_entity(self, **kwargs):
        return FileFactory(**kwargs)


class TestFolderTranslationFields(
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Folder"

    def build_entity(self, **kwargs):
        return FolderFactory(**kwargs)
