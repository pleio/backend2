from django.core.files.base import ContentFile

from core.constances import ACCESS_TYPE, ConfigurationChoices
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from entities.file.models import FileReference
from user.factories import UserFactory


class TestRefreshReadAccessTestCase(PleioTenantTestCase):
    """
    Test several edge cases around file.FileFolder.refresh_read_access
    """

    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.USER_ACCESS = ACCESS_TYPE.user.format(self.user.id)
        self.file = FileFactory(
            title="avatar.txt",
            owner=self.user,
            upload=ContentFile(b"123", "avatar.txt"),
        )

        self.group = GroupFactory(owner=UserFactory())
        self.group.join(self.user)
        self.GROUP_ACCESS = ACCESS_TYPE.group.format(self.group.id)

        self.blog = BlogFactory(owner=self.user, read_access=[self.GROUP_ACCESS])
        ref, _ = FileReference.objects.get_or_create(
            file=self.file, container=self.blog
        )

    def test_call_refresh_read_access(self):
        self.file.read_access = []
        result = self.file.refresh_read_access()

        self.assertTrue(result)
        self.assertEqual(len(self.file.read_access), 2)
        self.assertIn(self.USER_ACCESS, self.file.read_access)
        self.assertIn(self.GROUP_ACCESS, self.file.read_access)

    def test_call_refresh_personal_files_access(self):
        self.file.persist_file()

        self.file.read_access = []
        result = self.file.refresh_read_access()

        self.assertFalse(result)
        self.assertEqual(self.file.read_access, [])

    def test_call_refresh_configuration_files_access(self):
        self.file.referenced_by.create(configuration=ConfigurationChoices.ICON.name)

        self.file.read_access = []
        result = self.file.refresh_read_access()

        self.assertTrue(result)
        self.assertEqual(self.file.read_access, [ACCESS_TYPE.public])

    def test_call_refresh_avatar_files_access(self):
        FileReference.objects.update_avatar_file(self.user, self.file)

        self.file.read_access = []
        result = self.file.refresh_read_access()

        self.assertTrue(result)
        self.assertEqual(self.file.read_access, [ACCESS_TYPE.public])

    def test_call_refresh_group_files_access(self):
        self.file.group = self.group
        self.file.save()

        self.file.read_access = []
        result = self.file.refresh_read_access()

        self.assertFalse(result)
        self.assertEqual(self.file.read_access, [])
