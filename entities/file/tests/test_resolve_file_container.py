from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory, FolderFactory, PadFactory
from user.factories import UserFactory


class Template:
    class TestResolveContainerBase(PleioTenantTestCase):
        """
        Test the `rootContainer` and `container` properties of files, folders, pads.
        """

        def build_owner_once(self):
            if not getattr(self, "owner", None):
                self.owner = UserFactory(email="owner@example.com")
            return self.owner

        def build_entity(self, **kwargs):  # pragma: no cover
            raise NotImplementedError()

        def setUp(self):
            super().setUp()

            self.owner = self.remember(UserFactory(email="owner@example.com"))
            self.entity = self.remember(self.build_entity(owner=self.owner))

            self.query = """
            query LoadEntity($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ... on FileFolder {
                        rootContainer { guid }
                        container { guid }
                    }
                }
            }
            """

            self.variables = {"guid": self.entity.guid}

        def test_entity_in_group_root(self):
            group = self.remember(GroupFactory(owner=self.owner))
            self.entity.group = group
            self.entity.save()

            self.graphql_client.force_login(self.owner)
            response = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                response["data"]["entity"],
                {
                    "guid": self.entity.guid,
                    "rootContainer": {"guid": group.guid},
                    "container": {"guid": group.guid},
                },
            )

        def test_entity_in_group_folder(self):
            group = self.remember(GroupFactory(owner=self.owner))
            folder = self.remember(FolderFactory(owner=self.owner, group=group))
            self.entity.group = group
            self.entity.parent = folder
            self.entity.save()

            self.graphql_client.force_login(self.owner)
            response = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                response["data"]["entity"],
                {
                    "guid": self.entity.guid,
                    "rootContainer": {"guid": group.guid},
                    "container": {"guid": folder.guid},
                },
            )

        def test_entity_in_owner_root(self):
            self.entity.persist_file()

            self.graphql_client.force_login(self.owner)
            response = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                response["data"]["entity"],
                {
                    "guid": self.entity.guid,
                    "rootContainer": {"guid": self.owner.guid},
                    "container": {"guid": self.owner.guid},
                },
            )

        def test_entity_in_owner_folder(self):
            folder = self.remember(FolderFactory(owner=self.owner))
            folder.persist_file()
            self.entity.parent = folder
            self.entity.save()
            self.entity.persist_file()

            self.graphql_client.force_login(self.owner)
            response = self.graphql_client.post(self.query, self.variables)

            self.assertEqual(
                response["data"]["entity"],
                {
                    "guid": self.entity.guid,
                    "rootContainer": {"guid": self.owner.guid},
                    "container": {"guid": folder.guid},
                },
            )


class TestResolveFileContainerTestCase(Template.TestResolveContainerBase):
    def build_entity(self, **kwargs):
        return FileFactory(**kwargs)


class TestResolveFolderContainerTestCase(Template.TestResolveContainerBase):
    def build_entity(self, **kwargs):
        return FolderFactory(**kwargs)


class TestResolvePadContainerTestCase(Template.TestResolveContainerBase):
    def build_entity(self, **kwargs):
        return PadFactory(**kwargs)
