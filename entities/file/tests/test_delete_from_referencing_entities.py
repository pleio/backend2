from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from entities.file.tasks import delete_from_referencing_entities
from user.factories import UserFactory


class TestDeleteFromReferencingEntitiesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = self.remember(UserFactory(email="owner@example.com"))
        self.file = self.remember(FileFactory(owner=self.owner))
        self.blog = self.remember(
            BlogFactory(
                owner=self.owner, rich_description=self.tiptap_attachment(self.file)
            )
        )

    def test_delete_from_referencing_entities(self):
        delete_from_referencing_entities(
            self.tenant.schema_name, self.file.guid, self.blog.guid
        )
        self.blog.refresh_from_db()

        self.assertIn('"notAvailable": true', self.blog.rich_description)
