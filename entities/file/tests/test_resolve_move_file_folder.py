from unittest.mock import patch

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory

from ..factories import FileFactory, FolderFactory
from ..models import FileFolder


class MoveFileFolderTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = UserFactory()

        self.group = GroupFactory(
            name="Demo group",
            owner=self.authenticatedUser,
            is_membership_on_request=False,
        )

        self.another_group = GroupFactory(owner=UserFactory(), name="The new group")

        self.folder_root = FileFolder.objects.create(
            title="root",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            type=FileFolder.Types.FOLDER,
        )

        self.folder = FileFolder.objects.create(
            title="images",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            type=FileFolder.Types.FOLDER,
            parent=self.folder_root,
        )

        self.file = FileFolder.objects.create(
            title="file.jpg",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
        )
        self.group_file = FileFolder.objects.create(
            title="file.jpg",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            group=self.group,
        )

        self.variables = {
            "input": {
                "guid": None,
                "containerGuid": None,
            }
        }
        self.mutation = """
            mutation ($input: moveFileFolderInput!) {
                moveFileFolder(input: $input) {
                    entity {
                        guid
                        status
                        ... on File {
                            title
                            parentFolder {
                                guid
                            }
                            group {
                                guid
                            }
                        }
                        ... on Folder {
                            title
                            parentFolder {
                                guid
                            }
                            group {
                                guid
                            }
                        }
                    }
                }
            }
        """

    def test_move_file_to_folder(self):
        """
        Test that after moving a file to a folder, the file's parent property is updated.
        """
        self.variables["input"]["guid"] = self.file.guid
        self.variables["input"]["containerGuid"] = self.folder.guid

        with patch(
            "core.resolvers.shared.copy_attachment_references"
        ) as copy_attachment_references:
            self.graphql_client.force_login(self.authenticatedUser)
            result = self.graphql_client.post(self.mutation, self.variables)

        entity = result["data"]["moveFileFolder"]["entity"]
        self.assertEqual(entity["parentFolder"]["guid"], self.folder.guid)
        self.assertTrue(copy_attachment_references.called)

    def test_move_file_to_group(self):
        """
        Test that moving a non-group-file to a group is not allowed.
        """
        self.variables["input"]["guid"] = self.file.guid
        self.variables["input"]["containerGuid"] = self.group.guid

        with self.assertGraphQlError("INVALID_CONTAINER_GUID"):
            self.graphql_client.force_login(AdminFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_move_folder_to_group(self):
        """
        Test that moving a non-group-folder to a group is not allowed.
        """
        self.variables["input"]["guid"] = self.folder.guid
        self.variables["input"]["containerGuid"] = self.group.guid

        with self.assertGraphQlError("INVALID_CONTAINER_GUID"):
            self.graphql_client.force_login(AdminFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_move_folder_to_self(self):
        """
        Test that moving a folder to itself is not allowed.
        """
        self.variables["input"]["guid"] = self.folder.guid
        self.variables["input"]["containerGuid"] = self.folder.guid

        with self.assertGraphQlError("INVALID_CONTAINER_GUID"):
            self.graphql_client.force_login(self.authenticatedUser)
            self.graphql_client.post(self.mutation, self.variables)

    def test_move_folder_to_descendant_folder(self):
        """
        Test that moving a folder to a descendant folder is not allowed.
        """
        self.variables["input"]["guid"] = self.folder_root.guid
        self.variables["input"]["containerGuid"] = self.folder.guid

        with self.assertGraphQlError("INVALID_CONTAINER_GUID"):
            self.graphql_client.force_login(self.authenticatedUser)
            self.graphql_client.post(self.mutation, self.variables)

    def test_move_to_another_group_as_member(self):
        """
        Test that members can't move files to another group.
        """
        actor = self.group_file.owner
        self.group.join(actor)
        self.another_group.join(actor)
        self.variables["input"]["guid"] = self.group_file.guid
        self.variables["input"]["containerGuid"] = self.another_group.guid

        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(actor)
            self.graphql_client.post(self.mutation, self.variables)

    def test_move_to_another_group_as_siteadmin(self):
        """
        Test that site-admins can move files to another group.
        """
        self.variables["input"]["guid"] = self.group_file.guid
        self.variables["input"]["containerGuid"] = self.another_group.guid

        with self.assertNotRaisesException():
            self.graphql_client.force_login(AdminFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def prepare_move_to_folder_in_another_group(self):
        self.another_group.join(self.group_file.owner)
        folder = FolderFactory(owner=self.group_file.owner, group=self.another_group)
        self.variables["input"]["guid"] = self.group_file.guid
        self.variables["input"]["containerGuid"] = folder.guid

    def test_move_to_folder_in_another_group_as_non_admin(self):
        """
        Test that just-the-owner can't move files to a folder in another group.
        """
        self.prepare_move_to_folder_in_another_group()

        with self.assertGraphQlError("user_not_site_admin"):
            self.graphql_client.force_login(self.group_file.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_move_to_folder_in_another_group_as_siteadmin(self):
        """
        Test that site-admins can move files to a folder in another group.
        """
        self.prepare_move_to_folder_in_another_group()

        with self.assertNotRaisesException():
            self.graphql_client.force_login(AdminFactory())
            self.graphql_client.post(self.mutation, self.variables)


class MoveFolderWithChildrenToOtherGroup(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = UserFactory()
        self.source_group = GroupFactory(
            owner=self.authenticatedUser, name="Demo group"
        )
        self.target_group = GroupFactory(owner=UserFactory(), name="The new group")

        self.GROUP_ACCESS = ACCESS_TYPE.group.format(self.source_group.guid)
        self.TARGET_ACCESS = ACCESS_TYPE.group.format(self.target_group.guid)

        self.folder = FolderFactory(
            title="Parent folder",
            owner=self.authenticatedUser,
            group=self.source_group,
            read_access=[self.GROUP_ACCESS],
            write_access=[self.GROUP_ACCESS],
        )
        self.sub_file = FileFactory(
            title="Sub file.txt",
            owner=self.authenticatedUser,
            group=self.source_group,
            parent=self.folder,
            read_access=[self.GROUP_ACCESS],
            write_access=[self.GROUP_ACCESS],
        )
        self.variables = {
            "input": {
                "guid": self.folder.guid,
                "containerGuid": self.target_group.guid,
            }
        }
        self.mutation = """
            mutation ($input: moveFileFolderInput!) {
                moveFileFolder(input: $input) {
                    entity { guid }
                }
            }
        """

    def test_move_folder_with_children_to_other_group(self):
        self.graphql_client.force_login(AdminFactory())
        self.graphql_client.post(self.mutation, self.variables)

        self.folder.refresh_from_db()
        self.sub_file.refresh_from_db()

        self.assertEqual(self.folder.group, self.target_group)
        self.assertEqual(self.sub_file.group, self.target_group)

        self.assertIn(self.TARGET_ACCESS, self.folder.read_access)
        self.assertIn(self.TARGET_ACCESS, self.folder.write_access)
        self.assertNotIn(self.GROUP_ACCESS, self.folder.read_access)
        self.assertNotIn(self.GROUP_ACCESS, self.folder.write_access)

        self.assertIn(self.TARGET_ACCESS, self.sub_file.read_access)
        self.assertIn(self.TARGET_ACCESS, self.sub_file.write_access)
        self.assertNotIn(self.GROUP_ACCESS, self.sub_file.read_access)
        self.assertNotIn(self.GROUP_ACCESS, self.sub_file.write_access)
