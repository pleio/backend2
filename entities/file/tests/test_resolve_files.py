from django.core.files.base import ContentFile
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.models import PublishRequest, Subgroup
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory, PadFactory
from entities.file.models import FileFolder
from user.factories import UserFactory


class TestFileQueryOrderByAccessWeight(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.subgroup = mixer.blend(Subgroup, group=self.group, members=[self.owner])

        self.public_file = mixer.blend(
            FileFolder,
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.public],
            group=self.group,
            title="public",
        )
        self.authentic_file = mixer.blend(
            FileFolder,
            read_access=[ACCESS_TYPE.logged_in],
            write_access=[ACCESS_TYPE.logged_in],
            group=self.group,
            title="logged_in",
        )
        self.subgroup_file = mixer.blend(
            FileFolder,
            read_access=[ACCESS_TYPE.subgroup.format(self.subgroup.access_id)],
            write_access=[ACCESS_TYPE.subgroup.format(self.subgroup.access_id)],
            group=self.group,
            title="subgroup",
        )
        self.group_file = mixer.blend(
            FileFolder,
            read_access=[ACCESS_TYPE.group.format(self.group.id)],
            write_access=[ACCESS_TYPE.group.format(self.group.id)],
            group=self.group,
            title="group",
        )
        self.user_file = mixer.blend(
            FileFolder,
            read_access=[ACCESS_TYPE.user.format(self.owner.id)],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
            group=self.group,
            title="private",
        )
        self.query = """
            query FilesQuery(
                    $containerGuid: String
                    $orderBy: String
                    $orderDirection: String
                    $filterBookmarks: Boolean) {
                files(
                        containerGuid: $containerGuid
                        orderBy: $orderBy
                        orderDirection: $orderDirection
                        filterBookmarks: $filterBookmarks) {
                    total
                    edges {
                        guid
                    }
                }
            }
        """

    def test_read_access_weight_of_files(self):
        variables = {
            "containerGuid": self.group.guid,
            "orderBy": "readAccessWeight",
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        actual_order = [record["guid"] for record in result["data"]["files"]["edges"]]
        self.assertEqual(
            actual_order,
            [
                self.user_file.guid,
                self.subgroup_file.guid,
                self.group_file.guid,
                self.authentic_file.guid,
                self.public_file.guid,
            ],
        )

    def test_read_access_weight_of_files_reverse(self):
        variables = {
            "containerGuid": self.group.guid,
            "orderBy": "readAccessWeight",
            "orderDirection": "desc",
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        actual_order = [record["guid"] for record in result["data"]["files"]["edges"]]
        self.assertEqual(
            actual_order,
            [
                self.public_file.guid,
                self.authentic_file.guid,
                self.group_file.guid,
                self.subgroup_file.guid,
                self.user_file.guid,
            ],
        )

    def test_write_access_weight_of_files(self):
        variables = {
            "containerGuid": self.group.guid,
            "orderBy": "writeAccessWeight",
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        actual_order = [record["guid"] for record in result["data"]["files"]["edges"]]
        self.assertEqual(
            actual_order,
            [
                self.user_file.guid,
                self.subgroup_file.guid,
                self.group_file.guid,
                self.authentic_file.guid,
                self.public_file.guid,
            ],
        )

    def test_write_access_weight_of_files_reverse(self):
        variables = {
            "containerGuid": self.group.guid,
            "orderBy": "writeAccessWeight",
            "orderDirection": "desc",
        }

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        actual_order = [record["guid"] for record in result["data"]["files"]["edges"]]
        self.assertEqual(
            actual_order,
            [
                self.public_file.guid,
                self.authentic_file.guid,
                self.group_file.guid,
                self.subgroup_file.guid,
                self.user_file.guid,
            ],
        )

    def test_filter_bookmarked_files(self):
        bookmarked_file = FileFactory(
            owner=self.owner, title="test123", upload=ContentFile(b"123", "test123.txt")
        )
        bookmarked_file.add_bookmark(self.owner)

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, {"filterBookmarks": True})

        self.assertEqual(
            result["data"]["files"],
            {
                "total": 1,
                "edges": [
                    {
                        "guid": bookmarked_file.guid,
                    }
                ],
            },
        )


class FilesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.folder = FileFolder.objects.create(
            title="images",
            read_access=[ACCESS_TYPE.user.format(self.owner.id)],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
            owner=self.owner,
            type=FileFolder.Types.FOLDER,
        )
        self.folder.persist_file()

        self.file1 = FileFolder.objects.create(
            owner=self.owner,
            upload=None,
            title="file1",
            type=FileFolder.Types.FILE,
            group=None,
            parent=None,
            read_access=[ACCESS_TYPE.user.format(self.owner.id)],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
        )
        self.file1.persist_file()

        self.file2 = FileFolder.objects.create(
            owner=self.owner,
            upload=None,
            title="file2",
            type=FileFolder.Types.FILE,
            group=None,
            parent=self.folder,
            read_access=[ACCESS_TYPE.user.format(self.owner.id)],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
        )
        self.file2.persist_file()

        self.file3 = FileFolder.objects.create(
            owner=self.owner,
            upload=None,
            title="file3",
            type=FileFolder.Types.FILE,
            group=self.group,
            parent=None,
            read_access=[ACCESS_TYPE.user.format(self.owner.id)],
            write_access=[ACCESS_TYPE.user.format(self.owner.id)],
        )

        self.user_pad = PadFactory(owner=self.owner, title="owner pad")
        self.user_pad.persist_file()

        self.group_pad = PadFactory(
            owner=self.owner, title="group pad", group=self.group
        )

        self.query = """
            query FilesQuery($containerGuid: String!) {
                files(containerGuid: $containerGuid) {
                    total
                    edges {
                        ... on FileFolder {
                            guid
                            title
                            __typename
                        }
                    }
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_user_container(self):
        variables = {"containerGuid": self.owner.guid}

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["files"]["total"], 3, msg=data["files"]["edges"])

        self.assertEqual(
            {file["guid"] for file in data["files"]["edges"]},
            {self.folder.guid, self.user_pad.guid, self.file1.guid},
        )

    def test_folder_container(self):
        variables = {"containerGuid": self.folder.guid}

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["files"]["total"], 1)
        self.assertEqual(data["files"]["edges"][0]["title"], "file2")

    def test_group_container(self):
        variables = {"containerGuid": self.group.guid}

        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, variables)

        data = result["data"]
        self.assertEqual(data["files"]["total"], 2)

        self.assertEqual(
            {file["guid"] for file in data["files"]["edges"]},
            {self.file3.guid, self.group_pad.guid},
        )


class TestFileHasModerationRequestTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.file = FileFactory(
            owner=self.owner, upload=ContentFile(b"123", "test.txt"), published=None
        )
        self.pr = PublishRequest.objects.create(
            entity=self.file,
        )

        self.query = """
            query FileQuery($guid: String!) {
                entity(guid: $guid) {
                    guid
                    ... on File {
                        publishRequest {
                            guid
                            author {
                                guid
                            }
                            assignedTo {
                                guid
                            }
                            entity {
                                guid
                            }
                            publishedAt
                            status
                        }
                    }
                }
            }
        """

        self.variables = {
            "guid": self.file.guid,
        }

    def test_open_publish_request(self):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        data = result["data"]["entity"]["publishRequest"]
        self.assertEqual(data["guid"], self.pr.guid)
        self.assertEqual(data["author"]["guid"], self.pr.author.guid)
        self.assertEqual(data["assignedTo"], None)
        self.assertEqual(data["entity"]["guid"], self.pr.entity.guid)
        self.assertEqual(data["publishedAt"], self.pr.time_published.isoformat())
        self.assertEqual(data["status"], "open")
