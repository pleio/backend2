from django.core.files.base import ContentFile
from django.utils import timezone
from django.utils.timezone import timedelta
from freezegun import freeze_time

from core.constances import ConfigurationChoices
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from entities.file.models import FileReference
from user.factories import UserFactory


class TestFileReferenceModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.file = FileFactory(
            owner=self.owner, upload=ContentFile(b"Test!\n", "testfile.txt")
        )

    def tearDown(self):
        super().tearDown()

    def test_delete_container_deletes_relation(self):
        blog = BlogFactory(owner=self.owner)
        FileReference.objects.get_or_create(file=self.file, container=blog)
        self.assertEqual(self.file.referenced_by.count(), 1)

        blog.delete()

        self.assertEqual(self.file.referenced_by.count(), 0)


class TestFileReferenceQuerySetTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()

    def test_touch_files(self):
        # Given
        start_time = timezone.now()
        with freeze_time(start_time):
            file1 = FileFactory(owner=self.owner)
            file2 = FileFactory(owner=self.owner)
            FileReference.objects.create(
                file=file1, container=BlogFactory(owner=self.owner)
            )
            reference2 = FileReference.objects.create(
                file=file2, container=BlogFactory(owner=self.owner)
            )

        # When
        new_time = timezone.now() + timedelta(days=1)
        with freeze_time(new_time):
            FileReference.objects.filter(id=reference2.id).touch_files()

        # Then
        file1.refresh_from_db()
        self.assertEqual(file1.last_action, start_time)
        file2.refresh_from_db()
        self.assertEqual(file2.last_action, new_time)

    def test_all_touch_files(self):
        start_time = timezone.now()
        with freeze_time(start_time):
            file1 = FileFactory(owner=self.owner)
            file2 = FileFactory(owner=self.owner)
            file3 = FileFactory(owner=self.owner)
            FileReference.objects.create(
                file=file1, container=BlogFactory(owner=self.owner)
            )
            FileReference.objects.create(
                file=file2, container=BlogFactory(owner=self.owner)
            )

        # When
        new_time = timezone.now() + timedelta(days=1)
        with freeze_time(new_time):
            FileReference.objects.touch_files()

        # Then
        file1.refresh_from_db()
        self.assertEqual(file1.last_action, new_time)
        file2.refresh_from_db()
        self.assertEqual(file2.last_action, new_time)
        file3.refresh_from_db()
        self.assertEqual(file3.last_action, start_time)


class TestConfigurationFileTestCase(PleioTenantTestCase):
    def build_file(self, owner):
        return FileFactory(owner=owner, upload=ContentFile(b"test content", "test.txt"))

    def test_is_configuration(self):
        owner = UserFactory(email="configuration-owner@example.com")
        for key in ConfigurationChoices._member_names_:
            with self.subTest(msg="Testing %s" % key):
                file = self.build_file(owner)
                ref = FileReference.objects.update_configuration(key, file)
                self.assertEqual(file.is_configuration_file, True)
                self.assertEqual(ref.is_configuration, True)

    def test_is_not_configuration(self):
        file = self.build_file(UserFactory())
        ref = FileReference.objects.persist_file(file)
        self.assertEqual(file.is_configuration_file, False)
        self.assertEqual(ref.is_configuration, False)


class TestAvatarFileTestCase(PleioTenantTestCase):
    def build_file(self, owner):
        return FileFactory(owner=owner, upload=ContentFile(b"test content", "test.txt"))

    def test_avatar(self):
        file = self.build_file(UserFactory())
        ref = FileReference.objects.update_avatar_file(file.owner, file)
        self.assertEqual(file.is_avatar_file, True)
        self.assertEqual(ref.is_avatar, True)

    def test_not_avatar_file(self):
        file = self.build_file(UserFactory())
        ref = FileReference.objects.persist_file(file)
        self.assertEqual(file.is_avatar_file, False)
        self.assertEqual(ref.is_avatar, False)

    def test_not_yet_avatar_file(self):
        file = self.build_file(UserFactory())
        self.assertEqual(file.is_avatar_file, False)
