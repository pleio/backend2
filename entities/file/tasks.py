from __future__ import absolute_import, unicode_literals

import os
import re
import statistics
from datetime import timedelta
from math import ceil
from tempfile import NamedTemporaryFile

import celery.exceptions
import textract
from celery import chord, shared_task, signature
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.utils import timezone
from django.utils.timezone import now
from django.utils.translation import gettext
from django_tenants.utils import schema_context

from control.models import FileOperationLog
from core.lib import Distribute, datetime_format, get_file_extension
from core.utils import clamav
from core.utils.entity import load_entity_by_id
from core.utils.tiptap_parser import Tiptap
from entities.file.helpers.images import resize_and_update_image
from entities.file.helpers.post_processing import ensure_correct_file_without_signals
from entities.file.mail_builders.file_scan_found import schedule_file_scan_found_mail
from entities.file.models import (
    EXTRACT_FILE_CONTENT_MIME_TYPES,
    FileFolder,
    ScanIncident,
)
from entities.file.validators import is_upload_complete
from tenants.models import Client
from user.models import User

logger = get_task_logger(__name__)


@shared_task
def resize_featured(schema_name, file_guid):
    """
    Resize featured image for tenant
    """
    with schema_context(schema_name):
        try:
            image = FileFolder.objects.get(id=file_guid)
            resize_and_update_image(image, 1200, 2000)
        except Exception as e:
            logger.error("resize_featured %s %s: %s", schema_name, file_guid, e)


@shared_task
def schedule_scan_finished(schema_name):
    with schema_context(schema_name):
        incidents = ScanIncident.objects.filter(
            file_created__gte=timezone.now() - timezone.timedelta(days=1)
        )

        virus_count = incidents.filter(is_virus=True).count()
        error_count = incidents.filter(is_virus=False).count()

        if incidents.count():
            for admin_user in User.objects.filter(is_superadmin=True):
                schedule_file_scan_found_mail(
                    virus_count=virus_count, error_count=error_count, admin=admin_user
                )

        logger.info("Scanned found %i incidents @%s", incidents.count(), schema_name)


@shared_task
def schedule_scan_all_tenants():
    scheduler = FileScanScheduler(distance=50)
    scheduler.schedule_scan()


class FileScanScheduler:
    def __init__(self, distance):
        self.distance = distance
        self.instances = []
        self.signatures = []

    def schedule_scan(self):
        self._collect_files()
        self._shuffle_files()
        self._create_signatures()
        self._schedule_signatures()

    def _collect_files(self):
        self.instances = []
        for tenant in Client.objects.active_clients():
            with schema_context(tenant.schema_name):
                all_files = FileFolder.objects.filter_files()
                limit = ceil(all_files.count() / int(settings.SCAN_CYCLE_DAYS))
                for pk, size in all_files.values_list("pk", "size")[:limit]:
                    self.instances.append(
                        {
                            "schema_name": tenant.schema_name,
                            "file_guid": str(pk),
                            "file_size": size,
                        }
                    )

    def _shuffle_files(self):
        self.instances = [*sorted(self.instances, key=lambda x: x["file_size"])]
        self.instances = [*Distribute(self.instances, distance=self.distance).spread()]

    def _create_signatures(self):
        start = now() + timedelta(minutes=1)
        mean = statistics.mean([i["file_size"] for i in self.instances])
        deviation = statistics.stdev([i["file_size"] for i in self.instances])
        for task in self.instances:
            start = start + timedelta(seconds=1)
            self.signatures.append(
                signature(
                    scan_file, args=(task["schema_name"], task["file_guid"]), eta=start
                )
            )

            # Give bigger files more time.
            if task["file_size"] > (mean + deviation):
                FileOperationLog.objects.create(
                    client=Client.objects.get(schema_name=task["schema_name"]),
                    operation="large_files_extra_time",
                    result={"task": task, "eta": datetime_format(start, seconds=True)},
                )
                start = start + timedelta(seconds=5)

    def _schedule_signatures(self):
        from core.tasks import dispatch_task

        close_task = signature(
            dispatch_task, args=("entities.file.tasks.schedule_scan_finished",)
        )
        chord(self.signatures, close_task).apply_async()

    def generate_tasks(self):
        reference = timezone.now()
        for count_down, file_id in enumerate([*self.collect_files()]):
            offset_seconds = self.file_offset + count_down
            yield signature(
                scan_file,
                args=(self.schema_name, str(file_id)),
                eta=(reference + timezone.timedelta(seconds=offset_seconds)),
            )


@shared_task(bind=True, rate_limit="30/m", max_retries=5)
def scan_file(self, schema_name, file_id):
    try:
        with schema_context(schema_name):
            file = FileFolder.objects.filter(id=file_id).first()
            if not file or not file.is_file():
                return

            if not file.upload.name or not default_storage.exists(file.upload.name):
                file.blocked = True
                file.block_reason = gettext("File not found on file storage")
                file.save()
                return

            logger.info(
                "At %s Scan file %s, last scanned %s",
                schema_name,
                os.path.basename(file.upload.name),
                datetime_format(file.last_scan),
            )
            file.last_scan = timezone.now()

            try:
                clamav.scan(file.upload.name)
                file.blocked = False
                file.block_reason = None
                file.save()
                return True
            except clamav.FileScanError as e:
                ScanIncident.objects.create_from_file_folder(e, file)
                if not e.is_virus():
                    self.retry(eta=now() + timedelta(minutes=15))
                else:
                    file.blocked = True
                    file.block_reason = gettext("This file contains a virus")
                    file.save()
                return False

    except celery.exceptions.Retry:
        # Allow retry
        raise

    except Exception as e:
        # Make sure the task exits OK.
        return str(e)


@shared_task(autoretry_for=(AssertionError,), retry_backoff=10, max_retries=10)
def post_process_file_attributes(schema_name, instance_id):
    with schema_context(schema_name):
        instance = FileFolder.objects.get(id=instance_id)

        ensure_correct_file_without_signals(instance)

        if not is_upload_complete(instance):
            msg = f"{instance_id}@{schema_name} still not complete."
            raise AssertionError(msg)


@shared_task
def delete_from_referencing_entities(schema_name, file_guid, entity_guid):
    with schema_context(schema_name):
        try:
            entity = load_entity_by_id(entity_guid, ["core.Entity", "core.Group"])
            assert hasattr(entity, "map_rich_text_fields")

            def _update_removed_file_details(rich_description):
                parser = Tiptap(rich_description)
                parser.update_file_attrs(file_guid, {"notAvailable": True})
                parser.update_image_attrs(file_guid, {"notAvailable": True})
                return parser.as_string()

            entity.map_rich_text_fields(_update_removed_file_details)
            entity.save()
        except (ObjectDoesNotExist, AssertionError):
            pass


@shared_task
def update_referencing_entities(schema_name, file_guid, entity_guid):
    with schema_context(schema_name):
        try:
            entity = load_entity_by_id(entity_guid, ["core.Entity", "core.Group"])
            assert hasattr(entity, "map_rich_text_fields")
            file: FileFolder = FileFolder.objects.get(pk=file_guid)

            def _update_file_details(rich_description):
                parser = Tiptap(rich_description)
                parser.update_file_attrs(
                    file_guid,
                    {
                        "name": file.title,
                        "notAvailable": False,
                        "mimeType": file.mime_type,
                        "url": file.url,
                    },
                )
                return parser.as_string()

            entity.map_rich_text_fields(_update_file_details)
            entity.save()

        except (ObjectDoesNotExist, AssertionError):
            pass


@shared_task
def post_process_file_contents(schema_name, file_guid):
    def _extract_file_contents(instance):
        temp_file = NamedTemporaryFile(
            delete=True,
            suffix=get_file_extension(instance.upload.name),
        )
        with instance.upload.open("rb") as f:
            temp_file.write(f.read())
        temp_file.seek(0)

        file_contents = re.sub(
            r"\s+",
            " ",
            textract.process(temp_file.name, encoding="utf8").decode("utf-8"),
        )
        temp_file.close()

        return file_contents[:500000]  # ~500kb

    with schema_context(schema_name):
        try:
            instance = FileFolder.objects.get(id=file_guid)
            if instance.mime_type not in EXTRACT_FILE_CONTENT_MIME_TYPES:
                return
            if instance.size > 100 * 1024 * 1024:  # 100 MB
                return
            instance.file_contents = _extract_file_contents(instance)
            instance.save()
        except Exception as e:
            logger.error(
                "Error occured while updating file contents for %s.%s: %s",
                schema_name,
                file_guid,
                e,
            )
