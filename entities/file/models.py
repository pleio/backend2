import logging
import os
from hashlib import md5

from auditlog.registry import auditlog
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db import models
from django.db.models import ObjectDoesNotExist
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.utils.timezone import timedelta
from django.utils.translation import gettext_lazy as _
from model_utils.managers import InheritanceQuerySet
from mutagen import MutagenError
from mutagen.mp3 import MP3, HeaderNotFoundError

from core import config
from core.constances import (
    ACCESS_TYPE,
    DOWNLOAD_AS_OPTIONS,
    ENTITY_STATUS,
    PERSONAL_FILE,
    ConfigurationChoices,
)
from core.lib import (
    generate_object_filename,
    get_basename,
    get_file_checksum,
    get_mimetype,
    tenant_schema,
)
from core.models import (
    BookmarkMixin,
    Comment,
    Entity,
    NotificationMixin,
    RevisionMixin,
    Tag,
)
from core.models.entity import EntityManager
from core.models.group import Group
from core.models.image import ResizedImageMixin
from core.models.mixin import (
    HasMediaMixin,
    HierarchicalEntityMixin,
    ModelWithFile,
    TitleMixin,
)
from core.models.rich_fields import AttachmentMixin
from core.models.tags import EntityTag
from core.utils import clamav
from core.utils.access import get_read_access_weight, get_write_access_weight
from core.utils.convert import tiptap_to_html, tiptap_to_text
from core.utils.export.content import ContentSnapshot
from entities.file.validators import is_upload_complete

logger = logging.getLogger(__name__)


def read_access_default():
    return []


def write_access_default():
    return []


EXTRACT_FILE_CONTENT_MIME_TYPES = [
    "application/pdf",
    "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "text/plain",
]


class FileReferenceQuerySet(models.QuerySet):
    def exclude_personal_references(self):
        return self.exclude(configuration=PERSONAL_FILE)

    def filter_entities(self):
        return self.filter(container_fk__isnull=False)

    def is_personal_file(self):
        return self.personal_references().exists()

    def filter_configuration(self):
        return self.filter(
            configuration__isnull=False,
            configuration__in=ConfigurationChoices._member_names_,
        )

    def filter_avatar(self):
        return self.filter(configuration__startswith="avatar:")

    def personal_references(self):
        return self.filter(configuration=PERSONAL_FILE)

    def touch_files(self):
        return FileFolder.objects.filter(
            id__in=[*self.values_list("file_id", flat=True)]
        ).update(last_action=timezone.now())


class FileReferenceManager(models.Manager):
    def get_queryset(self):
        return FileReferenceQuerySet(self.model, using=self._db)

    def persist_file(self, file):
        ref, created = super().get_or_create(
            file=file, container_ct=None, container_fk=None, configuration=PERSONAL_FILE
        )
        return ref

    def update_configuration(self, configuration, file):
        self.get_queryset().filter(configuration=configuration).delete()

        if not file:
            return

        ref = super().create(configuration=configuration, file_id=file.id)

        if file.refresh_read_access():
            file.save()

        return ref

    def update_avatar_file(self, user, file):
        key = "avatar:%s" % user.guid
        self.get_queryset().filter(configuration=key).delete()

        if not file:
            return

        if file.refresh_read_access():
            file.save()

        return super().create(configuration=key, file_id=file.id)

    def get_or_create(self, container=None, **kwargs):
        if container:
            return self._get_or_create_by_container_id(container, **kwargs)
        return super().get_or_create(**kwargs)

    def _get_or_create_by_container_id(self, container, **kwargs):
        try:
            return self.get(container_fk=container.id, **kwargs), False
        except FileReference.DoesNotExist:
            return self.create(container=container, **kwargs), True

    def exclude_personal_references(self):
        return self.get_queryset().exclude_personal_references()

    def filter_entities(self):
        return self.get_queryset().filter_entities()

    def personal_references(self):
        return self.get_queryset().personal_references()

    def is_personal_file(self):
        return self.get_queryset().is_personal_file()

    def filter_configuration(self):
        return self.get_queryset().filter_configuration()

    def filter_avatar(self):
        return self.get_queryset().filter_avatar()

    def touch_files(self):
        return self.get_queryset().touch_files()


class FileReference(models.Model):
    """
    Files without FileFolder.group filled in or a FileReference object that refers to it get removed
    from the system automatically by core.tasks.cleanup_orphaned_files.

    So if the file that is created by any code in pleio should be available for longer time then
    one month keep this in mind:

    * Files may be personal.
        Add a FileReference with FileReference.configuration = core.constances.PERSONAL_FILE.
    * Files may be used in configuration.
        Use FileReference.objects.update_configuration with one of core.constances.ConfigurationChoices.
    * Files may be attached to other content in widgets or rich text.
        The core.models.rich_fields.AttachmentMixin manages FileReference objects with container filled in.
    * Files may be used as cover image.
        These files both have a featured_image relation (explicitly set) and a FileReference relation (managed
        by AttachmentMixin).
    * Files may be in a group
        Assign a group to FileFolder.group.
    """

    objects = FileReferenceManager()

    created_at = models.DateTimeField(default=timezone.now)

    file = models.ForeignKey(
        "file.FileFolder",
        blank=False,
        null=False,
        on_delete=models.CASCADE,
        related_name="referenced_by",
    )
    container_ct = models.ForeignKey(
        ContentType, blank=True, null=True, on_delete=models.CASCADE
    )
    container_fk = models.UUIDField(blank=True, null=True)
    container = GenericForeignKey(ct_field="container_ct", fk_field="container_fk")

    configuration = models.CharField(max_length=256, blank=True, null=True)

    def delete(self, using=None, keep_parents=False):
        file = self.file
        super().delete(using=using, keep_parents=keep_parents)

        if file.refresh_read_access():
            file.save()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if self.file.refresh_read_access():
            self.file.save()

    @property
    def is_entity(self):
        return isinstance(self.container, (Entity, Comment))

    @property
    def is_configuration(self):
        return (
            self.configuration
            and self.configuration in ConfigurationChoices._member_names_
        )

    @property
    def is_avatar(self):
        return isinstance(self.configuration, str) and self.configuration.startswith(
            "avatar:"
        )

    @property
    def is_group_icon(self):
        return (
            isinstance(self.container, Group) and self.container.icon_id == self.file_id
        )

    def __str__(self):
        return f"{self.file}@{self.container}"


class FileFolderQuerySet(InheritanceQuerySet):
    def filter_files(self):
        return self.filter(type=FileFolder.Types.FILE)

    def filter_pads(self):
        return self.filter(type=FileFolder.Types.PAD)

    def filter_orphaned_files(self):
        qs = self.filter_files()
        return qs.filter(group__isnull=True, referenced_by__isnull=True)

    def filter_attachments(self):
        qs = self.filter_files()
        return qs.filter(group__isnull=True)


class FileFolderManager(EntityManager):
    def get_queryset(self):
        return FileFolderQuerySet(self.model, using=self._db)

    def file_by_path(self, path):
        for maybe_guid in path.split("/"):
            try:
                qs = self.get_queryset().filter(
                    pk=maybe_guid, type=FileFolder.Types.FILE
                )
                if qs.exists():
                    return qs.first()
            except Exception:
                pass
        return None

    def content_snapshots(self, user):
        tags = list(Tag.translate_tags([ContentSnapshot.EXCLUDE_TAG]))
        all_snapshots = EntityTag.objects.filter(tag__label__in=tags).values_list(
            "entity_id", flat=True
        )
        qs = self.visible(user=user)
        qs = qs.filter(owner=user)
        qs = qs.filter(id__in=all_snapshots)
        return qs.order_by("-created_at")

    def filter_files(self):
        return self.get_queryset().filter_files()

    def filter_pads(self):
        return self.get_queryset().filter_pads()

    def filter_orphaned_files(self):
        return self.get_queryset().filter_orphaned_files()

    def filter_attachments(self):
        return self.get_queryset().filter_attachments()


class FileFolder(
    HierarchicalEntityMixin,
    HasMediaMixin,
    BookmarkMixin,
    NotificationMixin,
    TitleMixin,
    ModelWithFile,
    ResizedImageMixin,
    AttachmentMixin,
    RevisionMixin,
    Entity,
):
    """
    @NOTE when create a FileFolder object: Keep in mind the documentation at file.models.FileReference.
    """

    class Types(models.TextChoices):
        FILE = "File", _("File")
        FOLDER = "Folder", _("Folder")
        PAD = "Pad", _("Pad")

    objects = FileFolderManager()

    parent = models.ForeignKey(
        "self", blank=True, null=True, related_name="children", on_delete=models.CASCADE
    )

    type = models.CharField(max_length=36, choices=Types.choices, default=Types.FILE)

    upload = models.FileField(
        upload_to=generate_object_filename, blank=True, null=True, max_length=512
    )
    thumbnail = models.FileField(upload_to="thumbnails/", blank=True, null=True)
    checksum = models.CharField(max_length=32, blank=True, null=True)

    mime_type = models.CharField(null=True, blank=True, max_length=100)
    size = models.IntegerField(default=0)

    last_scan = models.DateTimeField(default=timezone.now)
    last_download = models.DateTimeField(default=None, null=True)

    blocked = models.BooleanField(default=False)
    block_reason = models.CharField(max_length=255, null=True, blank=True)

    read_access_weight = models.IntegerField(default=0)
    write_access_weight = models.IntegerField(default=0)

    rich_description = models.TextField(null=True, blank=True)
    pad_state = models.TextField(null=True, blank=True)
    duration = models.DurationField(blank=True, null=True)

    file_contents = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.type}[{self.title}]"

    @property
    def type_to_string(self):
        return self.type.lower()

    @property
    def hide_owner(self):
        if self.is_folder():
            return False
        return "file" in config.HIDE_CONTENT_OWNER

    @property
    def url(self):
        prefix = ""

        if self.type == self.Types.FOLDER:
            if self.group:
                prefix = self.group.url
            elif self.is_personal_file:  # personal file browser url
                prefix = "/user/{}".format(self.owner.guid)

            return "{}/files/{}".format(prefix, self.guid).lower()

        return "{}/files/view/{}/{}".format(prefix, self.guid, self.slug).lower()

    @property
    def slug(self):
        if self.type == self.Types.FILE:
            return os.path.basename(self.upload.name) or super().slug
        return super().slug

    @property
    def download_url(self):
        if self.type != self.Types.FILE:
            return None
        return reverse("download", args=[self.id, self.slug or "undefined"])

    @property
    def attachment_url(self):
        return reverse(
            "attachment", kwargs={"attachment_id": self.id, "attachment_type": "entity"}
        )

    @property
    def embed_url(self):
        if self.type != self.Types.FILE:
            return None
        return reverse("embed", args=[self.id, self.slug or "undefined"])

    @property
    def thumbnail_url(self):
        if self.type != self.Types.FILE:
            return None
        checksum = (
            "?check=%s" % md5(self.upload.name.encode()).hexdigest()[:12]
            if self.upload.name
            else ""
        )
        return reverse("thumbnail", args=[self.id]) + checksum

    @property
    def file_fields(self):
        return [self.thumbnail, self.upload]

    @property
    def should_notify_on_create(self):
        return not self.is_folder() and self.group and self.group.file_notifications

    @property
    def upload_field(self):
        return self.upload

    @property
    def mime_type_field(self):
        return self.mime_type

    @property
    def description(self):
        return tiptap_to_text(self.rich_description)

    @property
    def rich_fields(self):
        return [self.rich_description]

    @property
    def root_container(self):
        if self.is_personal_file:
            return self.owner
        if self.group:
            return self.group
        return None

    @property
    def download_as_options(self):
        if self.type != self.Types.PAD:
            return None
        download_as_options = []
        for option in DOWNLOAD_AS_OPTIONS:
            download_as_options.append(
                {
                    "type": option,
                    "url": "/download_rich_description_as/{}/{}".format(
                        self.guid, option
                    ),
                }
            )
        return download_as_options

    def can_read(self, user):
        if (
            config.CONTENT_MODERATION_ENABLED
            and self.type_to_string in config.REQUIRE_CONTENT_MODERATION_FOR
            and self.status_published == ENTITY_STATUS.DRAFT
        ):
            return user.is_authenticated and (user == self.owner or user.is_editor)

        return super().can_read(user)

    def has_revisions(self):
        return self.is_pad()

    def is_referenced(self):
        if self.group:
            return True
        if self.referenced_by.count() > 0:
            return True
        return False

    def is_featured_image(self):
        for file_id in [
            getattr(r.container, "featured_image_id", None)
            for r in self.referenced_by.all()
            if r.container
        ]:
            if file_id == self.id:
                return True
        return False

    def cleanup_featured_image(self):
        for container in [r.container for r in self.referenced_by.all() if r.container]:
            if getattr(container, "featured_image_id", None) == self.id:
                container.featured_image = None
                container.save()

    def has_hard_references(self):
        return self.referenced_by.exclude_personal_references().exists()

    def has_soft_references(self):
        return bool(self.is_personal_file or self.group)

    def remove_soft_references(self):
        self.group = None
        self.referenced_by.personal_references().delete()

    @property
    def is_personal_file(self):
        return self.referenced_by.is_personal_file()

    @property
    def is_configuration_file(self):
        return self.referenced_by.filter_configuration().exists()

    @property
    def is_avatar_file(self):
        return self.referenced_by.filter_avatar().exists()

    def refresh_read_access(self):  # noqa: C901
        if self.group or self.is_personal_file or not self.referenced_by.exists():
            return False

        new_read_access = {ACCESS_TYPE.user.format(self.owner.guid)}
        try:
            for referencing in self.referenced_by.all():
                if (
                    referencing.is_configuration
                    or referencing.is_avatar
                    or referencing.is_group_icon
                ):
                    new_read_access = [ACCESS_TYPE.public]
                    break
                try:
                    if referencing.container:
                        for ac in referencing.container.get_read_access():
                            new_read_access.add(ac)
                except AttributeError as e:
                    logger.error(
                        "Error while trying to get_read_access: container_fk=%s, container_ct=%s, error=%s",
                        referencing.container_fk,
                        referencing.container_ct,
                        e,
                    )
        except ValueError:
            pass

        if ACCESS_TYPE.public in new_read_access:
            new_read_access = {
                ACCESS_TYPE.public,
            }
        elif ACCESS_TYPE.logged_in in new_read_access:
            new_read_access = {
                ACCESS_TYPE.logged_in,
            }

        if new_read_access != {*self.read_access}:
            self.read_access = [*new_read_access]
            return True
        return False

    def get_media_status(self):
        if self.type == self.Types.PAD:
            return bool(self.rich_description)
        if self.type == self.Types.FILE:
            return bool(self.upload.name and default_storage.exists(self.upload.name))
        return False

    def get_media_filename(self):
        if self.upload.name:
            return "%s/%s" % (self.pk, os.path.basename(self.upload.name))
        if self.type == self.Types.PAD:
            return "%s.html" % self.slug
        return None

    def clean_filename(self):
        if not self.upload:
            return self.title
        # Take the extension from the diskfile, and the filename from self.title
        _, ext = os.path.splitext(self.upload.name)
        basename, _ = os.path.splitext(self.title)
        return slugify(basename) + ext.lower()

    def original_filename(self):
        try:
            return str(os.path.basename(self.upload.name))
        except Exception:
            return ""

    def get_media_content(self):
        if self.type == self.Types.FILE and not self.blocked:
            with self.upload.open() as fh:
                return fh.read()
        if self.type == self.Types.PAD:
            return tiptap_to_html(self.rich_description)
        return None

    def persist_file(self):
        FileReference.objects.persist_file(file=self)

    def save(self, *args, **kwargs):
        self.ensure_owner_read_access()
        self.ensure_owner_write_access()
        self.update_metadata()
        should_update_file_contents = self._should_update_file_contents()
        super(FileFolder, self).save(*args, **kwargs)
        if not is_upload_complete(self):
            from entities.file.tasks import post_process_file_attributes

            post_process_file_attributes.delay(tenant_schema(), str(self.id))
        if should_update_file_contents:
            from entities.file.tasks import post_process_file_contents

            post_process_file_contents.delay(tenant_schema(), str(self.id))

    def ensure_owner_read_access(self):
        if not self.read_access:
            self.read_access = [ACCESS_TYPE.user.format(self.owner.guid)]

    def ensure_owner_write_access(self):
        if not self.write_access:
            self.write_access = [ACCESS_TYPE.user.format(self.owner.guid)]

    def scan(self):
        try:
            if self.is_file():
                FileFolder.objects.filter(id=self.id).update(last_scan=timezone.now())
                clamav.scan(self.upload.name)
            return True
        except AttributeError:
            return False
        except clamav.FileScanError as e:
            ScanIncident.objects.create_from_file_folder(e, self)
            return not e.is_virus()

    def delete(self, *args, **kwargs):
        self.cleanup_extra_file()
        super(FileFolder, self).delete(*args, **kwargs)

    def update_metadata(self):
        if self.upload:
            self._update_type_size()
        self._update_title()
        self._update_checksum()
        self._update_duration()
        self._update_file_contents()
        self.read_access_weight = get_read_access_weight(self)
        self.write_access_weight = get_write_access_weight(self)

    def _update_title(self):
        if not self.title and self.upload:
            self.title = get_basename(self.upload.name)

    def _update_type_size(self):
        try:
            self.mime_type = get_mimetype(self.upload.name)
            self.size = self.upload.size
        except FileNotFoundError:
            pass

    def _update_checksum(self):
        if self.checksum:
            return
        if not self.group and self.is_image():
            self.checksum = get_file_checksum(self.upload)
        else:
            from core.tasks.misc import update_file_checksum

            try:
                update_file_checksum.delay(tenant_schema(), self.guid)
            except Exception:
                pass

    def _update_duration(self):
        if not (
            self.upload
            and self.mime_type
            and self.mime_type in ["audio/mpeg", "audio/mp3"]
        ):
            return None
        try:
            audio = MP3(self.upload.open())
            duration_in_seconds = int(audio.info.length)
            self.duration = timedelta(seconds=duration_in_seconds)
        except (HeaderNotFoundError, MutagenError) as e:
            logger.error("Error while calculating duration: %s", e)

    def _update_file_contents(self):
        if self.mime_type not in EXTRACT_FILE_CONTENT_MIME_TYPES:
            self.file_contents = None

    def update_updated_at(self):
        """Needs to be executed before save so we can compare if the File or Folder moved to a new parent and also update those dates"""
        self.updated_at = timezone.now()
        set_parent_folders_updated_at(self)

        try:
            # Also update old parent if changed
            old_instance = FileFolder.objects.get(id=self.id)
            if old_instance.parent != self.parent:
                set_parent_folders_updated_at(old_instance)
        except ObjectDoesNotExist:
            pass

    def cleanup_extra_file(self):
        if self.type != FileFolder.Types.FILE:
            return

        try:
            if self.upload and self.upload.name:
                default_storage.delete(self.upload.name)
        except (FileNotFoundError, ValueError):
            pass

    def get_content(self, wrap=None):
        if default_storage.exists(self.upload.name):
            with self.upload.open() as fh:
                data = fh.read()
                if callable(wrap):
                    return wrap(data)
                return data
        return None

    def make_copy(self, user):
        new = FileFolder()
        new_file = ContentFile(self.upload.read())
        new_file.name = self.clean_filename()
        new.upload = new_file
        new.owner = user
        new.save()

        return new

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)

    def serialize(self):
        return {
            "title": self.title,
            "file": self.upload.name,
            "mimeType": self.mime_type,
            "size": self.size,
            "richDescription": self.rich_description,
            "parentGuid": str(self.parent_id) if self.parent else None,
            **super().serialize(),
        }

    def is_file(self):
        return self.type == self.Types.FILE

    def is_folder(self):
        return self.type == self.Types.FOLDER

    def is_pad(self):
        return self.type == self.Types.PAD

    def featured_image_at(self):
        for field in self._meta.get_fields():
            if (
                not field.auto_created
                or field.concrete
                or field.remote_field.name != "featured_image"
            ):
                continue
            query_set = getattr(self, f"{field.name}_set", None) or getattr(
                self, field.name, FileReference.objects.none()
            )
            yield from query_set.all()

    def _should_update_file_contents(self):
        """Check if the upload has changed (before save)."""
        if self.is_file() and self.mime_type in EXTRACT_FILE_CONTENT_MIME_TYPES:
            if self._state.adding:
                return True
            try:
                old_instance = FileFolder.objects.get(id=self.id)
                if old_instance.upload != self.upload:
                    return True
            except ObjectDoesNotExist:
                logger.error(
                    "FileFolder._should_update_file_contents: old file instance not found %s.%s",
                    tenant_schema(),
                    self.id,
                )
        return False

    def all_children_and_me(self):
        for child in self.get_children():
            yield from child.all_children_and_me()
        yield self

    def has_children(self):
        return self.children.exists()

    def get_children(self):
        return self.children.all()

    def get_parent(self):
        return self.parent


class ScanIncidentManager(models.Manager):
    def create_from_attachment(self, e, attachment):
        self.create(
            message=e.feedback,
            is_virus=e.is_virus(),
            file_group=attachment.group,
            file_created=attachment.created_at,
            file_title=attachment.upload.name,
            file_mime_type=get_mimetype(attachment.upload.name),
            file_owner=attachment.owner,
        )

    def create_from_file_folder(self, e, file_folder):
        self.create(
            message=e.feedback,
            is_virus=e.is_virus(),
            file_group=file_folder.group,
            file_created=file_folder.created_at,
            file_title=file_folder.upload.name,
            file_mime_type=get_mimetype(file_folder.upload.name),
            file_owner=file_folder.owner,
        )


class ScanIncident(models.Model):
    objects = ScanIncidentManager()

    date = models.DateTimeField(default=timezone.now)
    message = models.CharField(max_length=256)
    file = models.ForeignKey(
        "file.FileFolder",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="scan_incidents",
    )
    file_created = models.DateTimeField(default=timezone.now)
    file_group = models.ForeignKey(
        "core.Group", blank=True, null=True, on_delete=models.SET_NULL
    )
    file_title = models.CharField(max_length=256)
    file_mime_type = models.CharField(null=True, blank=True, max_length=100)
    file_owner = models.ForeignKey(
        "user.User", blank=True, null=True, on_delete=models.SET_NULL
    )
    is_virus = models.BooleanField(default=False)

    class Meta:
        ordering = ("-date",)


def set_parent_folders_updated_at(instance):
    if instance == instance.parent:
        return
    if instance.parent and instance.parent.type == FileFolder.Types.FOLDER:
        instance.parent.updated_at = timezone.now()
        instance.parent.save()
        set_parent_folders_updated_at(instance.parent)


auditlog.register(FileFolder)
