import logging

from django.db.models import Q
from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl.registries import registry

from core.documents import DefaultDocument, custom_analyzer

from .models import FileFolder

logger = logging.getLogger(__name__)


class Wrapper:
    class BaseFileDocument(DefaultDocument):
        id = fields.KeywordField()
        is_archived = fields.BooleanField()
        tags = fields.ListField(fields.TextField(fields={"raw": fields.KeywordField()}))
        tags_matches = fields.ListField(
            fields.TextField(fields={"raw": fields.KeywordField()})
        )
        category_tags = fields.ListField(
            fields.KeywordField(attr="category_tags_index")
        )

        read_access = fields.ListField(fields.KeywordField())
        type = fields.KeywordField()
        title = fields.TextField(
            analyzer=custom_analyzer,
            search_analyzer="standard",
            boost=2,
            fields={"raw": fields.KeywordField()},
        )
        file_contents = fields.TextField(
            analyzer=custom_analyzer, search_analyzer="standard"
        )

        read_access_weight = fields.IntegerField()

        description = fields.TextField(
            analyzer=custom_analyzer, search_analyzer="standard"
        )

        def prepare_type(self, instance):
            return instance.type_to_string

        def prepare_tags(self, instance):
            return [x.lower() for x in instance.tags]

        def prepare_description(self, instance):
            return instance.description_index_value()

        def _file_filter(self):
            return ~Q(blocked=True) & (
                Q(
                    referenced_by__configuration=None,
                    referenced_by__container_fk__isnull=False,
                )
                | Q(group__isnull=False)
            )

        def should_index_object(self, obj):
            return self.get_queryset().filter(id=obj.id).exists()

        class Django:
            model = FileFolder

            fields = ["created_at", "updated_at", "published"]


@registry.register_document
class FileDocument(Wrapper.BaseFileDocument):
    class Index:
        name = "file"

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(self._file_filter() & ~Q(mime_type__startswith="image/"))
        return qs


@registry.register_document
class ImageDocument(Wrapper.BaseFileDocument):
    class Index:
        name = "image"

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(self._file_filter() & Q(mime_type__startswith="image/"))
        return qs

    def prepare_type(self, instance):
        return "image"
