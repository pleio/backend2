import logging

from celery import chord
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from post_deploy import post_deploy_action

from core.constances import ACCESS_TYPE
from core.lib import is_schema_public, tenant_schema
from core.models import Group
from core.tasks import elasticsearch_install_new_indices, elasticsearch_rebuild_all
from entities.file.models import (
    EXTRACT_FILE_CONTENT_MIME_TYPES,
    FileFolder,
    FileReference,
)

logger = logging.getLogger(__name__)


@post_deploy_action
def fix_broken_filenames():
    if is_schema_public():
        return

    retry_files = FileFolder.objects.filter(
        Q(title__isnull=True)
        | Q(title__startswith="/")
        | Q(mime_type__isnull=True)
        | Q(size__lt=1)
    ).filter(type=FileFolder.Types.FILE)

    for file in retry_files:
        from entities.file.tasks import post_process_file_attributes

        post_process_file_attributes.delay(tenant_schema(), str(file.id))


@post_deploy_action
def fix_hidden_avatars():
    if is_schema_public():
        return

    for reference in FileReference.objects.filter(configuration__startswith="user"):
        if reference.file.refresh_read_access():
            reference.file.save()


@post_deploy_action
def fix_attachments_with_multiple_acl_choices():
    if is_schema_public():
        return

    for file_id, read_access in FileFolder.objects.filter_files().values_list(
        "id", "read_access"
    ):
        new_read_access = {*read_access}

        if ACCESS_TYPE.public in new_read_access:
            new_read_access = {
                ACCESS_TYPE.public,
            }
        elif ACCESS_TYPE.logged_in in new_read_access:
            new_read_access = {
                ACCESS_TYPE.logged_in,
            }

        if new_read_access != {*read_access}:
            FileFolder.objects.filter(pk=file_id).update(read_access=[*new_read_access])


@post_deploy_action
def fix_hidden_avatars_again_and_other_configuration_files():
    if is_schema_public():
        return

    for reference in FileReference.objects.filter(
        configuration__isnull=False
    ).exclude_personal_references():
        if reference.file.refresh_read_access():
            reference.file.save()


@post_deploy_action
def update_file_contents():
    if is_schema_public():
        return

    from entities.file.tasks import post_process_file_contents

    qs = FileFolder.objects.exclude(blocked=True).filter(
        Q(
            mime_type__in=EXTRACT_FILE_CONTENT_MIME_TYPES,
        )
        & (
            Q(
                referenced_by__configuration=None,
                referenced_by__container_fk__isnull=False,
            )
            | Q(group__isnull=False)
        )
    )

    for file in qs.iterator(chunk_size=1000):
        post_process_file_contents.delay(tenant_schema(), str(file.id))


@post_deploy_action
def persist_folders_that_likely_are_personal_folders():
    if is_schema_public():
        return

    for file in FileFolder.objects.filter(type=FileFolder.Types.FOLDER, group=None):
        file.persist_file()


@post_deploy_action
def image_in_search_results():
    # Perform only once.
    if not is_schema_public():
        return

    tasks = [elasticsearch_install_new_indices.si()]
    callback = elasticsearch_rebuild_all.si("file")

    chord(tasks)(callback)


@post_deploy_action
def fix_hidden_group_icons():
    if is_schema_public():
        return

    group_content_type_id = ContentType.objects.get_for_model(Group).id
    group_references = FileReference.objects.filter(
        container_ct_id=group_content_type_id
    )

    for reference in group_references:
        if reference.file.refresh_read_access():
            reference.file.save()
            logger.info("Fixing hidden group icon: %s", reference.file)
