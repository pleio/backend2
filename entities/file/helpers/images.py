from io import BytesIO
from os.path import basename

from django.core.files.base import ContentFile
from PIL import Image, UnidentifiedImageError

from entities.file.models import FileFolder


def generate_thumbnail(file: FileFolder, size):
    thumbnail_size = (size, size)
    infile = file.upload.open()

    try:
        with Image.open(infile) as im:
            im.thumbnail(thumbnail_size, Image.LANCZOS)
            with BytesIO() as output:
                im = im.convert("RGB")
                im.save(output, "JPEG")
                contents = output.getvalue()
                file_name = str(file.id) + ".jpg"
                file.thumbnail.save(file_name, ContentFile(contents))

    except IOError:
        print("cannot create thumbnail for", infile)


def resize_and_update_image(file: FileFolder, max_width=1400, max_height=2000):
    """
    Resize FileFolder image to max bounderies
    """
    infile = file.upload.open()

    with Image.open(infile) as im:
        im.thumbnail((max_width, max_height), Image.LANCZOS)
        output = BytesIO()
        im = im.convert("RGB")
        im.save(output, "JPEG")
        contents = output.getvalue()
        file_name = str(file.id) + ".jpg"
        file.upload.save(file_name, ContentFile(contents))


def resize_and_save_as_png(file: FileFolder, max_width=180, max_height=180):
    """
    Resize FileFolder image to png
    """
    infile = file.upload.open()

    with Image.open(infile) as im:
        im.thumbnail((max_width, max_height), Image.LANCZOS)
        output = BytesIO()
        im = im.convert("RGBA")
        im.save(output, "PNG")
        contents = output.getvalue()
        file_name = str(file.id) + ".png"
        file.upload.save(file_name, ContentFile(contents))


def strip_exif(upload_field):
    try:
        output = BytesIO()
        image = Image.open(upload_field.open())
        if image.getexif():
            # Not all plugins support animated images (https://pillow.readthedocs.io/en/stable/reference/Image.html#PIL.Image.Image.is_animated)
            is_animated = getattr(image, "is_animated", False)
            image.save(output, image.format, save_all=is_animated)
            upload_field.save(
                basename(upload_field.name), ContentFile(output.getvalue())
            )
        image.close()

    except (FileNotFoundError, ValueError, UnidentifiedImageError):
        pass
