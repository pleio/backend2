from .filefolder import file, filefolder, folder
from .mutation import mutation_resolver
from .query import query

resolvers = [filefolder, file, folder, mutation_resolver, query]
