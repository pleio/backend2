import logging

from graphql import GraphQLError

from core.models.mixin import HierarchicalEntityMixin
from core.resolvers.mutations.mutation_delete_entity import DefaultDeleteEntityResolver
from entities.file.resolvers.helpers.references import (
    schedule_delete_from_referencing_entities,
    schedule_update_referencing_entities,
)

logger = logging.getLogger(__name__)


class RecursiveDeleteFileResolver(DefaultDeleteEntityResolver):
    def recursive_deep_references(self, root):
        if root.has_hard_references():
            yield root
        else:
            for child in root.children.all():
                yield from self.recursive_deep_references(child)

    def get_deep_references(self):
        return [*self.recursive_deep_references(self.entity)]

    def report_deep_references(self, files):
        return {
            "success": False,
            "errors": [
                {
                    "entity": file,
                    "next": self.next_action(file),
                }
                for file in files
            ],
        }

    @staticmethod
    def next_action(file):
        return "softDelete" if file.has_soft_references() else "forceDelete"

    def resolve_force_delete(self):
        for child in self.entity.all_children_and_me():
            if child.is_featured_image():
                child.cleanup_featured_image()
            if child.has_hard_references():
                schedule_delete_from_referencing_entities(child)
            if child.can_delete(self.request.user):
                child.delete()
        self.entity.update_updated_at()
        return {"success": True}

    def resolve_soft_delete(self):
        self.recursive_soft_delete(self.entity)
        self.entity.update_updated_at()
        return {"success": True}

    def recursive_soft_delete(self, maybe_folder):
        if maybe_folder.has_hard_references():
            # Soft-delete the whole branch.
            for child in maybe_folder.all_children_and_me():
                if child.has_soft_references() and child.can_delete(self.request.user):
                    child.remove_soft_references()
                    child.save()
                    schedule_update_referencing_entities(child)
        else:
            for child in maybe_folder.children.all():
                if not child.can_delete(self.request.user):
                    continue
                # The parent will be deleted anyway, make sure the children persist.
                child.parent = None
                # Test for hard refereces in this branch
                if not self.recursive_soft_delete(child):
                    # File still exists.
                    child.save()

            if self._can_delete(maybe_folder):
                maybe_folder.delete()
                return True
        return False

    def _can_delete(self, entity):
        if entity.can_delete(self.request.user):
            return not entity.has_children()
        return False

    def check_permissions_recursively(self, root):
        if not root.can_delete(self.request.user):
            msg = "missing_permissions"
            raise GraphQLError(msg)
        if isinstance(self.entity, HierarchicalEntityMixin):
            for child in root.get_children():
                self.check_permissions_recursively(child)

    def resolve(self):
        self.check_permissions_recursively(self.entity)

        recursive = self.parameters.get("recursive") or "tryDelete"

        if recursive == "forceDelete":
            return self.resolve_force_delete()

        elif recursive == "softDelete":
            return self.resolve_soft_delete()

        if deep_references := self.get_deep_references():
            return self.report_deep_references(deep_references)

        self.entity.update_updated_at()
        return super().resolve()
