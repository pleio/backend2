from ariadne import InterfaceType, ObjectType

from core import constances
from core.resolvers import shared
from entities.file.resolvers.helpers.references import ReferenceNegotiator

file = ObjectType("File")
folder = ObjectType("Folder")
filefolder = InterfaceType("FileFolder")


@filefolder.type_resolver
def resolve_filefolder_type(obj, *_):
    return obj.type


@filefolder.field("references")
def resolve_references(obj, info):
    acting_user = info.context["request"].user
    return ReferenceNegotiator.generate(obj, acting_user)


@filefolder.field("referenceCount")
def resolve_reference_count(obj, info):
    return obj.referenced_by.exclude_personal_references().count()


@filefolder.field("container")
def resolve_container(obj, info):
    if obj.parent:
        return obj.parent
    if obj.group:
        return obj.group
    if obj.is_personal_file:
        return obj.owner


@filefolder.field("rootContainer")
def resolve_root_container(obj, info):
    return obj.root_container


@filefolder.field("isPersonalFile")
def resolve_is_personal_file(obj, info):
    return obj.referenced_by.filter(configuration=constances.PERSONAL_FILE).exists()


@file.field("subtype")
@folder.field("subtype")
def resolve_subtype(obj, info):
    return obj.type_to_string


@file.field("parentFolder")
@folder.field("parentFolder")
def resolve_parent_folder(obj, info):
    return obj.parent


@file.field("mimeType")
def resolve_mimetype(obj, info):
    return obj.mime_type


@file.field("hasChildren")
@folder.field("hasChildren")
def resolve_has_children(obj, info):
    return obj.has_children()


@file.field("url")
@folder.field("url")
def resolve_url(obj, info):
    return obj.url


@file.field("lastDownload")
@folder.field("lastDownload")
def resolve_last_download(obj, info):
    return obj.last_download


@file.field("thumbnail")
def resolve_thumbnail(obj, info):
    mime_types = ["image/jpeg", "image/pjpeg", "image/png", "image/x-png", "image/gif"]
    # Only thumbnails for images
    if obj.mime_type not in mime_types:
        return None

    return obj.thumbnail_url


@file.field("download")
def resolve_download(obj, info):
    return obj.download_url


@file.field("embed")
def resolve_embed(obj, info):
    return obj.embed_url


@file.field("size")
def resolve_size(obj, info):
    return obj.size


@file.field("isFeaturedImage")
def resolve_is_featured_image(obj, info):
    return bool([*obj.featured_image_at()])


@file.field("originalFileName")
def resolve_original_filename(obj, info):
    return obj.original_filename()


file.set_field("guid", shared.resolve_entity_guid)
file.set_field("status", shared.resolve_entity_status)
file.set_field("title", shared.resolve_entity_title)
file.set_field("tags", shared.resolve_entity_tags)
file.set_field("tagCategories", shared.resolve_entity_categories)
file.set_field("timeCreated", shared.resolve_entity_time_created)
file.set_field("timeUpdated", shared.resolve_entity_time_updated)
file.set_field("timePublished", shared.resolve_entity_time_published)
file.set_field("scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity)
file.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
file.set_field("canEdit", shared.resolve_entity_can_edit)
file.set_field("accessId", shared.resolve_entity_access_id)
file.set_field("writeAccessId", shared.resolve_entity_write_access_id)
file.set_field("views", shared.resolve_entity_views)
file.set_field("owner", shared.resolve_entity_owner)
file.set_field("isPinned", shared.resolve_entity_is_pinned)
file.set_field("tags", shared.resolve_entity_tags)
file.set_field("richDescription", shared.resolve_entity_rich_description)
file.set_field("excerpt", shared.resolve_entity_excerpt)
file.set_field("inputLanguage", shared.resolve_entity_input_language)
file.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
file.set_field("localDescription", shared.resolve_entity_local_description)
file.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
file.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
file.set_field("publishRequest", shared.resolve_entity_publish_request)

folder.set_field("guid", shared.resolve_entity_guid)
folder.set_field("status", shared.resolve_entity_status)
folder.set_field("title", shared.resolve_entity_title)
folder.set_field("tags", shared.resolve_entity_tags)
folder.set_field("tagCategories", shared.resolve_entity_categories)
folder.set_field("timeCreated", shared.resolve_entity_time_created)
folder.set_field("timeUpdated", shared.resolve_entity_time_updated)
folder.set_field("timePublished", shared.resolve_entity_time_published)
folder.set_field("scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity)
folder.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
folder.set_field("canEdit", shared.resolve_entity_can_edit)
folder.set_field("accessId", shared.resolve_entity_access_id)
folder.set_field("writeAccessId", shared.resolve_entity_write_access_id)
folder.set_field("views", shared.resolve_entity_views)
folder.set_field("owner", shared.resolve_entity_owner)
folder.set_field("isPinned", shared.resolve_entity_is_pinned)
folder.set_field("tags", shared.resolve_entity_tags)
folder.set_field("richDescription", shared.resolve_entity_rich_description)
folder.set_field("excerpt", shared.resolve_entity_excerpt)
folder.set_field("inputLanguage", shared.resolve_entity_input_language)
folder.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)

filefolder.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
filefolder.set_field("canBookmark", shared.resolve_entity_can_bookmark)
filefolder.set_field("showOwner", shared.resolve_entity_show_owner)
filefolder.set_field("inputLanguage", shared.resolve_entity_input_language)
filefolder.set_field(
    "isTranslationEnabled", shared.resolve_entity_is_translation_enabled
)
filefolder.set_field("localDescription", shared.resolve_entity_local_description)
filefolder.set_field(
    "localRichDescription", shared.resolve_entity_local_rich_description
)
filefolder.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
filefolder.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
