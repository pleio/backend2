import logging

from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import ACCESS_TYPE, COULD_NOT_SAVE
from core.lib import TrackForChanges, clean_graphql_input
from core.models import Group
from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from core.utils.entity import load_entity_by_id
from user.models import User

from ..models import FileFolder
from .helpers.references import schedule_update_referencing_entities

logger = logging.getLogger(__name__)


def update_file_folder_owner(entity, owner, new_owner, recursive, full_recursive):
    current_owner_access = ACCESS_TYPE.user.format(entity.owner.guid)
    new_owner_access = ACCESS_TYPE.user.format(new_owner.guid)

    entity.owner = new_owner

    if current_owner_access in entity.write_access:
        new_write_access = [
            access for access in entity.write_access if not current_owner_access
        ]
        new_write_access.append(new_owner_access)
        entity.write_access = new_write_access

    if current_owner_access in entity.read_access:
        new_read_access = [
            access for access in entity.read_access if not current_owner_access
        ]
        new_read_access.append(new_owner_access)
        entity.read_access = new_read_access

    if recursive:
        child_filefolders = FileFolder.objects.filter(parent=entity)
        if not full_recursive:
            child_filefolders = child_filefolders.filter(owner=owner)
        for file_folder in child_filefolders:
            update_file_folder_owner(
                file_folder, owner, new_owner, recursive, full_recursive
            )
            file_folder.save()


mutation_resolver = ObjectType("Mutation")


@mutation_resolver.field("addFile")
def resolve_add_file(_, info, input):
    user = info.context["request"].user
    shared.assert_authenticated(user)

    clean_input = clean_graphql_input(input)
    group, parent = resolve_container(clean_input.get("containerGuid"), user)

    if not clean_input.get("file"):
        msg = "NO_FILE"
        raise GraphQLError(msg)

    entity = FileFolder()

    track_publication_date = ContentModerationTrackTimePublished(
        entity, user, is_new=True
    )

    entity.owner = user
    entity.upload = clean_input["file"]

    shared.resolve_update_tags(entity, clean_input)

    if parent:
        entity.parent = parent

    if group:
        entity.group = group

    shared.resolve_add_access_id(entity, clean_input, user)

    shared.resolve_update_rich_description(entity, clean_input)

    shared.update_publication_dates(entity, user, clean_input)

    entity.update_updated_at()

    track_publication_date.maybe_revert_time_published()
    entity.save()

    shared.scan_file(entity, delete_if_virus=True)
    shared.post_upload_file(entity)

    shared.resolve_update_input_language(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.copy_attachment_references(entity, parent, group)

    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}


def resolve_add_folder(_, info, input):
    """
    Used in core / addEntity
    """

    user = info.context["request"].user
    shared.assert_authenticated(user)

    clean_input = clean_graphql_input(input)
    group, parent = resolve_container(clean_input.get("containerGuid"), user)

    entity = FileFolder()

    entity.owner = user

    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)

    shared.resolve_update_input_language(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    entity.type = FileFolder.Types.FOLDER

    shared.resolve_update_rich_description(entity, clean_input)

    shared.update_publication_dates(entity, user, clean_input)

    if parent:
        entity.parent = parent

    if group:
        entity.group = group

    shared.resolve_add_access_id(entity, clean_input, user)

    entity.update_updated_at()

    entity.save()

    shared.copy_attachment_references(entity, parent, group)

    return {"entity": entity}


@mutation_resolver.field("editFileFolder")
def resolve_edit_file_folder(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [FileFolder])

    track_publication_date = ContentModerationTrackTimePublished(entity, user)

    change_tracker = TrackForChanges(entity.serialize(), ["title", "mimeType"])

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)
    shared.assert_not_attachment(entity)

    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)
    shared.resolve_update_input_language(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)

    resolve_update_file(entity, clean_input)

    if "ownerGuid" in clean_input:
        update_file_folder_owner(
            entity,
            owner=entity.owner,
            new_owner=User.objects.get(id=clean_input["ownerGuid"]),
            recursive="ownerGuidRecursive" in clean_input,
            full_recursive=clean_input.get("ownerGuidRecursive") == "updateAllFiles",
        )

    entity.update_updated_at()
    track_publication_date.maybe_revert_time_published()
    entity.save()

    track_publication_date.maybe_create_publish_request()

    if change_tracker.is_changed(entity.serialize()):
        schedule_update_referencing_entities(entity)

    return {"entity": entity}


@mutation_resolver.field("moveFileFolder")
def resolve_move_file_folder(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [FileFolder])

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)

    current_group = entity.group
    group, parent = resolve_container(clean_input.get("containerGuid"), user)

    # Only site-admins are allowed to change the group of a file/folder.
    if current_group != group:
        shared.assert_administrator(user)

    # Files/folders are not allowed to change from group to personal or vice versa.
    assert_not_group_to_personal_change(entity.group, group)

    if group:
        entity.group = group
        entity.parent = None

    if parent:
        # prevent moving folder in self or descendant of self
        parent_check = parent

        while parent_check:
            if parent_check == entity:
                msg = "INVALID_CONTAINER_GUID"
                raise GraphQLError(msg)
            parent_check = parent_check.parent

        # entity already in parent
        if entity.parent == parent:
            msg = "INVALID_CONTAINER_GUID"
            raise GraphQLError(msg)

        entity.parent = parent

    if current_group != group:
        if entity.is_folder():
            # Move children to new group.
            _recursive_update_child_group(entity, group)

        entity.read_access = _replace_group_access(
            entity.read_access, old_group=current_group, new_group=group
        )
        entity.write_access = _replace_group_access(
            entity.write_access, old_group=current_group, new_group=group
        )

    entity.update_updated_at()
    entity.save()

    shared.copy_attachment_references(entity, parent, group)

    return {"entity": entity}


def _recursive_update_child_group(entity, group):
    for child in FileFolder.objects.filter(parent=entity):
        child.read_access = _replace_group_access(
            child.read_access, old_group=child.group, new_group=group
        )
        child.write_access = _replace_group_access(
            child.write_access, old_group=child.group, new_group=group
        )
        child.group = group
        child.save()

        shared.copy_attachment_references(child, entity, group)
        _recursive_update_child_group(child, group)


def _replace_group_access(access_list, old_group, new_group):
    old_access = ACCESS_TYPE.group.format(old_group.guid) if old_group else None
    new_access = ACCESS_TYPE.group.format(new_group.guid) if new_group else None

    if old_access and new_access and old_access in access_list:
        return [new_access] + [access for access in access_list if access != old_access]

    return [access for access in access_list if access != old_access]


def resolve_update_file(entity, clean_input):
    if clean_input.get("file"):
        entity.upload = clean_input.get("file")
        if entity.thumbnail.name:
            entity.thumbnail.delete()
        entity.checksum = None
        shared.scan_file(entity, delete_from_disk=True)
        shared.post_upload_file(entity)


def resolve_container(guid, acting_user):
    group = None
    parent = None

    if guid:
        try:
            group = Group.objects.get(id=guid)
        except ObjectDoesNotExist:
            try:
                parent = FileFolder.objects.get_subclass(id=guid)
                if not parent.can_write(acting_user):
                    raise GraphQLError(COULD_NOT_SAVE)
            except ObjectDoesNotExist:
                if guid != acting_user.guid:
                    msg = "INVALID_CONTAINER_GUID"
                    raise GraphQLError(msg)

        # get parent group
        if parent and parent.group:
            group = parent.group

        if group:
            shared.assert_group_member(acting_user, group)

    return group, parent


def assert_not_group_to_personal_change(before, after):
    if (before and not after) or (not before and after):
        msg = "INVALID_CONTAINER_GUID"
        raise GraphQLError(msg)
