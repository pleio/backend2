from core.constances import ConfigurationChoices
from core.lib import tenant_schema
from core.models import Comment
from entities.activity.models import StatusUpdate


class ReferenceBase:
    @classmethod
    def applies_to(cls, record):
        return False

    @classmethod
    def as_dict(cls, record, acting_user) -> list:
        raise NotImplementedError()


class EntityReference(ReferenceBase):
    @classmethod
    def applies_to(cls, record):
        return record.is_entity

    @classmethod
    def as_dict(cls, record, acting_user):
        return {
            "referenceType": "entity",
            "entityType": record.container.type_to_string,
            "label": cls._label(record.container),
            "url": getattr(record.container, "url", None),
            "guid": str(record.container.pk),
        }

    @classmethod
    def _label(cls, container):
        if isinstance(container, (StatusUpdate, Comment)):
            return container.excerpt
        return getattr(container, "title", None)


class SettingReference(ReferenceBase):
    @classmethod
    def applies_to(cls, record):
        return record.is_configuration

    @staticmethod
    def _label(record):
        try:
            return ConfigurationChoices[record.configuration].value
        except KeyError:
            pass

    @classmethod
    def as_dict(cls, record, acting_user):
        return {
            "referenceType": "setting",
            "label": cls._label(record),
        }


class ReferenceNegotiator:
    reference_processors = [EntityReference, SettingReference]

    @classmethod
    def generate(cls, file, acting_user):
        for reference in file.referenced_by.all():
            for reference_processor in cls.reference_processors:
                if not reference_processor.applies_to(reference):
                    continue
                details = reference_processor.as_dict(reference, acting_user)
                if details:
                    yield details
                break


def schedule_delete_from_referencing_entities(file_entity):
    from entities.file.tasks import delete_from_referencing_entities

    for file_reference in file_entity.referenced_by.filter_entities():
        delete_from_referencing_entities.delay(
            tenant_schema(), file_entity.guid, str(file_reference.container_fk)
        )


def schedule_update_referencing_entities(file_entity):
    from entities.file.tasks import update_referencing_entities

    for file_reference in file_entity.referenced_by.filter_entities():
        update_referencing_entities.delay(
            tenant_schema(), file_entity.guid, str(file_reference.container.pk)
        )
