from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Case, Q, When
from graphql import GraphQLError

from core import constances
from core.models import Annotation, Group
from entities.file.models import FileFolder
from user.models import User

query = ObjectType("Query")


def conditional_group_folder_user_container_filter(container_guid, is_folder, is_user):
    if container_guid and not is_folder and not is_user:
        return Q(Q(group__id=container_guid) & Q(parent=None))

    if container_guid and is_folder:
        folder = FileFolder.objects.get(id=container_guid)
        # Group match
        if folder.group:
            return Q(parent__id=container_guid, group=folder.group)
        # Or references match
        if folder.referenced_by.is_personal_file():
            return Q(
                parent__id=container_guid,
                referenced_by__configuration=constances.PERSONAL_FILE,
            )
        # Or any match
        return Q(parent__id=container_guid)

    if container_guid and is_user:
        return Q(
            owner__id=container_guid,
            parent=None,
            group=None,
            referenced_by__configuration=constances.PERSONAL_FILE,
        )

    return Q()


def conditional_filter_subtypes(subtypes):
    q_objects = Q()
    if subtypes:
        for subtype in subtypes:
            if subtype.lower() == "file":
                q_objects.add(Q(type=FileFolder.Types.FILE), Q.OR)

            if subtype.lower() == "folder":
                q_objects.add(Q(type=FileFolder.Types.FOLDER), Q.OR)

            if subtype.lower() == "pad":
                q_objects.add(Q(type=FileFolder.Types.PAD), Q.OR)

    return q_objects


@query.field("files")
def resolve_files(  # noqa: C901
    _,
    info,
    typeFilter=None,
    containerGuid=None,
    filterBookmarks=None,
    offset=0,
    limit=20,
    orderBy="title",
    orderDirection="asc",
):
    acting_user = info.context["request"].user

    order_by = ["title"]
    if orderBy == "size":
        order_by.insert(0, "size")
    if orderBy == "timeUpdated":
        order_by.insert(0, "updated_at")
    elif orderBy == "timeCreated":
        order_by.insert(0, "created_at")
    elif orderBy == "timePublished":
        order_by.insert(0, "published")
    elif orderBy == "readAccessWeight":
        order_by.insert(0, "read_access_weight")
    elif orderBy == "writeAccessWeight":
        order_by.insert(0, "write_access_weight")
    elif orderBy == "lastDownload":
        order_by.insert(0, "last_download")

    if orderDirection == "desc":
        for n, value in enumerate(order_by):
            order_by[n] = "-%s" % value

    is_folder = False
    is_user = False

    # check if containerGuid is group, folder or user
    if containerGuid:
        try:
            Group.objects.get(id=containerGuid)
        except ObjectDoesNotExist:
            try:
                FileFolder.objects.get_subclass(id=containerGuid)
                is_folder = True
            except ObjectDoesNotExist:
                try:
                    User.objects.get(id=containerGuid)
                    is_user = True
                except ObjectDoesNotExist:
                    msg = "INVALID_CONTAINER_GUID"
                    raise GraphQLError(msg)

    qs = FileFolder.objects.visible(acting_user)
    qs = qs.filter(
        conditional_group_folder_user_container_filter(
            containerGuid, is_folder, is_user
        )
        & conditional_filter_subtypes(typeFilter)
    )
    if filterBookmarks:
        bookmarks = Annotation.objects.filter(
            user=acting_user, key="bookmarked"
        ).values_list("object_id", flat=True)
        qs = qs.filter(pk__in=bookmarks)
    qs = qs.order_by(
        Case(When(type=FileFolder.Types.FOLDER, then=0), default=1), *order_by
    )

    edges = qs[offset : offset + limit]
    return {"total": qs.count(), "edges": edges}
