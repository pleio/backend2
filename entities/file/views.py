import zipfile
from os import path

from django.core.exceptions import ObjectDoesNotExist
from django.http import FileResponse, Http404
from django.shortcuts import redirect
from django.utils import timezone
from django.views.decorators.cache import cache_control

from core.constances import USER_ROLES
from core.http import file_blocked_response
from core.lib import get_mimetype, get_tmp_file_path
from core.models import Entity
from core.utils.streaming_response import PleioStreamingHttpResponse
from entities.file.helpers.compression import add_folders_to_zip, get_download_filename
from entities.file.helpers.images import generate_thumbnail
from entities.file.models import FileFolder


def download(request, file_id=None, file_name=None):
    user = request.user

    size = request.GET.get("size", None)

    if not file_id:
        msg = "File not found"
        raise Http404(msg)

    try:
        entity = FileFolder.objects.visible_all(user).get(id=file_id)

        if entity.blocked:
            return file_blocked_response(
                request, entity.upload.name, entity.block_reason
            )

        if (
            entity.group
            and entity.group.is_closed
            and not entity.group.is_full_member(user)
            and not user.has_role(USER_ROLES.ADMIN)
        ) or not entity.can_read(user):
            msg = "File not found"
            raise Http404(msg)

        return_file = entity

        # update last_download without calling "save"
        FileFolder.objects.visible(user).filter(id=file_id).update(
            last_download=timezone.now()
        )

        if size:
            resized_image = entity.get_resized_image(size)

            if resized_image:
                return_file = resized_image
            else:
                return redirect(entity.download_url)

        response = PleioStreamingHttpResponse(
            file_name=get_download_filename(entity),
            streaming_content=return_file.upload.open(),
            content_type=return_file.mime_type,
        )

        response["Content-Length"] = return_file.upload.size
        return response
    except (ObjectDoesNotExist, FileNotFoundError):
        msg = "File not found"
        raise Http404(msg)


@cache_control(public=True, max_age=15724800)
def embed(request, file_id=None, file_name=None):
    user = request.user
    size = request.GET.get("size", None)

    if not file_id:
        msg = "File not found"
        raise Http404(msg)

    try:
        entity = FileFolder.objects.visible(user).get(id=file_id)

        if entity.blocked:
            return file_blocked_response(
                request, entity.upload.name, entity.block_reason
            )

        if (
            entity.group
            and entity.group.is_closed
            and not entity.group.is_full_member(user)
            and not user.has_role(USER_ROLES.ADMIN)
        ):
            msg = "File not found"
            raise Http404(msg)

        return_file = entity

        if size:
            resized_image = entity.get_resized_image(size)

            if resized_image:
                return_file = resized_image
            else:
                return redirect(entity.embed_url)

        response = PleioStreamingHttpResponse(
            file_name=return_file.upload.name,
            streaming_content=return_file.upload.open(),
            content_type=return_file.mime_type,
        )
        response["Content-Length"] = return_file.upload.size
        return response
    except (ObjectDoesNotExist, FileNotFoundError):
        msg = "File not found"
        raise Http404(msg)


@cache_control(public=True, max_age=15724800)
def featured(request, entity_guid=None):
    size = request.GET.get("size", None)

    if not entity_guid:
        msg = "File not found"
        raise Http404(msg)

    try:
        # don't check user access on featured images because they are also used in email
        entity = Entity.objects.get_subclass(id=entity_guid)

        if hasattr(entity, "featured_image") and entity.featured_image:
            return_file = entity.featured_image

            if return_file.blocked:
                return file_blocked_response(
                    request, return_file.upload.name, return_file.block_reason
                )

            if size:
                resized_image = entity.featured_image.get_resized_image(size)

                if resized_image:
                    return_file = resized_image
                else:
                    return redirect(entity.featured_image_url)

            response = PleioStreamingHttpResponse(
                file_name=return_file.upload.name,
                streaming_content=return_file.upload.open(),
                content_type=return_file.mime_type,
            )
            response["Content-Length"] = return_file.upload.size
            return response
    except ObjectDoesNotExist:
        pass

    msg = "File not found"
    raise Http404(msg)


def bulk_download(request):
    user = request.user

    file_ids = request.GET.getlist("file_guids[]")
    folder_ids = request.GET.getlist("folder_guids[]")

    if not file_ids and not folder_ids:
        msg = "File not found"
        raise Http404(msg)

    temp_file_path = get_tmp_file_path(user, ".zip")
    zipf = zipfile.ZipFile(temp_file_path, "w", zipfile.ZIP_DEFLATED)

    # Add selected files to zip
    files = FileFolder.objects.visible(user).filter(
        id__in=file_ids, type=FileFolder.Types.FILE
    )
    for f in files:
        if (
            f.group
            and f.group.is_closed
            and not f.group.is_full_member(user)
            and not user.has_role(USER_ROLES.ADMIN)
        ):
            continue
        zipf.writestr(path.basename(get_download_filename(f)), f.upload.read())
        f.last_download = timezone.now()
        f.save()

    # Add selected folders to zip
    folders = FileFolder.objects.visible(user).filter(
        id__in=folder_ids, type=FileFolder.Types.FOLDER
    )
    add_folders_to_zip(zipf, folders, user, "")

    zipf.close()

    response = FileResponse(open(temp_file_path, "rb"))
    response["Content-Disposition"] = "attachment; filename=file_contents.zip"

    return response


@cache_control(public=True, max_age=15724800)
def thumbnail(request, file_id=None):
    user = request.user

    if not file_id:
        msg = "File not found"
        raise Http404(msg)

    try:
        entity = FileFolder.objects.visible(user).get(id=file_id)
    except ObjectDoesNotExist:
        msg = "File not found"
        raise Http404(msg)

    if entity.blocked:
        return file_blocked_response(request, entity.upload.name, entity.block_reason)

    if not entity.thumbnail:
        try:
            generate_thumbnail(entity, 153)
        except FileNotFoundError:
            pass

    if entity.thumbnail:
        try:
            response = PleioStreamingHttpResponse(
                file_name=entity.thumbnail.name,
                streaming_content=entity.thumbnail.open(),
                content_type=get_mimetype(entity.thumbnail.name),
            )
            response["Content-Length"] = entity.thumbnail.size
            return response
        except FileNotFoundError:
            pass

    msg = "File not found"
    raise Http404(msg)
