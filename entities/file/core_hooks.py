from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from core import config
from core.exceptions import ExceptionDuringQueryIndex, IgnoreIndexError
from core.tests.helpers import GraphQLClient
from user.models import User


def get_entity_filters():
    yield {
        "key": "file",
        "value": _("File"),
    }
    yield {
        "key": "folder",
        "value": _("Folder"),
    }
    if config.COLLAB_EDITING_ENABLED:
        yield {
            "key": "pad",
            "value": _("Pad"),
        }


def get_search_filters():
    yield {
        "key": "file",
        "value": _("File"),
        "plural": pgettext_lazy("plural", "Files"),
    }
    yield {
        "key": "folder",
        "value": _("Folder"),
        "plural": pgettext_lazy("plural", "Folders"),
    }
    yield {
        "key": "image",
        "value": _("Image"),
        "plural": pgettext_lazy("plural", "Images"),
    }


def test_elasticsearch_index(index_name):
    if index_name != "file":
        raise IgnoreIndexError()

    try:
        client = GraphQLClient()
        client.force_login(User.objects.filter(is_superadmin=True).first())
        client.post(
            """
        query ElasticsearchQuery($type: String) {
            search(subtype: $type, subtypes: [$type]) {
                total
                totals {
                    title
                    subtype
                    total
                }
                edges {
                    ... on File {
                        guid
                        title
                    }
                    ... on Folder {
                        guid
                        title
                    }
                    ... on Pad {
                        guid
                        title
                    }
                }
            }
        }
        """,
            {
                "type": "file",
            },
        )
    except Exception as e:
        raise ExceptionDuringQueryIndex(str(e))
