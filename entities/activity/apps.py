from django.apps import AppConfig


class ActivityConfig(AppConfig):
    name = "entities.activity"
    label = "activity"
