from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.queries.entities.test_order_by import OrderByBaseTestCases
from user.factories import UserFactory


class BuildEntityMixin:
    def __init__(self, *args, **kwargs):
        self._status_update_group = None
        self.owner = None
        super().__init__(*args, **kwargs)

    @property
    def status_update_group(self):
        if not self._status_update_group:
            self._status_update_group = GroupFactory(
                owner=UserFactory(), name="Group for statusupdates"
            )
        return self._status_update_group

    def build_entity(self, **kwargs):
        if "group" not in kwargs:
            kwargs["group"] = self.status_update_group
        if "owner" in kwargs:
            kwargs["group"].join(kwargs["owner"])
            if "read_access" not in kwargs:
                kwargs["read_access"] = [ACCESS_TYPE.logged_in]
        return mixer.blend("activity.StatusUpdate", **kwargs)


class TestOrderByTimeCreated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeCreated,
):
    pass


class TestOrderByTimeUpdated(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimeUpdated,
):
    pass


class TestOrderByTimePublished(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTimePublished,
):
    pass


class TestOrderByLastAction(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastAction,
):
    pass


class TestOrderByLastSeen(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByLastSeen,
):
    pass


class TestOrderByStartDateNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByStartDateNotAvailable,
):
    pass


class TestOrderByFileSizeNotAvailable(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByFileSizeNotAvailable,
):
    pass


class TestOrderByTitle(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByTitle,
):
    pass


class TestOrderByGroupName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByGroupName,
):
    pass


class TestOrderByOwnerName(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByOwnerName,
):
    pass


class TestOrderByReadAccess(
    BuildEntityMixin,
    OrderByBaseTestCases.TestOrderByReadAccess,
):
    pass
