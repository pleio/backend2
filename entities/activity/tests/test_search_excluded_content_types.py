from core.factories import BuildGroupOnceFactoryMixin
from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.activity.models import StatusUpdate
from entities.blog.factories import BlogFactory


class TestSearchWithExcludedContentTypesTestCase(
    BuildGroupOnceFactoryMixin, Template.TestSearchWithExcludedContentTypesTestCase
):
    EXCLUDE_TYPES = ["statusupdate", "group", "page"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return StatusUpdate.objects.create(
            **self.add_group_to_entity(title=title, owner=self.owner)
        )
