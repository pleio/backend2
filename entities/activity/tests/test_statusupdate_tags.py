from core.factories import GroupFactory
from core.tests.helpers.tags_testcase import Template
from entities.activity.models import StatusUpdate


class TestStatusUpdateTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "StatusUpdate"
    model = StatusUpdate

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._group = None

    def variables_add(self):
        if not self._group:
            self._group = GroupFactory(owner=self.owner)
        return {
            "input": {
                "title": "New Status",
                "subtype": "statusupdate",
                "groupGuid": self._group.guid,
            }
        }

    include_entity_search = True
    include_activity_search = True
