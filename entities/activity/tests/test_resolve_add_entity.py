from django.test import tag
from mixer.backend.django import mixer

from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from user.factories import UserFactory


@tag("createEntity")
class AddStatusUpdateTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.authenticated_user = UserFactory()
        self.group = mixer.blend(
            Group, owner=self.authenticated_user, is_membership_on_request=False
        )
        self.group.join(self.authenticated_user, "owner")

        self.override_config(
            EXTRA_LANGUAGES=["en", "nl", "de"],
            LANGUAGE="de",
        )

        self.data = {
            "input": {
                "subtype": "statusupdate",
                "title": "My first StatusUpdate",
                "richDescription": "richDescription",
                "tags": ["tag1", "tag2"],
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "groupGuid": self.group.guid,
            }
        }
        self.mutation = """
            fragment StatusUpdateParts on StatusUpdate {
                inputLanguage
                isTranslationEnabled
                title
                richDescription
                timeCreated
                timeUpdated
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                url
                inGroup
                group {
                    guid
                }
                showOwner
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...StatusUpdateParts
                    }
                }
            }
        """

    def test_add_status_update(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(entity["inputLanguage"], "en")
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["showOwner"], True)

    def test_add_status_update_to_group(self):
        variables = self.data
        variables["input"]["containerGuid"] = self.group.guid

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["inGroup"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)

    def test_add_minimal_entity(self):
        variables = {
            "input": {
                "groupGuid": self.group.guid,
                "title": "Simple status update",
                "subtype": "statusupdate",
            }
        }

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])
