import logging
from copy import deepcopy

from ariadne import ObjectType
from django.db.models import F, Q
from django.db.models.functions import Coalesce
from graphql import GraphQLError

from core.constances import COULD_NOT_USE_EVENT_FILTER, ORDER_BY, ORDER_DIRECTION
from core.lib import early_this_morning
from core.models import Entity
from core.resolvers.queries import shared_filters as filters
from core.resolvers.queries.shared_order_by import order_by_entity_column
from entities.event.lib import complement_expected_range

query = ObjectType("Query")

logger = logging.getLogger(__name__)

QUERY_SUBTYPES = [
    filters.MagazineIssueEntityFilter,
    filters.NewsEntityFilter,
    filters.BlogEntityFilter,
    filters.PodcastEntityFilter,
    filters.EpisodeEntityFilter,
    filters.EventEntityFilter,
    filters.DiscussionEntityFilter,
    filters.StatusupdateEntityFilter,
    filters.QuestionEntityFilter,
    filters.WikiEntityFilter,
    filters.PageEntityFilter,
]


def conditional_subtypes_filter(subtypes):
    query = Q()

    for entity_filter in QUERY_SUBTYPES:
        entity_filter(subtypes).add_if_applicable(query)

    return query


def conditional_event_filter(date_filter):
    if date_filter == "previous":
        return Q(event__end_date__lt=early_this_morning())
    return Q(event__end_date__gte=early_this_morning())


@query.field("activities")
def resolve_activities(
    _,
    info,
    offset=0,
    limit=20,
    tags=None,
    tagCategories=None,
    matchStrategy="any",
    groupFilter=None,
    eventFilter=None,
    subtypes=None,
    orderBy=ORDER_BY.timePublished,
    orderDirection=ORDER_DIRECTION.desc,
    sortPinned=False,
    statusPublished=None,
    userGuid=None,
    containerGuid=None,
):
    order_by_processor = order_by_entity_column(orderBy, subtypes)
    order_by = order_by_processor.column()

    # Set orderDirection to default when none value is used
    if orderDirection is None:
        orderDirection = ORDER_DIRECTION.desc

    if orderDirection == ORDER_DIRECTION.desc:
        order_by = "-%s" % order_by

    order = [order_by]

    if sortPinned:
        order = ["-is_pinned", *order]

    if (statusPublished is not None) and (len(statusPublished) > 0):
        qs = Entity.objects.status_published(
            statusPublished, info.context["request"].user
        )
    else:
        qs = Entity.objects.visible(info.context["request"].user)

    qs = qs.annotate(
        sort_title=Coalesce(
            "news__title",
            "blog__title",
            "filefolder__title",
            "poll__title",
            "statusupdate__title",
            "wiki__title",
            "page__title",
            "question__title",
            "discussion__title",
            "event__title",
        )
    )

    qs = qs.filter(
        conditional_subtypes_filter(subtypes)
        & filters.conditional_tags_filter(tags, matchStrategy == "any")
        & filters.conditional_tag_lists_filter(tagCategories, matchStrategy != "all")
        & filters.conditional_group_guid(containerGuid)
        & filters.conditional_groups_filter(groupFilter, info.context["request"].user)
    )
    qs = qs.exclude(issue_references__isnull=False)

    if userGuid:
        qs = qs.filter(owner__id=userGuid)

    if eventFilter:
        if subtypes == ["event"]:
            qs = qs.filter(conditional_event_filter(eventFilter))
        else:
            raise GraphQLError(COULD_NOT_USE_EVENT_FILTER)

    if subtypes == ["event"]:
        qs2 = deepcopy(qs).annotate(start_date=F("event__start_date"))
        complement_expected_range(qs2, offset, limit)

    qs = order_by_processor.extend_query(qs)

    qs = qs.order_by(*order).select_subclasses()

    total = qs.count()

    qs = qs[offset : offset + limit]

    activities = []

    for item in qs:
        activity = {
            "guid": "activity:%s" % (item.guid),
            "type": "create",
            "entity": item,
        }

        activities.append(activity)

    return {"total": total, "edges": activities}
