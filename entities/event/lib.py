from __future__ import annotations

from io import StringIO

from django.utils import timezone
from django.utils.text import slugify
from graphql import GraphQLError

from core.constances import INVALID_NAME
from core.lib import early_this_morning, get_base_url, get_full_url
from core.utils.convert import tiptap_to_text


def get_url(obj):
    prefix = ""

    if obj.group:
        prefix = "/groups/view/{}/{}".format(obj.group.guid, slugify(obj.group.name))

    return (
        get_base_url()
        + "{}/events/view/{}/{}".format(prefix, obj.guid, slugify(obj.title)).lower()
    )


def validate_name(name):
    if not name or not len(name.strip()) > 2:
        raise GraphQLError(INVALID_NAME)
    return name.strip()


def complement_expected_range(events, offset, limit):
    least_starttime = None
    for start_date in events.values_list("start_date", flat=True)[: offset + limit]:
        if not start_date:
            continue
        if not least_starttime or start_date > least_starttime:
            least_starttime = early_this_morning(
                start_date + timezone.timedelta(days=1)
            )

    if not least_starttime:
        least_starttime = early_this_morning(
            timezone.now() + timezone.timedelta(days=1)
        )

    from entities.event.models import Event
    from entities.event.range.sync import complete_range

    threshold = timezone.now() - timezone.timedelta(minutes=60)

    for event in Event.objects.filter_range_events():
        # Remove old incomplete events.
        Event.objects.filter(
            start_date__isnull=True, range_master=event, created_at__lte=threshold
        ).delete()
        complete_range(event, until=least_starttime, cycle=limit)


def mark_events_for_indexing(number_of_events_ahead=None):
    from entities.event.range.index import RangeIndexProcessor

    processor = RangeIndexProcessor(number_of_events_ahead)
    processor.process()


def get_ics_file(event) -> StringIO:
    """
    event: entities.event.models.Event
    """
    stream = StringIO()
    stream.write("BEGIN:VCALENDAR\n")
    stream.write("VERSION:2.0\n")
    stream.write("BEGIN:VEVENT\n")

    fmt = "%Y%m%dT%H%M%S"
    stream.write("DTSTART:%sZ\n" % event.start_date.strftime(fmt))
    stream.write("DTEND:%sZ\n" % event.end_date.strftime(fmt))

    stream.write("SUMMARY:%s\n" % event.title)
    if event.abstract:
        stream.write("DESCRIPTION:%s\n" % tiptap_to_text(event.rich_description))
    if event.location:
        stream.write("LOCATION:%s\n" % event.location)
    elif event.location_address:
        stream.write("LOCATION:%s\n" % event.location_address)
    stream.write("URL:%s\n" % get_full_url(event.url))
    stream.write("END:VEVENT\n")
    stream.write("END:VCALENDAR\n")

    stream.seek(0)
    return stream
