from email.mime.text import MIMEText

from django.utils.translation import gettext_lazy as _

from core.lib import get_full_url
from core.mail_builders.template_mailer import TemplateMailerBase
from core.utils.entity import load_entity_by_id
from entities.event.lib import get_ics_file


def submit_attend_event_confirm(attendee_id, code):
    from core.models import MailInstance

    MailInstance.objects.submit(
        AttendEventConfirmMailer,
        mailer_kwargs={"attendee": attendee_id, "code": code},
    )


class AttendEventConfirmMailer(TemplateMailerBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.attendee = load_entity_by_id(
            self.kwargs["attendee"], ["event.EventAttendee"]
        )
        self.event = self.attendee.event
        self.code = self.kwargs["code"]

    def get_context(self):
        leave_url = LeaveUrl(self.event)
        leave_url.add_email(self.attendee.email)
        leave_url.add_code(self.code)

        return {
            **super().get_context(),
            **self.add_local_context(),
            "link": get_full_url(self.event.url),
            "leave_link": get_full_url(leave_url.get_url()),
            "title": self.event.title,
            "location": self.event.location,
            "locationLink": self.event.location_link,
            "locationAddress": self.event.location_address,
            "start_date": self.event.start_date,
            "state": self.attendee.state,
        }

    def get_language(self):
        return self.attendee.language

    def get_template(self):
        return "email/attend_event_confirm.html"

    def get_receiver(self):
        return None

    def get_receiver_email(self):
        return self.attendee.email

    def get_sender(self):
        return None

    def get_subject(self):
        return _("Confirmation of registration for %s") % self.event.title

    def pre_send(self, email):
        email.attach(self.get_attachment())

    def get_attachment(self):
        file = get_ics_file(self.attendee.event)
        file_data = file.read()
        ical_data = MIMEText(file_data, _subtype='calendar; method="REQUEST"')
        ical_data.add_header(
            "Content-Disposition",
            f'attachment; filename="{self.attendee.event.slug}.ics"',
        )
        return ical_data


class LeaveUrl:
    def __init__(self, event):
        self.url = "/events/confirm/" + event.guid + "?delete=true"

    def add_email(self, email):
        self.url = self.url + "&email=" + email
        return self

    def add_code(self, code):
        self.url = self.url + "&code=" + code
        return self

    def get_url(self):
        return self.url
