from django.utils.translation import gettext as _

from core.mail_builders.template_mailer import TemplateMailerBase
from core.utils.entity import load_entity_by_id


def submit_attend_event_request(kwargs):
    from core.models import MailInstance

    MailInstance.objects.submit(AttendEventRequestMailer, mailer_kwargs=kwargs)


class AttendEventRequestMailer(TemplateMailerBase):
    def __init__(self, **kwargs):
        super(AttendEventRequestMailer, self).__init__(**kwargs)
        self.event = load_entity_by_id(self.kwargs["event"], ["event.Event"])
        self.email = self.kwargs["email"]
        self.language = self.kwargs["language"]
        self.link = self.kwargs["link"]

    def get_subject(self):
        return _("Confirm your registration for %s") % self.event.title

    def get_receiver_email(self):
        return self.email

    def get_language(self):
        return self.kwargs["language"]

    def get_template(self):
        return "email/attend_event_request.html"

    def get_context(self):
        return {
            **super().get_context(),
            **self.add_local_context(),
            "link": self.link,
            "title": self.event.title,
            "location": self.event.location,
            "locationLink": self.event.location_link,
            "locationAddress": self.event.location_address,
            "start_date": self.event.start_date,
        }

    def get_sender(self):
        return None

    def get_receiver(self):
        return None
