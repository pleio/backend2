from email.mime.text import MIMEText

from core.mail_builders.template_mailer import TemplateMailerBase
from core.utils.convert import tiptap_to_html
from core.utils.entity import load_entity_by_id
from entities.event.lib import get_ics_file
from entities.event.models import EventAttendee


def send_mail(attendee):
    from core.models import MailInstance

    MailInstance.objects.submit(
        AttendeeWelcomeMailMailer, mailer_kwargs={"attendee": attendee.pk}, delay=False
    )


class AttendeeWelcomeMailMailer(TemplateMailerBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.attendee: EventAttendee = load_entity_by_id(
            kwargs["attendee"], [EventAttendee]
        )

    def get_context(self):
        return {
            **super().get_context(),
            **self.add_local_context(self.attendee.as_mailinfo()),
            "message": self._replace_wildcards(
                self.attendee.event.attendee_welcome_mail_content
            ),
            "subject": self.attendee.event.title,
            "location": self.attendee.event.location,
            "locationLink": self.attendee.event.location_link,
            "locationAddress": self.attendee.event.location_address,
            "start_date": self.attendee.event.start_date,
        }

    def _replace_wildcards(self, message):
        message = tiptap_to_html(message)
        message = message.replace("[name]", self.attendee.name)
        return message

    def get_language(self):
        return self.attendee.language

    def get_template(self):
        return "email/send_attendee_welcome_mail.html"

    def get_receiver(self):
        return self.attendee.user

    def get_receiver_email(self):
        return self.attendee.email

    def get_sender(self):
        return None

    def get_subject(self):
        return self.attendee.event.attendee_welcome_mail_subject

    def pre_send(self, email):
        email.attach(self.get_attachment())

    def get_attachment(self):
        file = get_ics_file(self.attendee.event)
        file_data = file.read()
        ical_data = MIMEText(file_data, _subtype='calendar; method="REQUEST"')
        ical_data.add_header(
            "Content-Disposition",
            f'attachment; filename="{self.attendee.event.slug}.ics"',
        )
        return ical_data
