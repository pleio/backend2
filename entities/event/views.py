import csv
import io

import qrcode
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404, HttpResponse, StreamingHttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.utils.text import slugify

from core.lib import generate_code, get_base_url
from core.views.shared import Echo
from entities.event.export import AttendeeExporter
from entities.event.models import Event, EventAttendee


def export(request, event_id=None):
    user = request.user

    if not user.is_authenticated:
        msg = "Not found"
        raise Http404(msg)

    try:
        event = Event.objects.get(id=event_id)
    except (ObjectDoesNotExist, ValidationError):
        msg = "Not found"
        raise Http404(msg)

    if not event.can_write(user):
        msg = "Not found"
        raise Http404(msg)

    event_export = AttendeeExporter(event, user)

    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer, delimiter=";", quotechar='"')
    response = StreamingHttpResponse(
        (writer.writerow(row) for row in event_export.rows()), content_type="text/csv"
    )
    filename = slugify(event.title)
    response["Content-Disposition"] = f'attachment; filename="{filename}.csv"'

    return response


def export_calendar(request):
    output = io.StringIO()
    output.write("BEGIN:VCALENDAR\n")
    output.write("VERSION:2.0\n")
    output.write("BEGIN:VEVENT\n")
    output.write("DTSTART:" + request.GET.get("startDate", "") + "\n")
    output.write("DTEND:" + request.GET.get("endDate", "") + "\n")
    output.write("SUMMARY:" + request.GET.get("text", "") + "\n")
    output.write("URL:" + request.GET.get("url", "") + "\n")
    output.write("DESCRIPTION:" + request.GET.get("details", "") + "\n")
    if request.GET.get("location", ""):
        output.write("LOCATION:" + request.GET.get("location", "") + "\n")
    elif request.GET.get("locationAddress", ""):
        output.write("LOCATION:" + request.GET.get("locationAddress", "") + "\n")
    output.write("END:VEVENT\n")
    output.write("END:VCALENDAR\n")

    response = HttpResponse(
        output.getvalue(), content_type="text/calendar; charset=utf-8"
    )
    output.close()
    filename = slugify(request.GET.get("text", "event"))
    response["Content-Disposition"] = f'attachment; filename="{filename}.ics"'
    return response


def get_access_qr(request, entity_id):
    user = request.user
    if not user.is_authenticated:
        msg = "Event not found"
        raise Http404(msg)

    try:
        entity = Event.objects.visible(user).get(id=entity_id)
    except ObjectDoesNotExist:
        msg = "Event not found"
        raise Http404(msg)

    try:
        attendee = EventAttendee.objects.get(user=user, event=entity)
    except ObjectDoesNotExist:
        msg = "Attendee not found"
        raise Http404(msg)

    if hasattr(entity, "title") and entity.title:
        filename = slugify(entity.title)[:238].removesuffix("-")
    else:
        filename = entity.id
    filename = f"qr_access_{filename}.png"

    code = attendee.code

    if not code:
        code = generate_code()
        attendee.code = code
        attendee.save(update_fields=["code"])

    url = get_base_url() + "/events/view/guest-list?code={}".format(code)

    qr_code = qrcode.make(url)

    response = HttpResponse(content_type="image/png")
    qr_code.save(response, "PNG")
    response["Content-Disposition"] = f'attachment; filename="{filename}"'

    return response


def check_in(request):
    user = request.user

    try:
        attendee = EventAttendee.objects.get(code=request.GET.get("code"))
    except EventAttendee.DoesNotExist:
        msg = "Attendee not found"
        raise Http404(msg)

    event = Event.objects.get(id=attendee.event.guid)

    if event.owner != user:
        msg = "Not event owner"
        raise Http404(msg)

    context = {
        "next": request.GET.get("next", ""),
        "name": attendee.user.name,
        "email": attendee.user.email,
        "check_in": attendee.checked_in_at,
        "event": event.title,
        "date": event.start_date,
    }

    if request.method == "POST":
        attendee.checked_in_at = timezone.now()
        attendee.save()
        return render(request, "check_in/checked_in.html", context)

    if attendee.checked_in_at is None:
        return render(request, "check_in/check_in.html", context)

    return render(request, "check_in/already_checked_in.html", context)
