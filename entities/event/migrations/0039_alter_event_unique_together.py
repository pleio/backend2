# Generated by Django 4.2.16 on 2024-11-27 14:23

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("event", "0038_cleanup_duplicate_recurring_events"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="event",
            unique_together={("range_master", "range_starttime")},
        ),
    ]
