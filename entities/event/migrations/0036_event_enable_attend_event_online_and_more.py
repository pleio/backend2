# Generated by Django 4.2.11 on 2024-06-13 11:01

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("event", "0035_delete_eventattendeerequest"),
    ]

    operations = [
        migrations.AddField(
            model_name="event",
            name="enable_attend_event_online",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="eventattendee",
            name="state",
            field=models.CharField(
                blank=True,
                choices=[
                    ("unconfirmed", "Unconfirmed"),
                    ("online", "Attend online"),
                    ("accept", "Accepted"),
                    ("maybe", "Maybe"),
                    ("reject", "Rejected"),
                    ("waitinglist", "At waitinglist"),
                ],
                max_length=16,
                null=True,
            ),
        ),
    ]
