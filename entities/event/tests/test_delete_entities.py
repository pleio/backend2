from django.utils import timezone

from core.tests.helpers import PleioTenantTestCase
from entities.event.factories import EventFactory as EntityFactory
from entities.event.models import Event
from entities.event.range.calculator import DailyRange
from entities.event.range.factory import complete_range
from user.factories import UserFactory


class Wrapper:
    class TestDeleteEntitiesBase(PleioTenantTestCase):
        def setUp(self):
            super().setUp()

            self.owner = UserFactory()
            self.mutation = """
                mutation deleteEntities($input: deleteEntitiesInput!) {
                    deleteEntities(input: $input) {
                        success
                    }
                }
            """

        def send_mutation(self, variables):
            self.graphql_client.force_login(self.owner)
            response = self.graphql_client.post(self.mutation, variables)
            return response["data"]["deleteEntities"]


class TestDeleteEvents(Wrapper.TestDeleteEntitiesBase):
    def setUp(self):
        super().setUp()

        self.single_event = EntityFactory(
            owner=self.owner,
            title="Single event",
        )
        self.event = EntityFactory(
            owner=self.owner,
            title="Parent event",
        )
        self.meeting = EntityFactory(
            owner=self.owner,
            parent=self.event,
            title="Meeting1",
        )
        self.meeting2 = EntityFactory(
            owner=self.owner,
            parent=self.event,
            title="Meeting2",
        )

    def test_delete_entities(self):
        data = self.send_mutation(
            {
                "input": {
                    "guids": [self.event.guid],
                }
            }
        )
        self.assertEqual(data, {"success": True})
        self.assertEqual([*Event.objects.all()], [self.single_event])


class TestDeleteRecurringEvents(Wrapper.TestDeleteEntitiesBase):
    def setUp(self):
        super().setUp()

        start_date = timezone.datetime.fromisoformat("2020-10-10T10:00:00.000000+01:00")
        repeating_event = EntityFactory(
            title="Recurring event",
            owner=self.owner,
            start_date=start_date,
            range_starttime=start_date,
            range_settings={
                "type": DailyRange.key,
                "interval": 1,
            },
        )
        complete_range(repeating_event, start_date, 5)
        self.repeating_events = [*Event.objects.get_full_range(repeating_event)]

    def test_delete_entities(self):
        # Given
        events_to_delete = [
            self.repeating_events[1],
            self.repeating_events[3],
        ]
        removed_dates = [e.start_date.date() for e in events_to_delete]
        all_dates = [e.start_date.date() for e in self.repeating_events]
        variables = {
            "input": {
                "guids": [r.guid for r in events_to_delete],
            }
        }

        # When
        data = self.send_mutation(variables)
        after_delete_dates = [e.start_date.date() for e in Event.objects.all()]

        # Then
        self.assertEqual(data, {"success": True})

        # And
        for date in removed_dates:
            with self.subTest(
                msg="Test that %s is no longer in the list of dates" % date
            ):
                self.assertNotIn(date, after_delete_dates)

        # And
        for date in [d for d in all_dates if d not in removed_dates]:
            with self.subTest(msg="Test that %s is still in the list of dates" % date):
                self.assertIn(date, after_delete_dates)
