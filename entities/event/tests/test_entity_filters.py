from unittest import mock

from core.tests.helpers.entity_filters import Template
from entities.blog.factories import BlogFactory
from entities.event.factories import EventFactory


class TestEntityFilters(Template.TestEntityFiltersTestCase):
    def get_subtype(self):
        return "event"

    def subtype_factory(self, **kwargs):
        return EventFactory(**kwargs)

    def reference_factory(self, **kwargs):
        return BlogFactory(**kwargs)

    @mock.patch("entities.activity.resolvers.query.complement_expected_range")
    def test_complement_extended_range_activities(self, complement_expected_range):
        # given.
        query = """query SomeQuery($subtypes: [String]) {
            activities(subtypes: $subtypes) {
                edges {
                    guid
                }
            }
        }
        """
        variables = {"subtypes": [self.get_subtype()]}

        # When.
        self.graphql_client.force_login(self.get_owner())
        self.graphql_client.post(query, variables)

        # Then.
        self.assertTrue(complement_expected_range.called)

    @mock.patch("entities.activity.resolvers.query.complement_expected_range")
    def test_complement_extended_range_activities_any(self, complement_expected_range):
        # Given.
        query = """query SomeQuery {
            activities {
                edges {
                    guid
                }
            }
        }
        """
        variables = {}

        # When
        self.graphql_client.force_login(self.get_owner())
        self.graphql_client.post(query, variables)

        self.assertFalse(complement_expected_range.called)

    @mock.patch("core.resolvers.queries.query_entities.complement_expected_range")
    def test_complement_extended_range_entities(self, complement_expected_range):
        """
        Complement the range if the eventFilter is set to upcoming.
        """
        # given.
        query = """query SomeQuery($subtypes: [String], $eventFilter: EventFilter) {
            entities(subtypes: $subtypes, eventFilter: $eventFilter) {
                edges {
                    guid
                }
            }
        }
        """
        variables = {"subtypes": [self.get_subtype()], "eventFilter": "upcoming"}

        # When.
        self.graphql_client.force_login(self.get_owner())
        self.graphql_client.post(query, variables)

        # Then.
        self.assertTrue(complement_expected_range.called)

    @mock.patch("core.resolvers.queries.query_entities.complement_expected_range")
    def test_no_complement_extended_range_entities(self, complement_expected_range):
        """
        Don't complement the range if the eventFilter is not set to upcoming.
        """
        # given.
        query = """query SomeQuery($subtypes: [String]) {
            entities(subtypes: $subtypes) {
                edges {
                    guid
                }
            }
        }
        """
        variables = {"subtypes": [self.get_subtype()]}

        # When.
        self.graphql_client.force_login(self.get_owner())
        self.graphql_client.post(query, variables)

        # Then.
        self.assertFalse(complement_expected_range.called)

    @mock.patch("core.resolvers.queries.query_entities.complement_expected_range")
    def test_complement_extended_range_entities_any(self, complement_expected_range):
        # given.
        query = """query SomeQuery {
            entities {
                edges {
                    guid
                }
            }
        }
        """
        variables = {}

        # When.
        self.graphql_client.force_login(self.get_owner())
        self.graphql_client.post(query, variables)

        # Then.
        self.assertFalse(complement_expected_range.called)

    @mock.patch("entities.event.resolvers.query.complement_expected_range")
    def test_complement_extended_range_events(self, complement_expected_range):
        # given.
        query = """query SomeQuery {
            entities {
                edges {
                    guid
                }
            }
        }
        """
        variables = {}

        # When.
        self.graphql_client.force_login(self.get_owner())
        self.graphql_client.post(query, variables)

        # Then.
        self.assertFalse(complement_expected_range.called)
