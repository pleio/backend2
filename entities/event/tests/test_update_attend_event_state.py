from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from core.utils.entity_form import EntityFormWrapper
from entities.event.factories import EventFactory
from entities.event.models import EventAttendee
from user.factories import UserFactory


class ConfirmAttendEventWithoutAccountTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.authenticated_user = UserFactory()
        self.event = EventFactory(
            owner=UserFactory(),
            location="place",
            location_link="test.com",
            location_address="Straat 10",
            max_attendees=1,
            attend_event_without_account=True,
            attendee_welcome_mail_subject="Some subject",
            attendee_welcome_mail_content=self.tiptap_paragraph("Welcome!"),
        )

        EventAttendee.objects.create(
            code="1234567890",
            email="pete@tenant.fast-test.com",
            event=self.event,
            state="unconfirmed",
        )
        EventAttendee.objects.create(
            code="1234567890",
            email="test@tenant.fast-test.com",
            event=self.event,
            state="unconfirmed",
        )

        self.mutation = """
            mutation updateAttendEventState($input: updateAttendEventStateInput!) {
                updateAttendEventState(input: $input) {
                    entity {
                        guid
                        attendees {
                            total
                            totalAccept
                            totalUnconfirmed
                            edges {
                                name
                            }
                        }
                        __typename
                    }
                    __typename
                }
            }
        """

    @mock.patch("entities.event.mail_builders.attendee_welcome_mail.send_mail")
    @mock.patch(
        "entities.event.resolvers.mutation_update_attend_event_state.submit_attend_event_confirm"
    )
    def test_confirm_attend_event_without_account_email(
        self, mocked_send_mail, mocked_welcome_mail
    ):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "pete@tenant.fast-test.com",
            }
        }

        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["attendees"]["edges"], []
        )
        self.assertEqual(1, mocked_send_mail.call_count)
        self.assertEqual(1, mocked_welcome_mail.call_count)
        self.assertEqual(
            mocked_welcome_mail.call_args.kwargs,
            {"attendee": self.event.attendees.get(email=variables["input"]["email"])},
        )

    def test_confirm_attend_event_is_full_without_account(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "pete@tenant.fast-test.com",
            }
        }

        EventAttendee.objects.create(
            event=self.event,
            state="accept",
            user=self.authenticated_user,
            email=self.authenticated_user.email,
        )

        with self.assertGraphQlError("event_is_full"):
            self.graphql_client.post(self.mutation, variables)

    def test_confirm_delete_attend_event_without_account(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "test@tenant.fast-test.com",
                "delete": True,
            }
        }

        self.assertEqual(
            self.event.attendees.filter(
                code="1234567890", email="test@tenant.fast-test.com"
            ).count(),
            1,
        )

        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["attendees"]["edges"], []
        )
        self.assertEqual(
            self.event.attendees.filter(
                code="1234567890", email="test@tenant.fast-test.com"
            ).count(),
            0,
        )

    def test_confirm_attend_event_is_full_without_account_waitinglist(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "state": "waitinglist",
                "email": "pete@tenant.fast-test.com",
            }
        }

        EventAttendee.objects.create(
            event=self.event,
            state="accept",
            user=self.authenticated_user,
            email=self.authenticated_user.email,
        )

        self.graphql_client.post(self.mutation, variables)
        self.assertEqual(self.event.attendees.filter(state="waitinglist").count(), 1)

    def test_confirm_attend_event_without_account_attendees(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "pete@tenant.fast-test.com",
            }
        }

        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["attendees"]["totalAccept"],
            1,
        )
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.filter(state="accept").count(), 1)
        self.assertEqual(self.event.attendees.filter(state="unconfirmed").count(), 1)


class TestUpdateAttendEventStateWithFormTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        # Populate the field id's in the serialize method.
        self.form_specs = EntityFormWrapper(
            {
                "title": "form_title",
                "description": "form_description",
                "fields": [
                    {
                        "title": "mandatory_field_title",
                        "description": "field_description",
                        "fieldType": "textField",
                        "isMandatory": True,
                    },
                    {
                        "title": "field_title",
                        "description": "field_description",
                        "fieldType": "selectField",
                        "isMandatory": False,
                        "fieldOptions": ["bert", "ernie"],
                    },
                ],
            }
        ).serialize()
        self.form = EntityFormWrapper(self.form_specs)
        self.form_field1, self.form_field2 = self.form.fields

        self.owner = UserFactory(email="owner@example.com", name="Owner")
        self.event = EventFactory(
            owner=self.owner,
            title="Master event",
            is_form_enabled=True,
            form_specs=self.form_specs,
        )

        EventAttendee.objects.create(
            code="1234567890",
            email="pete@tenant.fast-test.com",
            event=self.event,
            state="unconfirmed",
        )
        EventAttendee.objects.create(
            code="1234567890",
            email="test@tenant.fast-test.com",
            event=self.event,
            state="unconfirmed",
        )

        self.mutation = """
            mutation updateAttendEventState($input: updateAttendEventStateInput!) {
                updateAttendEventState(input: $input) {
                    entity {
                        guid
                        attendees {
                            total
                            totalAccept
                            totalUnconfirmed
                            edges {
                                email
                                name
                            }
                        }
                        __typename
                    }
                    success
                    fields {
                        guid
                        message
                    }
                }
            }
            """

    def test_confirm_attend_event_without_account_with_form(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "pete@tenant.fast-test.com",
                "formFields": [
                    {"guid": self.form_field1.guid, "value": "text_value"},
                    {"guid": self.form_field2.guid, "value": "bert"},
                ],
            }
        }

        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]

        self.assertEqual(
            data["updateAttendEventState"]["entity"]["guid"], self.event.guid
        )
        self.assertTrue(data["updateAttendEventState"]["success"])
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.filter(state="accept").count(), 1)
        self.assertEqual(self.event.attendees.filter(state="unconfirmed").count(), 1)

    def test_attend_event_without_all_mandatory_field(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "pete@tenant.fast-test.com",
                "formFields": [
                    {"guid": self.form_field2.guid, "value": "bert"},
                ],
            }
        }

        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]
        self.assertEqual(
            data["updateAttendEventState"]["entity"]["guid"], self.event.guid
        )
        self.assertFalse(data["updateAttendEventState"]["success"])
        self.assertEqual(
            data["updateAttendEventState"]["fields"][0]["guid"], self.form_field1.guid
        )
        self.assertEqual(
            data["updateAttendEventState"]["fields"][0]["message"],
            "Dit veld is verplicht",
        )
        self.assertFalse(data["updateAttendEventState"]["success"])
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.filter(state="accept").count(), 0)
        self.assertEqual(self.event.attendees.filter(state="unconfirmed").count(), 2)

    def test_attend_event_not_valid_select(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "code": "1234567890",
                "email": "pete@tenant.fast-test.com",
                "formFields": [
                    {"guid": self.form_field1.guid, "value": "text_value"},
                    {"guid": self.form_field2.guid, "value": "okidoki"},
                ],
            }
        }

        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]
        self.assertFalse(data["updateAttendEventState"]["success"])
        self.assertEqual(
            data["updateAttendEventState"]["fields"][0]["guid"], self.form_field2.guid
        )
        self.assertEqual(
            data["updateAttendEventState"]["fields"][0]["message"],
            "okidoki staat niet in veldopties. Probeer een van bert of ernie",
        )
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.filter(state="accept").count(), 0)
        self.assertEqual(self.event.attendees.filter(state="unconfirmed").count(), 2)
