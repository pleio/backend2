from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from entities.event.factories import EventFactory
from entities.event.models import EventAttendee
from entities.event.resolvers.shared import AttendeeMutation
from user.factories import UserFactory


class TestAttendeeMutationTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()

        self.parent_participant = UserFactory(name="Parent Participant")
        self.primary_participant = UserFactory(name="Primary Participant")
        self.secondary_participant = UserFactory(name="Secondary Participant")
        self.anonymous_email = "anonymous@example.com"
        self.anonymous_name = "Antony Mous"
        self.anonymous = {"email": self.anonymous_email, "name": self.anonymous_name}

        self.parent_event = EventFactory(owner=self.owner, title="Parent event")
        self.primary_child_event = EventFactory(
            owner=self.owner, title="Primary event", parent=self.parent_event
        )
        self.secondary_child_event = EventFactory(
            owner=self.owner, title="Secondary event", parent=self.parent_event
        )

        self.parent_attendee = self.parent_event.attendees.create(
            user=self.parent_participant, state="accept"
        )
        self.parent_anonymous = self.parent_event.attendees.create(
            **self.anonymous, state="accept"
        )
        self.primary_attendee = self.primary_child_event.attendees.create(
            user=self.primary_participant, state="accept"
        )
        self.primary_anonymous = self.primary_child_event.attendees.create(
            **self.anonymous, state="accept"
        )
        self.secondary_attendee = self.secondary_child_event.attendees.create(
            user=self.secondary_participant, state="accept"
        )
        self.secondary_anonymous = self.secondary_child_event.attendees.create(
            **self.anonymous, state="accept"
        )

    def tearDown(self):
        self.secondary_child_event.delete()
        self.primary_child_event.delete()
        self.parent_event.delete()

        self.parent_participant.delete()
        self.primary_participant.delete()
        self.secondary_participant.delete()
        self.owner.delete()

        super().tearDown()

    def test_query_me(self):
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, email=self.anonymous_email
                ).query_me()
            ],
            [self.parent_anonymous],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, user=self.parent_participant
                ).query_me()
            ],
            [self.parent_attendee],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, user=self.primary_participant
                ).query_me()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, user=self.secondary_participant
                ).query_me()
            ],
            [],
        )

        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, email=self.anonymous_email
                ).query_me()
            ],
            [self.primary_anonymous],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, user=self.parent_participant
                ).query_me()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, user=self.primary_participant
                ).query_me()
            ],
            [self.primary_attendee],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, user=self.secondary_participant
                ).query_me()
            ],
            [],
        )

        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, email=self.anonymous_email
                ).query_me()
            ],
            [self.secondary_anonymous],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, user=self.parent_participant
                ).query_me()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, user=self.primary_participant
                ).query_me()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, user=self.secondary_participant
                ).query_me()
            ],
            [self.secondary_attendee],
        )

    def test_query_attending_subevents(self):
        self.assertEqual(
            {
                *AttendeeMutation(
                    event=self.parent_event, email=self.anonymous_email
                ).query_attending_subevents()
            },
            {self.primary_anonymous, self.secondary_anonymous},
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, user=self.parent_participant
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, user=self.primary_participant
                ).query_attending_subevents()
            ],
            [self.primary_attendee],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.parent_event, user=self.secondary_participant
                ).query_attending_subevents()
            ],
            [self.secondary_attendee],
        )

        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, email=self.anonymous_email
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, user=self.parent_participant
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, user=self.primary_participant
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.primary_child_event, user=self.secondary_participant
                ).query_attending_subevents()
            ],
            [],
        )

        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, email=self.anonymous_email
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, user=self.parent_participant
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, user=self.primary_participant
                ).query_attending_subevents()
            ],
            [],
        )
        self.assertEqual(
            [
                *AttendeeMutation(
                    event=self.secondary_child_event, user=self.secondary_participant
                ).query_attending_subevents()
            ],
            [],
        )


class TestAttendeeMutationUnitTestsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory()
        self.participant = UserFactory(name="Participant")
        self.event = EventFactory(owner=self.owner, title="Event")
        self.attendee = self.event.attendees.create(
            user=self.participant, state="accept"
        )

    def tearDown(self):
        self.attendee.delete()
        self.event.delete()
        self.participant.delete()
        self.owner.delete()

        super().tearDown()

    @mock.patch("entities.event.resolvers.shared.AttendeeMutation.query_me")
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._delete_attendee")
    def test_delete_found(self, delete_attendee, query_me):
        query_me.return_value = EventAttendee.objects.filter(id=self.attendee.id)

        AttendeeMutation(event=self.event).delete()

        self.assertTrue(query_me.called)
        self.assertTrue(delete_attendee.called)
        self.assertEqual(delete_attendee.call_args.args, (self.attendee,))

    @mock.patch("entities.event.resolvers.shared.AttendeeMutation.query_me")
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._delete_attendee")
    def test_delete_not_found(self, delete_attendee, query_me):
        query_me.return_value = EventAttendee.objects.none()

        AttendeeMutation(event=self.event).delete()

        self.assertTrue(query_me.called)
        self.assertFalse(delete_attendee.called)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.query_attending_subevents"
    )
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._delete_attendee")
    def test_delete_from_subevents_found(
        self, delete_attendee, query_attending_subevents
    ):
        query_attending_subevents.return_value = EventAttendee.objects.filter(
            id=self.attendee.id
        )

        AttendeeMutation(event=self.event).delete_from_subevents()

        self.assertTrue(query_attending_subevents.called)
        self.assertTrue(delete_attendee.called)
        self.assertEqual(delete_attendee.call_args.args, (self.attendee,))

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.query_attending_subevents"
    )
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._delete_attendee")
    def test_delete_from_subevents_not_found(
        self, delete_attendee, query_attending_subevents
    ):
        query_attending_subevents.return_value = EventAttendee.objects.none()

        AttendeeMutation(event=self.event).delete_from_subevents()

        self.assertTrue(query_attending_subevents.called)
        self.assertFalse(delete_attendee.called)

    @mock.patch("entities.event.resolvers.shared.AttendeeMutation.query_me")
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._mutate_attendee")
    def test_mutate_state_found(self, mutate_attendee, query_me):
        query_me.return_value = EventAttendee.objects.filter(id=self.attendee.id)

        AttendeeMutation(event=self.event).mutate_state("faux")

        self.assertTrue(query_me.called)
        self.assertTrue(mutate_attendee.called)
        self.assertEqual(mutate_attendee.call_args.args, (self.attendee, "faux"))

    @mock.patch("entities.event.resolvers.shared.AttendeeMutation.query_me")
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._mutate_attendee")
    def test_mutate_state_not_found(self, mutate_attendee, query_me):
        query_me.return_value = EventAttendee.objects.none()

        AttendeeMutation(event=self.event).mutate_state("faux")

        self.assertTrue(query_me.called)
        self.assertFalse(mutate_attendee.called)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.query_attending_subevents"
    )
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._mutate_attendee")
    def test_mutate_state_subevents_found(
        self, mutate_attendee, query_attending_subevents
    ):
        query_attending_subevents.return_value = EventAttendee.objects.filter(
            id=self.attendee.id
        )

        AttendeeMutation(event=self.event).mutate_state_subevents("faux")

        self.assertTrue(query_attending_subevents.called)
        self.assertTrue(mutate_attendee.called)
        self.assertEqual(mutate_attendee.call_args.args, (self.attendee, "faux"))

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.query_attending_subevents"
    )
    @mock.patch("entities.event.resolvers.shared.AttendeeMutation._mutate_attendee")
    def test_mutate_state_subevents_not_found(
        self, mutate_attendee, query_attending_subevents
    ):
        query_attending_subevents.return_value = EventAttendee.objects.none()

        AttendeeMutation(event=self.event).mutate_state_subevents("faux")

        self.assertTrue(query_attending_subevents.called)
        self.assertFalse(mutate_attendee.called)

    @mock.patch("entities.event.models.EventAttendee.update_state")
    @mock.patch("entities.event.models.Event.process_waitinglist")
    def test_mutate_attendee(self, process_waitinglist, update_state):
        AttendeeMutation._mutate_attendee(self.attendee, "faux")

        self.assertTrue(update_state.called)
        self.assertTrue(process_waitinglist.called)
        self.assertEqual(update_state.call_args.args, ("faux",))

    @mock.patch("entities.event.models.EventAttendee.update_state")
    @mock.patch("entities.event.models.Event.process_waitinglist")
    def test_mutate_attendee_accept(self, process_waitinglist, update_state):
        AttendeeMutation._mutate_attendee(self.attendee, "accept")

        self.assertTrue(update_state.called)
        self.assertFalse(process_waitinglist.called)
        self.assertEqual(update_state.call_args.args, ("accept",))

    @mock.patch(
        "entities.event.mail_builders.delete_event_attendees.submit_delete_event_attendees_mail"
    )
    @mock.patch("entities.event.models.EventAttendee.as_mailinfo")
    @mock.patch("entities.event.models.EventAttendee.delete")
    def test_delete_attendee(
        self, delete, as_mailinfo, submit_delete_event_attendees_mail
    ):
        as_mailinfo.return_value = mock.MagicMock()

        AttendeeMutation._delete_attendee(self.attendee)

        self.assertTrue(as_mailinfo.called)
        self.assertTrue(delete.called)
        self.assertTrue(submit_delete_event_attendees_mail.called)
        self.assertEqual(
            submit_delete_event_attendees_mail.call_args.kwargs,
            {
                "event": self.event,
                "mail_info": as_mailinfo.return_value,
                "user": self.participant,
            },
        )
