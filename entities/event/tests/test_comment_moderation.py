from core.tests.helpers.test_comment_moderation import CommentModerationTestCases
from entities.event.factories import EventFactory


class TestAddCommentForModerationTestCase(
    CommentModerationTestCases.TestAddCommentForModerationTestCase
):
    type_to_string = "event"

    def entity_factory(self, **kwargs):
        return EventFactory(**kwargs)
