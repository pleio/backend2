from unittest import mock

from core.tests.helpers import PleioTenantTestCase
from core.utils.entity_form import EntityFormWrapper
from entities.event.factories import EventFactory
from entities.event.models import EventAttendee
from user.factories import UserFactory


class AttendEventTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticated_user = UserFactory()
        self.event = EventFactory(
            owner=self.authenticated_user,
            qr_access=True,
            attend_event_without_account=True,
            enable_attend_event_online=True,
            attendee_welcome_mail_subject="Some subject",
            attendee_welcome_mail_content=self.tiptap_paragraph("Welcome!"),
        )
        EventAttendee.objects.create(
            code="1234567890",
            email="pete@tenant.fast-test.com",
            event=self.event,
            state="unconfirmed",
        )

        self.mutation = """
            mutation AttendEvent($input: attendEventInput!) {
                attendEvent(input: $input) {
                    entity {
                        guid
                        attendees(state: "accept") {
                            edges {
                                name
                            }
                        }
                        __typename
                    }
                    __typename
                }
            }
        """
        self.variables = {"input": {"guid": self.event.guid, "state": "accept"}}

    @mock.patch("entities.event.mail_builders.attendee_welcome_mail.send_mail")
    @mock.patch("entities.event.mail_builders.qr_code.submit_mail_event_qr")
    def test_attend_event_with_qr_access(self, send_qr_mail, send_welcome_mail):
        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, self.variables)

        entity = result["data"]["attendEvent"]["entity"]
        self.assertEqual(
            entity["attendees"]["edges"][0]["name"], self.authenticated_user.name
        )
        self.assertTrue(send_qr_mail.called)
        self.assertTrue(send_welcome_mail.called)
        self.assertEqual(
            send_welcome_mail.call_args.kwargs,
            {"attendee": self.event.attendees.get(email=self.authenticated_user.email)},
        )

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.mutate_state_subevents"
    )
    def test_subscribe_for_events_and_subevents(self, mutate_state_subevents):
        self.variables["input"]["updateSubEvents"] = True

        self.graphql_client.force_login(self.authenticated_user)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertFalse(mutate_state_subevents.called)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.mutate_state_subevents"
    )
    def test_unsubscribe_for_events_and_subevents(self, mutate_state_subevents):
        self.variables["input"]["updateSubEvents"] = True
        self.variables["input"]["state"] = "reject"

        self.graphql_client.force_login(self.authenticated_user)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(mutate_state_subevents.called)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.mutate_state_subevents"
    )
    def test_unsubscribe_for_events_and_subevents_using_maybe(
        self, mutate_state_subevents
    ):
        self.variables["input"]["updateSubEvents"] = True
        self.variables["input"]["state"] = "maybe"

        self.graphql_client.force_login(self.authenticated_user)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(mutate_state_subevents.called)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.mutate_state_subevents"
    )
    def test_unsubscribe_for_events_and_subevents_using_online(
        self, mutate_state_subevents
    ):
        self.variables["input"]["updateSubEvents"] = True
        self.variables["input"]["state"] = "online"

        self.graphql_client.force_login(self.authenticated_user)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(mutate_state_subevents.called)


class TestIsAttendingEventsSubeventsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory(email="owner@example.com")
        self.participant = UserFactory(email="participant@example.com")

        self.master_event = EventFactory(owner=self.owner, title="Master event")
        self.sub_event = EventFactory(
            owner=self.owner, title="Sub event", parent=self.master_event
        )

        self.query = """
        query Events($masterGuid: String!, $subGuid: String) {
            master: entity(guid: $masterGuid) {
                ... on Event {
                    isAttending
                    isAttendingSubEvents
                }
            }
            sub: entity(guid: $subGuid) {
                ... on Event {
                    isAttending
                    isAttendingSubEvents
                }
            }
        }
        """
        self.variables = {
            "masterGuid": self.master_event.guid,
            "subGuid": self.sub_event.guid,
        }

    def test_all_events(self):
        EventAttendee.objects.create(
            state="accept", event=self.master_event, user=self.participant
        )
        EventAttendee.objects.create(
            state="accept", event=self.sub_event, user=self.participant
        )

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertTrue(result["data"]["master"]["isAttending"])
        self.assertTrue(result["data"]["master"]["isAttendingSubEvents"])
        self.assertTrue(result["data"]["sub"]["isAttending"])
        self.assertFalse(result["data"]["sub"]["isAttendingSubEvents"])

    def test_master_event_only(self):
        EventAttendee.objects.create(
            state="accept", event=self.master_event, user=self.participant
        )

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertTrue(result["data"]["master"]["isAttending"])
        self.assertFalse(result["data"]["master"]["isAttendingSubEvents"])
        self.assertFalse(result["data"]["sub"]["isAttending"])
        self.assertFalse(result["data"]["sub"]["isAttendingSubEvents"])

    def test_sub_event_only(self):
        EventAttendee.objects.create(
            state="accept", event=self.sub_event, user=self.participant
        )

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertFalse(result["data"]["master"]["isAttending"])
        self.assertTrue(result["data"]["master"]["isAttendingSubEvents"])
        self.assertTrue(result["data"]["sub"]["isAttending"])
        self.assertFalse(result["data"]["sub"]["isAttendingSubEvents"])

    def test_other_sub_event(self):
        other_subevent = EventFactory(owner=self.owner, parent=self.master_event)
        EventAttendee.objects.create(
            state="accept", event=other_subevent, user=self.participant
        )

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertFalse(result["data"]["master"]["isAttending"])
        self.assertTrue(result["data"]["master"]["isAttendingSubEvents"])
        self.assertFalse(result["data"]["sub"]["isAttending"])
        self.assertFalse(result["data"]["sub"]["isAttendingSubEvents"])


class TestAttendEventOnlineTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory(email="owner@example.com", name="Owner")
        self.participant = UserFactory(
            email="participant@example.com", name="Participant"
        )
        self.event = EventFactory(
            owner=self.owner, title="Master event", enable_attend_event_online=True
        )
        self.subevent_online = EventFactory(
            owner=self.owner,
            title="Sub event (online)",
            parent=self.event,
            enable_attend_event_online=True,
        )
        self.subevent_offline = EventFactory(
            owner=self.owner,
            title="Sub event (offline)",
            parent=self.event,
            enable_attend_event_online=False,
        )

    def test_attend_event_with_account(self):
        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(
            """
            mutation AttendEvent($input: attendEventInput!) {
                attendEvent(input: $input) {
                    entity {
                        guid
                        attendees {
                            total
                            totalOnline
                            edges {
                                guid
                                name
                            }
                        }
                    }
                }
            }
            """,
            {"input": {"guid": self.event.guid, "state": "online"}},
        )

        self.assertEqual(
            result["data"]["attendEvent"]["entity"],
            {
                "guid": self.event.guid,
                "attendees": {
                    "total": 1,
                    "totalOnline": 1,
                    "edges": [
                        {
                            "guid": self.participant.guid,
                            "name": "Participant",
                        }
                    ],
                },
            },
        )

    def test_reject_subevents_when_attending_online(self):
        """Check if sub event attendance cascades when rejecting the main event."""
        main_attend = EventAttendee.objects.create(
            state="accept", event=self.event, user=self.participant
        )
        online_attend = EventAttendee.objects.create(
            state="accept", event=self.subevent_online, user=self.participant
        )
        offline_reject = EventAttendee.objects.create(
            state="accept", event=self.subevent_offline, user=self.participant
        )

        mutation = """
            mutation AttendEvent($input: attendEventInput!) {
                attendEvent(input: $input) {
                    entity {
                        guid
                        attendees(state: "accept") {
                            edges {
                                name
                            }
                        }
                        __typename
                    }
                    __typename
                }
            }
        """
        variables = {
            "input": {
                "guid": self.event.guid,
                "state": "online",
                "updateSubEvents": True,
            }
        }

        self.graphql_client.force_login(self.participant)
        self.graphql_client.post(mutation, variables)

        main_attend.refresh_from_db()
        self.assertEqual(main_attend.state, "online")

        online_attend.refresh_from_db()
        self.assertEqual(online_attend.state, "online")

        offline_reject.refresh_from_db()
        self.assertEqual(offline_reject.state, "reject")


class TestAttendEventWithFormTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        specs = EntityFormWrapper(
            {
                "title": "form_title",
                "description": "form_description",
                "fields": [
                    {
                        "title": "mandatory_field_title",
                        "description": "field_description",
                        "fieldType": "textField",
                        "isMandatory": True,
                    },
                    {
                        "title": "field_title",
                        "description": "field_description",
                        "fieldType": "selectField",
                        "isMandatory": False,
                        "fieldOptions": ["bert", "ernie"],
                    },
                ],
            }
        ).serialize()
        self.form = EntityFormWrapper(specs)
        self.form_field1, self.form_field2 = self.form.fields

        self.owner = UserFactory(email="owner@example.com", name="Owner")
        self.participant = UserFactory(
            email="participant@example.com", name="Participant"
        )
        self.event = EventFactory(
            owner=self.owner,
            title="Master event",
            is_form_enabled=True,
            form_specs=self.form.serialize(),
        )

        self.mutation = """
            mutation AttendEvent($input: attendEventInput!) {
                attendEvent(input: $input) {
                    entity {
                        guid
                        attendees {
                            total
                            totalOnline
                            totalAccept
                            totalWaitinglist
                            edges {
                                guid
                                name
                            }
                        }
                    }
                    success
                    fields {
                        guid
                        message
                    }
                }
            }
            """

    def test_attend_event_with_form(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "state": "accept",
                "formFields": [
                    {"guid": self.form_field1.guid, "value": "text_value"},
                    {"guid": self.form_field2.guid, "value": "bert"},
                ],
            }
        }

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]
        self.assertEqual(data["attendEvent"]["entity"]["guid"], self.event.guid)
        self.assertTrue(data["attendEvent"]["success"])
        self.assertEqual(
            data["attendEvent"]["entity"]["attendees"]["edges"][0]["guid"],
            self.participant.guid,
        )
        self.assertEqual(
            EventAttendee.objects.filter(state__in=["accept", "waitinglist"]).count(), 1
        )

    def test_attend_event_without_all_mandatory_field(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "state": "accept",
                "formFields": [
                    {"guid": self.form_field2.guid, "value": "bert"},
                ],
            }
        }

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]
        self.assertEqual(data["attendEvent"]["entity"]["guid"], self.event.guid)
        self.assertFalse(data["attendEvent"]["success"])
        self.assertEqual(
            data["attendEvent"]["fields"][0]["guid"], self.form_field1.guid
        )
        self.assertEqual(
            data["attendEvent"]["fields"][0]["message"],
            "Dit veld is verplicht",
        )
        self.assertEqual(
            EventAttendee.objects.filter(state__in=["accept", "waitinglist"]).count(), 0
        )

    def test_attend_event_not_valid_select(self):
        variables = {
            "input": {
                "guid": self.event.guid,
                "state": "accept",
                "formFields": [
                    {"guid": self.form_field1.guid, "value": "text_value"},
                    {"guid": self.form_field2.guid, "value": "okidoki"},
                ],
            }
        }

        self.graphql_client.force_login(self.participant)
        result = self.graphql_client.post(self.mutation, variables)
        data = result["data"]
        self.assertFalse(data["attendEvent"]["success"])
        self.assertEqual(
            data["attendEvent"]["fields"][0]["guid"], self.form_field2.guid
        )
        self.assertEqual(
            data["attendEvent"]["fields"][0]["message"],
            "okidoki staat niet in veldopties. Probeer een van bert of ernie",
        )
        self.assertEqual(
            EventAttendee.objects.filter(state__in=["accept", "waitinglist"]).count(), 0
        )


class TestAttendeeFormResponse(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        specs = EntityFormWrapper(
            {
                "title": "form_title",
                "description": "form_description",
                "fields": [
                    {
                        "title": "mandatory_field_title",
                        "description": "field_description",
                        "fieldType": "textField",
                        "isMandatory": True,
                    },
                    {
                        "title": "field_title",
                        "description": "field_description",
                        "fieldType": "selectField",
                        "isMandatory": False,
                        "fieldOptions": ["bert", "ernie"],
                    },
                ],
            }
        ).serialize()
        self.form = EntityFormWrapper(specs)
        self.form_field1, self.form_field2 = self.form.fields

        self.owner = UserFactory(email="owner@example.com", name="Owner")
        self.event = EventFactory(
            owner=self.owner,
            title="Master event",
            is_form_enabled=True,
            form_specs=self.form.serialize(),
        )

        self.participant1 = self.create_participant("participant1", self.event)
        self.participant2 = self.create_participant("participant2", self.event)

        self.query = """
        query AttendeeFormResponse($guid: String!) {
            entity(guid: $guid) {
                ... on Event {
                    attendees {
                        edges {
                            name
                            email
                            formResponse {
                                guid
                                value
                            }
                        }
                    }
                }
            }
        }
        """
        self.variables = {"guid": self.event.guid}

    def create_participant(self, name, entity):
        participant = UserFactory(email=f"{name}@example.com", name=name)
        entity.attendees.create(user=participant, state="accept")
        entity.entity_form_user_answers.create(
            form_field=self.form_field1.guid, value="text_value", user=participant
        )
        entity.entity_form_user_answers.create(
            form_field=self.form_field2.guid, value="bert", user=participant
        )
        return participant

    def fetch_attendees(self, user):
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, self.variables)
        return result["data"]["entity"]["attendees"]["edges"]

    def test_attendee_form_response_as_owner(self):
        attendees = self.fetch_attendees(self.owner)

        self.assertEqual(len(attendees), 2)
        self.assertEqual(
            attendees,
            [
                {
                    "name": "participant1",
                    "email": "participant1@example.com",
                    "formResponse": [
                        {"guid": self.form_field1.guid, "value": "text_value"},
                        {"guid": self.form_field2.guid, "value": "bert"},
                    ],
                },
                {
                    "name": "participant2",
                    "email": "participant2@example.com",
                    "formResponse": [
                        {"guid": self.form_field1.guid, "value": "text_value"},
                        {"guid": self.form_field2.guid, "value": "bert"},
                    ],
                },
            ],
        )

    def test_attendee_form_response_as_participant(self):
        attendees = self.fetch_attendees(self.participant1)

        self.assertEqual(len(attendees), 2)
        self.assertEqual(
            attendees,
            [
                {
                    "name": "participant1",
                    "email": "",
                    "formResponse": [
                        {
                            "guid": self.form_field1.guid,
                            "value": "text_value",
                        },
                        {
                            "guid": self.form_field2.guid,
                            "value": "bert",
                        },
                    ],
                },
                {"name": "participant2", "email": "", "formResponse": []},
            ],
        )

    def test_attendee_form_response_as_participant2(self):
        attendees = self.fetch_attendees(self.participant2)

        self.assertEqual(len(attendees), 2)
        self.assertEqual(
            attendees,
            [
                {"name": "participant1", "email": "", "formResponse": []},
                {
                    "name": "participant2",
                    "email": "",
                    "formResponse": [
                        {
                            "guid": self.form_field1.guid,
                            "value": "text_value",
                        },
                        {
                            "guid": self.form_field2.guid,
                            "value": "bert",
                        },
                    ],
                },
            ],
        )
