from core.tests.helpers.test_translation import Wrapper
from entities.event.factories import EventFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Event"

    def build_entity(self, **kwargs):
        return EventFactory(**kwargs)
