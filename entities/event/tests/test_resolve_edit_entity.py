from unittest.mock import patch

from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, USER_ROLES
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_advanced_tab import TestAdvancedTab
from core.utils.entity_form import EntityFormWrapper
from entities.blog.factories import BlogFactory
from entities.event.factories import EventFactory
from entities.event.models import Event
from entities.event.range.factory import EventRangeFactory, complete_range
from user.factories import UserFactory
from user.models import User


class EditEventTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.authenticatedUser = mixer.blend(User)
        self.user2 = mixer.blend(User)
        self.admin = mixer.blend(User, roles=[USER_ROLES.ADMIN])
        self.group = mixer.blend(Group)
        self.suggested_item = BlogFactory(owner=self.authenticatedUser)

        specs = EntityFormWrapper(
            {
                "title": "old_form_title",
                "description": "old_form_description",
                "fields": [
                    {
                        "title": "old_form_field_title",
                        "description": "old_form_field_description",
                        "fieldType": "textField",
                        "isMandatory": True,
                    },
                    {
                        "title": "keep_form_field_title",
                        "description": "old_form_field_description",
                        "fieldType": "selectField",
                        "isMandatory": True,
                        "fieldOptions": ["bert", "ernie"],
                    },
                ],
            }
        ).serialize()

        self.eventPublic = Event.objects.create(
            title="Test public event",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            start_date=timezone.now(),
            end_date=timezone.now(),
            is_form_enabled=True,
            form_specs=specs,
        )

        self.refresh_entity_form_fields()

        self.video_call_moderator1 = UserFactory(name="Bert")
        self.video_call_moderator2 = UserFactory(name="Antony")
        self.video_call = mixer.blend("core.EntityVideoCall", entity=self.eventPublic)
        self.moderator = mixer.blend(
            "core.EntityVideoCallModerator",
            video_call=self.video_call,
            user=self.video_call_moderator1,
        )

        self.data = {
            "input": {
                "guid": self.eventPublic.guid,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "My first Event",
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "isRecommendedInSearch": True,
                "slotsAvailable": [{"name": "primary"}],
                "tags": ["tag1", "tag2"],
                "startDate": "2019-10-02T09:00:00+02:00",
                "endDate": "2019-10-02T10:00:00+02:00",
                "maxAttendees": "10",
                "location": "Utrecht",
                "locationLink": "maps.google.nl",
                "locationAddress": "Kerkstraat 10",
                "source": "https://www.pleio.nl",
                "ticketLink": "https://www.pleio-bookings.com/my-first-event",
                "attendEventWithoutAccount": True,
                "enableMaybeAttendEvent": False,
                "attendEventOnline": True,
                "rsvp": True,
                "qrAccess": True,
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "attendeeWelcomeMailSubject": "Mail Subject",
                "attendeeWelcomeMailContent": "Welcome Content",
                "suggestedItems": [self.suggested_item.guid],
                "videoCallModeratorUserGuids": [
                    self.video_call_moderator2.guid,
                ],
                "isFormEnabled": False,
                "form": {
                    "title": "form_title",
                    "description": "form_description",
                    "fields": [
                        {
                            "title": "textField_title",
                            "description": "textField_description",
                            "fieldType": "textField",
                            "isMandatory": True,
                            "fieldOptions": [],
                        },
                        {
                            "guid": self.form_field2.guid,
                            "title": "keep_form_field_title",
                            "description": "selectField_new_description",
                            "fieldType": "multiSelectField",
                            "isMandatory": False,
                            "fieldOptions": ["new_opt_1", "new_opt_2"],
                        },
                        {
                            "title": "multipleSelectField_title",
                            "description": "multipleSelectField_description",
                            "fieldType": "multiSelectField",
                            "isMandatory": True,
                            "fieldOptions": ["opt_1", "opt_2"],
                        },
                        {
                            "title": "dateField_title",
                            "description": "dateField_description",
                            "fieldType": "dateField",
                            "isMandatory": False,
                            "fieldOptions": [],
                        },
                        {
                            "title": "htmlField_title",
                            "description": "htmlField_description",
                            "fieldType": "htmlField",
                            "isMandatory": True,
                            "fieldOptions": [],
                        },
                    ],
                },
            }
        }
        self.mutation = """
            fragment EventParts on Event {
                title
                inputLanguage
                isTranslationEnabled
                richDescription
                timeCreated
                timeUpdated
                timePublished
                isRecommendedInSearch
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                slotsAvailable {
                    name
                    subEventGuids
                }
                canEdit
                canArchiveAndDelete
                tags
                url
                inGroup
                group {
                    guid
                }
                owner {
                    guid
                }
                rsvp
                source
                ticketLink
                attendEventWithoutAccount
                startDate
                endDate
                location
                locationLink
                locationAddress
                maxAttendees
                qrAccess
                attendeeWelcomeMailSubject
                attendeeWelcomeMailContent
                suggestedItems {
                    guid
                }
                enableMaybeAttendEvent
                videoCallEnabled
                videoCallModerators {
                    guid
                }
                attendEventOnline
                isFormEnabled
                form {
                    title
                    description
                    fields {
                        guid
                        title
                        description
                        fieldType
                        fieldOptions
                        isMandatory
                    }
                }
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                        guid
                        status
                        ...EventParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def refresh_entity_form_fields(self):
        self.form = EntityFormWrapper(self.eventPublic.form_specs)
        self.form_field1, self.form_field2 = self.form.fields[:2]

    def test_edit_event(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)
        self.assertMutationProcessedCorrectly(result)

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_event_by_admin(self):
        self.data["input"]["timeCreated"] = "2018-12-10T23:00:00.000Z"
        self.data["input"]["groupGuid"] = self.group.guid
        self.data["input"]["ownerGuid"] = self.user2.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.data)

        self.assertMutationProcessedCorrectly(result)

        # And...
        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")
        self.assertEqual(entity["isRecommendedInSearch"], True)
        self.assertEqual(entity["videoCallEnabled"], True)
        self.assertEqual(len(entity["videoCallModerators"]), 1)
        self.assertEqual(
            entity["videoCallModerators"][0]["guid"],
            self.video_call_moderator2.guid,
        )

    def assertMutationProcessedCorrectly(self, result):
        entity = result["data"]["editEntity"]["entity"]
        self.eventPublic.refresh_from_db()

        self.assertEqual(entity["inputLanguage"], self.data["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], self.data["input"]["title"])
        self.assertEqual(
            entity["richDescription"], self.data["input"]["richDescription"]
        )
        self.assertEqual(entity["startDate"], "2019-10-02T09:00:00+02:00")
        self.assertEqual(entity["endDate"], "2019-10-02T10:00:00+02:00")
        self.assertEqual(entity["maxAttendees"], self.data["input"]["maxAttendees"])
        self.assertEqual(entity["location"], self.data["input"]["location"])
        self.assertEqual(entity["locationLink"], self.data["input"]["locationLink"])
        self.assertEqual(
            entity["locationAddress"], self.data["input"]["locationAddress"]
        )
        self.assertEqual(entity["source"], self.data["input"]["source"])
        self.assertEqual(entity["ticketLink"], self.data["input"]["ticketLink"])
        self.assertEqual(
            entity["attendEventWithoutAccount"],
            self.data["input"]["attendEventWithoutAccount"],
        )
        self.assertEqual(entity["rsvp"], self.data["input"]["rsvp"])
        self.assertEqual(entity["qrAccess"], self.data["input"]["qrAccess"])
        self.assertEqual(
            entity["attendeeWelcomeMailSubject"],
            self.data["input"]["attendeeWelcomeMailSubject"],
        )
        self.assertEqual(
            entity["attendeeWelcomeMailContent"],
            self.data["input"]["attendeeWelcomeMailContent"],
        )
        self.assertDateEqual(
            entity["timePublished"], self.data["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], self.data["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], self.data["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(
            entity["slotsAvailable"][0]["name"],
            self.data["input"]["slotsAvailable"][0]["name"],
        )
        self.assertEqual(entity["suggestedItems"], [{"guid": self.suggested_item.guid}])
        self.assertEqual(
            entity["enableMaybeAttendEvent"],
            self.data["input"]["enableMaybeAttendEvent"],
        )
        self.assertEqual(
            entity["attendEventOnline"], self.data["input"]["attendEventOnline"]
        )

        self.assertEqual(entity["isFormEnabled"], False)
        self.assertEqual(entity["form"]["title"], "form_title")
        self.assertEqual(entity["form"]["description"], "form_description")
        self.assertEqual(entity["form"]["fields"][0]["title"], "textField_title")
        self.assertEqual(
            entity["form"]["fields"][0]["description"], "textField_description"
        )
        self.assertEqual(entity["form"]["fields"][0]["fieldType"], "textField")
        self.assertEqual(entity["form"]["fields"][0]["fieldOptions"], [])
        self.assertEqual(entity["form"]["fields"][0]["isMandatory"], True)

        self.assertEqual(entity["form"]["fields"][1]["guid"], self.form_field2.guid)
        self.assertEqual(entity["form"]["fields"][1]["title"], "keep_form_field_title")
        self.assertEqual(
            entity["form"]["fields"][1]["description"], "selectField_new_description"
        )
        self.assertEqual(entity["form"]["fields"][1]["fieldType"], "multiSelectField")

        self.refresh_entity_form_fields()
        self.assertEqual(self.form_field2.field_type, "multiSelectField")

        self.assertEqual(
            entity["form"]["fields"][1]["fieldOptions"], ["new_opt_1", "new_opt_2"]
        )
        self.assertEqual(entity["form"]["fields"][1]["isMandatory"], False)

    def test_edit_event_group_null_by_admin(self):
        variables = self.data
        variables["input"]["groupGuid"] = None

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["group"], None)

        self.eventPublic.refresh_from_db()
        self.assertEqual(entity["group"], None)

    def test_assign_subevent_to_slot(self):
        subevent: Event = EventFactory(parent=self.eventPublic)
        mutation = """
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                        ... on Event {
                            guid
                            slotsAvailable {
                                name
                                subEventGuids
                            }
                        }
                    }
                }
            }
        """
        self.graphql_client.force_login(self.eventPublic.owner)

        # assign to slot
        self.graphql_client.post(
            mutation,
            {
                "input": {
                    "guid": self.eventPublic.guid,
                    "slotsAvailable": [
                        {"name": "Some slot", "subEventGuids": [subevent.guid]},
                    ],
                },
            },
        )
        self.eventPublic.refresh_from_db()
        self.assertDictEqual(
            {"content": [{"id": 0, "name": "Some slot"}]},
            {"content": list(subevent.get_slots())},
        )

        # Remove from slot
        self.graphql_client.post(
            mutation,
            {
                "input": {
                    "guid": self.eventPublic.guid,
                    "slotsAvailable": [],
                },
            },
        )
        self.eventPublic.refresh_from_db()
        self.assertEqual([], list(subevent.get_slots()))


class TestEditRangeEventTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.now = timezone.datetime.fromisoformat("2019-01-01T09:00:00+01:00")
        self.timezone_now = patch("django.utils.timezone.now").start()
        self.timezone_now.return_value = self.now

        self.authenticated_user = mixer.blend(User)
        self.event = EventFactory(
            owner=self.authenticated_user,
            title="Initial title",
            start_date=self.now,
            end_date=self.now,
            range_starttime=self.now,
            range_settings={"type": "daily", "interval": 7},
        )
        self.following_event = EventRangeFactory(self.event).create_next_event()

        self.variables = {
            "input": {
                "guid": self.event.guid,
                "title": "New title",
                "rangeSettings": {
                    "type": "daily",
                    "interval": 7,
                    "updateRange": True,
                },
            },
        }
        self.mutation = """
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                        ... on Event {
                            guid
                            title
                            rangeSettings {
                                repeatUntil
                                instanceLimit
                                type
                                interval
                                isIgnored
                                nextEvent {
                                    guid
                                    title
                                    rangeSettings {
                                        isIgnored
                                    }
                                }
                            }
                        }
                    }
                }
            }
        """

    def test_edit_range_event(self):
        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editEntity"]["entity"]
        following = entity["rangeSettings"]["nextEvent"]

        self.assertEqual(entity["guid"], self.event.guid)
        self.assertEqual(entity["title"], "New title")
        self.assertEqual(following["title"], "New title")

    def test_edit_single_range_event(self):
        self.variables["input"]["rangeSettings"]["updateRange"] = False

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editEntity"]["entity"]
        following = entity["rangeSettings"]["nextEvent"]

        self.assertEqual(entity["guid"], self.event.guid)
        self.assertEqual(entity["title"], "New title")
        self.assertEqual(following["title"], "Initial title")

        self.assertEqual(entity["rangeSettings"]["isIgnored"], True)
        self.assertEqual(following["rangeSettings"]["isIgnored"], False)

    def test_set_instance_limit_greater_then_number_of_items(self):
        changing_item = Event.objects.get_full_range(self.event).first()
        self.variables["input"]["guid"] = changing_item.guid
        self.variables["input"]["rangeSettings"]["instanceLimit"] = 10
        self.assertEqual(Event.objects.count(), 2)

        self.graphql_client.force_login(self.event.owner)
        self.graphql_client.post(self.mutation, self.variables)
        self.assertTrue(Event.objects.count() > 1)

    def test_set_instance_limit_less_then_number_of_items(self):
        changing_item = Event.objects.get_full_range(self.event).last()
        self.variables["input"]["guid"] = changing_item.guid
        self.variables["input"]["rangeSettings"]["instanceLimit"] = 1
        self.assertEqual(Event.objects.count(), 2)

        with self.assertGraphQlError("event_invalid_repeat_instance_limit"):
            self.graphql_client.force_login(self.event.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_set_repeat_until_beyond_changing_items_date(self):
        changing_item = Event.objects.get_full_range(self.event).last()
        self.variables["input"]["guid"] = changing_item.guid
        self.variables["input"]["rangeSettings"]["repeatUntil"] = (
            changing_item.start_date + timezone.timedelta(days=10)
        ).isoformat()
        self.assertEqual(Event.objects.count(), 2)

        self.graphql_client.force_login(self.event.owner)
        self.graphql_client.post(self.mutation, self.variables)
        self.assertTrue(Event.objects.count() > 1)

    def test_set_repeat_until_less_then_changing_items_date(self):
        changing_item = Event.objects.get_full_range(self.event).last()
        self.variables["input"]["guid"] = changing_item.guid
        self.variables["input"]["rangeSettings"]["repeatUntil"] = (
            self.event.start_date.isoformat()
        )
        self.assertEqual(Event.objects.count(), 2)

        with self.assertGraphQlError("event_invalid_repeat_until_date"):
            self.graphql_client.force_login(self.event.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_change_interval_to_monthly(self):
        complete_range(self.event, timezone.now(), 10)
        changing_item = Event.objects.get_full_range(self.event)[4]
        earlier_before = [
            e.event.start_date for e in Event.objects.get_range_before(changing_item)
        ]
        self_and_later_before = [changing_item.start_date] + [
            e.event.start_date for e in Event.objects.get_range_after(changing_item)
        ]

        self.variables["input"]["guid"] = changing_item.guid
        self.variables["input"]["rangeSettings"]["type"] = "dayOfTheMonth"
        self.variables["input"]["rangeSettings"]["interval"] = 1

        self.graphql_client.force_login(self.event.owner)
        self.graphql_client.post(self.mutation, self.variables)

        earlier_after = [
            e.event.start_date for e in Event.objects.get_range_before(changing_item)
        ]
        self_and_later_after = [changing_item.start_date] + [
            e.event.start_date for e in Event.objects.get_range_after(changing_item)
        ]

        self.assertEqual(earlier_after, earlier_before)
        self.assertNotEqual(self_and_later_after, self_and_later_before)


class TestAdvancedTabAtEventsTestCase(TestAdvancedTab.TestCase):
    TYPE = "Event"

    def build_entity(self, **kwargs):
        return EventFactory(**kwargs)

    def build_child_entity(self):
        child_entity = self.build_entity(
            owner=self.entity_owner,
            group=self.group,
        )
        child_entity.parent = self.entity
        child_entity.created_at = self.ORIGINAL_CREATED_TIME
        child_entity.save()
        return child_entity

    def run_test_with_user(self, user):
        child_entity = self.build_child_entity()
        self.variables["input"]["guid"] = child_entity.guid
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(
            self.mutation,
            {**self.variables, "guid": child_entity.guid},
        )

        self.assertEqual(
            result["data"]["editEntity"]["entity"],
            {
                "canEdit": True,
                "canArchiveAndDelete": True,
                "canEditAdvanced": False,
                "canEditGroup": False,
                "timeCreated": self.ORIGINAL_CREATED_TIME,
                "group": {"guid": self.group.guid},
                "owner": {"guid": self.entity_owner.guid},
                "title": "My first update",
            },
        )

    def test_update_child_entity_as_owner(self):
        self.run_test_with_user(self.entity_owner)

    def test_update_child_entity_as_group_admin(self):
        self.run_test_with_user(self.group_admin)

    def test_update_child_entity_as_admin(self):
        self.run_test_with_user(self.admin)
