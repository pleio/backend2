from unittest import mock
from urllib.parse import urlencode

import faker
from django.utils import timezone
from mixer.backend.django import mixer

from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class TestAttendEventMailerTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        from entities.event.models import Event

        self.admin = AdminFactory()

        self.event = mixer.blend(
            Event,
            location=faker.Faker().sentence(),
            location_address=faker.Faker().sentence(),
            location_link=faker.Faker().url(),
            start_date=timezone.localtime() - timezone.timedelta(days=100, hours=10),
            attend_event_without_account=True,
        )
        self.user = UserFactory()

    @mock.patch(
        "entities.event.resolvers.mutation_attend_event.submit_attend_event_request"
    )
    def test_send_mail_called_when_attend_to_event_without_email(
        self, mocked_attend_event_mail
    ):
        from entities.event.models import EventAttendee

        query = """
        mutation AttendEventWithoutAccount($input: attendEventWithoutAccountInput!) {
            attendEventWithoutAccount(input: $input) {
                entity {
                    guid
                }
            }
        }
        """

        self.graphql_client.post(
            query,
            {
                "input": {
                    "guid": str(self.event.id),
                    "name": self.user.name,
                    "email": self.user.email,
                }
            },
        )

        attendee_request = EventAttendee.objects.get(
            event=self.event, email=self.user.email, state="unconfirmed"
        )

        assert mocked_attend_event_mail.called, (
            "Unexpectedly did not call submit_attend_event_request"
        )
        (kwargs,) = mocked_attend_event_mail.call_args.args
        self.assertEqual(
            4,
            len(kwargs),
            msg="Unexpectedly called with another set of keyword arguments",
        )
        self.assertEqual(kwargs["event"], self.event.guid)
        self.assertEqual(kwargs["email"], self.user.email)
        self.assertIn("language", kwargs)
        self.assertIn(f"/events/confirm/{self.event.guid}", kwargs["link"])
        self.assertIn(urlencode({"email": self.user.email}), kwargs["link"])
        self.assertIn(urlencode({"code": attendee_request.code}), kwargs["link"])

    @mock.patch(
        "entities.event.resolvers.mutation_invite_to_event.submit_attend_event_invite"
    )
    def test_send_mail_called_when_invited(self, mocked_submit_attend_event_invite):
        from entities.event.models import EventAttendee

        query = """
        mutation InviteToEvent($input: inviteToEventInput!) {
            inviteToEvent(input: $input) {
                event {
                    guid
                }
            }
        }
        """

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(
            query,
            {
                "input": {
                    "guid": str(self.event.id),
                    "users": [{"email": self.user.email}],
                }
            },
        )

        attendee_request = EventAttendee.objects.get(
            event=self.event, email=self.user.email, state="unconfirmed"
        )

        assert mocked_submit_attend_event_invite.called, (
            "Unexpectedly did not call submit_attend_event_invite"
        )
        (kwargs,) = mocked_submit_attend_event_invite.call_args.args
        self.assertEqual(
            5,
            len(kwargs),
            msg="Unexpectedly called with another set of keyword arguments",
        )
        self.assertEqual(kwargs["event"], self.event.guid)
        self.assertEqual(kwargs["email"], self.user.email)
        self.assertEqual(kwargs["invited_by"], self.admin.name)
        self.assertIn("language", kwargs)
        self.assertIn(f"/events/confirm/{self.event.guid}", kwargs["link"])
        self.assertIn(urlencode({"email": self.user.email}), kwargs["link"])
        self.assertIn(urlencode({"code": attendee_request.code}), kwargs["link"])

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_mail_builder_parameters(self, add_local_context, get_context):
        from entities.event.mail_builders.attend_event_request import (
            AttendEventRequestMailer,
        )

        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}
        LINK = faker.Faker().url()

        mailer = AttendEventRequestMailer(
            event=self.event.guid,
            email=self.user.email,
            language=self.user.get_language(),
            link=LINK,
        )

        self.assertIn(self.event.title, mailer.get_subject())
        self.assertEqual(mailer.get_receiver_email(), self.user.email)
        self.assertEqual(mailer.get_language(), self.user.get_language())
        self.assertEqual(mailer.get_template(), "email/attend_event_request.html")
        self.assertEqual(mailer.get_sender(), None)
        self.assertEqual(mailer.get_receiver(), None)

        context = mailer.get_context()
        self.assertEqual(8, len(context))
        self.assertEqual(context["link"], LINK)
        self.assertEqual(context["location"], self.event.location)
        self.assertEqual(context["locationAddress"], self.event.location_address)
        self.assertEqual(context["locationLink"], self.event.location_link)
        self.assertDateEqual(str(context["start_date"]), str(self.event.start_date))
        self.assertEqual(context["title"], self.event.title)
        self.assertEqual(context["get_context"], "mocked")
        self.assertEqual(context["add_local_context"], "mocked")

    @mock.patch("core.mail_builders.template_mailer.TemplateMailerBase.get_context")
    @mock.patch(
        "core.mail_builders.template_mailer.TemplateMailerBase.add_local_context"
    )
    def test_mail_builder_invite_parameters(self, add_local_context, get_context):
        from entities.event.mail_builders.attend_event_invite import (
            AttendEventInviteMailer,
        )

        add_local_context.return_value = {"add_local_context": "mocked"}
        get_context.return_value = {"get_context": "mocked"}
        LINK = faker.Faker().url()

        mailer = AttendEventInviteMailer(
            event=self.event.guid,
            email=self.user.email,
            language=self.user.get_language(),
            link=LINK,
        )

        self.assertIn(self.event.title, mailer.get_subject())
        self.assertEqual(mailer.get_receiver_email(), self.user.email)
        self.assertEqual(mailer.get_language(), self.user.get_language())
        self.assertEqual(mailer.get_template(), "email/attend_event_invite.html")
        self.assertEqual(mailer.get_sender(), None)
        self.assertEqual(mailer.get_receiver(), None)

        context = mailer.get_context()
        self.assertEqual(9, len(context))
        self.assertEqual(context["link"], LINK)
        self.assertEqual(context["location"], self.event.location)
        self.assertEqual(context["locationAddress"], self.event.location_address)
        self.assertEqual(context["locationLink"], self.event.location_link)
        self.assertEqual(context["invited_by"], None)
        self.assertDateEqual(str(context["start_date"]), str(self.event.start_date))
        self.assertEqual(context["title"], self.event.title)

        self.assertEqual(context["get_context"], "mocked")
        self.assertEqual(context["add_local_context"], "mocked")
