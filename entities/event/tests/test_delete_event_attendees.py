from unittest import mock

from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.tests.helpers import PleioTenantTestCase
from entities.event.models import Event, EventAttendee
from user.models import User


class DeleteEventAttendeesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.attendee_user = mixer.blend(User)
        self.owner = mixer.blend(User)
        self.admin = mixer.blend(User)
        self.admin.roles = ["ADMIN"]
        self.admin.save()
        self.event = mixer.blend(Event)
        self.event.owner = self.owner
        self.event.read_access = [ACCESS_TYPE.public]
        self.event.write_access = [ACCESS_TYPE.user.format(self.owner.id)]
        self.event.attend_event_without_account = True
        self.event.save()

        EventAttendee.objects.create(
            event=self.event, user=self.attendee_user, email=self.attendee_user.email
        )
        EventAttendee.objects.create(event=self.event, user=None, email="test1@test.nl")
        EventAttendee.objects.create(event=self.event, user=None, email="test2@test.nl")

        self.mutation = """
            mutation deleteEventAttendees($input: deleteEventAttendeesInput!) {
                deleteEventAttendees(input: $input) {
                    entity {
                        guid
                        __typename
                    }
                    __typename
                }
            }
        """

        self.variables = {
            "input": {
                "guid": self.event.guid,
                "emailAddresses": ["test2@test.nl", self.attendee_user.email],
            }
        }

    @mock.patch(
        "entities.event.mail_builders.delete_event_attendees.submit_delete_event_attendees_mail"
    )
    def test_delete_attendees_from_event_by_admin(self, mocked_send_mail):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.count(), 1)
        self.assertEqual(
            data["deleteEventAttendees"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(mocked_send_mail.call_count, 2)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.delete_from_subevents"
    )
    @mock.patch(
        "entities.event.mail_builders.delete_event_attendees.submit_delete_event_attendees_mail"
    )
    def test_delete_attendees_from_event_by_owner(
        self, mocked_send_mail, delete_from_subevents
    ):
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.count(), 1)
        self.assertEqual(
            data["deleteEventAttendees"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(mocked_send_mail.call_count, 2)
        self.assertFalse(delete_from_subevents.called)

    @mock.patch(
        "entities.event.mail_builders.delete_event_attendees.submit_delete_event_attendees_mail"
    )
    def test_delete_attendees_from_event_by_user(self, mocked_send_mail):
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.attendee_user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_delete_attendees_from_event_with_subevent(self):
        subevent = mixer.blend(Event, parent=self.event)

        mixer.blend(EventAttendee, user=self.attendee_user, event=subevent)

        self.assertEqual(subevent.attendees.count(), 1)

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.count(), 1)
        self.assertEqual(
            data["deleteEventAttendees"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(subevent.attendees.count(), 1)

    def test_delete_attendees_from_event(self):
        subevent = mixer.blend(Event, parent=self.event)

        attendee = mixer.blend(User)

        EventAttendee.objects.create(
            event=subevent,
            user=None,
            name=attendee.name,
            email=attendee.email,
        )

        self.assertEqual(subevent.attendees.count(), 1)

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        data = result["data"]
        self.event.refresh_from_db()
        self.assertEqual(self.event.attendees.count(), 1)
        self.assertEqual(
            data["deleteEventAttendees"]["entity"]["guid"], self.event.guid
        )
        self.assertEqual(subevent.attendees.count(), 1)

    @mock.patch(
        "entities.event.resolvers.shared.AttendeeMutation.delete_from_subevents"
    )
    def test_delete_attendees_from_sub_events(self, delete_from_subevents):
        self.variables["input"]["updateSubEvents"] = True

        self.graphql_client.force_login(self.owner)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertTrue(delete_from_subevents.called)
