from core.tests.helpers.test_group_content import Template
from entities.event.factories import EventFactory
from user.factories import UserFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return EventFactory(**kwargs)

    def build_owner(self):
        return UserFactory()
