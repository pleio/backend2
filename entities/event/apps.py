from django.apps import AppConfig


class EventConfig(AppConfig):
    name = "entities.event"
    label = "event"
