from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.resolvers import shared
from entities.event.models import Event
from entities.event.resolvers import shared as event_shared

mutation = ObjectType("Mutation")


@mutation.field("deleteEventAttendees")
def resolve_delete_event_attendees(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    try:
        event = Event.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    shared.assert_write_access(event, user)

    for email_address in clean_input.get("emailAddresses"):
        attendee_mutation = event_shared.AttendeeMutation(
            event=event, email=email_address
        )
        attendee_mutation.delete()
        if clean_input.get("updateSubEvents"):
            attendee_mutation.delete_from_subevents()

    return {"entity": event}
