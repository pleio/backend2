from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core import constances
from core.lib import clean_graphql_input
from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from entities.event.models import Event
from entities.event.resolvers import shared as event_shared

mutation = ObjectType("Mutation")


def resolve_add_event(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(
        input, ["attendeeWelcomeMailSubject", "attendeeWelcomeMailContent"]
    )

    group = shared.get_group(clean_input)
    parent = None

    if "containerGuid" in clean_input:
        try:
            parent = Event.objects.get(id=clean_input.get("containerGuid"))
            if isinstance(parent.parent, Event):
                msg = "SUBEVENT_OF_SUBEVENT"
                raise GraphQLError(msg)
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND_CONTAINER)

    if parent and parent.group:
        group = parent.group

    shared.assert_can_create(user, Event, group)
    event_shared.assert_valid_new_range(clean_input)

    entity = Event(owner=user, group=group, parent=parent)

    track_publication_date = ContentModerationTrackTimePublished(
        entity, user, is_new=True
    )

    shared.resolve_add_input_language(entity, clean_input)
    shared.resolve_add_access_id(entity, clean_input, user)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_abstract(entity, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.update_featured_image(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.resolve_add_suggested_items(entity, clean_input)
    shared.update_is_form_enabled(entity, clean_input)
    shared.update_is_featured(entity, user, clean_input)
    shared.resolve_update_entity_form(entity, clean_input)

    event_shared.resolve_update_startenddate(entity, clean_input)
    event_shared.resolve_update_source(entity, clean_input)
    event_shared.resolve_update_location(entity, clean_input)
    event_shared.resolve_update_max_attendees(entity, clean_input)
    event_shared.resolve_update_ticket_link(entity, clean_input)
    event_shared.resolve_update_rsvp(entity, clean_input)
    event_shared.resolve_update_attend_without_account(entity, clean_input)
    event_shared.resolve_update_qr_access(entity, clean_input)
    event_shared.resolve_update_attendee_welcome_mail(entity, clean_input)
    event_shared.resolve_update_range_settings(entity, clean_input)
    event_shared.resolve_update_enable_maybe_attend_event(entity, clean_input)
    event_shared.resolve_update_enable_attend_event_online(entity, clean_input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    entity.add_follow(user)
    track_publication_date.maybe_create_publish_request()

    event_shared.resolve_update_slots_available(entity, clean_input)
    event_shared.followup_range_setting_changes(entity)
    shared.resolve_update_video_call_enabled(entity, clean_input)
    shared.resolve_update_video_call_moderators(entity, clean_input)

    return {"entity": entity}
