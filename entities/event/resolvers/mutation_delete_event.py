from core.resolvers.mutations.mutation_delete_entity import DefaultDeleteEntityResolver


class DeleteEventResolver(DefaultDeleteEntityResolver):
    def resolve(self):
        if self.entity.is_recurring:
            from entities.event.range.sync import EventRangeSync

            sync = EventRangeSync(self.entity)
            sync.pre_delete()

        return super().resolve()
