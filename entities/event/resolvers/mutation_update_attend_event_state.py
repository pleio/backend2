from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_email
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND, EVENT_IS_FULL, INVALID_EMAIL
from core.lib import clean_graphql_input
from core.resolvers import shared
from entities.event.mail_builders.attend_event_confirm import (
    submit_attend_event_confirm,
)
from entities.event.models import Event, EventAttendee
from entities.event.resolvers import shared as event_shared

mutation = ObjectType("Mutation")


@mutation.field("updateAttendEventState")
def resolve_update_attend_event_state(_, info, input):  # noqa: C901
    clean_input = clean_graphql_input(input)
    success = True
    fields = []

    email = clean_input.get("email")
    delete = clean_input.get("delete")
    # TODO: set default value for backwards compatibility, remove if frontend is altered
    state = clean_input.get("state", "accept")

    try:
        event = Event.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    event_shared.assert_valid_event_state(event, state)

    try:
        validate_email(email)
    except ValidationError:
        raise GraphQLError(INVALID_EMAIL)

    try:
        attendee = EventAttendee.objects.get(
            email=email, code=clean_input.get("code"), event=event
        )
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    # delete attendee and attendee request
    if delete:
        attendee.delete()

    # update attendee state
    else:
        if state != attendee.state:
            if state == "accept":
                if event.is_full():
                    raise GraphQLError(EVENT_IS_FULL)

            if state in ["accept", "waitinglist"] and attendee.state not in [
                "accept",
                "waitinglist",
            ]:
                if event.is_form_enabled and "formFields" in clean_input:
                    success, fields = shared.update_user_form(
                        event, attendee.user, clean_input["formFields"], attendee.email
                    )

            if success:
                attendee.update_state(state)

                submit_attend_event_confirm(attendee.id, attendee.code)

                if state != "accept":
                    event.process_waitinglist()

    return {"entity": event, "success": success, "fields": fields}
