from ariadne import ObjectType

from core.resolvers import decorators
from entities.event.resolvers.shared import AttendeeMutation

attendee = ObjectType("Attendee")


@attendee.field("guid")
def resolve_guid(obj, info):
    if obj.user:
        return obj.user.guid
    return ""


@attendee.field("email")
def resolve_email(obj, info):
    if obj.event.can_write(info.context["request"].user):
        return obj.email
    return ""


@attendee.field("timeCheckedIn")
def resolve_time_checked_in(obj, info):
    return obj.checked_in_at


@attendee.field("url")
def resolve_url(obj, info):
    return obj.user_url


@attendee.field("icon")
def resolve_icon(obj, info):
    return obj.user_icon


@attendee.field("isAttendingSubEvents")
def resolve_is_attending_sub_events(obj, info):
    mutation = AttendeeMutation(obj.event, obj.email, obj.user)
    return mutation.query_attending_subevents().exists()


@attendee.field("state")
def resolve_state(obj, info):
    return obj.state


@attendee.field("isInvited")
def resolve_is_invited(obj, info):
    return obj.is_invited


@attendee.field("formResponse")
@decorators.resolve_obj_request
def resolve_form_response(obj, request):
    acting_user = request.user
    if not (obj.event.can_write(acting_user) or obj.user == acting_user):
        return []

    response = []
    if obj.event.is_form_enabled:
        qs = obj.event.entity_form_user_answers.filter_respondent(obj.user, obj.email)
        for field in obj.event.get_form_fields():
            value = ""
            if qs.filter(form_field=field.guid).exists():
                value = qs.get(form_field=field.guid).value

            response.append(
                {
                    "guid": field.guid,
                    "value": value,
                }
            )
    return response
