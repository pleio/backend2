from .mutation_attend_event import mutation as _attend_event_resolvers
from .mutation_delete_event_attendees import (
    mutation as _delete_event_attendees_resolvers,
)
from .mutation_edit_event_attendee import mutation as _edit_event_attendee_resolvers
from .mutation_event_add import mutation as _event_add_resolvers
from .mutation_event_copy import mutation as _event_copy_resolvers
from .mutation_event_edit import mutation as _event_edit_resolvers
from .mutation_invite_to_event import mutation as _invite_to_event_resolvers
from .mutation_messages import mutation as _send_message_to_event_resolvers
from .mutation_update_attend_event_state import (
    mutation as _update_attend_event_state_resolvers,
)

resolvers = [
    _attend_event_resolvers,
    _delete_event_attendees_resolvers,
    _edit_event_attendee_resolvers,
    _event_add_resolvers,
    _event_copy_resolvers,
    _event_edit_resolvers,
    _invite_to_event_resolvers,
    _send_message_to_event_resolvers,
    _update_attend_event_state_resolvers,
]
