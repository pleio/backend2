from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_email
from django.db.models import Q
from graphql import GraphQLError

from core import config
from core.constances import COULD_NOT_FIND, NOT_AUTHORIZED
from core.lib import clean_graphql_input, generate_code, get_full_url
from entities.event.mail_builders.attend_event_invite import submit_attend_event_invite
from entities.event.models import Event, EventAttendee
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("inviteToEvent")
def resolve_invite_to_event(_, info, input):
    acting_user = info.context["request"].user
    clean_input = clean_graphql_input(input)

    try:
        event = Event.objects.visible(acting_user).get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    if not event.can_write(acting_user):
        raise GraphQLError(NOT_AUTHORIZED)

    mutation = InviteToEventMutation(clean_input, acting_user, event)
    mutation.process()

    return {"event": event, "status": mutation.report}


class InviteToEventMutation:
    class InviteUser:
        guid = None
        email = None
        user: User = None
        name = None
        status = None

        class InviteUserStatus:
            invited = "invited"
            invalidEmail = "invalidEmail"
            invalidUser = "invalidUser"
            alreadyExists = "alreadyExists"

        def __init__(self, input):
            self.guid = input.get("guid")
            self.email = input.get("email")
            self.check_input()

        def check_input(self):
            if self.guid:
                try:
                    self.user = User.objects.get(id=self.guid)
                except Exception:
                    self.status = self.InviteUserStatus.invalidUser
            elif self.email:
                if not self.check_email():
                    self.status = self.InviteUserStatus.invalidEmail
                else:
                    self.user = User.objects.filter(email=self.email).first()

            if self.user:
                self.name = self.user.name
                self.email = self.user.email
            elif self.email:
                self.name = self.email.split("@")[0]

        def check_email(self):
            try:
                validate_email(self.email)
                return True
            except ValidationError:
                return False

    def __init__(self, clean_input, acting_user, event):
        self.clean_input = clean_input
        self.acting_user = acting_user
        self.event = event
        self.report = []

    def process(self):
        for user_input in self.clean_input.get("users"):
            invite_user = self.InviteUser(user_input)

            self.mutation_invite_user(invite_user)

    def add_to_report(self, invited_user):
        self.report.append(
            {
                "item": invited_user.name or invited_user.email or invited_user.guid,
                "status": invited_user.status,
            }
        )

    def mutation_invite_user(self, invite_user):
        if not invite_user.status:
            resend = False

            attendee_filter = Q()
            if invite_user.user:
                attendee_filter.add(Q(user=invite_user.user), Q.AND)
            else:
                attendee_filter.add(Q(email=invite_user.email), Q.AND)
            attendee_filter.add(Q(event=self.event), Q.AND)

            exists = EventAttendee.objects.filter(attendee_filter).first()

            code = exists.code if exists else None

            if not exists:
                code = generate_code()
                EventAttendee.objects.create(
                    event=self.event,
                    user=invite_user.user,
                    email=invite_user.email,
                    name=invite_user.name,
                    code=code,
                    state="unconfirmed",
                    is_invited=True,
                )
            elif (
                exists.state == "unconfirmed" and self.clean_input.get("resend") is True
            ):
                code = exists.code
                resend = True
            else:  # already is an attendee
                invite_user.status = self.InviteUser.InviteUserStatus.alreadyExists

            if not exists or resend:
                invite_user.status = self.InviteUser.InviteUserStatus.invited
                submit_attend_event_invite(
                    {
                        "event": self.event.guid,
                        "email": invite_user.email,
                        "language": config.LANGUAGE,
                        "invited_by": self.acting_user.name,
                        "link": get_full_url(
                            f"/events/confirm/{self.event.guid}",
                            email=invite_user.email,
                            code=code,
                        ),
                    }
                )

        self.add_to_report(invite_user)
