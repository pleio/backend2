from ariadne import ObjectType
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import validate_email
from graphql import GraphQLError

from core import config
from core.constances import (
    COULD_NOT_FIND,
    EMAIL_ALREADY_USED,
    EVENT_IS_FULL,
    INVALID_EMAIL,
    NOT_AUTHORIZED,
    NOT_LOGGED_IN,
)
from core.lib import clean_graphql_input, generate_code, get_full_url
from core.resolvers import shared
from entities.event.lib import validate_name
from entities.event.mail_builders.attend_event_request import (
    submit_attend_event_request,
)
from entities.event.models import Event, EventAttendee
from entities.event.resolvers import shared as event_shared
from user.models import User

mutation = ObjectType("Mutation")


@mutation.field("attendEventWithoutAccount")
def resolve_attend_event_without_account(_, info, input):
    clean_input = clean_graphql_input(input, ["resend"])
    user = info.context["request"].user

    try:
        event = Event.objects.get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    try:
        validate_email(clean_input.get("email"))
    except ValidationError:
        raise GraphQLError(INVALID_EMAIL)

    if not event.can_add_attendees_by_email(user):
        raise GraphQLError(NOT_AUTHORIZED)

    if user.is_authenticated:
        resolve_add_email_to_event(event=event, clean_input=clean_input)
    else:
        resolve_require_confirmation(event=event, clean_input=clean_input)

    return {"entity": event}


def resolve_add_email_to_event(event, clean_input):
    email = clean_input.get("email")
    name = validate_name(clean_input.get("name"))
    user = User.objects.filter(email=email).first()

    if EventAttendee.objects.filter(event=event, email=email).exists():
        return

    attendee = EventAttendee.objects.create(
        event=event, user=user, email=email, name=user.name if user else name
    )
    attendee.update_state("accept")


def resolve_require_confirmation(event, clean_input):
    email = clean_input.get("email")
    name = validate_name(clean_input.get("name"))
    code = ""

    # silently fail if attendee already exists
    if (
        EventAttendee.objects.exclude(state="unconfirmed")
        .filter(event=event, email=email)
        .exists()
    ):
        return

    try:
        code = EventAttendee.objects.get(
            email=email, event=event, state="unconfirmed"
        ).code
    except ObjectDoesNotExist:
        pass

    if code and clean_input.get("resend") is not True:
        raise GraphQLError(EMAIL_ALREADY_USED)

    if not code:
        code = generate_code()
        existing_user = User.objects.filter(email=email).first()
        EventAttendee.objects.create(
            state="unconfirmed",
            code=code,
            user=existing_user,
            email=email,
            event=event,
            name=name,
        )

    submit_attend_event_request(
        {
            "event": event.guid,
            "email": email,
            "language": config.LANGUAGE,
            "link": get_full_url(
                f"/events/confirm/{event.guid}", email=email, code=code
            ),
        }
    )


@mutation.field("attendEvent")
def resolve_attend_event(_, info, input):  # noqa: C901
    user = info.context["request"].user
    clean_input = clean_graphql_input(input)
    success = True
    fields = []

    if not user.is_authenticated:
        raise GraphQLError(NOT_LOGGED_IN)

    try:
        event = Event.objects.visible(user).get(id=clean_input.get("guid"))
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    try:
        attendee = event.attendees.get(user=user)
    except ObjectDoesNotExist:
        attendee = None

    attendee_mutation = event_shared.AttendeeMutation(event=event, user=user)

    if not clean_input.get("state"):
        attendee_mutation.delete()
        if clean_input.get("updateSubEvents"):
            attendee_mutation.delete_from_subevents()
        return {"entity": event}

    if not attendee:
        attendee = EventAttendee.objects.create(
            event=event, user=user, email=user.email, name=user.name
        )

    event_shared.assert_valid_event_state(event, clean_input["state"])

    if clean_input["state"] == "accept" and attendee.state != "accept":
        if event.is_full():
            raise GraphQLError(EVENT_IS_FULL)

    if clean_input["state"] in ["accept", "waitinglist"] and attendee.state not in [
        "accept",
        "waitinglist",
    ]:
        if event.is_form_enabled and "formFields" in clean_input:
            success, fields = shared.update_user_form(
                event, attendee.user, clean_input["formFields"], attendee.email
            )
    if success:
        attendee_mutation.mutate_state(clean_input["state"])

        if clean_input["state"] in ("reject", "maybe", "online") and clean_input.get(
            "updateSubEvents"
        ):
            attendee_mutation.mutate_state_subevents(clean_input["state"])

    return {"entity": event, "success": success, "fields": fields}
