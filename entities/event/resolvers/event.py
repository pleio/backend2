from ariadne import ObjectType
from django.db.models import Case, Q, When

from core.constances import ATTENDEE_ORDER_BY, ENTITY_STATUS, ORDER_DIRECTION
from core.lib import datetime_isoformat
from core.models.videocall import EntityVideoCall
from core.resolvers import shared
from entities.event.models import Event, EventAttendee
from entities.event.resolvers import shared as event_shared


def conditional_state_filter(state):
    if state:
        return Q(state=state)

    return Q()


event = ObjectType("Event")


@event.field("subtype")
def resolve_subtype(obj, info):
    return "event"


@event.field("canEditAdvanced")
def resolve_can_edit_advanced(obj, info):
    return not obj.parent and shared.resolve_entity_can_edit_advanced(obj, info)


@event.field("canEditGroup")
def resolve_can_edit_group(obj, info):
    return not obj.parent and shared.resolve_entity_can_edit_group(obj, info)


@event.field("hasChildren")
def resolve_has_children(obj, info):
    return obj.has_children()


@event.field("children")
def resolve_children(obj, info):
    """
    Children fields published and isArchived are kept in sync with the parent event, see save in event/models.py
    """
    if obj.status_published == ENTITY_STATUS.PUBLISHED:
        qs = obj.children.visible(info.context["request"].user)
    else:
        qs = Event.objects.filter(parent=obj)
    return qs.order_by("start_date", "created_at")


@event.field("parent")
def resolve_parent(obj, info):
    return obj.parent


@event.field("slotsAvailable")
def resolve_slots_available(obj: Event, info):
    return obj.slots_available


@event.field("slots")
def resolve_slots(obj: Event, info):
    return [slot["name"] for slot in obj.get_slots()]


@event.field("canAttendWithEmail")
def resolve_can_attend_with_email(obj: Event, info):
    return obj.can_add_attendees_by_email(info.context["request"].user)


@event.field("alreadySignedUpInSlot")
def resolve_slot_already_signed_in_for_slot(obj, info):
    if not obj.parent:
        return None

    attending_events = event_shared.attending_events(info)
    return bool([n for n in obj.shared_via_slot if n in attending_events])


@event.field("isFeatured")
def resolve_is_featured(obj, info):
    return obj.is_featured


@event.field("isHighlighted")
def resolve_is_highlighted(obj, info):
    """Deprecated: not used in frontend"""
    return False


@event.field("isRecommended")
def resolve_is_recommended(obj, info):
    """Deprecated: not used in frontend"""
    return False


@event.field("url")
def resolve_url(obj, info):
    return obj.url


@event.field("startDate")
def resolve_start_date(obj, info):
    return datetime_isoformat(obj.start_date)


@event.field("endDate")
def resolve_end_date(obj, info):
    return datetime_isoformat(obj.end_date)


@event.field("source")
def resolve_source(obj, info):
    return obj.external_link


@event.field("location")
def resolve_location(obj, info):
    return obj.location


@event.field("locationLink")
def resolve_location_link(obj, info):
    return obj.location_link


@event.field("locationAddress")
def resolve_location_address(obj, info):
    return obj.location_address


@event.field("ticketLink")
def resolve_ticket_link(obj, info):
    return obj.ticket_link


@event.field("attendEventWithoutAccount")
def resolve_attend_event_without_account(obj, info):
    return obj.attend_event_without_account


@event.field("maxAttendees")
def resolve_max_attendees(obj, info):
    if obj.max_attendees:
        return str(obj.max_attendees)
    return None


@event.field("isAttending")
def resolve_is_attending(obj, info):
    user = info.context["request"].user

    if user.is_authenticated:
        attendee = obj.get_attendee(user.email)
        if attendee:
            return attendee.state

    return None


@event.field("isAttendingSubEvents")
def resolve_is_attending_subevents(obj, info):
    user = info.context["request"].user
    if user.is_authenticated:
        mutation = event_shared.AttendeeMutation(event=obj, user=user)
        return mutation.query_attending_subevents().exists()
    return None


@event.field("qrAccess")
def resolve_qr_access(obj, info):
    return obj.qr_access


@event.field("attendeeWelcomeMailSubject")
def resolve_attendee_welcome_mail_subject(obj, _):
    return obj.attendee_welcome_mail_subject


@event.field("attendeeWelcomeMailContent")
def resolve_attendee_welcome_mail(obj, _):
    return obj.attendee_welcome_mail_content


@event.field("attendEventOnline")
def resolve_attend_event_online(obj, info):
    return obj.enable_attend_event_online


@event.field("attendees")
def resolve_attendees(  # noqa: C901
    obj,
    info,
    query=None,
    limit=20,
    offset=0,
    state=None,
    orderBy=ATTENDEE_ORDER_BY.name,
    orderDirection=ORDER_DIRECTION.asc,
    isCheckedIn=None,
    isInvited=None,
):
    user = info.context["request"].user

    if not user.is_authenticated:
        return {
            "total": obj.attendees.count(),
            "totalAccept": obj.attendees.filter(state="accept").count(),
            "totalOnline": obj.attendees.filter(state="online").count(),
            "totalWaitinglist": obj.attendees.filter(state="waitinglist").count(),
            "edges": [],
        }

    qs = obj.attendees.all()

    qs = qs.annotate(
        names=Case(
            When(user=None, then="name"),
            default="user__name",
        )
    )
    qs = qs.annotate(
        emails=Case(
            When(user=None, then="email"),
            default="user__email",
        )
    )

    if query:
        qs = qs.filter(
            Q(names__icontains=query) | Q(emails__icontains=query) | Q(id__iexact=query)
        )

    qs = qs.filter(conditional_state_filter(state))

    if isCheckedIn is False:
        qs = qs.filter(checked_in_at__isnull=True)
    elif isCheckedIn is True:
        qs = qs.filter(checked_in_at__isnull=False)

    if isInvited is not None:
        qs = qs.filter(is_invited=isInvited)

    if orderBy == ATTENDEE_ORDER_BY.email:
        order_by = "email"
    elif orderBy == ATTENDEE_ORDER_BY.timeUpdated:
        order_by = "updated_at"
    elif orderBy == ATTENDEE_ORDER_BY.timeCheckedIn:
        order_by = "checked_in_at"
    elif orderBy == ATTENDEE_ORDER_BY.name:
        order_by = "names"

    if orderDirection == ORDER_DIRECTION.desc:
        order_by = "-%s" % (order_by)

    qs = qs.order_by(order_by)
    attendees = qs[offset : offset + limit]

    notCheckedIn = obj.attendees.filter(checked_in_at__isnull=True)

    return {
        "total": qs.count(),
        "edges": attendees,
        "totalAccept": obj.attendees.filter(state="accept").count(),
        "totalOnline": obj.attendees.filter(state="online").count(),
        "totalAcceptNotCheckedIn": notCheckedIn.filter(state="accept").count(),
        "totalWaitinglist": obj.attendees.filter(state="waitinglist").count(),
        "totalWaitinglistNotCheckedIn": notCheckedIn.filter(
            state="waitinglist"
        ).count(),
        "totalMaybe": obj.attendees.filter(state="maybe").count(),
        "totalReject": obj.attendees.filter(state="reject").count(),
        "totalCheckedIn": obj.attendees.filter(checked_in_at__isnull=False).count(),
        "totalUnconfirmed": obj.attendees.filter(state="unconfirmed").count(),
    }


@event.field("rangeSettings")
def resolve_range_settings(obj, info):
    if obj.is_recurring:
        return {"event": obj, **obj.range_settings}
    return None


@event.field("enableMaybeAttendEvent")
def resolve_enable_maybe_attend_event(obj, info):
    return obj.enable_maybe_attend_event


@event.field("videoCallUrl")
def resolve_video_call_url(obj, info):
    user = info.context["request"].user
    if user.is_authenticated:
        video_call = EntityVideoCall.objects.filter(entity=obj).first()
        if (
            video_call
            and user.is_authenticated
            and (
                obj.can_write(user)
                or video_call.is_moderator(user)
                or EventAttendee.objects.filter(
                    event=obj, state="accept", user=user
                ).exists()
            )
        ):
            return video_call.url
    return ""


event.set_field("inputLanguage", shared.resolve_entity_input_language)
event.set_field("guid", shared.resolve_entity_guid)
event.set_field("status", shared.resolve_entity_status)
event.set_field("title", shared.resolve_entity_title)
event.set_field("localTitle", shared.resolve_entity_local_title)
event.set_field("abstract", shared.resolve_entity_abstract)
event.set_field("localAbstract", shared.resolve_entity_local_abstract)
event.set_field("description", shared.resolve_entity_description)
event.set_field("localDescription", shared.resolve_entity_local_description)
event.set_field("richDescription", shared.resolve_entity_rich_description)
event.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
event.set_field("excerpt", shared.resolve_entity_excerpt)
event.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
event.set_field("tags", shared.resolve_entity_tags)
event.set_field("tagCategories", shared.resolve_entity_categories)
event.set_field("timeCreated", shared.resolve_entity_time_created)
event.set_field("timeUpdated", shared.resolve_entity_time_updated)
event.set_field("timePublished", shared.resolve_entity_time_published)
event.set_field("scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity)
event.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
event.set_field("statusPublished", shared.resolve_entity_status_published)
event.set_field("canEdit", shared.resolve_entity_can_edit)
event.set_field("canComment", shared.resolve_entity_can_comment)
event.set_field("canBookmark", shared.resolve_entity_can_bookmark)
event.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
event.set_field("accessId", shared.resolve_entity_access_id)
event.set_field("writeAccessId", shared.resolve_entity_write_access_id)
event.set_field("featured", shared.resolve_entity_featured)
event.set_field("canComment", shared.resolve_entity_can_comment)
event.set_field("comments", shared.resolve_entity_comments)
event.set_field("commentCount", shared.resolve_entity_comment_count)
event.set_field("views", shared.resolve_entity_views)
event.set_field("owner", shared.resolve_entity_owner)
event.set_field("isPinned", shared.resolve_entity_is_pinned)
event.set_field("lastSeen", shared.resolve_entity_last_seen)
event.set_field("suggestedItems", shared.resolve_entity_suggested_items)
event.set_field("group", shared.resolve_entity_group)
event.set_field("inGroup", shared.resolve_entity_in_group)
event.set_field("showOwner", shared.resolve_entity_show_owner)
event.set_field("isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search)
event.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
event.set_field("publishRequest", shared.resolve_entity_publish_request)
event.set_field("videoCallEnabled", shared.resolve_entity_video_call_enabled)
event.set_field("videoCallModerators", shared.resolve_entity_video_call_moderators)
event.set_field("publishRequest", shared.resolve_entity_publish_request)
event.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
event.set_field("isFormEnabled", shared.resolve_entity_form_enabled)
event.set_field("form", shared.resolve_entity_form)
