from ariadne import ObjectType
from graphql import GraphQLError

from core.constances import COULD_NOT_FIND
from core.lib import clean_graphql_input
from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from core.utils.entity import load_entity_by_id
from entities.event.models import Event
from entities.event.resolvers import shared as event_shared

mutation = ObjectType("Mutation")


def resolve_edit_event(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [Event])
    track_publication_date = ContentModerationTrackTimePublished(entity, user)

    clean_input = clean_graphql_input(
        input, ["attendeeWelcomeMailSubject", "attendeeWelcomeMailContent"]
    )

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)
    event_shared.assert_valid_updated_range(entity, clean_input)

    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_abstract(entity, clean_input)
    shared.update_is_form_enabled(entity, clean_input)

    if "containerGuid" in clean_input:
        try:
            container = Event.objects.get(id=clean_input.get("containerGuid"))
            if isinstance(container.parent, Event):
                msg = "SUBEVENT_OF_SUBEVENT"
                raise GraphQLError(msg)

            entity.parent = container
            entity.group = container.group

        except Event.DoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)

    shared.resolve_update_input_language(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)

    shared.update_featured_image(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)

    shared.update_is_featured(entity, user, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.update_updated_at(entity)
    shared.resolve_update_suggested_items(entity, clean_input)
    shared.resolve_update_video_call_enabled(entity, clean_input)
    shared.resolve_update_video_call_moderators(entity, clean_input)
    shared.resolve_update_entity_form(entity, clean_input)

    event_shared.resolve_update_startenddate(entity, clean_input)
    event_shared.resolve_update_source(entity, clean_input)
    event_shared.resolve_update_location(entity, clean_input)
    event_shared.resolve_update_ticket_link(entity, clean_input)
    event_shared.resolve_update_max_attendees(entity, clean_input)
    event_shared.resolve_update_rsvp(entity, clean_input)
    event_shared.resolve_update_attend_without_account(entity, clean_input)
    event_shared.resolve_update_qr_access(entity, clean_input)
    event_shared.resolve_update_slots_available(entity, clean_input)
    event_shared.resolve_update_attendee_welcome_mail(entity, clean_input)
    event_shared.resolve_update_range_settings(entity, clean_input)
    event_shared.resolve_update_enable_maybe_attend_event(entity, clean_input)
    event_shared.resolve_update_enable_attend_event_online(entity, clean_input)

    if not entity.parent:
        if user.is_site_admin or (entity.group and entity.group.can_write(user)):
            shared.resolve_update_owner(entity, clean_input)
            shared.resolve_update_time_created(entity, clean_input)

        if user.is_site_admin:
            shared.resolve_update_group(entity, clean_input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    event_shared.followup_range_setting_changes(entity)
    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}
