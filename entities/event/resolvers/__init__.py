from .attendee import attendee
from .event import event
from .mutation import resolvers as mutation_resolvers
from .query import query as resolve_query
from .range_settings import range_settings
from .slot import slot

resolvers = [
    resolve_query,
    event,
    attendee,
    *mutation_resolvers,
    slot,
    range_settings,
]
