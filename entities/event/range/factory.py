import logging

from django.db import IntegrityError
from django.utils import timezone

from entities.event.models import Event
from entities.event.range.calculator import RangeCalculator

logger = logging.getLogger(__name__)


class EventRangeFactory:
    class EventRangeCompleteEffect(Exception):
        pass

    class DuplicateRepetitionEffect(Exception):
        pass

    def __init__(self, event: Event):
        self.event = event

    @property
    def last_event(self):
        return Event.objects.get_range_stopper(self.event) or self.event

    def get_last_referable(self):
        return Event.objects.get_range_last_referable(self.event) or self.last_event

    @property
    def repeat_until(self):
        return self.event.range_settings.get("repeatUntil")

    @property
    def instance_limit(self):
        return self.event.range_settings.get("instanceLimit")

    def assert_due_date_valid(self, next_starttime):
        if not self.repeat_until:
            return

        until = timezone.datetime.fromisoformat(self.repeat_until)
        if next_starttime <= until:
            return

        raise self.EventRangeCompleteEffect()

    def assert_instance_count_valid(self):
        if not self.instance_limit:
            return

        range_items = Event.objects.get_full_range(self.event)
        if self.instance_limit > range_items.count():
            return

        raise self.EventRangeCompleteEffect()

    def create_next_event(self):
        last = self.last_event
        last_referable = self.get_last_referable()
        next_starttime = RangeCalculator(last).next()

        create_fields = {
            "owner": self.event.owner,
            "range_master": last.range_master or self.event,
            "range_starttime": next_starttime,
            "range_settings": last_referable.range_settings,
            "index_item": False,
        }

        try:
            self.assert_due_date_valid(next_starttime)
            self.assert_instance_count_valid()

            next_event = Event(**create_fields)
            next_event.import_values(last_referable)
            next_event.save()
            return next_event
        except IntegrityError:
            # Het event dat ik toe probeer te voegen bestaat al. even zoeken.
            raise self.DuplicateRepetitionEffect()


def complete_range(reference, until, cycle=1):
    section = Event.objects.get_full_range(reference).filter(start_date__gte=until)
    if section.count() >= cycle:
        # Range is complete
        return

    try:
        factory = EventRangeFactory(Event.objects.get_range_stopper(reference))
        factory.create_next_event()

        complete_range(reference, until, cycle)
    except EventRangeFactory.EventRangeCompleteEffect:
        Event.objects.get_full_range(reference).update(range_closed=True)
    except EventRangeFactory.DuplicateRepetitionEffect:
        pass
