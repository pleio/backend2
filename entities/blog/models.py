from auditlog.registry import auditlog

from core.models import (
    ArticleMixin,
    AttachmentMixin,
    BookmarkMixin,
    CommentMixin,
    Entity,
    FollowMixin,
    MentionMixin,
    RevisionMixin,
    VoteMixin,
)
from core.models.featured import FeaturedCoverMixin
from core.models.mixin import RichDescriptionMediaMixin, TitleMixin


class Blog(
    RichDescriptionMediaMixin,
    TitleMixin,
    FeaturedCoverMixin,
    VoteMixin,
    BookmarkMixin,
    FollowMixin,
    CommentMixin,
    MentionMixin,
    AttachmentMixin,
    ArticleMixin,
    RevisionMixin,
    Entity,
):
    """
    Blog
    """

    class Meta:
        ordering = ["-published"]

    def __str__(self):
        return f"Blog[{self.title}]"

    def has_revisions(self):
        return True

    @property
    def type_to_string(self):
        return "blog"

    @property
    def url(self):
        if self.issue_references.exists():
            return "/magazine-issue/view/{}/{}/{}".format(
                self.issue_references.first().issue_id,
                self.guid,
                self.slug,
            ).lower()
        return "{}/blog/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "abstract": self.abstract or "",
            "featured": self.serialize_featured(),
            **super().serialize(),
        }


auditlog.register(Blog)
