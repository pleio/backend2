from unittest.mock import patch

from django.contrib.auth.models import AnonymousUser
from django.test import tag
from django.utils import timezone
from django.utils.translation import activate, deactivate
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from entities.news.models import News
from user.factories import AdminFactory, UserFactory
from user.models import User


@tag("createEntity")
class AddBlogTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            EXTRA_LANGUAGES=["en", "nl"],
            LANGUAGE="en",
        )
        activate("en")

        self.anonymousUser = AnonymousUser()
        self.authenticatedUser = mixer.blend(User)
        self.admin = mixer.blend(User, roles=["ADMIN"])
        self.group = mixer.blend(
            Group, owner=self.authenticatedUser, is_membership_on_request=False
        )
        self.group.join(self.authenticatedUser, "owner")
        self.suggestedBlog = mixer.blend(Blog)
        self.suggestedNews = mixer.blend(News)

        self.data = {
            "input": {
                "subtype": "blog",
                "inputLanguage": "en",
                "title": "My first Blog",
                "richDescription": "richDescription",
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isRecommended": True,
                "isRecommendedInSearch": True,
                "isTranslationEnabled": False,
                "suggestedItems": [self.suggestedBlog.guid, self.suggestedNews.guid],
                "backgroundColor": "#000000",
            }
        }
        self.mutation = """
            fragment BlogParts on Blog {
                showOwner
                title
                inputLanguage
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canEditGroup
                canArchiveAndDelete
                tags
                url
                statusPublished
                inGroup
                group {
                    guid
                }
                isRecommended
                isRecommendedInSearch
                isTranslationEnabled
                suggestedItems {
                    guid
                }
                backgroundColor
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def tearDown(self):
        deactivate()
        super().tearDown()

    def test_add_blog(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["showOwner"], True)
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isRecommended"], False)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["statusPublished"], "published")
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"][0]["guid"], self.suggestedBlog.guid)
        self.assertEqual(entity["suggestedItems"][1]["guid"], self.suggestedNews.guid)
        self.assertEqual(entity["backgroundColor"], None)

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_add_blog_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.data)
        data = result["data"]

        self.assertEqual(
            data["addEntity"]["entity"]["title"], self.data["input"]["title"]
        )
        self.assertEqual(
            data["addEntity"]["entity"]["richDescription"],
            self.data["input"]["richDescription"],
        )
        self.assertEqual(
            data["addEntity"]["entity"]["tags"], self.data["input"]["tags"]
        )
        self.assertEqual(data["addEntity"]["entity"]["isRecommended"], True)
        self.assertEqual(data["addEntity"]["entity"]["isRecommendedInSearch"], True)
        self.assertEqual(data["addEntity"]["entity"]["isTranslationEnabled"], False)
        self.assertEqual(data["addEntity"]["entity"]["backgroundColor"], "#000000")

    def test_add_blog_to_group(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["addEntity"]["entity"]["title"], variables["input"]["title"]
        )
        self.assertEqual(
            data["addEntity"]["entity"]["richDescription"],
            variables["input"]["richDescription"],
        )
        self.assertEqual(data["addEntity"]["entity"]["inGroup"], True)
        self.assertEqual(data["addEntity"]["entity"]["group"]["guid"], self.group.guid)

    def test_add_unpublished_blog_admin(self):
        variables = {
            "input": {
                "subtype": "blog",
                "title": "My first Blog",
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isRecommended": True,
                "timePublished": None,
            }
        }
        mutation = """
            fragment BlogParts on Blog {
                title
                richDescription
                timeCreated
                timeUpdated
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                url
                inGroup
                timePublished
                statusPublished
                group {
                    guid
                }
                isRecommended
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(mutation, variables)

        data = result["data"]
        self.assertEqual(
            data["addEntity"]["entity"]["title"], variables["input"]["title"]
        )
        self.assertEqual(
            data["addEntity"]["entity"]["richDescription"],
            variables["input"]["richDescription"],
        )
        self.assertEqual(
            data["addEntity"]["entity"]["tags"], variables["input"]["tags"]
        )
        self.assertEqual(data["addEntity"]["entity"]["isRecommended"], True)
        self.assertEqual(data["addEntity"]["entity"]["timePublished"], None)
        self.assertEqual(data["addEntity"]["entity"]["statusPublished"], "draft")

    def test_add_minimal_entity(self):
        variables = {
            "input": {
                "title": "Simple blog",
                "subtype": "blog",
            }
        }

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])

    def test_add_with_attachment(self):
        attachment = self.file_factory(
            self.relative_path(__file__, ["assets", "featured.jpeg"])
        )

        variables = self.data
        variables["input"]["richDescription"] = self.tiptap_attachment(attachment)

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        blog = Blog.objects.get(id=entity["guid"])
        self.assertTrue(blog.attachments.filter(file_id=attachment.id).exists())


class TestAddToMagazineIssue(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = AdminFactory()
        self.magazine = MagazineFactory(
            owner=self.user, read_access=[ACCESS_TYPE.public]
        )
        self.issue = MagazineIssueFactory(
            container=self.magazine, owner=self.user, read_access=[ACCESS_TYPE.public]
        )

        self.mutation = """
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                        guid
                        ... on Blog {
                            accessId
                        }
                    }
                }
            }
        """
        self.variables = {
            "input": {
                "subtype": "blog",
                "title": "Blog",
                "containerGuid": self.issue.guid,
            }
        }

    def test_add_to_magazine_issue(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.mutation, self.variables)

        article_response = result["data"]["addEntity"]["entity"]
        articles = self.issue.get_articles(self.user)

        self.assertIsNotNone(articles)
        self.assertEqual(len(articles), 1)

        article = articles[0]
        self.assertEqual(article.title, "Blog")
        self.assertEqual(article_response["guid"], article.guid)

    def test_add_to_invalid_magazine_issue(self):
        another_blog = BlogFactory(owner=self.user)
        self.variables["input"]["containerGuid"] = another_blog.guid

        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_magazine_issue_without_authorization(self):
        user = UserFactory()

        with self.assertGraphQlError("not_authorized"):
            self.graphql_client.force_login(user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_magazine_issue_default_read_access_logged_in(self):
        self.override_config(DEFAULT_ACCESS_ID=1)

        self.graphql_client.force_login(self.user)
        response = self.graphql_client.post(self.mutation, self.variables)
        article = response["data"]["addEntity"]["entity"]

        self.assertEqual(article["accessId"], 2)
