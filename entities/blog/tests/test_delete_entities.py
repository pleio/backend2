from core.tests.helpers.test_delete_entities import Wrapper
from entities.blog.factories import BlogFactory as EntityFactory


class TestDeleteEntities(Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return EntityFactory(**kwargs)
