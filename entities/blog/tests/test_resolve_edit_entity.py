from unittest.mock import patch

from django.utils import timezone
from django.utils.translation import activate, deactivate
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, TEXT_TOO_LONG
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_advanced_tab import TestAdvancedTab
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from entities.news.models import News
from user.factories import AdminFactory, UserFactory


class EditBlogTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(
            EXTRA_LANGUAGES=["en", "nl"],
            LANGUAGE="en",
        )
        activate("en")

        self.authenticatedUser = UserFactory()
        self.user2 = UserFactory()
        self.admin = AdminFactory()

        self.featured_file = self.file_factory(
            self.relative_path(__file__, ["assets", "featured.jpeg"])
        )
        self.group = mixer.blend(Group)
        self.blog = Blog.objects.create(
            title="Test public event",
            input_language="en",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_recommended=False,
            featured_image=self.featured_file,
            background_color="#000000",
        )
        self.blog_multiple_read_access = Blog.objects.create(
            title="Test private blog",
            rich_description="JSON to string",
            read_access=[
                ACCESS_TYPE.user.format(self.authenticatedUser.id),
                ACCESS_TYPE.public,
            ],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_recommended=False,
        )
        self.suggestedBlog = mixer.blend(Blog)
        self.suggestedNews = mixer.blend(News)

        self.mutation = """
            fragment BlogParts on Blog {
                title
                inputLanguage
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                timePublished
                statusPublished
                url
                inGroup
                group {
                    guid
                }
                owner {
                    guid
                }
                suggestedItems {
                    guid
                }
                isRecommended
                isRecommendedInSearch
                isTranslationEnabled
                featured {
                    image { guid }
                    alt
                }
                backgroundColor
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """

        self.variables = {
            "input": {
                "guid": self.blog.guid,
                "title": "My first Event",
                "inputLanguage": "nl",
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isRecommended": True,
                "isRecommendedInSearch": True,
                "isTranslationEnabled": False,
                "timeCreated": "2018-12-10T23:00:00.000Z",
                "groupGuid": self.group.guid,
                "ownerGuid": self.user2.guid,
                "timePublished": None,
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.suggestedBlog.guid, self.suggestedNews.guid],
                "backgroundColor": "#AAAAAA",
            }
        }
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def tearDown(self):
        deactivate()
        super().tearDown()

    def test_edit_blog(self):
        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, self.variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(
            entity["inputLanguage"], self.variables["input"]["inputLanguage"]
        )
        self.assertEqual(entity["title"], self.variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], self.variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], self.variables["input"]["tags"])
        self.assertEqual(
            entity["isRecommended"], False
        )  # only admin can set isRecommended
        self.assertEqual(entity["group"], None)
        self.assertEqual(entity["owner"]["guid"], self.authenticatedUser.guid)
        self.assertEqual(entity["timeCreated"], self.blog.created_at.isoformat())
        self.assertEqual(entity["statusPublished"], "draft")
        self.assertEqual(entity["timePublished"], None)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertDateEqual(
            entity["scheduleArchiveEntity"],
            self.variables["input"]["scheduleArchiveEntity"],
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"],
            self.variables["input"]["scheduleDeleteEntity"],
        )
        self.assertEqual(entity["suggestedItems"][0]["guid"], self.suggestedBlog.guid)
        self.assertEqual(entity["suggestedItems"][1]["guid"], self.suggestedNews.guid)
        self.assertEqual(entity["featured"]["image"]["guid"], self.featured_file.guid)
        self.assertEqual(
            entity["backgroundColor"],
            "#000000",
            msg="Background color unexpectedly changed by non-privileged user",
        )

        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_blog_by_admin(self):
        variables = self.variables
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isRecommended"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")
        self.assertEqual(entity["backgroundColor"], "#AAAAAA")

    def test_edit_blog_group_null_by_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, self.variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["group"]["guid"], self.group.guid)

        self.variables["input"]["groupGuid"] = None

        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertIsNone(entity["group"])

    def test_edit_blog_set_future_published(self):
        variables = self.variables
        variables["input"]["timePublished"] = "4018-12-10T23:00:00.000Z"

        self.graphql_client.force_login(self.authenticatedUser)

        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(
            entity["isRecommended"], False
        )  # only admin can set isRecommended
        self.assertEqual(entity["group"], None)
        self.assertEqual(entity["owner"]["guid"], self.authenticatedUser.guid)
        self.assertEqual(entity["timeCreated"], self.blog.created_at.isoformat())
        self.assertEqual(entity["statusPublished"], "draft")
        self.assertEqual(entity["timePublished"], "4018-12-10T23:00:00+00:00")

    def test_edit_abstract(self):
        variables = {
            "input": {
                "guid": self.blog.guid,
                "abstract": "intro",
                "richDescription": "description",
            }
        }
        mutation = """
            fragment BlogParts on Blog {
                abstract
                excerpt
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["abstract"], "intro")
        self.assertEqual(entity["excerpt"], "intro")

    def test_edit_without_abstract(self):
        variables = {
            "input": {
                "guid": self.blog.guid,
                "richDescription": "description",
            }
        }
        mutation = """
            fragment BlogParts on Blog {
                abstract
                excerpt
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["abstract"], None)
        self.assertEqual(entity["excerpt"], variables["input"]["richDescription"])

    def test_edit_abstract_too_long(self):
        variables = {
            "input": {
                "guid": self.blog.guid,
                "abstract": "x" * 321,
            }
        }
        mutation = """
            fragment BlogParts on Blog {
                abstract
                excerpt
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """

        self.graphql_client.force_login(self.authenticatedUser)

        with self.assertGraphQlError(TEXT_TOO_LONG):
            self.graphql_client.post(mutation, variables)

    def test_edit_owner_of_blog_by_admin(self):
        variables = {
            "input": {
                "guid": self.blog_multiple_read_access.guid,
                "ownerGuid": self.user2.guid,
            }
        }

        mutation = """
            fragment BlogParts on Blog {
                owner {
                    guid
                }
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...BlogParts
                    }
                }
            }
        """

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)

        self.blog_multiple_read_access.refresh_from_db()
        self.assertEqual(
            self.blog_multiple_read_access.read_access,
            [ACCESS_TYPE.public, ACCESS_TYPE.user.format(self.user2.id)],
        )
        self.assertEqual(
            self.blog_multiple_read_access.write_access,
            [ACCESS_TYPE.user.format(self.user2.id)],
        )

    def test_remove_featured_image(self):
        self.graphql_client.force_login(self.authenticatedUser)
        response = self.graphql_client.post(
            self.mutation, {"input": {"guid": self.blog.guid, "featured": {}}}
        )
        entity = response["data"]["editEntity"]["entity"]
        self.assertIsNone(entity["featured"]["image"])
        self.assertEqual(entity["featured"]["alt"], "")

    def test_update_featured_alt(self):
        self.graphql_client.force_login(self.authenticatedUser)
        response = self.graphql_client.post(
            self.mutation,
            {
                "input": {
                    "guid": self.blog.guid,
                    "featured": {
                        "imageGuid": self.featured_file.guid,
                        "alt": "Next Alt text",
                    },
                }
            },
        )
        entity = response["data"]["editEntity"]["entity"]
        self.assertEqual(entity["featured"]["image"]["guid"], self.featured_file.guid)
        self.assertEqual(entity["featured"]["alt"], "Next Alt text")


class TestAdvancedTabTestCase(TestAdvancedTab.TestCase):
    TYPE = "Blog"

    def build_entity(self, **kwargs):
        return BlogFactory(**kwargs)
