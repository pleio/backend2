from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory
from entities.wiki.factories import WikiFactory


class TestSearchWithExcludedContentTypesTestCase(
    Template.TestSearchWithExcludedContentTypesTestCase
):
    EXCLUDE_TYPES = ["blog"]

    def build_included_article(self, title):
        return WikiFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return BlogFactory(title=title, owner=self.owner)
