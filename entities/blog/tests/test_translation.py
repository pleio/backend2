from core.tests.helpers.test_translation import Wrapper
from entities.blog.factories import BlogFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Blog"

    def build_entity(self, **kwargs):
        return BlogFactory(**kwargs)
