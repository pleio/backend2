from unittest import mock

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_entity_with_publish_request import PublishRequestTestCases
from entities.blog.factories import BlogFactory
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import AdminFactory, UserFactory


class TestBlogEntityTestCase(PleioTenantTestCase):
    TITLE = "Demo title"
    CONTENT = "Demo content"
    ABSTRACT = "Demo abstract"

    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.blog = BlogFactory(
            owner=self.owner,
            title=self.TITLE,
            rich_description=self.CONTENT,
            abstract=self.ABSTRACT,
        )

    def tearDown(self):
        super().tearDown()

    @mock.patch("core.models.Entity.serialize")
    def test_serialize(self, serialize):
        serialize.return_value = {}

        self.assertEqual(
            self.blog.serialize(),
            {
                "abstract": self.ABSTRACT,
                "featured": {
                    "alt": "",
                    "image": None,
                    "positionY": 0,
                    "video": None,
                    "videoThumbnailUrl": None,
                    "videoTitle": "",
                    "caption": None,
                },
                "richDescription": self.CONTENT,
                "title": self.TITLE,
            },
        )

    def test_map_rich_text_fields(self):
        before = self.blog.serialize()
        expected = self.blog.serialize()
        expected["richDescription"] = "new %s" % self.CONTENT
        expected["abstract"] = "new %s" % self.ABSTRACT

        self.blog.map_rich_text_fields(lambda v: "new %s" % v)
        snapshot = self.blog.serialize()

        self.assertNotEqual(snapshot, before)
        self.assertEqual(snapshot, expected)


class TestEditEntityWithPublishRequest(
    PublishRequestTestCases.TestEditEntityWithPublishRequest
):
    TYPE_TO_STRING = "blog"

    def entity_factory(self, **kwargs):
        return BlogFactory(**kwargs)


class TestBlogPropertiesTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.blog = BlogFactory(owner=self.owner)

    def test_url(self):
        self.assertEqual(
            self.blog.url,
            "/blog/view/{}/{}".format(
                self.blog.guid,
                self.blog.slug,
            ).lower(),
        )

    def test_group_blog_url(self):
        group = GroupFactory(owner=self.owner)
        self.blog.group = group
        self.blog.save()

        self.assertEqual(
            self.blog.url,
            "{}/blog/view/{}/{}".format(
                group.url,
                self.blog.guid,
                self.blog.slug,
            ).lower(),
        )

    def test_magazine_article_url(self):
        owner = AdminFactory()
        magazine = MagazineFactory(owner=owner)
        issue = MagazineIssueFactory(owner=owner, container=magazine)
        issue.append_article(self.blog.guid)

        self.assertEqual(
            self.blog.url,
            "/magazine-issue/view/{}/{}/{}".format(
                issue.guid,
                self.blog.guid,
                self.blog.slug,
            ).lower(),
        )
