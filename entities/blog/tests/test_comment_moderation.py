from core.tests.helpers.test_comment_moderation import CommentModerationTestCases
from entities.blog.factories import BlogFactory


class TestAddCommentForModerationTestCase(
    CommentModerationTestCases.TestAddCommentForModerationTestCase
):
    type_to_string = "blog"

    def entity_factory(self, **kwargs):
        return BlogFactory(**kwargs)
