from django.test import tag

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_can_update_access_level import Wrapper
from entities.blog.factories import BlogFactory
from user.factories import AdminFactory, UserFactory


class Local:
    class BlogCanUpdateAccessLevelBaseClass(Wrapper.TestCanUpdateAccessLevelBaseClass):
        output_type = "Blog"
        input_type = "editEntityInput"
        update_call = "editEntity"

        def build_entity(self, **kwargs):
            return BlogFactory(**kwargs)


class AuthenticatedVisitorTestCase(Local.BlogCanUpdateAccessLevelBaseClass):
    """
    Authenticated visitors are not allowed to update the access id
    """

    def build_owner(self, **kwargs):
        return UserFactory(**kwargs)

    expected_access_id = 2


class SiteAdministratorsTestCase(Local.BlogCanUpdateAccessLevelBaseClass):
    """
    SiteAdministrators can update the access id
    """

    def build_owner(self, **kwargs):
        return AdminFactory(**kwargs)

    expected_access_id = 0


class AuthenticatedVisitorUpdatingGroupContentTestcase(
    Local.BlogCanUpdateAccessLevelBaseClass
):
    """
    Group-members can't update the visibility level.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.group = None

    def build_group(self):
        if not self.group:
            group_owner = UserFactory(
                name="Group Owner", email="group-owner@example.com"
            )
            self.group = GroupFactory(owner=group_owner)
            self.group.join(self.owner)
        return self.group

    def build_entity(self, **kwargs):
        """
        Active user is group member and content owner.
        """

        kwargs["group"] = self.build_group()
        return super().build_entity(**kwargs)

    expected_access_id = 2


class GroupOwnerUpdatingGroupContentTestCase(
    AuthenticatedVisitorUpdatingGroupContentTestcase
):
    """
    Act as group owner on content where a group member is owner of.
    """

    def acting_user(self):
        return self.group.owner

    expected_access_id = 0


class GroupAdminUpdatingGroupContentTestCase(GroupOwnerUpdatingGroupContentTestCase):
    """
    Act as group-admin on content where a group member is owner of.
    """

    def acting_user(self):
        acting_user = UserFactory()
        self.group.join(acting_user, "admin")
        return acting_user


@tag("createEntity")
class TestCreateContent(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(HIDE_ACCESS_LEVEL_SELECT=True)
        self.mutation = """
        mutation createEntity($input: addEntityInput!) {
            addEntity(input: $input) {
                entity {
                    ... on Blog {
                        title
                        accessId
                    }
                }
            }
        }
        """
        self.variables = {
            "input": {
                "title": "Simple blog",
                "subtype": "blog",
                "accessId": 0,
            }
        }

    def test_as_authenticated_visitor(self):
        owner = UserFactory()

        self.graphql_client.force_login(owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["title"], "Simple blog")
        self.assertEqual(entity["accessId"], 1)

    def test_as_site_admin(self):
        owner = AdminFactory()

        self.graphql_client.force_login(owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["title"], "Simple blog")
        self.assertEqual(entity["accessId"], 0)
