from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.entity_filters import Template
from entities.blog.factories import BlogFactory
from entities.discussion.factories import DiscussionFactory
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import AdminFactory


class TestEntityFilters(Template.TestEntityFiltersTestCase):
    def get_subtype(self):
        return "blog"

    def subtype_factory(self, **kwargs):
        return BlogFactory(**kwargs)

    def reference_factory(self, **kwargs):
        return DiscussionFactory(**kwargs)


class TestActivitiesExcludesMagazineArticles(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = AdminFactory()
        self.magazine = MagazineFactory(owner=self.user)
        self.issue = MagazineIssueFactory(owner=self.user, container=self.magazine)
        self.article = BlogFactory(owner=self.user)
        self.blog = BlogFactory(owner=self.user)

        self.issue.append_article(self.article.guid)
        self.query = """
        fragment ActivityResult on ActivityList {
            total
            edges {
                guid
            }
        }
        query QueryActivities {
            activities { ...ActivityResult }
        }
        """

    def test_query_activities(self):
        """
        Query activities should give back:
        - Magazines? No
        - Magazine issues? Yes
        - Magazine articles? No
        - Other articles? Yes
        """
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(self.query, {})
        self.assertEqual(result["data"]["activities"]["total"], 2)
        self.assertEqual(
            {e["guid"] for e in result["data"]["activities"]["edges"]},
            {f"activity:{self.blog.guid}", f"activity:{self.issue.guid}"},
        )
