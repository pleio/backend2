from core.tests.helpers.tags_testcase import Template
from entities.blog.models import Blog


class TestBlogTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "Blog"
    model = Blog
    subtype = "blog"

    def variables_add(self):
        return {
            "input": {
                "title": "Test blog",
                "subtype": "blog",
            }
        }

    include_entity_search = True
    include_activity_search = True
    include_site_search = True
