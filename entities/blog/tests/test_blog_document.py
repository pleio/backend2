from elasticsearch_dsl import Q, Search
from mixer.backend.django import mixer

from core.tests.helpers import ElasticsearchTestCase
from entities.blog.factories import BlogFactory
from user.models import User


class AddBlogTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.authenticatedUser = mixer.blend(User, name="Jan de Vries")
        self.data = BlogFactory(title="test", owner=self.authenticatedUser)

        self.populate_index()

    def test_blog_document(self):
        s = (
            Search(index="blog")
            .query(Q("simple_query_string", query="Jan", fields=["owner.name"]))
            .filter("term", tenant_name=self.tenant.schema_name)
        )
        response = s.execute()

        self.assertEqual(len(response), 1)
        for hit in response:
            self.assertEqual(hit.title, self.data.title)
            self.assertEqual(hit.owner.name, self.authenticatedUser.name)
