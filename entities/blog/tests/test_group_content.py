from core.tests.helpers.test_group_content import Template
from entities.blog.factories import BlogFactory
from user.factories import UserFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return BlogFactory(**kwargs)

    def build_owner(self):
        return UserFactory()
