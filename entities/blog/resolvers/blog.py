from ariadne import ObjectType

from core.resolvers import shared

blog = ObjectType("Blog")


@blog.field("subtype")
def resolve_subtype(obj, info):
    return "blog"


@blog.field("isFeatured")
def resolve_is_featured(obj, info):
    return obj.is_featured


@blog.field("isHighlighted")
def resolve_is_highlighted(obj, info):
    """Deprecated: not used in frontend"""
    return False


@blog.field("isRecommended")
def resolve_is_recommended(obj, info):
    return obj.is_recommended


@blog.field("url")
def resolve_url(obj, info):
    return obj.url


blog.set_field("guid", shared.resolve_entity_guid)
blog.set_field("status", shared.resolve_entity_status)
blog.set_field("inputLanguage", shared.resolve_entity_input_language)
blog.set_field("title", shared.resolve_entity_title)
blog.set_field("localTitle", shared.resolve_entity_local_title)
blog.set_field("abstract", shared.resolve_entity_abstract)
blog.set_field("localAbstract", shared.resolve_entity_local_abstract)
blog.set_field("description", shared.resolve_entity_description)
blog.set_field("localDescription", shared.resolve_entity_local_description)
blog.set_field("richDescription", shared.resolve_entity_rich_description)
blog.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
blog.set_field("excerpt", shared.resolve_entity_excerpt)
blog.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
blog.set_field("tags", shared.resolve_entity_tags)
blog.set_field("tagCategories", shared.resolve_entity_categories)
blog.set_field("timeCreated", shared.resolve_entity_time_created)
blog.set_field("timeUpdated", shared.resolve_entity_time_updated)
blog.set_field("timePublished", shared.resolve_entity_time_published)
blog.set_field("scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity)
blog.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
blog.set_field("statusPublished", shared.resolve_entity_status_published)
blog.set_field("canEdit", shared.resolve_entity_can_edit)
blog.set_field("canEditAdvanced", shared.resolve_entity_can_edit_advanced)
blog.set_field("canEditGroup", shared.resolve_entity_can_edit_group)
blog.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
blog.set_field("canComment", shared.resolve_entity_can_comment)
blog.set_field("canVote", shared.resolve_entity_can_vote)
blog.set_field("canBookmark", shared.resolve_entity_can_bookmark)
blog.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
blog.set_field("accessId", shared.resolve_entity_access_id)
blog.set_field("writeAccessId", shared.resolve_entity_write_access_id)
blog.set_field("featured", shared.resolve_entity_featured)
blog.set_field("votes", shared.resolve_entity_votes)
blog.set_field("hasVoted", shared.resolve_entity_has_voted)
blog.set_field("comments", shared.resolve_entity_comments)
blog.set_field("commentCount", shared.resolve_entity_comment_count)
blog.set_field("isFollowing", shared.resolve_entity_is_following)
blog.set_field("views", shared.resolve_entity_views)
blog.set_field("owner", shared.resolve_entity_owner)
blog.set_field("isPinned", shared.resolve_entity_is_pinned)
blog.set_field("suggestedItems", shared.resolve_entity_suggested_items)
blog.set_field("lastSeen", shared.resolve_entity_last_seen)
blog.set_field("group", shared.resolve_entity_group)
blog.set_field("inGroup", shared.resolve_entity_in_group)
blog.set_field("showOwner", shared.resolve_entity_show_owner)
blog.set_field("isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search)
blog.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
blog.set_field("publishRequest", shared.resolve_entity_publish_request)
blog.set_field("backgroundColor", shared.resolve_entity_background_color)
