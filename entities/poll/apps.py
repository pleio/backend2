from django.apps import AppConfig


class PollConfig(AppConfig):
    name = "entities.poll"
    label = "poll"
