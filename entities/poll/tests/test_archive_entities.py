from core.tests.helpers.test_archive_entities import Wrapper
from entities.poll.factories import PollFactory as EntityFactory


class TestArchiveEntities(Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return EntityFactory(**kwargs)
