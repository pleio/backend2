from core.tests.helpers.test_delete_entities import Wrapper
from entities.poll.factories import PollFactory as EntityFactory


class TestDeleteEntities(Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return EntityFactory(**kwargs)
