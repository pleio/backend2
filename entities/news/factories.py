from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from entities.news.models import News
from user.models import User


def NewsFactory(**attributes):
    owner = attributes.get("owner")
    group = attributes.get("group")
    assert isinstance(owner, User), "owner is a required property"
    assert News.can_add(owner, group), (
        "The owner should be member-editor or group owner."
    )
    attributes.setdefault("read_access", [ACCESS_TYPE.public])
    attributes.setdefault(
        "write_access", [ACCESS_TYPE.user.format(attributes["owner"].guid)]
    )
    return mixer.blend(News, **attributes)
