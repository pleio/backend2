# Generated by Django 4.2.16 on 2024-12-30 10:57

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("news", "0013_news_sub_title"),
    ]

    operations = [
        migrations.AddField(
            model_name="news",
            name="featured_video_thumbnail_url",
            field=models.TextField(blank=True, null=True),
        ),
    ]
