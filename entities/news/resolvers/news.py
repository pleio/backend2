from ariadne import ObjectType

from core.resolvers import shared

news = ObjectType("News")


@news.field("subtype")
def resolve_subtype(obj, info):
    return obj.type_to_string


@news.field("source")
def resolve_source(obj, info):
    return obj.source


@news.field("isFeatured")
def resolve_is_featured(obj, info):
    return obj.is_featured


@news.field("isHighlighted")
def resolve_is_highlighted(obj, info):
    """Deprecated: not used in frontend"""
    return False


@news.field("isRecommended")
def resolve_is_recommended(obj, info):
    """Deprecated: not by news"""
    return False


@news.field("url")
def resolve_url(obj, info):
    return obj.url


@news.field("canEditAdvanced")
def resolve_can_edit_advanced(obj, info):
    if shared.resolve_entity_can_edit(obj, info):
        return True
    actor = info.context["request"].user
    return obj.group and obj.group.is_news_editor(actor)


news.set_field("guid", shared.resolve_entity_guid)
news.set_field("inputLanguage", shared.resolve_entity_input_language)
news.set_field("status", shared.resolve_entity_status)
news.set_field("title", shared.resolve_entity_title)
news.set_field("localTitle", shared.resolve_entity_local_title)
news.set_field("abstract", shared.resolve_entity_abstract)
news.set_field("localAbstract", shared.resolve_entity_local_abstract)
news.set_field("description", shared.resolve_entity_description)
news.set_field("localDescription", shared.resolve_entity_local_description)
news.set_field("richDescription", shared.resolve_entity_rich_description)
news.set_field("localRichDescription", shared.resolve_entity_local_rich_description)
news.set_field("excerpt", shared.resolve_entity_excerpt)
news.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
news.set_field("tags", shared.resolve_entity_tags)
news.set_field("tagCategories", shared.resolve_entity_categories)
news.set_field("timeCreated", shared.resolve_entity_time_created)
news.set_field("timeUpdated", shared.resolve_entity_time_updated)
news.set_field("timePublished", shared.resolve_entity_time_published)
news.set_field("scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity)
news.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
news.set_field("statusPublished", shared.resolve_entity_status_published)
news.set_field("canEdit", shared.resolve_entity_can_edit)
news.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
news.set_field("canEditGroup", shared.resolve_entity_can_edit_group)
news.set_field("canComment", shared.resolve_entity_can_comment)
news.set_field("canVote", shared.resolve_entity_can_vote)
news.set_field("canBookmark", shared.resolve_entity_can_bookmark)
news.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
news.set_field("accessId", shared.resolve_entity_access_id)
news.set_field("writeAccessId", shared.resolve_entity_write_access_id)
news.set_field("featured", shared.resolve_entity_featured)
news.set_field("votes", shared.resolve_entity_votes)
news.set_field("hasVoted", shared.resolve_entity_has_voted)
news.set_field("canComment", shared.resolve_entity_can_comment)
news.set_field("comments", shared.resolve_entity_comments)
news.set_field("commentCount", shared.resolve_entity_comment_count)
news.set_field("isFollowing", shared.resolve_entity_is_following)
news.set_field("views", shared.resolve_entity_views)
news.set_field("owner", shared.resolve_entity_owner)
news.set_field("isPinned", shared.resolve_entity_is_pinned)
news.set_field("suggestedItems", shared.resolve_entity_suggested_items)
news.set_field("lastSeen", shared.resolve_entity_last_seen)
news.set_field("group", shared.resolve_entity_group)
news.set_field("inGroup", shared.resolve_entity_in_group)
news.set_field("showOwner", shared.resolve_entity_show_owner)
news.set_field("isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search)
news.set_field("isTranslationEnabled", shared.resolve_entity_is_translation_enabled)
news.set_field("publishRequest", shared.resolve_entity_publish_request)
news.set_field("backgroundColor", shared.resolve_entity_background_color)
