from ariadne import InterfaceType, ObjectType

from .news import news

resolvers = [news]
