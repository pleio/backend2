from unittest.mock import patch

from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, GROUP_MEMBER_ROLES
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_advanced_tab import TestAdvancedTab
from entities.news.factories import NewsFactory
from entities.news.models import News
from user.factories import AdminFactory, EditorFactory, NewsEditorFactory, UserFactory


class EditNewsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.authenticatedUser = UserFactory()
        self.user2 = UserFactory()
        self.editorUser = EditorFactory()
        self.admin = AdminFactory()
        self.group = GroupFactory(owner=self.admin)
        self.news = News.objects.create(
            title="Test public news",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_featured=False,
            background_color="#000000",
        )
        self.relatedNews1 = mixer.blend(News)
        self.relatedNews2 = mixer.blend(News)

        self.data = {
            "input": {
                "guid": self.news.guid,
                "title": "My first News item",
                "richDescription": "richDescription",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "source": "https://www.nos.nl",
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.relatedNews1.guid, self.relatedNews2.guid],
                "backgroundColor": "#AAAAAA",
            }
        }
        self.mutation = """
            fragment NewsParts on News {
                title
                richDescription
                inputLanguage
                isTranslationEnabled
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                url
                isFeatured
                isRecommendedInSearch
                source
                owner {
                    guid
                }
                suggestedItems {
                    guid
                }
                group {
                    guid
                }
                backgroundColor
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...NewsParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_edit_news(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )  # rich_description is changed when published
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(
            entity["isFeatured"], False
        )  # Only (news) editor or admin can set isFeatured
        self.assertEqual(entity["source"], variables["input"]["source"])
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"][0]["guid"], self.relatedNews1.guid)
        self.assertEqual(entity["suggestedItems"][1]["guid"], self.relatedNews2.guid)

        self.news.refresh_from_db()

        self.assertEqual(entity["title"], self.news.title)
        self.assertEqual(entity["richDescription"], self.news.rich_description)
        self.assertEqual(entity["tags"], self.news.tags)
        self.assertEqual(entity["isFeatured"], self.news.is_featured)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["source"], self.news.source)
        self.assertDateEqual(entity["timePublished"], str(self.news.published))
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], str(self.news.schedule_archive_after)
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], str(self.news.schedule_delete_after)
        )
        self.assertEqual(entity["backgroundColor"], "#000000")

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_news_editor(self):
        variables = self.data
        variables["input"]["title"] = "Update door editor"

        self.graphql_client.force_login(self.editorUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["source"], variables["input"]["source"])
        self.assertEqual(entity["owner"]["guid"], self.authenticatedUser.guid)
        self.assertEqual(entity["timeCreated"], self.news.created_at.isoformat())
        self.assertEqual(entity["backgroundColor"], "#AAAAAA")

        self.news.refresh_from_db()

        self.assertEqual(entity["title"], self.news.title)
        self.assertEqual(entity["richDescription"], self.news.rich_description)
        self.assertEqual(entity["tags"], self.news.tags)
        self.assertEqual(entity["isFeatured"], self.news.is_featured)
        self.assertEqual(entity["isRecommendedInSearch"], True)
        self.assertEqual(entity["source"], self.news.source)
        self.assertEqual(entity["owner"]["guid"], self.authenticatedUser.guid)
        self.assertEqual(entity["timeCreated"], self.news.created_at.isoformat())

    def test_edit_news_news_editor(self):
        news_editor = NewsEditorFactory()
        variables = self.data
        variables["input"]["ownerGuid"] = self.user2.guid
        variables["input"]["timeCreated"] = "2018-12-10T23:00:00.000Z"

        self.graphql_client.force_login(news_editor)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )  # rich_description is changed when published
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(
            entity["isFeatured"], True
        )  # Only (news) editor or admin can set isFeatured
        self.assertEqual(entity["source"], variables["input"]["source"])
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"][0]["guid"], self.relatedNews1.guid)
        self.assertEqual(entity["suggestedItems"][1]["guid"], self.relatedNews2.guid)
        self.assertEqual(entity["backgroundColor"], "#000000")

        self.news.refresh_from_db()

        self.assertEqual(entity["title"], self.news.title)
        self.assertEqual(entity["richDescription"], self.news.rich_description)
        self.assertEqual(entity["tags"], self.news.tags)
        self.assertEqual(entity["isFeatured"], self.news.is_featured)
        self.assertEqual(entity["source"], self.news.source)
        self.assertDateEqual(entity["timePublished"], str(self.news.published))
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], str(self.news.schedule_archive_after)
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], str(self.news.schedule_delete_after)
        )

    def test_edit_news_admin(self):
        variables = self.data
        variables["input"]["timeCreated"] = "2018-12-10T23:00:00.000Z"
        variables["input"]["ownerGuid"] = self.user2.guid
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["tags"], variables["input"]["tags"])
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)
        self.assertEqual(entity["source"], variables["input"]["source"])
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")
        self.assertEqual(entity["backgroundColor"], "#AAAAAA")

        self.news.refresh_from_db()

        self.assertEqual(entity["title"], self.news.title)
        self.assertEqual(entity["richDescription"], self.news.rich_description)
        self.assertEqual(entity["tags"], self.news.tags)
        self.assertEqual(entity["isFeatured"], self.news.is_featured)
        self.assertEqual(entity["source"], self.news.source)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")


class TestEditGroupNews(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(IS_CLOSED=False)

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.news = NewsFactory(group=self.group, owner=self.owner)

        self.mutation = """
        mutation EditNewsInGroup($input: editEntityInput!) {
            editEntity(input: $input) {
                entity { ...NewsParts }
            }
        }
        fragment NewsParts on News {
            guid
            group { guid }
            isFeatured
        }
        """
        self.variables = {
            "input": {
                "guid": self.news.guid,
            }
        }

    def test_edit_group_news_anonymously(self):
        """
        Test that anonymous users can't edit news.
        """
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_just_a_user(self):
        """
        Test that non-member-users can't edit news.
        """
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_just_an_editor(self):
        """
        Test that non-member-editors can't edit news.
        """
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(EditorFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_just_a_member(self):
        """
        Test that member-users can't edit news.
        """
        user = UserFactory()
        self.group.join(user)

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_news_editor(self):
        """
        Test that global news editor can't edit news in group.
        """
        news_editor = NewsEditorFactory()
        self.group.join(news_editor)

        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(news_editor)
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_member_editor(self):
        """
        Test that member-editors can edit news.
        """
        user = EditorFactory()
        self.group.join(user)

        with self.assertNotRaisesException():
            self.graphql_client.force_login(user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_member_news_editor(self):
        """
        Test that member-news-editors can edit news.
        """
        self.variables = {"input": {"guid": self.news.guid, "isFeatured": True}}
        user = UserFactory()
        self.group.join(user)
        self.group.members.filter(user=user).update(
            roles=[GROUP_MEMBER_ROLES.GROUP_NEWS_EDITOR]
        )

        with self.assertNotRaisesException():
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)
            # group news editor can not edit isFeatured
            self.assertEqual(
                result["data"]["editEntity"]["entity"]["isFeatured"], False
            )

    def test_edit_group_news_as_group_owner(self):
        """
        Test that member-users can edit news.
        """
        with self.assertNotRaisesException():
            self.graphql_client.force_login(self.owner)
            self.graphql_client.post(self.mutation, self.variables)

    def test_edit_group_news_as_privileged_user(self):
        """
        Test that member-users that have write access can edit news.
        """
        user = EditorFactory()
        self.news.write_access.append(ACCESS_TYPE.user.format(user.guid))
        self.news.save()

        with self.assertNotRaisesException():
            self.graphql_client.force_login(user)
            self.graphql_client.post(self.mutation, self.variables)


class TestAdvancedTabForNewsTestCase(TestAdvancedTab.TestCase):
    TYPE = "News"

    def build_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def build_entity(self, **kwargs):
        return NewsFactory(**kwargs)

    def test_update_entity_as_owner(self):
        self.graphql_client.force_login(self.entity_owner)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["editEntity"]["entity"]

        # At news: owners can always update some advanced properties.
        self.assertEqual(
            entity,
            {
                "canEdit": True,
                "canEditAdvanced": True,
                "canEditGroup": False,
                "canArchiveAndDelete": True,
                "timeCreated": self.UPDATED_CREATED_TIME,
                "group": {"guid": self.group.guid},
                "owner": {"guid": self.another_user.guid},
                "title": "My first update",
            },
        )
