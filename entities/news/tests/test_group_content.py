from core.tests.helpers.test_group_content import Template
from entities.news.factories import NewsFactory
from user.factories import EditorFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return NewsFactory(**kwargs)

    def build_owner(self):
        return EditorFactory()
