from unittest.mock import patch

from django.core.files.base import ContentFile
from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import GROUP_MEMBER_ROLES
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from entities.news.models import News
from user.factories import AdminFactory, EditorFactory, NewsEditorFactory, UserFactory
from user.models import User


@tag("createEntity")
class AddNewsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.authenticatedUser = mixer.blend(User)
        self.adminUser = AdminFactory()
        self.editorUser = EditorFactory()
        self.newsEditorUser = NewsEditorFactory()
        self.relatedNews1 = mixer.blend(News)
        self.relatedNews2 = mixer.blend(News)
        self.attachment = FileFactory(
            owner=self.authenticatedUser,
            upload=ContentFile(b"Content!\n", name="some-file.txt"),
        )

        self.data = {
            "input": {
                "subtype": "news",
                "title": "My first News",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "source": "https://www.pleio.nl",
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.relatedNews1.guid, self.relatedNews2.guid],
                "backgroundColor": "#000000",
            }
        }
        self.minimal_data = {
            "input": {
                "title": "Simple news",
                "subtype": "news",
            }
        }
        self.mutation = """
            fragment NewsParts on News {
                showOwner
                title
                inputLanguage
                isTranslationEnabled
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                url
                isFeatured
                isRecommendedInSearch
                source
                suggestedItems {
                    guid
                }
                backgroundColor
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...NewsParts
                    }
                }
            }
        """

        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def tearDown(self):
        super().tearDown()

    def test_add_news(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.authenticatedUser)
            self.graphql_client.post(self.mutation, self.data)

        self.assertEqual(self.graphql_client.result["data"]["addEntity"], None)
        self.assertFalse(self.mocked_revert_published.called)
        self.assertFalse(self.mocked_revert_published.called)

    def test_add_news_as_authorized_user(self):
        variables = self.data

        for user, msg in (
            (self.adminUser, "as admin"),
            (self.editorUser, "as editor"),
            (self.newsEditorUser, "as news editor"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, variables)

            entity = result["data"]["addEntity"]["entity"]
            self.assertEqual(entity["showOwner"], True, msg=msg)
            self.assertEqual(
                entity["inputLanguage"], variables["input"]["inputLanguage"], msg=msg
            )
            self.assertEqual(entity["isTranslationEnabled"], False)
            self.assertEqual(entity["title"], variables["input"]["title"], msg=msg)
            self.assertEqual(
                entity["richDescription"],
                variables["input"]["richDescription"],
                msg=msg,
            )
            self.assertEqual(entity["tags"], variables["input"]["tags"], msg=msg)
            self.assertEqual(entity["isFeatured"], True, msg=msg)
            self.assertEqual(entity["source"], variables["input"]["source"], msg=msg)
            self.assertDateEqual(
                entity["timePublished"], variables["input"]["timePublished"], msg=msg
            )
            self.assertDateEqual(
                entity["scheduleArchiveEntity"],
                variables["input"]["scheduleArchiveEntity"],
                msg=msg,
            )
            self.assertDateEqual(
                entity["scheduleDeleteEntity"],
                variables["input"]["scheduleDeleteEntity"],
                msg=msg,
            )
            self.assertEqual(
                entity["suggestedItems"][0]["guid"], self.relatedNews1.guid, msg=msg
            )
            self.assertEqual(
                entity["suggestedItems"][1]["guid"], self.relatedNews2.guid, msg=msg
            )

            self.mocked_revert_published.assert_called_once()
            self.mocked_revert_published.reset_mock()
            self.mocked_create_publish_request.assert_called_once()
            self.mocked_create_publish_request.reset_mock()

    def test_add_news_as_admin_editor(self):
        variables = self.data

        for user, _msg in (
            (self.adminUser, "as admin"),
            (self.editorUser, "as editor"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, variables)
            entity = result["data"]["addEntity"]["entity"]
            self.assertEqual(entity["isRecommendedInSearch"], True)
            self.assertEqual(entity["backgroundColor"], "#000000")

    def test_add_minimal_entity(self):
        for user, msg in (
            (self.editorUser, "Error for Editors"),
            (self.adminUser, "Error for Administrators"),
        ):
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.minimal_data)
            entity = result["data"]["addEntity"]["entity"]

            self.assertTrue(entity["canEdit"], msg=msg)
            self.assertTrue(entity["canArchiveAndDelete"], msg=msg)

    def test_add_minimal_entity_as_regular_user(self):
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(self.authenticatedUser)
            self.graphql_client.post(self.mutation, self.minimal_data)

    def test_add_with_attachment(self):
        variables = self.data
        variables["input"]["richDescription"] = self.tiptap_attachment(self.attachment)

        self.graphql_client.force_login(self.editorUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        news = News.objects.get(id=entity["guid"])
        self.assertTrue(news.attachments.filter(file_id=self.attachment.id).exists())


@tag("createEntity")
class TestAddGroupNews(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(IS_CLOSED=False)

        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)

        self.mutation = """
        mutation AddNewsToGroup($input: addEntityInput!) {
            addEntity(input: $input) {
                entity { ...NewsParts }
            }
        }
        fragment NewsParts on News {
            group { guid }
            isFeatured
        }
        """
        self.variables = {
            "input": {
                "title": "Foo bar",
                "subtype": "news",
                "groupGuid": self.group.guid,
            }
        }

    def test_add_to_group_anonymously(self):
        """
        Test that anonymous users can't add news.
        """
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_group_as_just_a_user(self):
        """
        Test that non-member-users can't add news.
        """
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(UserFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_group_as_just_an_editor(self):
        """
        Test that non-member-editors can't add news.
        """
        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(EditorFactory())
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_group_as_news_editor(self):
        """
        Test that global news editor can't add news to group.
        """
        news_editor = NewsEditorFactory()
        self.group.join(news_editor)

        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(news_editor)
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_group_as_just_a_member(self):
        """
        Test that member-users can't add news.
        """
        user = UserFactory()
        self.group.join(user)

        with self.assertGraphQlError("could_not_add"):
            self.graphql_client.force_login(user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_add_to_group_as_member_editor(self):
        """
        Test that member-editors can add news.
        """
        user = EditorFactory()
        self.group.join(user)

        with self.assertNotRaisesException():
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)

            self.assertValidResult(result)

    def test_add_to_group_as_member_news_editor(self):
        """
        Test that member-news-editors can add news.
        """
        self.variables = {
            "input": {
                "title": "Foo bar",
                "subtype": "news",
                "groupGuid": self.group.guid,
                "isFeatured": True,
            }
        }

        user = UserFactory()
        self.group.join(user)
        self.group.members.filter(user=user).update(
            roles=[GROUP_MEMBER_ROLES.GROUP_NEWS_EDITOR]
        )

        with self.assertNotRaisesException():
            self.graphql_client.force_login(user)
            result = self.graphql_client.post(self.mutation, self.variables)
            # member-news-editors can not alter isFeatured
            self.assertEqual(result["data"]["addEntity"]["entity"]["isFeatured"], False)
            self.assertValidResult(result)

    def test_add_to_group_as_group_owner(self):
        """
        Test that member-users can add news.
        """
        with self.assertNotRaisesException():
            self.graphql_client.force_login(self.owner)
            result = self.graphql_client.post(self.mutation, self.variables)

            self.assertValidResult(result)

    def assertValidResult(self, result):
        self.assertEqual(
            result["data"]["addEntity"]["entity"]["group"]["guid"], self.group.guid
        )
