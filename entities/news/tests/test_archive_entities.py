from core.tests.helpers.test_archive_entities import Wrapper
from entities.news.factories import NewsFactory as EntityFactory
from user.factories import EditorFactory


class TestArchiveEntities(Wrapper.TestArchiveEntities):
    def create_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def create_entity(self, **kwargs):
        return EntityFactory(**kwargs)
