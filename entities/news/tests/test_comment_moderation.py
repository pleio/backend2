from typing import TYPE_CHECKING

from core.constances import USER_ROLES
from core.tests.helpers.test_comment_moderation import CommentModerationTestCases
from entities.news.factories import NewsFactory

if TYPE_CHECKING:
    from user.models import User


class TestAddCommentForModerationTestCase(
    CommentModerationTestCases.TestAddCommentForModerationTestCase
):
    type_to_string = "news"

    def entity_factory(self, **kwargs):
        owner: User = kwargs.get("owner")
        if owner and not owner.is_news_editor:
            owner.roles.append(USER_ROLES.NEWS_EDITOR)
            owner.save()
        return NewsFactory(**kwargs)
