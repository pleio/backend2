from django.utils.text import slugify
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE, USER_ROLES
from core.factories import GroupFactory
from core.tests.comment.test_comments import SpecificCommentTestCases
from core.tests.helpers import PleioTenantTestCase
from entities.news.factories import NewsFactory
from entities.news.models import News
from user.factories import EditorFactory, UserFactory
from user.models import User


class NewsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = mixer.blend(User)

        self.newsPublic = News.objects.create(
            title="Test public news",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_featured=True,
            source="source1",
        )

        self.newsPrivate = News.objects.create(
            title="Test private news",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_featured=False,
            source="source2",
        )

        self.query = """
            fragment NewsParts on News {
                title
                isTranslationEnabled
                richDescription
                group { guid }
                inGroup
                accessId
                timeCreated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    caption
                }
                isFeatured
                isRecommendedInSearch
                canEdit
                canArchiveAndDelete
                tags
                url
                views
                votes
                hasVoted
                isBookmarked
                isFollowing
                canBookmark
                owner {
                    guid
                }
                source
                publishRequest { guid }
                backgroundColor
            }
            query GetNews($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...NewsParts
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_news_anonymous(self):
        variables = {"guid": self.newsPublic.guid}

        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.newsPublic.guid)
        self.assertEqual(entity["title"], self.newsPublic.title)
        self.assertEqual(entity["richDescription"], self.newsPublic.rich_description)
        self.assertFalse(entity["inGroup"])
        self.assertIsNone(entity["group"])
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["timeCreated"], self.newsPublic.created_at.isoformat())
        self.assertEqual(entity["isFeatured"], self.newsPublic.is_featured)
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["views"], 0)
        self.assertEqual(entity["votes"], 0)
        self.assertEqual(entity["hasVoted"], None)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isTranslationEnabled"], True)
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFollowing"], False)
        self.assertEqual(entity["canBookmark"], False)
        self.assertEqual(entity["canEdit"], False)
        self.assertEqual(entity["canArchiveAndDelete"], False)
        self.assertEqual(entity["owner"]["guid"], self.newsPublic.owner.guid)
        self.assertEqual(
            entity["url"],
            "/news/view/{}/{}".format(
                self.newsPublic.guid, slugify(self.newsPublic.title)
            ),
        )
        self.assertEqual(entity["source"], self.newsPublic.source)
        self.assertIsNotNone(entity["timePublished"])
        self.assertIsNone(entity["scheduleArchiveEntity"])
        self.assertIsNone(entity["scheduleDeleteEntity"])
        self.assertIsNone(entity["publishRequest"])
        self.assertIsNone(entity["backgroundColor"])

        variables = {"guid": self.newsPrivate.guid}

        result = self.graphql_client.post(self.query, variables)

        self.assertEqual(result["data"]["entity"], None)

    def test_news_private(self):
        variables = {"guid": self.newsPrivate.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.newsPrivate.guid)
        self.assertEqual(entity["title"], self.newsPrivate.title)
        self.assertEqual(entity["richDescription"], self.newsPrivate.rich_description)
        self.assertFalse(entity["inGroup"])
        self.assertIsNone(entity["group"])
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(entity["timeCreated"], self.newsPrivate.created_at.isoformat())
        self.assertEqual(entity["isFeatured"], self.newsPrivate.is_featured)
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["views"], 0)
        self.assertEqual(entity["votes"], 0)
        self.assertEqual(entity["hasVoted"], None)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFollowing"], False)
        self.assertEqual(entity["canBookmark"], True)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["owner"]["guid"], self.newsPrivate.owner.guid)
        self.assertEqual(
            entity["url"],
            "/news/view/{}/{}".format(
                self.newsPrivate.guid, slugify(self.newsPrivate.title)
            ),
        )
        self.assertEqual(entity["source"], self.newsPrivate.source)
        self.assertIsNone(entity["publishRequest"])

    def test_news_with_publish_request_anonymous(self):
        self.newsPublic.publish_requests.create(
            time_published=self.newsPublic.published
        )

        result = self.graphql_client.post(self.query, {"guid": self.newsPublic.guid})

        self.assertIsNone(result["data"]["entity"]["publishRequest"])

    def test_news_with_publish_request_authenticated(self):
        pr = self.newsPublic.publish_requests.create(
            time_published=self.newsPublic.published
        )

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, {"guid": self.newsPublic.guid})

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.newsPublic.guid)
        self.assertEqual(entity["publishRequest"]["guid"], pr.guid)


class NewsInGroupTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.owner = UserFactory()
        self.group = GroupFactory(owner=self.owner)
        self.news = NewsFactory(owner=self.owner, group=self.group)

        self.query = """
            fragment NewsParts on News {
                group { guid }
                inGroup
                canEdit
                canArchiveAndDelete
            }
            query GetNews($guid: String!) {
                entity(guid: $guid) {
                    ...NewsParts
                }
            }
        """

        self.variables = {
            "guid": self.news.guid,
        }

    def assertValidGroupProperties(self, data):
        self.assertTrue(data["entity"]["inGroup"])
        self.assertEqual(data["entity"]["group"]["guid"], self.group.guid)

    def assertAccessGranted(self, data):
        self.assertTrue(data["entity"]["canEdit"])
        self.assertTrue(data["entity"]["canArchiveAndDelete"])

    def assertAccessNotGranted(self, data):
        self.assertFalse(data["entity"]["canEdit"])
        self.assertFalse(data["entity"]["canArchiveAndDelete"])

    def test_news_anonymous(self):
        """
        Test that group properties are returned, but write access is not granted.
        """
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessNotGranted(result["data"])

    def test_news_just_an_editor(self):
        """
        Test that group properties are returned, but write access is not granted.
        """
        self.graphql_client.force_login(EditorFactory())
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessNotGranted(result["data"])

    def test_news_not_a_group_member(self):
        """
        Test that group properties are returned, but write access is not granted.
        """
        self.graphql_client.force_login(UserFactory())
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessNotGranted(result["data"])

    def test_news_just_a_group_member(self):
        """
        Test that group properties are returned, but write access is not granted.
        """
        user = UserFactory()
        self.group.join(user)

        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessNotGranted(result["data"])

    def test_news_member_editor(self):
        """
        Test that group properties are returned, and write access is granted.
        """
        editor = EditorFactory()
        self.group.join(editor)

        self.graphql_client.force_login(editor)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessGranted(result["data"])

    def test_news_owner(self):
        """
        Test that group properties are returned, and write access is granted.
        """
        self.graphql_client.force_login(self.owner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessGranted(result["data"])

    def test_superadmin(self):
        user = UserFactory(is_superadmin=True)
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertValidGroupProperties(result["data"])
        self.assertAccessGranted(result["data"])


class TestCommentsInModeration(SpecificCommentTestCases.TestCommentsInModeration):
    QUERY_CONTENT_TYPE = "News"

    def build_entity(self, **kwargs):
        owner: User = kwargs.get("owner")
        if owner and not owner.is_news_editor:
            owner.roles.append(USER_ROLES.NEWS_EDITOR)
            owner.save()
        return NewsFactory(**kwargs)
