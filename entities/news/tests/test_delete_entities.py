from core.models import Entity
from core.tests.helpers import PleioTenantTestCase
from entities.news.factories import NewsFactory as EntityFactory
from user.factories import EditorFactory


class TestDeleteEntities(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = EditorFactory()
        self.entities = [
            EntityFactory(owner=self.user),
            EntityFactory(owner=self.user),
            EntityFactory(owner=self.user),
            EntityFactory(owner=self.user),
        ]
        self.entity_ids = [entity.id for entity in self.entities]

        self.mutation = """
            mutation deleteEntities($input: deleteEntitiesInput!) {
                deleteEntities(input: $input) {
                    success
                }
            }
        """
        self.variables = {
            "input": {
                "guids": [self.entities[0].guid, self.entities[1].guid],
            }
        }

    def test_delete_entities(self):
        self.graphql_client.force_login(self.user)
        self.assertEqual(Entity.objects.filter(id__in=self.entity_ids).count(), 4)

        self.graphql_client.post(self.mutation, self.variables)

        self.assertEqual(Entity.objects.filter(id__in=self.entity_ids).count(), 2)
