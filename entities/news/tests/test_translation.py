from core.tests.helpers.test_translation import Wrapper
from entities.news.factories import NewsFactory
from user.factories import EditorFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "News"

    def build_owner(self, **kwargs):
        return EditorFactory(**kwargs)

    def build_entity(self, **kwargs):
        return NewsFactory(**kwargs)
