from unittest import mock

from core.constances import USER_ROLES
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_entity_with_publish_request import PublishRequestTestCases
from entities.news.factories import NewsFactory
from user.factories import EditorFactory


class TestNewsModelTestCase(PleioTenantTestCase):
    TITLE = "Demo news"
    CONTENT = "Demo news content"
    ABSTRACT = "Demo news abstract"
    SOURCE = "News source description"
    FEATURED = "FEATURED_MEDIA"

    maxDiff = None

    def setUp(self):
        super().setUp()

        self.owner = EditorFactory()
        self.entity = NewsFactory(
            owner=self.owner,
            title=self.TITLE,
            rich_description=self.CONTENT,
            abstract=self.ABSTRACT,
            source=self.SOURCE,
        )

    def tearDown(self):
        super().tearDown()

    @mock.patch("entities.news.models.News.serialize_featured")
    @mock.patch("core.models.Entity.serialize")
    def test_serialize(self, parent_serialize, serialize_featured):
        parent_serialize.return_value = {}
        serialize_featured.return_value = self.FEATURED
        serialized = self.entity.serialize()

        self.assertTrue(parent_serialize.called)
        self.assertEqual(
            serialized,
            {
                "title": self.TITLE,
                "richDescription": self.CONTENT,
                "abstract": self.ABSTRACT,
                "featured": self.FEATURED,
                "source": self.SOURCE,
            },
        )

    def test_map_rich_text_fields(self):
        before = self.entity.serialize()
        expected = self.entity.serialize()
        expected["richDescription"] = f"new {self.CONTENT}"
        expected["abstract"] = f"new {self.ABSTRACT}"

        self.entity.map_rich_text_fields(lambda s: "new {}".format(s))
        after = self.entity.serialize()

        self.assertNotEqual(after, before)
        self.assertEqual(after, expected)


class TestEditEntityWithPublishRequest(
    PublishRequestTestCases.TestEditEntityWithPublishRequest
):
    TYPE_TO_STRING = "news"
    EDITORS_CAN_ALWAYS_EDIT = True

    def entity_factory(self, **kwargs):
        if not self.owner.is_news_editor:
            self.owner.roles.append(USER_ROLES.NEWS_EDITOR)
            self.owner.save()
        return NewsFactory(**kwargs)
