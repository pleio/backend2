from auditlog.registry import auditlog
from django.db import models

from core.entity_access import (
    DenyAnonymousVisitors,
    DenyNonContentModerators,
    GrantAdministrators,
    GrantAuthorizedUsers,
    GrantContentModerators,
    GrantUsersThatCreate,
)
from core.models import (
    ArticleMixin,
    AttachmentMixin,
    BookmarkMixin,
    CommentMixin,
    Entity,
    FollowMixin,
    MentionMixin,
    RevisionMixin,
    VoteMixin,
)
from core.models.featured import FeaturedCoverMixin
from core.models.mixin import RichDescriptionMediaMixin, TitleMixin


class News(
    RichDescriptionMediaMixin,
    TitleMixin,
    VoteMixin,
    BookmarkMixin,
    FollowMixin,
    CommentMixin,
    FeaturedCoverMixin,
    ArticleMixin,
    MentionMixin,
    AttachmentMixin,
    RevisionMixin,
    Entity,
):
    """
    News
    """

    class Meta:
        ordering = ["-published"]

    source = models.TextField(default="")

    def has_revisions(self):
        return True

    def __str__(self):
        return f"News[{self.title}]"

    @property
    def type_to_string(self):
        return "news"

    @property
    def url(self):
        return "{}/news/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    def _get_write_validators(self, user):
        return [
            DenyAnonymousVisitors(user, entity=self),
            GrantAdministrators(user, entity=self),
            GrantContentModerators(user, entity=self),
            DenyNonContentModerators(user, entity=self),
            GrantUsersThatCreate(user, entity=self),
            GrantAuthorizedUsers(user, entity=self),
        ]

    @classmethod
    def can_add(cls, user, group=None):
        if not group and user.is_news_editor:
            return True

        if group and (
            group.is_owner(user) or group.is_admin(user) or group.is_news_editor(user)
        ):
            return True

        return super().can_add(user, group) and (user.is_editor or user.is_site_admin)

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "abstract": self.abstract or "",
            "source": self.source or "",
            "featured": self.serialize_featured(),
            **super().serialize(),
        }


auditlog.register(News)
