from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = "entities.news"
    label = "news"
