from django.apps import AppConfig


class WikiConfig(AppConfig):
    name = "entities.wiki"
    label = "wiki"
