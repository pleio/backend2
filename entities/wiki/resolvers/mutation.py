from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core import constances
from core.constances import COULD_NOT_CHANGE, COULD_NOT_FIND, COULD_NOT_FIND_GROUP
from core.lib import clean_graphql_input
from core.models import Group
from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from core.utils.entity import load_entity_by_id
from entities.wiki.models import Wiki


def resolve_add_wiki(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    group = shared.get_group(clean_input)
    parent = None

    if "containerGuid" in clean_input:
        try:
            parent = Wiki.objects.get_subclass(id=clean_input.get("containerGuid"))
        except ObjectDoesNotExist:
            raise GraphQLError(constances.COULD_NOT_FIND_CONTAINER)

    if parent and parent.group:
        group = parent.group

    shared.assert_can_create(user, Wiki, group)

    if parent:
        shared.assert_write_access(parent, user)

    # default fields for all entities
    entity = Wiki(owner=user, group=group, parent=parent)

    track_publication_date = ContentModerationTrackTimePublished(
        entity, user, is_new=True
    )

    shared.resolve_add_input_language(entity, clean_input)
    shared.resolve_add_access_id(entity, clean_input, user)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_abstract(entity, clean_input)
    shared.update_featured_image(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.update_is_featured(entity, user, clean_input)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.resolve_add_suggested_items(entity, clean_input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    shared.store_initial_revision(entity)
    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}


def resolve_edit_wiki(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [Wiki])

    track_publication_date = ContentModerationTrackTimePublished(entity, user)

    clean_input = clean_graphql_input(input, ["containerGuid"])

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)

    revision = shared.resolve_start_revision(entity, user)

    shared.resolve_update_input_language(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_abstract(entity, clean_input)
    shared.update_featured_image(entity, clean_input)
    shared.update_publication_dates(entity, user, clean_input)
    shared.update_is_featured(entity, user, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)
    shared.update_is_recommended_in_search(entity, user, clean_input)
    shared.resolve_update_suggested_items(entity, clean_input)
    shared.update_updated_at(entity)

    if "containerGuid" in clean_input:
        container = None
        try:
            container = Wiki.objects.get(id=clean_input.get("containerGuid"))
        except ObjectDoesNotExist:
            raise GraphQLError(COULD_NOT_FIND)

        revision.content["parent"] = clean_input["containerGuid"]

        entity.parent = container
        entity.group = container.group if container else None

    if user.is_site_admin or (entity.group and entity.group.can_write(user)):
        shared.resolve_update_owner(entity, clean_input)
        shared.resolve_update_time_created(entity, clean_input)

    if not entity.parent and user.is_site_admin:
        resolve_update_group(entity, clean_input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    shared.store_update_revision(revision, entity)
    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}


def resolve_update_group(entity, clean_input):
    def _update_child(entity, group):
        entity.group = group
        entity.save()
        for child in entity.children.all():
            _update_child(child, group)

    if "groupGuid" in clean_input:
        if clean_input.get("groupGuid"):
            try:
                new_group = Group.objects.get(id=clean_input.get("groupGuid"))
            except ObjectDoesNotExist:
                raise GraphQLError(COULD_NOT_FIND_GROUP)
        else:
            new_group = None

        if entity.group != new_group:
            if entity.parent:
                raise GraphQLError(COULD_NOT_CHANGE)
            entity.group = new_group

            # also update wiki children when group changes
            for child in entity.children.all():
                _update_child(child, entity.group)
