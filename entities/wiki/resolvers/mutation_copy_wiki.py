from django.utils import timezone
from django.utils.translation import gettext as _

from core.lib import access_id_to_acl, get_access_id
from core.resolvers import shared
from core.utils.entity import load_entity_by_id
from entities.wiki.models import Wiki


def create_copy(entity, user, parent=None):
    now = timezone.now()
    wiki_id = entity.id

    entity.owner = user
    entity.notifications_created = False
    entity.published = now
    entity.created_at = now
    entity.updated_at = now
    entity.update_last_action(entity.published)
    entity.read_access = access_id_to_acl(entity, get_access_id(entity.read_access))
    entity.write_access = access_id_to_acl(entity, 0)

    entity.parent = parent
    entity.title = _("Copy %s") % entity.title

    entity.pk = None
    entity.id = None
    entity.save()

    resolve_copy_subwikis(entity, wiki_id, user)

    return entity


def resolve_copy_wiki(_, info, input):
    user = info.context["request"].user
    wiki = load_entity_by_id(input["guid"], [Wiki])

    shared.assert_authenticated(user)
    shared.assert_write_access(wiki, user)

    entity = create_copy(wiki, user, wiki.parent)

    return {"entity": entity}


def resolve_copy_subwikis(entity, wiki_id, user):
    wiki = Wiki.objects.get(id=wiki_id)
    if wiki.has_children():
        for child in wiki.children.all():
            create_copy(child, user, entity)
