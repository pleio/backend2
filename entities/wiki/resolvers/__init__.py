from ariadne import InterfaceType, ObjectType

from .wiki import wiki

resolvers = [wiki]
