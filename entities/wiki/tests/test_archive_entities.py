from core.tests.helpers.test_archive_entities import Wrapper
from entities.wiki.factories import WikiFactory as EntityFactory


class CreateWikiMixin:
    def create_entity(self, **kwargs):
        return EntityFactory(**kwargs)


class TestArchiveEntities(CreateWikiMixin, Wrapper.TestArchiveEntities):
    pass


class TestArchiveWithSubpages(CreateWikiMixin, Wrapper.TestArchiveNestedEntities):
    pass
