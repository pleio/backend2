from core.tests.helpers.test_group_content import Template
from entities.wiki.factories import WikiFactory
from user.factories import UserFactory


class TestGroupContent(Template.TestGroupContentTestCase):
    def build_entity(self, **kwargs):
        return WikiFactory(**kwargs)

    def build_owner(self):
        return UserFactory()
