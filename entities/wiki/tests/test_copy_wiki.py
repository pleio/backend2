import json

from core.lib import get_access_id
from core.tests.helpers import PleioTenantTestCase
from core.utils.entity import load_entity_by_id
from entities.file.models import FileReference
from entities.wiki.factories import WikiFactory
from entities.wiki.models import Wiki
from user.factories import AdminFactory, EditorFactory, UserFactory


class CopyWikiTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.editor = EditorFactory()
        self.admin = AdminFactory()
        self.parent_wiki = WikiFactory(owner=self.editor)
        self.file = self.file_factory(
            self.relative_path(__file__, ["assets", "landscape.jpeg"]),
            owner=self.editor,
        )
        rich_description = json.dumps(
            {
                "type": "file",
                "attrs": {"url": f"/attachment/entity/{self.file.id}?size=123"},
            }
        )
        self.wiki = WikiFactory(
            owner=self.editor,
            parent=self.parent_wiki,
            rich_description=rich_description,
        )

        self.reference, _ = FileReference.objects.get_or_create(
            container=self.wiki, file=self.file
        )
        self.sub_wiki1 = WikiFactory(owner=self.editor, parent=self.wiki)
        self.sub_wiki2 = WikiFactory(owner=self.editor, parent=self.wiki)

        self.mutation = """
            mutation CopyPage($input: copyEntityInput!) {
                copyEntity(input: $input) {
                    entity {
                        guid
                        ... on Wiki {
                            canEdit
                            title
                            url
                            richDescription
                            tags
                            hasChildren
                            children {
                                guid
                            }
                            parent {
                                guid
                            }
                            accessId
                            owner {
                                guid
                            }
                        }
                        __typename
                    }
                    __typename
                }
            }
        """
        self.variables = {"input": {"guid": self.wiki.guid, "subtype": "wiki"}}

    def assert_wiki_copy_with_children_ok(self, user):
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["copyEntity"]["entity"]

        self.assertNotEqual(entity["guid"], self.wiki.guid)
        self.assertIn(self.wiki.title, entity["title"])
        self.assertEqual(entity["richDescription"], self.wiki.rich_description)
        self.assertEqual(entity["tags"], self.wiki.tags)
        self.assertEqual(entity["accessId"], get_access_id(self.wiki.read_access))
        self.assertEqual(entity["canEdit"], self.wiki.can_write(user))
        self.assertEqual(entity["parent"]["guid"], self.parent_wiki.guid)
        self.assertEqual(entity["hasChildren"], True)
        self.assertEqual(len(entity["children"]), 2)
        self.assertEqual(entity["owner"]["guid"], user.guid)

    def assertFileReferenceExists(self, file, container, msg=None):
        self.assertTrue(
            FileReference.objects.filter(container_fk=container.id, file=file).exists(),
            msg=msg,
        )

    def test_copy_wiki_with_children_by_admin(self):
        self.graphql_client.force_login(self.admin)
        self.assert_wiki_copy_with_children_ok(self.admin)

    def test_copy_wiki_with_children_by_editor(self):
        self.graphql_client.force_login(self.editor)
        self.assert_wiki_copy_with_children_ok(self.editor)

    def test_copy_wiki_by_anonymous(self):
        with self.assertGraphQlError("not_logged_in"):
            self.graphql_client.post(self.mutation, self.variables)

    def test_copy_wiki_by_user(self):
        with self.assertGraphQlError("could_not_save"):
            self.graphql_client.force_login(self.user)
            self.graphql_client.post(self.mutation, self.variables)

    def test_copy_wiki_with_file_reference(self):
        self.graphql_client.force_login(self.editor)
        result = self.graphql_client.post(self.mutation, self.variables)
        entity = result["data"]["copyEntity"]["entity"]
        wiki = load_entity_by_id(entity["guid"], [Wiki])
        self.assertFileReferenceExists(self.file, wiki)
