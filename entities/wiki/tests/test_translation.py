from core.tests.helpers.test_translation import Wrapper
from entities.wiki.factories import WikiFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Wiki"

    def build_entity(self, **kwargs):
        return WikiFactory(**kwargs)
