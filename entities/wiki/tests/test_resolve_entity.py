from django.utils.text import slugify
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.wiki.factories import WikiFactory
from user.factories import AdminFactory, EditorFactory, UserFactory
from user.models import User


class WikiTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = mixer.blend(User)
        self.authenticatedUser2 = mixer.blend(User)

        self.wikiPublic = WikiFactory(
            title="Test public wiki",
            rich_description="JSON to string",
            owner=self.authenticatedUser,
        )

        self.wikiPrivate = WikiFactory(
            title="Test private wiki",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            parent=self.wikiPublic,
            is_featured=True,
        )

        self.wikiPrivate2 = WikiFactory(
            title="Test private wiki 2",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser2.id)],
            owner=self.authenticatedUser2,
            parent=self.wikiPublic,
            is_featured=True,
        )

        self.query = """
            fragment WikiParts on Wiki {
                title
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                canEditAdvanced
                canEditGroup
                tags
                url
                isBookmarked
                canBookmark
                inGroup
                group {
                    guid
                }
                hasChildren
                children {
                    guid
                }
                parent {
                    guid
                }
                rootParent {
                    guid
                }
                isFeatured
                isRecommendedInSearch
                isTranslationEnabled
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    caption
                }
                publishRequest { guid }
            }
            query GetWiki($guid: String!) {
                entity(guid: $guid) {
                    guid
                    status
                    ...WikiParts
                }
            }
        """

    def tearDown(self):
        super().tearDown()

    def test_news_anonymous(self):
        variables = {"guid": self.wikiPublic.guid}

        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.wikiPublic.guid)
        self.assertEqual(entity["title"], self.wikiPublic.title)
        self.assertEqual(entity["richDescription"], self.wikiPublic.rich_description)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["timeCreated"], self.wikiPublic.created_at.isoformat())
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFeatured"], False)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["isTranslationEnabled"], True)
        self.assertEqual(entity["canBookmark"], False)
        self.assertEqual(entity["canEdit"], False)
        self.assertEqual(entity["canArchiveAndDelete"], False)
        self.assertEqual(entity["canEditAdvanced"], False)
        self.assertEqual(entity["canEditGroup"], False)
        self.assertEqual(
            entity["url"],
            "/wiki/view/{}/{}".format(
                self.wikiPublic.guid, slugify(self.wikiPublic.title)
            ),
        )
        self.assertEqual(entity["parent"], None)
        self.assertEqual(entity["rootParent"], None)
        self.assertEqual(entity["hasChildren"], True)
        self.assertEqual(len(entity["children"]), 0)
        self.assertIsNotNone(entity["timePublished"])
        self.assertIsNone(entity["scheduleArchiveEntity"])
        self.assertIsNone(entity["scheduleDeleteEntity"])
        self.assertIsNone(entity["publishRequest"])

        variables = {"guid": self.wikiPrivate.guid}

        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertIsNone(entity)

    def test_news_private(self):
        variables = {"guid": self.wikiPrivate.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.wikiPrivate.guid)
        self.assertEqual(entity["title"], self.wikiPrivate.title)
        self.assertEqual(entity["richDescription"], self.wikiPrivate.rich_description)
        self.assertEqual(entity["accessId"], 0)
        self.assertEqual(entity["timeCreated"], self.wikiPrivate.created_at.isoformat())
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["canBookmark"], True)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["canEditAdvanced"], False)
        self.assertEqual(entity["canEditGroup"], False)
        self.assertEqual(
            entity["url"],
            "/wiki/view/{}/{}".format(
                self.wikiPrivate.guid, slugify(self.wikiPrivate.title)
            ),
        )
        self.assertEqual(entity["parent"]["guid"], self.wikiPublic.guid)
        self.assertEqual(entity["rootParent"]["guid"], self.wikiPublic.guid)
        self.assertIsNone(entity["publishRequest"])

    def test_wiki_public(self):
        variables = {"guid": self.wikiPublic.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.wikiPublic.guid)
        self.assertEqual(entity["title"], self.wikiPublic.title)
        self.assertEqual(entity["richDescription"], self.wikiPublic.rich_description)
        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["timeCreated"], self.wikiPublic.created_at.isoformat())
        self.assertEqual(entity["tags"], [])
        self.assertEqual(entity["isBookmarked"], False)
        self.assertEqual(entity["isFeatured"], False)
        self.assertEqual(entity["isRecommendedInSearch"], False)
        self.assertEqual(entity["canBookmark"], True)
        self.assertEqual(entity["canEdit"], True)
        self.assertEqual(entity["canArchiveAndDelete"], True)
        self.assertEqual(entity["canEditAdvanced"], False)
        self.assertEqual(entity["canEditGroup"], False)
        self.assertEqual(
            entity["url"],
            "/wiki/view/{}/{}".format(
                self.wikiPublic.guid, slugify(self.wikiPublic.title)
            ),
        )
        self.assertEqual(len(entity["children"]), 1)
        self.assertEqual(entity["children"][0]["guid"], self.wikiPrivate.guid)
        self.assertIsNone(entity["publishRequest"])

    def test_wiki_editor(self):
        variables = {"guid": self.wikiPublic.guid}

        self.graphql_client.force_login(EditorFactory())
        result = self.graphql_client.post(self.query, variables)
        self.assertFalse(result["data"]["entity"]["canEdit"])

    def test_wiki_admin(self):
        variables = {"guid": self.wikiPublic.guid}

        self.graphql_client.force_login(AdminFactory())
        result = self.graphql_client.post(self.query, variables)

        self.assertTrue(result["data"]["entity"]["canEdit"])
        self.assertTrue(result["data"]["entity"]["canArchiveAndDelete"])
        self.assertTrue(result["data"]["entity"]["canEditAdvanced"])
        self.assertTrue(result["data"]["entity"]["canEditGroup"])

    def test_wiki_group_admin(self):
        group = GroupFactory(owner=UserFactory())
        groupAdmin = UserFactory()
        group.join(groupAdmin, "admin")
        self.wikiPublic.group = group
        self.wikiPublic.save()

        variables = {"guid": self.wikiPublic.guid}
        self.graphql_client.force_login(groupAdmin)

        result = self.graphql_client.post(self.query, variables)

        self.assertTrue(result["data"]["entity"]["canEdit"])
        self.assertTrue(result["data"]["entity"]["canArchiveAndDelete"])
        self.assertTrue(result["data"]["entity"]["canEditAdvanced"])
        self.assertFalse(result["data"]["entity"]["canEditGroup"])

    def test_wiki_root_parent(self):
        # add some extra deep levels
        level2 = WikiFactory(
            title="Test private wiki 3",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            parent=self.wikiPublic,
            is_featured=True,
        )
        level3 = WikiFactory(
            title="Test private wiki 4",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            parent=level2,
            is_featured=True,
        )
        level4 = WikiFactory(
            title="Test private wiki 4",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            parent=level3,
            is_featured=True,
        )

        variables = {"guid": level4.guid}

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, variables)

        entity = result["data"]["entity"]
        self.assertEqual(entity["parent"]["guid"], level3.guid)
        self.assertEqual(entity["rootParent"]["guid"], self.wikiPublic.guid)

    def test_wiki_with_publish_request_anonymous(self):
        self.wikiPublic.publish_requests.create(
            time_published=self.wikiPublic.published
        )

        result = self.graphql_client.post(self.query, {"guid": self.wikiPublic.guid})

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.wikiPublic.guid)
        self.assertIsNone(entity["publishRequest"])

    def test_wiki_with_publish_request_authenticated(self):
        pr = self.wikiPublic.publish_requests.create(
            time_published=self.wikiPublic.published
        )

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.query, {"guid": self.wikiPublic.guid})

        entity = result["data"]["entity"]
        self.assertEqual(entity["guid"], self.wikiPublic.guid)
        self.assertEqual(entity["publishRequest"]["guid"], pr.guid)
