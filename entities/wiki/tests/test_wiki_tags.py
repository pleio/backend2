from core.tests.helpers.tags_testcase import Template
from entities.wiki.models import Wiki


class TestWikiTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "Wiki"
    model = Wiki
    subtype = "wiki"

    def variables_add(self):
        return {
            "input": {
                "title": "Wiki",
                "subtype": "wiki",
            }
        }

    include_site_search = True
    include_entity_search = True
    include_activity_search = True
