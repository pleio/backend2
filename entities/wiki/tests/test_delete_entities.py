from core.tests.helpers.test_delete_entities import Wrapper
from entities.wiki.factories import WikiFactory as EntityFactory
from entities.wiki.models import Wiki


class TestDeleteEntities(Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return EntityFactory(**kwargs)

    def prepare_test(self):
        super().prepare_test()

        self.deleted_subwikis = [
            EntityFactory(parent_id=self.delete_entity_ids[0], owner=self.user),
            EntityFactory(parent_id=self.delete_entity_ids[0], owner=self.user),
        ]
        self.kept_subwikis = [
            EntityFactory(parent_id=self.expected_entity_ids[0], owner=self.user),
            EntityFactory(parent_id=self.expected_entity_ids[0], owner=self.user),
        ]

    def test_delete_entities(self):
        super().test_delete_entities()

        self.assertFalse(
            Wiki.objects.filter(id__in=[e.id for e in self.deleted_subwikis]).exists()
        )
        self.assertEqual(
            Wiki.objects.filter(id__in=[e.id for e in self.kept_subwikis]).count(), 2
        )
