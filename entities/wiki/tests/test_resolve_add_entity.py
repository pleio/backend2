from unittest.mock import patch

from django.test import tag
from django.utils import timezone
from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from core.models import Group
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.wiki.models import Wiki
from user.models import User


@tag("createEntity")
class AddWikiCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])

        self.authenticated_user = mixer.blend(User)
        self.group = mixer.blend(
            Group, owner=self.authenticated_user, is_membership_on_request=False
        )
        self.group.join(self.authenticated_user, "owner")
        self.suggested_item = BlogFactory(owner=self.authenticated_user)

        self.wikiPublic = Wiki.objects.create(
            title="Test public wiki",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticated_user.id)],
            owner=self.authenticated_user,
        )

        self.wikiGroupPublic = Wiki.objects.create(
            title="Test public wiki",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticated_user.id)],
            owner=self.authenticated_user,
            group=self.group,
        )

        self.data = {
            "input": {
                "subtype": "wiki",
                "title": "My first Wiki",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "richDescription": "richDescription",
                "containerGuid": None,
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "featured": {
                    "positionY": 2,
                    "video": "testVideo2",
                    "videoTitle": "testVideoTitle2",
                    "alt": "testAlt2",
                    "caption": "(c) Pleio, 1996",
                },
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.suggested_item.guid],
            }
        }
        self.mutation = """
            fragment WikiParts on Wiki {
                showOwner
                inputLanguage
                isTranslationEnabled
                title
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                tags
                url
                isBookmarked
                canBookmark
                inGroup
                group {
                    guid
                }
                hasChildren
                children {
                    guid
                }
                parent {
                    guid
                }
                isFeatured
                isRecommendedInSearch
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    alt
                    caption
                }
                suggestedItems {
                    guid
                }
            }
            mutation ($input: addEntityInput!) {
                addEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...WikiParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_add_wiki(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["showOwner"], False)
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["isFeatured"], False)  # only with editor or admin role
        self.assertEqual(
            entity["isRecommendedInSearch"], False
        )  # only with editor or admin role
        self.assertEqual(entity["featured"]["positionY"], 2)
        self.assertEqual(entity["featured"]["video"], "testVideo2")
        self.assertEqual(entity["featured"]["videoTitle"], "testVideoTitle2")
        self.assertEqual(entity["featured"]["alt"], "testAlt2")
        self.assertEqual(entity["featured"]["caption"], "(c) Pleio, 1996")
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"], [{"guid": self.suggested_item.guid}])

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_add_wiki_to_parent(self):
        variables = self.data
        variables["input"]["containerGuid"] = self.wikiPublic.guid

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["parent"]["guid"], self.wikiPublic.guid)

        self.wikiPublic.refresh_from_db()

        self.assertTrue(self.wikiPublic.has_children())
        self.assertEqual(self.wikiPublic.children.first().guid, entity["guid"])

    def test_add_wiki_to_group(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["addEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["parent"], None)
        self.assertEqual(entity["inGroup"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)

    def test_add_wiki_to_parent_with_group(self):
        variables = self.data
        variables["input"]["containerGuid"] = self.wikiGroupPublic.guid

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["inGroup"], True)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["parent"]["guid"], self.wikiGroupPublic.guid)

        self.wikiGroupPublic.refresh_from_db()

        self.assertTrue(self.wikiGroupPublic.has_children())
        self.assertEqual(self.wikiGroupPublic.children.first().guid, entity["guid"])

    def test_add_minimal_entity(self):
        variables = {
            "input": {
                "title": "Simple wiki",
                "subtype": "wiki",
            }
        }

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addEntity"]["entity"]

        self.assertTrue(entity["canEdit"])
        self.assertTrue(entity["canArchiveAndDelete"])
