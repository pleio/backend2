from unittest.mock import patch

from django.utils import timezone

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.lib import get_access_id
from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_advanced_tab import TestAdvancedTab
from entities.blog.factories import BlogFactory
from entities.wiki.factories import WikiFactory
from user.factories import AdminFactory, UserFactory


class AddWikiCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en", "nl"])
        self.authenticated_user = UserFactory()
        self.user2 = UserFactory()
        self.admin = AdminFactory()
        self.group = GroupFactory(owner=self.admin)
        self.suggested_item = BlogFactory(owner=self.authenticated_user)
        self.wikiPublic = WikiFactory(
            owner=self.authenticated_user,
        )

        self.data = {
            "input": {
                "guid": self.wikiPublic.guid,
                "title": "My first Wiki",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True,
                "isRecommendedInSearch": True,
                "featured": {
                    "positionY": 2,
                    "video": "testVideo2",
                    "videoTitle": "testVideoTitle2",
                    "alt": "testAlt2",
                    "caption": "(c) Pleio, 1996",
                },
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
                "suggestedItems": [self.suggested_item.guid],
            }
        }
        self.mutation = """
            fragment WikiParts on Wiki {
                title
                inputLanguage
                isTranslationEnabled
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                canEdit
                tags
                url
                isBookmarked
                canBookmark
                inGroup
                group {
                    guid
                }
                owner {
                    guid
                }
                hasChildren
                children {
                    guid
                }
                parent {
                    guid
                }
                isFeatured
                isRecommendedInSearch
                subtype
                featured {
                    image { guid }
                    video
                    videoTitle
                    positionY
                    alt
                    caption
                }
                suggestedItems {
                    guid
                }
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...WikiParts
                    }
                }
            }
        """
        self.mocked_revert_published = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_revert_time_published"
        ).start()
        self.mocked_create_publish_request = patch(
            "core.utils.content_moderation.ContentModerationTrackTimePublished.maybe_create_publish_request"
        ).start()

    def test_edit_wiki(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticated_user)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["inputLanguage"], variables["input"]["inputLanguage"])
        self.assertEqual(entity["isTranslationEnabled"], False)
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["isFeatured"], False)  # Only with editor or admin role
        self.assertEqual(
            entity["isRecommendedInSearch"], False
        )  # Only with editor or admin role
        self.assertEqual(entity["subtype"], "wiki")
        self.assertEqual(entity["group"], None)
        self.assertEqual(entity["owner"]["guid"], self.authenticated_user.guid)
        self.assertEqual(entity["timeCreated"], self.wikiPublic.created_at.isoformat())
        self.assertEqual(entity["featured"]["positionY"], 2)
        self.assertEqual(entity["featured"]["video"], "testVideo2")
        self.assertEqual(entity["featured"]["videoTitle"], "testVideoTitle2")
        self.assertEqual(entity["featured"]["alt"], "testAlt2")
        self.assertEqual(entity["featured"]["caption"], "(c) Pleio, 1996")
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )
        self.assertEqual(entity["suggestedItems"], [{"guid": self.suggested_item.guid}])

        self.mocked_revert_published.assert_called_once()
        self.mocked_create_publish_request.assert_called_once()

    def test_edit_wiki_by_admin(self):
        variables = self.data
        variables["input"]["timeCreated"] = "2018-12-10T23:00:00.000Z"
        variables["input"]["groupGuid"] = self.group.guid
        variables["input"]["ownerGuid"] = self.user2.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["hasChildren"], False)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)
        self.assertEqual(entity["subtype"], "wiki")
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")

        self.wikiPublic.refresh_from_db()

        self.assertEqual(entity["title"], self.wikiPublic.title)
        self.assertEqual(entity["richDescription"], self.wikiPublic.rich_description)
        self.assertEqual(entity["hasChildren"], self.wikiPublic.has_children())
        self.assertEqual(entity["isFeatured"], self.wikiPublic.is_featured)
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")

    def test_edit_wiki_group_null_by_admin(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["isFeatured"], True)
        self.assertEqual(entity["isRecommendedInSearch"], True)

        variables["input"]["groupGuid"] = None

        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["group"], None)

    def test_edit_child_wiki_group_null_by_admin(self):
        childWiki = WikiFactory(
            owner=self.authenticated_user,
            parent=self.wikiPublic,
        )

        variables = self.data
        variables["input"]["guid"] = childWiki.guid
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["editEntity"]["entity"]

        self.assertEqual(entity["group"], None)


class TestAdvancedTabAtWikiContentTestCase(TestAdvancedTab.TestCase):
    TYPE = "Wiki"

    def build_entity(self, **kwargs):
        return WikiFactory(**kwargs)

    def build_child_entity(self):
        child_entity = self.build_entity(
            owner=self.entity_owner,
            group=self.group,
        )
        child_entity.parent = self.entity
        child_entity.created_at = self.ORIGINAL_CREATED_TIME
        child_entity.save()
        return child_entity

    def run_test_with_user(self, user, expected_result):
        child_entity = self.build_child_entity()
        self.variables["input"]["guid"] = child_entity.guid
        self.graphql_client.force_login(user)
        result = self.graphql_client.post(
            self.mutation,
            {**self.variables, "guid": child_entity.guid},
        )

        self.assertEqual(
            result["data"]["editEntity"]["entity"],
            expected_result,
        )

    def test_update_child_entity_as_owner(self):
        self.run_test_with_user(
            self.entity_owner,
            {
                "canEdit": True,
                "canArchiveAndDelete": True,
                "canEditAdvanced": False,
                "canEditGroup": False,
                "title": "My first update",
                "timeCreated": self.ORIGINAL_CREATED_TIME,
                "owner": {"guid": self.entity_owner.guid},
                "group": {"guid": self.group.guid},
            },
        )

    def test_update_child_entity_as_group_admin(self):
        self.run_test_with_user(
            self.group_admin,
            {
                "canEdit": True,
                "canArchiveAndDelete": True,
                "canEditAdvanced": True,
                "canEditGroup": False,
                "title": "My first update",
                "timeCreated": self.UPDATED_CREATED_TIME,
                "owner": {"guid": self.another_user.guid},
                "group": {"guid": self.group.guid},
            },
        )

    def test_update_child_entity_as_admin(self):
        self.run_test_with_user(
            self.admin,
            {
                "canEdit": True,
                "canArchiveAndDelete": True,
                "canEditAdvanced": True,
                "canEditGroup": False,
                "title": "My first update",
                "timeCreated": self.UPDATED_CREATED_TIME,
                "owner": {"guid": self.another_user.guid},
                "group": {"guid": self.group.guid},
            },
        )


class TestRecursiveAccessControlForWikiPages(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory(email="owner@example.com")

        self.initial_access = {
            "read_access": [ACCESS_TYPE.user.format(self.owner.guid)],
            "write_access": [ACCESS_TYPE.user.format(self.owner.guid)],
        }
        self.changed_access = {
            "read_access": [ACCESS_TYPE.public],
            "write_access": [ACCESS_TYPE.logged_in],
        }
        self.grand_father = WikiFactory(
            owner=self.owner,
            **self.initial_access,
        )
        self.father = WikiFactory(
            owner=self.owner,
            parent=self.grand_father,
            **self.initial_access,
        )
        self.child = WikiFactory(
            owner=self.owner,
            parent=self.father,
            **self.initial_access,
        )
        self.child2 = WikiFactory(
            owner=self.owner,
            parent=self.father,
            **self.initial_access,
        )

        self.mutation = """
        mutation EditEntity($input: editEntityInput!) {
            editEntity(input: $input) {
                entity { guid }
            }
        }
        """

        self.variables = {
            "input": {
                "guid": self.father.guid,
                "accessId": get_access_id(self.changed_access["read_access"]),
                "writeAccessId": get_access_id(self.changed_access["write_access"]),
            }
        }

    def assertAccess(self, entity, read_access, write_access):
        entity.refresh_from_db()
        for access_item in read_access:
            self.assertIn(
                access_item,
                entity.read_access,
                msg=f"{access_item} not in {entity.read_access}",
            )
        for access_item in write_access:
            self.assertIn(
                access_item,
                entity.write_access,
                msg=f"{access_item} not in {entity.write_access}",
            )

    def test_update_parent(self):
        self.graphql_client.force_login(self.owner)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertAccess(self.grand_father, **self.initial_access)
        self.assertAccess(self.father, **self.changed_access)
        self.assertAccess(self.child, **self.initial_access)
        self.assertAccess(self.child2, **self.initial_access)

    def test_update_parent_recursive(self):
        self.variables["input"]["isAccessRecursive"] = True
        self.graphql_client.force_login(self.owner)
        self.graphql_client.post(self.mutation, self.variables)

        self.assertAccess(self.grand_father, **self.initial_access)
        self.assertAccess(self.father, **self.changed_access)
        self.assertAccess(self.child, **self.changed_access)
        self.assertAccess(self.child2, **self.changed_access)
