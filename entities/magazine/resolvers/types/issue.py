from ariadne import ObjectType

from core.resolvers import decorators, shared
from entities.magazine.models import Magazine
from entities.magazine.resolvers import shared as magazine_shared

magazine_issue = ObjectType("MagazineIssue")


@magazine_issue.field("container")
@decorators.resolve_obj_request
def resolve_container(obj, request):
    return Magazine.objects.visible(request.user).filter(id=obj.container_id).first()


@magazine_issue.field("articles")
@decorators.resolve_obj_request
def resolve_articles(obj, request):
    return obj.get_articles(request.user)


@magazine_issue.field("issueNumber")
@decorators.resolve_obj
def resolve_issue_number(obj):
    return obj.issue_number


magazine_issue.set_field("guid", shared.resolve_entity_guid)
magazine_issue.set_field("status", shared.resolve_entity_status)
magazine_issue.set_field("subtype", shared.resolve_entity_type_as_string)
magazine_issue.set_field("inputLanguage", shared.resolve_entity_input_language)
magazine_issue.set_field(
    "isTranslationEnabled", shared.resolve_entity_is_translation_enabled
)
magazine_issue.set_field("title", shared.resolve_entity_title)
magazine_issue.set_field("localTitle", shared.resolve_entity_local_title)
magazine_issue.set_field("abstract", shared.resolve_entity_abstract)
magazine_issue.set_field("localAbstract", shared.resolve_entity_local_abstract)
magazine_issue.set_field("description", shared.resolve_entity_description)
magazine_issue.set_field("localDescription", shared.resolve_entity_local_description)
magazine_issue.set_field("richDescription", shared.resolve_entity_rich_description)
magazine_issue.set_field(
    "localRichDescription", shared.resolve_entity_local_rich_description
)
magazine_issue.set_field("backgroundColor", shared.resolve_entity_background_color)
magazine_issue.set_field("inGroup", shared.resolve_entity_in_group)
magazine_issue.set_field("group", shared.resolve_entity_group)
magazine_issue.set_field("excerpt", shared.resolve_entity_excerpt)
magazine_issue.set_field("featured", shared.resolve_entity_featured)
magazine_issue.set_field("localExcerpt", shared.resolve_entity_local_excerpt)
magazine_issue.set_field("url", shared.resolve_entity_url_from_object)
magazine_issue.set_field("tags", shared.resolve_entity_tags)
magazine_issue.set_field("tagCategories", shared.resolve_entity_categories)
magazine_issue.set_field("timeCreated", shared.resolve_entity_time_created)
magazine_issue.set_field("timeUpdated", shared.resolve_entity_time_updated)
magazine_issue.set_field("timePublished", shared.resolve_entity_time_published)
magazine_issue.set_field("statusPublished", shared.resolve_entity_status_published)
magazine_issue.set_field(
    "scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity
)
magazine_issue.set_field(
    "scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity
)
magazine_issue.set_field("isFeatured", shared.resolve_entity_is_featured_property)
magazine_issue.set_field("isRecommended", shared.resolve_entity_is_recommended_property)
magazine_issue.set_field(
    "isRecommendedInSearch", shared.resolve_entity_is_recommended_in_search
)
magazine_issue.set_field("canEdit", shared.resolve_entity_can_edit)
magazine_issue.set_field("canEditAdvanced", shared.resolve_entity_can_edit_advanced)
magazine_issue.set_field("canEditGroup", shared.resolve_entity_can_edit_group)
magazine_issue.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
magazine_issue.set_field("canBookmark", shared.resolve_entity_can_bookmark)
magazine_issue.set_field("canComment", shared.resolve_entity_can_comment)
magazine_issue.set_field("canVote", shared.resolve_entity_can_vote)
magazine_issue.set_field("accessId", shared.resolve_entity_access_id)
magazine_issue.set_field("writeAccessId", shared.resolve_entity_write_access_id)
magazine_issue.set_field("isBookmarked", shared.resolve_entity_is_bookmarked)
magazine_issue.set_field("views", shared.resolve_entity_views)
magazine_issue.set_field("owner", shared.resolve_entity_owner)
magazine_issue.set_field("showOwner", shared.resolve_entity_show_owner)
magazine_issue.set_field("isPinned", shared.resolve_entity_is_pinned)
magazine_issue.set_field("lastSeen", shared.resolve_entity_last_seen)
magazine_issue.set_field("publishRequest", shared.resolve_entity_publish_request)
magazine_issue.set_field("colophon", magazine_shared.resolve_entity_colophon)
