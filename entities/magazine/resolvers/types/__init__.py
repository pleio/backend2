from .issue import magazine_issue as magazine_issue_type
from .magazine import magazine as magazine_type

resolvers = [magazine_type, magazine_issue_type]
