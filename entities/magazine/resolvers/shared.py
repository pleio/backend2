import uuid

from django.core.exceptions import ValidationError
from graphql import GraphQLError

from core.constances import (
    COULD_NOT_FIND_CONTAINER,
    COULD_NOT_FIND_ICON_FILE,
    COULD_NOT_SAVE,
)
from core.models import Entity
from entities.file.models import FileFolder
from entities.magazine.models import Magazine


def resolve_update_container(entity, clean_input):
    try:
        if "containerGuid" not in clean_input:
            return
        entity.container = Magazine.objects.get(id=clean_input["containerGuid"])
    except (Magazine.DoesNotExist, ValidationError):
        raise GraphQLError(COULD_NOT_FIND_CONTAINER)


def resolve_update_icon(entity, clean_input):
    try:
        if "iconGuid" not in clean_input:
            return
        entity.icon = FileFolder.objects.filter_files().get(id=clean_input["iconGuid"])
    except (FileFolder.DoesNotExist, ValidationError):
        raise GraphQLError(COULD_NOT_FIND_ICON_FILE)


def resolve_update_articles(entity, user, clean_input):
    try:
        if "articles" not in clean_input:
            return
        valid_articles = Entity.objects.visible(user).filter(
            id__in=clean_input["articles"]
        )
        assert valid_articles.count() == len(clean_input["articles"])
        entity.set_articles(clean_input["articles"])
    except (Entity.DoesNotExist, ValidationError, AssertionError):
        raise GraphQLError(COULD_NOT_SAVE)


def resolve_update_colophon(entity, clean_input):
    if "colophon" in clean_input:
        entity.colophon = clean_input["colophon"]


def resolve_entity_colophon(obj, info):
    values = {v["key"]: v["value"] for v in (obj.colophon or [])}

    def key_value_label(field):
        return {
            "key": field["key"],
            "value": values.get(field["key"]),
            "label": field["label"],
        }

    return [key_value_label(f) for f in (obj.container.colophon_fields or [])]


class ColophonField:
    def __init__(self, item):
        self.key = item.get("key") or str(uuid.uuid4())
        self.label = item.get("label")

    def as_dict(self):
        return {
            "key": self.key,
            "label": self.label,
        }


def resolve_update_colophon_fields(entity, clean_input):
    if "colophonFields" in clean_input:
        entity.colophon_fields = [
            ColophonField(item).as_dict()
            for item in (clean_input["colophonFields"] or [])
        ]


def resolve_entity_colophon_fields(obj, info):
    if obj.colophon_fields and isinstance(obj.colophon_fields, list):
        return obj.colophon_fields
    return []


def resolve_update_issue_number(entity, clean_input):
    if "issueNumber" in clean_input:
        entity.issue_number = clean_input["issueNumber"]
