from .mutations import resolvers as mutation_resolvers
from .types import resolvers as type_resolvers

resolvers = [*type_resolvers, *mutation_resolvers]
