from core.resolvers import shared
from core.utils.content_moderation import ContentModerationTrackTimePublished
from entities.magazine.models import Magazine
from entities.magazine.resolvers import shared as magazine_shared


def resolve_add_magazine(request, input):
    user = request.user
    group = shared.get_group(input)

    shared.assert_authenticated(user)
    shared.assert_can_create(user, Magazine, group)

    entity = Magazine(owner=user, group=group)

    track_publication_date = ContentModerationTrackTimePublished(
        entity, user, is_new=True
    )

    shared.resolve_add_input_language(entity, input)
    shared.resolve_entity_is_translation_enabled(entity, input)
    shared.resolve_update_title(entity, input)
    shared.resolve_update_sub_title(entity, input)
    shared.resolve_update_abstract(entity, input)
    shared.resolve_update_rich_description(entity, input)
    shared.update_is_recommended(entity, user, input)
    shared.update_is_recommended_in_search(entity, user, input)
    shared.update_is_featured(entity, user, input)
    shared.resolve_update_tags(entity, input)
    shared.resolve_update_access_id(entity, input, user)
    shared.update_publication_dates(entity, user, input)

    magazine_shared.resolve_update_icon(entity, input)
    magazine_shared.resolve_update_colophon_fields(entity, input)

    if user.is_editor:
        shared.resolve_update_background_color(entity, input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}


def resolve_edit_magazine(request, input):
    user = request.user
    shared.assert_authenticated(user)

    entity = Magazine.objects.get(id=input["guid"])
    shared.assert_write_access(entity, user)
    track_publication_date = ContentModerationTrackTimePublished(entity, user)

    shared.resolve_update_input_language(entity, input)
    shared.update_is_translation_enabled(entity, input)
    shared.resolve_update_title(entity, input)
    shared.resolve_update_sub_title(entity, input)
    shared.resolve_update_abstract(entity, input)
    shared.resolve_update_rich_description(entity, input)
    shared.update_is_recommended(entity, user, input)
    shared.update_is_recommended_in_search(entity, user, input)
    shared.update_is_featured(entity, user, input)
    shared.update_featured_image(entity, input)
    shared.resolve_update_tags(entity, input)
    shared.resolve_update_access_id(entity, input, user)
    shared.update_publication_dates(entity, user, input)

    shared.resolve_update_entity_icon(entity, input)
    magazine_shared.resolve_update_colophon_fields(entity, input)

    if user.is_site_admin or (entity.group and entity.group.can_write(user)):
        shared.resolve_update_owner(entity, input)
        shared.resolve_update_time_created(entity, input)

    if user.is_site_admin:
        shared.resolve_update_group(entity, input)

    track_publication_date.maybe_revert_time_published()
    entity.save()

    track_publication_date.maybe_create_publish_request()

    return {"entity": entity}
