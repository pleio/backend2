from typing import TYPE_CHECKING

from django.contrib.auth.models import AnonymousUser

from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import AdminFactory, EditorFactory, UserFactory

if TYPE_CHECKING:
    from entities.blog.models import Blog
    from entities.magazine.models import MagazineIssue


class TestMagazineIssueModel(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory(email="owner@example.com")
        self.magazine = MagazineFactory(owner=EditorFactory(email="editor@example.com"))
        self.issue: MagazineIssue = MagazineIssueFactory(
            owner=self.owner, container=self.magazine
        )

    def test_set_articles(self):
        article: Blog = BlogFactory(owner=self.owner)
        self.issue.set_articles([article.id])

        self.assertEqual(self.issue.article_references.count(), 1)
        self.assertEqual(self.issue.get_articles(self.owner), [article])
        self.assertEqual(article.issue_references.issue(), self.issue)

    def test_can_write(self):
        self.assertTrue(self.issue.can_write(self.owner))
        self.assertTrue(self.issue.can_write(AdminFactory()))

        self.assertFalse(self.issue.can_write(AnonymousUser()))
        self.assertFalse(self.issue.can_write(UserFactory()))

    def test_get_all_children(self):
        # Given
        articles = [
            BlogFactory(owner=self.owner),
            BlogFactory(owner=self.owner),
            BlogFactory(owner=self.owner),
        ]
        self.issue.set_articles([a.id for a in articles])

        # When
        children = self.issue.get_all_children()

        # Then
        self.assertEqual([*children], articles)
