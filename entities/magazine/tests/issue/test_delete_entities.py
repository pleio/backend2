from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_delete_entities import Wrapper
from entities.blog.factories import BlogFactory
from entities.blog.models import Blog
from entities.magazine.models import MagazineIssue
from entities.magazine.tests.issue.shared import MagazineIssueFactoryMixin
from user.factories import UserFactory


class TestDeleteEntities(MagazineIssueFactoryMixin, Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return self.build_magazine_issue(**kwargs)


class TestRecursiveDeleteEntities(MagazineIssueFactoryMixin, PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.magazine_issue = self.build_magazine_issue(owner=self.user)
        self.magazine_issue.set_articles(
            [
                BlogFactory(owner=self.user, title="01 First").guid,
                BlogFactory(owner=self.user, title="02 Second").guid,
                BlogFactory(owner=self.user, title="03 Third").guid,
            ]
        )

        self.mutation = """
            mutation DeleteEntities($input: deleteEntityInput!) {
                deleteEntity(input: $input) {
                    success
                }
            }
        """
        self.variables = {
            "input": {
                "guid": self.magazine_issue.guid,
                "recursive": "tryDelete",
            }
        }

    def submit_mutation(self, user):
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, self.variables)
        return response["data"]["deleteEntity"]

    def test_delete_entities(self):
        # When
        response = self.submit_mutation(self.user)

        # Then
        self.assertEqual(response, {"success": True})
        self.assertFalse(MagazineIssue.objects.exists())
        self.assertFalse(Blog.objects.exists())

    def test_try_delete_entities(self):
        # Given
        self.magazine_issue.append_article(
            BlogFactory(
                owner=UserFactory(email="someone@else.com"), title="04 Fourth"
            ).guid
        )

        # When
        response = self.submit_mutation(self.user)

        # Then
        self.assertEqual(response, {"success": False})
        self.assertTrue(MagazineIssue.objects.exists())
        self.assertEqual(Blog.objects.count(), 4)

    def test_soft_delete_entities(self):
        # Given
        blog = BlogFactory(
            owner=UserFactory(email="someone@else.com"), title="04 Fourth"
        )
        self.magazine_issue.append_article(blog.guid)
        self.variables["input"]["recursive"] = "softDelete"

        # When
        response = self.submit_mutation(self.user)

        # Then
        self.assertEqual(response, {"success": True})
        self.assertTrue(MagazineIssue.objects.exists())
        self.assertEqual([*Blog.objects.all()], [blog])
