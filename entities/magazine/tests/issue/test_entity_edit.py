from django.core.files.base import ContentFile
from django.utils.timezone import now, timedelta

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.blog.factories import BlogFactory
from entities.file.factories import FileFactory
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import AdminFactory, EditorFactory, UserFactory

from .shared import FULL_MAGAZINE_ISSUE_FRAGMENT


class TestEntityEdit(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.override_config(EXTRA_LANGUAGES=["en"])

        self.INITIAL_CREATED_AT = now() - timedelta(days=333)
        self.INITIAL_PUBLISHED = now() - timedelta(days=222)

        self.owner = UserFactory(email="owner@example.com")
        self.magazine = MagazineFactory(
            title="Magazine",
            owner=EditorFactory(email="magazine-owner@example.com"),
            colophon_fields=[
                {"key": "key", "label": "label"},
                {"key": "key2", "label": "label2"},
            ],
        )
        self.entity = MagazineIssueFactory(
            title="Magazine Issue",
            owner=self.owner,
            container=self.magazine,
            created_at=self.INITIAL_CREATED_AT,
            published=self.INITIAL_PUBLISHED,
        )

        self.next_file = FileFactory(
            owner=self.owner, upload=ContentFile(b"content", "header.jpg")
        )

        self.next_owner = EditorFactory(email="next-owner@example.com")
        self.next_magazine = MagazineFactory(owner=self.next_owner)
        self.next_group = GroupFactory(owner=self.next_owner)

        self.YESTERDAY = now() - timedelta(days=1)
        self.FOREVER = now() - timedelta(days=777)
        self.TOMORROW = now() + timedelta(days=1)
        self.NEXT_WEEK = now() + timedelta(days=7)

        self.articles = [
            BlogFactory(owner=self.owner).guid,
            BlogFactory(owner=self.owner).guid,
            BlogFactory(owner=self.owner).guid,
        ]
        self.expected_articles = [{"guid": guid} for guid in self.articles]

        self.mutation = """
        fragment MagazineIssueFragment on MagazineIssue %(fragment)s
        mutation EditEntity($input: editEntityInput!) {
            editEntity(input: $input) {
                entity { ...MagazineIssueFragment }
            }
        }
        """ % {"fragment": FULL_MAGAZINE_ISSUE_FRAGMENT}

        self.variables = {
            "input": {
                "guid": self.entity.guid,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "I don't mean to be mean",
                "abstract": self.tiptap_paragraph("It's just that"),
                "richDescription": self.tiptap_paragraph(
                    "I don't accept this paragraph"
                ),
                "isRecommended": True,
                "isRecommendedInSearch": True,
                "isFeatured": True,
                "featured": {"imageGuid": self.next_file.guid},
                "accessId": 1,
                "writeAccessId": 1,
                "isAccessRecursive": False,
                "containerGuid": self.next_magazine.guid,
                "scheduleArchiveEntity": self.TOMORROW.isoformat(),
                "scheduleDeleteEntity": self.NEXT_WEEK.isoformat(),
                "groupGuid": self.next_group.guid,
                "ownerGuid": self.next_owner.guid,
                "timeCreated": self.FOREVER.isoformat(),
                "timePublished": self.YESTERDAY.isoformat(),
                "backgroundColor": "#AAAADD",
                "articles": self.articles,
                "colophon": [{"key": "key", "value": "value"}],
                "issueNumber": 42,
            }
        }

    def send_mutation(self, user):
        if user:
            self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, self.variables)
        return response["data"]["editEntity"]

    def get_common_expectation_fields(self):
        return {
            "abstract": self.tiptap_paragraph("It's just that"),
            "accessId": 1,
            "articles": self.expected_articles,
            "canArchiveAndDelete": True,
            "canBookmark": True,
            "canComment": False,
            "canEdit": True,
            "canVote": False,
            "description": "I don't accept this paragraph\n\n",
            "excerpt": "It's just that\n\n",
            "featured": {
                "video": "",
                "image": {"guid": self.entity.featured_image.guid},
            },
            "inputLanguage": "en",
            "isBookmarked": False,
            "isPinned": False,
            "issueNumber": 42,
            "isTranslationEnabled": False,
            "lastSeen": None,
            "localAbstract": None,
            "localDescription": None,
            "localExcerpt": "",
            "localRichDescription": None,
            "localTitle": None,
            "publishRequest": None,
            "richDescription": self.tiptap_paragraph("I don't accept this paragraph"),
            "scheduleArchiveEntity": self.TOMORROW.isoformat(),
            "scheduleDeleteEntity": self.NEXT_WEEK.isoformat(),
            "showOwner": True,
            "status": 200,
            "statusPublished": "published",
            "subtype": "magazine_issue",
            "tagCategories": [],
            "tags": [],
            "timePublished": self.YESTERDAY.isoformat(),
            "timeUpdated": self.entity.updated_at.isoformat(),
            "title": "I don't mean to be mean",
            "url": self.entity.url,
            "views": 0,
            "writeAccessId": 1,
        }

    def test_entity_edit(self):
        result = self.send_mutation(self.owner)
        self.entity.refresh_from_db()

        self.maxDiff = None
        self.assertEqual(
            result["entity"],
            {
                "backgroundColor": None,
                "canEditAdvanced": False,
                "canEditGroup": False,
                "container": {"guid": self.magazine.guid},
                "group": None,
                "guid": self.entity.guid,
                "inGroup": False,
                "isFeatured": False,
                "isRecommended": False,
                "isRecommendedInSearch": False,
                "owner": {"guid": self.owner.guid},
                "timeCreated": self.INITIAL_CREATED_AT.isoformat(),
                "colophon": [
                    {"key": "key", "label": "label", "value": "value"},
                    {"key": "key2", "label": "label2", "value": None},
                ],
                **self.get_common_expectation_fields(),
            },
        )

        self.assertDateNotEqual(self.entity.created_at, self.FOREVER)

    def test_update_magazine_issue_as_admin(self):
        result = self.send_mutation(AdminFactory())
        self.entity.refresh_from_db()

        self.maxDiff = None
        self.assertEqual(
            result["entity"],
            {
                "backgroundColor": "#AAAADD",
                "canEditAdvanced": True,
                "canEditGroup": True,
                "container": {"guid": self.next_magazine.guid},
                "group": {"guid": self.next_group.guid},
                "guid": self.entity.guid,
                "inGroup": True,
                "isFeatured": True,
                "isRecommended": True,
                "isRecommendedInSearch": True,
                "owner": {"guid": self.next_owner.guid},
                "timeCreated": self.FOREVER.isoformat(),
                "colophon": [],
                **self.get_common_expectation_fields(),
            },
        )

        self.assertDateEqual(self.entity.created_at, self.FOREVER)


class TestUpdateMagazineIssueReadAccessTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory()
        self.magazine = MagazineFactory(
            read_access=[ACCESS_TYPE.user.format(self.owner.guid)], owner=self.owner
        )
        self.issue = MagazineIssueFactory(
            container=self.magazine,
            read_access=[ACCESS_TYPE.user.format(self.owner.guid)],
            owner=self.owner,
            published=None,
        )

        self.mutation = """
        fragment MagazineIssueFragment on MagazineIssue {
            guid
            accessId
            writeAccessId
            container {
                guid
                accessId
            }
        }
        mutation UpdateMagazineIssue($input: editEntityInput!) {
            editEntity(input: $input) {
                entity { ...MagazineIssueFragment }
            }
        }
        """

        self.variables = {
            "input": {
                "guid": self.issue.guid,
                "timePublished": now().isoformat(),
                "isAccessRecursive": True,
            }
        }

    def test_update_permission(self):
        self.graphql_client.force_login(self.owner)
        self.variables["input"]["accessId"] = 2
        response = self.graphql_client.post(self.mutation, self.variables)
        entity = response["data"]["editEntity"]["entity"]

        self.assertEqual(entity["accessId"], 2)
        self.assertEqual(entity["container"]["accessId"], 2)

    def test_update_to_lower_permission(self):
        self.magazine.read_access = [ACCESS_TYPE.public]
        self.magazine.save()
        self.variables["input"]["accessId"] = 1

        self.graphql_client.force_login(self.owner)
        response = self.graphql_client.post(self.mutation, self.variables)
        entity = response["data"]["editEntity"]["entity"]

        self.assertEqual(entity["accessId"], 1)
        self.assertEqual(entity["container"]["accessId"], 2)
