from core.constances import SitePlanChoices
from core.tests.helpers.tags_testcase import Template
from entities.magazine.factories import MagazineFactory
from entities.magazine.models import MagazineIssue
from user.factories import EditorFactory


class TestBlogTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "MagazineIssue"
    model = MagazineIssue

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.magazine = None

    def setUp(self):
        super().setUp()
        self.override_config(SITE_PLAN=SitePlanChoices.pro_plus)
        self.override_config(OPEN_FOR_CREATE_CONTENT_TYPES=["magazine_issue"])

    def get_or_create_magazine(self):
        if not self.magazine:
            self.magazine = MagazineFactory(
                owner=EditorFactory(email="magazine@example.com")
            )
        return self.magazine

    def variables_add(self):
        return {
            "input": {
                "title": "Test magazine issue",
                "subtype": "magazine_issue",
                "containerGuid": self.get_or_create_magazine().guid,
            }
        }

    include_entity_search = True
    include_activity_search = True
    include_site_search = True
