from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.entity_filters import Template
from entities.blog.factories import BlogFactory
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from entities.magazine.tests.issue.shared import MagazineIssueFactoryMixin
from user.factories import AdminFactory


class TestEntityFilters(MagazineIssueFactoryMixin, Template.TestEntityFiltersTestCase):
    def get_subtype(self):
        return "magazine_issue"

    def subtype_factory(self, **kwargs):
        return self.build_magazine_issue(**kwargs)

    def reference_factory(self, **kwargs):
        return BlogFactory(**kwargs)


class TestQueryIssuesInMagazine(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = AdminFactory()
        self.magazine1 = MagazineFactory(owner=self.user)
        self.issue1_1 = MagazineIssueFactory(owner=self.user, container=self.magazine1)
        self.issue1_2 = MagazineIssueFactory(owner=self.user, container=self.magazine1)
        self.magazine2 = MagazineFactory(owner=self.user)
        self.issue2_1 = MagazineIssueFactory(owner=self.user, container=self.magazine2)
        self.issue2_2 = MagazineIssueFactory(owner=self.user, container=self.magazine2)

        self.query = """
        query QueryIssues($parentGuid: String!) {
            entities(subtype: "magazine_issue", parentGuid: $parentGuid) {
                total
                edges {
                    ... on MagazineIssue {
                        guid
                    }
                }
            }
        }
        """

    def test_filter_magazine_1_issues(self):
        self.graphql_client.force_login(self.user)
        result = self.graphql_client.post(
            self.query, {"parentGuid": self.magazine1.guid}
        )
        self.assertEqual(len(result["data"]["entities"]["edges"]), 2)
        self.assertEqual(
            {issue["guid"] for issue in result["data"]["entities"]["edges"]},
            {self.issue1_1.guid, self.issue1_2.guid},
        )
