from elasticsearch_dsl import Q, Search

from core.tests.helpers import ElasticsearchTestCase
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import EditorFactory, UserFactory


class AddEntityTestCase(ElasticsearchTestCase):
    def setUp(self):
        super().setUp()

        self.owner = UserFactory(name="Jan de Vries")
        magazine = MagazineFactory(owner=EditorFactory())
        self.data = MagazineIssueFactory(
            title="test", container=magazine, owner=self.owner
        )

        self.populate_index()

    def test_blog_document(self):
        s = (
            Search(index="magazine_issue")
            .query(Q("simple_query_string", query="Jan", fields=["owner.name"]))
            .filter("term", tenant_name=self.tenant.schema_name)
        )
        response = s.execute()

        self.assertEqual(len(response), 1)
        hit = next(iter(response))
        self.assertEqual(hit.id, self.data.guid)
        self.assertEqual(hit.title, self.data.title)
        self.assertEqual(hit.owner.name, self.owner.name)
