from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import EditorFactory


class MagazineIssueFactoryMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._magazine = False

    def build_magazine_issue(self, **kwargs):
        if "container" not in kwargs:
            if not self._magazine:
                self._magazine = MagazineFactory(
                    owner=EditorFactory(
                        email="magzineOwner@MagazineIssueFactoryMixin.com"
                    )
                )
            kwargs["container"] = self._magazine
        return MagazineIssueFactory(**kwargs)


FULL_MAGAZINE_ISSUE_FRAGMENT = """
{
    abstract
    accessId
    articles { guid }
    backgroundColor
    canArchiveAndDelete
    canBookmark
    canComment
    canEdit
    canEditAdvanced
    canEditGroup
    canVote
    colophon { key, value, label }
    container { guid }
    description
    excerpt
    featured {
        video
        image { guid }
    }
    group { guid }
    guid
    inGroup
    inputLanguage
    isBookmarked
    isFeatured
    isPinned
    isRecommended
    isRecommendedInSearch
    issueNumber
    isTranslationEnabled
    lastSeen
    localAbstract
    localDescription
    localExcerpt
    localRichDescription
    localTitle
    owner { guid }
    publishRequest { guid }
    richDescription
    scheduleArchiveEntity
    scheduleDeleteEntity
    showOwner
    status
    statusPublished
    subtype
    tagCategories { name, values }
    tags
    timeCreated
    timePublished
    timeUpdated
    title
    url
    views
    writeAccessId
}
"""
