from core.tests.queries.test_search_with_excluded_content_types import Template
from entities.blog.factories import BlogFactory
from entities.magazine.tests.issue.shared import MagazineIssueFactoryMixin


class TestSearchWithExcludedContentTypesTestCase(
    MagazineIssueFactoryMixin,
    Template.TestSearchWithExcludedContentTypesTestCase,
):
    EXCLUDE_TYPES = ["magazine_issue"]

    def build_included_article(self, title):
        return BlogFactory(title=title, owner=self.owner)

    def build_excluded_article(self, title):
        return self.build_magazine_issue(title=title, owner=self.owner)
