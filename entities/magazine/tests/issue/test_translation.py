from core.tests.helpers.test_translation import Wrapper

from .shared import MagazineIssueFactoryMixin


class TestTranslationFields(
    MagazineIssueFactoryMixin,
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestAbastractFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "MagazineIssue"

    def build_entity(self, **kwargs):
        return self.build_magazine_issue(**kwargs)
