from django.core.files.base import ContentFile
from django.test import tag
from django.utils.timezone import now, timedelta

from core.constances import SitePlanChoices
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from entities.magazine.factories import MagazineFactory
from entities.magazine.models import MagazineIssue
from user.factories import EditorFactory, UserFactory

from .shared import FULL_MAGAZINE_ISSUE_FRAGMENT


@tag("createEntity")
class TestEntityCreate(PleioTenantTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.override_config(SITE_PLAN=SitePlanChoices.pro_plus)
        self.override_config(OPEN_FOR_CREATE_CONTENT_TYPES=["magazine_issue"])

        self.owner = UserFactory(email="owner@example.com")
        self.magazine = MagazineFactory(
            owner=EditorFactory(email="editor@example.com"),
            colophon_fields=[
                {"key": "key", "label": "label"},
                {"key": "key2", "label": "label2"},
            ],
        )

        self.mutation = """
        fragment MagazineIssueFragment on MagazineIssue %(fragment)s
        mutation AddEntity($input: addEntityInput!) {
            addEntity(input: $input) {
                entity { ...MagazineIssueFragment }
            }
        }
        """ % {"fragment": FULL_MAGAZINE_ISSUE_FRAGMENT}

    def send_mutation(self, user, variables):
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, variables)
        return response["data"]["addEntity"]

    def get_common_expected_fields(self, db_result):
        return {
            "accessId": 2,
            "articles": [],
            "backgroundColor": None,
            "canArchiveAndDelete": True,
            "canBookmark": True,
            "canComment": False,
            "canEdit": True,
            "canEditGroup": False,
            "canVote": False,
            "container": {"guid": self.magazine.guid},
            "guid": db_result.guid,
            "isBookmarked": False,
            "isFeatured": False,
            "isPinned": False,
            "isRecommended": False,
            "isRecommendedInSearch": False,
            "isTranslationEnabled": True,
            "lastSeen": None,
            "localAbstract": None,
            "localDescription": None,
            "localExcerpt": "",
            "localRichDescription": None,
            "localTitle": None,
            "owner": {"guid": db_result.owner.guid},
            "publishRequest": None,
            "showOwner": True,
            "status": 200,
            "statusPublished": "published",
            "subtype": db_result.type_to_string,
            "tagCategories": [],
            "tags": [],
            "url": db_result.url,
            "views": 0,
            "writeAccessId": 0,
        }

    def test_entity_create_with_basic_plan(self):
        self.override_config(SITE_PLAN=SitePlanChoices.basic)

        with self.assertGraphQlError("could_not_add"):
            self.send_mutation(self.owner, self.get_minimum_create_arguments())

    def test_entity_create_with_plus_plan(self):
        self.override_config(SITE_PLAN=SitePlanChoices.plus)

        with self.assertGraphQlError("could_not_add"):
            self.send_mutation(self.owner, self.get_minimum_create_arguments())

    def get_minimum_create_arguments(self):
        return {
            "input": {
                "containerGuid": self.magazine.guid,
                "subtype": "magazine_issue",
                "accessId": 2,
                "writeAccessId": 0,
            }
        }

    def test_entity_minimum_required_fields(self):
        # When
        data = self.send_mutation(self.owner, self.get_minimum_create_arguments())
        # Then
        api_result = data["entity"]
        db_result = MagazineIssue.objects.last()
        self.assertIsNotNone(api_result)
        self.assertIsNotNone(db_result)

        # And
        self.assertEqual(
            api_result,
            {
                "abstract": None,
                "canEditAdvanced": False,
                "description": None,
                "excerpt": "",
                "featured": {"image": None, "video": None},
                "group": None,
                "inGroup": False,
                "inputLanguage": "nl",
                "richDescription": "",
                "scheduleArchiveEntity": None,
                "scheduleDeleteEntity": None,
                "timeCreated": db_result.created_at.isoformat(),
                "timePublished": db_result.published.isoformat(),
                "timeUpdated": db_result.updated_at.isoformat(),
                "title": "",
                "colophon": [
                    {"key": "key", "label": "label", "value": None},
                    {"key": "key2", "label": "label2", "value": None},
                ],
                "issueNumber": None,
                **self.get_common_expected_fields(db_result),
            },
        )

    def test_entity_maximum_allowed_fields(self):
        # Given
        self.override_config(EXTRA_LANGUAGES=["en"])
        TOMORROW = now() + timedelta(days=1)
        NEXT_WEEK = TOMORROW + timedelta(days=6)
        GROUP = GroupFactory(owner=self.owner)
        EXPECTED_ABSTRACT = self.tiptap_paragraph(
            "Except for the event when the president elect is..."
        )
        EXPECTED_RICH_DESCRIPTION = self.tiptap_paragraph("Some name with a D.")
        FEATURED_FILE = FileFactory(
            owner=self.owner, upload=ContentFile(b"content", "featured.jpg")
        )

        # When
        data = self.send_mutation(
            self.owner,
            {
                "input": {
                    "containerGuid": self.magazine.guid,
                    "subtype": "magazine_issue",
                    "accessId": 2,
                    "issueNumber": 42,
                    "writeAccessId": 0,
                    "inputLanguage": "en",
                    "isTranslationEnabled": True,
                    "title": "Sam Harris is always intellectually honest. Except...",
                    "abstract": EXPECTED_ABSTRACT,
                    "richDescription": EXPECTED_RICH_DESCRIPTION,
                    "isRecommended": True,
                    "isRecommendedInSearch": True,
                    "isFeatured": True,
                    "featured": {"imageGuid": FEATURED_FILE.guid},
                    "scheduleArchiveEntity": TOMORROW.isoformat(),
                    "scheduleDeleteEntity": NEXT_WEEK.isoformat(),
                    "groupGuid": GROUP.guid,
                    "backgroundColor": "#AABBDD",
                    "colophon": [{"key": "key", "value": "value"}],
                }
            },
        )

        # Then
        api_result = data["entity"]
        db_result = MagazineIssue.objects.last()
        self.assertIsNotNone(api_result)
        self.assertIsNotNone(db_result)

        # And
        self.assertEqual(
            api_result,
            {
                "abstract": EXPECTED_ABSTRACT,
                "canEditAdvanced": True,
                "colophon": [
                    {"key": "key", "value": "value", "label": "label"},
                    {"key": "key2", "value": None, "label": "label2"},
                ],
                "description": "Some name with a D.\n\n",
                "excerpt": "Except for the event when the president elect is...\n\n",
                "featured": {
                    "video": "",
                    "image": {"guid": db_result.featured_image.guid},
                },
                "group": {"guid": GROUP.guid},
                "inGroup": True,
                "inputLanguage": "en",
                "richDescription": EXPECTED_RICH_DESCRIPTION,
                "scheduleArchiveEntity": TOMORROW.isoformat(),
                "scheduleDeleteEntity": NEXT_WEEK.isoformat(),
                "timeCreated": db_result.created_at.isoformat(),
                "timePublished": db_result.published.isoformat(),
                "timeUpdated": db_result.updated_at.isoformat(),
                "title": "Sam Harris is always intellectually honest. Except...",
                "issueNumber": 42,
                **self.get_common_expected_fields(db_result),
            },
        )
