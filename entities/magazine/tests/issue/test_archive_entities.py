from core.tests.helpers.test_archive_entities import Wrapper
from entities.magazine.tests.issue.shared import MagazineIssueFactoryMixin


class TestArchiveEntities(MagazineIssueFactoryMixin, Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return self.build_magazine_issue(**kwargs)
