from django.core.files.base import ContentFile
from django.test import tag
from django.utils.timezone import now, timedelta

from core.constances import SitePlanChoices
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from entities.magazine.models import Magazine
from user.factories import EditorFactory, UserFactory

from .shared import FULL_MAGAZINE_FRAGMENT


@tag("createEntity")
class TestEntityCreate(PleioTenantTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.override_config(SITE_PLAN=SitePlanChoices.pro_plus)

        self.owner = EditorFactory(email="owner@example.com")

        self.mutation = """
        fragment MagazineFragment on Magazine %(fragment)s
        mutation AddEntity($input: addEntityInput!) {
            addEntity(input: $input) {
                entity { ...MagazineFragment }
            }
        }
        """ % {"fragment": FULL_MAGAZINE_FRAGMENT}

    def send_mutation(self, user, variables):
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, variables)
        return response["data"]["addEntity"]

    @staticmethod
    def get_common_response_fields(db_result):
        """Fields that are common for any create-input"""
        return {
            "accessId": 2,
            "canArchiveAndDelete": True,
            "canBookmark": True,
            "canComment": False,
            "canEdit": True,
            "canEditGroup": False,
            "canVote": False,
            "guid": db_result.guid,
            "isBookmarked": False,
            "lastSeen": None,
            "localAbstract": None,
            "localDescription": None,
            "localExcerpt": "",
            "localRichDescription": None,
            "localSubtitle": None,
            "localTitle": None,
            "owner": {"guid": db_result.owner.guid},
            "isPinned": False,
            "publishRequest": None,
            "showOwner": True,
            "status": 200,
            "statusPublished": "published",
            "subtype": db_result.type_to_string,
            "tagCategories": [],
            "tags": [],
            "timeCreated": db_result.created_at.isoformat(),
            "timePublished": db_result.published.isoformat(),
            "timeUpdated": db_result.updated_at.isoformat(),
            "url": db_result.url,
            "views": 0,
            "writeAccessId": 0,
        }

    def test_entity_create_as_normal_user(self):
        # Given
        user = UserFactory(email="nonprivileged@example.com")

        # When... Then.
        with self.assertGraphQlError("could_not_add"):
            self.send_mutation(user, self.get_minimum_create_arguments())

    def test_entity_create_with_basic_plan(self):
        self.override_config(SITE_PLAN=SitePlanChoices.basic)

        with self.assertGraphQlError("could_not_add"):
            self.send_mutation(self.owner, self.get_minimum_create_arguments())

    def test_entity_create_with_plus_plan(self):
        self.override_config(SITE_PLAN=SitePlanChoices.plus)

        with self.assertGraphQlError("could_not_add"):
            self.send_mutation(self.owner, self.get_minimum_create_arguments())

    @staticmethod
    def get_minimum_create_arguments():
        return {
            "input": {
                "subtype": "magazine",
                "accessId": 2,
                "writeAccessId": 0,
            }
        }

    def test_entity_minimum_required_fields(self):
        # When
        data = self.send_mutation(self.owner, self.get_minimum_create_arguments())
        # Then
        api_result = data["entity"]
        db_result = Magazine.objects.last()
        self.assertIsNotNone(api_result)
        self.assertIsNotNone(db_result)

        # And
        self.assertEqual(
            api_result,
            {
                "abstract": None,
                "icon": None,
                "description": None,
                "excerpt": "",
                "group": None,
                "inGroup": False,
                "canEditAdvanced": False,
                "inputLanguage": "nl",
                "isFeatured": False,
                "isRecommended": False,
                "isRecommendedInSearch": False,
                "isTranslationEnabled": True,
                "richDescription": "",
                "scheduleArchiveEntity": None,
                "scheduleDeleteEntity": None,
                "title": "",
                "subtitle": "",
                "colophonFields": [],
                **self.get_common_response_fields(db_result),
            },
        )

    def test_entity_maximum_allowed_fields(self):
        # Given
        self.override_config(EXTRA_LANGUAGES=["en"])
        TOMORROW = now() + timedelta(days=1)
        NEXT_WEEK = TOMORROW + timedelta(days=6)
        GROUP = GroupFactory(owner=self.owner)
        EXPECTED_ABSTRACT = self.tiptap_paragraph(
            "Except for the event when the president elect is..."
        )
        EXPECTED_RICH_DESCRIPTION = self.tiptap_paragraph(
            "Some guy who's name starts with a D."
        )

        self.icon_file = FileFactory(
            owner=self.owner, upload=ContentFile(b"content", "icon.jpg")
        )

        # When
        data = self.send_mutation(
            self.owner,
            {
                "input": {
                    "subtype": "magazine",
                    "accessId": 2,
                    "writeAccessId": 0,
                    "inputLanguage": "en",
                    "isTranslationEnabled": True,
                    "title": "Sam Harris is always intellectually honest. Except...",
                    "subtitle": "You know, for the record",
                    "abstract": EXPECTED_ABSTRACT,
                    "richDescription": EXPECTED_RICH_DESCRIPTION,
                    "isRecommended": True,
                    "isRecommendedInSearch": True,
                    "isFeatured": True,
                    "scheduleArchiveEntity": TOMORROW.isoformat(),
                    "scheduleDeleteEntity": NEXT_WEEK.isoformat(),
                    "groupGuid": GROUP.guid,
                    "iconGuid": self.icon_file.guid,
                    "colophonFields": [{"label": "Field1"}, {"label": "Field2"}],
                }
            },
        )

        # Then
        api_result = data["entity"]
        self.assertIsNotNone(api_result)
        db_result = Magazine.objects.last()
        self.assertIsNotNone(db_result)

        # And
        self.assertEqual(
            api_result,
            {
                "abstract": EXPECTED_ABSTRACT,
                "icon": {"guid": self.icon_file.guid},
                "description": "Some guy who's name starts with a D.\n\n",
                "excerpt": "Except for the event when the president elect is...\n\n",
                "group": {"guid": GROUP.guid},
                "inGroup": True,
                "canEditAdvanced": True,
                "inputLanguage": "en",
                "isFeatured": True,
                "isRecommended": True,
                "isRecommendedInSearch": True,
                "isTranslationEnabled": True,
                "richDescription": EXPECTED_RICH_DESCRIPTION,
                "scheduleArchiveEntity": TOMORROW.isoformat(),
                "scheduleDeleteEntity": NEXT_WEEK.isoformat(),
                "title": "Sam Harris is always intellectually honest. Except...",
                "subtitle": "You know, for the record",
                "colophonFields": [
                    {"key": db_result.colophon_fields[0]["key"], "label": "Field1"},
                    {"key": db_result.colophon_fields[1]["key"], "label": "Field2"},
                ],
                **self.get_common_response_fields(db_result),
            },
        )
