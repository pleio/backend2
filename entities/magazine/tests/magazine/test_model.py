from unittest.mock import patch

from core.tests.helpers import PleioTenantTestCase
from entities.magazine.factories import MagazineFactory, MagazineIssueFactory
from user.factories import EditorFactory


class TestModelTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.owner = EditorFactory(email="owner@example.com")
        self.magazine = MagazineFactory(owner=self.owner)
        self.issue = MagazineIssueFactory(owner=self.owner, container=self.magazine)

    @patch("entities.magazine.models.MagazineIssue.get_all_children")
    def test_get_all_children(self, mocked_get_all_issue_children):
        self.assertEqual([*self.magazine.get_all_children()], [self.issue])
        mocked_get_all_issue_children.assert_called_once()
