from core.tests.helpers import PleioTenantTestCase
from core.tests.helpers.test_delete_entities import Wrapper
from entities.magazine.factories import MagazineIssueFactory
from entities.magazine.models import Magazine, MagazineIssue
from entities.magazine.tests.magazine.shared import MagazineFactoryMixin
from user.factories import EditorFactory, UserFactory


class TestDeleteEntities(MagazineFactoryMixin, Wrapper.TestDeleteEntities):
    def build_entity(self, **kwargs):
        return self.build_magazine(**kwargs)


class TestRecursiveDeleteEntities(MagazineFactoryMixin, PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.user = EditorFactory()
        self.magazine = self.build_magazine(owner=self.user, title="THE Magazine")
        self.issues = [
            MagazineIssueFactory(
                owner=self.user, title="01. First", container=self.magazine
            ),
            MagazineIssueFactory(
                owner=self.user, title="02. Second", container=self.magazine
            ),
            MagazineIssueFactory(
                owner=self.user, title="03. Third", container=self.magazine
            ),
        ]

        self.mutation = """
            mutation DeleteEntities($input: deleteEntityInput!) {
                deleteEntity(input: $input) {
                    success
                }
            }
        """
        self.variables = {
            "input": {
                "guid": self.magazine.guid,
                "recursive": "tryDelete",
            }
        }

    def submit_mutation(self, user):
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, self.variables)
        return response["data"]["deleteEntity"]

    def test_delete_entities(self):
        # When
        response = self.submit_mutation(self.user)

        # Then
        self.assertEqual(response, {"success": True})
        self.assertFalse(Magazine.objects.exists())
        self.assertFalse(MagazineIssue.objects.exists())

    def test_try_delete_entities(self):
        # Given
        MagazineIssueFactory(
            owner=UserFactory(email="someone@else.com"),
            title="04 Fourth",
            container=self.magazine,
        )

        # When
        response = self.submit_mutation(self.user)

        # Then
        self.assertEqual(response, {"success": False})
        self.assertTrue(Magazine.objects.exists())
        self.assertEqual(MagazineIssue.objects.count(), 4)

    def test_soft_delete_entities(self):
        # Given
        protected_issue = MagazineIssueFactory(
            owner=UserFactory(email="someone@else.com"),
            title="04 Fourth",
            container=self.magazine,
        )
        self.variables["input"]["recursive"] = "softDelete"

        # When
        response = self.submit_mutation(self.user)

        self.magazine.refresh_from_db()

        # Then
        self.assertEqual(response, {"success": True})
        self.assertTrue(Magazine.objects.exists())
        self.assertEqual([*MagazineIssue.objects.all()], [protected_issue])

    def test_hard_delete_entities(self):
        # Given
        MagazineIssueFactory(
            owner=UserFactory(email="someone@else.com"),
            title="04 Fourth",
            container=self.magazine,
        )
        self.variables["input"]["recursive"] = "forceDelete"

        # When
        with self.assertGraphQlError("could_not_delete"):
            self.submit_mutation(self.user)
