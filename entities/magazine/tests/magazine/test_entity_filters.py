from core.tests.helpers.entity_filters import Template
from entities.blog.factories import BlogFactory
from entities.magazine.tests.magazine.shared import MagazineFactoryMixin


class TestEntityFilters(MagazineFactoryMixin, Template.TestEntityFiltersTestCase):
    include_activity_query = False

    def get_subtype(self):
        return "magazine"

    def subtype_factory(self, **kwargs):
        return self.build_magazine(**kwargs)

    def reference_factory(self, **kwargs):
        return BlogFactory(**kwargs)
