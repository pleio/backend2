from core.constances import USER_ROLES
from entities.magazine.factories import MagazineFactory


class MagazineFactoryMixin:
    def build_magazine(self, **kwargs):
        assert "owner" in kwargs
        if not kwargs["owner"].is_editor:
            kwargs["owner"].roles.append(USER_ROLES.EDITOR)
            kwargs["owner"].save()
        return MagazineFactory(**kwargs)


FULL_MAGAZINE_FRAGMENT = """
{
    abstract
    accessId
    canArchiveAndDelete
    canBookmark
    canComment
    canEdit
    canEditAdvanced
    canEditGroup
    canVote
    colophonFields { key, label }
    description
    excerpt
    group { guid }
    guid
    icon { guid }
    inGroup
    inputLanguage
    isBookmarked
    isFeatured
    isPinned
    isRecommended
    isRecommendedInSearch
    isTranslationEnabled
    lastSeen
    localAbstract
    localDescription
    localExcerpt
    localRichDescription
    localSubtitle
    localTitle
    owner { guid }
    publishRequest { guid }
    richDescription
    scheduleArchiveEntity
    scheduleDeleteEntity
    showOwner
    status
    statusPublished
    subtitle
    subtype
    tagCategories { name, values }
    tags
    timeCreated
    timePublished
    timeUpdated
    title
    url
    views
    writeAccessId
}
"""
