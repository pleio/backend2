from core.tests.helpers.test_archive_entities import Wrapper
from entities.magazine.tests.magazine.shared import MagazineFactoryMixin


class TestArchiveEntities(MagazineFactoryMixin, Wrapper.TestArchiveEntities):
    def create_entity(self, **kwargs):
        return self.build_magazine(**kwargs)
