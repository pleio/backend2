from core.constances import SitePlanChoices
from core.tests.helpers.tags_testcase import Template
from entities.magazine.models import Magazine
from user.factories import EditorFactory


class TestBlogTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "Magazine"
    model = Magazine

    def setUp(self):
        super().setUp()
        self.override_config(SITE_PLAN=SitePlanChoices.pro_plus)

    def owner_factory(self):
        return EditorFactory(email="editor-owner@example.com")

    def variables_add(self):
        return {
            "input": {
                "title": "Test magazine",
                "subtype": "magazine",
            }
        }

    include_entity_search = True
