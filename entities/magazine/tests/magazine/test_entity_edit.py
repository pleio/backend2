from django.core.files.base import ContentFile
from django.utils.timezone import now, timedelta

from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from entities.file.factories import FileFactory
from entities.magazine.factories import MagazineFactory
from user.factories import AdminFactory, EditorFactory

from .shared import FULL_MAGAZINE_FRAGMENT


class TestEntityEditTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.override_config(EXTRA_LANGUAGES=["en"])
        self.owner = EditorFactory(email="owner@example.com")
        self.next_owner = EditorFactory(email="next@example.com")
        self.next_group = GroupFactory(owner=self.next_owner)

        self.INITIAL_CREATED = now() - timedelta(days=333)
        self.INITIAL_PUBLISHED = now() - timedelta(days=333)
        self.magazine = MagazineFactory(
            owner=self.owner,
            created_at=self.INITIAL_CREATED,
            published=self.INITIAL_PUBLISHED,
        )

        self.mutation = """
        fragment MagazineFragment on Magazine %(fragment)s
        mutation EditEntity($input: editEntityInput!) {
            editEntity(input: $input) {
                entity { ...MagazineFragment }
            }
        }
        """ % {"fragment": FULL_MAGAZINE_FRAGMENT}

        self.FOREVER = now() - timedelta(days=777)
        self.YESTERDAY = now() - timedelta(days=1)
        self.TOMORROW = now() + timedelta(days=1)
        self.NEXT_WEEK = now() + timedelta(days=7)

        self.icon = FileFactory(
            owner=self.owner, upload=ContentFile(b"content", "icon.jpg")
        )

        self.variables = {
            "input": {
                "guid": self.magazine.guid,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
                "title": "I don't mean to be mean",
                "subtitle": "You know, for the record",
                "abstract": self.tiptap_paragraph("It's just that"),
                "richDescription": self.tiptap_paragraph(
                    "I don't accept this paragraph"
                ),
                "isRecommended": True,
                "isRecommendedInSearch": True,
                "isFeatured": True,
                "accessId": 1,
                "writeAccessId": 1,
                "scheduleArchiveEntity": self.TOMORROW.isoformat(),
                "scheduleDeleteEntity": self.NEXT_WEEK.isoformat(),
                "timeCreated": self.FOREVER.isoformat(),
                "timePublished": self.YESTERDAY.isoformat(),
                "ownerGuid": self.next_owner.guid,
                "groupGuid": self.next_group.guid,
                "iconGuid": self.icon.guid,
                "colophonFields": [{"label": "Field1"}, {"label": "Field2"}],
            }
        }

    def send_mutation(self, user):
        self.graphql_client.force_login(user)
        response = self.graphql_client.post(self.mutation, self.variables)
        return response["data"]["editEntity"]

    def get_common_expectation_fields(self):
        """
        Result that is the same regardless of the acting user with sufficient authorization.
        """
        return {
            "guid": self.magazine.guid,
            "status": 200,
            "subtype": "magazine",
            "inputLanguage": "en",
            "isTranslationEnabled": False,
            "icon": {"guid": self.icon.guid},
            "colophonFields": [
                {"key": self.magazine.colophon_fields[0]["key"], "label": "Field1"},
                {"key": self.magazine.colophon_fields[1]["key"], "label": "Field2"},
            ],
            "title": "I don't mean to be mean",
            "localTitle": None,
            "subtitle": "You know, for the record",
            "localSubtitle": None,
            "abstract": self.tiptap_paragraph("It's just that"),
            "localAbstract": None,
            "description": "I don't accept this paragraph\n\n",
            "localDescription": None,
            "richDescription": self.tiptap_paragraph("I don't accept this paragraph"),
            "localRichDescription": None,
            "excerpt": "It's just that\n\n",
            "localExcerpt": "",
            "url": self.magazine.url,
            "tags": [],
            "tagCategories": [],
            "timeUpdated": self.magazine.updated_at.isoformat(),
            "timePublished": self.YESTERDAY.isoformat(),
            "statusPublished": "published",
            "scheduleArchiveEntity": self.TOMORROW.isoformat(),
            "scheduleDeleteEntity": self.NEXT_WEEK.isoformat(),
            "isFeatured": True,
            "isRecommended": True,
            "isRecommendedInSearch": True,
            "canEdit": True,
            "canArchiveAndDelete": True,
            "canBookmark": True,
            "canComment": False,
            "canVote": False,
            "accessId": 1,
            "writeAccessId": 1,
            "isBookmarked": False,
            "views": 0,
            "showOwner": True,
            "isPinned": False,
            "lastSeen": None,
            "publishRequest": None,
        }

    def test_edit_magazine(self):
        """
        Test that the owner can edit a magazine
        """
        response = self.send_mutation(self.owner)
        api_result = response["entity"]
        self.magazine.refresh_from_db()

        self.maxDiff = None
        self.assertEqual(
            api_result,
            {
                "timeCreated": self.INITIAL_CREATED.isoformat(),
                "canEditAdvanced": False,
                "canEditGroup": False,
                "owner": {"guid": self.owner.guid},
                "inGroup": False,
                "group": None,
                **self.get_common_expectation_fields(),
            },
        )
        self.assertIsNotNone(self.magazine.colophon_fields[0]["key"])
        self.assertIsNotNone(self.magazine.colophon_fields[1]["key"])

    def test_edit_magazine_as_admin(self):
        """
        Test that an admin can edit a magazine, including the advanced fields
        """
        response = self.send_mutation(AdminFactory())
        api_result = response["entity"]
        self.magazine.refresh_from_db()

        self.maxDiff = None
        self.assertEqual(
            api_result,
            {
                "timeCreated": self.FOREVER.isoformat(),
                "canEditAdvanced": True,
                "canEditGroup": True,
                "owner": {"guid": self.next_owner.guid},
                "inGroup": True,
                "group": {"guid": self.next_group.guid},
                **self.get_common_expectation_fields(),
            },
        )
        self.assertIsNotNone(self.magazine.colophon_fields[0]["key"])
        self.assertIsNotNone(self.magazine.colophon_fields[1]["key"])

        self.assertDateEqual(api_result["timeCreated"], self.FOREVER)
        self.assertTrue(api_result["canEditAdvanced"], True)
        self.assertTrue(api_result["canEditGroup"], True)
        self.assertEqual(api_result["owner"]["guid"], self.next_owner.guid)
        self.assertTrue(api_result["inGroup"], True)
        self.assertEqual(api_result["group"]["guid"], self.next_group.guid)
