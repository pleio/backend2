# Generated by Django 4.2.16 on 2024-12-04 08:18

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("magazine", "0002_rename_colofon_to_colophon"),
    ]

    operations = [
        migrations.AddField(
            model_name="magazineissue",
            name="issue_number",
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="magazineissue",
            name="issue_year",
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
