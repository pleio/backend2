from mixer.backend.django import mixer

from core.constances import ACCESS_TYPE
from entities.magazine.models import MagazineIssue
from entities.magazine.models.issue import MagazineIssueArticle


def MagazineFactory(**kwargs):
    assert "owner" in kwargs, "Owner field is required"
    assert kwargs["owner"].is_editor, "Owner must be an editor or higher"
    kwargs.setdefault("read_access", [ACCESS_TYPE.public])
    kwargs.setdefault("write_access", [ACCESS_TYPE.user.format(kwargs.get("owner").id)])
    return mixer.blend("magazine.Magazine", **kwargs)


def MagazineIssueFactory(**kwargs):
    assert "owner" in kwargs, "Owner field is required"
    assert "container" in kwargs, "Magazine field (container) is required"
    kwargs.setdefault("read_access", [ACCESS_TYPE.public])
    kwargs.setdefault("write_access", [ACCESS_TYPE.user.format(kwargs.get("owner").id)])
    return mixer.blend("magazine.MagazineIssue", **kwargs)


def MagazineArticleFactory(magazine_issue, **kwargs):
    assert "owner" in kwargs, "Owner field is required"
    assert isinstance(magazine_issue, MagazineIssue), "MagazineIssue field is required"
    kwargs.setdefault("read_access", [ACCESS_TYPE.public])
    kwargs.setdefault("write_access", [ACCESS_TYPE.user.format(kwargs.get("owner").id)])
    article_reference = MagazineIssueArticle.objects.create(
        issue=magazine_issue, _article=mixer.blend("blog.Blog", **kwargs)
    )
    return article_reference.article
