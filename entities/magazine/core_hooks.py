from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy


def get_entity_filters():
    yield {
        "key": "magazine",
        "value": _("Magazine"),
    }
    yield {
        "key": "magazine_issue",
        "value": _("Magazine issue"),
    }


def get_activity_filters():
    yield {
        "key": "magazine_issue",
        "value": _("Magazine issue"),
    }


def get_search_filters():
    yield {
        "key": "magazine_issue",
        "value": _("Magazine issue"),
        "plural": pgettext_lazy("plural", "Magazine issues"),
    }
