from auditlog.registry import auditlog
from django.db import models

from core import config
from core.constances import SitePlanChoices
from core.models import AttachmentMixin, Entity
from core.models.mixin import (
    ArticleMixin,
    BookmarkMixin,
    FollowMixin,
    HierarchicalEntityMixin,
    RichDescriptionMediaMixin,
    TitleMixin,
)


class Magazine(
    HierarchicalEntityMixin,
    RichDescriptionMediaMixin,
    TitleMixin,
    BookmarkMixin,
    FollowMixin,
    AttachmentMixin,
    ArticleMixin,
    Entity,
):
    ALLOWED_SITE_PLANS = [SitePlanChoices.pro, SitePlanChoices.pro_plus]

    class Meta:
        ordering = ("-published",)

    colophon_fields = models.JSONField(null=True, default=list, blank=True)
    icon = models.ForeignKey(
        "file.FileFolder", on_delete=models.PROTECT, null=True, blank=True
    )

    def __str__(self):
        return self.title

    def has_children(self):
        return self.issues.exists()

    def get_children(self):
        return self.issues.all()

    def has_revisions(self):
        return False

    @property
    def type_to_string(self):
        return "magazine"

    @property
    def url(self):
        return "{}/magazine/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    @classmethod
    def can_add(cls, user, group=None):
        if config.SITE_PLAN not in cls.ALLOWED_SITE_PLANS:
            return False

        if group and (group.is_owner(user) or group.is_admin(user)):
            return True

        return super().can_add(user, group) and (user.is_editor or user.is_site_admin)

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def lookup_attachments(self):
        yield from super().lookup_attachments()
        if self.icon:
            yield self.icon.guid

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "abstract": self.abstract or "",
            "colophonFields": self.colophon_fields,
            "iconGuid": str(self.icon_id),
            **super().serialize(),
        }


auditlog.register(Magazine)
