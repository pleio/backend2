from auditlog.registry import auditlog
from django.db import models

from core import config
from core.constances import SitePlanChoices
from core.models import AttachmentMixin, Entity
from core.models.featured import FeaturedCoverMixin
from core.models.mixin import (
    ArticleMixin,
    BookmarkMixin,
    FollowMixin,
    HierarchicalEntityMixin,
    RichDescriptionMediaMixin,
    TitleMixin,
)
from core.utils.entity import load_entity_by_id


class MagazineIssue(
    HierarchicalEntityMixin,
    RichDescriptionMediaMixin,
    TitleMixin,
    FeaturedCoverMixin,
    BookmarkMixin,
    FollowMixin,
    AttachmentMixin,
    ArticleMixin,
    Entity,
):
    ALLOWED_SITE_PLANS = [SitePlanChoices.pro, SitePlanChoices.pro_plus]

    class Meta:
        ordering = ("-published",)

    colophon = models.JSONField(default=dict, null=True, blank=True)
    container = models.ForeignKey(
        "magazine.Magazine", on_delete=models.PROTECT, related_name="issues"
    )

    issue_number = models.IntegerField(null=True, blank=True)

    def set_articles(self, ids):
        self.article_references.all().delete()
        for weight, article_id in enumerate(ids):
            self.article_references.create(weight=weight, _article_id=article_id)

    def append_article(self, article_id):
        weight = self.article_references.count()
        self.article_references.create(weight=weight, _article_id=article_id)

    def has_children(self):
        return self.article_references.exists()

    def get_children(self):
        return [r.article for r in self.article_references.all()]

    def __str__(self):
        return self.title or self.container.title

    def has_revisions(self):
        return False

    @property
    def type_to_string(self):
        return "magazine_issue"

    @classmethod
    def content_type_from_class(cls):
        return "magazine_issue"

    @property
    def url(self):
        return "{}/magazine-issue/view/{}/{}".format(
            self.group.url if self.group else "",
            self.guid,
            self.slug,
        ).lower()

    @classmethod
    def can_add(cls, user, group=None):
        if config.SITE_PLAN not in cls.ALLOWED_SITE_PLANS:
            return False
        return super().can_add(user=user, group=group)

    @property
    def rich_fields(self):
        return [self.rich_description, self.abstract]

    def map_rich_text_fields(self, callback):
        self.rich_description = callback(self.rich_description)
        self.abstract = callback(self.abstract)

    def get_articles(self, user):
        return self.article_references.articles(user)

    def serialize(self):
        return {
            "title": self.title or "",
            "richDescription": self.rich_description or "",
            "abstract": self.abstract or "",
            "featured": self.serialize_featured(),
            "colophon": self.colophon,
            "container": self.container.guid,
            **super().serialize(),
        }


class MagazineIssueArticleQuerySet(models.QuerySet):
    def issue(self):
        try:
            return self.first().issue
        except AttributeError:
            pass

    def articles(self, user):
        from core.models import Entity

        articles_ids = [*self.values_list("_article_id", flat=True)][:100]
        qs = Entity.objects.visible(user).filter(id__in=articles_ids)
        entities = {e.guid: e for e in qs.select_subclasses()}
        return [entities[str(id)] for id in articles_ids if str(id) in entities]


class MagazineIssueArticle(models.Model):
    class Meta:
        ordering = ("weight",)

    objects = MagazineIssueArticleQuerySet.as_manager()

    weight = models.FloatField(null=False, default=0)

    # On an issue, call self.article_references.articles() to select the included articles
    issue = models.ForeignKey(
        "magazine.MagazineIssue",
        on_delete=models.CASCADE,
        related_name="article_references",
    )

    # On an article, call self.issue_references.issue() to select the issue at a magazine.
    _article = models.ForeignKey(
        "core.Entity",
        on_delete=models.CASCADE,
        related_name="issue_references",
        db_column="article",
    )

    @property
    def article(self):
        return load_entity_by_id(self._article_id, ["core.Entity"])

    def __str__(self):
        return "%s@%s" % (self.article, self.issue)


auditlog.register(MagazineIssue)
