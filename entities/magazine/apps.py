from django.apps import AppConfig


class MagazineConfig(AppConfig):
    name = "entities.magazine"
    label = "magazine"
