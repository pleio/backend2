from django.apps import AppConfig


class TaskConfig(AppConfig):
    name = "entities.task"
    label = "task"
