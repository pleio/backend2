from ariadne import ObjectType

from core.resolvers import shared

task = ObjectType("Task")


@task.field("subtype")
def resolve_subtype(obj, info):
    return obj.type_to_string


@task.field("url")
def resolve_url(obj, info):
    return obj.url


@task.field("state")
def resolve_state(obj, info):
    return obj.state


@task.field("canComment")
def resolve_can_comment(obj, info):
    """Deprecated: not by tasks"""
    return False


@task.field("canVote")
def resolve_can_vote(obj, info):
    """Deprecated: not by tasks"""
    return False


@task.field("isFollowing")
def resolve_is_following(obj, info):
    """Deprecated: not by tasks"""
    return False


@task.field("commentCount")
def resolve_comment_count(obj, info):
    """Deprecated: not by tasks"""
    return 0


@task.field("comments")
def resolve_comments(obj, info):
    """Deprecated: not by tasks"""
    return []


task.set_field("guid", shared.resolve_entity_guid)
task.set_field("status", shared.resolve_entity_status)
task.set_field("title", shared.resolve_entity_title)
task.set_field("description", shared.resolve_entity_description)
task.set_field("richDescription", shared.resolve_entity_rich_description)
task.set_field("excerpt", shared.resolve_entity_excerpt)
task.set_field("tags", shared.resolve_entity_tags)
task.set_field("tagCategories", shared.resolve_entity_categories)
task.set_field("timeCreated", shared.resolve_entity_time_created)
task.set_field("timeUpdated", shared.resolve_entity_time_updated)
task.set_field("timePublished", shared.resolve_entity_time_published)
task.set_field("scheduleArchiveEntity", shared.resolve_entity_schedule_archive_entity)
task.set_field("scheduleDeleteEntity", shared.resolve_entity_schedule_delete_entity)
task.set_field("statusPublished", shared.resolve_entity_status_published)
task.set_field("canEdit", shared.resolve_entity_can_edit)
task.set_field("canArchiveAndDelete", shared.resolve_entity_can_archive)
task.set_field("canEditAdvanced", shared.resolve_entity_can_edit_advanced)
task.set_field("canEditGroup", shared.resolve_entity_can_edit_group)
task.set_field("accessId", shared.resolve_entity_access_id)
task.set_field("writeAccessId", shared.resolve_entity_write_access_id)
task.set_field("views", shared.resolve_entity_views)
task.set_field("owner", shared.resolve_entity_owner)
task.set_field("group", shared.resolve_entity_group)
task.set_field("inGroup", shared.resolve_entity_in_group)
task.set_field("showOwner", shared.resolve_entity_show_owner)
