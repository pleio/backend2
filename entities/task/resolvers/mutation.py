import logging

from core.lib import clean_graphql_input
from core.resolvers import shared
from core.utils.entity import load_entity_by_id

from ..models import Task

logger = logging.getLogger(__name__)


def resolve_add_task(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    group = shared.get_group(clean_input)
    shared.assert_can_create(user, Task, group)

    entity = Task(owner=user, group=group)

    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_add_access_id(entity, clean_input, user)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)

    shared.update_publication_dates(entity, user, clean_input)

    entity.save()

    return {"entity": entity}


def resolve_edit_task(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [Task])

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)

    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)
    shared.update_publication_dates(entity, user, clean_input)
    shared.update_updated_at(entity)

    if user.is_site_admin or (entity.group and entity.group.can_write(user)):
        shared.resolve_update_owner(entity, clean_input)
        shared.resolve_update_time_created(entity, clean_input)

    if user.is_site_admin:
        shared.resolve_update_group(entity, clean_input)

    entity.save()

    return {"entity": entity}


def resolve_edit_task_state(_, info, input):
    user = info.context["request"].user
    entity = load_entity_by_id(input["guid"], [Task])

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)

    resolve_update_state(entity, clean_input)

    entity.save()

    return {"entity": entity}


def resolve_update_state(entity, clean_input):
    if "state" in clean_input:
        entity.state = clean_input.get("state")
