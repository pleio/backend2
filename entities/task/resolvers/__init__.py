from ariadne import ObjectType

from .mutation import resolve_edit_task_state
from .task import task

mutation = ObjectType("Mutation")
mutation.set_field("editTask", resolve_edit_task_state)

resolvers = [mutation, task]
