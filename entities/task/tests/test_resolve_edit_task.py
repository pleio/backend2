from django.utils import timezone

from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory

from ..factories import TaskFactory
from ..models import Task


class EditTaskTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = UserFactory()
        self.user2 = UserFactory()
        self.admin = AdminFactory()
        self.groupOwner = UserFactory()
        self.group = GroupFactory(owner=self.groupOwner)

        self.taskPublic = Task.objects.create(
            title="Test public update",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
        )

        self.data = {
            "input": {
                "guid": self.taskPublic.guid,
                "title": "My first update",
                "richDescription": "richDescription",
                "timePublished": str(timezone.localtime()),
                "scheduleArchiveEntity": str(
                    timezone.localtime() + timezone.timedelta(days=10)
                ),
                "scheduleDeleteEntity": str(
                    timezone.localtime() + timezone.timedelta(days=20)
                ),
            }
        }
        self.mutation = """
            fragment TaskParts on Task {
                title
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                accessId
                writeAccessId
                tags
                url
                inGroup
                group {
                    guid
                }
                owner {
                    guid
                }
                state
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...TaskParts
                    }
                }
            }
        """

    def test_edit_task(self):
        variables = self.data

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertDateEqual(
            entity["timeCreated"], self.taskPublic.created_at.isoformat()
        )
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["state"], "NEW")
        self.assertEqual(entity["group"], None)
        self.assertEqual(entity["owner"]["guid"], self.authenticatedUser.guid)
        self.assertDateEqual(
            entity["timePublished"], variables["input"]["timePublished"]
        )
        self.assertDateEqual(
            entity["scheduleArchiveEntity"], variables["input"]["scheduleArchiveEntity"]
        )
        self.assertDateEqual(
            entity["scheduleDeleteEntity"], variables["input"]["scheduleDeleteEntity"]
        )

        self.taskPublic.refresh_from_db()

        self.assertEqual(entity["title"], self.taskPublic.title)
        self.assertEqual(entity["richDescription"], self.taskPublic.rich_description)
        self.assertEqual(entity["state"], self.taskPublic.state)

    def test_edit_task_by_admin(self):
        variables = self.data
        variables["input"]["timeCreated"] = "2018-12-10T23:00:00.000Z"
        variables["input"]["groupGuid"] = self.group.guid
        variables["input"]["ownerGuid"] = self.user2.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["title"], variables["input"]["title"])
        self.assertEqual(
            entity["richDescription"], variables["input"]["richDescription"]
        )
        self.assertEqual(entity["state"], "NEW")
        self.assertEqual(entity["group"]["guid"], self.group.guid)
        self.assertEqual(entity["owner"]["guid"], self.user2.guid)
        self.assertDateEqual(entity["timeCreated"], "2018-12-10T23:00:00+00:00")

        self.taskPublic.refresh_from_db()

        self.assertEqual(entity["title"], self.taskPublic.title)
        self.assertEqual(entity["richDescription"], self.taskPublic.rich_description)
        self.assertEqual(entity["state"], self.taskPublic.state)

    def test_edit_task_group_null_by_admin(self):
        variables = self.data
        variables["input"]["groupGuid"] = self.group.guid

        self.graphql_client.force_login(self.admin)
        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["group"]["guid"], self.group.guid)

        variables["input"]["groupGuid"] = None

        result = self.graphql_client.post(self.mutation, variables)

        entity = result["data"]["editEntity"]["entity"]
        self.assertEqual(entity["group"], None)


class TestGroupTasks(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.taskOwner = UserFactory()
        self.newTaskOwner = UserFactory()
        self.groupOwner = UserFactory()
        self.groupAdmin = UserFactory()

        self.group = GroupFactory(owner=self.groupOwner)
        self.group.join(self.groupAdmin, "admin")
        self.group.join(self.taskOwner)
        self.group.join(self.newTaskOwner)

        self.taskPublic = TaskFactory(owner=self.taskOwner, group=self.group)

        self.query = """
        mutation UpdateTaskOwner($input: editEntityInput!) {
            editEntity(input: $input) {
                entity {
                    guid
                    ... on Task {
                        owner { guid }
                    }
                }
            }
        }
        """

        self.variables = {
            "input": {
                "guid": self.taskPublic.guid,
                "ownerGuid": self.newTaskOwner.guid,
            }
        }

    def assertOwnerIsChanged(self, result):
        self.assertEqual(
            result["data"]["editEntity"]["entity"],
            {"guid": self.taskPublic.guid, "owner": {"guid": self.newTaskOwner.guid}},
        )

    def assertOwnerIsNotChanged(self, result):
        self.assertEqual(
            result["data"]["editEntity"]["entity"],
            {"guid": self.taskPublic.guid, "owner": {"guid": self.taskOwner.guid}},
        )

    def test_update_task_owner_by_group_owner(self):
        self.graphql_client.force_login(self.groupOwner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertOwnerIsChanged(result)

    def test_update_task_owner_by_group_admin(self):
        self.graphql_client.force_login(self.groupAdmin)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertOwnerIsChanged(result)

    def test_update_task_owner_by_task_owner(self):
        self.graphql_client.force_login(self.taskOwner)
        result = self.graphql_client.post(self.query, self.variables)

        self.assertOwnerIsNotChanged(result)
