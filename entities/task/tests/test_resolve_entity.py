from core.constances import ACCESS_TYPE
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory

from ..factories import TaskFactory


class TaskTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.site_admin = AdminFactory(email="site_admin@example.com")
        self.owner = UserFactory(email="owner@example.com")
        self.admin = UserFactory(email="admin@example.com")
        self.member = UserFactory(email="member@example.com")

        self.group = GroupFactory(owner=self.owner)
        self.group.join(self.admin, "admin")
        self.group.join(self.member)

        common_properties = {
            "rich_description": "JSON to string",
            "owner": self.member,
            "group": self.group,
            "state": "NEW",
        }

        self.taskPublic = TaskFactory(
            **common_properties,
            title="Test public task",
            read_access=[ACCESS_TYPE.public],
        )

        self.taskProtected = TaskFactory(
            **common_properties,
            title="Test protected task",
            read_access=[ACCESS_TYPE.user.format(self.member.id)],
        )

        self.query = """
            fragment TaskParts on Task {
                # common properties
                title
                richDescription
                timeCreated
                timeUpdated
                timePublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                url
                inGroup
                group {
                    guid
                }
                state

                # specific properties
                accessId
                writeAccessId
                canEdit
                canArchiveAndDelete
                canEditAdvanced
                canEditGroup
            }
            query GetTask($public: String!, $protected: String!) {
                publicEntity: entity(guid: $public) {
                    status
                    ...TaskParts
                }
                protectedEntity: entity(guid: $protected) {
                    status
                    ...TaskParts
                }
            }
        """
        self.variables = {
            "public": self.taskPublic.guid,
            "protected": self.taskProtected.guid,
        }

    def post_query(self):
        try:
            self.graphql_client.post(self.query, self.variables)
        except Exception:
            # ignore errors
            pass
        return self.graphql_client.result

    def test_task_anonymous(self):
        result = self.post_query()

        self.assertCommonProperties(
            result["data"]["publicEntity"],
            self.taskPublic,
        )
        self.assertPublicAccessProperties(result["data"]["publicEntity"])
        self.assertFalse(result["data"]["publicEntity"]["canEdit"], False)
        self.assertFalse(result["data"]["publicEntity"]["canArchiveAndDelete"], False)
        self.assertFalse(result["data"]["publicEntity"]["canEditAdvanced"], False)
        self.assertFalse(result["data"]["publicEntity"]["canEditGroup"], False)

        self.assertIsNone(result["data"]["protectedEntity"])

    def test_task_member(self):
        self.graphql_client.force_login(self.member)
        result = self.post_query()

        self.assertCommonProperties(
            result["data"]["publicEntity"],
            self.taskPublic,
        )
        self.assertPublicAccessProperties(result["data"]["publicEntity"])
        self.assertTrue(result["data"]["publicEntity"]["canEdit"])
        self.assertTrue(result["data"]["publicEntity"]["canArchiveAndDelete"])
        self.assertFalse(result["data"]["publicEntity"]["canEditAdvanced"])
        self.assertFalse(result["data"]["publicEntity"]["canEditGroup"])

        self.assertCommonProperties(
            result["data"]["protectedEntity"],
            self.taskProtected,
        )
        self.assertProtectedAccessProperties(result["data"]["protectedEntity"])
        self.assertTrue(result["data"]["protectedEntity"]["canEdit"])
        self.assertTrue(result["data"]["protectedEntity"]["canArchiveAndDelete"])
        self.assertFalse(result["data"]["protectedEntity"]["canEditAdvanced"])
        self.assertFalse(result["data"]["protectedEntity"]["canEditGroup"])

    def test_task_admin(self):
        self.graphql_client.force_login(self.admin)
        result = self.post_query()

        self.assertCommonProperties(
            result["data"]["publicEntity"],
            self.taskPublic,
        )
        self.assertPublicAccessProperties(result["data"]["publicEntity"])
        self.assertTrue(result["data"]["publicEntity"]["canEdit"])
        self.assertTrue(result["data"]["publicEntity"]["canArchiveAndDelete"])
        self.assertTrue(result["data"]["publicEntity"]["canEditAdvanced"])
        self.assertFalse(result["data"]["publicEntity"]["canEditGroup"])

        self.assertIsNone(result["data"]["protectedEntity"])

    def test_task_super_admin(self):
        self.graphql_client.force_login(self.site_admin)
        result = self.post_query()

        self.assertCommonProperties(
            result["data"]["publicEntity"],
            self.taskPublic,
        )
        self.assertPublicAccessProperties(result["data"]["publicEntity"])
        self.assertTrue(result["data"]["publicEntity"]["canEdit"])
        self.assertTrue(result["data"]["publicEntity"]["canArchiveAndDelete"])
        self.assertTrue(result["data"]["publicEntity"]["canEditAdvanced"])
        self.assertTrue(result["data"]["publicEntity"]["canEditGroup"])

        self.assertCommonProperties(
            result["data"]["protectedEntity"], self.taskProtected
        )
        self.assertProtectedAccessProperties(result["data"]["protectedEntity"])
        self.assertTrue(result["data"]["protectedEntity"]["canEdit"])
        self.assertTrue(result["data"]["protectedEntity"]["canArchiveAndDelete"])
        self.assertTrue(result["data"]["protectedEntity"]["canEditAdvanced"])
        self.assertTrue(result["data"]["protectedEntity"]["canEditGroup"])

    def assertCommonProperties(self, entity, expected_entity, msg=None):
        self.assertEqual(entity["title"], expected_entity.title, msg=msg)
        self.assertEqual(
            entity["richDescription"], expected_entity.rich_description, msg=msg
        )
        self.assertEqual(
            entity["timeCreated"], expected_entity.created_at.isoformat(), msg=msg
        )
        self.assertEqual(
            entity["timePublished"], expected_entity.published.isoformat(), msg=msg
        )
        self.assertIsNone(entity["scheduleArchiveEntity"], msg=msg)
        self.assertIsNone(entity["scheduleDeleteEntity"], msg=msg)
        self.assertEqual(entity["url"], expected_entity.url, msg=msg)
        self.assertEqual(entity["state"], expected_entity.state, msg=msg)
        self.assertEqual(entity["inGroup"], True, msg=msg)
        self.assertEqual(entity["group"]["guid"], expected_entity.group.guid, msg=msg)

    def assertPublicAccessProperties(self, entity, msg=None):
        self.assertEqual(entity["accessId"], 2, msg=msg)
        self.assertEqual(entity["writeAccessId"], 0, msg=msg)

    def assertProtectedAccessProperties(self, entity, msg=None):
        self.assertEqual(entity["accessId"], 0, msg=msg)
        self.assertEqual(entity["writeAccessId"], 0, msg=msg)
