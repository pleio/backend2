from core.factories import GroupFactory
from core.tests.helpers.tags_testcase import Template
from entities.task.models import Task


class TestTaskTagsTestCase(Template.TagsTestCaseTemplate):
    graphql_label = "Task"
    model = Task
    subtype = "task"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._group = None

    def variables_add(self):
        if not self._group:
            self._group = GroupFactory(owner=self.owner)

        return {
            "input": {
                "title": "Todo",
                "subtype": "task",
                "containerGuid": self._group.guid,
            }
        }

    include_entity_search = True
