from lxml import etree

from core.tests.helpers import PleioTenantTestCase
from customer_migrations.tangelo_html.html_tags import (
    BElement,
    BRElement,
    DivElement,
    HtmlElement,
    IElement,
    IMGElement,
    LinkElement,
    PElement,
    SpanElement,
    TableElement,
    TableTBodyElement,
    TableTDElement,
    TableTHElement,
    TableTRElement,
    UnorderedListElement,
)


class TestHtmlElementTestCase(PleioTenantTestCase):
    def tree(self, html):
        return etree.fromstring(html)

    def test_html_table(self):
        tree = self.tree(
            "<table><tbody><tr><td>Hi</td><td>Bye</td></tr></tbody></table>"
        )

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, TableElement)
        self.assertEqual(
            str(node), "<table><tbody><tr><td>Hi</td><td>Bye</td></tr></tbody></table>"
        )

    def test_html_tbody(self):
        tree = self.tree("<tbody><tr></tr></tbody>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, TableTBodyElement)
        self.assertEqual(str(node), "<tbody><tr></tr></tbody>")

    def test_html_row(self):
        tree = self.tree("<tr><td>Hello world</td></tr>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, TableTRElement)
        self.assertEqual(str(node), "<tr><td>Hello world</td></tr>")

    def test_html_td(self):
        tree = self.tree("<td>Hello<br />World</td>")
        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, TableTDElement)
        self.assertEqual(str(node), "<td>Hello<br/>World</td>")

    def test_html_th(self):
        tree = self.tree("<th>Hello World</th>")
        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, TableTHElement)
        self.assertEqual(str(node), "<th>Hello World</th>")

    def test_html_paragraph(self):
        tree = self.tree("<p>Hello world</p>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, PElement)
        self.assertEqual(str(node), "<p>Hello world</p>")

    def test_empty_paragraph(self):
        tree = self.tree("<p/>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, PElement)
        self.assertEqual(str(node), "")

    def test_strong_tag(self):
        tree = self.tree("<b>Hello world</b>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, BElement)
        self.assertEqual(str(node), "<b>Hello world</b>")

    def test_linebreak(self):
        tree = self.tree("<br />")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, BRElement)
        self.assertEqual(str(node), "<br/>")

    def test_link(self):
        tree = self.tree('<a href="https://www.example.com">Hello world</a>')

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, LinkElement)
        self.assertEqual(str(node), '<a href="https://www.example.com">Hello world</a>')

    def test_div_element(self):
        tree = self.tree("<div><p>Hello world</p></div>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, DivElement)
        self.assertEqual(str(node), "<p>Hello world</p>")

    def test_i_element(self):
        tree = self.tree("<i class='lost'>Hello world</i>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, IElement)
        self.assertEqual(str(node), "<i>Hello world</i>")

    def test_span_element(self):
        tree = self.tree("<span class='lost'>Hello world</span>")

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, SpanElement)
        self.assertEqual(str(node), "<span>Hello world</span>")

    def test_unordered_list_element(self):
        tree = self.tree("<ul><li><a href='#top'>One</a></li><li>Two</li></ul>")
        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, UnorderedListElement)
        self.assertEqual(
            str(node), """<ul><li><a href="#top">One</a></li><li>Two</li></ul>"""
        )

    def test_img_element(self):
        tree = self.tree(
            '<img src="https://www.example.com/image.jpg" alt="Image" title="Image title"/>'
        )

        node = HtmlElement.from_tree(tree)

        self.assertIsInstance(node, IMGElement)
        self.assertEqual(str(node), "")
