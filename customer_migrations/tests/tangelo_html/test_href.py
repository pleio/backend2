import lxml.html

from core.tests.helpers import PleioTenantTestCase
from customer_migrations.tangelo_html.html_file import LinkElement


class TestHrefTestCase(PleioTenantTestCase):
    def get_href(self, url):
        html = "<a href='{}'>Content</a>".format(url)
        element = lxml.html.fromstring(html)

        return LinkElement(element).href

    def test_href_behavior(self):
        self.assertEqual(
            self.get_href("http://www.example.com"), "http://www.example.com"
        )
        self.assertEqual(
            self.get_href("https://www.example.com"), "https://www.example.com"
        )
        self.assertEqual(self.get_href("\\\\local\\share"), "\\\\local\\share")

        self.assertEqual(
            self.get_href("/internal/link"),
            "http://www.vwaintranet.nl/internal/link.html",
        )
        self.assertEqual(
            self.get_href("https://www.vwaintranet.nl/internal/link"),
            "http://www.vwaintranet.nl/internal/link.html",
        )
        self.assertEqual(
            self.get_href("http://www.vwaintranet.nl/internal/link"),
            "http://www.vwaintranet.nl/internal/link.html",
        )
        self.assertEqual(
            self.get_href("http://www.vwaintranet.nl/internal/link?extra=param"),
            "http://www.vwaintranet.nl/internal/link.html?extra=param",
        )
        self.assertEqual(
            self.get_href("/internal/link/?extra=param"),
            "http://www.vwaintranet.nl/internal/link.html?extra=param",
        )
        self.assertEqual(
            self.get_href("/internal/link/#location"),
            "http://www.vwaintranet.nl/internal/link.html#location",
        )
