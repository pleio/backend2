from core.tests.helpers import PleioTenantTestCase
from customer_migrations.tangelo_html.tiptap import fix_hardbreak_repetitions


class TestTiptapHardbreakElementsTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()

        self.element = {
            "type": "paragraph",
            "content": [
                {"type": "hardBreak"},
                {"type": "text", "text": "Alpha"},
                {"type": "hardBreak"},
                {"type": "hardBreak"},
                {"type": "text", "text": "beta"},
            ],
        }

    def test_remove_hardbreaks(self):
        result = fix_hardbreak_repetitions(self.element)
        self.assertEqual(
            result,
            {
                "type": "paragraph",
                "content": [
                    {"type": "text", "text": "Alpha"},
                    {"type": "hardBreak"},
                    {"type": "text", "text": "beta"},
                ],
            },
        )
