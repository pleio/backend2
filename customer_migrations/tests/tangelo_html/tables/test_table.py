from lxml import etree, html

from core.tests.helpers import PleioTenantTestCase
from customer_migrations.tangelo_html.html_tags import (
    TableWithContentElement,
    _ensure_row_size,
    _resolve_colspan,
)
from customer_migrations.tests.tangelo_html.tables.fixtures import (
    CONTENT_TABLE_HTML,
    DATA_TABLE_HTML,
    VERTICAL_TABLE_HTML,
)

"""
Test Two special cases of tables:
- TableWithDataElement: A table with data in the cells.
- TableWithContentElement: A table with content in the cells.
"""


class TestTableTestCase(PleioTenantTestCase):
    # Show the full diff when it fails.
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.data_tree = html.fromstring(DATA_TABLE_HTML)
        self.content_tree = html.fromstring(CONTENT_TABLE_HTML)
        self.vertical_tree = html.fromstring(VERTICAL_TABLE_HTML)


class TestContentTableTestCase(TestTableTestCase):
    def test_is_applicable(self):
        self.assertTrue(TableWithContentElement.is_applicable(self.content_tree))
        self.assertFalse(TableWithContentElement.is_applicable(self.data_tree))
        self.assertFalse(TableWithContentElement.is_applicable(self.vertical_tree))

    def test_conversion(self):
        element = TableWithContentElement(self.content_tree, set())
        self.assertEqual(
            str(element).strip(),
            """
<h3>Subject 1</h3>
First row, column 1
<h3>Subject 2</h3>
First row, column 2
<h3>Subject 3</h3>
First row, column 3
<h3>Subject 4</h3>
Fourth row, column 1
<h3>Subject 5</h3>
Fourth row, column 2
<h3>Subject 6</h3>
Fourth row, column 3
            """.strip(),
        )


class TestTableToolsTestCase(PleioTenantTestCase):
    def test_rowspan(self):
        tree = html.fromstring(
            """<tr><td>Cell</td><td colspan="2">Cell 2</td><td>Cell 3</td></tr>"""
        )
        cells = [
            etree.tostring(cell).decode() if cell is not None else cell
            for cell in _resolve_colspan(tree.xpath(".//td"))
        ]
        self.assertEqual(
            cells,
            [
                "<td>Cell</td>",
                '<td colspan="2">Cell 2</td>',
                None,
                "<td>Cell 3</td>",
            ],
        )

    def test_row_size(self):
        tree = html.fromstring("""<tr><td>Cell</td><td>Cell 2</td></tr>""")
        cells = [
            etree.tostring(cell).decode() if cell is not None else cell
            for cell in _ensure_row_size(tree.xpath(".//td"), 3)
        ]
        self.assertEqual(
            cells,
            [
                "<td>Cell</td>",
                "<td>Cell 2</td>",
                None,
            ],
        )
