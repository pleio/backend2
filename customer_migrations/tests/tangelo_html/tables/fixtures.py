DATA_TABLE_HTML = """
<table>
    <tbody>
        <tr>
            <th>Header 1</th>
            <th>Header 2</th>
            <th>Header 3</th>
        </tr>
        <tr>
            <td>First row, column 1</td>
            <td>First row, column 2</td>
            <td>First row, column 3</td>
        </tr>
        <tr>
            <td>Second row, column 1</td>
            <td>Second row, column 2</td>
            <td>Second row, column 3</td>
        </tr>
    </tbody>
</table>
"""

CONTENT_TABLE_HTML = """
<table>
    <tbody>
        <tr>
            <th>Subject 1</th>
            <th>Subject 2</th>
            <th>Subject 3</th>
        </tr>
        <tr>
            <td>First row, column 1</td>
            <td>First row, column 2</td>
            <td>First row, column 3</td>
        </tr>
        <tr>
            <th>Subject 4</th>
            <th>Subject 5</th>
            <th>Subject 6</th>
        </tr>
        <tr>
            <td>Fourth row, column 1</td>
            <td>Fourth row, column 2</td>
            <td>Fourth row, column 3</td>
        </tr>
    </tbody>
</table>
"""

VERTICAL_TABLE_HTML = """
<table>
    <tbody>
        <tr>
            <th>Subject 1</th>
            <td>Item 1</td>
        </tr>
        <tr>
            <th>Subject 2</th>
            <td>Item 2</td>
        </tr>
        <tr>
            <th>Subject 3</th>
            <td>Item 3</td>
        </tr>
        <tr>
            <th>Subject 4</th>
            <td>Item 4</td>
        </tr>
    </tbody>
</table>
"""
