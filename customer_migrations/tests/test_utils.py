from core.tests.helpers import PleioTenantTestCase
from customer_migrations.utils import first_admin_account
from user.factories import AdminFactory, UserFactory


class TestFirstAdminAccountTestCase(PleioTenantTestCase):
    def test_first_admin_account_without_admins(self):
        with self.assertRaises(AssertionError):
            first_admin_account()

    def test_first_admin_account_with_owner(self):
        try:
            AdminFactory(email="admin@example.com")
            owner = UserFactory(email="test@example.com")
            self.tenant.administrative_details["site_owner_email"] = owner.email
            self.tenant.save()

            account = first_admin_account()

            self.assertEqual(account, owner.guid)
        finally:
            self.tenant.administrative_details = {}
            self.tenant.save()

    def test_first_admin_account_with_admin(self):
        UserFactory()
        admin = AdminFactory(email="admin@example.com")

        account = first_admin_account()

        self.assertEqual(account, admin.guid)
