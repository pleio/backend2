from django.apps import AppConfig


class CustomerMigrationsConfig(AppConfig):
    name = "customer_migrations"
