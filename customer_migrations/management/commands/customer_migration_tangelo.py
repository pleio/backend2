from django.core.management import BaseCommand

from customer_migrations.tangelo_html.archive import TangeloArchive
from entities.file.models import FileFolder


class Command(BaseCommand):
    help = """
    This command is used to import customer data from Tangelo to pleio.
    """

    def add_arguments(self, parser):
        parser.add_argument("--file-id", type=str, required=False)
        parser.add_argument("--cleanup", action="store_true", required=False)
        parser.add_argument("--one", required=False)
        parser.add_argument("--type", default="wiki", required=False)

    def handle(self, *args, file_id=None, type=None, cleanup=None, one=None, **options):
        assert type in ["wiki", "page", "widget"]

        if cleanup:
            TangeloArchive.cleanup()

        if file_id:
            file = FileFolder.objects.get(id=file_id)
            importer = TangeloArchive(file, type)
            importer.apply(one)
