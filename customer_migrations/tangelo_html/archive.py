from collections import defaultdict
from zipfile import ZipFile

from customer_migrations.tangelo_html.html_file import HtmlFile
from customer_migrations.tangelo_html.page_types import (
    TextPageType,
    WidgetPageType,
    WikiType,
)
from customer_migrations.tangelo_html.shared import wiki_get_imported_items

"""
Hoe werkt de inhoud van deze zipfile?

Elke zipfile met een context menu heeft een parent link. Elke parent link zou naar een resource in de zip moeten gaan.
  Als dit voldoet kan elke pagina worden geïmporteerd en gekoppeld aan een parent-pagina.
  Een 50tal pagina's heeft geen noemenswaardige inhoud. Deze hebben geen parent. Deze zijn te herkennen aan de md5 checksum.
  De overige pagina's hebben allemaal een parent die te herleiden is tot een bron in de zipfile.
  Slechts 1 pagina's heeft geen parent: home.html. Het hoeft geen uitleg dat dit geen probleem is voor het importeren.
De zipfile bevat verschillende links.
  - Externe links. Deze kan ik gewoon overnemen.
     - Links naar andere websites
     - Mailto links
     - Links naar netwerkbronnen
     - Links die als placeholder dienst doen
  - Interne links. Deze moeten geconverteerd worden na de import.
     - Links naar andere paginas. Niet elke pagina waar naar gelinkt wordt in de tekst bestaat in de zipfile.
     - Links naar Apex bestanden. Meer info volgt.
     - Links naar Apex zoekopdrachten. Meer info volgt.
     - Links naar Apex verzamelpagina's (2 stuks). Meer info volgt.
     - Links naar een ander soort bestand. Meer info volgt.
Html layout
  De layout wordt uitgedrukt in tabellen. Dat zal worden omgezet naar de smallere Pleio layout.

Het importeren gaat via een tweetrapsraket.
1) In ronde 1 wordt van elke resource een wiki pagina gemaakt. In een aparte tabel van deze module wordt het ID van
de wiki pagina gelinkt met de originele url van de resource.
2) In ronde 2 worden
  - De parents worden ingevuld.
  - De originele interne links omgezet naar Pleio-interne links.
  - De rich_description wordt in dit proces overschreven.
"""


class CustomLogger:
    def __init__(self):
        self.bin = defaultdict(list)

    def add(self, tag, *args):
        self.bin[tag].append(args)

    def __str__(self):
        result = []
        for tag, items in self.bin.items():
            result.append("\n---\n%s\n---" % tag)
            for item in items:
                for subitem in item:
                    result.append(subitem)
        return "\n".join(result)


class TangeloArchive:
    """
    Unpack the Tangelo zip file to import the HTML files that are inside.
    """

    def __init__(self, file, type=None):
        self.file = file
        self.type = type

        self.zipped_files = set()
        self.internal_links = set()
        self.checksums = {}
        self.reverse_checksums = defaultdict(list)
        self.references = set()
        self.real_page_locations = set()
        self.apex_links = set()
        self.images = set()

        self.parent_links = set()
        self.menu_links = set()
        self.links_without_a_page = set()
        self.parents_without_a_page = set()
        self.blacklist = set()
        self.root_url = None
        self.logger = CustomLogger()

    @classmethod
    def cleanup(cls):
        for item in wiki_get_imported_items():
            item.delete()

    def apply(self, one=None):
        # Collect information about the zipfile.
        self.describe_resource_recursive()

        # Validate if the zipfile is complete.
        self.assert_all_parents_have_a_page()
        # self.assert_all_links_have_a_page()

        # Run once. The management command must be run
        # twice to convert the internal links.
        self.import_resource_recursive(one)

        print(self.logger)

    def import_resource_recursive(self, one=None):
        with ZipFile(self.file.upload.open()) as fh:
            for name in self.zipped_files:
                with fh.open(name) as htmlfile:
                    if one and name != one:
                        continue
                    self.import_resource(name, fh=htmlfile)

    def describe_resource_recursive(self):
        with ZipFile(self.file.upload.open()) as fh:
            for name in fh.namelist():
                with fh.open(name) as htmlfile:
                    self.describe_resource(name, htmlfile)

        self.blacklist = set()
        for similar_pages in self.reverse_checksums.values():
            if len(similar_pages) > 5:
                self.blacklist.update(similar_pages)

    def describe_resource(self, name, fh):
        try:
            assert self.is_valid_filename(name)
            file = HtmlFile(name, fh)
            file.assert_content_file()

            self.zipped_files.add(name)
            original_id = file.get_url()

            self.checksums[str(original_id)] = file.checksum()
            self.reverse_checksums[file.checksum()].append(original_id)
            self.references.update(list(file.get_references()))
            self.real_page_locations.add(original_id)
            self.apex_links.update(list(file.get_apex_links()))
            self.images.update(file.get_images())
            if not file.get_parent_url():
                self.root_url = file.get_url()

            if parent_link := file.get_parent_url():
                self.parent_links.add(parent_link)
        except AssertionError:
            pass
        except Exception:
            print("Error when processing %s" % name)
            raise

    def is_valid_filename(self, name):
        if not name.endswith(".html"):
            return False

        *left, right = name.split("/")
        if right.startswith("."):
            return False
        return True

    def import_resource(self, name, fh):
        file = HtmlFile(name, fh)
        if file.get_url() in self.blacklist:
            return

        if self.type == "page":
            factory = TextPageType()
        elif self.type == "wiki":
            factory = WikiType()
        else:
            factory = WidgetPageType()

        file.create_pleio_wiki(
            root_url=self.root_url, factory=factory, logger=self.logger
        )

    def assert_all_parents_have_a_page(self):
        self.parents_without_a_page = set()
        for item in self.parent_links:
            if item in self.real_page_locations:
                continue
            self.parents_without_a_page.add(item)

        if self.parents_without_a_page:
            print("\nAll links.")
            for link in sorted(self.real_page_locations):
                print(link)
            print("\nMissing parents.")
            for link in sorted(self.parents_without_a_page):
                print(link)
            msg = "Not all parent links go to a page"
            raise AssertionError(msg)

    def assert_all_links_have_a_page(self):
        self.links_without_a_page = set()
        for item in self.references:
            if item in self.real_page_locations:
                continue
            self.links_without_a_page.add(item)

        if self.links_without_a_page:
            print("\nDeze pagina's zitten niet in de zipfile:")
            for link in sorted(self.links_without_a_page):
                print(link)
            print("\n")
            # assert False, "Not all links go to a page"
