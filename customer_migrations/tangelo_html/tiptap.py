def decorate(subject, decorators):
    for decorator in decorators:
        subject = decorator(subject)
    return subject


def fix_empty_paragraphs(tree):
    """
    Remove extra whitespace on the page due to empty paragraph elements.
    """

    def exclude_empty_paragraphs(content):
        for subtree in content:
            if subtree.get("type") != "paragraph":
                yield subtree
            elif subtree.get("content") and len(subtree["content"]) > 0:
                yield subtree

    # Loop the content in the tree.
    if isinstance(tree, dict) and "content" in tree:
        return {
            **tree,
            "content": [
                fix_empty_paragraphs(subtree)
                for subtree in exclude_empty_paragraphs(tree["content"])
            ],
        }

    elif isinstance(tree, list):
        return [fix_empty_paragraphs(subtree) for subtree in tree]
    else:
        return tree


def fix_hardbreak_repetitions(tree):
    """
    Remove whitespace due to repeated hardBreak elements.
    """
    if isinstance(tree, dict) and "content" in tree:

        def exclude_repeated_hardbreaks(content):
            last_element = None
            for subtree in content:
                if subtree == {"type": "hardBreak"}:
                    if (
                        last_element != {"type": "hardBreak"}
                        and last_element is not None
                    ):
                        yield subtree
                else:
                    yield subtree
                last_element = subtree

        return {
            **tree,
            "content": [
                fix_hardbreak_repetitions(subtree)
                for subtree in exclude_repeated_hardbreaks(tree["content"])
            ],
        }

    elif isinstance(tree, list):
        return [fix_hardbreak_repetitions(subtree) for subtree in tree]
    else:
        return tree
