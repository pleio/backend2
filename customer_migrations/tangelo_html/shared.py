import re
from urllib.parse import urljoin, urlparse

from django.core.exceptions import ObjectDoesNotExist

from core.models import Entity
from customer_migrations.models import MigrationConversion
from customer_migrations.tangelo_html import CONVERSION_TAG

LOCAL_URL = "http://www.vwaintranet.nl"


def normalize_text(text):
    return re.sub(r"\s+", " ", text).strip()


def wiki_at_id(original_id):
    try:
        instance = MigrationConversion.objects.get(
            tag=CONVERSION_TAG, original_id=original_id
        )
        return Entity.objects.select_subclasses().get(id=instance.local_id)
    except ObjectDoesNotExist:
        return None


def write_wiki_at_id(original_id, local_id):
    mc, created = MigrationConversion.objects.get_or_create(
        tag=CONVERSION_TAG, original_id=original_id
    )
    mc.local_id = local_id
    mc.save()


def wiki_get_imported_items():
    for mc in MigrationConversion.objects.filter(tag=CONVERSION_TAG):
        try:
            yield Entity.objects.select_subclasses().get(id=mc.local_id)
        except ObjectDoesNotExist:
            pass


def maybe_local_url(href):
    try:
        link = href.strip()
        if link.startswith("/"):
            link = urljoin(LOCAL_URL, link)
        if link.startswith("https://www.vwaintranet.nl/"):
            link = "http://" + link[8:]

        if link.startswith(LOCAL_URL):
            p = urlparse(link)

            link = p.path.strip("/")
            if not link.endswith(".html"):
                link += ".html"

            return "{scheme}://{netloc}/{path}{query}{fragment}".format(
                scheme=p.scheme,
                netloc=p.netloc,
                path=link,
                query=("?%s" % p.query) if p.query else "",
                fragment=("#%s" % p.fragment) if p.fragment else "",
            )
        return link
    except AttributeError:
        return ""
