import json
import uuid

from customer_migrations.tangelo_html.shared import wiki_at_id
from entities.cms.models import Page
from entities.wiki.models import Wiki


class BaseType:
    def create(self, **kwargs):
        raise NotImplementedError()

    def get(self, tangelo_id):
        return wiki_at_id(tangelo_id)

    def update(self, entity, **kwargs):
        for key, value in kwargs.items():
            setattr(entity, key, value)
        return entity


class WikiType(BaseType):
    def create(self, **kwargs):
        return Wiki(**kwargs)


class TextPageType(BaseType):
    def create(self, **kwargs):
        return Page(page_type="text", **kwargs)


class WidgetPageType(BaseType):
    def create(self, **kwargs):
        return Page(page_type="campagne", **kwargs)

    def update(self, entity, **kwargs):
        persist_description = entity.rich_description
        super().update(entity, **kwargs)
        entity.row_repository = [
            {
                "isFullWidth": False,
                "backgroundColor": "white",
                "columns": [
                    {
                        "width": [12],
                        "widgets": [
                            self._title_widget(entity.title),
                            self._body_widget(entity.rich_description),
                        ],
                    }
                ],
            }
        ]
        entity.rich_description = persist_description
        return entity

    @staticmethod
    def _title_widget(title):
        return {
            "guid": str(uuid.uuid4()),
            "type": "text",
            "settings": [
                {
                    "key": "richDescription",
                    "richDescription": json.dumps(
                        {
                            "type": "doc",
                            "content": [
                                {
                                    "type": "heading",
                                    "attrs": {"level": 1},
                                    "content": [{"type": "text", "text": title}],
                                }
                            ],
                        }
                    ),
                }
            ],
        }

    @staticmethod
    def _body_widget(body):
        return {
            "guid": str(uuid.uuid4()),
            "type": "text",
            "settings": [
                {
                    "key": "richDescription",
                    "richDescription": body,
                }
            ],
        }
