import hashlib
import json
import re
from urllib.parse import urljoin

from django.utils import timezone
from lxml import etree, html

from core.constances import ACCESS_TYPE
from core.utils.convert import html_to_tiptap
from customer_migrations.tangelo_html.html_tags import HtmlElement, LinkElement
from customer_migrations.tangelo_html.shared import (
    LOCAL_URL,
    maybe_local_url,
    wiki_at_id,
    write_wiki_at_id,
)
from customer_migrations.tangelo_html.tiptap import (
    decorate,
    fix_empty_paragraphs,
    fix_hardbreak_repetitions,
)
from customer_migrations.utils import first_admin_account


class HtmlFile:
    def __init__(self, name, fh):
        self.name = name
        self.fh = fh
        self.root = html.fromstring(re.sub(r"\s+", " ", (fh.read()).decode()))

    def get_url(self):
        # vanaf "/xmlpages" overnemen.
        xmlpages = self.name.split("/xmlpages/")[1]
        return maybe_local_url(urljoin(LOCAL_URL, "xmlpages/%s" % xmlpages))

    def assert_content_file(self):
        assert self._content() is not None, (
            f"File {self.name} does not contain a content div"
        )

    def get_title(self):
        try:
            content = self._content()
            assert content is not None
            for element in content.xpath(".//h1"):
                return str(element.text).strip()
        except AssertionError:
            pass
        parts = self.name.strip("/").split("/")
        return parts[-1]

    def cleanup_content(self, tree):
        for element in tree.xpath(".//p[i]"):
            for italic_element in element.xpath(".//i"):
                if italic_element.text.strip().startswith("Laatst gewijzigd:"):
                    element.getparent().remove(element)
                    break
        return tree

    def _content(self):
        for element in self.root.xpath("//div[contains(@class, 'content-standaard')]"):
            return element

    def _subnavigation(self):
        for element in self.root.xpath("//div[@id='c-subn']"):
            return element

    def get_content_xml(self):
        content = self._content()
        return etree.tostring(content, pretty_print=True).decode()

    def get_references(self):
        for element in self._content().xpath(".//a"):
            wrapper = LinkElement(element)
            if wrapper.is_internal_link() and not wrapper.is_apex_link():
                yield wrapper.href

    def get_apex_links(self):
        for element in self._content().xpath(".//a"):
            wrapper = LinkElement(element)
            if wrapper.is_internal_link() and wrapper.is_apex_link():
                yield wrapper.href

    def get_images(self):
        for element in self._content().xpath(".//img"):
            yield maybe_local_url(element.get("src"))

    def get_parent_url(self):
        subnavigation = self._subnavigation()
        if subnavigation is not None:
            for element in subnavigation.xpath(".//h1/a"):
                link_element = LinkElement(element)
                return link_element.href

    def get_menu_links(self):
        subnavigation = self._subnavigation()
        if subnavigation is not None:
            for link_element in subnavigation.xpath(".//ul/li/a"):
                yield LinkElement(link_element)

    def checksum(self):
        return md5(self.get_content_xml())

    def get_html(self, logger=None):
        try:
            clean_content = self.cleanup_content(self._content())
            element = HtmlElement.from_tree(clean_content, logger)
            return str(element)
        except ValueError:
            pass
        return ""

    def get_tiptap(self, logger=None):
        html = self.get_html(logger)

        return decorate(
            html,
            [
                html_to_tiptap,
                json.loads,
                fix_hardbreak_repetitions,
                fix_empty_paragraphs,
                json.dumps,
            ],
        )

    def create_pleio_wiki(self, root_url, factory, logger):
        wiki = factory.get(self.get_url())
        if not wiki:
            wiki = factory.create(
                owner_id=first_admin_account(),
                published=timezone.now(),
            )

        before = wiki.serialize()

        element_types = set()

        factory.update(
            wiki,
            title=self.get_title(),
            rich_description=self.get_tiptap(element_types).strip(),
            parent=None,
            is_archived=False,
            read_access=[ACCESS_TYPE.logged_in],
        )

        parent_url = self.get_parent_url()
        if parent_url and (parent_url == self.get_url()):
            parent_url = root_url

        if parent_url and parent_url != self.get_url():
            parent_wiki = wiki_at_id(parent_url)
            if parent_wiki:
                wiki.parent = parent_wiki

        if before != wiki.serialize():
            wiki.save()
            write_wiki_at_id(self.get_url(), wiki.guid)


def md5(content):
    return hashlib.md5(content.encode()).hexdigest()
