from django.utils.html import escape, strip_tags
from lxml import etree

from core.lib import maybe_prop
from customer_migrations.tangelo_html.shared import (
    LOCAL_URL,
    maybe_local_url,
    wiki_at_id,
)


class HtmlElement:
    expected_tag = None
    xml_style = True

    def __init__(self, tree, logger=None):
        self.tree = tree
        self.logger = logger
        if isinstance(logger, set):
            self.logger.add(self.__class__.__name__)

    @staticmethod
    def from_tree(tree, logger=None):
        types = (
            BElement,
            BRElement,
            DivElement,
            NonPrintableElement,
            IElement,
            IMGElement,
            LinkElement,
            ListItemElement,
            PElement,
            SpanElement,
            TableTBodyElement,
            TableTDElement,
            TableTHElement,
            TableTRElement,
            UnorderedListElement,
            # @WARNING: Below this line the sort order matters
            TableWithAColspanPreludeElement,
            TableWithOneCellElement,
            TableWithContentElement,
            TableElement,
        )
        for type in types:
            if type.is_applicable(tree):
                return type(tree, logger)
        raise ValueError(
            "No applicable html type found for tree: %s\n%s"
            % (tree.tag, etree.tostring(tree).decode().strip())
        )

    def _raw_children(self):
        if self.tree.text:
            yield PlainText(self.tree.text)
        for child in self.tree.getchildren():
            yield HtmlElement.from_tree(child, self.logger)
            if child.tail:
                yield PlainText(child.tail)

    def children(self):
        for child in self._raw_children():
            if not isinstance(child, PlainText) or str(child).strip() != "":
                yield child

    def tag(self):
        return self.tree.tag

    def inner_text(self):
        children = [*self.children()]
        if children:
            return "".join(str(child) for child in children)
        if self.tree.text:
            return str(self.tree.text)
        return ""

    def properties(self):
        return {}

    def format_properties(self):
        properties = " ".join(
            f'{k}="{escape(v)}"' for k, v in self.properties().items()
        )
        if properties:
            return " " + properties
        return ""

    def __str__(self):
        text = self.inner_text()
        properties = self.format_properties()
        if text or not self.xml_style:
            return "<%s%s>%s</%s>" % (self.tag(), properties, text, self.tag())
        return "<%s%s/>" % (self.tag(), properties)

    @classmethod
    def is_applicable(cls, tree):
        return cls.expected_tag == tree.tag


class NonPrintableElement(HtmlElement):
    @classmethod
    def is_applicable(cls, tree):
        return tree.tag in ("h1", "form")

    def __str__(self):
        """
        Don't print the h1 element
        """
        return ""


class UnorderedListElement(HtmlElement):
    expected_tag = "ul"


class ListItemElement(HtmlElement):
    expected_tag = "li"


class IElement(HtmlElement):
    expected_tag = "i"

    @classmethod
    def is_applicable(cls, tree):
        return tree.tag in ("i", "em")

    def tag(self):
        return "i"


class IMGElement(HtmlElement):
    expected_tag = "img"

    def __str__(self):
        return ""


class SpanElement(HtmlElement):
    expected_tag = "span"


class DivElement(HtmlElement):
    expected_tag = "div"

    def __str__(self):
        return "".join(self.inner_text())


class PElement(HtmlElement):
    xml_style = False
    expected_tag = "p"

    def __str__(self):
        inner_text = "".join(self.inner_text())
        for child in self.tree.getchildren():
            if child.tag == "p":
                return inner_text
            break
        if inner_text:
            return "<p>%s</p>" % inner_text
        return ""


class ElementReport:
    def __init__(self, tree):
        self.tree = tree

    def all_p_have_b(self):
        has_p = False
        for element in self.tree.xpath(".//p"):
            has_p = True
            if not element.xpath(".//b"):
                return False
        return has_p

    def get_table_cell_type(self):
        try:
            assert self.tree.tag in ("td", "th")
            if self.tree.tag == "th":
                return "H"
            if self.all_p_have_b():
                return "H"
            return "D"
        except Exception:
            pass
        return "*"


class TableTypeMixin(HtmlElement):
    def __init__(self, tree, logger):
        self.insert_tbody(tree)
        super().__init__(tree, logger)

    @staticmethod
    def insert_tbody(tree):
        if not tree.xpath(".//tbody"):
            tbody = etree.Element("tbody")
            for child in tree.getchildren():
                tree.remove(child)
                tbody.append(child)
            tree.append(tbody)
        pass


class TableWithAColspanPreludeElement(TableTypeMixin, HtmlElement):
    @staticmethod
    def _get_header_cells(tree):
        rows = [*tree.xpath(".//tr")]
        if len(rows) < 3:
            return
        *others, last = rows
        cells = [*others[0].xpath(".//th|td")]
        if len(cells) > 1:
            return
        first_cells = [*others[0].xpath(".//td|th")]
        last_cells = [*last.xpath(".//td|th")]
        if len(first_cells) > 1 or len(last_cells) == len(first_cells):
            return

        for row in rows:
            cells = [*row.xpath(".//td|th")]
            if len(cells) > 1:
                return
            yield row

    @classmethod
    def is_applicable(cls, tree):
        if tree.tag != "table":
            return False

        header_cells = [*cls._get_header_cells(tree)]
        return len(header_cells) > 0

    def __str__(self):
        result = ""
        table_rows = []
        for row in self.tree.xpath(".//tr"):
            cells = [*row.xpath(".//td|th")]
            if len(cells) > 1:
                table_rows.append(row)
                continue

            element = HtmlElement.from_tree(cells[0], self.logger)
            result += element.inner_text()

        result += "<table><tbody>%s</tbody></table>" % (
            "".join(
                [str(HtmlElement.from_tree(row, self.logger)) for row in table_rows]
            )
        )

        return result


class TableWithOneCellElement(TableTypeMixin, HtmlElement):
    @classmethod
    def is_applicable(cls, tree):
        # Test of de tabel paren heeft van 1 header row (th's), gevolgd door 1 data row (td's).
        if tree.tag != "table":
            return False

        cells = [*tree.xpath(".//td|th")]
        if len(cells) > 1:
            return False
        return True

    def __str__(self):
        for cell in self.tree.xpath(".//td|th"):
            element = HtmlElement.from_tree(cell, self.logger)
            return str(element.inner_text())
        return ""


class TableWithContentElement(TableTypeMixin, HtmlElement):
    @classmethod
    def is_applicable(cls, tree):
        # Test of de tabel paren heeft van 1 header row (th's), gevolgd door 1 data row (td's).
        if tree.tag != "table":
            return False
        if _table_width(tree) > 3:
            return False
        row_summary = _row_summary(tree)
        if len(row_summary) == 0:
            return False
        try:
            for h, d in _group_seconds(row_summary):
                if h != "H" or d != "D":
                    return False
        except AssertionError:
            return False

        return True

    def _children_as_string(self, element):
        try:
            if children := [*element.getchildren()]:
                return "".join(
                    str(HtmlElement.from_tree(child, self.logger)) for child in children
                )
            else:
                return element.text
        except Exception:
            return "-"

    def _childrens_text_at_index(self, elements, index):
        try:
            return self._children_as_string(elements[index])
        except IndexError:
            return "-"

    def __str__(self):
        result = ""
        for first, second in _group_seconds(self.tree.xpath(".//tr")):
            headers = first.xpath(".//th|td")
            data = second.xpath(".//th|td")
            for i, header in enumerate(headers):
                result += "<h3>%s</h3>\n" % strip_tags(self._children_as_string(header))
                result += "%s\n" % self._childrens_text_at_index(data, i)

        return result


class TableElement(TableTypeMixin, HtmlElement):
    xml_style = False
    expected_tag = "table"


class TableTBodyElement(HtmlElement):
    xml_style = False
    expected_tag = "tbody"


class TableTHTDElementBase(HtmlElement):
    def properties(self):
        return {
            **maybe_prop(self.tree, "colspan"),
            **maybe_prop(self.tree, "rowspan"),
        }

    def __str__(self):
        try:
            return super().__str__()
        finally:
            if self.tree.get("rowspan") and self.tree.get("colspan"):
                self.logger.add("BothRowspanAndColspan")
            elif self.tree.get("colspan"):
                self.logger.add("HasColspan")
            elif self.tree.get("rowspan"):
                self.logger.add("HasRowspan")


class TableTRElement(HtmlElement):
    xml_style = False
    expected_tag = "tr"


class TableTDElement(TableTHTDElementBase, HtmlElement):
    xml_style = False
    expected_tag = "td"


class TableTHElement(TableTHTDElementBase, HtmlElement):
    xml_style = False
    expected_tag = "th"


class BElement(HtmlElement):
    xml_style = False
    expected_tag = "b"

    @classmethod
    def is_applicable(cls, tree):
        return tree.tag in ("b", "strong")

    def tag(self):
        return "b"


class BRElement(HtmlElement):
    expected_tag = "br"


class LinkElement(HtmlElement):
    expected_tag = "a"

    def properties(self):
        href = self.href
        if not href:
            return {}

        if not self.is_apex_link() and self.is_internal_link():
            wiki = wiki_at_id(href)
            if wiki:
                return {
                    "href": wiki.url,
                }
        # TODO: convert apex links.
        return {
            "href": href,
        }

    @property
    def href(self):
        return maybe_local_url(self.tree.get("href"))

    def is_internal_link(self):
        return self.href.startswith(LOCAL_URL)

    def is_apex_link(self):
        if "/xmlpages/page/zoeken-kwaliteit-systeem" in self.href:
            return True
        return "/xmlpages/tan/" in self.href


class PlainText(HtmlElement):
    def __init__(self, text):
        super().__init__(None)
        self.text = text

    def __str__(self):
        return self.text


def xprint(tree):
    print(etree.tostring(tree, pretty_print=True).decode().strip())


def _row_summary(tree):
    row_summary = []
    for row in tree.xpath(".//tr"):
        cells = [
            ElementReport(cell).get_table_cell_type() for cell in row.xpath(".//td|th")
        ]
        if "H" in cells and "D" not in cells:
            row_summary.append("H")
        else:
            row_summary.append("D")
    return row_summary


def _table_width(tree):
    rows = [*tree.xpath(".//tr")]
    *others, last = rows
    cells = [*last.xpath(".//td|th")]
    return len(cells)


def _resolve_colspan(cells):
    for cell in cells:
        yield cell
        span = cell.get("colspan")
        if span:
            span = int(span)
            while span > 1:
                yield None
                span -= 1


def _ensure_row_size(cells, size):
    for cell in cells:
        yield cell
    for _ in range(size - len(cells)):
        yield None


def _group_seconds(array):
    assert len(array) % 2 == 0
    for first, second in [array[i : i + 2] for i in range(0, len(array), 2)]:
        yield first, second
