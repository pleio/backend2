from core.constances import USER_ROLES
from tenants.helpers import current_tenant
from user.models import User


def first_admin_account():
    details = current_tenant().administrative_details
    user = User.objects.filter(email=details.get("site_owner_email") or "").first()

    if not user:
        user = (
            User.objects.filter(roles__contains=[USER_ROLES.ADMIN])
            .order_by("created_at")
            .first()
        )

    assert user, "No admin user found"
    return user.guid
