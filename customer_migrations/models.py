from django.db import models


class MigrationConversion(models.Model):
    """ " """

    tag = models.CharField(max_length=512, null=False)
    original_id = models.CharField(max_length=512, null=True, blank=True)
    local_id = models.UUIDField(null=True, blank=True)
