---
sidebar_position: 1
---

## Project overview

This repository contains the work in progress of Backend2, a brand new social engine based on [Django](https://www.djangoproject.com/) and [GraphQL](http://graphql.org/). The backend will initially be used by [Pleio](https://www.pleio.nl). The goal of this backend is to be:

- Generic
- Scalable
- Accessible
- Multi-lingual
- Modular
- Extensible

#### Features

- Object versioning, including archival requirements (in progress)
- Access control (read/write permissions, groups support)
- Full-text search
- OpenID connect support
- Logging (audit trail) (in progress)
- Notifications
- Handling large file uploads

## Requirements

- [docker](https://docs.docker.com/desktop/)
- Local DNS for tenants (for example [Dnsmasq](./Getting%20started#dnsmasq) for mac)
