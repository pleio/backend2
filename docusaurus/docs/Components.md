---
sidebar_position: 2
---

Backend2 consists of multiple services this page explains what they do.

## API

This is the main service that handles all the site requests and GraphQL api calls.

Things to know:

* The tenant middleware selects a tenant on hostname, each tenant has its own schema in the database.

## Control

This is service with its own url routing and models (in the public database schema) that runs the admin interface.

Things to know:

* The admin service runs the database migrations on startup

## Background

This is a celery service that runs background tasks.

Things to know:

* Production/test also run a specific `background-control` instance of this service. This instance only handles control tasks, this is done so that long running control tasks don't interference with normal site tasks.


## Subscriptions-api

This is a websocket service specific for async GraphQL subscriptions, it starts the backend2/websockets.py app under the `/subscriptions` path.

## External: frontend

Frontend is separately developed in this repository: https://gitlab.com/pleio/frontend

Things to know:

* In production/test the build assets for frontend are copied to /static-frontend and included in collectstatic on startup.
* In local development you can run the frontend development server on port 9001 and backend will load assets from that port.

## External: collab

Collab is a service for collaborating with multiple users in a "Pad".

Collab is separately developed in this repository: https://gitlab.com/pleio/collab-backend

## External: concierge (account)

Concierge is the authentication service for Pleio sites.

Concierge is separately developed in this repository: https://gitlab.com/pleio/concierge

## Pleio-dev-stack

Although you can run backend2 standalone we have also created `pleio-dev-stack` to integrate external services together in 1 docker project for easy development. You can find this repository here: https://gitlab.com/pleio/pleio-dev-stack
