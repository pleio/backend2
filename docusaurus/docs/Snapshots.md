---
sidebar_position: 6
---

## Snapshots

We use [restic](https://restic.readthedocs.io/en/stable/) for making site snapshots.


## Initialize restic repository

To start using snapshots (locally) you have to initialize the restic repository.

To initialize that directory you can execute the following command in the python shell:

```
import restic

restic.init()
```

For development we use the `./media/restic` folder on your filesystem as a repository.


## Other restic endpoints

You can configure other Restic endpoints using environment variables. Read the [restic docs](https://restic.readthedocs.io/en/stable/) for more information.
