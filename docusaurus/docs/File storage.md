---
sidebar_position: 5
---

## Working with files

When working with files in backend2 make sure you access the files using the Django [File storage API](https://docs.djangoproject.com/en/4.2/ref/files/storage/).

Exception may be temporary files or other non customer media (system) files.

## File storage backends

The file storage backend can be set using the environment variable `DEFAULT_STORAGE`.

This variable defaults to `file_storage`. Currently we have the options `file_storage` and `object_storage`.

Environment variables can be set in your local `.env` file.

### file_storage

This options stores the files on the file system.

The base path can be set using the environment variable `MEDIA_ROOT`. This defaults to the `media/` folder relative to the root.

### object_storage

This option stores the files in a s3 compatible object storage.

For this option the following environment settings should be set:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_S3_ENDPOINT_URL`  (optional if you want to use something different than 'minio')
- `AWS_STORAGE_BUCKET_NAME` (optional if you want to use something different than 'dev')
