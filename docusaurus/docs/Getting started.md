---
sidebar_position: 3
---

How to get started with your development environment.

## pleio-dev-stack

Are you looking for a complete development setup?

Check out [pleio-dev-stack](https://gitlab.com/pleio/pleio-dev-stack),
which combines the backend with
[frontend](https://gitlab.com/pleio/frontend) and
[collab](https://gitlab.com/pleio/collab-backend).

## Install the git pre-commit hook

```bash
# Globally on the host system
$ pip install pre-commit

# At the backend git-root folder.
$ pre-commit install
```

## Get it running

Copy `.env-example` to `.env` and update the OIDC endpoint credentials you got for account.pleio-test.nl.

Make sure [Docker](https://www.docker.com/) is installed. Then run the following commands within the repository:

```bash
docker compose pull
docker compose up
```

If you started your development environment for the first time execute the following commands:

### Create admin tenant

```bash
docker compose exec admin /app/manage.py create_tenant --noinput --schema_name=public --name=public --domain-domain=localhost
```

### Create superuser for admin

```bash
docker compose exec admin /app/manage.py createsuperuser
```

### Create your first tenant

- Login to the control dashboard on: http://localhost:8888/
- Go to sites and add (example):
    - Databaseschema: `test1`
    - Domainnaam: `test1.pleio.local`
    - Website profiel: "Standaard behouden"

### Edit hosts file
To be able to reach `.local` urls add the following line to the hosts file:

`127.0.0.1 test1.pleio.local`

On Windows you can find your hosts file here:

`C:/Windows/System32/drivers/etc/hosts`

On OSX enter the following in Terminal:

```
sudo nano /etc/hosts
```

Now browse to: http://test1.pleio.local:8000

### Run with a local frontend

To run a local frontend with backend2 it is needed to first run the frontend (`yarn start`) and then restart the backend. At launch it will check whether a local frontend is running.

When opening http://test1.pleio.local:8000 you should see your local front-end being used. This frontend is now loaded from `localhost:9001`. This can be checked by inspecting the network activity. The CSS and JS should be loaded from `localhost:9001`.

### Cleanup and start over

When you want to start with a clean installations run the following command to delete all volumes:

```bash
docker compose down -v
docker compose rm -f
docker compose pull
```

### Run unittests and linting

Format your code using [Ruff](https://docs.astral.sh/ruff/):

```bash
docker compose exec api ruff format .
```

Run code quality checks using Ruff, optionally with automatic fixes:

```bash
docker compose exec api ruff check .
# Optionally use the `--fix` flag to perform autofixes (recommended):
docker compose exec api ruff check . --fix
```

**Note:** Ruff will also format, check, and autofix through `pre-commit`.

Before pushing your local development branch to Gitlab it is adviced to locally run all unittests + linting with --parallel option (fastest):

```bash
docker compose -f docker-compose.test.yml build
docker compose -f docker-compose.test.yml run api /ci-test.sh --parallel
```

Run specific app tests:

```bash
docker compose -f docker-compose.test.yml build
docker compose -f docker-compose.test.yml run api /ci-test.sh core --parallel
```

Run specific test:

```bash
docker compose -f docker-compose.test.yml build
docker compose -f docker-compose.test.yml run api /ci-test.sh core.tests.test_entities.EntitiesTestCase
```

## Dnsmasq

How to setup Dnsmasq for `*.local` domains on a mac:

```bash
brew install dnsmasq
echo 'address=/.local/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf
sudo brew services start dnsmasq
sudo mkdir -v /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/local'
```

## EMAIL

You can enable mailcatcher by adding mailcather to `COMPOSE_PROFILES` in your `.env` for example:

COMPOSE_PROFILES=base,admin,mailcatcher

After you restart docker-compose you should be able to view mail at `http://localhost:1080`
