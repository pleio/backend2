---
sidebar_position: 7
---

For testing or bugfixing purposes you might need to restore the backup of a production site locally. Follow these steps to restore a backup:

## Create a backup

### At Control

1. Open the [Test Control center](https://control-test.pleio-beheer.nl/).
2. Go to the `Sites` tab.
3. Find your site and click on the `Back-up` button.
4. Check "Bestanden opnemen" and "Maak een zip-bestand" and click "Ga verder".
5. The backup will be created and can be downloaded a few minutes later.

### Using the commandline

```bash
docker compose exec api python manage.py backup_tenant --schema=test1
```

## Restore the backup

### At Control

1. Unzip the downloaded site in the folder `/pleio-dev-stack/backend/media/backups/`.
1. Open the [Local Control center](http://localhost:8888/).
2. Go to the `Sites` tab.
3. Click "Voeg toe".
4. Fill in the following fields, according to this example:
    - Databaseschema: `klantnaam`
    - Domeinnaam: `klantnaam.pleio.local`
    - Vanuit back-up: `datum_klantnaam` (this is the name of the folder you unzipped in `backend/media/backups`)
    - Website profiel: `Standaard behouden`
5. Click "Website toevoegen".
6. Wait a few minutes for the site to be created. It will show up in the "Sites" tab.

### Using the commandline

```bash
docker compose exec api python manage.py restore_tenant --schema=test1 --backup-folder=[datestamp]_test1
```

## Clean up

When you're done testing/bugfixing, you need to remove all local data:

1. In [Local Control center](http://localhost:8888/) delete the site.
2. Delete the unzipped backup folder from `/pleio-dev-stack/backend/media/backups/`.
3. Delete the downloaded zip file.
4. Empty your OS trash bin.
