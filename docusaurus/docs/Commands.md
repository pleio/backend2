---
sidebar_position: 4
---

## Advanced

Now you have your first tenant running there are some more advanced topics:

- [Elasticsearch](#elasticsearch)
- [Background](#background)
- [Translations](#translations)

## Quickly start with a fresh database:

```bash
# Clean up the old database
docker compose exec api python manage.py delete_tenant -s test1

# Create a tenant from a backup
export BACKUP_NAME=[name of the folder where the backup is located - not a path]
docker compose exec api python manage.py restore_tenant --backup-folder BACKUP_NAME --schema test1 --domain test1.pleio.local
```

## Elasticsearch

We use [elasticsearch](https://www.elastic.co/) for searching.

#### Create search index

First time you have to create the search index.

```bash
docker compose exec api python manage.py tenant_command search_index --create --schema=test1
```

#### Rebuilding search index

Rebuilding the index is required when the schema for a model changes at document.py.
Rebuilding the index causes the search functionality to be unavailable until all data is indexed again.

```bash
# Rebuild for all (Notice: search will be unavailable until an index command is completed)
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_recreate_indices
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_rebuild_all

# Rebuild for one (Notice: all data is removed first)
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_rebuild_for_tenant --args='["test1"]'
```

When there has been a bug that caused the content to be indexed incorrectly it is possible to index all data without search engine down-time.

```bash
# Build for all (search will be available all the time)
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_index_data_for_all

# Build for one (search will be available all the time)
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_index_data_for_tenant --args='["test1"]'
```

## Translations

### (re)Generate the translations files

With this command, you will create and edit .po files. The files will be filled with strings added in the code as msgid's

```bash
docker compose exec api python manage.py makemessages -a
```

With this command, you will automatically translate fuzzy and missing strings in .po files. See [django-autotranslate](https://github.com/ankitpopli1891/django-autotranslate/) for more usage options. A DEEPL token in your environment is necesarry.

```bash
docker compose exec api python manage.py translate_messages --untranslated
```


With this command, you will compile the translation files which the application will use. To activate locally, restart the api container.

```bash
docker compose exec api python manage.py compilemessages
```

## Background

Pleio uses [Celery](http://www.celeryproject.org/) for running background tasks.

### Manual call commands

To manually call commands from the CLI, use:

```bash
docker compose exec background celery -A backend2.celery call {taskname} --args='{args}'
```

Possible tasknames and arguments:

- `core.tasks.cronjobs.dispatch_hourly_cron`
- `core.tasks.cronjobs.dispatch_daily_cron`
- `core.tasks.cronjobs.dispatch_weekly_cron`
- `core.tasks.cronjobs.dispatch_monthly_cron`
- `core.tasks.cronjobs.dispatch_task, ["{task_name}", **"{arguments}"]`
- `core.tasks.notification_tasks.send_notifications, ["{schema_name}"]`
- `core.tasks.cronjobs.send_overview, ["{schema_name}", "{overview}"]`
- core.tasks.elasticsearch_tasks.elasticsearch_recreate_indices
- `core.tasks.elasticsearch_tasks.elasticsearch_recreate_indices ["{index_name}"]`
- core.tasks.elasticsearch_tasks.elasticsearch_rebuild_all
- `core.tasks.elasticsearch_tasks.elasticsearch_rebuild_all ["{index_name}"]`
- `core.tasks.elasticsearch_tasks.elasticsearch_rebuild, ["{schema_name}"]`
- `core.tasks.elasticsearch_tasks.elasticsearch_rebuild, ["{schema_name}","{index_name}"]`

Some example commands:

### Search index recreate all indexes after changes in documents
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_recreate_indices
```

### Search index recreate 1 index after changes in documents
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_recreate_indices --args='["blog"]'
```

### Search index populate all content for all tenants
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_index_data_for_all
```

### Search index populate 1 index for all tentants
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_index_data_for_all --args='["blog"]'
```

### Search index populate 1 index of 1 tentant
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_index_data_for_tenant --args='["tenant1", "blog"]'
```

### Search index repopulate all content for all tenants
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_rebuild_all
```

### Search index repopulate 1 index for all tentants
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_rebuild_all --args='["blog"]'
```

### Search index repopulate 1 index of 1 tentant
```
docker compose exec background celery -A backend2.celery call core.tasks.elasticsearch_tasks.elasticsearch_rebuild_for_tenant --args='["tenant1", "blog"]'
```

#### Run the daily cron on all tenants:

```bash
docker compose exec background celery -A backend2.celery call core.tasks.cronjobs.dispatch_daily_cron
```

#### Report about inconsistent TAG_CATEGORIES:

```bash
docker compose exec background celery -A backend2.celery call core.tasks.reporting.report_is_tag_categories_consistent --args='["erlend@pleio.nl"]'
```
