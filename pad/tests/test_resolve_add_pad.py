from unittest import mock

from core import config
from core.factories import GroupFactory
from core.tests.helpers import PleioTenantTestCase
from user.factories import AdminFactory, UserFactory


class AddPadTestCase(PleioTenantTestCase):
    def setUp(self):
        super().setUp()
        self.authenticatedUser = self.remember(UserFactory())
        self.authenticatedUserNonGroupMember = self.remember(UserFactory())
        self.adminUser = self.remember(AdminFactory())
        self.override_config(
            EXTRA_LANGUAGES=["en", "nl", "de"],
            LANGUAGE="de",
        )
        self.data = {
            "input": {
                "title": "My first Pad",
                "richDescription": "richDescription",
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            }
        }
        self.mutation = """
            mutation ($input: addPadInput!) {
                addPad(input: $input) {
                    entity {
                        ... on Pad {
                            showOwner
                            inputLanguage
                            isTranslationEnabled
                            title
                            richDescription
                            accessId
                            writeAccessId
                            group { guid }
                            rootContainer { guid }
                            container { guid }
                            isPersonalFile
                        }
                    }
                }
            }
        """

    @mock.patch("core.resolvers.shared.copy_attachment_references")
    def test_add_pad(self, copy_attachment_references):
        group = self.remember(GroupFactory(owner=self.authenticatedUser))
        variables = self.data
        variables["input"]["containerGuid"] = group.guid

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addPad"]["entity"]

        self.assertEqual(
            entity,
            {
                "accessId": config.DEFAULT_ACCESS_ID,
                "container": {"guid": group.guid},
                "group": {"guid": group.guid},
                "isPersonalFile": False,
                "richDescription": variables["input"]["richDescription"],
                "rootContainer": {"guid": group.guid},
                "showOwner": True,
                "title": variables["input"]["title"],
                "writeAccessId": 0,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            },
        )
        self.assertTrue(copy_attachment_references.called)

    def test_add_pad_to_personal_files(self):
        variables = self.data
        variables["input"]["containerGuid"] = self.authenticatedUser.guid

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addPad"]["entity"]

        self.assertEqual(
            entity,
            {
                "showOwner": True,
                "title": variables["input"]["title"],
                "richDescription": variables["input"]["richDescription"],
                "accessId": config.DEFAULT_ACCESS_ID,
                "writeAccessId": 0,
                "group": None,
                "rootContainer": {"guid": self.authenticatedUser.guid},
                "container": {"guid": self.authenticatedUser.guid},
                "isPersonalFile": True,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            },
        )

    def test_add_pad_access(self):
        group = self.remember(GroupFactory(owner=self.authenticatedUser))
        variables = self.data
        variables["input"]["containerGuid"] = group.guid
        variables["input"]["accessId"] = 4
        variables["input"]["writeAccessId"] = 4

        self.graphql_client.force_login(self.authenticatedUser)
        result = self.graphql_client.post(self.mutation, variables)
        entity = result["data"]["addPad"]["entity"]

        self.maxDiff = None

        self.assertEqual(
            entity,
            {
                "accessId": 4,
                "container": {"guid": group.guid},
                "group": {"guid": group.guid},
                "isPersonalFile": False,
                "richDescription": variables["input"]["richDescription"],
                "rootContainer": {"guid": group.guid},
                "showOwner": True,
                "title": variables["input"]["title"],
                "writeAccessId": 4,
                "inputLanguage": "en",
                "isTranslationEnabled": False,
            },
        )
