from core.tests.helpers.test_translation import Wrapper
from entities.file.factories import PadFactory


class TestTranslationFields(
    Wrapper.TestTitleFieldTranslationTestCase,
    Wrapper.TestRichDescriptionFieldTranslationTestCase,
    Wrapper.TestDescriptionFieldTranslationTestCase,
):
    ENTITY_TYPE = "Pad"

    def build_entity(self, **kwargs):
        return PadFactory(**kwargs)
