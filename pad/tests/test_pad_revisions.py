from core.tests.helpers.revision_template import RevisionTemplate
from entities.file.factories import PadFactory
from user.factories import UserFactory


class TestPadRevisionsTestCase(RevisionTemplate.BaseTestCase):
    editMutation = "editPad"
    editMutationInput = "editPadInput"

    useWriteAccess = True

    def build_entity(self, owner):
        return PadFactory(
            owner=owner,
            title="Default title",
            rich_description=self.tiptap_paragraph("Default rich description"),
        )

    def build_owner(self):
        return UserFactory()

    def localSetUp(self):
        super().localSetUp()
        self.reference_data = {
            "title": self.entity.title,
            "richDescription": self.entity.rich_description,
        }

    def test_multiple_authors(self):
        self.localSetUp()
        coauthor1 = UserFactory()
        coauthor2 = UserFactory()
        mutation = """
        mutation UpdatePadMultipleAuthors($input: editPadInput!, $coauthors: [String]) {
            editPad(input: $input, coauthors: $coauthors) {
                entity { guid }
            }
        }
        """
        variables = {
            "input": {
                "guid": self.entity.guid,
                "richDescription": "other rich description 2",
            },
            "coauthors": [coauthor1.guid, coauthor2.guid],
        }

        self.graphql_client.force_login(self.admin)
        self.graphql_client.post(mutation, variables)
        response = self.graphql_client.post(self.query, {"guid": self.entity.guid})

        edges = response["data"]["revisions"]["edges"]
        coauthors = {a["guid"] for a in edges[0]["authors"]}
        self.assertEqual(coauthors, {coauthor1.guid, coauthor2.guid})
