from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from core import config


def get_search_filters():
    if config.COLLAB_EDITING_ENABLED:
        yield {
            "key": "pad",
            "value": _("Pad"),
            "plural": pgettext_lazy("plural", "Pads"),
        }
