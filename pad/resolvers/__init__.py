from ariadne import ObjectType

from .mutation import resolve_add_pad, resolve_edit_pad
from .pad import pad

mutation = ObjectType("Mutation")

mutation.set_field("addPad", resolve_add_pad)
mutation.set_field("editPad", resolve_edit_pad)

resolvers = [pad, mutation]
