from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core import config
from core.constances import COULD_NOT_FIND, COULD_NOT_SAVE
from core.lib import access_id_to_acl, clean_graphql_input
from core.models.group import Group
from core.resolvers import shared
from entities.file.models import FileFolder


def resolve_add_pad(_, info, input):
    user = info.context["request"].user

    clean_input = clean_graphql_input(input)

    shared.assert_authenticated(user)

    group = None
    parent = None

    if "containerGuid" in clean_input:
        try:
            group = Group.objects.get(id=clean_input.get("containerGuid"))
        except ObjectDoesNotExist:
            try:
                parent = FileFolder.objects.get(id=clean_input.get("containerGuid"))
                if not parent.can_write(user):
                    raise GraphQLError(COULD_NOT_SAVE)
                group = parent.group
            except ObjectDoesNotExist:
                if clean_input.get("containerGuid") != user.guid:
                    msg = "INVALID_CONTAINER_GUID"
                    raise GraphQLError(msg)

    shared.assert_group_member(user, group)

    entity = FileFolder()
    entity.type = FileFolder.Types.PAD

    entity.owner = user
    entity.tags = clean_input.get("tags", [])

    if parent:
        entity.parent = parent

    if group:
        entity.group = group

    shared.resolve_add_input_language(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    resolve_update_access_id(entity, clean_input, user)

    entity.save()
    shared.store_initial_revision(entity)

    shared.copy_attachment_references(entity, parent, group)

    return {"entity": entity}


def resolve_edit_pad(_, info, input, coauthors=None):
    user = info.context["request"].user

    try:
        entity = FileFolder.objects.filter(type=FileFolder.Types.PAD).get(
            id=input.get("guid")
        )
    except ObjectDoesNotExist:
        raise GraphQLError(COULD_NOT_FIND)

    clean_input = clean_graphql_input(input)

    revision = shared.resolve_start_revision(entity, user, coauthors)

    shared.assert_authenticated(user)
    shared.assert_write_access(entity, user)

    shared.resolve_update_input_language(entity, clean_input)
    shared.resolve_update_tags(entity, clean_input)
    shared.resolve_update_title(entity, clean_input)
    shared.resolve_update_rich_description(entity, clean_input)
    shared.update_is_translation_enabled(entity, clean_input)
    shared.resolve_update_access_id(entity, clean_input, user)
    shared.update_updated_at(entity)

    if clean_input.get("state"):
        entity.pad_state = clean_input.get("state")

    entity.save()
    shared.store_update_revision(revision, entity)

    return {"entity": entity}


def resolve_update_access_id(entity, clean_input, user):
    can_update_access_level = shared.update_access_level_allowed(user, entity.group)
    if not can_update_access_level or "accessId" not in clean_input:
        default_access_id = (
            4 if entity.group and entity.group.is_closed else config.DEFAULT_ACCESS_ID
        )
        entity.read_access = access_id_to_acl(entity, default_access_id)
    else:
        entity.read_access = access_id_to_acl(entity, clean_input["accessId"])
    entity.write_access = access_id_to_acl(entity, clean_input.get("writeAccessId", 0))
